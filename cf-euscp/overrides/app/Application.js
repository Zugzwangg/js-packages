Ext.define('CFJS.overrides.app.Application', {
	override: 'CFJS.app.Application',
	requires: [ 'CFJS.euscp.Util' ],

	showLogin: function(api) {
		if (CFJS.hasSessionStorage) CFJS.euscp.removeStoredPrivateKey();
		this.callParent(arguments);
	}

});