Ext.define('CFJS.overrides.model.webdav.Resource', {
	override: 'CFJS.model.webdav.Resource',
	signFile: function(service, callback) {
		var me = this, file;
		if (me.get('isFile')) {
			if (file = me.get('file')) {
				CFJS.euscp.signFile({
					file		: file,
					saveToFile	: false,
					callback	: function(success, sign) {
						if (success === true) {
							me.set({ name: me.get('name') + '.p7s', lastModified: new Date() });
							me.save({
								callback: function(record, operation, success) {
									file = new File([sign], record.get('name'), { type: file.type, lastModified: record.get('lastModified') });
									file.data = sign;
									me.set({ file: file }, { commit: true });
									Ext.callback(callback, me, [me, success, sign]);
								}
							});
						} else Ext.callback(callback, me, [me, success, sign]);
					}
				});
			} else me.download(service, function(response, fileName) {
				if (response && response.responseBytes) {
					file = new File([response.responseBytes], fileName, { type: response.getResponseHeader('Content-Type'), lastModified: new Date() });
					file.data = response.responseBytes;
					me.set('file', file);
					me.signFile(service, callback); 
				} else Ext.callback(callback, me, [me, false]);
			});
		} else Ext.callback(callback, me, [me, false]);
	}

});