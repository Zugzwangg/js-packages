Ext.define('CFJS.overrides.model.FileModel', {
	override: 'CFJS.model.FileModel',
	signFile: function(service, callback) {
		var me = this, file;
		if (me.get('isFile')) {
			if (file = me.get('file')) {
				CFJS.euscp.signFile({
					file		: file,
					saveToFile	: false,
					callback	: function(success, sign) {
						if (success === true) {
							file = new File([sign], me.get('name') + '.p7s', { type: file.type, lastModified: new Date() });
							file.data = sign;
							record.set({
								file: file,
								name: file.name,
								date: file.lastModifiedDate,
								size: file.size
							}, { commit: true });
							Ext.callback(callback, me, [me, success, sign]);
						} else Ext.callback(callback, me, [me, success, sign]);
					}
				});

			} else me.download(service, function(response, fileName) {
				if (response && response.responseBytes) {
					me.set('file', file = new File([response.responseBytes], fileName, { type: response.getResponseHeader('Content-Type'), lastModified: new Date() }));
					me.signFile(service, callback);
				} else Ext.callback(callback, me, [me, false]);
			});
		} else Ext.callback(callback, me, [me, false]);
	}

});