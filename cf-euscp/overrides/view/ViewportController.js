Ext.define('CFJS.overrides.view.ViewportController', {
	override: 'CFJS.view.ViewportController',

	readPK: function(scope, callback) {
		var me = this, pkreader, signButton,
			picker = CFJS.apply({
				xtype			: 'window',
				autoShow		: true,
				bodyStyle		: 'background: #ececec;',
				buttons			: [{
					action		: 'signFile',
					disabled	: true,
					reference	: 'signButton',
					text		: 'Sign'
				}],
				defaultButton	: 'signButton',
				iconCls			: CFJS.UI.iconCls.KEY,
				modal			: true,
				referenceHolder	: true,
				title			: 'Private key reading',
				items			: { xtype: 'pkreader' }
			}, me.pkPickerConfig);
		picker = Ext.create(picker);
		picker.on({
			close: function() {
				Ext.callback(callback, scope || me, [picker.sign === true]);
			}
		});
		if (signButton = picker.down('button[action=signFile]')) {
			signButton.on({
				click: function() {
					picker.sign = true;
					picker.close();
				}
			});
			if (pkreader = picker.down('pkreader')) {
				pkreader.on({
					pkreaded: function(pkOwnerInfo) {
						signButton.setDisabled(Ext.Object.isEmpty(pkOwnerInfo));
					}
				});
			}
		}
	}

});