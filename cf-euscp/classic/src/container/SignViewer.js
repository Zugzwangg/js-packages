Ext.define('CFJS.euscp.container.SignViewer', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.euscp.SignViewer',
	alias				: 'widget.sign-viewer',
	requires			: [ 'Ext.util.Format' ],
	colspan				: 2,
	layout				: 'vbox',
	naValue				: 'N/A',
	sgFileLabel			: 'Selected file',
	sgIssuerCNLabel		: 'CA',
	sgOrgLabel			: 'Company',
	sgResultLabel		: 'Verify result',
	sgSerialLabel		: 'Serial number',
	sgSignatureLabel	: 'Signature',
	sgSignerLabel		: 'Signer',
	sgTimeLabel			: 'Time',
	timeSignatureText	: '(time signature)',
	timeStampText		: '(timestamp)',
	verifyErrorText		: 'Error signature verification',
	verifyingText		: 'Verifying signatures...',
	verifySuccessText	: 'Signature successfully tested',

	addInfoContainer: function(index) {
		var me = this,
			container = {
				xtype		: 'container',
				defaults	: {
					labelStyle	: 'font-size:16px;font-weight:bolder;',
					labelWidth	: 160,
					margin		: 0,
					publishes	: 'value',
					value		: me.naValue,
					width		: 700
				},
				defaultType	: 'displayfield',
				layout		: 'vbox',
				items		: [{
					fieldLabel	: me.sgFileLabel,
					name		: 'sgFileName'
				},{
					fieldLabel	: me.sgResultLabel,
					name		: 'sgVerifyResult'
				},{
					fieldLabel	: me.sgSignerLabel,
					name		: 'sgSignerName'
				},{
					fieldLabel	: me.sgOrgLabel,
					name		: 'sgOrgName'
				},{
					fieldLabel	: me.sgTimeLabel,
					name		: 'sgTime'
				},{
					fieldLabel	: me.sgIssuerCNLabel,
					name		: 'sgIssuerCN'
				},{
					fieldLabel	: me.sgSerialLabel,
					name		: 'sgSerial'
				}]
			};
		if (index > 0) me.add({ xtype: 'label', cls: 'signature-label', margin: '15 0 10 0',  text: me.sgSignatureLabel + ' ' + index });
		return me.add(container);
	},

	initComponent: function() {
		var me = this;
		me.dateFormat = me.dateFormat || Ext.Date.patterns.LongDateTime;
		me.maskCmp = me.maskCmp || me;
		me.callParent();
	},

	setSignInfo: function(signInfo) {
		var me = this, single = !Ext.isArray(signInfo),
		 	i = 0, container, info, key, field;
		Ext.suspendLayouts();
		me.removeAll();
		if (single) signInfo = [signInfo];
		for (; i < signInfo.length; i++) {
			container = me.addInfoContainer(me, single ? i : i + 1);
			info = signInfo[i];
			info.sgVerifyResult = info.sgVerifyResult || me.verifySuccessText;
			if (!Ext.isEmpty(info.data)) info.sgFileName = '<a href="' + CFJS.createObjectURL(info.data) + '" type="' + info.data.type + '" download >' + info.sgFileName + '</a>';
			if (Ext.isDate(info.sgTime)) info.sgTime = Ext.util.Format.date(info.sgTime, me.dateFormat) + ' ' + (info.isTimeStamp ? me.timeStampText : me.timeSignatureText);
			delete info.isTimeStamp;
			for (key in info) {
				field = container.down('displayfield[name=' + key +']');
				if (field && field.isFormField) {
					field.setValue(Ext.isEmpty(info[key]) ? me.naValue : info[key]);
				}
			}
		}
		Ext.resumeLayouts(true);
	},

	verifyFile: function(file, callback) {
		var me = this;
		me.maskCmp.mask(me.verifyingText);
		setTimeout(function() {
			CFJS.euscp.verifyFile({
				file: file,
				callback: function(success, info) {
					me.maskCmp.unmask();
					if (success !== true) info = {
						sgVerifyResult: me.verifyErrorText,
						sgFileName: null,
						sgSignerName: null,
						sgOrgName: null,
						sgIssuerCN: null,
						sgSerial: null,
						sgTime: null
					};
					me.setSignInfo(info);
					Ext.callback(callback, me, [me, success, info]);
				}
			});
		}, 100);
	}

});
