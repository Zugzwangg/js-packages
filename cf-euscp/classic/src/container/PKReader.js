Ext.define('CFJS.euscp.container.PKReader', {
	extend				: 'CFJS.container.FileUpload',
	alternateClassName	: 'CFJS.euscp.PKReader',
	alias				: 'widget.pkreader',
	requires			: [
		'Ext.button.Button',
		'Ext.form.field.Display',
		'Ext.form.field.Text',
		'CFJS.form.field.DictionaryComboBox'
	],
	caLabel			: 'Select CA',
	defaultFocus	: 'dictionarycombo[name=caServers]',
	header			: false,
	loadText		: 'Load key',
	loadingText		: 'Loading key...',
	naValue			: 'N/A',
	pkIssuerCNLabel	: 'CA',
	pkOrgLabel		: 'Company',
	pkOwnerLabel	: 'Owner',
	pkSerialLabel	: 'Serial number',
	pwdLabel		: 'Protection key password',
	plugins			: [{ ptype: 'filedrop', readFiles: false }],
	uploadText		: 'Choose a key file (usually is Key-6.dat) or drag it here:',
	
	initComponent: function() {
		var me = this, container; 
		me.callParent();
		if ((container = me.form) && container.isPanel) {
			CFJS.euscp.Util.pkReader = me;
			container.on('validitychange', me.onValidityChange, me);
			container = container.down('container#table');
			container.remove(container.down('image'));
			container.insert(0, {
				xtype			: 'dictionarycombo',
				allowBlank		: false,
				colspan			: 2,
				editable		: false,
				emptyText		: me.caLabel,
				fieldLabel		: me.caLabel,
				forceSelection	: true,
				labelStyle		: 'font-size:16px;font-weight:bold;',
				labelWidth		: 160,
				margin			: 0,
				name			: 'caServers',
				publishes		: 'value',
				queryMode		: 'local',
				store			: CFJS.euscp.getStore(),
				typeAhead		: false,
				value			: CFJS.euscp.caServer,
				width			: 700,
				listeners		: { scope: me, change: me.onCAChange }
			});
			container.add({
				xtype		: 'textfield',
				emptyText	: me.pwdLabel.toLowerCase(),
				fieldLabel	: me.pwdLabel,
				labelStyle	: 'font-size:16px;font-weight:bold;',
				labelWidth	: 250,
				inputType	: 'password',
				margin		: 0,
				name		: 'keyPassword',
				required	: true,
				width		: 590
			},{
				xtype		: 'button',
				formBind	: true,
				handler		: me.onLoadKeyClick, 
				scope		: me,
				text		: me.loadText,
				width		: 100
			});
		}
	},
	
	afterRender: function() {
		var me = this;
		me.callParent();
		me.down(me.defaultFocus).focus();
	},
	
	onCAChange: function(combo, ca) {
		CFJS.euscp.setCASettings(ca);
	},
	
	onLoadKeyClick: function() {
		var me = this;
		me.setLoading(me.loadingText);
		CFJS.euscp.loadPrivateKey({
			keyFile	: me.keyFile,
			password: me.down('textfield[name=keyPassword]').getValue(),
			callback: function(success, pkOwnerInfo) {
				me.setLoading(false);
				if (success !== true) pkOwnerInfo = { pkOwnerName: null, pkIssuerCN: null, pkSerial: null };
				me.setPKOwnerInfo(pkOwnerInfo);
				me.fireEvent('pkreaded', pkOwnerInfo);
			}
		});
	},
	
	onValidityChange: function(form, valid) {
		var me = this,
			boundItems = form.getBoundItems(),
			i = 0, cmp;
		if (boundItems) {
			boundItems = boundItems.items;
			valid = valid && !Ext.isEmpty(me.keyFile);
			for (; i < boundItems.length; i++) {
				if ((cmp = boundItems[i]).disabled === valid) {
                    cmp.setDisabled(!valid);
                }
            }
		}
	},
	
	getInfoContainer: function() {
		var me = this;
		me.infoContainer = me.infoContainer || me.down('container#table').add({
			xtype		: 'container',
			colspan		: 2,
			defaults	: {
				labelStyle	: 'font-size:16px;font-weight:bolder;',
				labelWidth	: 160,
				margin		: 0,
				publishes	: 'value',
				value		: me.naValue,
				width		: 700
			},
			defaultType	: 'displayfield',
			layout		: 'vbox',
			items		: [{
				fieldLabel	: me.pkOwnerLabel,
				name		: 'pkOwnerName'
			},{
				fieldLabel	: me.pkOrgLabel,
				name		: 'pkOrgName'
			},{
				fieldLabel	: me.pkIssuerCNLabel,
				name		: 'pkIssuerCN'
			},{
				fieldLabel	: me.pkSerialLabel,
				name		: 'pkSerial'
			}]
		});
		return me.infoContainer;
	},
	
	setPKOwnerInfo: function(ownerInfo) {
		var me = this, container = me.getInfoContainer(),
			key, field;
		Ext.suspendLayouts();
		for (key in ownerInfo) {
			field = container.down('displayfield[name=' + key +']');
			if (field && field.isFormField) field.setValue(Ext.isEmpty(ownerInfo[key]) ? me.naValue : ownerInfo[key]);
		}
		Ext.resumeLayouts(true);
	},
	
	uploadFile: function(file) {
		var me = this, 
			form = me.form.getForm();
		me.keyFile = file;
		me.onValidityChange(form, !form.hasInvalidField());
	}

});