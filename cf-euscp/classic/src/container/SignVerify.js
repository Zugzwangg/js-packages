Ext.define('CFJS.euscp.container.SignVerify', {
	extend				: 'CFJS.container.FileUpload',
	alternateClassName	: 'CFJS.euscp.SignVerify',
	alias				: 'widget.sign-verify',
	requires			: [ 'CFJS.euscp.container.SignViewer' ],
	header				: false,
	plugins				: [{ ptype: 'filedrop', readType: 'ArrayBuffer' }],
	uploadText			: 'Choose a signed file (usually is .p7s) or drag it here:',

	getInfoContainer: function() {
		var me = this;
		me.infoContainer = me.infoContainer || me.down('container#table').add({ xtype: 'sign-viewer', maskCmp: me });
		return me.infoContainer;
	},
	
	setSignInfo: function(signInfo) {
		this.getInfoContainer().setSignInfo(signInfo);
	},

	uploadFile: function(file) {
		this.getInfoContainer().verifyFile(file);
	}

});
