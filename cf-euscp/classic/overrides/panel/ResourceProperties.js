Ext.define('CFJS.euscp.overrides.panel.ResourceProperties', {
    override: 'CFJS.panel.ResourceProperties',
	requires: [ 'CFJS.euscp.container.SignViewer' ],

	createTabs: function() {
		var me = this, tabs = me.callParent();
		tabs.push(CFJS.apply({
			xtype	: 'form',
			bbar	: {
				items: [{
					xtype	: 'button',
					handler	: function(btn) {
						me.disableComponent(btn, true);
						me.verifySignatures(me.lookupViewModel().get('resource'), function() {
							me.disableComponent(btn);
						});
					},
					iconCls	: CFJS.UI.iconCls.KEY,
					text	: 'Verify signatures'
				}],
				layout: { pack: 'end' }
			},
			iconCls	: 'x-fa fa-check-circle-o',
			layout	: 'fit',
			title	: 'Signatures',
			name	: 'signViewer',
			items	: [{ xtype: 'sign-viewer', maskCmp: me }]
		}, me.signaturesConfig));
		return tabs;
	},
	
	onResourceTypeChange: function(isFile) {
		var me = this, signViewer = me.down('[name=signViewer]');
		if (signViewer && signViewer.tab) {
			signViewer.tab.setVisible(isFile);
			if (!isFile && signViewer === me.getActiveTab()) me.setActiveTab(0);
		}
		me.callParent(arguments);
	},
	
	verifySignatures: function(resource, callback) {
		var me = this, util = CFJS.euscp.Util,
			showAlerts = util.showAlerts,
			signViewer = me.down('sign-viewer'),
			signInfo, file;
		if (signViewer && resource && resource.get('isFile')) {
			if (!(signInfo = resource.get('signInfo'))) {
				if (file = resource.get('file')) {
					util.showAlerts = false;
					signViewer.verifyFile(file, function(viewer, success, info) {
						util.showAlerts = showAlerts;
						resource.set('signInfo', signInfo, { commit: true, silent: true });
						Ext.callback(callback, me, [resource, signInfo]);
					});
				} else resource.download(null, function(response, fileName) {
					if (response && response.responseBytes) {
						file = new File([response.responseBytes], fileName, { type: response.getResponseHeader('Content-Type'), lastModified: resource.get('lastModified') });
						file.data = response.responseBytes;
						resource.set('file', file, { commit: true, silent: true });
						me.verifySignatures(resource, callback);
					}
				});
				return;
			}
			signViewer.setSignInfo(signInfo);
		}
		Ext.callback(callback, me, [resource, signInfo]);
	}

});