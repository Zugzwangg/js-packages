Ext.define('CFJS.euscp.overrides.grid.FilesPanel', {
	override: 'CFJS.grid.FilesPanel',
	requires: [ 'Ext.window.Window' ],
	config	: {
		actions		: {
			signFile: {
				disabled: !CFJS.hasFile,
				handler	: 'onFileSign',
				iconCls	: CFJS.UI.iconCls.SIGN,
				title	: 'Sign files',
				tooltip	: 'Sign selected files',
			}
		},
		actionsDisable	: [ 'signFile' ],
		actionsMap		: {
			contextMenu	: { items: [ 'levelUp', 'signFile', 'removeFile' ] },
			header		: { items: [ 'levelUp', 'signFile', 'reloadStore' ] }
		},
		contextMenu	: { items: new Array(3) },
		permissions	: { create: true, 'delete': true, sign: true },
	},
	signFile: true,

	createPKReader: function() {
		var me = this, pkreader, signButton,
			picker = CFJS.apply({
			xtype			: 'window',
			autoShow		: true,
			bodyStyle		: 'background: #ececec;',
			buttons			: [{
				action		: 'signFile',
				disabled	: true,
				reference	: 'signButton',
				text		: 'Sign'
			}],
			defaultButton	: 'signButton',
			iconCls			: CFJS.UI.iconCls.KEY,
			modal			: true,
			referenceHolder	: true,
			title			: 'Private key reading',
			items			: { xtype: 'pkreader' }
		}, me.pkPickerConfig);
		picker = Ext.create(picker);
		if (signButton = picker.down('button[action=signFile]')) {
			signButton.on({
				click: function() {
					picker.hide();
					me.onFileSign();
				}
			});
			if (pkreader = picker.down('pkreader')) {
				pkreader.on({
					pkreaded: function(pkOwnerInfo) {
						signButton.setDisabled(Ext.Object.isEmpty(pkOwnerInfo));
					}
				});
			}
		}
		return picker;
	},
	
	doSelectionChange: function(selectable, selection, actions) {
		this.disableComponent(actions.signFile, !(selection && selection.length > 0));
	},

	initComponent: function() {
		var me = this;
		me.hideComponent(me.getActions().signFile, !((me.getPermissions()||{}).sign && me.signFile && CFJS.euscp.initialized));
		me.callParent();
	},
	
	onFileSign: function(component) {
		var me = this, $sg = CFJS.euscp.$sg, records,
			formData = new FormData(), i = 0, count = 0;
		if (!$sg.IsPrivateKeyReaded()) {
			var picker = me.createPKReader();
			return;
		};
		if (records = me.getSelection()) {
			me.setLoading(me.waitMsg);
			for (; i < records.length; i++) {
				count++;
				records[i].signFile(null, function(rec, sign) {
					count--;
					var file = sign && rec.get('file');
					if (file) formData.append('upload', file, file.name);
					if (count === 0) me.uploadFormData(formData);
				});
			}
		}
	}
	
});