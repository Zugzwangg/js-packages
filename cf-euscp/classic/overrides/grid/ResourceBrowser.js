Ext.define('CFJS.euscp.overrides.grid.ResourceBrowser', {
	override: 'CFJS.grid.ResourceBrowser',
	config	: {
		actionsMap	: {
			contextMenu	: { items: [ 'levelUp', 'makeFolder', 'signFile', 'removeFile' ] },
			header		: { items: [ 'levelUp', 'makeFolder', 'signFile', 'reloadStore' ] }
		},
		contextMenu	: { items: new Array(4) },
	}
});