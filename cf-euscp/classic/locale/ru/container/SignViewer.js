Ext.define('CFJS.euscp.locale.ru.container.SignViewer', {
    override			: 'CFJS.euscp.container.SignViewer',
	naValue				: 'Нет',
	sgFileLabel			: 'Выбраный файл',
	sgIssuerCNLabel		: 'АЦСК',
	sgOrgLabel			: 'Организация',
	sgResultLabel		: 'Результат',
	sgSerialLabel		: 'Серийный номер',
	sgSignatureLabel	: 'Подпись',
	sgSignerLabel		: 'Подписант',
	sgTimeLabel			: 'Время подписи',
	timeSignatureText	: '(время подписи)',
	timeStampText		: '(метка времени)',
	verifyErrorText		: 'Ошибка проверки подписи',
	verifySuccessText	: 'Подпись проверена успешно',
	verifyingText		: 'Проверяю подписи...'
});
