Ext.define('CFJS.euscp.locale.ru.container.PKReader', {
    override		: 'CFJS.euscp.container.PKReader',
	caLabel			: 'Выберите АЦСК',
	loadText		: 'Прочитать',
	loadingText		: 'Чтение ключа...',
	naValue			: 'Нет',
	pkIssuerCNLabel	: 'АЦСК',
	pkOrgLabel		: 'Организация',
	pkOwnerLabel	: 'Владелец',
	pkSerialLabel	: 'Серийный номер',
	pwdLabel		: 'Пароль защиты ключа',
	uploadText		: 'Выберите файл ключа (обычно Key-6.dat) или перетащите его сюда:'
});
