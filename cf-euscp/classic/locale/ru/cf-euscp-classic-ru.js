Ext.define('CFJS.euscp.locale.ru.grid.FilesPanel', {
	override: 'CFJS.grid.FilesPanel',
	config	: {
		actions		: {
			signFile: { title: 'Подписать файлы', tooltip: 'Подписать выделенные файлы' }
		},
		pkPickerConfig: {
			buttons	: [{ text: 'Подписать', tooltip: 'Подписать файлы' }],
			title	: 'Чтение секретного ключа'
		}
	}
});
Ext.define('CFJS.euscp.locale.ru.panel.ResourceProperties', {
    override: 'CFJS.panel.ResourceProperties',
    signaturesConfig: {
   		bbar	: { items: [{ text: 'Проверить подписи' }] },
    	title	: 'Подписи'
    }
});