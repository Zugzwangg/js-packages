Ext.define('CFJS.euscp.locale.ukr.container.SignViewer', {
    override			: 'CFJS.euscp.container.SignViewer',
	naValue				: 'Ні',
	sgFileLabel			: 'Обраний файл',
	sgIssuerCNLabel		: 'АЦСК',
	sgOrgLabel			: 'Організація',
	sgResultLabel		: 'Результат',
	sgSerialLabel		: 'Серійний номер',
	sgSignatureLabel	: 'Підпис',
	sgSignerLabel		: 'Підписувач',
	sgTimeLabel			: 'Час підпису',
	timeSignatureText	: '(час підпису)',
	timeStampText		: '(мітка часу)',
	verifyErrorText		: 'Помилка перевірки підпису',
	verifySuccessText	: 'Підпис перевірено успішно',
	verifyingText		: 'Перевіряю підписи...'
});
