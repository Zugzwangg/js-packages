Ext.define('CFJS.euscp.locale.ukr.container.PKReader', {
    override		: 'CFJS.euscp.container.PKReader',
	caLabel			: 'Оберіть АЦСК',
	loadText		: 'Зчитати',
	loadingText		: 'Зчитування ключа...',
	naValue			: 'Ні',
	pkIssuerCNLabel	: 'АЦСК',
	pkOrgLabel		: 'Організація',
	pkOwnerLabel	: 'Власник',
	pkSerialLabel	: 'Серійний номер',
	pwdLabel		: 'Пароль захисту ключа',
	uploadText		: 'Оберіть файл із ключем (зазвичай Key-6.dat) або перетягніть його сюди:'
});
