Ext.define('CFJS.euscp.locale.ukr.grid.FilesPanel', {
	override: 'CFJS.grid.FilesPanel',
	config	: {
		actions		: {
			signFile: { title: 'Підписати файли', tooltip: 'Підписати обрані файли' }
		},
		pkPickerConfig: {
			buttons	: [{ text: 'Підписати', tooltip: 'Підписати файли' }],
			title	: 'Читання секретного ключа'
		}
	}
});
Ext.define('CFJS.euscp.locale.ukr.panel.ResourceProperties', {
    override		: 'CFJS.panel.ResourceProperties',
   	signaturesConfig: {
   		bbar	: { items: [{ text: 'Перевірити підписи' }] },
   		title	: 'Підписи'
   	}
});