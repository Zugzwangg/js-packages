Ext.define('CFJS.euscp.store.CAs', {
	extend		: 'Ext.data.Store',
	alias		: 'store.euscp.CAs',
	model		: 'CFJS.euscp.model.CACertificate',
	storeId		: 'CAs',
	pageSize	: 0
});
