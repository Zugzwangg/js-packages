
var EU_PERSISTENT_FS_SIZE = 10 * 1024 * 1024;

Ext.Loader.loadScript(['/scripts/euscp/euscpt.js', '/scripts/euscp/euscpm.js', '/scripts/euscp/euscp.js']);

//	@define CFJS.euscp.Util
//	@require CFJS
Ext.define('CFJS.euscp.Util', {
	singleton	: true,
	requires	: [
		'Ext.data.StoreManager',
		'CFJS.euscp.container.PKReader',
		'CFJS.euscp.store.CAs'
	],
	pkReader	: null,
	showAlerts	: true,
	
	base64Encode: function(data) {
		if (!Ext.isString(data)) {
			if (Ext.isArray(data) || data instanceof ArrayBuffer) data = new Uint8Array(data);
			if (ArrayBuffer.isView(data)) {
				var bstr = '', len = data.byteLength, i = 0;
				for (; i < len; i++) {
					bstr += String.fromCharCode(data[i]);
				}
				data = bstr;
			}
		}
		if (Ext.isString(data) && !CFJS.euscp.Util.isBase64(data)) data = window.btoa(data);
		return data;
	},

	base64Decode: function(data) {
		if (Ext.isString(data)) {
			if (CFJS.euscp.Util.isBase64(data)) data = window.atob(data);
			var bstr = data, len = bstr.length, i = 0;
			data = new Uint8Array(len)
			for (; i < len; i++){
				data[i] = bstr.charCodeAt(i);
			}
		}
		return data;
	},
	
	clearFolder: function (folderName) {
		CFJS.euscp.Util.deleteFolder(folderName);
		CFJS.euscp.Util.createFolder(folderName);
	},

	createFolder: function (folderName) {
		if (CFJS.euscp.Util.isFolderExists(folderName)) return true;
		window.localStorage.setItem(folderName, JSON.stringify([]));
	},

	deleteFile: function (folderName, fileName, data) {
		var filesArr = CFJS.euscp.Util.getFiles(folderName),
			index = filesArr.indexOf(fileName);
		if (index > 0) {
			filesArr.splice(index, 1);
			window.localStorage.setItem(folderName, JSON.stringify(filesArr));
		}
		window.localStorage.removeItem(folderName + "/" + fileName);
	},

	deleteFolder: function (folderName) {
		var me = CFJS.euscp.Util, i = 0, files;
		if (!me.isFolderExists(folderName)) return false;
		files = me.getFiles(folderName);
		for (; i < files.length; i++) {
			window.localStorage.removeItem(files[i]);
		}
		window.localStorage.removeItem(folderName);
		return true;
	},

	errorAlert: function(message, callback) {
		if (Ext.isObject(message)) message = 'Код: ' + message.errorCode + '. ' + message.message;
		if (CFJS.euscp.Util.showAlerts) CFJS.errorAlert('Помилка', message, null, callback);
		else {
			console.error(message);
			Ext.callback(callback, CFJS.euscp.Util);
		}
	},
	
	getFiles: function (folderName) {
		if (!CFJS.euscp.Util.isFolderExists(folderName)) return null;
		return JSON.parse(window.localStorage.getItem(folderName));
	},

	getDataFromServerAsync: function(url, onSuccess, onFail, asByteArray) {
		onSuccess = onSuccess || Ext.emptyFn;
		onFail = onFail || Ext.emptyFn;
		Ext.Ajax.request({
			url: url,
			binary: asByteArray || false,
			callback: function(options, success, response) {
				if (success) onSuccess(asByteArray ? response.responseBytes : response.responseText);
				else onFail(response.status);
			}
		});
	},

	getSessionStorageItem: function(name, decode, protect) {
		try {
			var $sg = CFJS.euscp.$sg,
				item = window.sessionStorage.getItem(name);
			if (item == null || item === 'underfined') return null;
			if (protect) return $sg.UnprotectDataByPassword(item, null, !decode);
			else {
				if (!decode) return item;
				else return CFJS.euscp.Util.base64Decode(item);
			}
		} catch (e) {
			return null;
		}
	},
	
	getSessionStorageItems: function(name, decode, protect) {
		var me = CFJS.euscp.Util,
			length = me.getSessionStorageItem(name + 'Count', false, false),
			items = [], i = 0, item;
		if (length == null) return null;
		length = parseInt(length);
		for (; i < length; i++) {
			item = me.getSessionStorageItem(name + i.toString(), decode, protect);
			if (item == null) return false;
			items.push(item);
		}
		return items;
	},
	
	infoAlert: function(message, fn, scope, animateTarget) {
		Ext.fireEvent('infoalert', message, fn, scope, animateTarget);
	},

	isBase64: function(str) {
		return Ext.isString(str) && /^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/.test(str);
	},
	
	isFileImage: function (file) {
		return (/image/.test(file.type));
	},
	
	isFileSystemSupported: function () {
		window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		window.requestFileSystem(window.PERSISTENT, EU_PERSISTENT_FS_SIZE, initFunction, HandleErrorFileSystem);
	},

	isFolderExists: function (folderName) {
		return window.localStorage.getItem(folderName) != null;
	},

	loadFile: function(file, callback) {
		callback = callback || Ext.emptyFn;
		if (!Ext.isDefined(file)) { callback(false); return; }
		var fileReader = new FileReader();
		fileReader.onloadend = function (e) {
			if (e.target.readyState != FileReader.DONE) callback(false, file);
			file.data = new Uint8Array(e.target.result);
			callback(true, file);
		};
		fileReader.readAsArrayBuffer(file);
	},
	
	readFile: function (folderName, fileName) {
		var fileData = window.localStorage.getItem(folderName + "/" + fileName);
		if (fileData != null) {
			try { return CFJS.euscp.Util.base64Decode(fileData); }
			catch (e) {}
		}
		return null;
	},

	removeSessionStorageItem: function(name) {
		try { window.sessionStorage.removeItem(name); }
		catch (e) { return false; }
		return true;
	},
	
	removeSessionStorageItems: function(name) {
		var me = CFJS.euscp.Util,
			length = me.getSessionStorageItem(name + 'Count', false, false),
			i = 0;
		if (length == null) return true;
		length = parseInt(length);
		for (; i < length; i++) {
			me.removeSessionStorageItem(name + i.toString());
		}
		me.removeSessionStorageItem(name + 'Count');
		return true;
	},
	
	resourcePath: function(path, poolName) {
		return CFJS.localizedResource(path, poolName || 'shared', 'cf-euscp');
	},

	saveFile: function(fileName, array) {
		saveAs(new Blob([array], { type: 'application/octet-stream' }), fileName);
	},
	
	setSessionStorageItem: function(name, item, protect) {
		try {
			var $sg = CFJS.euscp.$sg;
			if (protect) window.sessionStorage.setItem(name, $sg.ProtectDataByPassword(item, null, true));
			else window.sessionStorage.setItem(name, CFJS.euscp.Util.base64Encode(item));
		} catch (e) { return false; }
		return true;
	},
	
	setSessionStorageItems: function(name, items, protect) {
		var me = CFJS.euscp.Util,
			length = items.length, 
			i = 0;
		if (!me.setSessionStorageItem(name + 'Count', length.toString(), protect)) return false;
		for (; i < length; i++)  {
			if (!me.setSessionStorageItem(name + i.toString(), items[i], protect)) {
				for (var j = 0; j < i; j++) {
					me.removeSessionStorageItem(name + j.toString());
				}
				me.removeSessionStorageItem(name + 'Count');
				return false;
			}
		}
		return true;
	},
	
	writeFile: function (folderName, fileName, data) {
		try {
			var me = CFJS.euscp.Util,
				dataStr = me.base64Encode(data);
			window.localStorage.setItem(folderName + "/" + fileName, dataStr);
			var filesArr = me.getFiles(folderName),
				index = filesArr.indexOf(fileName);
			if (index < 0) filesArr.push(fileName);
			window.localStorage.setItem(folderName, JSON.stringify(filesArr));
		} catch (e) {}
	}

}, function() {
	var utils = CFJS.euscp.Util,
		me = Ext.apply(CFJS.euscp, {

		caCertificatesSessionStorageName		: 'CACertificates',
		caServer								: null,
		caServerIndexSessionStorageName			: 'CAServerIndex',
		certsLocalStorageName					: 'Certificates',
		crlsLocalStorageName					: 'CRLs',
		httpProxyService						: '/caproxy',
		initialized								: false,
		loadPKCertsFromFile						: false,
		offline									: false,
		pkCertificatesChainSessionStorageName	: 'PrivateKeyCertificatesChain',
		pkCertificatesSessionStorageName		: 'PrivateKeyCertificates',
		pkNameSessionStorageName				: 'PrivateKeyName',
		pkPasswordSessionStorageName			: 'PrivateKeyPassword',
		pkSessionStorageName					: 'PrivateKey',
		privateKeyCerts							: null,
		useCMP									: false,
		
		initEUSignCp: function() {
			var onLoad = function(store, records, successful) {
					try {
						if (!window.EUSignCP) return;
						var $sg = me.$sg = EUSignCP();
						$sg.Initialize();
						$sg.SetJavaStringCompliant(true);
						$sg.SetCharset("UTF-16LE");
						if (!successful) return;
						if ($sg.DoesNeedSetSettings()) {
							me.setDefaultSettings();
							if (CFJS.hasStorage) me.loadCertsAndCRLsFromLocalStorage();
							else utils.errorAlert('Локальне сховище не підтримується!');
						}
						me.loadCertsFromServer();
						me.setCASettings(store.getAt(0).getData());
						if (CFJS.hasSessionStorage) setTimeout(function() { me.readPrivateKeyAsStoredFile(); }, 10);
						me.initialized = true;
					} catch (e) { utils.errorAlert(e); }
				};
			me.setStore({
				type: 'euscp.CAs',
				autoLoad: true,
				proxy		: {
					type	: 'ajax',
					url		: utils.resourcePath('data/CAs_{0}.json'),
					noCache	: false
				},
				listeners: { load: { fn: onLoad, single: true } } 
			});
		},
	
		bindStore: function(store, initial) {
	        var oldStore = initial ? null : me.store;
	        if (store !== oldStore) {
	            if (oldStore) {
	                if (oldStore.autoDestroy) oldStore.destroy();
	                else me.unbindStoreListeners(oldStore);
	            }
	            if (store) {
	                me.store = store = Ext.data.StoreManager.lookup(store);
	                me.bindStoreListeners(store);
	            } else me.store = null;
	        }
	        return me;
	    },
	
	    bindStoreListeners: function(store) {
	        var listeners = me.getStoreListeners(store);
	        if (listeners) {
	            listeners = Ext.apply({}, listeners);
	            if (!listeners.scope) listeners.scope = me;
	            me.storeListeners = listeners;
	            store.on(listeners);
	        }
	    },
	    
		clearPrivateKeyCertificatesList: function() {
			me.privateKeyCerts = null;
		},
		
		getCAServer: function() {
			var data = me.caServer;
			return data ? (data.isModel ? data.getData() : data) : null;
		},

		getPrivateKeyCertificates: function(key, password, fromCache, onSuccess, onError) {
			onSuccess = onSuccess || Ext.emptyFn;
			onError = onError || Ext.emptyFn;
			if (me.caServer && me.caServer.certsInKey) {
				onSuccess([]);
				return;
			}
			if (fromCache) {
				var certificates;
				if (me.useCMP) certificates = utils.getSessionStorageItem(me.pkCertificatesChainSessionStorageName, true, false);
				else if (me.loadPKCertsFromFile) certificates = utils.getSessionStorageItems(me.pkCertificatesSessionStorageName, true, false);
				onSuccess(certificates);
			} else if (me.useCMP) {
				me.getPrivateKeyCertificatesByCMP(key, password, onSuccess, onError);
			} else if (me.loadPKCertsFromFile) {
				me.$sg.ReadFiles(me.privateKeyCerts, function(files) {
					var certificates = [], i = 0;
					for (; i < files.length; i++) {
						certificates.push(files[i].data);
					}
					onSuccess(certificates);
				}, onError);
			}
		},
		
		getPrivateKeyCertificatesByCMP: function(key, password, onSuccess, onError) {
			try {
				var cmpAddress = me.getCAServer().cmpAddress + ":80", 
					keyInfo = me.$sg.GetKeyInfoBinary(key, password);
				onSuccess(me.$sg.GetCertificatesByKeyInfo(keyInfo, [cmpAddress]));
			} catch (e) { onError(e); }
		},

		getStore: function () {
	        return me.store;
	    },
		
	    getStoreListeners: Ext.emptyFn,
	    
		loadCAServer: function() {
			var index = utils.getSessionStorageItem(me.caServerIndexSessionStorageName, false, false);
			me.setCASettings(me.store.getAt(CFJS.safeParseInt(index)));
		},

		loadCertsAndCRLsFromLocalStorage: function() {
			try {
					files = me.loadFilesFromLocalStorage(me.certsLocalStorageName, function(fileName, fileData) {
						if (fileName.indexOf(".cer") >= 0) me.$sg.SaveCertificate(fileData);
						else if (fileName.indexOf(".p7b") >= 0)	me.$sg.SaveCertificates(fileData);
					});
				if (files != null && files.length > 0) me.setItemsToList('SelectedCertsList', files);
				else console.info('Сертифікати відсутні в локальному сховищі');
			} catch (e) { utils.errorAlert('Виникла помилка при завантаженні сертифікатів з локального сховища'); }
			try {
				var files = me.loadFilesFromLocalStorage(me.crlsLocalStorageName, function(fileName, fileData) {
						if (fileName.indexOf(".crl") >= 0) {
							try { me.$sg.SaveCRL(true, fileData); }
							catch (e) { me.$sg.SaveCRL(false, fileData); }
						}
					});
				if (files != null && files.length > 0) me.setItemsToList('SelectedCRLsList', files);
				else console.info('СВС відсутні в локальному сховищі');
			} catch (e) { utils.errorAlert('Виникла помилка при завантаженні СВС з локального сховища'); }
		},
	    
		loadCertsFromServer: function() {
			var certificates = utils.getSessionStorageItem(me.caCertificatesSessionStorageName, true, false),
				onSuccess = function(certificates) {
					try {
						me.$sg.SaveCertificates(certificates);
						utils.setSessionStorageItem(me.caCertificatesSessionStorageName, certificates, false);
					} catch (e) { utils.errorAlert('Виникла помилка при імпорті завантажених з сервера сертифікатів до файлового сховища: ' + e); }
				},
				onFail = function(errorCode) {
					utils.errorAlert('Виникла помилка при завантаженні сертифікатів з сервера. (HTTP статус ' + errorCode + ')');
				};
			if (certificates != null) {
				try {
					me.$sg.SaveCertificates(certificates);
					return;
				} catch (e) { utils.errorAlert('Виникла помилка при імпорті завантажених з сервера сертифікатів до файлового сховища: ' + e); }
			}
			utils.getDataFromServerAsync(utils.resourcePath('data/CACertificates.p7b?version=1.0.8'), onSuccess, onFail, true);
		},
		
		loadFilesFromLocalStorage: function(localStorageFolder, loadFunc) {
			if (!CFJS.hasStorage) me.$sg.RaiseError(EU_ERROR_NOT_SUPPORTED);
			if (utils.isFolderExists(localStorageFolder)) {
				var files = utils.getFiles(localStorageFolder), i = 0, file;
				for (; i < files.length; i++) {
					file = utils.readFile(localStorageFolder, files[i]);
					loadFunc(files[i], file);
				}
				return files;
			} else {
				utils.createFolder(localStorageFolder);
				return null;
			}
		},

		loadPrivateKey: function(options) {
	    	options = options || {};
	    	var keyFile = options.keyFile,
	    		pwd = options.password,
	    		certificatesFiles = me.privateKeyCerts,
	    		msg = 'Виникла помилка при зчитуванні особистого ключа: ',
	    		callback = options.callback || Ext.emptyFn,
	    		failure = function(e) { utils.errorAlert(e, callback); },
	    		readPrivateKey = function(success, options) {
					options = options || {};
					if (success !== true) failure(options);
					else me.readPrivateKey(options.keyName, new Uint8Array(options.key), pwd, null, false, callback);
	    		},
	    		onReadFile = function(readedFile) {
	    			readPrivateKey(true, { keyName: readedFile.file.name, key: readedFile.data });
	    		};
	    	try {
		    	if (Ext.isEmpty(keyFile))
		    		throw msg + 'не обрано жодного файлу ключа.';
				if (me.loadPKCertsFromFile && (certificatesFiles == null || certificatesFiles.length <= 0)) 
					throw msg + 'не обрано жодного сертифіката відкритого ключа.';
				if (utils.isFileImage(keyFile)) me.readPrivateKeyAsImage(keyFile, readPrivateKey);
				else me.$sg.ReadFile(keyFile, onReadFile, failure);
			} catch (e) { failure(e); }
		},
		
		readPrivateKey: function(keyName, key, password, certificates, fromCache, callback) {
			var onError = function(e) {
					if (fromCache) me.removeStoredPrivateKey();
					else utils.errorAlert(e);
					if (e.GetErrorCode != null && e.GetErrorCode() == EU_ERROR_CERT_NOT_FOUND) {
//						me.mainMenuItemClicked(document.getElementById('MainPageMenuCertsAndCRLs'), 'MainPageMenuCertsAndCRLsPage');
					}
					callback(false, e);
				};
			callback = callback || Ext.emptyFn;
			if (certificates == null) {
				me.getPrivateKeyCertificates(key, password, fromCache, function(certs) {
					if (certs == null) {
						onError(me.$sg.MakeError(EU_ERROR_CERT_NOT_FOUND));
						return;
					}
					me.readPrivateKey(keyName, key, password, certs, fromCache, callback);
				}, onError);
				return;
			}
			try {
				if (Ext.isArray(certificates)) {
					for (var i = 0; i < certificates.length; i++) {
						me.$sg.SaveCertificate(certificates[i]);
					}
				} else me.$sg.SaveCertificates(certificates);
				me.$sg.ReadPrivateKeyBinary(key, password);
				if (!fromCache && CFJS.hasSessionStorage) {
					if (!me.storePrivateKey(keyName, key, password, certificates)) {
						me.removeStoredPrivateKey();
					}
				}
				var pkOwnerInfo = me.$sg.GetPrivateKeyOwnerInfo();
				callback(true, {
					pkOwnerName	: pkOwnerInfo.GetSubjCN(),
					pkOrgName	: pkOwnerInfo.GetSubjOrg(),
					pkIssuerCN	: pkOwnerInfo.GetIssuerCN(),
					pkSerial	: pkOwnerInfo.GetSerial()
				});
			} catch (e) { onError(e); }
		},
		
		readPrivateKeyAsImage: function(file, callback) {
			var image = new Image();
				callback = callback || Ext.emptyFn;
				image.onload=function() {
					try {
						var qr = new QRCodeDecode(),
							canvas = document.createElement('canvas'),
							context = canvas.getContext('2d'),
							imagedata, decoded;
						canvas.width = image.width;
						canvas.height = image.height;
						context.drawImage(image, 0, 0, canvas.width, canvas.height);
						imagedata = context.getImageData(0, 0, canvas.width, canvas.height);
						decoded = qr.decodeImageData(imagedata, canvas.width, canvas.height);
						callback(true, { keyName: file.name, key: StringToArray(decoded) });
					} catch (e) { callback(false, e); }
				}
			image.src = CFJS.createObjectURL(file);
		},

		readPrivateKeyAsStoredFile: function(callback) {
			var keyName = utils.getSessionStorageItem(me.pkNameSessionStorageName, false, false),
				key = utils.getSessionStorageItem(me.pkSessionStorageName, true, false),
				password = utils.getSessionStorageItem(me.pkPasswordSessionStorageName, false, true);
			if (keyName == null || key == null || password == null) return;
			me.loadCAServer();
			setTimeout(function() { me.readPrivateKey(keyName, key, password, null, true, callback); }, 10);
			return;
		},
		
		removeCAServer: function() {
			utils.removeSessionStorageItem(me.caServerIndexSessionStorageName);
		},

		removeStoredPrivateKey: function() {
			utils.removeSessionStorageItem(me.pkNameSessionStorageName);
			utils.removeSessionStorageItem(me.pkSessionStorageName);
			utils.removeSessionStorageItem(me.pkPasswordSessionStorageName);
			utils.removeSessionStorageItem(me.pkCertificatesChainSessionStorageName);
			utils.removeSessionStorageItem(me.pkCertificatesSessionStorageName);
			me.removeCAServer();
		},
		
		setCASettings: function(record) {
			try {
				record = record || me.store.getAt(0).getData();
				var $sg = me.$sg,
					caServer = record ? (record.isModel ? record.getData() : record) : null,
					offline = !(caServer && !Ext.isEmpty(caServer.address)),
					useCMP = !offline && !Ext.isEmpty(caServer.cmpAddress),
					loadPKCertsFromFile = !caServer || (!useCMP && !caServer.certsInKey),
					settings;

				if (me.caServer === caServer) return;
				
				me.caServer = caServer;
				me.offline = offline;
				me.useCMP = useCMP;
				me.loadPKCertsFromFile = loadPKCertsFromFile;
				me.clearPrivateKeyCertificatesList();
				
				// TSP settings
				settings = $sg.CreateTSPSettings();
				if (!offline) {
					settings.SetGetStamps(true);
					if (!Ext.isEmpty(caServer.tspAddress)) {
						settings.SetAddress(caServer.tspAddress);
						settings.SetPort(caServer.tspAddressPort);
					} else {
						settings.SetAddress('acskidd.gov.ua');
						settings.SetPort('80');
					}
				}
				$sg.SetTSPSettings(settings);
				
				// OCSP settings
				settings = $sg.CreateOCSPSettings();
				if (!offline) {
					settings.SetUseOCSP(true);
					settings.SetBeforeStore(true);
					settings.SetAddress(caServer.ocspAccessPointAddress);
					settings.SetPort(caServer.ocspAccessPointPort);
				}
				$sg.SetOCSPSettings(settings);
				
				// CMP settings
				settings = $sg.CreateCMPSettings();
				settings.SetUseCMP(useCMP);
				if (useCMP) {
					settings.SetAddress(caServer.cmpAddress);
					settings.SetPort("80");
				}
				$sg.SetCMPSettings(settings);
				
				// LDAP settings
				settings = $sg.CreateLDAPSettings();
				$sg.SetLDAPSettings(settings);
			} catch (e) { utils.errorAlert("Виникла помилка при встановленні налашувань: " + e); }
		},

	    setDefaultSettings: function() {
	    	var $sg = me.$sg;
	    	try {
				$sg.SetXMLHTTPProxyService(me.httpProxyService);

				var settings = $sg.CreateFileStoreSettings();
				settings.SetPath('/certificates');
				settings.SetSaveLoadedCerts(true);
				$sg.SetFileStoreSettings(settings);

				settings = $sg.CreateProxySettings();
				$sg.SetProxySettings(settings);
				
				settings = $sg.CreateTSPSettings();
				$sg.SetTSPSettings(settings);

				settings = $sg.CreateOCSPSettings();
				$sg.SetOCSPSettings(settings);

				settings = $sg.CreateCMPSettings();
				$sg.SetCMPSettings(settings);

				settings = $sg.CreateLDAPSettings();
				$sg.SetLDAPSettings(settings);
				
				settings = $sg.CreateOCSPAccessInfoModeSettings();
				settings.SetEnabled(true);
				$sg.SetOCSPAccessInfoModeSettings(settings);

				settings = $sg.CreateOCSPAccessInfoSettings();
				me.store.each(function(record) {
					settings.SetAddress(record.get('ocspAccessPointAddress'));
					settings.SetPort(record.get('ocspAccessPointPort'));
					var issuerCNs = record.get('issuerCNs'), i = 0;
					for (; i < issuerCNs.length; i++) {
						settings.SetIssuerCN(issuerCNs[i]);
						$sg.SetOCSPAccessInfoSettings(settings);
					}
				});
			} catch (e) { utils.errorAlert('Виникла помилка при встановленні налашувань: ' + e); }
	    },
	    
	    setItemsToList: function(listId, items) {
	    	console.log(listId, items);
	    },
	    
		setStore: function (store) {
	        me.bindStore(store);
	    },
	    
	    signFile: function(options) {
	    	options = options || {};
	    	var file = options.file,
	    		data = file.data,
	    		msg = 'Виникла помилка при підпису файлу: ',
	    		callback = options.callback || Ext.emptyFn,
	    		failure = function(e) { utils.errorAlert(e, callback); },
	    		internalSign = function(data, fileName) {
	    			var $sg = me.$sg,
	    				alg = options.alg || 'DSTU4145',
		    			appendCert = options.appendCert === true,
		    			external = options.external === true,
		    			saveToFile = options.saveToFile !== false,
		    			sign;
			    	if (alg.toUpperCase().startsWith() === 'RSA') {
			    		sign = $sg.SignDataRSA(data, appendCert, external, false);
			    	} else {
			    		if (external) sign = $sg.SignData(data, false);
			    		else sign = $sg.SignDataInternal(appendCert, data, false);
			    	}
			    	if (saveToFile) utils.saveFile(fileName + '.p7s', sign);
					callback(true, sign);
	    		};
	    	try {
		    	if (Ext.isEmpty(file))
		    		throw msg + 'файл не обрано.';
		    	if (file.size > Module.MAX_DATA_SIZE)
		    		throw msg + 'розмір файлу для підпису занадто великий. Оберіть файл меньшого розміру.';
		    	
		    	if (Ext.isEmpty(data) || data == 0) {
		    		utils.loadFile(file, function(success, file) {
	    				try {
			    			if (!success) throw msg + 'не можливо прочитати файл ' + file.name + '.';
			    			else internalSign(file.data, file.name);
						} catch (e) { failure(e); }
		    		});
		    	} else internalSign(data, file.name);
			} catch (e) { failure(e); }
	    },
	    
		storeCAServer: function() {
			var index = me.store.indexOfId((me.caServer||{}).id);
			return utils.setSessionStorageItem(me.caServerIndexSessionStorageName, index > 0 ? index : 0, false);
		},

		storePrivateKey: function(keyName, key, password, certificates) {
			if (!utils.setSessionStorageItem(me.pkNameSessionStorageName, keyName, false) ||
				!utils.setSessionStorageItem(me.pkSessionStorageName, key, false) ||
				!utils.setSessionStorageItem(me.pkPasswordSessionStorageName, password, true) ||
				!me.storeCAServer()) {
				return false;
			}
			if (Ext.isArray(certificates)) {
				if (!utils.setSessionStorageItems(me.pkCertificatesSessionStorageName, certificates, false)) return false;
			} else {
				if (!utils.setSessionStorageItem(me.pkCertificatesChainSessionStorageName, certificates, false)) return false;
			}
			return true;
		},
		
		verifyData: function(fileName, data, signs) {
			var $sg = me.$sg,
				info, ownerInfo, timeInfo;
			data = utils.base64Encode(data);
			if (Ext.isArray(signs) && signs.length > 0) {
				var i = 0, sign, ex
				for (; i < signs.length && !info; i++) {
					try {
						info = $sg.VerifyData(data, utils.base64Encode((sign = signs[i]||{}).data ? sign.data : sign));
						signs.splice(i, 1);
					} catch (e) { ex = e }
				}
				if (!info) throw ex;
			} else info = $sg.VerifyDataInternal(data);
			ownerInfo = info.GetOwnerInfo();
			timeInfo = info.GetTimeInfo();
			data = info.GetData();
			if (!Ext.isEmpty(fileName)) {
				fileName = fileName.split('.');
				if (fileName.length > 1) fileName.pop();
				fileName = fileName.join('.');
			}
			info = {
				data: new Blob([data], { type: 'application/octet-stream' }),
				isTimeStamp: timeInfo.IsTimeStamp(),
				sgFileName: fileName,
				sgSignerName: ownerInfo.GetSubjCN(),
				sgOrgName: ownerInfo.GetSubjOrg(),
				sgIssuerCN: ownerInfo.GetIssuerCN(),
				sgSerial: ownerInfo.GetSerial(),
				sgTime: timeInfo.IsTimeAvail() ? timeInfo.GetTime() : null
			}
			try {
				if ($sg.IsDataInSignedDataAvailable(data)) return [info].concat(me.verifyData(fileName, data, signs));
			} catch (e) {}
			return info;
		},
		
		verifyFile: function(options) {
	    	options = options || {};
	    	var $sg = me.$sg,
	    		file = options.file,
	    		callback = options.callback || Ext.emptyFn,
	    		msg = 'Виникла помилка при перевірці підпису файлу: ',
	    		failure = function(e) { utils.errorAlert(e, callback); }, info;
	    	try {
		    	if (Ext.isEmpty(file))
		    		throw msg + 'файл не обрано.';
		    	if (file.size > Module.MAX_DATA_SIZE + EU_MAX_P7S_CONTAINER_SIZE)
		    		throw msg + 'розмір файлу занадто великий. Оберіть файл меньшого розміру.';
		    	if (Ext.isEmpty(file.data) || file.data == 0) {
		    		utils.loadFile(file, function(success, file) {
	    				try {
			    			if (!success) throw msg + 'не можливо прочитати файл ' + file.name + '.';
			    			else me.verifyFile(options);
						} catch (e) { failure(e); }
		    		});
		    		return;
		    	}
		    	if (options.signs) {
		    		var signs = Ext.isArray(options.signs) ? options.signs : [options.signs], i;
			    	for (i = 0; i < signs.length; i++ ) {
				    	if (signs[i].size > Module.MAX_DATA_SIZE)
				    		throw msg + 'розмір файлу підпису занадто великий. Оберіть файл меньшого розміру.';
			    	}
			    	for (i = 0; i < signs.length; i++) {
			    		if (Ext.isEmpty(signs[i].data) || signs[i].data == 0) {
				    		utils.loadFile(signs[i], function(success, file) {
			    				try {
					    			if (!success) throw msg + 'не можливо прочитати файл ' + signs[i].name + '.';
					    			else me.verifyFile(options);
								} catch (e) { failure(e); }
				    		});
				    		return;
			    		}
			    	}
		    	}
		    	info = me.verifyData(file.name, file.data, signs);
		    	callback(!Ext.isEmpty(info), info);
			} catch (e) { failure(e); }
		},
		
		unbindStoreListeners: function(store) {
	        var listeners = me.storeListeners;
	        if (listeners) store.un(listeners);
	    }

	});

	var fn = CFJS.Api.updateConfig;
	CFJS.Api.updateConfig = function() {
		fn.apply(this, arguments);
		var controller = this.controller('proxy');
		if (!Ext.isEmpty(controller) && !Ext.isEmpty(controller.url)) {
			me.httpProxyService = controller.url;
	    	try {
	    		if (me.$sg && me.initialized) me.$sg.SetXMLHTTPProxyService(me.httpProxyService);
			} catch (e) { utils.errorAlert('Виникла помилка при встановленні налашувань: ' + e); }
		}
	};

});
Ext.onReady(CFJS.euscp.initEUSignCp);
