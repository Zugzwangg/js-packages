Ext.define('CFJS.euscp.model.CACertificate', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'issuerCNs',				type: 'auto' }, 
		{ name: 'address',					type: 'string' }, 
		{ name: 'ocspAccessPointAddress',	type: 'string' }, 
		{ name: 'ocspAccessPointPort', 		type: 'string' },
		{ name: 'cmpAddress', 				type: 'string' },
		{ name: 'tspAddress', 				type: 'string' },
		{ name: 'tspAddressPort', 			type: 'string' },
		{ name: 'name', 					type: 'string', 
			calculate: function (data) { 
				return data.issuerCNs && data.issuerCNs.length > 0 ? data.issuerCNs[0] : null;
			}
		}
	]
});