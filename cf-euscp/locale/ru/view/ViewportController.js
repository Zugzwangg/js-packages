Ext.define('CFJS.locale.ru.view.ViewportController', {
	override		: 'CFJS.view.ViewportController',
	pkPickerConfig	: {
		buttons	: [{ text: 'Подписать' }],
		title	: 'Чтение секретного ключа'
	}
});