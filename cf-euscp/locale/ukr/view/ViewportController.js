Ext.define('CFJS.locale.ukr.view.ViewportController', {
	override		: 'CFJS.view.ViewportController',
	pkPickerConfig: {
		buttons	: [{ text: 'Підписати' }],
		title	: 'Читання секретного ключа'
	}
});