Ext.define('CFJS.plugins.service.view.ServiceMenuController', {
	extend		: 'CFJS.plugins.service.view.EditController',
	alias		: 'controller.plugins.service.servicemenu',
	requires	: [
	],

	onFinish: function(button) {
		// finish
	},

	onPrevious: function(button) {
		var me = this, view = me.getView(), vm = view.lookupViewModel(),
			steps = vm.get('steps'), currentStep = vm.get('currentStep'), activeItem, saveItem;
		vm.set('currentStep', --currentStep);
		--currentStep;
		vm.set('finish', false);
		if (currentStep === 0) {
			saveItem = view.items.getAt(0);
			view.items.removeAll();
			view.items.add(saveItem);
		}
		vm.set('progress', 1 / steps * currentStep);
		activeItem = view.items.getAt(currentStep);
		view.getLayout().setActiveItem(activeItem);
	},

	onNext : function(button) {
		var me = this, view = me.getView(), vm = view.lookupViewModel(),
			steps = vm.get('steps'), currentStep = vm.get('currentStep'), activeItem, evm;
		activeItem = view.items.getAt(currentStep);
		
		if (currentStep === steps) {
			vm.set('finish', true);
		} 

		vm.set('progress', 1 / steps * currentStep);
		vm.set('currentStep', currentStep + 1);
		view.getLayout().setActiveItem(activeItem);
	},

	onClick : function(btn) {
		var me = this, mainView = me.getView(), vm = mainView.lookupViewModel(), urlItem, items, evm; 
		switch (btn.reference) {
			case 'integratedInsurance':
				break;
			case 'medicine':
				break;
			case 'accident':
				break;
			case 'go':
				urlItem = 'resources/go.json';
				break;
			case 'financialRisks':
				break;
			case 'luggageInsurance':
				break;
			case 'other':
				break;
			default:
				break;
		}
		
		items = Ext.create('Ext.data.Store', {
			autoLoad: true,
			proxy: {
				type: 'ajax',
				url : urlItem,
				reader: {
					type: 'json'
				}
			}
		});
		
		items.load(function(records, operation, success) {
			
			for (i = 0; i < records.length; i++) {
				card = mainView.add({
					xtype: records[i].get('xtype')
				});
			}
			
			vm.set('steps', mainView.items.length-1);
			vm.set('currentStep', 1);
			me.onNext(btn);
		});
		
	}
	
});