Ext.define('CFJS.plugins.service.view.ServiceMenuViewModel', {
	extend	: 'CFJS.plugins.service.view.ServiceModel',
	alias	: 'viewmodel.plugins.service.servicemenu',
	
	data	: {
		steps		: 0,
		currentStep	: 0,
		progress	: 0,
		finish		: false,
		serviceArr  : [],
		currService : null
	}
});