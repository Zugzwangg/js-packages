Ext.define('CFJS.plugins.service.view.ServiceModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.plugins.service.edit',
	requires	: [
		'CFJS.store.Named',
		'CFJS.store.dictionary.Cities',
		'CFJS.store.dictionary.Countries',
		'CFJS.store.dictionary.Currencies',
		'CFJS.store.hotel.Hotels',
		'CFJS.model.airline.AirlineTicket',
		'CFJS.model.bus.BusTicket',
		'CFJS.model.dictionary.Consumer',
		'CFJS.model.financial.TaxAmount',
		'CFJS.model.hotel.HotelService',
		'CFJS.model.insurance.InsuranceService',
		'CFJS.model.railway.RailwayTicket',
		'CFJS.model.travel.TravelService'
	],
	itemName	: undefined,
	itemModel	: 'Service',
	data		: { supplierTitle: 'Поставщик' },
	stores		: {
		taxes: {
			data	: '{_itemName_.taxes}',
			model	: 'CFJS.model.financial.TaxAmount'
		}
	},
	
	setCityFilter: function(combo, city) {
		var detail = combo.ownerCt.down(combo.detailField),
			store = detail ? detail.getStore() : null,
			filter = combo.cityFilter;
		if (store) {
			combo.cityFilter = null;
			if (filter) store.removeFilter(filter, true);
			if (city) {
	            filter = combo.cityFilter = new Ext.util.Filter({
	                id			: detail.id + '-city-filter',
					type		: 'numeric',
					operator	: 'eq',
					property	: 'city',
					updatable	: true,
	                value		: city.id ? city.id : city
	            });
				store.addFilter(filter, true);
			}
		}
	},
	
	setCountryFilter: function(combo, country) {
		var detail = combo.ownerCt.down(combo.detailField),
			store = detail ? detail.getStore() : null,
			filter = combo.countryFilter;
		if (store) {
			combo.countryFilter = null;
			if (filter) store.removeFilter(filter, true);
			if (country) {
	            filter = combo.countryFilter = new Ext.util.Filter({
	                id			: detail.id + '-country-filter',
					type		: 'numeric',
					operator	: 'eq',
					property	: 'country',
					updatable	: true,
	                value		: country.id ? country.id : country
	            });
				store.addFilter(filter, true);
			}
		}
	},
	
	setItem: function(item) {
		this.linkTo(this.getItemName(), this.configItem(item));
	},

	privates: {
		
		configItem: function(item) {
			if (item && item.isModel) {
				var types = Ext.getStore('serviceTypes');
				return { id: item.get('id'), type: types.findModel(item.get('serviceType')) };
			}
			var me = this,
				user = me.get('user'),
				order = me.get('editOrder'),
				serviceType = me.get('serviceType'),
				number = CFJS.generateUUID(serviceType.id + '-xxxxxxxx').toUpperCase();
			return {
				type	: serviceType.get('model'),
				create	: Ext.apply({
					post 		: user && user.isPerson ? user.get('post') : null,
//					author 		: user && user.isPerson ? user.get('post') : null,
					order		: order ? order.get('id') : null,
					number		: number,
					extNumber	: number,
					serviceType	: serviceType.id,
					showVat		: serviceType.get('showVat'),
					updatable	: true,
					validUntil	: new Date()
				}, item || {})
			};
		},

		setEntityModel: function(field, record, store) {
			var me = this, item = me.getItem();
			if (Ext.isString(store)) store = me.getStore(store);
			record = store.findRecord('id', record);
			item.set(field, record ? record.data : record);
		}	
		
	}

});