Ext.define('CFJS.plugins.service.view.InsuranceModel', {
	extend		: 'CFJS.plugins.service.view.ServiceModel',
	alias		: 'viewmodel.plugins.service.insurance',
	id			: 'insurance',
	requires	: [ 'CFJS.store.insurance.InsurancePrograms' ],
	data		: {
		supplierTitle: 'Страховщик'
	},
	stores		: {
		insurancePrograms: {
			type	: 'insurance_programs',
			autoLoad: true
		}
	},
	
	constructor: function (config) {
		var me = this;
		me.callParent(arguments);
		me.bind('{_itemName_.program}', me.switchProgram, me);
	},
	
	switchProgram: function(program) {
		var me = this, view = me.getView(),
			controller = view.lookupController(),
			editor = view.switchProgram(program),
			child = editor ? editor.getViewModel() : null;
		if (child && child.isInsuranceModel) {
			child.bind('{program}', function() { child.setItem(me.getItem()); }, child, { single: true });
			child.initInternalListeners(controller);
		}
	},
	
	privates: {
		
		configItem: function(item) {
			var rec = this.callParent(arguments)
				dateFrom = Ext.Date.add(new Date(), Ext.Date.SECOND, 1);
			Ext.apply(rec.created, {
				dateFrom: dateFrom,
				dateTo	: Ext.Date.add(dateFrom, Ext.Date.YEAR, 1)
			});
			return rec; 
		}
	
	}

});
