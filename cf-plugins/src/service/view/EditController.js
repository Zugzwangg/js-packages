Ext.define('CFJS.plugins.service.view.EditController', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.plugins.service.edit',
	id			: 'service-edit',
	editor		: 'panel-base-editor[name=serviceEditor]',
	documentsUrl: 'documents/form-',
	listeners	: { beforesaverecord: 'onBeforeSaveRecord', beforeremoverecord: 'onBeforeRemoveRecord' },
	messages	: {
		confirmAction		: 'Confirm action',
		documentsRedirect	: 'Go to documents?',
		documentSign		: 'Sign the document?',
	},

	initViewModel: function(vm) {
		if (vm.isMaster) {
			vm.bind('{purchaseHistoryFilter}', this.setPurchaseHistoryFilter, this, { deep: true });
		} 
		if (vm.isEditorModel && vm.hasOwnProperty('item')) {
			vm.setItem(vm.item);
			delete vm.item;
		}
	},

	beforeLoadSummary: function(store, operation) {
		var filters = operation.getFilters() || [];
		filters.push(new Ext.util.Filter({ property: 'state', type: 'string', operator: '!eq', value: 'REFUND' }));
		operation.setFilters(filters);
	},

	cloneRecordData: function(record, linkParent) {
		if (record) {
			record = Ext.clone(record.getData({ persist: true, serialize: true }));
			if (linkParent) record.parent = record.id;
			delete record.id;
			delete record.number;
			record.updatable = true;
		}
		return record;
	},

	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.addVoid, !selection || selection.length !== 1);
		selectable.disableComponent(actions.copyRecord, !selection || selection.length !== 1);
		selectable.disableComponent(actions.printRecord, !selection || selection.length !== 1);
		this.callParent(arguments);
	},
	
	getEditorType: function(serviceType) {
		switch ((serviceType || 'OTHER').toUpperCase()) {
		case 'AIR': return 'plugins.service.airline.editor';
		case 'BUS': return 'plugins.service.bus.editor';
		case 'HOTEL': return 'plugins.service.hotel.editor';
		case 'INSURANCE': return 'plugins.service.insurance.editor';
		case 'RAILWAY': return 'plugins.service.railway.editor';
		case 'TRAVEL': return 'plugins.service.travel.editor';
		case 'OTHER':
		default: return 'plugins.service.editor';
		}
	},
	
	getSegmentsStore: function() {
		var panel = this.lookupSegments();
		return panel ? panel.getStore() : null;
	},

	getTaxesStore: function() {
		var panel = this.lookupTaxes();
		return panel ? panel.getStore() : null;
	},
	
	lookupSegments: function() {
		return this.getView().down('grid-base-panel[name=segments]');
	},

	lookupTaxes: function() {
		return this.getView().down('taxamountgrid[name=taxes]');
	},
	
	maybeEditRecord: function(record, addNew) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			editor = me.getEditor() || '',
			activeItem;
		if (grid && editor) {
			if (view.rendered) Ext.suspendLayouts();
			if (record !== false && !grid.readOnly) {
				var types = Ext.getStore('serviceTypes'),
					viewModel = me.getViewModel(),
					selection = grid.getSelection(), serviceType, evm;
				if (activeItem = view.down(editor)) view.remove(activeItem, true);
				if (!record && !addNew && selection && selection.length > 0) record = selection[0];
				viewModel.set('serviceType', serviceType = types.getById(addNew ? record : record.get('serviceType')));
				activeItem = view.add({ xtype: me.getEditorType(serviceType.id) });
				evm = activeItem.getViewModel(); 
				evm.bind('{' + evm.getItemName() + '}', me.startEditRecord, evm, { single: true });
				evm.setItem(record);
			} else activeItem = grid;
			view.getLayout().setActiveItem(activeItem);
			if (view.rendered) Ext.resumeLayouts(true);	
		}
	},
	
	onAddRecord: function(component) {
		this.maybeEditRecord(component ? component.serviceCode || 'OTHER' : 'OTHER', true);
	},
	
	onAddSegment: function(component) {
		var me = this, editor = me.getEditorComponent(), panel, rowEditing, record;	
		if (editor && (panel = me.lookupSegments())) {
			rowEditing = panel.findPlugin('rowediting'); 
			if (rowEditing) rowEditing.cancelEdit();
			record = editor.getViewModel().addSegment();
			if (rowEditing) rowEditing.startEdit(record, 1);
			else panel.getView().focusNode(record); 
		}
	},
	
	onAddTax: function(component) {
		var me = this, service = me.lookupRecord(), panel, rowEditing, record;
		if (service && (panel = me.lookupTaxes())) {
			rowEditing = panel.findPlugin('rowediting'); 
			if (rowEditing) rowEditing.cancelEdit();
			record = panel.getStore().add({ parent: service.id, created: new Date() })[0];
			if (rowEditing) rowEditing.startEdit(record, 1);
			else panel.getView().focusNode(record); 
		}
	},

	onAddVoid: function(component) {
		var me = this, record;
		me.startEditRecord = function(record) {
			var data = me.cloneRecordData(record, true);
			data.number = record.get('number') + '-R';
			data.state = 'REFUND';
			me.startEditRecord = Ext.emptyFn;
			this.setItem(data);
		};
		me.maybeEditRecord();
	},

	onBackClick: function(component) {
		var me = this, record = me.lookupRecord();
		if (record && record.isModel && record.get('updatable')) me.callParent(arguments);
		else me.maybeEditRecord(false);
	},

	onBeforeRemoveRecord: Ext.emptyFn,
	
	onBeforeSaveRecord: function(service) {
		var me = this, consumer, route, segments, len;
		if (service.isModified('consumer') && (consumer = service.get('consumer'))) {
			delete consumer.id;
			consumer.properties = {};
		}
		me.onCommitTaxes(service);
		me.onCommitSegments(service);
		route = service.get('route');
		segments = route ? route.segments : null;
		if (Ext.isArray(segments)) {
			if ((len = segments.length) <= 0) {
				CFJS.errorAlert('Ошибка', 'Вы не заполнили блок сегментов!');
				return false;
			}
		}
	},
	
	onCommitSegments: function(editor, context) {
		var me = this, 
			beforeSave = editor && editor.isModel,
			service = beforeSave ? editor : me.lookupRecord(),
			store = !beforeSave ? context.store : me.getSegmentsStore(), 
			segments = [], len;
		if (service && store && (beforeSave || store.isDirty())) {
			var route = service.get('route');
			store.commitChanges();
			store.each(function(record) {
				segments.push(record.getData({ service: beforeSave ? service : undefined }));
			});
			route = { id: route.id, segments: segments, created: route.created, updated: route.updated }
			if ((len = segments.length) > 0) route = Ext.apply(route, {
				departure	: segments[0].departure,
				from		: segments[0].from,
				arrive		: segments[len - 1].arrive,
				to			: segments[len - 1].to
			});
			service.set('route', route, { convert: false, silent: beforeSave });
		}
	},
	
	onCommitTaxes: function(editor, context) {
		var me = this,
			beforeSave = editor && editor.isModel,
			service = beforeSave ? editor : me.lookupRecord(),
			store = !beforeSave ? context.store : me.getTaxesStore(), 
			taxes = [];
		if (service && store && store.isDirty()) {
			store.commitChanges();
			store.each(function(record) {
				taxes.push(record.getData({ persist: beforeSave, serialize: true }));
			});
			service.set('taxes', taxes);
		}
	},
	
	onCopyRecord: function(component) {
		var me = this;
		me.startEditRecord = function(record) {
			me.startEditRecord = Ext.emptyFn;
			this.setItem(me.cloneRecordData(record));
		};
		me.maybeEditRecord();
	},
	
	onEditServices: function(component) {
		var editor = this.getEditorComponent();		
		Ext.create({
			xtype		: 'taxeseditor',
			autoShow	: true,
			service		: editor ? editor.getViewModel().getItem() : null,
			listeners	: {
				close: function(window) {
					if (editor) editor.updateTaxes(window.service);
				}
			}
		});
	},
	
	onLoadSummary: function(store, records, successful, operation) {
		if (successful) this.lookupGrid().updateSummaries(records);
	},
	
	onPrintRecord: function(component) {
		var me = this, 
			grid = me.lookupGrid(),
			selection = grid && grid.getSelection(),
			record = selection && selection.length > 0 && selection[0];
		if (record && record.isService) record.print(false);
	},
	
	onRemoveSegments: function(component) {
		var segments = this.lookupSegments(), selection;
		if (segments && (selection = segments.getSelection())) {
			segments.getStore().remove(selection);
		}
	},
	
	onRemoveTaxes: function(component) {
		var taxes = this.lookupTaxes(), selection;
		if (taxes && (selection = taxes.getSelection())) {
			taxes.getStore().remove(selection);
		}
	},
	
	onSummaryRefresh: function(component) {
		var store = this.lookupGridStore();
		if (store) store.summary();
	},
	
	onWriteOut: function(component) {
		var me = this, vm = me.getViewModel(), msg = me.messages, service = me.lookupRecord(), 
			callback = function(record, operation, success) {
				var form = success && record && record.isDocument && record.get('form');
				me.disableComponent(component);
				if (form && form.id > 0) {
					Ext.Msg.confirm(msg.confirmAction, msg.documentsRedirect,
						function(btn) {
							if (btn === 'yes') me.redirectTo(me.documentsUrl + form.id);
						}
					);
				}
			};
		vm = vm && (vm = vm.children['insurance']) && vm.children['policy'];
		if (service && service.isModel && vm && vm.isInsuranceModel) {
			Ext.Msg.confirm(msg.confirmAction, msg.documentSign, 
				function(btn) {
					// confirm and check record 
					if (btn === 'yes' && me.fireEvent('beforesaverecord', service) !== false) {
						me.disableComponent(component, true);
						// generate and save document
						vm.writeDocument(function(document, options, success) {
							// generate and save document
							if (success) {
								// sign document
								document.sign({
									params: { locale: CFJS.getLocale() },
									callback: function(document, options, success) {
										if (success) {
											// lock update for service
											service.set('updatable', false);
											service.save({
												callback: function(service, options, success) {
													callback(document, options, success);
												}
											});
										} else callback(document, options, success);
									}
								});
							} else callback(document, options, success);
						});
					}
				}
			);
		}
	},
	
	setPurchaseHistoryFilter: function(filter) {
		var me = this, vm = me.getViewModel();
		if (filter.view === 'services' && filter.type && filter.id > 0) {
			filter = [{
				id		: 'customerId',
				property: 'order.customer.id',
				type	: 'numeric',
				operator: 'eq',
				value	: filter.id
			},{
				id		: 'customerClass',
				property: 'order.customer.class',
				type	: 'string',
				operator: 'eq',
				value	: filter.type === 'company' ? 'company' : 'customer'
			}];
		} else filter = null;
		me.onBackClick();
		vm.setStoreFilters('services', filter);
	},
	
	startEditRecord: Ext.emptyFn

});
