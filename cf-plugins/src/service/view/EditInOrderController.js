Ext.define('CFJS.plugins.service.view.EditInOrderController', {
	extend	: 'CFJS.plugins.service.view.EditController',
	alias	: 'controller.plugins.service.editinorder',
	id		: 'service-edit-in-order',
	
	initViewModel: Ext.emptyFn,
	
	linkOrder: function(service, orderId, callback) {
		var options = { callback: callback || Ext.emptyFn };
		this.getViewModel().configService(service, function(record, operation, success) {
			if (success) {
				record.set('order', orderId);
				record.save(options);
			} else options.callback();
		});
	},
	
	lookupOrder: function() {
		return this.getViewModel().getParentItem();
	},
	
	onRemoveRecord: function(component) {
		var me = this, grid = me.lookupGrid(), i = 0, selection;
		if (grid && !grid.readOnly && (selection = grid.getSelection())) {
			me.disableComponent(component, true);
			if (selection.length > 0) {
				for (; i < selection.length; i++) {
					var record = selection[i], callback;
					if (i === selection.length - 1) {
						callback = function(record, operation, success) {
							me.getView().fireEvent('serviceschange', grid);
							me.disableComponent(component);
						};
					}
					if (record.get('order') > 0) me.unlinkOrder(record, callback); else callback();
				}
			}
		}
	},
	
	setPurchaseHistoryFilter: Ext.emptyFn,

	unlinkOrder: function(service, callback) {
		this.linkOrder(service, -1, callback);
	}

});
