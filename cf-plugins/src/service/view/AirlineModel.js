Ext.define('CFJS.plugins.service.view.AirlineModel', {
	extend		: 'CFJS.plugins.service.view.TicketModel',
	alias		: 'viewmodel.plugins.service.airline',
	requires	: ['CFJS.store.airline.AirlineSegments'],
	itemModel	: 'CFJS.model.airline.AirlineTicket',
	routeModel	: 'CFJS.model.airline.AirlineRoute',
	formulas	: {
		routeBySegments: {
			bind: '{route.segments}',
			get	: function(segments) {
				var i = 0, points = [], segment, last;
				if (Ext.isArray(segments) && segments.length > 0) {
					for (var i = 0; i < segments.length; i++) {
						segment = segments[i], paths = [];
						if (!segment) continue;
						// from
						if (segment.from && segment.from.iataCode) paths.push(segment.from.iataCode);
						if (segment.departure) paths.push(Ext.util.Format.date(segment.departure, 'd.m.Y H:i'));
						if (paths.length > 0) {
							paths = paths.join(' ');
							if (last !== paths) {
								points.push(paths);
								last = paths;
							}
							paths = [];
						}
						// to
						if (segment.to && segment.to.iataCode) paths.push(segment.to.iataCode);
						if (segment.arrive) paths.push(Ext.util.Format.date(segment.arrive, 'd.m.Y H:i'));
						if (paths.length > 0) {
							paths = paths.join(' ');
							if (last !== paths) {
								points.push(paths);
								last = paths;
							}
						}
					}
				}
				return points.join(' → ');
			}
		},
		segments: {
			bind: {
				consumer: '{_itemName_.consumer}',
				segments: '{route.segments}'
			},
			get: function(data) {
				var me = this, 
					store = me.getStore('segments'), service = me.getItem();
				if (!store) {
					store = Ext.Factory.store({ type: 'airline_segments', proxy: 'memory' });
					me.setupStore(store, 'segments');
				}
				store.setData(data.segments);
				store.each(function(record) {
					record.setConsumer(data.consumer);
				});
				return store;
			}
		}
	},

	addSegment: function(store, segment) {
		var me = this, 
			service = me.getItem(),
			hasService = service && service.isModel,
			last;
		store = store || me.getStore('segments');
		if (!segment) {
			segment = {};
			if (last = store.last()) {
				segment.carrier = Ext.clone(last.get('carrier'));
				segment.departure = Ext.clone(last.get('arrive'));
				segment.from = Ext.clone(last.get('to'));
			} else if (hasService) {
				segment.carrier = Ext.clone(service.get('supplier'));
				segment.departure = new Date();
			}
		}
		segment = store.add(segment)[0];
		if (hasService) segment.setConsumer(service.get('consumer'))
		return segment;
	}
});