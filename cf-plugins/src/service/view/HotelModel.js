Ext.define('CFJS.plugins.service.view.HotelModel', {
	extend		: 'CFJS.plugins.service.view.ServiceModel',
	alias		: 'viewmodel.plugins.service.hotel',
	itemModel	: 'CFJS.model.hotel.HotelService',
	formulas	: {
		hotel: {
			bind: '{_itemName_.hotel}',
			get	: function(hotel) { return hotel; }
		}
	}
});
