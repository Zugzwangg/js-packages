Ext.define('CFJS.plugins.service.view.SelectorController', {
	extend			: 'CFJS.plugins.service.view.EditInOrderController',
	alias			: 'controller.plugins.service.select',
	closeViewAction	: 'close',
	id				: 'service-select',

	applySelection: function(grid, selection, component) {
		var me = this, 
			order = me.lookupOrder(),
			orderId = order ? order.id : 0,
			vm = me.getViewModel();
		if (orderId > 0 
				&& (grid = grid || me.lookupGrid()) 
				&& (selection = selection || grid.getSelection())) {
			if (!Ext.isArray(selection)) selection = [selection];
			me.disableComponent(component = component || me.getView().findAction('selectRecords'), true);
			for (var i = 0; i < selection.length; i++) {
				if (i === selection.length - 1) {
					me.linkOrder(selection[i], orderId, function(record, operation, success) {
						me.disableComponent(component);
						me.closeView();
					});
				} else me.linkOrder(selection[i], orderId);
			}
		}
	},

	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.editRecord, !selection || selection.length !== 1);
		selectable.disableComponent(actions.removeRecord, !selection || selection.length < 1);
	},

	onGridDblClick: function(view, record, item, index, e, eOpts) {
		this.applySelection(view.ownerGrid, record);
	},
	
	onRemoveRecord: Ext.emptyFn,
	
	onSelectRecords: function(button) {
		this.applySelection();
	},

	onShowContextMenu: function(view, rec, node, index, e) {
		var mainView = this.getView();
		if (mainView.contextMenu) {
			var header = mainView.getHeader();
			e.stopEvent();
			mainView.showContextMenu([e.getX() - mainView.getX(), e.getY() - mainView.getY() - (header ? header.getHeight() : 0)]);
		}
		return !view.contextMenu;
	}

});
