Ext.define('CFJS.plugins.service.view.TravelModel', {
	extend		: 'CFJS.plugins.service.view.ServiceModel',
	alias		: 'viewmodel.plugins.service.travel',
	itemModel	: 'CFJS.model.travel.TravelService',
	formulas	: {
		city: {
			bind: '{_itemName_.city}',
			get	: function(city) { return city; }
		}
	}
});
