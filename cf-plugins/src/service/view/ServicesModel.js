Ext.define('CFJS.plugins.service.view.ServicesModel', {
	extend	: 'CFJS.app.InlineEditorViewModel',
	alias	: 'viewmodel.plugins.service.list',
	requires: [ 'CFJS.store.financial.AbstractServices' ],
	itemName: 'editService',
	stores	: {
		services: {
			type		: 'abstract_services',
			autoLoad	: true,
			session		: { schema: 'financial' },
			filters		: [{
				id		: 'order',
				property: 'order.id',
				type	: 'numeric',
				operator: 'eq',
				value	: undefined
			},{
				id		: 'startDate',
				property: 'created',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'created',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			}],
			sorters		: [{ property: 'created', direction: 'DESC' }],
			summaries	: [{ property: 'id' }, { property: 'amount', type: 'sum' }],
			listeners	: { /* beforesummary: 'beforeLoadSummary',*/ summary: 'onLoadSummary' }
		}
	},

	configService: function(item, callback) {
		if (item && item.data) {
			var serviceType = Ext.getStore('serviceTypes').findModel(item.get('serviceType'));
			item = Ext.create(serviceType, item.data);
			item.load({ callback: callback });
		}
		return item;
	},
	
	privates: {
		
		applyStores: function(stores) {
			var me = this, types = Ext.getStore('serviceTypes'), filter = [];
			if (types && types.isStore) {
				types.each(function(record) {
					filter.push("'" + record.getId() + "'");
				});
			}
			stores.services.filters.push(me.createTypesFilter(filter));
			me.callParent(arguments);
		},
		
		createTypesFilter: function(types) {
			return {
				id		: 'serviceType',
				property: 'serviceType',
				type	: 'set',
				operator: 'in',
				value	: types && types.length > 0 ? types.join() : null
			}
		}
	
	}
});