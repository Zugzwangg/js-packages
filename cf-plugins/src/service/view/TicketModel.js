Ext.define('CFJS.plugins.service.view.TicketModel', {
	extend		: 'CFJS.plugins.service.view.ServiceModel',
	alias		: 'viewmodel.plugins.service.ticket',
	itemModel	: 'TicketModel',
	routeModel	: 'CFJS.model.RouteModel',
	data		: {
		supplierTitle: 'Перевозчик'
	},
	formulas	: {
		route: {
			bind: '{_itemName_.route}',
			get	: function(route) { return route; }
		}
	}
});