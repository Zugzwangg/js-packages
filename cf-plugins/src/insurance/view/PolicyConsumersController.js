Ext.define('CFJS.plugins.insurance.view.PolicyConsumersController', {
	extend	: 'CFJS.admin.controller.Consumers',
	alias	: 'controller.plugins.insurance.consumers',

	beforeSelectCustomer: function(selector, record) {
		return record ? true : false;
	},

	editorConfig: function(config) {
		return Ext.apply(config, {
			viewModel	: { type: 'admin.consumer-edit', itemModel: 'CFJS.plugins.insurance.model.PolicyConsumer' }	
		})
	},
	
	onClearFilters: Ext.emptyFn,
	
	onCustomerPicker: function(component) {
		var me = this, grid = me.lookupGrid();
		if (grid) grid.showCustomerPicker({ beforeselect: 'beforeSelectCustomer', select: 'onSelectCustomer', scope: me });
	},
	
	onReloadStore: Ext.emptyFn,
	
	onRemoveRecord: function(component) {
		var me = this, grid = me.lookupGrid(), i = 0, selection;
		if (grid && !grid.readOnly 
				&& me.fireEvent('beforeremoverecord', selection = grid.getSelection()) !== false) {
			me.disableComponent(component, true);
			if (selection.length > 0) {
				for (; i < selection.length; i++) {
					selection[i].erase(i < selection.length - 1 || {
						callback: function(record, operation, success) {
							me.updatePolicy(grid.getStore());
							me.disableComponent(component);
						}
					});
				}
			}
		}
	},

	onSaveRecord: function(component, force) {
		var me = this, record = me.lookupRecord(), store = me.lookupGridStore();
		if (record && me.fireEvent('beforesaverecord', record) !== false && (record.dirty || record.phantom || force)) {
			me.disableComponent(component, true);
			store.add(record);
			record.save({
				callback: function(record, operation, success) {
					me.updatePolicy(store);
					me.disableComponent(component);
				}
			});
		}
	},
	
	onSelectCustomer: function(selector, record) {
		var me = this, store = me.lookupGridStore(), recordCons;
		if (store && store.isStore && record) {
			recordCons = record.asConsumer();
			store.add(recordCons);
			recordCons.save({
				callback: function(record, operation, success) {
					me.updatePolicy(store);
				}
			});
		}
	},
	
	updatePolicy: function(store) {
		var me = this, vm = me.getViewModel(),
			policy = vm && vm.getParentItem(), data = [];
		if (store && store.isStore && policy) {
			store.each(function(record) {
				data.push(record.getData());
			});
			policy.set('consumers', data);
		}
	}

});