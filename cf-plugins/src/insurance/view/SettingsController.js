Ext.define('CFJS.plugins.insurance.view.SettingsController', {
	extend	: 'CFJS.plugins.app.SettingsController',
	alias	: 'controller.plugins.insurance.settings',
	
	beforeDicRecordEdit: function(editor, context) {
		var record = context.record, value;
		if (record.isTypeVehicle && (value = record.get('capacity')) && !Ext.isString(value)) {
			record.set('capacity', value = JSON.stringify(value));
			if (context.field == 'capacity') context.value = value;
		}
	},
	
	editDicRecord: function(editor, context) {
		var record = context.record, value;
		if (record.isTypeVehicle && (value = record.get('capacity'))) {
			record.set('capacity', value = Ext.decode(value, true) || value);
			if (context.field == 'capacity') context.value = value;
		}
	},
	
	initViewModel : function(vm) {
		var me = this;
		vm.bind('{program}', me.onReconfigure, me);
	},
	
	onAddRecord: function(component) {
		var tab = this.lookupActiveTab(),
			store = this.lookupTabStore(tab),
			Model = store && store.isStore && store.getModel(),
			editor, record = {};
		if (Model && !tab.readOnly) {
			if (!store.remote) record[Model.idProperty] = store.max(Model.idProperty) + 1;
			record = store.add(record)[0];
			editor = tab.findPlugin('rowediting')
			if (record.isModel && editor) editor.startEdit(record);
		}
	},

	onCopyRecord: function(component) {
		var tab = this.lookupActiveTab(),
			store = this.lookupTabStore(tab),
			Model = store && store.isStore ? store.getModel() : null,
			editor, records;
		if (Model && !tab.readOnly && (records = tab.getSelection()) && records.length > 0) {
			records = Ext.clone(records[0].getData());
			records[Model.idProperty] = !store.remote ? store.max(Model.idProperty) + 1 : undefined;
			records = store.add(records)[0];
			editor = tab.findPlugin('rowediting')
			if (records.isModel && editor) editor.startEdit(records);
		}
	},
	
	onReconfigure: function(program) {
		var me = this, view = me.getView(),
			vm = view && view.lookupViewModel(),
			store = vm.get('properties'),
			record, listGrid, i, item;
		if (store && store.isStore) {
			record = store.data.findBy(function(rec, id) {
				return rec.get('name') === 'listSettings'; 
			});
			if (record && record.isModel && (listGrid = record.get('value'))) {
				listGrid = Ext.decode(listGrid);
				if (Ext.isArray(listGrid)) {
					// add grid (except 'properties')
					for (i = 0; i < listGrid.length; i++) {
						if ((item = listGrid[i]) && item.bind.store !== 'properties') {
							view.items.each(function(itemView){
								if (itemView.store.name && item.bind.store === '{'+itemView.store.name+'}') {
									view.remove(itemView);
								}
							});
							view.add(item);
						}
					}
				}
			}
		}
	},

	onReloadStore: function() {
		var me = this, vm = me.getViewModel(),
			program = vm && vm.isSettings && vm.getProgram(),
			store = me.lookupActiveStore();
		if (store && store.remote) store.reload();
		else if (program && program.isModel && (store = vm.get('properties')) && store.isStore) {
			store.removeAll();
			program.reject();
			vm.initializeProgram(program);
		}
	},

	onRemoveRecords: function(component) {
		var me = this, 
			tab = me.lookupActiveTab(),
			store = me.lookupTabStore(tab),
			i = 0, records;
		if (store && store.remote) me.callParent(arguments);
		else if (store && store.isStore && !Ext.isEmpty(store.name) && !tab.readOnly
				&& me.fireEvent('beforeremoverecord', records = tab.getSelection()) !== false && records.length > 0 ) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].erase();
			}
			me.disableComponent(component);
		}
	},

	onSaveRecords: function(component, force) {
		var me = this, vm = me.getViewModel(),
			store = me.lookupActiveStore(), record;
		if (store && store.remote) {
			me.disableComponent(component, true);
			store.sync({
				callback: function(record, operation, success) {
					me.disableComponent(component);
				}
			});
		} else if ((record = vm && vm.isSettings && vm.saveProgram()) && me.fireEvent('beforesaverecord', record) !== false && (record.dirty || record.phantom || force)) {
			me.disableComponent(component, true);
			record.save({
				callback: function(record, operation, success) {
					me.disableComponent(component);
					vm.setProgram(record);
				}
			});
		}
	},
	
	updateVehicles: function(vehicles) {
		var vm = this.getViewModel(),
			childs = vm && vm.isSettings && vm.getStore('typeVehiclesCombo'),
			i, records = [], capacity, rate, name, item;
    	if (childs && childs.isStore) {
    		childs.removeAll();
    		vehicles.each(function(record) {
    			record = record.getData();
    			if ((capacity = record.capacity) && (rate = capacity.rate)) {
        			capacity = capacity.name || '';
        			if (!Ext.isArray(rate)) rate = [rate];
        			for (i = 0; i < rate.length; i++) {
        				item = rate[i];
        				name = [record.name || '', '(' + (item.min || 0), capacity];
        				if (item.max >= 0) name.push('-', item.max, capacity);
        				else name.push ('&#8805;');
        				name.push(')');
        				records.push({ id: (record.code || '') + item.id, name: name.join(' ') });
        			}
    			} else records.push({ id: (record.code || ''), name: record.name || '' });
    		});
    		childs.loadData(records);
    	}
	}
	
});