Ext.define('CFJS.plugins.insurance.view.PolicyConsumersModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.plugins.insurance.consumers',
	id			: 'policyConsumers',
	stores		: {
		consumers: {
			data	: '{_parentItemName_.consumers}',
			model	: 'CFJS.model.dictionary.Consumer'
		}
	},
	
	policyName: function() {
		return this.getParentItemName();
	},
	
	serviceName: function() {
		var vm = this;
		while (vm && vm.type !== 'plugins.service.insurance' && Ext.isFunction(vm.getParent)) {
			vm = vm.getParent();
		}
		return vm && vm.isBaseModel ? vm.getItemName() : null;
	}
	
});
