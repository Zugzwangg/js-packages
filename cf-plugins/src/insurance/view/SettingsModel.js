Ext.define('CFJS.plugins.insurance.view.SettingsModel', {
    extend		: 'CFJS.plugins.insurance.view.EditorViewModel',
	alias		: 'viewmodel.plugins.insurance.settings',
    isSettings	: true,
    stores		: {
		properties: { 
			name	: 'properties',
			fields	: [
				{ name: 'name',		type: 'string',	allowBlank: false,	critical: true },
				{ name: 'value',	type: 'string',	allowBlank: false,	critical: true }
			],
			sorters: ['name']
		}
    },
    
    beforeSaveRecord: function(record) {
		return this.checkService(record, this.getItem());
	},
    
	setProgramField: function(field, data) {
		var store = this.get('properties');
		if (store && store.isStore) {
			store.add({ name: field, value: field !== 'listFormulas' ? Ext.encode(data) : data });
			store.sort();
		}
		this.callParent(arguments);
    },
    
	saveProgram: function() {
		var vm = this, view = vm.getView(), grid, properties = {}, i, record, name, value,
			program = vm.getProgram(), grids = view.query('grid'), len = grids.length;
		if (!program) return;
		for (i = 0; i < len; i++) {
			grid = grids[i];
			if ((store = grid.getStore()) && store.isStore && !Ext.isEmpty(store.name)) {
				store.commitChanges();
				if (store.name === 'properties') {
					store.each(function(record) {
						name = record.get('name'); 
						value = record.get('value');
						if (!Ext.isEmpty(value) && !Ext.Object.isEmpty(value)) {
							properties['field_' + name] = { '@type': 'string', '$': !Ext.isString(value) ? Ext.stringify(value) : value };
						}
					});
				} else {
					value = [];
					store.each(function(record) {
						value.push(record.getData({ persist: true, serialize: true }));
					});
					if (value.length > 0) {
						properties['field_' + store.name] = { '@type': 'string', '$': Ext.encode(value) };
					}
				}
			}
		}
		program.set('properties', properties );
		return program;
	}

});