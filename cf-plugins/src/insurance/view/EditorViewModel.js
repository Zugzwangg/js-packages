Ext.define('CFJS.plugins.insurance.view.EditorViewModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.plugins.insurance.editor',
	mixins		: [ 'CFJS.plugins.insurance.mixin.InsuranceModel' ],
    requires	: [
    	'CFJS.model.insurance.InsuranceProgram',
    	'CFJS.plugins.insurance.model.*',
		'CFJS.store.dictionary.Currencies',
		'CFJS.store.dictionary.CustomerIDs',
		'CFJS.store.dictionary.IDTypes'
    ],
	config		: { program: null, defaultScheme: 'DEF' },
	stores		: {
		cashCover		: { model: 'CFJS.plugins.insurance.model.CashCover', name: 'cashCover', sorters: [ 'amount' ] },
    	companyScheme	: { model: 'CFJS.plugins.insurance.model.CompanyScheme', name: 'companyScheme' },
		franchises		: { model: 'CFJS.plugins.insurance.model.CashCover', name: 'franchises', sorters: [ 'amount' ] },
		schemes			: { model: 'CFJS.plugins.insurance.model.Scheme', name: 'schemes', sorters: ['name'] }
	},
	
	beforeSaveRecord: function(record) {
		var me = this, taxAmount = me.get('taxAmount') || 0,
			program = record && record.isInsurance && record.get('program') || me.get('program');
			programCode = program && (program.isModel && program.get('code') || program.code) || null;
		if (programCode) record.setTax('INSURANCE-' + programCode, taxAmount);
		return me.checkService(record, me.getItem());
	},
	
	checkService: function(record, item) {
		return record && record.isInsurance && item && item.isInsuranceData && item.updateServiceProperties();
	},

	destroy: function () {
		this.destroyInternalListeners();
		this.callParent(arguments);
	},

	destroyInternalListeners: function() {
		var me = this, listeners = me.internalListeners,
			name, listener;
		for (name in listeners) {
			if ((listener = listeners[name]) && Ext.isFunction(listener.destroy)) listener.destroy();
		}
		delete me.internalListeners;
	},
	
	getService: function() {
		var policy = this.getItem();
		return policy && policy.isInsuranceData ? policy.getService() : null;
	},
	
    initInternalListeners: function(observable) {
		var me = this, listeners = {};
		me.destroyInternalListeners();
		listeners.bindProgram = me.bind('{program}', me.onInitProgram);
		listeners.bindScheme = me.bind('{_itemName_.scheme}', me.onSchemeChange);
		if (observable) listeners.beforeSaveRecord = observable.on('beforesaverecord', me.beforeSaveRecord, me, { destroyable: true });
		return me.internalListeners = listeners;
	},
	
	onInitProgram: function(program) {
		var me = this, 
			schemes = me.get('schemes'),
			defaultScheme = program && program.defaultScheme;
		if (schemes && schemes.isStore) {
			defaultScheme = schemes.getById(defaultScheme || me.getDefaultScheme());
			if (defaultScheme && defaultScheme.isModel) defaultScheme = defaultScheme.get('code');
		}
		me.setDefaultScheme(defaultScheme);
	},
	
	onSchemeChange: Ext.emptyFn,
	
	privates: {

		updateDefaultScheme: function(defaultScheme) {
			var policy = this.getItem(), service = policy && policy.isInsuranceData && policy.getService();
			if (service && service.isModel && service.phantom) policy.set('scheme', defaultScheme);
		}
		
	}
	
});