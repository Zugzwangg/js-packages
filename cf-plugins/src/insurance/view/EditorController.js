Ext.define('CFJS.plugins.insurance.view.EditorController', {
	extend: 'CFJS.app.DictionaryController',
	alias: 'controller.plugins.insurance.editor',
	
	initViewModel : function(vm) {
		var me = this;
		vm.bind('{program}', me.onReconfigure, me);
	},
	
	onReconfigure: Ext.emptyFn
	
});
