Ext.define('CFJS.plugins.insurance.civilliability.view.SettingsModel', {
    extend		: 'CFJS.plugins.insurance.view.SettingsModel',
    requires	: [
    	'CFJS.plugins.insurance.civilliability.model.*',
    	'CFJS.store.dictionary.CarBrands',
    	'CFJS.store.dictionary.CarModels'
    ],
    alias		: 'viewmodel.plugins.insurance.civilliability.settings',
    program		: 'CL',
    stores		: {
		bonusMaluses		: { model: 'CFJS.plugins.insurance.civilliability.model.BonusMalus', name: 'bonusMaluses' },
		carBrands			: { type: 'carbrands' },
		carModels			: { type: 'carmodels' },
		cityZones			: { model: 'CFJS.plugins.insurance.civilliability.model.CityZone', name: 'cityZones' },
		duration			: { model: 'CFJS.plugins.insurance.civilliability.model.Period', name: 'duration', sorters: [ 'period' ] },
		persons				: { type: 'enumerated', data: [{ id: '0', name: "Юр.лицо" }, { id: '1', name: "Физ.лицо" }] },
		places				: { model: 'CFJS.plugins.insurance.civilliability.model.Place', name: 'places', sorters: [ 'zone' ] },
		tariffs				: { model: 'CFJS.plugins.insurance.civilliability.model.Tariff', name: 'tariffs', sorters: [ 'zone', 'vehicle', 'person' ] },
		typeVehiclesCombo	: { type: 'enumerated', sorters: [ 'id' ] },
		typeVehicles		: {
			model	: 'CFJS.plugins.insurance.civilliability.model.TypeVehicle',
			name	: 'typeVehicles',
			listeners	: { datachanged: 'updateVehicles', update: 'updateVehicles' }
		},
		validity			: { model: 'CFJS.plugins.insurance.civilliability.model.Period', name: 'validity', sorters: [ 'period' ] },
		volumeDiscounts		: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] },
		zones				: {
			type: 'enumerated',
			data: [
				{ id: '1', name: "Зона 1" },
				{ id: '2', name: "Зона 2" },
				{ id: '3', name: "Зона 3" },
				{ id: '4', name: "Зона 4" },
				{ id: '5', name: "Зона 5" },
				{ id: '6', name: "Зона 6" },
				{ id: '7', name: "Зона 7" }
			]
		}
	}
    
});