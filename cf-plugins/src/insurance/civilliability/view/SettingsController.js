Ext.define('CFJS.plugins.insurance.civilliability.view.SettingsController', {
	extend	: 'CFJS.plugins.insurance.view.SettingsController',
	alias	: 'controller.plugins.insurance.civilliability.settings',
	
	capacityRenderer: function(value) {
		return value && !Ext.isString(value) ? JSON.stringify(value) : value;
	},
	
	monthsRenderer: function(value) {
		if (value > 0) {
			if (value <= .5) return '15 дней';
			if (value === 1) return '1 месяц';
			if (value < 5) return value + ' месяца';
			return value + ' месяцев';
		}
	},
    
	zoneRenderer: function( value ) {
		var grid = this, vm = grid.getViewModel(), store = vm && vm.isSettings ? vm.getStore('zones') : null, record;
		if( store && store.isStore )
		{
			record = store.findRecord('id', value);
			if(record)
				return record.get('name');
		}
		return value;
	},

    personRenderer: function( value ) {
    	var grid = this, vm = grid.getViewModel(), store = vm && vm.isSettings ? vm.getStore('persons') : null, record;
    	if( store && store.isStore )
    	{
    		record = store.findRecord('id', value);
        	if(record)
        		return record.get('name');
    	}
    	return value;
    },
    
    sorterCityFn : function(record1, record2) {
        var name1 = record1.data.city.name,
        	zone1 = record1.data.zone,
            name2 = record2.data.city.name,
            zone2 = record2.data.zone;
        return zone1 > zone2 ? 1 : (zone1 === zone2) ? (name1 > name2 ? 1 : (name1 === name2) ? 0 : -1) : -1;
    },
    
	filterCityFn : function(record, value) {
        return (record.data.city.name.toLowerCase().indexOf(value.toLowerCase()) === -1) ? 0 : 1;
    }
	
});