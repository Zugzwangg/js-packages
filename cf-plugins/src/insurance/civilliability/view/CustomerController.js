Ext.define('CFJS.plugins.insurance.civilliability.view.CustomerController', {
    extend: 'CFJS.plugins.insurance.civilliability.view.ClientController',
    alias: 'controller.plugins.insurance.civilliability.customer',
    
	validatorNextControl : function(value) {
		var item = this.lookupViewModel().getItem();
		return Ext.isEmpty(value) && !(item && item.get('needControl')) || this.blankText;
	},
	
	onChangeUseMonths: function(field, newVal, oldVal) {
		if (field.getChecked().length > 6) {
			Ext.fireEvent('infoalert', 'Не может быть больше 6 месяцев!', function() { field.setValue(oldVal); }, this, field.ownerCt);
			return;
		}
	},
	
	onChangeDateFrom: function(field, newVal, oldVal) {
		var me = this, view = me.view, vm = view && view.lookupViewModel(),
			item = vm.getItem(), service = item && item.isInsuranceData && item.getService();
		if(newVal && service) service.set('dateTo', Ext.Date.add(Ext.Date.add(newVal, Ext.Date.YEAR, 1), Ext.Date.DAY, -1));
		return;
	},
	
	dateTodayAddOneDay : function() {
		return Ext.Date.add(Ext.Date.today(), Ext.Date.DAY, 1); 
	}

});
