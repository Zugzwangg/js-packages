Ext.define('CFJS.plugins.insurance.civilliability.view.ClientModel', {
	extend	: 'CFJS.plugins.insurance.view.EditorViewModel',
	alias	: 'viewmodel.plugins.insurance.civilliability.client',
	requires: [
		'CFJS.store.dictionary.CarBrands',
		'CFJS.store.dictionary.CarModels',
		'CFJS.store.dictionary.Cities',
		'CFJS.plugins.insurance.civilliability.model.*'
	],
	program		: 'CL',
	itemModel	: 'CFJS.plugins.insurance.civilliability.model.Policy',
	itemName	: 'policy',
	data		: { baseAmount: 0 },
	formulas	: {
		carBrand			: {
			bind: '{_itemName_.carModel.brand}',
			get	: function(brand) { return brand; },
			set	: function(brand) {
				var models = this.getStore('carModels');
				if (models && models.isStore) {
					if (brand && brand.isModel) brand = brand.getData();
					models.addFilter({ id: 'brand', property: 'brand.id', type: 'numeric', operator: 'eq', value: (brand||{}).id });
				}
			}
		},
		registrationCountry	: {
			bind: '{_itemName_.registrationCity.country}',
			get	: function(country) { return country; },
			set	: function(country) {
				var cities = this.getStore('cities');
				if (cities && cities.isStore) {
					if (country && country.isModel) country = country.getData();
					cities.addFilter({ id: 'country', property: 'country.id', type: 'numeric', operator: 'eq', value: (country||{}).id });
				}
			}
		},
		cityZone			: {
			bind: '{_itemName_.registrationCity}',
			get	: function(city) {
				city = city || {};
				var me = this, policy = me.getItem(), store = me.get('cityZones'),
					service = policy && policy.isInsuranceData && policy.getService(),
					country = city.country || {}, cityZone;
				if (service && service.isModel && store && store.isStore) {
					if (city.id > 0) cityZone = store.data.findBy(function(info) {
						return (info.get('city')||{}).id === city.id; 
					});
					service.set('zone', cityZone = cityZone ? cityZone.get('zone') : ( country.id === 206) ? 6 : 7);
				}
				return cityZone;
			}
		},
		cityMREO			: {
			bind: '{_itemName_.mreo}',
			get	: function(mreo) {
				var me = this, item = me.getItem(), store = me.get('cityZones'),
					cityMREO, city, code, view = me.getView(), field;
				if (mreo && mreo.length === 4 && item && store && store.isStore) {
					cityMREO = store.data.findBy(function(info) {
						return (info && info.isModel) ? info.get('code').indexOf(mreo) !== -1 : null
					});
					city = cityMREO ? cityMREO.get('city') : null;
//					if (field = view.down('dictionarycombo[name=country]')) {
//						if (city) field.setValue(city.country);
//						else field.clearValue();
//					}
//					if (field = view.down('dictionarycombo[name=registrationCity]')) {
//						if (city) field.setValue(city);
//						else field.clearValue();
//					}
					item.set('registrationCity', city);
				}
				return city;
			}
		},
		tariff: {
			bind: [ '{_itemName_.customerType}', '{_itemName_.typeVehicleCode}', '{_parentItemName_.zone}' ],
			get: function(data) {
				var me = this, tariffs = me.get('tariffs'), tariff;
				if (tariffs && tariffs.isStore) {
					tariff = tariffs.data.findBy( function(rec, id) {
						return rec.get('person') === data[0] 
							&& rec.get('vehicle') === data[1]
							&& rec.get('zone') === data[2];
					});
				}
				return tariff;
			}
		},
		zoneK2	: {
			bind: '{_parentItemName_.zone}',
			get: function(zone) {
				var me = this, places = me.get('places'), 
					place = places && places.isStore ? places.data.find('zone', zone) : null;
				return place ? place.get('k2') : 0;
			}
		},
		areaK3			: {
			bind: { tariff: '{tariff}', isTaxi: '{_itemName_.isTaxi}' },
			get	: function(data) {
				var tariff = (data.tariff||{}).data;
				return tariff ? tariff['k3' + (data.isTaxi ? '_taxi' : '')] : 0;
			}
		},
		experienceK4	: {
			bind: { customerType: '{_itemName_.customerType}', useNovice: '{_itemName_.useNovice}' },
			get	: function(data) {
				switch (data.customerType||0) {
					case 0: return 1.2;
					case 1: return data.useNovice ? 1.5 : 1.5;
					default: return 1;
				}
			}
		}, 
		durationK5	: {
			bind: '{_itemName_.useMonths}',
			get: function(useMonths) {
				var me = this,
					duration = me.get('duration'),
					field = me.getView().down('manyofmanyfield[name=useMonths]'),
					checked = 12 - (field && field.isCheckboxGroup ? field.getChecked().length : 0),
					finder = function(months) {
						var period = duration.data.findBy(function(rec, id) {
							return rec.get('period') === months; 
						});
						return (period ? period.get('rate') : null) || 0;
					}, period = 0;
				if (duration && duration.isStore) {
					if (!duration.isLoaded() || duration.isLoading()) {
						duration.on({
							datachanged: function() {
								me.set('durationK5', finder.apply(me, [checked]));
							},
							single: true
						});
					} else period = finder.call(me, checked);
				}
				return period;
			}
		},
		fraudK6	: {
			bind: { hasFraud: '{_itemName_.hasFraud}', fraud: '{program.fraud}' },
			get: function(data) {
				return (data.hasFraud ? data.fraud : 0) || 1;
			}
		},
		validityK7	: {
			bind: '{_itemName_.validity}',
			get: function(countMonth) {
				var me = this, validity = me.get('validity'),
					period = validity && validity.isStore ? validity.data.find('period', countMonth) : null;
				return period ? period.get('rate') : 0;
			}
		},
		validityK7Wiz	: {
			bind: ['{_parentItemName_.dateFrom}','{_parentItemName_.dateTo}'],
			get: function(data) {
				var me = this, validity = me.get('validity'),
				period, months, dateFrom = data[0], dateTo = data[1];
				if(dateFrom && dateTo) {
					dateTo = Ext.Date.add(dateTo, Ext.Date.DAY, 2);
					months = Ext.Date.diff(dateFrom, dateTo, Ext.Date.MONTH);
		            months = (months > 12) ? 12 : months;
					if(validity && validity.isStore) {
						if(validity.getCount()) {
							period = validity.data.find('period', months);
							return period ? period.get('rate') : 0;
						} else
							return 1;
					}
				} else
					return 1;
			}
		},
		bonusMalusK8		: {
			bind: '{_itemName_.bonusMalus}',
			get	: function(bonusMalus) {
				return (bonusMalus||{}).rate || 1;
			}
		},
		volumeDiscountsK9		: {
			bind: '{_itemName_.vehiclesNumber}',
			get	: function(vehiclesNumber) {
				var me = this,
					volumeDiscounts = me.get('volumeDiscounts'), count;
				if (volumeDiscounts && volumeDiscounts.isStore) {
					count = volumeDiscounts.data.findBy( function(rec, id) {
						return rec.get('min') <= vehiclesNumber && vehiclesNumber <= rec.get('max');
					});
				}
				return count ? count.get('rate') : 1;
			}
		},
		benefitsK10			: {
			bind: [ '{_parentItemName_.consumer.document}', '{_itemName_.hasPrivilege}', '{program.privilegeRate}', '{_itemName_.capacityVehicle}', '{program.privilegeMaxSize}' ],
			get: function(data) {
				return (data[0] && data[1] && (data[3] < data[4])) * data[2] || 1;
			}
		},
		benefitsK10Wiz			: {
			bind: [ '{_itemName_.hasPrivilege}', '{program.privilegeRate}', '{_itemName_.capacityVehicle}', '{program.privilegeMaxSize}' ],
			get: function(data) {
				return (data[0] && (data[2] < data[3])) * data[1] || 1;
			}
		},
		calcAmount			: {
			bind: [ '{baseAmount}', '{tariff.k1}', '{zoneK2}', '{areaK3}', '{experienceK4}', '{durationK5}',
			        '{fraudK6}', '{validityK7}', '{bonusMalusK8}', '{volumeDiscountsK9}', '{benefitsK10}'],
			get	: function(data) {
				var me = this, i = 0, len = data.length, amount = 1,
					policy = me.getItem(), service = policy && policy.isInsuranceData && policy.getService();
				for (; i < len; i++) {
					amount *= (data[i] || 0);
				}
				amount = Math.round(amount * 100) / 100;
				me.set('taxAmount', amount);
    			if(service && service.isModel)
    				service.set('paymentAmount', amount);
    			return amount;
			}
		},
		calcAmountWiz			: {
			bind: [ '{baseAmount}', '{tariff.k1}', '{zoneK2}', '{areaK3}', '{experienceK4}', '{durationK5}',
			        '{fraudK6}', '{validityK7Wiz}', '{bonusMalusK8}', '{volumeDiscountsK9}', '{benefitsK10Wiz}'],
			get	: function(data) {
				var me = this, i = 0, len = data.length, amount = 1,
					policy = me.getItem(), service = policy && policy.isInsuranceData && policy.getService();
				for (; i < len; i++) {
					amount *= (data[i] || 0);
				}
				amount = Math.round(amount * 100) / 100;
    			me.set('taxAmount', amount );
    			if(service && service.isModel)
    				service.set('paymentAmount', amount);
    			return amount;
			}
		} 
	},
	stores	: {
		bonusMaluses	: { model: 'CFJS.plugins.insurance.civilliability.model.BonusMalus' },
		cityZones		: { model: 'CFJS.plugins.insurance.civilliability.model.CityZone' },
		validity		: { model: 'CFJS.plugins.insurance.civilliability.model.Period' },
		duration		: { model: 'CFJS.plugins.insurance.civilliability.model.Period' },
		places			: { model: 'CFJS.plugins.insurance.civilliability.model.Place' },
		tariffs			: { model: 'CFJS.plugins.insurance.civilliability.model.Tariff' },
		typeVehicles	: { model: 'CFJS.plugins.insurance.civilliability.model.TypeVehicle' },
		carBrands		: { type: 'carbrands' },
		carModels		: { type: 'carmodels' },
		cities			: { type: 'cities' },
		volumeDiscounts	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] }
	},
	
	privates	: {
		
		configItem: function(item) {
			var me = this, 
				dateFrom = Ext.Date.add(new Date(), Ext.Date.SECOND, 1),
				program = me.get('program') || {}, typeVehicles, record;
			me.set('baseAmount', program.baseAmount);
			if (item) {
				if (item.isInsuranceData) return item;
				if (item.isInsurance) {
					item.set('insuranceAmount', item.get('insuranceAmount') || program.insuranceAmount);
					item.set('supplier', item.get('supplier') || program.defaultInsurer);
					if (!(item.get('insuranceCurrency')||{}).code) item.set('insuranceCurrency', program.insuranceCurrency);
					item.set('dateFrom', dateFrom = item.get('dateFrom') || dateFrom);
					if (item.phantom)
						item.set('dateTo', Ext.Date.add(Ext.Date.add(dateFrom, Ext.Date.YEAR, 1), Ext.Date.DAY, -1));
					else
						item.set('dateTo', item.get('dateTo') || Ext.Date.add(Ext.Date.add(dateFrom, Ext.Date.YEAR, 1), Ext.Date.DAY, -1));
					item.set('dateContract', dateFrom = item.get('dateFrom') || dateFrom);
					item.set('scheme', Ext.clone(me.getDefaultScheme()));
					if(item.get('franchise') == null )
						item.set('franchise', program.franchise);
					item.set('paymentDate', item.get('paymentDate') || Ext.Date.today());
					item = item.getServiceData(Ext.create(me.getItemModel()));
					typeVehicles = me.getStore('typeVehicles');
					if(typeVehicles && typeVehicles.isStore) {
						record = typeVehicles.findRecord('code', 'B');
						if (record) 
							record = record.data;
					}
					if(!record)
						record = {code: 'B'};
					//----
					if(program.registrationCountry)
						me.set('registrationCountry', program.registrationCountry);
					//----
				}
			}
			return {
				type	: me.getItemModel(),
				create	: Ext.apply({
					bonusMalus			: program.bonusMalus,
					customerType		: 1,
					healthAmount		: program.healthAmount,
					registrationCity 	: { country : program.registrationCountry },
					yearIssue			: dateFrom.getFullYear(),
					typeVehicle			: record
				}, item) 
			};
		}
    
	}
	
});
