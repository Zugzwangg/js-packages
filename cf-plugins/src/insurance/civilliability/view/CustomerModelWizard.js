Ext.define('CFJS.plugins.insurance.civilliability.view.CustomerModelWizard', {
	extend	: 'CFJS.plugins.insurance.civilliability.view.ClientModel',
	alias	: 'viewmodel.plugins.insurance.civilliability.customer-wizard',
	mixins	: [ 'CFJS.plugins.insurance.mixin.InsuranceModel' ],
	
	requires	: [ 'CFJS.plugins.insurance.civilliability.model.Policy' ],
	
	formulas: {
		customerFilter: {
			bind: '{_parentItemName_.consumer}',
			get	: function(consumer) { return ((consumer||{}).customer||{}).id; }
		},
		customerId: {
			bind: '{_parentItemName_.consumer.document}',
			get	: function(document) { return document },
			set	: function(document) {
				var service = this.getParentItem(), 
					consumer = service && service.isInsurance ? service.get('consumer') : null;
				if (consumer) {
					consumer.document = document;
					service.set('consumer', consumer);
				}
			}
		}
	},
	stores	: {
		customerIDs	: {
			type		: 'customerids',
			filters		: [{
				id		: 'owner',
				property: 'owner.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{customerFilter}'
			}]
		}
	},
	
	
	privates	: {
		
		configItem: function(item) {
			return this.callParent(arguments);
		}
	
	}

});
