Ext.define( 'CFJS.plugins.insurance.civilliability.model.CountVehicle', {
	extend 		: 'Ext.data.Model',
	identifier	: 'sequential',
	idProperty	: 'min',
	fields		: [
		{ name: 'min',		type: 'int' },
		{ name: 'max',		type: 'int',	defaultValue: Number.MAX_SAFE_INTEGER },
		{ name: 'rate',		type: 'number', defaultValue: 1 },
		{ name: 'title',	type: 'string', calculate: function(data) { return data.min + ' - ' + data.max; } }
	],
	proxy		: 'memory'
});