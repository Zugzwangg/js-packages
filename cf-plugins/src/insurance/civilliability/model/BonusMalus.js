Ext.define( 'CFJS.plugins.insurance.civilliability.model.BonusMalus', {
	extend		: 'Ext.data.Model',
	identifier	: { type: 'sequential', prefix: 'B'},
	idProperty	: 'code',
	fields		: [
		{ name: 'code',	type: 'string', critical: true },
		{ name: 'rate', type: 'number', critical: true }
	],
	proxy		: 'memory'
});