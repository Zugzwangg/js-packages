Ext.define( 'CFJS.plugins.insurance.civilliability.model.Tariff', {
	extend: 'CFJS.plugins.insurance.model.SchemeDetail',
	fields	: [
 		{ name: 'zone',		type: 'int' },
		{ name: 'vehicle',	type: 'string' },
		{ name: 'person',	type: 'int' },
		{ name: 'k1',		type: 'number' },
		{ name: 'k3',		type: 'number' },
		{ name: 'k3_taxi',	type: 'number' }
	]
});