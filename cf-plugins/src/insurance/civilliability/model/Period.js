Ext.define( 'CFJS.plugins.insurance.civilliability.model.Period', {
	extend 		: 'Ext.data.Model',
	identifier	: 'sequential',
	idProperty	: 'period',
	fields		: [
		{ name: 'period',	type: 'number' },
		{ name: 'rate',		type: 'number' }
	],
	proxy		: 'memory'
});