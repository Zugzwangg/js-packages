Ext.define( 'CFJS.plugins.insurance.civilliability.model.TypeVehicle', {
	extend 			: 'Ext.data.Model',
	identifier		: { type: 'sequential' },
	convertOnSet	: false,
	isTypeVehicle	: true,
	fields			: [
		{ name: 'id',		type: 'int' },
		{ name: 'code',		type: 'string',	critical: true },
		{ name: 'name',		type: 'string',	critical: true },
		{ name: 'capacity',	allowNull: true, convert: function(value) { return Ext.decode(value, true) || value; } }
	],
	proxy			: 'memory'
});