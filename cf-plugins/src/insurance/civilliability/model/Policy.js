Ext.define( 'CFJS.plugins.insurance.civilliability.model.Policy', {
	extend		: 'CFJS.model.insurance.InsuranceServiceData',
	requires	: [
		'CFJS.schema.Financial',
		'CFJS.model.dictionary.CarModel',
		'CFJS.model.dictionary.City',
		'CFJS.model.dictionary.CustomerID',
		'CFJS.plugins.insurance.model.*',
		'CFJS.plugins.insurance.civilliability.model.BonusMalus',
		'CFJS.plugins.insurance.civilliability.model.TypeVehicle'
	],
	schema		: 'financial',
	fields		: [
		//	main data
		{ name: 'name',				type: 'string',		allowBlank: false,	critical: true, defaultValue: 'ГО' },
		{ name: 'customerType',		type: 'int',		allowBlank: false,	critical: true },
		{ name: 'stickerSeries',	type: 'string',		allowBlank: false,	critical: true },
		{ name: 'stickerNumber',	type: 'string',		allowBlank: false,	critical: true },
		{ name: 'healthAmount',		type: 'number',		allowBlank: false,	critical: true },
		//	car details
		{ name: 'bodyNumber',		type: 'string',		allowBlank: false,	critical: true },
		{ name: 'capacityVehicle',	type: 'number' },
		{ name: 'carModel',			type: 'model',		allowBlank: false,	critical: true, entityName: 'CFJS.model.dictionary.CarModel' },
		{ name: 'plateNumber',		type: 'string',		allowBlank: false,	critical: true },
		{ name: 'registrationCity',	type: 'model',		allowBlank: false,	critical: true, entityName: 'CFJS.model.dictionary.City'},
		{ name: 'mreo',				type: 'string' },
		{ name: 'typeVehicle',		type: 'auto',		allowBlank: false,	critical: true },
		{ name: 'yearIssue',		type: 'int',		allowBlank: false,	critical: true },
		//	other details
		{ name: 'bonusMalus',		type: 'model',		allowBlank: false,	critical: true, entityName: 'CFJS.plugins.insurance.civilliability.model.BonusMalus' },
		{ name: 'dateNextControl',	type: 'date' },
		{ name: 'hasFraud',			type: 'boolean',	allowBlank: false,	critical: true },
		{ name: 'hasPrivilege',		type: 'boolean',	allowBlank: false,	critical: true }, 
		{ name: 'isTaxi',			type: 'boolean',	allowBlank: false,	critical: true },
		{ name: 'useNovice',		type: 'boolean',	allowBlank: false,	critical: true },	// driving_exp < 3 -> date_rights 
		{ name: 'useMonths',		type: 'int',		allowBlank: false,	critical: true },
		{ name: 'vehiclesNumber',	type: 'int',		allowBlank: false,	critical: true,	defaultValue: 1 },

		//	calculated
		
		{ name: 'typeVehicleCode',	type: 'string',
			calculate: function(data) {
				var typeVehicle = data.typeVehicle || {},
					rate = (typeVehicle.capacity || {}).rate || [],
					capacityVehicle = data.capacityVehicle || 0,
					typeVehicleCode = typeVehicle.code, 
					i = 0, max, min;
				if (!Ext.isArray(rate)) rate = [rate];
				for (; i < rate.length; i++) {
					if (capacityVehicle >= (rate[i].min || 0) 
							&& capacityVehicle <= (rate[i].max || Number.MAX_VALUE)) return typeVehicleCode + rate[i].id;
				}
				return typeVehicleCode;
			}
		},
		{ name: 'needControl',			type: 'boolean',
			calculate: function(data) {
				switch ((data.typeVehicle||{}).code||'') {
					case 'C':
					case 'E':
						return data.capacityVehicle > 3500 && Ext.Date.add(new Date(data.yearIssue), Ext.Date.YEAR, 2).getTime() <= Ext.Date.today().getTime();
					case 'B':
					case 'F':
						return data.isTaxi;
					case 'D': return true;
					case 'A':
					default: return false;
				}
				return false;
			}
		},
		{ name: 'validity',	type: 'number', 
			convert: function (v, rec) {
				var service = rec.getService(), dateFrom = Ext.Date.today(),
					dateTo = Ext.Date.today(), months = 0;
				if (service && service.isModel) {
					dateFrom = service.get('dateFrom') || dateFrom;
					dateTo = service.get('dateTo') || dateTo;
				}
				dateTo = Ext.Date.add(dateTo, Ext.Date.DAY, 1);
				months = Ext.Date.diff(dateFrom, dateTo, Ext.Date.MONTH);
				if (Ext.Date.add(dateFrom, months, Ext.Date.MONTH).getTime() < dateTo.getTime()) months++;
				return months > 12 ? 12 : months;
			},
			depends: 'service'
		}
	],
	
	updateServiceProperties: function() {
		var me = this,
			before = Ext.Date.before,
			service = me.getService(),
			dateContract = service.get('dateContract'),
			dateFrom = service.get('dateFrom'),
			dateTo = service.get('dateTo'),
			errors = [];
		if (before(dateFrom, dateContract)) {
			console.warn(dateContract.getTime(), dateFrom.getTime());
			errors.push('Дата начала действия договора не может быть раньше даты его создания!');
		}
		if (before(dateTo, dateFrom)) {
			console.warn(dateFrom.getTime(), dateTo.getTime());
			errors.push('Дата окончания действия договора не может быть раньше даты начала!');
		}
		if (errors.length > 0) {
			Ext.fireEvent('infoalert', errors.join('\n'));
			return false;
		}
		return me.callParent();
	}
	
});