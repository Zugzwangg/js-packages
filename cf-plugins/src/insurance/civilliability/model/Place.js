Ext.define( 'CFJS.plugins.insurance.civilliability.model.Place', {
	extend 	: 'Ext.data.Model',
	identifier	: { type: 'sequential' },
	idProperty	: 'zone',
	fields		: [
		{ name: 'zone',	type: 'int' },
		{ name: 'k2',	type: 'number' }
	],
	proxy		: 'memory'
});