Ext.define( 'CFJS.plugins.insurance.civilliability.model.CityZone', {
	extend 		: 'Ext.data.Model',
	requires	: [ 'CFJS.model.dictionary.City' ],
	identifier	: { type: 'sequential' },
	fields		: [
		{ name: 'id',	type: 'int' },
		{ name: 'city',	type: 'model', critical: true, entityName: 'CFJS.model.dictionary.City' },
		{ name: 'zone',	type: 'int' },
		{ name: 'code',	type: 'string' }
	],
	proxy		: 'memory'
});