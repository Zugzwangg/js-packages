Ext.define('CFJS.plugins.insurance.mixin.InsuranceModel', {
	requires		: 'CFJS.store.insurance.InsurancePrograms',
	mixinId			: 'insurancemodel',
	requires		: [ 'Ext.TaskManager' ],
	id				: 'policy',
	config			: { program: { $value: null, lazy: true } },
	isDetail		: true,
	isInsuranceModel: true,

	applyProgram: function(program) {
		if (program && !program.isModel) {
			if (Ext.isObject(program)) program = program.code;
			if (!Ext.isString(program) || Ext.isEmpty(program)) program = null;
		}
		return program;
	},
	
	getProgramField: function(field) {
		return this.get('program.' + field);
	},
    
	initializeProgram: function(program) {
		var me = this,
			properties = (program ? program.get('properties') : null) || {},
			i, field, value, item;
		me.set('program', null);
		for (field in properties) {
			value = properties[field];
			if (value) {
				if (value['$']) value = value['$'];
				else delete value['@type'];
			}
			if (field.startsWith('field_')) field = field.substring(6);
			me.setProgramData(field, value);
       		if (field === 'listAddStores') {
    			value = Ext.decode(value);
    			if (Ext.isArray(value)) {
    				// add store
    				for (i = 0; i < value.length; i++) {
    					if ((item = value[i]) && item.name && item.name !== 'properties') me.createStore(item.name, item);
					}
    			}
    		}
		}
		me.set('program', me.get('program') );
    },

    initInternalListeners: Ext.emptyFn,

	preparePolicyData: Ext.identityFn,

	setProgramData: function(field, data) {
		var me = this, store = me.get(field);
		if (Ext.isString(data) && field !== 'listFormulas') data = Ext.decode(data, true) || data;
		if (store && store.isStore) {
			if (!Ext.isArray(data)) data = [data];
			store.loadRawData(data);
		} else me.setProgramField(field, data);
	},
	
	setProgramField: function(field, data) {
		this.set('program.' + field, data);
    },

	updateProgram: function(program) {
		var me = this;
		if (Ext.isString(program)) {
			Ext.Factory.store({ type: 'insurance_programs', filters: [{ property: 'code', type: 'string', operator: 'eq', value: program }] })
				.load({
					callback: function(records, operation, success) {
						me.setProgram(success && records && records.length > 0 ? records[0] : null);
					}
				});
		} else Ext.TaskManager.start({
			run			: function() { me.initializeProgram(program); },
			fireOnStart	: false,
			interval	: 50,
			repeat		: 1
		});
	},
	
    writeDocument: function(callback) {
    	var me = this, record = me.getItem(), 
    		cfg = me.getProgramField('policyConfig'), data, document;
    	if (record && record.isModel) {
    		data = Ext.clone(record.getData());
    		if (record.isInsuranceData && (record = record.getService())) {
    			data = Ext.apply(data, record.getData());
    		}
    		me.saveDocumentData(me.fillDocumentData(me.getProgramField('policyConfig'), me.preparePolicyData(data)), { callback: callback });
    	}
    },
    
    privates: {
    	
    	saveDocumentData: function(data, options) {
			options = options || {};
    		var me = this, 
    			callback = options.callback,
    			document = data && data.document, 
    			childs = data && data.childs, i;
    		if (document && document.isModel) {
    			if (options.parentId > 0) document.set('parentId', options.parentId);
    			document.save({
    				params	: { locale: CFJS.getLocale() },
    				callback: function(record, operation, success) {
    					if (!options.args) options.args = arguments;
    					if (success && Ext.isArray(childs) && childs.length > 0) {
    						options.parentId = record.getId();
        					for (i = 0; i < childs.length; i++) {
        						options.callback = i < childs.length - 1 || callback;
        						me.saveDocumentData(childs[i], options);
        					}
    					}
    					if (callback && Ext.isFunction(callback) && options.args) callback.apply(me, options.args);
    				}
    			});
    		}
    	},
    
    	fillDocumentData: function(cfg, data) {
    		var me = this, childs = [], i, key, field, value, fields = {};
    		if (data && cfg && cfg.form > 0 && cfg.fields) {
    			for (key in cfg.fields) {
    				value = key === 'self' ? data : data[key];
        			if (value === null || value === undefined) continue;
        			// table
        			if ((field = cfg.fields[key]).form > 0 && Ext.isArray(value)) {
        				for (i = 0; i < value.length; i++) {
        					childs.push(me.fillDocumentData(field, value[i]));	
        				}
        				continue;
        			}
        			if (field.type === 'named_entity') value = { id: value.id, name: value.name };
        			else if (Ext.isDate(value)) value = { '$': Ext.Date.format(value, CFJS.Api.dateTimeFormat) };
        			else value = { '$': value }
        			value['@type'] = field.type || 'string';
        			if (field.id) field = field.id;
        			fields['field_' + field] = value;
    			}
    			return {
    				document: new CFJS.model.document.Document({
	    				author		: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
	    				created		: new Date(),
	    				form		: { id: cfg.form, name: cfg.name },
	    				properties	: fields,
	    				startDate	: new Date(),
	    				unit		: me.get('userUnit')
    				}),
    				childs	: childs
   	    		}
    		}
    	}
    
    }
});