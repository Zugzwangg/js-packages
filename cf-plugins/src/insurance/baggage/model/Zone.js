Ext.define( 'CFJS.plugins.insurance.baggage.model.Zone', {
	extend 		: 'Ext.data.Model',
	requires	: [ 'CFJS.model.dictionary.Currency' ],
	idProperty	: 'zone',
	identifier	: 'sequential',
	fields		: [
		{ name: 'zone',			type: 'int', 	critical: true },
		{ name: 'name',			type: 'string', critical: true },
		{ name: 'minCashCover',	type: 'int',	critical: true },
		{ name: 'currency', 	type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } },
		{ name: 'defFranchise',	type: 'number',	critical: true }
	]
});
