Ext.define( 'CFJS.plugins.insurance.baggage.model.Policy', {
	extend		: 'CFJS.model.insurance.InsuranceServiceData',
	requires	: [
		'CFJS.schema.Financial',
		'CFJS.model.dictionary.Consumer'
	],
	schema		: 'financial',
	fields		: [
		//	main data
		{ name: 'name',				type: 'string',		allowBlank: false,	critical: true, defaultValue: 'Багаж' },
		//	consumers
		{ name: 'consumers',		type: 'array',	critical: true, entityName: 'CFJS.plugins.insurance.model.PolicyConsumer' },
	],
	
	checkDateTo: function(period, force) {
		var me = this, util = Ext.Date,
			service = me.getService(),
			dateFrom = service.get('dateFrom'),
			dateTo = service.get('dateTo'),
			minDate = util.add(dateFrom, util.DAY, period = (period || me.get('duration')));
		if (force || util.before(dateTo || dateFrom, minDate)) {
			service.set('dateTo', minDate);
		}
	},

	updateServiceProperties: function() {
		var me = this,
			before = Ext.Date.before,
			service = me.getService(),
			dateContract = service.get('dateContract'),
			dateFrom = service.get('dateFrom'),
			dateTo = service.get('dateTo'),
			errors = [];
		if (before(dateFrom, dateContract)) {
			console.warn(dateContract.getTime(), dateFrom.getTime());
			errors.push('Дата выезда не может быть раньше даты договора!');
		}
		if (before(dateTo, dateFrom)) {
			console.warn(dateFrom.getTime(), dateTo.getTime());
			errors.push('Дата возвращения не может быть раньше даты выезда!');
		}
		if (errors.length > 0) {
			Ext.fireEvent('infoalert', errors.join('\n'));
			return false;
		}
		return me.callParent();
	}
	
});