Ext.define('CFJS.plugins.insurance.baggage.view.CustomerModelWizard', {
	extend	: 'CFJS.plugins.insurance.baggage.view.CustomerModel',
	alias	: 'viewmodel.plugins.insurance.baggage.customer-wizard',
	mixins	: [ 'CFJS.plugins.insurance.mixin.InsuranceModel' ],
	requires: [ 'CFJS.plugins.insurance.baggage.model.Policy' ]
});
