Ext.define('CFJS.plugins.insurance.baggage.view.SettingsController', {
	extend	: 'CFJS.plugins.insurance.view.SettingsController',
	alias	: 'controller.plugins.insurance.baggage.settings',
	
	sumInsRenderer: function( value ) {
		var grid = this, vm = grid.getViewModel(), store = vm && vm.isSettings ? vm.getStore('cashCover') : null,
			record, amount, code;
		if( store && store.isStore )
		{
			record = store.findRecord('id', value);
			if(record)
				return record.get('amount');
		}
		return value;
	}

});