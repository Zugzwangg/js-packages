Ext.define('CFJS.plugins.insurance.baggage.view.SettingsModel', {
    extend		: 'CFJS.plugins.insurance.view.SettingsModel',
    requires	: [ 'CFJS.plugins.insurance.baggage.model.*' ],
    alias		: 'viewmodel.plugins.insurance.baggage.settings',
    program		: 'BAG',
    stores		: {
		durationTrip	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'durationTrip', sorters: [ 'scheme', 'min' ] },
		schemeData		: { model: 'CFJS.plugins.insurance.model.SchemeData', name: 'schemeData', sorters: [ 'id_amount', 'durationId' ] },
		volumeDiscounts	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] },
		zones			: { model: 'CFJS.plugins.insurance.baggage.model.Zone', name: 'zones' }
	}

});