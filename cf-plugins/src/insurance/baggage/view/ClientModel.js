Ext.define('CFJS.plugins.insurance.baggage.view.ClientModel', {
	extend		: 'CFJS.plugins.insurance.view.EditorViewModel',
	alias		: 'viewmodel.plugins.insurance.baggage.client',
	requires	: [ 'CFJS.plugins.insurance.baggage.model.*' ],
	mixins		: [ 'CFJS.plugins.insurance.mixin.DescriptionValue' ],
	config		: { defaultZone: 1 },
	program		: 'BAG',
	itemModel	: 'CFJS.plugins.insurance.baggage.model.Policy',
	itemName	: 'policyBaggage',
	data		: { consumersNumber: 1, tariffMsg: null },
	formulas	: {
		
		tariff: {
			bind: ['{_parentItemName_.insuranceAmount}','{_parentItemName_.duration}','{_parentItemName_.scheme}'],
			get : function(data) {
		    	var me = this,
		    		cashCover = data[0],
		    		duration = data[1],
					scheme = data[2],
					durationTrip, tariff,
					errMess = me.noDictDataMess;
				if (cashCover > 0 && duration > 0) {
					// поиск по схеме и количеству дней
					durationTrip = me.findDurationTrip(scheme, duration);
					// если не найдено - поиск по дефолтной схеме и количеству дней
					if (!durationTrip) {
						scheme = me.getDefaultScheme();
						durationTrip = me.findDurationTrip(scheme, duration);
					}
					if (!durationTrip) {
						console.warn(me.attentionMess, errMess += me.durationTripMess);
						me.set('tariffMsg', errMess += me.durationTripMess);
					}
					else {
						// поиск по схеме, количеству дней и сумме покрытия (может не быть)
						tariff = me.findTariff(durationTrip.id, cashCover);
						if (!tariff) {
							console.warn(me.attentionMess, errMess + me.noSchemeAmountMess );
							me.set('tariffMsg', errMess + me.noSchemeAmountMess + '\n' + me.forSchemeMess + scheme + me.numberOfDaysMess + duration + me.tariffMess + durationTrip.rate);
						}
						else me.set('tariffMsg', me.forSchemeMess + scheme + me.numberOfDaysMess + duration + me.amountCoverageMess + cashCover + me.tariffMess + tariff.rate );
					}
				}
				return (tariff && tariff.rate) || (durationTrip && durationTrip.rate);
			}
		},

		calcAmount	: {
			bind: ['{_parentItemName_.scheme}', '{_itemName_.consumers}', '{_parentItemName_.duration}',
					'{discount}', '{tariff}'],
			get	: function(data) {
		    	var me = this,
					scheme = data[0],
					consumers = data[1] || [],
					duration = data[2],
					discount = data[3],
					tariff = data[4],
					rate = data[3] * data[4],
					policy = me.getItem(),
					service = policy && policy.isInsuranceData && policy.getService(),
					dateFrom = service && service.isModel && service.get('dateFrom'),
					paymentAmount = 0, i = 0, calculationDetailed;
		    	if (dateFrom && rate >= 0 && consumers && consumers.length) {
		    		data = [];
		    		calculationDetailed = me.get('tariffMsg') + '\n' + me.discountForNumberMess + consumers.length + ' = ' + discount + '\n'; 
					for (; i < consumers.length; i++) {
						var consumer = Ext.clone(consumers[i]);
						data.push(consumer);
						consumer.amount = Math.round(rate * duration * 100) / 100;
						paymentAmount += consumer.amount;
					}
					calculationDetailed += me.totalAmountMess + discount + ' * ' + tariff + ' * ' + duration + ' * ' + consumers.length + ' = ' + paymentAmount + '\n';
					policy.set('consumers', data);
	    			me.set('taxAmount', paymentAmount );
	    			if(service && service.isModel)
	    			{
	    				service.set('paymentAmount', paymentAmount);
	    				service.set('calculationDetailed', calculationDetailed);
	    			}
		    	}
				return paymentAmount;
			}
		},
				
		calcQuick	: {
			bind: ['{_parentItemName_.scheme}', '{consumersNumber}', '{_parentItemName_.duration}',
					'{discount}', '{tariff}'],
		    get	: function(data) {
		    	var me = this, policy = me.getItem(),
					scheme = data[0],
					consumersNumber = data[1] || 1,
					duration = data[2],
					rate = data[3] * data[4], paymentAmount = 0, 
					service = policy && policy.isInsuranceData && policy.getService();
				if (rate && duration ) {
					paymentAmount = Math.round( rate * duration * consumersNumber * 100 ) / 100;
					me.set('taxAmount', paymentAmount);
				}
				return paymentAmount;
			}
		},
		calcFranchise	: {
			bind: ['{_parentItemName_.franchise}', '{calcQuick}','{calcAmount}'],
		    get	: function(data) {
		    	var amount = data[1] || data[2],
					franchise = data[0];
				if (franchise && amount > 0)
					return (franchise < 1) ? franchise * amount : franchise;
				return 0;
			}
		}
	},
	stores	: {
		durationTrip	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'durationTrip', sorters: [ 'scheme', 'min' ] },
		schemeData		: { model: 'CFJS.plugins.insurance.model.SchemeData', name: 'schemeData', sorters: [ 'id_amount', 'durationId' ] },
		volumeDiscounts	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] },
		zones			: { model: 'CFJS.plugins.insurance.baggage.model.Zone', name: 'zones' }
	},
				
	calcDiscount: function(scheme, consumersNumber) {
    	var me = this, discount = me.findVolumeDiscount(scheme, consumersNumber);
    	if (!discount) discount = me.findVolumeDiscount(me.getDefaultScheme(), consumersNumber);
		return (discount && discount.rate) || 1;
	},
	
	findDurationTrip: function(scheme, duration) {
		var me = this, durations = me.getStore('durationTrip'), durationTrip,
			findDuration = function(scheme, duration) {
				var durationTrip = durations.data.findBy(function(rec, id) {
					return (rec = rec.data) && rec.scheme === scheme && rec.min <= duration && rec.max >= duration;
				});
				return durationTrip;
			};
		if (durations && durations.isStore && scheme) {
			if (!durations.isLoaded() || durations.isLoading()) {
				durations.on({
					datachanged: function() {
						me.set('findDurationTrip', findDuration.apply(me, [scheme, duration]));
					},
					single: true
				});
			} else durationTrip = findDuration.call(me, scheme, duration);
			if (durationTrip && durationTrip.isModel) durationTrip = durationTrip.getData();
		}
		return durationTrip;
	},
	
	findTariff: function(durationId, cashCover) {
		var me = this, schemeData = me.getStore('schemeData'), tariff,
			findSheme = function(durationId, cashCover) {
				var tariff = schemeData.data.findBy( function(rec, id) {
					return (rec = rec.data) && rec.durationId === durationId && rec.amount === cashCover;
				});
				return tariff;
			};
		if (schemeData && schemeData.isStore) {
			if (!schemeData.isLoaded() || schemeData.isLoading()) {
				schemeData.on({
					datachanged: function() {
						me.set('findTariff', findSheme.apply(me, [durationId, cashCover]));
					},
					single: true
				});
			} else tariff = findSheme.call(me, durationId, cashCover);
			if (tariff && tariff.isModel) tariff = tariff.getData();
		}
		return tariff;
	},
	
	findVolumeDiscount: function(scheme, consumersNumber) {
		var me = this, volumeDiscounts = me.getStore('volumeDiscounts'), discount,
			findDiscounts = function(scheme, consumersNumber) {
				var discount = volumeDiscounts.data.findBy(function(rec, id) {
					return (rec = rec.data) && rec.scheme === scheme && rec.min <= consumersNumber && rec.max >= consumersNumber;
				});
				return discount;
			};
		if (volumeDiscounts && volumeDiscounts.isStore && scheme) {
			if (!volumeDiscounts.isLoaded() || volumeDiscounts.isLoading()) {
				volumeDiscounts.on({
					datachanged: function() {
						me.set('findVolumeDiscount', findDiscounts.apply(me, [scheme, consumersNumber]));
					},
					single: true
				});
			} else discount = findDiscounts.call(me, scheme, consumersNumber);
			if (discount && discount.isModel) discount = discount.getData();
		}
		return discount;
	},

//	findSupplier: function(combo, supplier) {
//		var me = this, comboStore = combo && combo.getStore(), supplierData
//			findSupplierFn = function(id) {
//				return comboStore.getById(id);
//			};
//		if (comboStore && comboStore.isStore && supplier) {
//			if (!comboStore.isLoaded() || comboStore.isLoading()) {
//				comboStore.load(function() {
//					combo.setValue(supplier);
//					supplierData = comboStore.getById(id);
//				    return supplierData;
//				});
//			} else supplierData = findSupplierFn.call(me, supplier.id);
//			if (supplierData && supplierData.isModel) supplierData = supplierData.getData();
//		}
//		return supplierData;
//	},
	
	findSupplier: function(combo, supplier) {
		var me = this, comboStore = combo && combo.getStore(), supplierData
		if (comboStore && comboStore.isStore && supplier) {
			if (!comboStore.isLoaded() || comboStore.isLoading()) {
				comboStore.load(function() {
					combo.setValue(supplier);
				    return;
				});
			} else combo.setValue(supplier);
		}
		return;
	},
	
	findZone: function(zone) {
		var zones = this.getStore('zones');
		if (zones && zones.isStore && zone) {
			zone = zones.getById(zone);
			if (zone && zone.isModel) zone = zone.getData();
		}
		return zone;
	},
	
	initInternalListeners: function(observable) {
		var me = this, listeners = me.callParent(arguments),
			companyScheme = me.getStore('companyScheme');
		listeners.bindZone = me.bind('{_parentItemName_.zone}', me.onZoneChange);
		listeners.bindConsumersNumber = me.bind(['{_itemName_.consumers}', '{consumersNumber}'], me.onConsumersNumberChange);
		listeners.bindDates = me.bind(['{_parentItemName_.dateFrom}', '{_parentItemName_.dateTo}'], me.onDatesChange);
		listeners.bindDuration = me.bind('{_parentItemName_.duration}', me.onDurationChange);
		listeners.bindSupplier = me.bind('{_parentItemName_.supplier}', me.onSupplierChange);
		return listeners;
	},

	onInitProgram: function(program) {
		var me = this, records = [],
			policy = me.getItem(),
			service = policy && policy.isInsuranceData && policy.getService(),
			program = this.get('program') || {},
			defaultZone = program && program.defaultZone,
			schemes = me.getStore('companyScheme'),
			zones = me.getStore('zones'),
			view = me.getView(),
			comboInsurer = view.down('dictionarycombo[name="insurer"]'),
			comboScheme = view && view.down('combo[name=scheme]'),			
			comboZone = view && view.down('combo[name=zone]');			
		me.callParent(arguments);
		defaultZone = me.findZone(defaultZone || me.getDefaultZone());
		if (defaultZone && defaultZone.isModel) defaultZone = defaultZone.get('zone');
		me.setDefaultZone(defaultZone);
		if (schemes && schemes.isStore && service && comboScheme) {
			comboScheme.setValue(service.get('scheme') || me.getDefaultScheme());
		}
		if (zones && zones.isStore && service && comboZone) {
			comboZone.setValue(service.get('zone') || me.getDefaultZone());
		}
		if (service && !(service.get('insuranceCurrency')||{}).code) service.set('insuranceCurrency', program.insuranceCurrency);
//		if (service && comboInsurer) comboInsurer.setValue(me.findSupplier(comboInsurer, service.get('supplier') || program.defaultInsurer));
		if (service && comboInsurer) me.findSupplier(comboInsurer, service.get('supplier') || program.defaultInsurer);
	},
	
	// подсчет дисконта за количество застрахованных 
	onConsumersNumberChange: function(data) {
		var me = this, policy = me.getItem(),
			service = policy && policy.isInsuranceData && policy.getService(),		
			number = (data[0] || []).length || data[1];
		 me.set({
			 consumersNumber: number, 
			 discount: me.calcDiscount(service && service.isModel && service.get('scheme'), number)
		});
	},
	
	onDatesChange: function(dates) {
		var me = this, util = Ext.Date,
			policy = me.getItem(), period;
			service = policy && policy.isInsuranceData && policy.getService();		
		if (dates[0] && dates[1] && service && service.isModel) {
			period = util.diffDays(dates[0], dates[1]);
			if (period !== service.get('duration')) service.set('duration', period);
		}
	},
	
	onDurationChange: function(duration) {
		var me = this, policy = me.getItem(), program = this.get('program') || {}, 
			maxPeriod = program && program.maxDuration || 0;
		if (maxPeriod && duration > maxPeriod ) {
			Ext.fireEvent('infoalert', me.maxPeriodMess);
			duration = maxPeriod;
		}
		if (policy && policy.isModel) policy.checkDateTo(duration, true);
		
		var view = me.getView(), comboInsurer = view.down('dictionarycombo[name="insurer"]');
	},
	
	onSchemeChange: function(scheme) {
		var me = this, policy = me.getItem(),
			cashCover = me.getStore('cashCover'), service, rec; 
		if (cashCover && cashCover.isStore) {
			cashCover.addFilter({ property: 'amount', operator: '>=', value: 0 });
			rec = cashCover.getAt(0);
			if (policy && policy.isModel && rec) {
				if (service = policy.isInsuranceData && policy.getService()) {
//					service.set('insuranceAmount', rec.get('amount'));
				}
			}
		}
	},
	
	onSupplierChange: function(supplier) {
		var filter = { id: 'company' };
		if (supplier && supplier.id > 0) {
			filter.filterFn = function(record) {
				return  supplier.id === (((record||{}).data||{}).company||{}).id;
			}
		}
		this.setStoreFilter('companyScheme', filter);
	},
	
	onZoneChange: function(zone) {
		var me = this, policy = me.getItem(),
			cashCover = me.getStore('cashCover'), service, zonePolicy;
		if (zone) zone = me.findZone(zone);
		if (zone && zone.isModel) zone = zone.getData();
		if (cashCover && cashCover.isStore) {
			if (zone) cashCover.addFilter({ property: 'amount', operator: '>=', value: zone.minCashCover });
			else cashCover.removeFilter('amount');
		}
		if (policy && policy.isModel && zone) {
			if (service = policy.isInsuranceData && policy.getService()) {
				if( service.get('insuranceAmount') < zone.minCashCover )
					service.set('insuranceAmount', zone.minCashCover);
				service.set('insuranceCurrency', zone.currency);
				if (zone.defFranchise && !service.get('franchise') )
					service.set('franchise', zone.defFranchise)
			}
		}
	},
	
	preparePolicyData: function(data) {
		if (data) {
			var schemes = this.getStore('schemes'),
				scheme = schemes && schemes.isStore && schemes.getById(data.scheme);
			data.number = 0;
			data.customer = (data.consumer||{}).customer;
			if (scheme && scheme.isModel) data.scheme = scheme.get('name');
		}
		return data;
	},
		
	privates	: {
		
		configItem: function(item) {
			var me = this, 
				dateFrom = Ext.Date.today(),
				program = me.get('program') || {},
				properties = {}, field, value;
			if (item) {
				if (item.isInsuranceData) return item;
				if (item.isInsurance) {
					if (!(item.get('insuranceCurrency')||{}).code) item.set('insuranceCurrency', program.insuranceCurrency);
					item.set('supplier', item.get('supplier') || program.defaultInsurer);
					item.set('dateFrom', dateFrom = item.get('dateFrom') || dateFrom);
					item.set('duration', item.get('duration') || 1);
					item.set('dateContract', dateFrom = item.get('dateFrom') || dateFrom);
					item.set('zone', item.get('zone') || Ext.clone(me.getDefaultZone()));
					item.set('scheme', item.get('scheme') || Ext.clone(me.getDefaultScheme()));
					item.set('franchise', item.get('franchise') || program.franchise);
					item.set('paymentDate', item.get('paymentData') || Ext.Date.today());
					item = item.getServiceData();
				}
			}
			return {
				type	: me.getItemModel(),
				create	: Ext.apply({
				}, item) 
			};
		}
    
	}

});
