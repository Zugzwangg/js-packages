Ext.define( 'CFJS.plugins.insurance.health.model.CountryZone', {
	extend 		: 'Ext.data.Model',
	requires	: [ 'CFJS.model.dictionary.Country' ],
	identifier	: { type: 'sequential' },
	fields		: [
		{ name: 'id',			type: 'int' },
		{ name: 'zone',			type: 'int' },
		{ name: 'country',		type: 'model', critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});