Ext.define( 'CFJS.plugins.insurance.health.model.Policy', {
	extend		: 'CFJS.model.insurance.InsuranceServiceData',
	requires	: [
		'CFJS.schema.Financial',
		'CFJS.model.dictionary.Consumer',
		'CFJS.model.dictionary.Country',
		'CFJS.model.dictionary.Currency',
		'CFJS.model.dictionary.CustomerID'
	],
	schema		: 'financial',
	fields		: [
		//	main data
		{ name: 'name',				type: 'string',	allowBlank: false,	critical: true, defaultValue: 'Медицина' },
		{ name: 'validity',			type: 'int',	allowBlank: false,	critical: true, defaultValue: 1 },
		//	country details
		{ name: 'country',			type: 'model',	entityName: 'CFJS.model.NamedModel' },
		//	consumers
		{ name: 'consumers',		type: 'array',	critical: true, entityName: 'CFJS.plugins.insurance.model.PolicyConsumer' },
		//	other details
		{ name: 'conditions',		type: 'array',	item: 'CFJS.plugins.insurance.model.Condition' },
		{ name: 'addConditions',	type: 'int'	},
	],
	
	checkDateTo: function(period, force) {
		var me = this, util = Ext.Date,
			service = me.getService(),
			dateFrom = service.get('dateFrom'),
			dateTo = service.get('dateTo'),
			minDate = util.add(dateFrom, util.DAY, period = (period || me.get('validity')));
		if (force || util.before(dateTo || dateFrom, minDate)) {
			service.set('dateTo', minDate);
			me.set('duration', period);
		}
	},
	
	updateServiceProperties: function() {
		var me = this,
			before = Ext.Date.before,
			service = me.getService(),
			dateContract = service.get('dateContract'),
			dateFrom = service.get('dateFrom'),
			dateTo = service.get('dateTo'),
			errors = [];
		if (before(dateFrom, dateContract)) {
			console.warn(dateContract.getTime(), dateFrom.getTime());
			errors.push('Дата выезда не может быть раньше даты договора!');
		}
		if (before(dateTo, dateFrom)) {
			console.warn(dateFrom.getTime(), dateTo.getTime());
			errors.push('Дата возвращения не может быть раньше даты выезда!');
		}
		if (errors.length > 0) {
			Ext.fireEvent('infoalert', errors.join('\n'));
			return false;
		}
		return me.callParent();
	}
	
});