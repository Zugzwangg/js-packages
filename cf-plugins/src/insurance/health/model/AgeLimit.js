Ext.define( 'CFJS.plugins.insurance.health.model.AgeLimit', {
	extend: 'CFJS.plugins.insurance.model.SchemeDetail',
	fields: [
		{ name: 'zone',		type: 'int' },
		{ name: 'min',		type: 'int' },
		{ name: 'max',		type: 'int',	defaultValue: 400 },
		{ name: 'rate',		type: 'number', defaultValue: 1 },
		{ name: 'title',	type: 'string', calculate: function(data) { return data.min + ' - ' + data.max; } }
	]
});