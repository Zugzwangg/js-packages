Ext.define( 'CFJS.plugins.insurance.health.model.Multivisa', {
	extend 		: 'Ext.data.Model',
	identifier	: 'sequential',
	fields		: [{ name: "id", type: 'int' }, { name: 'validity', type: 'int' },{ name: 'duration', type: 'int' }],
	proxy		: 'memory'
});