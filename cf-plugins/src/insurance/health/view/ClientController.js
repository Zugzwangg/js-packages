Ext.define('CFJS.plugins.insurance.health.view.ClientController', {
	extend: 'CFJS.plugins.insurance.view.EditorController',
	alias: 'controller.plugins.insurance.health.client',
	
	onReconfigure: function(program) {
		var me = this, view = me.view,
			vm = view && view.lookupViewModel(),
			multivisa = vm.get('multivisa'), menu = [], data;
		if (multivisa && multivisa.isStore && Ext.isFunction(view.setMultivisaMenu)) {
			multivisa.each(function(record) {
				data = record.data;
				menu.push({
					handler		: 'onMultivisaClick',
					multivisa	: record,
					scope		: me,
					text		: [data.duration, data.validity].join(' / ')
				});
			});
			view.setMultivisaMenu(menu);
		}
	},
	
	onMultivisaClick: function(component) {
		var multivisa = component && component.multivisa,
			vm = this.getViewModel(),
			policy = vm && vm.getItem();
		if (policy && multivisa) {
			policy.set('validity', multivisa.get('validity'));
			Ext.TaskManager.start({
				run			: function() { policy.set('duration', multivisa.get('duration')); },
				fireOnStart	: false,
				interval	: 50,
				repeat		: 1
			});
		}
	}

});
