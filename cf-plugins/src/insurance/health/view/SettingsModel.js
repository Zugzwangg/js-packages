Ext.define('CFJS.plugins.insurance.health.view.SettingsModel', {
    extend		: 'CFJS.plugins.insurance.view.SettingsModel',
    requires	: [
        'CFJS.plugins.insurance.health.model.*',
        'CFJS.store.dictionary.Countries',
		'CFJS.store.dictionary.Currencies'
    ],
    alias		: 'viewmodel.plugins.insurance.health.settings',
    program		: 'HI',
    stores		: {
    	ageLimits		: { model: 'CFJS.plugins.insurance.health.model.AgeLimit', name: 'ageLimits' },
		conditions		: { model: 'CFJS.plugins.insurance.model.Condition', name: 'conditions', sorters: ['name'] },
		countryZone		: { model: 'CFJS.plugins.insurance.health.model.CountryZone', name: 'countryZone' },
		durationTrip	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'durationTrip', sorters: [ 'scheme', 'min' ] },
		multivisa		: { model: 'CFJS.plugins.insurance.health.model.Multivisa', name: 'multivisa', sorters: [ 'duration', 'validity' ] },
		schemeConditions: { model: 'CFJS.plugins.insurance.model.SchemeCondition', name: 'schemeConditions' },
		schemeData		: { model: 'CFJS.plugins.insurance.model.SchemeData', name: 'schemeData', sorters: [ 'durationId', 'amount' ] },
		volumeDiscounts	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] },
		zones			: { model: 'CFJS.plugins.insurance.health.model.Zone', name: 'zones' }
    }
    
});