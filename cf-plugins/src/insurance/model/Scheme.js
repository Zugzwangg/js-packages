Ext.define( 'CFJS.plugins.insurance.model.Scheme', {
	extend 		: 'Ext.data.Model',
	idProperty	: 'code',
	identifier	: { type: 'sequential', prefix: 'SCH_' },
	fields		: [
		{ name: 'code',			type: 'string' },
		{ name: 'name',			type: 'string' },
		{ name: 'dateBegin',	type: 'date' },
		{ name: 'dateEnd',		type: 'date' }
	],
	proxy		: 'memory'
});