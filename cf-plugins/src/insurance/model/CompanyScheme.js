Ext.define( 'CFJS.plugins.insurance.model.CompanyScheme', {
	extend 		: 'Ext.data.Model',
	requires	: [ 'CFJS.model.dictionary.Company' ],
	identifier	: { type: 'sequential' },
	fields		: [
		{ name: 'id',			type: 'int' },
		{ name: 'company',		type: 'model', entityName: 'CFJS.model.NamedModel' },
		{ name: 'scheme',		type: 'string' }
	]
});