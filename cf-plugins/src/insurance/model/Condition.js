Ext.define( 'CFJS.plugins.insurance.model.Condition', {
	extend		: 'Ext.data.Model',
	identifier	: 'sequential',
	fields		: [
		{ name: 'id',		type: 'int' },
		{ name: 'name',		type: 'string' },
		{ name: 'rate',		type: 'number' },
		{ name: 'amount',	type: 'number' }
	],
	proxy		: 'memory'
});