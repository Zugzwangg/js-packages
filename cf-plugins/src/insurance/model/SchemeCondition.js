Ext.define( 'CFJS.plugins.insurance.model.SchemeCondition', {
	extend	: 'CFJS.plugins.insurance.model.SchemeDetail',
	fields	: [{ name: 'conditionId', type: 'int' }],
	proxy	: 'memory'
});