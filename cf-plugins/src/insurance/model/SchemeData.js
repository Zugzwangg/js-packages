Ext.define( 'CFJS.plugins.insurance.model.SchemeData', {
	extend 		: 'Ext.data.Model',
	identifier	: 'sequential',
	fields		: [
		{ name: 'id',			type: 'int' },
		{ name: 'durationId',	type: 'int' },
		{ name: 'amount',		type: 'number' },
		{ name: 'rate',			type: 'number' }
	],
	proxy		: 'memory'
});