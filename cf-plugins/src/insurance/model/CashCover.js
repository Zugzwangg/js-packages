Ext.define('CFJS.plugins.insurance.model.CashCover', {
	extend 		: 'Ext.data.Model',
	identifier	: 'sequential',
	fields		: [{ name: 'id', type: 'int' }, { name: 'amount', type: 'number' }],
	proxy		: 'memory'
});