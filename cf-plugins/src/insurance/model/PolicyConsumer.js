Ext.define('CFJS.plugins.insurance.model.PolicyConsumer', {
    extend			: 'CFJS.model.dictionary.Consumer',
    dictionaryType	: 'consumer',
    fields			: [{ name: 'amount', type: 'number' }]

}, function() {
	Ext.data.schema.Schema.get('financial').addEntity(Ext.ClassManager.get(this.$className));
});
