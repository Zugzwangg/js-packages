Ext.define( 'CFJS.plugins.insurance.model.SchemeDetail', {
	extend 		: 'Ext.data.Model',
	identifier	: 'sequential',
	fields		: [
		{ name: 'id',		type: 'int' },
		{ name: 'scheme',	type: 'string', defaultValue: 'DEF' }
	],
	proxy		: 'memory'
});