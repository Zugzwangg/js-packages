Ext.define( 'CFJS.plugins.insurance.model.ShemeRange', {
	extend	: 'CFJS.plugins.insurance.model.SchemeDetail',
	fields	: [
		{ name: 'min',		type: 'int' },
		{ name: 'max',		type: 'int',	defaultValue: 400 },
		{ name: 'rate',		type: 'number', defaultValue: 1 },
		{ name: 'title',	type: 'string', calculate: function(data) { return data.scheme + ' (' + data.min + ' - ' + data.max + ')'; } }
	],
	proxy	: 'memory'
});