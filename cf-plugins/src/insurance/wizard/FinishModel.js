Ext.define('CFJS.plugins.insurance.wizard.FinishModel', {
	extend		: 'CFJS.app.EditorViewModel' ,
	alias		: 'viewmodel.plugins.insurance.wizard.finish',
	requires: [
		'CFJS.model.financial.Order',
		'CFJS.store.financial.Orders'
   	],

	itemName	: 'orderFinish',
	itemModel	: 'CFJS.model.financial.Order',
   	
	setItem: function(item) {
		this.linkTo(this.getItemName(), this.configItem(item));
	},
	
	defaultItemConfig: function() {
		var me = this, user = me.get('user');
		return {
			post 	: user && user.isPerson ? user.get('post') : null,
			created	: new Date()
		}
	},
	
	privates: {
		
		configItem: function(item) {
			var me = this, today = Ext.Date.today();
			if (item) return item;
			item = item || me.defaultItemConfig();
			return {
				type	: me.getItemModel(),
				create	: Ext.apply({
				}, item) 
			};
		}

	}
	
});
