Ext.define('CFJS.plugins.insurance.wizard.FinishController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.plugins.insurance.wizard.finish',
	
	initViewModel: function(vm) {
		// Add order
		vm.setItem();
	},
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},
	
	onRemoveRecord: function(component) {
		var me = this, view = this.getView(),
			grid = view ? view.lookupGrid() : null,
			store = view.lookupGridStore(),
			i = 0, selection;
		if (grid && !grid.readOnly 
				&& me.fireEvent('beforeremoverecord', selection = grid.getSelection()) !== false) {
			me.disableComponent(component, true);
			if (selection.length > 0) {
				for (; i < selection.length; i++) {
					selection[i].erase(i < selection.length - 1 || {
						callback: function(record, operation, success) {
							grid.getStore().reload();
							me.disableComponent(component);
						}
					});
				}
			}
		}
	},
	
	onPrintRecord: function(component) {
		var me = this, view = this.getView(), 
			grid = view ? view.lookupGrid() : null,
			selection = grid && grid.getSelection(),
			record = selection && selection.length > 0 && selection[0];
		if (record && record.isService) record.print(false);
	}
		
});
