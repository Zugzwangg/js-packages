Ext.define('CFJS.plugins.insurance.wizard.BrowserController', {
	extend		: 'Ext.app.ViewController',
	alias		: 'controller.plugins.insurance.wizard.browser',
	requires	: [ 'CFJS.exports.ObjToString' ],

	addInBusket: function(service) {
		var vm = this.getViewModel();
		if (vm && vm.isBrowser) vm.addInBusket(service);
		return service;
	},

	lookupService: function() {
		var me = this, childWizard = me.getView().childWizard,
			vm = childWizard && childWizard.isContainer && childWizard.lookupViewModel();
		return vm && vm.isBaseModel ? vm.getItem() : null;
	},
	
	onGotoFirstCard: function(component) {
		var me = this, view = me.getView(), vm = view.lookupViewModel();
		view.setActiveItem(vm.first());
	},

	onContinue: function(component) {
		var me = this;
		me.onGotoFirstCard();
	},
	
	onCancel: function(component) {
		var me = this, view = me.getView(), vm = view.lookupViewModel();
		vm.clearBusket();
		view.setActiveItem(vm.first());
	},

	onFinish: function(component) {
		var me = this, view = me.getView(), vm = view.lookupViewModel();
		//-----
		// Finishing
		//-----
		vm.clearBusket();
		me.onGotoFirstCard();		
	},

	onItemDblClick: function(view, record, item, index, e) {
		var me = this, view = me.getView(), vm = view.lookupViewModel(),
			childWizard = view.childWizard, steps = view.items.length,
			wizard, service;
		if (childWizard && childWizard.isContainer) {
			childWizard.removeAll();
			wizard = view.createChildWizard(record);
			if (wizard) {
				if (Ext.isString(wizard)) wizard = { xtype: wizard };
				wizard = childWizard.add(Ext.apply({
					viewModel: { program: record }
				}, wizard));
				service = childWizard.lookupViewModel().setItem({ program: record.getData(), consumer: vm.getConsumer()});
				// save service
				if (wizard.isSaveService() ) {
					// add local store
					// save to db
					me.saveService(me.addInBusket(service));
				}
				steps += wizard.getItemsCount();
			}
			view.lookupViewModel().set('steps', steps - 2);
			view.next();
		}
	},
	
	onNext: function(component) {
		var me = this, view = me.getView(),
			activeItem = view.getLayout().getActiveItem(),
			childWizard = view.childWizard, service;
		if (childWizard && childWizard.isContainer && activeItem === childWizard) {
			childWizard = childWizard.items.getAt(0);
			service = me.lookupService();
			if (childWizard.getNext()) {
				childWizard.next();
				// save service
				if( childWizard.isSaveService() ) {
					// add local store
					// save to db
					me.saveService(me.addInBusket(service));
				}
			} else {
				// next - finish card
				me.saveOrder(service);
				view.next();
			}
		}
	},
	
	onPrevious: function(component) {
		var me = this, view = me.getView(),
			activeItem = view.getLayout().getActiveItem()
			childWizard = view.childWizard;
		if (childWizard && childWizard.isContainer && activeItem === childWizard) {
			childWizard = childWizard.items.getAt(0);
			if (childWizard.getPrev()) childWizard.prev();
			else view.prev();
		}
	},
	
	saveOrder: function(service) {
		var me = this, view = me.getView(),
			master = view && view.lookupViewModel(),
			finish = view && view.finishCard,
			detail = finish && finish.lookupViewModel(),
			order = detail && detail.getItem();
		if (order && order.isModel) {
			if (order.phantom) {
				order.set({
					customer: ((master && master.getConsumer())||{}).customer,
					validUntil: new Date()
				});
			}
			if (order.phantom || order.dirty) {
				order.save({
					callback: function(record, operation, success) {
						if (success) {
							me.saveService(record.getId(), service);
						}
					}
				});
			} else {
				me.saveService(order.getId(), service);
			}
		}
	},
	
	saveService: function(order_id, service) {
		var me = this;
		service = service || me.lookupService();
		if (service && service.isModel) {
			if (order_id > 0) service.set('order', order_id);
			if (me.fireEvent('beforesaverecord', service) !== false && (service.dirty || service.phantom)) {
				service.save({
					callback: function(record, operation, success) {
			    		if (record && (taxes = service.get('taxes')) && Ext.isArray( taxes ) && taxes.length )
			    			record.set( 'amount', taxes[0].amount );
					}
				})
			}
		}
	}

});