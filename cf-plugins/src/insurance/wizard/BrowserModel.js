Ext.define('CFJS.plugins.insurance.wizard.BrowserModel', {
	extend		: 'Ext.app.ViewModel',
	alias		: 'viewmodel.plugins.insurance.wizard.browser',
	isBrowser	: true,
	data		: {
		steps			: 1,
		currentStep		: 0,
		valid 			: false,
		serviceType 	: null
	},
	
	formulas	: {
		canFinish: {
			bind: [ '{isLast}', '{isFirst}', '{busket}' ],
			get	: function(data) { return data[0] || data[1] && data[2] && data[2].getCount() > 0; }
		},
		isLast: {
			bind: [ '{steps}', '{currentStep}' ],
			get	: function(data) { return data[0] === data[1]; }
		},
		isFirst	: {
			bind: '{currentStep}',
			get	: function(currentStep) { return !currentStep; }
		},
		progress: {
			bind: [ '{steps}', '{currentStep}' ],
			get	: function(data) { return data[1]/(data[0]||1); }
		}
	},
	
	stores	: {
		busket	: { 
			model: 'CFJS.model.insurance.InsuranceService',
			proxy: 'memory'
		}
	},

	
	addInBusket: function(record) {
		var me = this, busket = me.getBusket();
		if (busket && busket.isStore && record && record.isModel) {
			busket.add(record);
		}
	},
	
	clearBusket: function(record) {
		var me = this, busket = me.getBusket();
		if (busket && busket.isStore) {
			busket.removeAll();
		}
	},
	
	getBusket: function() {
		return this.getStore('busket');
//		var store = this.getStore('busket');
//		return store;
	},
	
	getConsumer: function() {
		var busket = this.getBusket();
		return busket && busket.isStore && busket.getCount() > 0 && busket.getAt(0).get('consumer');
	},
	
	first: function() {
		return this.setCurrentStep(0);
	},

	last: function() {
		return this.setCurrentStep(this.get('steps'));
	},
	
	next: function() {
		var me = this, isLast = me.get('isLast');
		return isLast ? me.get('steps') : me.incStep(1); 
	},
	
	prev: function() {
		var me = this, isFirst = me.get('isFirst');
		return isFirst ? 0 : me.incStep(-1); 
	},
	
	incStep: function(delta) {
		return this.setCurrentStep(this.getCurrentStep() + (delta||1));
	},
	
	setCurrentStep: function(step) {
		this.set('currentStep', step);
		return this.getCurrentStep();
	},
	
	getCurrentStep: function() {
		return this.get('currentStep') || 0;
	}
	
});