Ext.define('CFJS.plugins.insurance.casualty.view.SettingsController', {
	extend	: 'CFJS.plugins.insurance.view.SettingsController',
	alias	: 'controller.plugins.insurance.casualty.settings',
	
	conditionRenderer: function( value ) {
		var grid = this, vm = grid.getViewModel(), store = vm && vm.isSettings ? vm.getStore('conditions') : null,
			record, amount, code;
		if( store && store.isStore )
		{
			record = store.findRecord('id', value);
			if(record)
				return record.get('name');
		}
		return value;
	},
	
	sumInsRenderer: function( value ) {
		var grid = this, vm = grid.getViewModel(), store = vm && vm.isSettings ? vm.getStore('cashCover') : null,
			record, amount, code;
		if( store && store.isStore )
		{
			record = store.findRecord('id', value);
			if(record)
				return record.get('amount');
		}
		return value;
	}

});