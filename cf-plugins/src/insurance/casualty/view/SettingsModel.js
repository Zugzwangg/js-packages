Ext.define('CFJS.plugins.insurance.casualty.view.SettingsModel', {
    extend		: 'CFJS.plugins.insurance.view.SettingsModel',
    requires	: [ 'CFJS.plugins.insurance.casualty.model.*' ],
    alias		: 'viewmodel.plugins.insurance.casualty.settings',
    program		: 'ACC',
    stores		: {
    	ageLimits		: { model: 'CFJS.plugins.insurance.casualty.model.AgeLimit', name: 'ageLimits' },
		conditions		: { model: 'CFJS.plugins.insurance.model.Condition', name: 'conditions', sorters: ['name'] },
		durationTrip	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'durationTrip', sorters: [ 'scheme', 'min' ] },
		schemeConditions: { model: 'CFJS.plugins.insurance.model.SchemeCondition', name: 'schemeConditions' },
		schemeData		: { model: 'CFJS.plugins.insurance.model.SchemeData', name: 'schemeData', sorters: [ 'durationId', 'amount' ] },
		volumeDiscounts	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] },
		zones			: { model: 'CFJS.plugins.insurance.casualty.model.Zone', name: 'zones' }
	}
	
});