Ext.define('CFJS.plugins.insurance.casualty.view.ClientModel', {
	extend		: 'CFJS.plugins.insurance.view.EditorViewModel',
	alias		: 'viewmodel.plugins.insurance.casualty.client',
	requires	: [ 'CFJS.plugins.insurance.casualty.model.*' ],
	mixins		: [ 'CFJS.plugins.insurance.mixin.DescriptionValue' ],
	config		: { defaultZone: 1 },
	program		: 'ACC',
	itemModel	: 'CFJS.plugins.insurance.casualty.model.Policy',
	itemName	: 'policyCasualty',
	data		: { consumersNumber: 1, averageAge: 35, conditionsArr : [], tariffMsg: null },
	formulas	: {

		conditionsRate: {
			bind: [ '{_itemName_.conditions}', '{_itemName_.addConditions}' ],
			get	: function(data) {
				var conditions = data[0],
					addConditions = data[1],
					i = 0, rate = 0, amount = 0;
				if (conditions && addConditions > 0) {
					if (!Ext.isArray(conditions)) conditions = [conditions];
					for (; i < conditions.length ; i++, addConditions = addConditions >>> 1 ) {
						rate += (addConditions & 1) * conditions[i].rate;
						amount += (addConditions & 1) * conditions[i].amount;
					}
				}
				return { rate: rate || 1, amount: amount };
			}
		},

		tariff: {
			bind: ['{_parentItemName_.insuranceAmount}','{_parentItemName_.duration}','{_parentItemName_.scheme}'],
			get : function(data) {
		    	var me = this,
		    		cashCover = data[0],
		    		duration = data[1],
					scheme = data[2],
					durationTrip, tariff,
					errMess = me.noDictDataMess;
				if (cashCover > 0 && duration > 0) {
					// поиск по схеме и количеству дней
					durationTrip = me.findDurationTrip(scheme, duration);
					// если не найдено - поиск по дефолтной схеме и количеству дней
					if (!durationTrip) {
						scheme = me.getDefaultScheme();
						durationTrip = me.findDurationTrip(scheme, duration);
					}
					if (!durationTrip) {
						console.warn(me.attentionMess, errMess += me.durationTripMess);
						me.set('tariffMsg', errMess += me.durationTripMess);
					} else {
						// поиск по схеме, количеству дней и сумме покрытия (может не быть)
						tariff = me.findTariff(durationTrip.id, cashCover);
						if (!tariff) {
							console.warn(me.attentionMess, errMess += me.noSchemeAmountMess );
							me.set('tariffMsg', errMess + me.noSchemeAmountMess + '\n' + me.forSchemeMess + scheme + me.numberOfDaysMess + duration + me.tariffMess + durationTrip.rate);
						}
						else me.set('tariffMsg', me.forSchemeMess + scheme + me.numberOfDaysMess + duration + me.amountCoverageMess + cashCover + me.tariffMess + tariff.rate );
					}
				}
				return (tariff && tariff.rate) || (durationTrip && durationTrip.rate);
			}
		},

		calcAmount	: {
			bind: ['{_parentItemName_.scheme}', '{_itemName_.consumers}', '{_parentItemName_.duration}',
					'{conditionsRate}', '{discount}', '{tariff}'],
		    get	: function(data) {
		    	var me = this,
					scheme = data[0],
					consumers = data[1] || [],
					duration = data[2],
					conditionsRate = data[3].rate,
					discount = data[4],
					tariff = data[5],
					rate = conditionsRate * discount * tariff,
					amount = data[3].amount,
					policy = me.getItem(),
					service = policy && policy.isInsuranceData && policy.getService(),
					dateFrom = service && service.isModel && service.get('dateFrom'),
					errMess = me.noDictDataMess, paymentAmount = 0, i = 0, calculationDetailed = '';
		    	if (dateFrom && rate >= 0) {
		    		data = [];
		    		calculationDetailed = me.get('tariffMsg') + '\n' + me.discountForNumberMess + consumers.length + ' = ' + discount + '\n';
		    		calculationDetailed += me.conditionsRateMess + conditionsRate + '\n';
					for (; i < consumers.length; i++) {
						var consumer = Ext.clone(consumers[i]), ageRate = 1, age, birthday = consumer.birthday;
						calculationDetailed += (i+1).toString() + me.insuredMess;
						if (birthday && !Ext.isDate(birthday))
							try {
								birthday = new Date(birthday);
							} catch(e) {
								;
							}
						age = Ext.isDate(birthday) ? Ext.Date.diff(birthday, dateFrom, Ext.Date.YEAR) : 0;
						data.push(consumer);
						if (age <= 0) {
							if (!birthday)
							{
								console.info(me.attentionMess, me.missingDateBirthMess );
								CFJS.errorAlert(me.attentionMess, me.missingDateBirthMess );
							}
							continue;
						}
						// поиск по схеме и возрасту
						ageRate = me.findAgeLimit(scheme, age);
						// если не найдено - поиск по дефолтной схеме и возрасту
						if (!ageRate) {
							scheme = me.getDefaultScheme();
							ageRate = me.findAgeLimit(scheme, age);
						}
						ageRate = (ageRate && ageRate.rate) || 0;
						if (!ageRate) {
							console.info(me.attentionMess, errMess + me.missingAgeRangesMess );
							calculationDetailed += errMess + me.missingAgeRangesMess;
						} else {
							calculationDetailed += me.forSchemeMess + scheme + me.ageMess + age + me.koeffAgeMess + ageRate + me.amountAgeMess + amount + '\n';
						}
						consumer.amount = Math.round((rate * ageRate + amount) * duration * 100) / 100;
						calculationDetailed += me.amountMess + '(' + conditionsRate + ' * ' + discount + ' * ' + tariff + ' * ' + ageRate + ' + ' + amount + ') * ' + duration + ' = ' + consumer.amount + '\n';
						paymentAmount += consumer.amount;
					}
					policy.set('consumers', data);
	    			me.set('taxAmount', paymentAmount);
	    			calculationDetailed += me.totalAmountMess + paymentAmount; 
	    			if(service && service.isModel) {
	    				service.set('paymentAmount', paymentAmount);
	    				service.set('calculationDetailed', calculationDetailed);
	    			}
		    	}
				return paymentAmount;
			}
		},
		
		calcQuick	: {
			bind: ['{_parentItemName_.scheme}', '{consumersNumber}', '{_parentItemName_.duration}',
					'{conditionsRate}', '{discount}', '{tariff}', '{averageAge}'],
		    get	: function(data) {
		    	var me = this, policy = me.getItem(),
					service = policy && policy.isInsuranceData && policy.getService(),
					scheme = data[0],
					consumersNumber = data[1] || 1,
					duration = data[2],
					rate = data[3].rate * data[4] * data[5],
					amount = data[3].amount,
					age = data[6],
					paymentAmount = 0,
					ageRate = rate >= 0 && age > 0 && me.findAgeLimit(scheme, age); 
				if (!ageRate) ageRate = me.findAgeLimit(me.getDefaultScheme(), age);
				if (ageRate && ageRate.rate > 0) {
					paymentAmount = Math.round( (rate * ageRate.rate + amount) * duration * consumersNumber * 100 ) / 100;
	    			me.set('taxAmount', paymentAmount );
				}
				return paymentAmount;
			}
		}
	},
	stores	: {
		ageLimits		: { model: 'CFJS.plugins.insurance.casualty.model.AgeLimit', name: 'ageLimits' },
		conditions		: { model: 'CFJS.plugins.insurance.model.Condition', name: 'conditions', sorters	: ['name'] },
		durationTrip	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'durationTrip', sorters: [ 'scheme', 'min' ] },
		schemeConditions: { model: 'CFJS.plugins.insurance.model.SchemeCondition', name: 'schemeConditions' },
		schemeData		: { model: 'CFJS.plugins.insurance.model.SchemeData', name: 'schemeData', sorters: [ 'id_amount', 'durationId' ] },
		volumeDiscounts	: { model: 'CFJS.plugins.insurance.model.ShemeRange', name: 'volumeDiscounts', sorters: [ 'scheme', 'min' ] },
		zones			: { model: 'CFJS.plugins.insurance.casualty.model.Zone', name: 'zones' }
	},

	calcDiscount: function(scheme, consumersNumber) {
    	var me = this, discount = me.findVolumeDiscount(scheme, consumersNumber);
    	if (!discount) discount = me.findVolumeDiscount(me.getDefaultScheme(), consumersNumber);
		return (discount && discount.rate) || 1;
	},
	
	findAgeLimit: function(scheme, age) {
		var ageLimits = this.getStore('ageLimits'), ageLimit;
		if (ageLimits && ageLimits.isStore && scheme) {
			ageLimit = ageLimits.data.findBy(function(rec, id) {
				return (rec = rec.data) && rec.scheme === scheme && rec.min <= age && rec.max >= age;
			});
			if (ageLimit && ageLimit.isModel) ageLimit = ageLimit.getData();
		}
		return ageLimit;
	},
	
	findDurationTrip: function(scheme, duration) {
		var me = this, durations = me.getStore('durationTrip'), durationTrip,
			findDuration = function(scheme, duration) {
				var durationTrip = durations.data.findBy(function(rec, id) {
					return (rec = rec.data) && rec.scheme === scheme && rec.min <= duration && rec.max >= duration;
				});
				return durationTrip;
			};
		if (durations && durations.isStore && scheme) {
			if (!durations.isLoaded() || durations.isLoading()) {
				durations.on({
					datachanged: function() {
						me.set('findDurationTrip', findDuration.apply(me, [scheme, duration]));
					},
					single: true
				});
			} else durationTrip = findDuration.call(me, scheme, duration);
			if (durationTrip && durationTrip.isModel) durationTrip = durationTrip.getData();
		}
		return durationTrip;
	},
	
	findTariff: function(durationId, cashCover) {
		var me = this, schemeData = me.getStore('schemeData'), tariff,
			findSheme = function(durationId, cashCover) {
				var tariff = schemeData.data.findBy( function(rec, id) {
					return (rec = rec.data) && rec.durationId === durationId && rec.amount === cashCover;
				});
				return tariff;
			};
		if (schemeData && schemeData.isStore) {
			if (!schemeData.isLoaded() || schemeData.isLoading()) {
				schemeData.on({
					datachanged: function() {
						me.set('findTariff', findSheme.apply(me, [durationId, cashCover]));
					},
					single: true
				});
			} else tariff = findSheme.call(me, durationId, cashCover);
			if (tariff && tariff.isModel) tariff = tariff.getData();
		}
		return tariff;
	},
	
	findVolumeDiscount: function(scheme, consumersNumber) {
		var me = this, volumeDiscounts = this.getStore('volumeDiscounts'), discount,
			findDiscounts = function(scheme, consumersNumber) {
				var discount = volumeDiscounts.data.findBy(function(rec, id) {
					return (rec = rec.data) && rec.scheme === scheme && rec.min <= consumersNumber && rec.max >= consumersNumber;
				});
				return discount;
			};
		if (volumeDiscounts && volumeDiscounts.isStore && scheme) {
			if (!volumeDiscounts.isLoaded() || volumeDiscounts.isLoading()) {
				volumeDiscounts.on({
					datachanged: function() {
						me.set('findVolumeDiscount', findDiscounts.apply(me, [scheme, consumersNumber]));
					},
					single: true
				});
			} else discount = findDiscounts.call(me, scheme, consumersNumber);
			if (discount && discount.isModel) discount = discount.getData();
		}
		return discount;
	},
	
	findSupplier: function(combo, supplier) {
		var me = this, comboStore = combo && combo.getStore(), supplierData
		if (comboStore && comboStore.isStore && supplier) {
			if (!comboStore.isLoaded() || comboStore.isLoading()) {
				comboStore.load(function() {
					combo.setValue(supplier);
				    return;
				});
			} else combo.setValue(supplier);
		}
		return;
	},
	
	findZone: function(zone) {
		var zones = this.getStore('zones');
		if (zones && zones.isStore && zone) {
			zone = zones.getById(zone);
			if (zone && zone.isModel) zone = zone.getData();
		}
		return zone;
	},
	
	initInternalListeners: function(observable) {
		var me = this, listeners = me.callParent(arguments),
			companyScheme = me.getStore('companyScheme');
		listeners.bindZone = me.bind('{_parentItemName_.zone}', me.onZoneChange);
		listeners.bindConsumersNumber = me.bind(['{_itemName_.consumers}', '{consumersNumber}'], me.onConsumersNumberChange);
		listeners.bindDates = me.bind(['{_parentItemName_.dateFrom}', '{_parentItemName_.dateTo}'], me.onDatesChange);
		listeners.bindDuration = me.bind('{_parentItemName_.duration}', me.onDurationChange);
		listeners.bindSupplier = me.bind('{_parentItemName_.supplier}', me.onSupplierChange);
		return listeners;
	},
	
	onInitProgram: function(program) {
		var me = this, records = [], condName = [],
			policy = me.getItem(),
			service = policy && policy.isInsuranceData && policy.getService(),
			zones = me.getStore('zones'),
			schemes = me.getStore('companyScheme'),
			conditions = me.getStore('conditions'),
			program = this.get('program') || {},
			defaultZone = program && program.defaultZone,
			view = me.getView(),
			comboInsurer = view.down('dictionarycombo[name="insurer"]'),
			comboScheme = view && view.down('combo[name=scheme]'),			
			comboZone = view && view.down('combo[name=zone]'),
			manyCheck = view && view.down('checkboxgroupfield');
		me.callParent(arguments);
		if (zones && zones.isStore) {
			defaultZone = zones.getById(defaultZone || me.getDefaultZone());
			if (defaultZone && defaultZone.isModel) defaultZone = defaultZone.get('zone');
		}
		me.setDefaultZone(defaultZone);
		if (schemes && schemes.isStore && service && comboScheme) {
			comboScheme.setValue(service.get('scheme') || me.getDefaultScheme());
		}
		if (zones && zones.isStore && service && comboZone) {
			comboZone.setValue(service.get('zone') || me.getDefaultZone());
		}
		if (service && !(service.get('insuranceCurrency')||{}).code) service.set('insuranceCurrency', program.insuranceCurrency);
		if (policy && policy.isInsuranceData && !policy.phantom) {
			// display saved data
			records = policy.get('conditions');
			policy.set('conditions', records);
			policy.set('addConditions', policy.get('addConditions'));
			records.forEach(function(record) {
				condName.push( record.name );
			});
			me.set( 'conditionsArr', condName );
			if (manyCheck) {
				manyCheck.setFormat(condName);
				manyCheck.setValue(policy.get('addConditions'));
			}
		} else {
			if (conditions && conditions.isStore ) {
				records = [];
				conditions.each(function(record) {
					record = record.getData();
					condName.push( record.name );
					records.push( record );
				});
				me.set( 'conditionsArr', condName );
				if (policy && policy.isInsuranceData)
				{
					policy.set('conditions', records);
					policy.set('addConditions', policy.get('addConditions'));
					if (manyCheck) {
						manyCheck.setFormat(policy.get('conditions'));
						manyCheck.setValue(policy.get('addConditions'));
					}
				}
			}
		}
		if (service && comboInsurer) me.findSupplier(comboInsurer, service.get('supplier') || program.defaultInsurer);
	},
	
	onConsumersNumberChange: function(data) {
		var me = this, policy = me.getItem(),
			service = policy && policy.isInsuranceData && policy.getService(),
			number = (data[0] || []).length || data[1];
		 me.set({
			 consumersNumber: number, 
			 discount: me.calcDiscount(service && service.isModel && service.get('scheme'), number)
		});
	},
	
	onDatesChange: function(dates) {
		var me = this, util = Ext.Date,
			policy = me.getItem(), period,
			service = policy && policy.isInsuranceData && policy.getService();
		if (dates[0] && dates[1] && service && service.isModel) {
			period = util.diffDays(dates[0], dates[1]);
			if (period !== service.get('duration')) service.set('duration', period);
		}
	},
	
	onDurationChange: function(duration) {
		var policy = this.getItem();
		if (policy && policy.isModel) policy.checkDateTo(duration, true);
	},
	
	onSchemeChange: function(scheme) {
		var me = this, policy = me.getItem(),
		cashCover = me.getStore('cashCover'), service, rec;
		if (policy && policy.isModel && scheme) policy.set('conditions', me.schemeConditions(scheme));
		if (cashCover && cashCover.isStore) {
			cashCover.addFilter({ property: 'amount', operator: '>=', value: 0 });
			rec = cashCover.getAt(0);
			if (policy && policy.isModel && rec) {
				if (service = policy.isInsuranceData && policy.getService()) {
					service.set('insuranceAmount', rec.get('amount'));
				}
			}
		}
	},
	
	onSupplierChange: function(supplier) {
		var filter = { id: 'company' };
		if (supplier && supplier.id > 0) {
			filter.filterFn = function(record) {
				return  supplier.id === (((record||{}).data||{}).company||{}).id;
			}
		}
		this.setStoreFilter('companyScheme', filter);
	},

	onZoneChange: function(zone) {
		var me = this, policy = me.getItem(),
			cashCover = me.getStore('cashCover'), service,
			zones = me.getStore('zones');
		if (zones && zones.isStore) zone = zones.getById(zone);
		if (zone && zone.isModel) zone = zone.getData();
		if (cashCover && cashCover.isStore) {
			if (zone) cashCover.addFilter({ property: 'amount', operator: '>=', value: zone.minCashCover });
			else cashCover.removeFilter('amount');
		}
		if (policy && policy.isModel && zone) {
			if (service = policy.isInsuranceData && policy.getService()) {
				service.set('insuranceAmount', zone.minCashCover);
				service.set('insuranceCurrency', zone.currency);
			}
		}
	},

	preparePolicyData: function(data) {
		if (data) {
			var schemes = this.getStore('schemes'),
				scheme = schemes && schemes.isStore && schemes.getById(data.scheme),
				addConditions = [], i = 0;
			data.number = 0;
			data.customer = (data.consumer||{}).customer;
			if (scheme && scheme.isModel) data.scheme = scheme.get('name');
			if (data.conditions && data.addConditions > 0) {
				if (!Ext.isArray(data.conditions)) data.conditions = [data.conditions];
				for (; i < data.conditions.length ; i++, data.addConditions = data.addConditions >>> 1 ) {
					if (data.addConditions & 1) addConditions.push(data.conditions[i].name);
				}
			}
			data.addConditions = addConditions.join(', ');
		}
		return data;
	},
		
	schemeConditions: function(scheme) {
		var me = this, conditions = me.getStore('conditions'),
			schemeConditions = me.getStore('schemeConditions'), ret = [];
		if (schemeConditions && schemeConditions.isStore && conditions && conditions.isStore) {
			schemeConditions.each(function(cond) {
				if (cond.get('scheme') === scheme && (cond = conditions.getById(cond.get('conditionId')))) ret.push(cond.getData());
			});
		}
		return ret;
	},

	privates	: {
		
		configItem: function(item) {
			var me = this, 
				dateFrom = Ext.Date.today(),
				program = me.get('program') || {},
				properties = {}, field, value, conditions = [];
			if (item) {
				if (item.isInsuranceData) return item;
				if (item.isInsurance) {
					if (!(item.get('insuranceCurrency')||{}).code) item.set('insuranceCurrency', program.insuranceCurrency);
					item.set('supplier', item.get('supplier') || program.defaultInsurer);
					item.set('dateFrom', dateFrom = item.get('dateFrom') || dateFrom);
					item.set('duration', item.get('duration') || 1);
					item.set('dateContract', dateFrom = item.get('dateFrom') || dateFrom);
					item.set('zone', item.get('zone') || Ext.clone(me.getDefaultZone()));
					item.set('scheme', item.get('scheme') || Ext.clone(me.getDefaultScheme()));
					item.set('franchise', item.get('franchise') || program.franchise);
					item.set('paymentDate', item.get('paymentData') || Ext.Date.today());
					item = item.getServiceData();
				}
			}
			return {
				type	: me.getItemModel(),
				create	: Ext.apply({
					conditions	: conditions
				}, item) 
			};
		}
    
	}

});
