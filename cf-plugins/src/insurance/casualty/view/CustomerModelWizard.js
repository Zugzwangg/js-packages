Ext.define('CFJS.plugins.insurance.casualty.view.CustomerModelWizard', {
	extend	: 'CFJS.plugins.insurance.casualty.view.CustomerModel',
	alias	: 'viewmodel.plugins.insurance.casualty.customer-wizard',
	mixins	: [ 'CFJS.plugins.insurance.mixin.InsuranceModel' ],
	requires	: [ 'CFJS.plugins.insurance.casualty.model.Policy' ]

});
