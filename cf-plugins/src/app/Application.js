Ext.define('CFJS.plugins.app.Application', {
	extend				: 'CFJS.app.Application',
	alternateClassName	: 'CFJS.plugins.Application',

	afterUpdateSession: function(api, session) {
		var me = this, viewport = me.getViewport();
		if (!viewport.isComponent) me.setViewport(Ext.create(viewport));
		Ext.app.route.Router.onStateChange(Ext.util.History.getToken());
    },

	onLoadManifest: function(response, application) {
		var me = application || this.getApplication(), api = me.getApi(),
			session = api.getSessionFromCookies(),
			pureUrl = location.protocol + "//" + location.host + location.pathname + location.hash,
			failure = function(responseData) { me.onErrorRequest(responseData, me); };
		api.updateSessionData({
			session		: api.isSessionExpired(session) ? api.getSessionFromUrl() : session,
			saveCookie	: me.getSaveSession(),
			callback	: {
				success: function(session) {
					Ext.getBody().removeChild('load-screen');
					window.history.replaceState({}, document.title, pureUrl);
					if (api.isSessionExpired()) {
						session = Ext.applyIf({ expires: Ext.Date.add(new Date(), Ext.Date.DAY, 1).toString(), uid: "1", utype: 'unknown' }, session);
						api.setSecure(1);
						api.setSession(session);
						if (me.getSaveSession()) api.saveSessionToCookies(session);
					}
					me.afterUpdateSession(api, session);
				},
				failure: failure
			}
		});
	}	
});
