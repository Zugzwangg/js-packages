Ext.define('CFJS.plugins.app.SettingsController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.plugins.settings',
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},

	doSelectionChange: function(selectable, selection, actions) {
		var me = this;
		me.disableComponent(actions.copyRecord, !selection || selection.length !== 1);
		me.disableComponent(actions.editRecord, !selection || selection.length !== 1);
		me.disableComponent(actions.removeRecord, !selection || selection.length < 1);
	},

	lookupActiveTab: function() {
		var view = this.getView();
		return view && Ext.isFunction(view.getActiveTab) ? view.getActiveTab() : null; 
	},

	lookupActiveStore: function() {
		return this.lookupTabStore(this.lookupActiveTab());
	},
	
	lookupTabStore: function(tab) {
		return tab && tab.isSettingsGrid ? tab.getStore() : null;
	},
	
	initActionable: function(actionable) {
		if (actionable && actionable.hasActions) {
			var me = this, tab = me.lookupActiveTab();
			me.onSelectionChange(tab, tab.getSelection());
		}
	},

	onAddRecord: function(component) {
		var tab = this.lookupActiveTab(),
			store = this.lookupTabStore(tab),
			editor, record;
		if (store && store.isStore && !tab.readOnly) {
			record = store.add({})[0];
			editor = tab.findPlugin('rowediting')
			if (record.isModel && editor) editor.startEdit(record);
		}
	},
	
	onClearFilters: function(component) {
		var tab = this.lookupActiveTab();
		if (tab && Ext.isFunction(tab.onClearFilters)) tab.onClearFilters();
	},
	
	onCopyRecord: function(component) {
		var tab = this.lookupActiveTab(),
			store = this.lookupTabStore(tab),
			Model = store && store.isStore ? store.getModel() : null,
			editor, records;
		if (Model && !tab.readOnly && (records = tab.getSelection()) && records.length > 0) {
			records = Ext.clone(records[0].getData());
			delete records[Model.idProperty];
			records = store.add(records)[0];
			editor = tab.findPlugin('rowediting')
			if (records.isModel && editor) editor.startEdit(records);
		}
	},
	
	onReloadStore: function(component) {
		var store = this.lookupActiveStore();
		if (store && store.isStore) store.reload();
	},
	
	onRemoveRecords: function(component) {
		var me = this, 
			tab = me.lookupActiveTab(),
			store = me.lookupTabStore(tab),
			i = 0, records;
		if (store && store.isStore && !tab.readOnly
				&& me.fireEvent('beforeremoverecord', records = tab.getSelection()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].erase(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},

	onSaveRecords: function(component, force) {
		var me = this,
			tab = me.lookupActiveTab(),
			store = me.lookupTabStore(tab),
			i = 0, records;
		if (store && !tab.readOnly
				&& me.fireEvent('beforesaverecord', records = store.getModifiedRecords()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].save(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		var me = this, actionable = me.getView().getActionable(),
			actions = actionable && actionable.hasActions && actionable.actions;
		if (actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.doSelectionChange(selectable, selection, actions);
		}
	}
	
});