Ext.define('CFJS.plugins.cellstore.view.RentsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.plugins.cellstore.rents',
	requires: [ 'CFJS.store.cellstore.Rents' ],
	itemName: 'editRent',
	stores	: {
		rents: {
			type		: 'cell_rents',
			autoLoad	: true,
			session		: { schema: 'cellstore' },
			filters		: [{
				id		: 'startDate',
				property: 'startDate',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			},{
				id		: 'endDate',
				property: 'endDate',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			}],
			sorters		: [ 'endDate' ],
			summaries	: [{ property: 'id' }, { property: 'amount', type: 'sum' }],
			listeners	: { summary: 'onLoadSummary' }
		}
	},

	configService: function(item, callback) {
		if (item && item.data) {
			var serviceType = Ext.getStore('serviceTypes').findModel(item.get('serviceType'));
			item = Ext.create(serviceType, item.data);
			item.load({ callback: callback });
		}
		return item;
	}
	
});