Ext.define('CFJS.plugins.cellstore.view.RentWizardController', {
	extend		: 'Ext.app.ViewController',
	alias		: 'controller.plugins.cellstore.rentwizard',
	id			: 'cellstore-rentwizard',
    
	init: function(view) {
		var vm = view.lookupViewModel(), items = view.items, stepEnd = 0;
		if (vm) {
			vm.set('wizardType', view.wizardType);
			if (Ext.isArray(items)) stepEnd = items.length;
			else if (items.isMixedCollection) stepEnd = items.getCount();
			if (stepEnd > 0) stepEnd--;
			vm.set('stepEnd', stepEnd);
		}
	},
	
	onCellsSelectionChange: function(selModel, selection) {
		var grid = selModel.view.grid,
			editor = grid.up('form[name="rentEditor"]'),
			vm = grid.lookupViewModel(),
			fieldName = grid.name, freeFrom;
		if (Ext.isArray(selection)) selection = selection[0];
		if (selection && selection.isModel) selection = selection.getData();
		if (vm && vm.isBaseModel) {
			vm.getItem().set(fieldName, selection);
			if (Ext.isFunction(vm.minStartDate) && editor
					&& (editor = editor.down('datefield[name="startDate"]')) && editor.isFormField) {
				editor.setMinValue(vm.minStartDate());
				editor.validate();
			}
		}
	},
	
	onFinish: function(button) {
		var me = this, view = me.getView(),
			rent = view.getLayout().getActiveItem().lookupViewModel().getItem(),
			cell = rent && rent.isModel ? rent.get('cell') : null;
		if (!cell || cell.id <= 0) {
			CFJS.errorAlert('Error', 'You must select one cell');
			return;
		}
		view.mask('Processing');
		rent.save({ callback: me.afterSaveRent, scope: me });
	},
	
	afterSaveAppendix: function(appendix, operation, success) {
		var me = this, view = me.getView(),
			fields = ((view.wizardConfig||{}).appendix||{}).fields,
			description, document;
		if (success && appendix && appendix.id > 0) {
			description = appendix.get('form').name;
			if (fields) description += ' #' + appendix.getPropertyValue(fields['number']);
			document = new CFJS.model.cellstore.AuthorizedDocument({
				type		: 'DOCUMENT',
				link		: appendix.id,
				description	: description,
				startDate	: appendix.get('startDate'),
				endDate		: appendix.get('endDate')
			});
		}
		if (document && document.isModel) document.save({ callback: me.afterSaveAuthorizedDocument, scope: me });
		else view.unmask();
	},
	
	afterSaveAuthorizedDocument: function(document, operation, success) {
		var me = this, view = me.getView(),
			rent = view.getLayout().getActiveItem().lookupViewModel().getItem(),
			customer = rent && rent.id > 0 ? rent.get('customer') : null,
			person;
		if (success && document && document.id > 0 && customer && customer.id > 0) {
			person = new CFJS.model.cellstore.AuthorizedPerson({
				document	: document.getData(),
				customer	: customer,
				pin			: '0000',
				rent		: { id: rent.id }
			});
		}
		if (person && person.isModel) {
			person.save({
				callback: function(record, operation, success) {
					view.unmask();
					if (success) view.fireEvent('wizardfinish', view);
				}
			});
			return;
		}
		view.unmask();
		if (view.wizardConfig.customerType !== 'customer') view.fireEvent('wizardfinish', view);
	},
	
	afterSaveRent: function(rent, operation, success) {
		var me = this, view = me.getView(), 
			appendix = (view.wizardConfig || {}).appendix,
			fields = (appendix || {}).fields,
			vm = me.getViewModel(),
			rentDocument = (vm.get('rentDocument') || {}).id,
			cell = success && rent && rent.id > 0 ? rent.get('cell') : null,
			document, cell, microCell, startDate;
		if (cell && cell.id > 0 && rentDocument > 0 && fields && appendix.form > 0) {
			startDate = rent.get('startDate');
			document = new CFJS.model.document.Document({
				author 		: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
				form		: { id: appendix.form },
				unit		: vm.get('userUnit'),
				startDate	: startDate,
				endDate		: rent.get('endDate'),
				created		: startDate
			});
			vm = view.getLayout().getActiveItem().lookupViewModel();
			document.setProperty(fields['number'], 		{ '@type':'long', $: 0 });
			document.setProperty(fields['date'], 		{ '@type':'date', $: Ext.Date.format(startDate, CFJS.Api.dateTimeFormat)});
			document.setProperty(fields['contract'],	{ '@type':'named_entity', id: rentDocument });
			document.setProperty(fields['cellNumber'], 	{ '@type':'string', $: cell.number });
			document.setProperty(fields['cellPrice'], 	{ '@type':'double', $: vm.get('cellAmount') });
			document.setProperty(fields['cellSize'], 	{ '@type':'string', $: cell.size.name });
			document.setProperty(fields['amount'],		{ '@type':'double', $: rent.get('amount') });
			cell = rent.get('microCell');
			if (cell && cell.id > 0) {
				document.setProperty(fields['microCellPrice'],	{ '@type':'double', $: vm.get('microCellAmount') });
				document.setProperty(fields['microCellSize'],	{ '@type':'string', $: cell.size.name });
			}
		}
		if (document && document.isModel) document.save({ params: { locale: CFJS.getLocale() }, callback: me.afterSaveAppendix, scope: me });
		else view.unmask();
	},
	
	onNavigate: function(button) {
		var me = this, view = me.getView(),
			wizardConfig = view.wizardConfig,
			vm = me.getViewModel(),
			layout = view.getLayout(),
			activeItem = layout.getActiveItem(), 
			activeIndex = button.direction + view.items.indexOf(activeItem),
			errorMessage, selection, store, rentOwner;
		switch (activeIndex) {
			case 'next0':
				if ((selection = activeItem.lookupGrid().getSelection()) 
						&& selection.length === 1) {
					vm.set('rentOwner', rentOwner = selection[0]);
				} else errorMessage = 'You must select one entry';
				break;
			case 'next1':
				if ((selection = activeItem.lookupGrid().getSelection()) 
						&& selection.length === 1) {
					vm.set('rentDocument', selection[0]);
				} else errorMessage = 'You must select one entry';
				break;
			default:
				break;
		}
		if (!Ext.isEmpty(errorMessage)) CFJS.errorAlert('Error', errorMessage);
		else {
			layout[button.direction]();
			vm.set('stepActive', view.items.indexOf(activeItem = layout.getActiveItem()));
			activeItem.focus();
			switch (activeIndex) {
				case 'next0':
					store = activeItem.lookupGrid().getStore();
					store.getFilters().beginUpdate();
					// TODO Add form configuration
					if (wizardConfig.id === 'company') {
						me.addRentOwnerFilter(store, 'Name', '3', rentOwner.get('name'))
					} else if (wizardConfig.id) {
						me.addRentOwnerFilter(store, 'LastName', '3', rentOwner.get('lastName'))
						me.addRentOwnerFilter(store, 'Name', '3', rentOwner.get('name'))
						me.addRentOwnerFilter(store, 'MiddleName', '3', rentOwner.get('middleName'))
					}
					store.getFilters().endUpdate();
					break;
				case 'next1':
					rentOwner = vm.get('rentOwner');
					selection = activeItem.lookupViewModel().setItem();
					selection.set(wizardConfig.customerType, rentOwner && rentOwner.isModel ? rentOwner.getData() : rentOwner);
					break;
				default:
					break;
			}

		}
	},
	
	addRentOwnerFilter: function(store, fieldKey, fieldProperty, value) {
		if (Ext.isString(value) && !Ext.isEmpty(value)) store.addFilter({
			id			: 'byOwner' + fieldKey,
			type		: 'string',
			operator	: 'contains',
			property	: fieldProperty,
			updatable	: true,
            value		: value
		});
	}
	
});