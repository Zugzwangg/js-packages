Ext.define('CFJS.plugins.cellstore.view.RentEditorModel', {
	extend		: 'CFJS.app.BaseEditorModel',
	alias		: 'viewmodel.plugins.cellstore.renteditor',
    requires	: [
        'CFJS.model.cellstore.CompanyRent',
        'CFJS.model.cellstore.CustomerRent',
        'CFJS.store.cellstore.Cells'
    ],
	itemName	: 'editRent',
	schema		: 'cellstore',
	stores		: {
		cells: {
			type: 'cells',
			filters: [{
				property: 'size.micro',
				type	: 'boolean',
				operator: 'eq',
				value	: false
			}]
		},
		microCells: {
			type: 'cells',
			filters: [{
				property: 'size.micro',
				type	: 'boolean',
				operator: 'eq',
				value	: true
			}]
		},
		prices: {
			type		: 'cell_prices',
			autoLoad	: true,
			pageSize	: 0,
			sorters		: [ 'period' ]
		}
	},
	formulas: {
		cellAmount: {
			bind: [ '{_itemName_.cell}', '{period}' ],
			get	: function(data) {
				var amount = this.findPriceAmount(data[0] && data[0].isModel ? data[0].getData() : data[0], data[1]) || 0;
				this.getItem().set('amount', amount + (this.get('microCellAmount') || 0));
				return amount;
			}
		},
		microCellAmount: {
			bind: [ '{_itemName_.microCell}', '{period}' ],
			get	: function(data) {
				var amount = this.findPriceAmount(data[0] && data[0].isModel ? data[0].getData() : data[0], data[1]) || 0;
				this.getItem().set('amount', amount + (this.get('cellAmount') || 0));
				return amount;
			}
		},
		period: {
			bind: ['{_itemName_.endDate}', '{_itemName_.startDate}'],
			get: function(data) {
				var today = Ext.Date.today(),
					period = Ext.Date.diffDays(data[1] || today, data[0] || today);
				if (period < 90) this.getItem().set('endDate', Ext.Date.add(data[1] || today, Ext.Date.DAY, period = 90));
				return period;
			},
			set: function(data) {
				var item = this.getItem(), startDate = item.get('startDate')||Ext.Date.today();
				item.set('endDate', Ext.Date.add(startDate, Ext.Date.DAY, data >= 90 ? data : 90));
			}
		}
	},
	
	minStartDate: function() {
		var me = this, today = Ext.Date.today(),
			cmpFn = me.compareFreeFrom, rent = me.getItem();
		if (rent && rent.isModel) {
			rent = rent.getData();
			return cmpFn(cmpFn(today, rent.cell), rent.microCell);
		}
		return today;
	},
	
	privates: {
		
		compareFreeFrom: function(freeFrom, cell) {
			if (cell) {
				if (cell.isModel) cell = cell.getData();
				if (Ext.Date.after(cell.freeFrom, freeFrom)) freeFrom = Ext.clone(cell.freeFrom);
			}
			return freeFrom;
		},
		
		findPriceAmount: function(cell, period) {
			var prices = this.getStore('prices'),
				sizeId = ((cell || {}).size || {}).id,
				minPeriod = Number.MAX_SAFE_INTEGER, amount;
			if (sizeId > 0 && period > 0 && prices && prices.isStore) {
				prices.each(function(rec, i, len) {
					var pricePeriod = CFJS.safeParseInt(rec.get('period')), size = rec.get('size') || {};
					if (size.id === sizeId && pricePeriod >= period && pricePeriod <= minPeriod) {
						minPeriod = pricePeriod;
						amount = CFJS.safeParseInt(rec.get('amount'));
					}
				});
			}
			return amount;
		},
		
		configItem: function(item) {
			var me = this, today = Ext.Date.today();
			if ((item && item.entityName === me.getItemModel()) || item === false) return item;
			return {
				type	: me.getItemModel(),
				create	: Ext.apply({
					endDate		: Ext.Date.add(today, Ext.Date.DAY, 90),
					startDate	: today
				}, item) 
			};
		}

	}

});