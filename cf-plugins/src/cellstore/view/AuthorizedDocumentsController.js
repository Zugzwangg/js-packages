Ext.define('CFJS.plugins.cellstore.view.AuthorizedDocumentsController', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.plugins.cellstore.authorizeddocuments',
	id			: 'cellstore-authorized-documents',
	routeId		: 'authorized-documents',
	editor		: 'panel-base-editor[name="authorizedDocumentEditor"]',
//	listeners	: { beforeremoverecord: 'onBeforeRemoveRecord' },
	
	editorConfig: function(config) {
		return { xtype: 'plugins.cellstore.authorized-document-editor' };
	},
	
	onAddRecord: function(component) {
		var me = this, vm = me.getViewModel(),
			record = { type: component.recordType || '' },
			form = {
				xtype	: 'form',
				layout	: { type: 'vbox', align: 'stretch' }
			},
			window = {
				xtype		: 'window',
				autoShow		: true,
				bodyPadding	: 10,
				buttons		: [{
					action	: 'ok',
					handler	: function(btn) {
						if (!form.isValid()) return;
						window.mask('Wait...');
						switch (record.type) {
						case 'FILE':
							var formData = new FormData(),
								files = form.down('[name="filepath"]').fileInputEl.dom.files;
							if (files.length > 0) {
								record.description = form.down('[name="description"]').getValue();
								formData.append('upload', files[0], files[0].name);
								Ext.Ajax.request({
									headers	: { 'Content-Type': null },
									method	: 'POST',
									params	: { method: 'resource.upload' },
									rawData	: formData,
									callback	: function(options, success, response) {
										window.unmask();
										if (success) {
											response = Ext.decode(response.responseText);
											if (response && response.files) {
												var file = response.files.file;
												if (Ext.isArray(file)) file = file[0];
												record.link = file.path + '/' + file.name;
											} else record.link = files[0].name
											window.close();
											me.maybeEditRecord(record, true);
										}
									}
								});
							}
							return;
						case 'DOCUMENT':
							var formData = form.down('[name="form"]').getValue(),
								link = form.down('[name="link"]').getValue();
							record.description = formData.name + ' #' + link.name;
							record.link = link.id
							break;
						case 'URL':
						default:
							record.description = form.down('[name="description"]').getValue();
							record.link = form.down('[name="link"]').getValue();
							break;
						}
						window.close();
						me.maybeEditRecord(record, true);
					},
					text	: Ext.MessageBox.buttonText.ok
				}],
				layout		: 'fit',
				minHeight	: 110,
				minWidth	: 400,
				modal		: true,
				items		: form
			};
		switch (record.type) {
		case 'DOCUMENT':
			window.title = 'Select document';
			form.defaults = {
				allowBlank		: false, 
				autoLoadOnValue	: true,
				forceSelection	: true,
				selectOnFocus	: true
			};
			form.items = [{
				xtype			: 'dictionarycombo',
				fieldLabel		: 'Form name',
				name			: 'form',
				store			: {
					type	: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'form' } },
					sorters	: [ 'name' ]
				},
				listeners		: {
					change: function(field, value) {
						var combo = form.down('referencecombobox'),
							loaderConfig = combo ? combo.getLoaderConfig() : null;
						if (loaderConfig) {
							loaderConfig.formId = (value||{}).id || 0;
							combo.updateLoaderConfig(loaderConfig);
							combo.getStore().reload();
						}
					}
				}
			},{
				xtype		: 'referencecombobox',
				loaderConfig	: {
					dictionaryType	: 'document',
					formId			: 0,
					unit			: vm.get('userUnit'),
					userInfo		: CFJS.lookupViewportViewModel().userInfo()
				},
				fieldLabel	: 'Document',
				name			: 'link'
	        }];
			break;
		case 'FILE':
			window.title = 'Upload document to a server';
			form.items = [{
				xtype	: 'form',
				layout	: { type: 'vbox', align: 'stretch' },
				items	: [{
		            xtype		: 'textfield',
		            fieldLabel	: 'Name',
		            name			: 'description',
		            selectOnFocus: true
		        },{
		            xtype		: 'filefield',
					allowBlank	: false, 
		            buttonConfig	: { iconCls: CFJS.UI.iconCls.SEARCH, tooltip: 'Choose a file' },
		            buttonText	: '',
		            emptyText	: 'Select a file',
		            fieldLabel	: 'File',
		            name			: 'filepath'
				}]
	        }];
			break;
		case 'URL':
			window.title = 'Specify link to a document';
			form.items = [{
				xtype			: 'textfield',
				fieldLabel		: 'Name',
				name				: 'description',
	            selectOnFocus	: true
			},{
				xtype			: 'textfield',
				allowBlank		: false, 
				fieldLabel		: 'Input URL',
				name				: 'link',
	            selectOnFocus	: true,
				vtype			: 'url', 
				width			: 300
			}];
			break;
		default: break;
		}
		if (form.items && form.items.length > 0) {
			window = Ext.create(window);
			form = window.down('form');
		}
	}

});