Ext.define('CFJS.plugins.cellstore.view.RentsController', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.plugins.cellstore.rents',
	id			: 'cellstore-rents',
	wizardConfig: {
		xtype: 'cellstore.rentwizard',
		listeners: {
			wizardfinish: 'onCloseWizard'
		}
	},
//	editor		: 'panel-base-editor[name=serviceEditor]',
//	listeners	: { beforesaverecord: 'onBeforeSaveRecord', beforeremoverecord: 'onBeforeRemoveRecord' },

//	initViewModel: function(vm) {
//		if (vm.isMaster) {
//			vm.bind('{purchaseHistoryFilter}', this.setPurchaseHistoryFilter, this, { deep: true });
//		} 
//	},

//	beforeLoadSummary: function(store, operation) {
//		var filters = operation.getFilters() || [];
//		filters.push(new Ext.util.Filter({ property: 'state', type: 'string', operator: '!eq', value: 'REFUND' }));
//		operation.setFilters(filters);
//	},

//	cloneRecordData: function(record, linkParent) {
//		if (record) {
//			record = Ext.clone(record.getData({ persist: true, serialize: true }));
//			if (linkParent) record.parent = record.id;
//			delete record.id;
//			delete record.number;
//			record.updatable = true;
//		}
//		return record;
//	},

//	getSegmentsStore: function() {
//		var panel = this.lookupSegments();
//		return panel ? panel.getStore() : null;
//	},

//	lookupSegments: function() {
//		return this.getView().down('grid-base-panel[name=segments]');
//	},

	maybeEditRecord: function(record, add) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			editor = me.getEditor() || '',
			activeItem;
		if (grid && editor) {
			if (view.rendered) Ext.suspendLayouts();
			if (record !== false && !grid.readOnly) {
				var types = Ext.getStore('serviceTypes'),
					viewModel = me.getViewModel(),
					selection = grid.getSelection(), serviceType, evm;
				if (activeItem = view.down(editor)) view.remove(activeItem, true);
				if (!record && !add && selection && selection.length > 0) record = selection[0];
				viewModel.set('serviceType', serviceType = types.getById(add ? record : record.get('serviceType')));
				activeItem = view.add({ xtype: me.getEditorType(serviceType.id) });
				evm = activeItem.getViewModel(); 
				evm.bind('{' + evm.getItemName() + '}', me.startEditRecord, evm, { single: true });
				evm.setItem(record);
			} else activeItem = grid;
			view.getLayout().setActiveItem(activeItem);
			if (view.rendered) Ext.resumeLayouts(true);	
		}
	},
	
	onAddRecord: function(component) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			wizard;
		if (grid && !grid.readOnly) {
			if (view.rendered) Ext.suspendLayouts();
			wizard = view.add(Ext.apply({ wizardType: component.wizardType }, me.wizardConfig));
			view.getLayout().setActiveItem(wizard);
			if (view.rendered) Ext.resumeLayouts(true);	
		}
	},
	
	onCloseWizard: function(wizard) {
		var view = this.getView(),
			grid = view ? view.lookupGrid() : null;
		if (view && grid) {
			if (view.rendered) Ext.suspendLayouts();
			grid.getStore().reload();
			view.remove(wizard);
			view.getLayout().setActiveItem(grid);
			if (view.rendered) Ext.resumeLayouts(true);	
		}
	},
	
	onBeforeRemoveRecord: Ext.emptyFn,
	
//	onBeforeSaveRecord: function(service) {
//		var me = this, consumer, route, segments, len;
//		if (service.isModified('consumer') && (consumer = service.get('consumer'))) {
//			delete consumer.id;
//			consumer.properties = {};
//		}
//		me.onCommitTaxes(service);
//		me.onCommitSegments(service);
//		route = service.get('route');
//		segments = route ? route.segments : null;
//		if (Ext.isArray(segments)) {
//			if ((len = segments.length) <= 0) {
//				CFJS.errorAlert('Ошибка', 'Вы не заполнили блок сегментов!');
//				return false;
//			}
//		}
//	},
	
//	onCommitSegments: function(editor, context) {
//		var me = this, 
//			beforeSave = editor && editor.isModel,
//			service = beforeSave ? editor : me.lookupRecord(),
//			store = !beforeSave ? context.store : me.getSegmentsStore(), 
//			segments = [], len;
//		if (service && store && (beforeSave || store.isDirty())) {
//			var route = service.get('route');
//			store.commitChanges();
//			store.each(function(record) {
//				segments.push(record.getData({ service: beforeSave ? service : undefined }));
//			});
//			route = { id: route.id, segments: segments, created: route.created, updated: route.updated }
//			if ((len = segments.length) > 0) route = Ext.apply(route, {
//				departure	: segments[0].departure,
//				from		: segments[0].from,
//				arrive		: segments[len - 1].arrive,
//				to			: segments[len - 1].to
//			});
//			service.set('route', route, { convert: false, silent: beforeSave });
//		}
//	},
	
	onCopyRecord: function(component) {
		var me = this;
		me.startEditRecord = function(record) {
			me.startEditRecord = Ext.emptyFn;
			this.setItem(me.cloneRecordData(record));
		};
		me.maybeEditRecord();
	},
	
	onEditServices: function(component) {
		var editor = this.getEditorComponent();		
		Ext.create({
			xtype		: 'taxeseditor',
			autoShow	: true,
			service		: editor ? editor.getViewModel().getItem() : null,
			listeners	: {
				close: function(window) {
					if (editor) editor.updateTaxes(window.service);
				}
			}
		});
	},
	
	onLoadSummary: function(store, records, successful, operation) {
		if (successful) this.lookupGrid().updateSummaries(records);
	},
	
	onPrintRecord: function(component) {
		var me = this, 
			grid = me.lookupGrid(),
			selection = grid && grid.getSelection(),
			record = selection && selection.length > 0 && selection[0];
		if (record && record.isRent) record.print();	
	},
	
	onRemoveSegments: function(component) {
		var segments = this.lookupSegments(), selection;
		if (segments && (selection = segments.getSelection())) {
			segments.getStore().remove(selection);
		}
	},
	
	onRemoveTaxes: function(component) {
		var taxes = this.lookupTaxes(), selection;
		if (taxes && (selection = taxes.getSelection())) {
			taxes.getStore().remove(selection);
		}
	},
	
	doSelectionChange: function(selectable, selection, actions) {
		var me = this;
		me.disableComponent(actions.addVoid, !selection || selection.length !== 1);
		me.disableComponent(actions.copyRecord, !selection || selection.length !== 1);
		me.disableComponent(actions.printRecord, !selection || selection.length !== 1);
		me.callParent(arguments);
	},
	
	onSummaryRefresh: function(component) {
		var store = this.lookupGridStore();
		if (store) store.summary();
	},

	setPurchaseHistoryFilter: function(filter) {
		var me = this, vm = me.getViewModel();
		if (filter.view === 'services' && filter.type && filter.id > 0) {
			filter = [{
				id		: 'customerId',
				property: 'order.customer.id',
				type	: 'numeric',
				operator: 'eq',
				value	: filter.id
			},{
				id		: 'customerClass',
				property: 'order.customer.class',
				type	: 'string',
				operator: 'eq',
				value	: filter.type === 'company' ? 'company' : 'customer'
			}];
		} else filter = null;
		me.onBackClick();
		vm.setStoreFilters('services', filter);
	},
	
	startEditRecord: Ext.emptyFn

});
