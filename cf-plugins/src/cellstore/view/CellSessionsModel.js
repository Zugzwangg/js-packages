Ext.define('CFJS.plugins.cellstore.view.CellSessionsModel', {
	extend	: 'Ext.app.ViewModel',
	alias	: 'viewmodel.plugins.cellstore.sessions',
	requires: [
		'CFJS.model.dictionary.Customer',
		'CFJS.model.orgchart.Person'
	],
	schema: 'org_chart',
	data	: {
		cell_session: null,
		cellNumber	: null,
		newPin		: null,
		password	: null,
		pin			: null,
		phoneOrEmail: null,
		username	: null
	},
	
	changePinPayload: function() {
		var me = this;
		return CFJS.Api.buildPayload({
			locale	: CFJS.getLocale(),
			username: me.get('phoneOrEmail'),
			number	: me.get('cellNumber'),
			pin		: me.get('pin'),
			newPin	: me.get('newPin')
		}, 'NEW');
	},
	
	getCellSession: function() {
		return this.get('cell_session');
	},
	
	isEmptyPin: function(pin) {
		return CFJS.safeParseInt(pin) === 0;
	},

	setCellSession: function(session) {
		if (session && !session.isModel) session = new CFJS.model.cellstore.CellBrowsing(session);
		this.set('cell_session', session);
	},
	
	startSessionPayload: function() {
		var me = this;
		return CFJS.Api.buildPayload({
			locale	: CFJS.getLocale(),
			username: me.get('phoneOrEmail'),
			number	: me.get('cellNumber'),
			pin		: me.get('pin')
		}, 'NEW');
	}
	
});
