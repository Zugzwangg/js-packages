Ext.define('CFJS.plugins.cellstore.view.AuthorizedDocumentModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.plugins.cellstore.authorizeddocument',
	requires	: [
		'CFJS.store.Named',
		'CFJS.model.cellstore.Rent',
		'CFJS.store.cellstore.AuthorizedPersons',
		'CFJS.store.cellstore.Rents',
		'CFJS.store.dictionary.Customers'
	],
	itemName	: undefined,
	itemModel	: 'CFJS.model.cellstore.AuthorizedDocument',
	schema		: 'cellstore',
	stores		: {
		persons: {
			type	: 'cell_authorized_persons',
			autoLoad: true,
			session	: { schema: 'cellstore' },
			filters	: [{
				id		: 'authorizedDocument',
				property: 'document.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			}]
		},
		rents	: {
			type	: 'base',
			model	: 'CFJS.model.cellstore.Rent',
			proxy	: {
				service		: 'cell_store',
				loaderConfig: { dictionaryType: 'cell_rent' },
				reader		: { rootProperty: 'response.transaction.list.cell_rent' }
			},
			session	: { schema: 'cellstore' },
			sorters	: [ 'cell.number', 'cell.cupboard.name', { property: 'startDate', direction: 'DESC' }]
		}
	},

	setItem: function(item) {
		return this.linkItemRecord(item);
	}
	
});