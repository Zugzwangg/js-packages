Ext.define('CFJS.plugins.cellstore.view.SettingsModel', {
    extend		: 'CFJS.app.EditorViewModel',
    alias		: 'viewmodel.plugins.cellstore.settings',
    requires	: [
        'CFJS.model.cellstore.*',
        'CFJS.store.cellstore.*'
    ],
    isSettings	: true,
    stores		: {
    	cells: {
    		type: 'cells'
    	},
    	cellSizes: {
    		type: 'cell_sizes'
    	},
    	cupboards: {
    		type: 'cell_cupboards'
    	},
    	prices: {
    		type: 'cell_prices'
    	}
    }
});