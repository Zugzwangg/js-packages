Ext.define('CFJS.plugins.cellstore.view.BrowsingHistoryController', {
	extend		: 'Ext.app.ViewController',
	alias		: 'controller.plugins.cellstore.browsinghistory',
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},
	
	doSelectionChange: function(selectable, selection, actions) {
		this.disableComponent(actions.endSession, !selection || selection.length !== 1);
	},
	
	lookupGrid: function() {
		var view = this.getView();
		return view ? view.lookupGrid() : null;
	},
	
	lookupGridStore: function(name) {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	},
	
	onCloseSession: function(component) {
		var me = this, grid = me.lookupGrid(),
			selection = grid ?  grid.getSelection() : [],
			record = selection && selection.length > 0 ? selection[0] : null;
		if (record && record.isModel) {
			me.disableComponent(component, true);
			record.set('endDate', new Date());
			record.save({
				callback: function(record, operation, success) {
					grid.getStore().reload();
					me.disableComponent(component);
				}
			});
		}
	},
	
	onCreateSession: function(component) {
		var me = this, window = Ext.create({ xtype: 'cellstore.sessionwizard'});
		window.on({
			destroy: function() { me.onReloadStore(); }
		})
	},

	onPeriodPicker: function(component) {
		var grid = this.lookupGrid();
		if (grid && grid.hasPeriodPicker) grid.showPeriodPicker(component);
	},

	onReloadStore: function(component) {
		var store = this.lookupGridStore();
		if (store && store.isStore) store.reload();
	},

	onSelectionChange: function(selectable, selection) {
		var me = this, actions;
		if (selectable && selectable.isSelectionModel) selectable = selectable.view.grid;
		if (!selectable || !selectable.isPanel) selectable = me.lookupGrid();
		if (actions = selectable && selectable.hasActions && selectable.actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.doSelectionChange(selectable, selection, actions);
		}
	}

});