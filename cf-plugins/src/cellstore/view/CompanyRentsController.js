Ext.define('CFJS.plugins.cellstore.view.CompanyRentsController', {
	extend		: 'CFJS.plugins.cellstore.view.RentsController',
	alias		: 'controller.plugins.cellstore.companyrents',
	id			: 'cellstore-company-rents'
});