Ext.define('CFJS.plugins.cellstore.view.CompanyRentsModel', {
	extend	: 'CFJS.plugins.cellstore.view.RentsModel',
	alias	: 'viewmodel.plugins.cellstore.companyrents',
	requires: [ 'CFJS.store.cellstore.CompanyRents' ],
	stores	: {
		rents: { type: 'cell_company_rents' }
	}

});