Ext.define('CFJS.plugins.cellstore.view.CustomerRentsModel', {
	extend	: 'CFJS.plugins.cellstore.view.RentsModel',
	alias	: 'viewmodel.plugins.cellstore.customerrents',
	requires: [ 'CFJS.store.cellstore.CustomerRents' ],
	stores	: {
		rents: { type: 'cell_customer_rents' }
	}

});