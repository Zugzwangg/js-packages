Ext.define('CFJS.plugins.cellstore.view.CustomerRentsController', {
	extend		: 'CFJS.plugins.cellstore.view.RentsController',
	alias		: 'controller.plugins.cellstore.customerrents',
	id			: 'cellstore-customer-rents'
});