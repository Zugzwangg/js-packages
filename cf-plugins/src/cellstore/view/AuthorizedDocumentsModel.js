Ext.define('CFJS.plugins.cellstore.view.AuthorizedDocumentsModel', {
	extend	: 'CFJS.app.InlineEditorViewModel',
	alias	: 'viewmodel.plugins.cellstore.authorizeddocuments',
	requires: [ 'CFJS.store.cellstore.AuthorizedDocuments' ],
	itemName: 'editAuthorizedDocument',
	stores	: {
		documents: {
			type		: 'cell_authorized_documens',
			autoLoad	: true,
			session		: { schema: 'cellstore' },
			filters		: [{
				id		: 'startDate',
				property: 'endDate',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'startDate',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			}],
			sorters		: [{ property: 'startDate', direction: 'DESC' }]
		}
	}
});