Ext.define('CFJS.plugins.cellstore.view.CellSessionsController', {
	extend		: 'Ext.app.ViewController',
	alias		: 'controller.plugins.cellstore.sessions',
	
	init: function(view) {
		var me = this;
		if (Ext.platformTags.desktop) {
			view.onAfter('render', function() {
				Ext.create('Ext.util.KeyNav', view.el, {	
					scope: view, 
					enter: function(e) {
						var button = me.bodyContainer().getLayout().getActiveItem().down('button[clickOnEnter=true]:focusable:not([hidden]):not([disabled])');
						if (button && button.isVisible() && !button.isDisabled()) button.click();
					}
				});
			});
		}
	},
	
	initViewModel: function(vm) {
		vm.bind('{user}', function(user) {
			if (user && user.isModel) vm.set('username', user.get('username'));
		}, vm, { single: true });

	},
	
	bodyContainer: function() {
		return this.getView().down('container[name="body"]');
	},
	
	login: function(options) {
		var api = CFJS.Api;
		api.login(options.username, options.password, api.service, true, options.success,
			function(context) { CFJS.errorAlert('Login failed', context); });
	},

	navigateNext: function(range) {
		var me = this, layout = me.bodyContainer().getLayout(), i = 0;
		range = range || 1;
		for (; i < range; i++) {
			if (layout.getNext()) layout.next();
			else { me.closeView(); break; }
		}
	},
	
	onFinishClick: function(button) {
		var me = this, formData = new FormData(), session = me.getViewModel().getCellSession(),
			files = me.getView().down('filefield[name="stampPhoto"]').fileInputEl.dom.files,
			file;
		button.disable();
		if (session && files.length > 0) {
			formData.append('upload', files[0], files[0].name);
			Ext.Ajax.request({
				headers	: { 'Content-Type': null },
				method	: 'POST',
				params	: { method: 'resource.upload' },
				rawData	: formData,
				callback: function(options, success, response) {
					button.enable();
					if (success) {
						response = Ext.decode(response.responseText);
						if (response && response.files) {
							file = response.files.file;
							if (Ext.isArray(file)) file = file[0];
							file = file.path + '/' + file.name;
						} else file = files[0].name
						session.set({ stampPhoto: file, endDate: new Date() });
						session.save({ callback: function() { me.navigateNext(); } });
					}
				}
			});
		}
	},
	
	onLoginClick: function(button) {
		var me = this, vm = me.getViewModel();
		button.disable();
		me.login({
			username: vm.get('username'),
			password: vm.get('password'),
			success: function(session) { me.navigateNext(); }
		})
	},
	
	onPinChangeClick: function(button) {
		var me = this, api = CFJS.Api, vm = me.getViewModel();
		button.disable();
		api.request({
			method: 'POST',
			params: { method: api.controllerMethod('cell_store') + '.change_pin' },
			jsonData: vm.changePinPayload(),
			success	: function(context) {
				var response = api.extractResponse(context);
				if (response.error) {
					CFJS.errorAlert('Ошибка записи данных', response.error);
					button.enable();
				} else if (response.transaction) {
					vm.getCellSession().set('person', response.transaction);
					me.navigateNext();
				}
			},
			failure	: function(context) { CFJS.errorAlert('Ошибка записи данных', context); button.enable(); }
		});
	},
	
	onStartClick: function(button) {
		var me = this, api = CFJS.Api, vm = me.getViewModel(), timer;
		button.disable();
		api.request({
			method: 'POST',
			params: { method: api.controllerMethod('cell_store') + '.create.session' },
			jsonData: vm.startSessionPayload(),
			success	: function(context) {
				var response = api.extractResponse(context);
				if (response.error) { 
					CFJS.errorAlert('Ошибка записи данных', response.error);
					button.enable();
				} else if (response.transaction) {
					vm.setCellSession(response.transaction);
					me.navigateNext(vm.isEmptyPin(vm.get('pin')) ? 1 : 2);
					if (timer = me.getView().down('timer')) timer.setStartAt(new Date());
				}
			},
			failure	: function(context) { CFJS.errorAlert('Ошибка записи данных', context); button.enable(); }
		});
	},
	
	onStopTimerClick: function(button) {
		button.disable();
		var timer = this.getView().down('timer');
		if (timer) timer.setEndAt(new Date());
		this.navigateNext();
	}
	
});