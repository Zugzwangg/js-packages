Ext.define('CFJS.plugins.cellstore.view.BrowsingHistoryModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.plugins.cellstore.browsinghistory',
	requires: [
		'CFJS.store.cellstore.BrowsingHistory'
	],
	schema: 'cellstore',
	stores: {
		browsingHistory: {
			type	: 'browsing_history',
			autoLoad: true,
			session	: { schema: 'cellstore' },
			filters	: [{
				id		: 'startDate',
				property: 'startDate',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'startDate',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			}]
		}
	}
	
});
