Ext.define('CFJS.plugins.cellstore.view.RentWizardModel', {
	extend	: 'Ext.app.ViewModel',
	alias	: 'viewmodel.plugins.cellstore.rentwizard',
	data	: {
		stepStart	: 0,
		stepActive	: 0,
		stepEnd		: 1
	},
	
	formulas	: {
		atEnd	: {
			bind: [ '{stepEnd}', '{stepActive}' ],
			get	: function(data) { 
				this.set('progress', data[1]/(data[0] || 1))
				return data[0] === data[1]; 
			}
		},
		atStart	: {
			bind: [ '{stepStart}', '{stepActive}' ],
			get	: function(data) { return data[0] === data[1]; }
//		},
//		canNextStep: {
//			bind: {
//				active: '{stepActive}',
//				
//			}
		}
	}
});