Ext.define('CFJS.plugins.view.ViewportController', {
	extend			: 'CFJS.view.ViewportController',
	alias			: 'controller.plugins.viewport',
	id				: 'pluginsViewportController',
	loadingText		: 'Loading...',

	init: Ext.emptyFn,
	
	beforeChangeView: function() { 
		return true;
	},
	
	findRecord: function(store, hashTag) {
		return this.getHiddenMenu()[hashTag];
	},
	
	onToggleNavigationSize: Ext.emptyFn,

	setCurrentView: function(hashTag) {
		var me = this;
		hashTag = (hashTag || '').toLowerCase();
		if (hashTag === 'login') {
			window.location.hash='';
			me.getApplication().showLogin(me.api);
		} else me.callParent(arguments);
	}

});
