Ext.define('CFJS.plugins.view.ViewportModel', {
	extend	: 'CFJS.view.ViewportModel',
	alias	: 'viewmodel.plugins.viewport',
	
	onUserChange: function(user) {
		var me = this, view = me.getView(),
			toolBar = view ? view.down('toolbar#headerBar') : null,
			canAccess = user && user.isModel && user.id > 0, button;
		if (toolBar) {
			if (button = toolBar.down('[action="login"]')) button.setVisible(!canAccess);
			if (button = toolBar.down('[action="lockScreen"]')) button.setVisible(canAccess);
		}
		me.callParent(arguments);
	}

});