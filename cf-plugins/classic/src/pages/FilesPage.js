Ext.define('CFJS.plugins.pages.FilesPage', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.plugins.FilesPage',
	xtype				: 'page.files',
	requires			: [ 'CFJS.grid.FileBrowser' ],
	layout				: 'fit',
    items				: {
		xtype		: 'grid-file-browser',
		fileUpload	: {
			platformConfig	: {
				desktop		: { width: 24 },  
				'!desktop'	: { width: 34 }  
			}
		},
		permissions	: { create: true, 'delete': true }
    }
	
});