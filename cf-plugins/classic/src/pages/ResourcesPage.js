Ext.define('CFJS.plugins.pages.ResourcesPage', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.plugins.ResourcesPage',
	xtype				: 'page.resources',
	requires			: [
		'Ext.layout.container.Border',
		'CFJS.admin.form.field.*',
		'CFJS.grid.ResourceBrowser',
		'CFJS.panel.ResourceProperties'
	],
	layout				: 'border',
	items				: [{
		xtype		: 'grid-resource-browser',
		collapsible	: false,
		fileUpload	: {
			platformConfig	: {
				desktop		: { width: 24 },  
				'!desktop'	: { width: 34 }  
			}
		},
		permissions	: { create: true, 'delete': true },
		region		: 'center'
	},{
		xtype			: 'panel-resource-properties',
		collapsed		: true,
		collapsible		: true,
		floatable		: false,
		region			: 'east',
		split			: true,
		width			: 600
	}],

	initComponent: function() {
		var me = this;
		me.callParent();
		me.editor = me.down('panel-resource-properties');
		me.browser = me.down('grid-resource-browser')
		me.browser.on({
			selectionchange: function(selectable, selection) {
				if (Ext.isObject(selection)) selection = selection.selectedRecords;
				me.editor.lookupViewModel().set('resource', selection && selection.length > 0 && selection[0]);
			}
		});
	}

});