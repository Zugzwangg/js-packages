Ext.define('CFJS.plugins.panel.SettingsPanel', {
	extend			: 'Ext.tab.Panel',
	xtype			: 'settings-panel',
	requires		: [ 'Ext.panel.Panel', 'CFJS.plugins.app.SettingsController' ],
	config			: {
		actionable		: null,
		actions			: {
			addRecord	: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Add record',
				tooltip	: 'Add record',
				handler	: 'onAddRecord'
			},
			copyRecord	: {
				iconCls	: CFJS.UI.iconCls.COPY,
				title	: 'Copy record',
				tooltip	: 'Copy selected record',
				handler	: 'onCopyRecord'
			},
			reloadStore	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
    			title	: 'Update the data',
    			tooltip	: 'Update the data',
				handler	: 'onReloadStore'
			},
			removeRecord: {
				iconCls	: CFJS.UI.iconCls.DELETE,
    			title	: 'Delete records',
    			tooltip	: 'Remove selected records',
				handler	: 'onRemoveRecords'
			},
			saveRecord	: {
				formBind: true,
				iconCls	: CFJS.UI.iconCls.SAVE,
				tooltip	: 'Save the changes',
				handler	: 'onSaveRecords'
			}
		},
//		actionsDisable	: [ 'removeRecord', 'saveRecord' ],
		actionsMap		: {
			contextMenu	: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'saveRecord' ] },
			header		: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'saveRecord', 'reloadStore' ] }
		}
//		contextMenu		: { items: new Array(4) }
	},
	controller			: 'plugins.settings',
	header	: {
		defaultType		: 'button',
		items			: new Array(5)
	},
	iconCls				: CFJS.UI.iconCls.COGS,
	tabPosition			: 'left',
	tabRotation			: 0,
	title				: 'Settings',
	ui					: 'navigation',
	
    currencyRenderer: function() {
    	var me = this;
    	return function(value) {
    		return Ext.util.Format.currency(value, me.currency.currencySign, me.currency.decimals, me.currency.end, me.currency.currencySpacer);
    	}
    },

    modelRenderer: function(store, searchKey, valueKey) {
    	var me = this, vm = me.lookupViewModel(), record;
		if (Ext.isString(store) && vm) store = vm.getStore(store);
		return function(value) {
			if (store && store.isStore) {
				var record = store.findRecord(searchKey, value, 0, false, false, true);
				if (record) value = record.get(valueKey);
			}
			return value;
		};
    },
    
	initComponent: function() {
		var me = this, i = 0;
		me.actionable = me.actionable || me;
		me.callParent();
	},

    onAdd: function(item, pos) {
		this.callParent(arguments);
		this.relayEvents(item, [ 'itemcontextmenu' ]);
    }
	
});