Ext.define('CFJS.plugins.service.panel.ServiceMenu', {
	extend			: 'Ext.form.Panel',
	xtype			: 'plugins.service.servicemenu',
	requires		: [ 'CFJS.plugins.service.view.ServiceModel' ],
	controller		: 'plugins.service.servicemenu',
	viewModel		: { type: 'plugins.service.servicemenu' },
	session			: { schema: 'financial' },
	bbar			: {
		margin	: 5,
		items	: [{
			bind		: { hidden: '{!progress}' },
			direction	: 'prev',
			iconCls		: CFJS.UI.iconCls.BACK,
			minWidth	: 90,
			text		: 'Previous',
			ui			: 'soft-blue',
			handler		: 'onPrevious'
		}, {
			xtype	: 'progressbar',
			bind	: { value: '{progress}' },
			flex	: 1
		}, {
			bind		: { hidden: '{!progress || finish}'},
			direction	: 'next',
			iconAlign	: 'right',
			iconCls		: CFJS.UI.iconCls.NEXT,
			minWidth	: 90,
			text		: 'Next',
			ui			: 'soft-blue',
			handler		: 'onNext'
		}, {
			bind		: { hidden: '{!finish}' },
			formBind	: true,
			minWidth	: 90,
			text		: 'Finish',
			ui			: 'soft-blue',
			handler		: 'onFinish'
		}]
	},
	bodyPadding		: 10,
	layout			: 'card',
	items	: [{
		 xtype: 'container',
		 id		: 'menu-service',
		 layout	: {
			 columns: 3,
			 tdAttrs: { style: 'padding: 5px 10px;' },
			 type: 'table'
		 },
		 items	: [{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: 'x-fa fa-stack-exchange',
			listeners	: {
				click	: 'onClick' 
			},
			reference	: 'integratedInsurance',
			scale		: 'large',
			text		: 'Комплексная страховка',
			width		: 240
		},{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: CFJS.UI.iconCls.MEDKIT,
			listeners	: {
				click	: 'onClick' 
			},
			reference	: 'medicine',
			scale		: 'large',
			text		: 'Медицина',
			width		: 240
		},{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: 'x-fa fa-ambulance',
			listeners	: {
				click	: 'onClick' 
			},
			reference	: 'accident',
			scale		: 'large',
			text		: 'Несчастный случай',
			width		: 240
		},{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: CFJS.UI.iconCls.CAR,
			listeners	: {
				click	: 'onClick' 
			},
			reference	: 'go',
			scale		: 'large',
			text		: 'ГО',
			width		: 240
		},{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: CFJS.UI.iconCls.MONEY,
			listeners	: {
				click	: 'onClick' 
			},
			reference	: 'financialRisks',
			scale		: 'large',
			text		: 'Финансовые риски',
			width		: 240
		},{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: 'x-fa fa-suitcase',
			listeners	: {
				click	: 'onClick'
			},
			reference	: 'luggageInsurance',
			scale		: 'large',
			text		: 'Страхование багажа',
			width		: 240
		},{
			xtype		: 'button',
			arrowAlign	: 'bottom',
			iconAlign	: 'top',
			iconCls		: 'x-fa fa-bomb',
			listeners	: {
				click	: 'onClick'
			},
			reference	: 'other',
			scale		: 'large',
			text		: 'Другое',
			width		: 240
		}],
		reference	: 'menu-service'
	}]

});