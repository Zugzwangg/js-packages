Ext.define('CFJS.plugins.service.grid.AirlineSegmentsPanel', {
	extend	: 'CFJS.grid.BasePanel',
	xtype	: 'airlinesegemntsgrid',
	requires: [
		'Ext.grid.plugin.RowEditing',
		'CFJS.form.DateTimeField', 
		'CFJS.store.airline.AirlineSuppliers', 
		'CFJS.store.airline.Airports'
	],
	actions	: {
		addRecord	: { handler: 'onAddSegment' },
		removeRecord: { handler: 'onRemoveSegments' }
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord' ] },
		header		: {	items: [ 'addRecord', 'removeRecord' ] }
	},
	bind			: { title: 'Route: {routeBySegments}', store: '{segments}' },
	contextMenu		: { items: new Array(2) },
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(2) },
	plugins			: {
		ptype		: 'rowediting',
		autoCancel	: false,
		clicksToEdit: 1, 
		listeners	: {
			beforeedit: function(editor, context) { return !this.grid.readOnly; },
			edit: 'onCommitSegments'
		}
	},
	scrollable		: true,
	sortableColumns : false,
	
	initComponent: function() {
		var me = this,
			editor = { allowBlank: false, selectOnFocus: true },
			combo = Ext.apply({
				xtype			: 'dictionarycombo',
				displayField	: 'iataCode',
				forceSelection	: false,
				hideTrigger		: true,
				listConfig		: { itemTpl: ['<div data-qtip="{iataCode}: {name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{iataCode}: {name}</div>'] },
				minChars 		: 1
			}, editor),
			date = Ext.apply({ xtype: 'datetimefield' }, editor);
		me.columns = [{
			align		: 'center',
			dataIndex	: 'tripNumber',
			editor		: Ext.apply({}, editor),
			text		: 'Рейс',
			width		: 70
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'carrier',
			editor		: Ext.apply({
				queryOperator	: undefined,
				store			: { type: 'airline_suppliers', pageSize: 0, sorters: [ 'iataCode' ] }
			}, combo), 
			style		: { textAlign: 'center' },
			text		: 'Перевозчик',
			tpl			: '{carrier.iataCode}',
			width		: 100
		},{
			text	: 'Отправление',
			columns	: [{
				xtype		: 'templatecolumn',
				align		: 'left',
				dataIndex	: 'from',
				editor		: Ext.apply({
					queryParam	: 'name',
					store		: { type: 'airports', pageSize: 0, sorters: [ 'iataCode' ] }
				}, combo), 
				style		: { textAlign: 'center' },
				text		: 'Аэропорт',
				tpl			: '{from.iataCode} ({from.name})',
				width		: 200
			},{
				align		: 'center',
				dataIndex	: 'terminalFrom',
				editor		: Ext.applyIf({ allowBlank: true }, editor),
//				style		: { textAlign: 'center' },
				text		: 'Терминал',
				width		: 70
			},{
				xtype		: 'datecolumn',
				align		: 'center',
				dataIndex	: 'departure',
				editor		: Ext.clone(date),
				format		: 'd.m.Y H:i',
				text		: 'Время',
				width		: 200
			}]
		},{
			text	: 'Прибытие',
			columns	: [{
				xtype		: 'templatecolumn',
				align		: 'left',
				dataIndex	: 'to',
				editor		: Ext.apply({
					queryParam	: 'name',
					store		: { type: 'airports', pageSize: 0, sorters: [ 'iataCode' ] }
				}, combo),
				style		: { textAlign: 'center' },
				text		: 'Аэропорт',
				tpl			: '{to.iataCode} ({to.name})',
				width		: 200
			},{
				align		: 'center',
				dataIndex	: 'terminalTo',
				editor		: Ext.applyIf({ allowBlank: true }, editor),
//				style		: { textAlign: 'center' },
				text		: 'Терминал',
				width		: 70
			},{
				xtype		: 'datecolumn',
				align		: 'center',
				editor		: Ext.clone(date),
				dataIndex	: 'arrive',
				format		: 'd.m.Y H:i',
				text		: 'Время',
				width		: 200
			}]
		},{
			align		: 'center',
			dataIndex	: 'duration',
			editor		: Ext.applyIf({ allowBlank: true }, editor),
			text		: 'Длительность',
			width		: 75
		},{
			align		: 'center',
			dataIndex	: 'ticketCategory',
			editor		: Ext.applyIf({ allowBlank: true }, editor),
			text		: 'Класс',
			width		: 60
		},{
			align		: 'left',
			dataIndex	: 'tariffCode',
			editor		: Ext.applyIf({ allowBlank: true }, editor),
			style		: { textAlign: 'center' },
			text		: 'Код тарифа'
		},{
			align		: 'left',
			dataIndex	: 'tariffTourCode',
			editor		: Ext.applyIf({ allowBlank: true }, editor),
			style		: { textAlign: 'center' },
			text		: 'Тур. код'
		},{
			dataIndex	: 'tariffBaggage',
			editor		: Ext.applyIf({ allowBlank: true }, editor),
			style		: { textAlign: 'center' },
			text		: 'Багаж',
			width		: 60
		},{
			align		: 'left',
			dataIndex	: 'tariffState',
			editor		: Ext.applyIf({ allowBlank: true }, editor),
			style		: { textAlign: 'center' },
			text		: 'Статус',
			width		: 70
		}]
		me.callParent();
	},
	
	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions;
		me.readOnly = readOnly;
		me.actions.addRecord.setHidden(readOnly);
		me.actions.removeRecord.setHidden(readOnly);
	}

});