Ext.define('CFJS.plugins.service.grid.TaxAmountPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'taxamountgrid',
	requires		: [ 'Ext.grid.plugin.RowEditing', 'CFJS.store.dictionary.Taxes', 'CFJS.plugins.service.window.TaxesEditor' ],
	actions		: {
		addRecord	: { handler: 'onAddTax' },
		editServices: {
			iconCls	: CFJS.UI.iconCls.MONEY,
			title	: 'Service fee and discounts',
			tooltip	: 'Edit service charge and discounts.',
			handler	: 'onEditServices'
		},
		removeRecord: { handler: 'onRemoveTaxes' },
		reloadStore	: false
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editServices' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editServices' ] }
	},
	actionsDisable	: [ 'removeRecord' ],
	bind			: '{taxes}',
	columns			: [{
		xtype		: 'templatecolumn',
		align		: 'left',
		cellWrap	: true,
		dataIndex	: 'tax',
		editor		: {
			xtype			: 'dictionarycombo',
			allowBlank		: false,
			autoSelect		: false,
			displayField	: 'code',
			forceSelection	: false,
			hideTrigger		: true,
			listConfig		: { itemTpl: ['<div data-qtip="{code}: {name}">{code}</div>'] },
			minChars 		: 1,
			queryOperator	: undefined,
			selectOnFocus	: true,
			store			: { type: 'taxes', pageSize: 0, filters:[{ property: 'id', type: 'numeric', operator: '!lt', value: 50 }], sorters: [ 'code' ] }
		},
		style		: { textAlign: 'center' },
		text		: 'ID',
		tpl			: '<tpl if="tax"><tpl if="tax.id &lt; 50">{tax.name}<tpl else>{tax.code}</tpl></tpl>',
		flex		: 1
	},{
		dataIndex	: 'amount',
		editor		: { xtype: 'numberfield', allowExponential: false, selectOnFocus: true },
		renderer	: function(value, metaData, record) {
			var service = this.lookupViewModel().getItem(),
				tax = record && record.get('tax'),
				currency = (service ? (tax && tax.id >= 50 ? service.get('baseCurrency') : service.get('currency')) : null) || { code: 'UAH' };
			if (value && value < 0) metaData.tdStyle = 'color: red';
			return Ext.util.Format.currency(value, ' ' + currency.code, 2, true);
		},
		style		: { textAlign: 'center' },
		text		: 'Amount',
		width		: 140
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'created',
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'Created',
		width		: 170
	}],
	contextMenu		: { items: new Array(3) },
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(3) },
	plugins			: { 
		ptype		: 'rowediting', 
		clicksToEdit: 1, 
		listeners	: {
			beforeedit: function(editor, context) {
				var record = context.record, tax = record.isModel ? record.get('tax') : record.tax;
				return !(tax && tax.id < 50) && !this.grid.readOnly;
			},
			edit: 'onCommitTaxes'
		}
	},
	selModel		: 'rowmodel',
	title			: 'Tariffs block',

	initComponent: function() {
		var me = this;
//		me.columns;
		me.callParent();
	},
	
//	amountRenderer: 

});