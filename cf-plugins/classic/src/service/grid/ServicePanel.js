Ext.define('CFJS.plugins.service.grid.ServicePanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'servicegrid',
	requires		: [ 'Ext.toolbar.TextItem', 'Ext.ux.statusbar.StatusBar' ],
	bind			: '{services}',
	enableColumnHide: false,
	iconCls			: CFJS.UI.iconCls.TAGS,
	reference		: 'serviceGrid',
	title			: 'Services',
	actions			: {
		addRecord	: {
			title	: 'Add "manual" service',
			tooltip	: 'Add service in "manual" mode'
		},
		addVoid		: {
			iconCls	: CFJS.UI.iconCls.LINK,
			title	: 'Add a refund',
			tooltip	: 'Add refund the selected services',
			handler	: 'onAddVoid'
		},
		copyRecord	: {
			iconCls	: CFJS.UI.iconCls.COPY,
			title	: 'Copy service',
			tooltip	: 'Copy the selected service',
			handler	: 'onCopyRecord'
		},
		editRecord	: {
			title	: 'Edit service',
			tooltip	: 'Edit the selected service'
		},
		printRecord	: {
			iconCls	: CFJS.UI.iconCls.PRINT,
			title	: 'Print service',
			tooltip	: 'Print the selected service',
			handler	: 'onPrintRecord'
		},
		removeRecord: {
			title	: 'Remove service',
			tooltip	: 'Remove the selected services'
		}
	},
	actionsDisable	: [ 'addVoid', 'copyRecord', 'editRecord', 'printRecord', 'removeRecord' ],
	actionsMap		: {
		contextMenu	: {
			items: [ 'addRecord', 'removeRecord', 'editRecord', 'copyRecord', 'printRecord', 'addVoid' ]
		},
		header		: {
			items: [ 'periodPicker', 'addRecord', 'removeRecord', 'editRecord', 'copyRecord', 'addVoid', 'printRecord', 'clearFilters', 'reloadStore' ]
		}
	},
	bbar			: {
		xtype: 'statusbar',
		items: [{
			xtype		: 'tbtext',
			summary		: 'count',
			text		: '<b>Количество</b>: 0',
			renderValue	: function(count) {
				this.setText('<b>Количество</b>: ' + Ext.util.Format.round(count || 0, 0));
			}
		},{
			xtype		: 'tbtext',
			summary		: 'amount',
			text		: '<b>Сумма</b>: 0',
			renderValue	: function(amount) {
				this.setText('<b>Сумма</b>: ' + Ext.util.Format.currency(amount || 0, ' грн.', 2, true));
			}
		},{
        	iconCls		: CFJS.UI.iconCls.REFRESH,
        	handler		: 'onSummaryRefresh'
		}]
	},
	contextMenu		: { items: new Array(6) },
	header			: { items: new Array(9) },
	plugins			: 'gridfilters',
	columns			: [{
		xtype		: 'templatecolumn',
		align		: 'center',
		dataIndex	: 'order',
		filter		: { type: 'number', itemDefaults: { emptyText: 'номер заказа', minValue: 1 } },
		style		: { textAlign: 'center' },
		text		: 'Заказ',
		tpl			: '{order}'
	},{
		align		: 'left',
		dataIndex	: 'number',
		filter		: { type: 'string', emptyText: 'номер услуги' },
		style		: { textAlign: 'center' },
		text		: 'Номер',
		width		: 180
	},{
		align		: 'left',
		dataIndex	: 'extNumber',
		filter		: { type: 'string', emptyText: 'внешний номер' },
		style		: { textAlign: 'center' },
		text		: 'Внешний номер',
		width		: 180
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		cellWrap	: true,
		dataIndex	: 'sector.name',
		filter		: { type: 'string', emptyText: 'сектор', operator: null },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Сектор',
		tpl			: '<tpl if="sector">{sector.name}</tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		cellWrap	: true,
		dataIndex	: 'consumer.lastName',
		filter		: { type: 'string', emptyText: 'ФИО клиента' },
		flex		: 2,
		style		: { textAlign: 'center' },
		text		: 'Клиент',
		tpl			: '<tpl if="consumer">{consumer.lastName} {consumer.name}</tpl>'
	},{
		xtype		: 'templatecolumn',
		dataIndex	: 'amount',
		filter		: {
			type		: 'number',
			itemDefaults: { emptyText: 'Укажите сумму', minValue: 0 }
		},
		style		: { textAlign: 'center' },
		text		: 'Сумма',
		tpl			: '{amount:number("0,000,000.00##")} <tpl if="currency">{currency.name}</tpl>',
		width		: 140
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		cellWrap	: true,
		dataIndex	: 'post',
		filter		: { type: 'string', dataIndex: 'post.employment.person.name', emptyText: 'ФИО агента' },
		flex		: 2,
		style		: { textAlign: 'center' },
		text		: 'Агент',
		tpl			: '<tpl if="post"><tpl if="post.person">{post.person.name}<tpl else>Вакантно</tpl></tpl>'
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'created',
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'Создан',
		width		: 170
	},{
		align		: 'left',
		dataIndex	: 'state',
		filter		: { type: 'list', dataType: 'string', itemDefaults: { hideOnClick: true }, labelField: 'name', operator: 'eq', store: 'orderStates', single: true },
		renderer	: Ext.util.Format.enumRenderer('orderStates'),
		style		: { textAlign: 'center' },
		text		: 'Статус'
	},{
		xtype		: 'datecolumn',
		align		: 'center',
		dataIndex	: 'validUntil',
		filter		: {
			type		: 'date',
			dataType	: 'date-time',
			itemDefaults: { emptyText: 'Укажите время окончания действия' }
		},
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'Заканчивается',
		width		: 170
	}],
	
	getRowClass: function(record, index, rowParams, store) {
		if (record.get('state') === 'REFUND') return 'red';
		var hasParent = record.get('parent'), 
			hasChilds = record.get('hasChilds');
		if (hasParent && hasChilds) return 'olive';
		if (hasParent) return 'green';
		if (hasChilds) return 'blue';
	},
	
	initComponent: function() {
		var me = this,
			types = Ext.getStore('serviceTypes'),
			actions = me.actions;
		me.selModel['rowNumbererHeaderWidth'] = 56;
		me.on('filterchange', me.onFilterChange, me);
		if (types && types.isStore) {
			types.on('filterchange', me.createAddRecordMenu, me);
			if (types.isFiltered()) me.createAddRecordMenu(types);
		}
		me.callParent();
		var vm = me.lookupViewModel();
		if (vm) {
			vm.bind('{hidePeriodPicker}', function(hidden) {
				actions.periodPicker.setHidden(hidden);
			});
		}
	},
	
	createAddRecordMenu: function(types) {
		var me = this,
			actions = me.actions, 
			addRecord = actions.addRecord,
			scope = me.lookupController(),
			items = [];
		if (addRecord.isAction) addRecord = addRecord.initialConfig;
		if (!addRecord.serviceCode && addRecord.menu !== false) {
			if (types && types.isStore) {
				types.each(function(record) {
					var data = record.data;
					items.push({
						disabled	: data.disabled,
						handler		: 'onAddRecord',
						iconCls 	: data.iconCls,
						serviceCode	: data.id,
						scope		: scope,
						text		: data.name
					});
				});
			}
			delete addRecord.handler;
			addRecord.menu = { items: items };
			actions.addRecord.callEach('setMenu', [addRecord.menu]);
			actions.addRecord.setDisabled(items.length === 0);
		}
	},
	
	onFilterChange: function(store, filters) {
		if (store) store.summary();
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.actions.addVoid.setHidden(readOnly);
		me.actions.copyRecord.setHidden(readOnly);
		me.callParent(arguments);
	},
	
	updateSummary: function(key, value) {
		var tbtext = this.down('tbtext[summary=' + key + ']');
		if (tbtext) tbtext.renderValue(value);
	},
	
	updateSummaries: function(records) {
		var me = this, data = { COUNT: 'count', SUM: 'amount' },
			key, type;
		for (key in records) {
			type = records[key].get('type');
			if (data[type]) {
				me.updateSummary(data[type], records[key].get('value'));
				delete data[type];
			}
		}
		for (key in data) {
			me.updateSummary(data[key]);
		}
	}

});