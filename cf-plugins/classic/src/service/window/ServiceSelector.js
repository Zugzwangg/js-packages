Ext.define('CFJS.plugins.service.window.ServiceSelector', {
	extend		: 'Ext.window.Window',
	xtype		: 'plugins.service.selector',
	requires	: [
		'CFJS.plugins.service.grid.ServicePanel',
		'CFJS.plugins.service.panel.Editor',
		'CFJS.plugins.service.view.SelectorController',
		'CFJS.plugins.service.view.ServicesModel'
	],
	config		: {
		actions			: {
			addRecord		: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Добавить новую услугу',
				tooltip	: 'Добавить новую услугу',
				handler	: 'onAddRecord'
			},
			clearFilters: {
				iconCls	: CFJS.UI.iconCls.CLEAR,
				title	: 'Очистить фильтры',
				tooltip	: 'Очистить все фильтры',
				handler	: 'onClearFilters'
			},
			editRecord		: {
				iconCls	: CFJS.UI.iconCls.EDIT,
				title	: 'Редактировать выделенную услугу',
				tooltip	: 'Редактировать выделенную услугу',
				handler	: 'onEditRecord'
			},
			periodPicker: {
				iconCls	: CFJS.UI.iconCls.CALENDAR,
    			title	: 'Working period',
    			tooltip	: 'Set the working period',
				handler	: 'onPeriodPicker'
			},
			selectRecords	: {
				iconCls	: CFJS.UI.iconCls.APPLY,
				title	: 'Выбрать выделенные услуги',
				tooltip	: 'Выбрать выделенные услуги',
				handler	: 'onSelectRecords'
			}
		},
		actionsDisable	: [ 'editRecord', 'selectRecords' ],
		actionsMap		: {
			contextMenu	: { items: [ 'selectRecords', 'editRecord', 'addRecord' ] },
			header		: { items: [ 'periodPicker', 'selectRecords', 'editRecord', 'addRecord', 'clearFilters' ] }
		},
		contextMenu		: { items: new Array(3) }
	},
	controller	: 'plugins.service.select',
	header		: CFJS.UI.buildHeader({ items: new Array(4) }),
	iconCls		: CFJS.UI.iconCls.TAGS,
	layout		: { type: 'card', anchor: '100%' },
	minWidth	: 600,
	minHeight	: 300,
	modal		: true,
	session		: { schema: 'financial' },
	title		: 'Поиск и выбор услуг',
	viewModel	: { type: 'plugins.service.list', isMaster: false, itemName: 'serviceSelector' },
	items		: [{
		xtype		: 'servicegrid',
		header		: false,
		contextMenu	: false,
		listeners	: { itemdblclick: 'onGridDblClick', itemcontextmenu: 'onShowContextMenu' }
	}],

	lookupGrid: function() {
		return this.down('servicegrid');
	}

});