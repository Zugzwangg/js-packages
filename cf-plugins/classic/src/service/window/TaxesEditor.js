Ext.define('CFJS.plugins.service.window.TaxesEditor', {
	extend				: 'Ext.window.Window',
	xtype				: 'taxeseditor',
	autoShow			: true,
	bodyPadding			: '10 10 0 10',
	config				: {
		actions		: {
			refreshRecord	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				tooltip	: 'Обновить данные',
				handler	: 'onRefreshRecord'
			},
			saveRecord		: {
				formBind: true,
				iconCls	: CFJS.UI.iconCls.APPLY,
				tooltip	: 'Записать внесенные изменения',
				handler	: 'onSaveRecord'
			}
		},
		actionsMap	: {
			header: { items: [ 'refreshRecord', 'saveRecord' ] }
		},
		contextMenu	: false,
		service		: undefined
	},
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader({ items: new Array(2) }),
	iconCls				: CFJS.UI.iconCls.MONEY,
	modal				: true,
	scrollable			: true,
	title				: 'Сервисный сбор и скидки',
	items				: {
		xtype		: 'form',
		defaults	: {
			allowBlank		: false,
			allowExponential: false,
			width			: 400
		},
		defaultType	: 'numberfield',
		fieldDefaults	: {
			labelAlign	: 'left',
			labelWidth	: 150,
			msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
		}
	},
	
	afterRender: function() {
		var me = this;
		me.callParent();
		me.updateService(me.service);
	},

	applyService: function(service) {
		if (service && !service.isModel) {
			service = CFJS.model.financial.Service.create(service);
		}
		return service;
	},
	
	initComponent: function() {
		var me = this,
			taxes = Ext.getStore('internal_taxes');
		if (taxes) {
			var items = me.items.items = [], item;
			taxes.each(function(record) {
				item = {
					fieldLabel	: record.get('name'),
					name		: record.get('field'),
					taxCode		: record.get('code'),
					taxId		: record.getId()
				};
				if (record.get('negative')) item.maxValue = 0; else item.minValue = 0;
				items.push(item);
			});
		}
		me.callParent();
	},

	onRefreshRecord: function(component) {
		var me = this;
		me.updateService(me.service);
	},
	
	onSaveRecord: function(component) {
		var me = this, form = me.down('form'), service = me.service;
		if (service && form.isValid()) {
			me.disableComponent(component, true)
			form.items.each(function(item) {
				if (item.isVisible()) {
					service.setTax({ id: item.taxId, code: item.taxCode, name: item.fieldLabel }, item.value);
				}
			});
			service.dirty = true;
			me.close();
		}
	},
	
	updateService: function(service) {
		var me = this;
		if (!me.rendered) return;
		var form = me.down('form'), serviceType = service.get('serviceType');
		form.items.each(function(item) {
			if (item.name === 'additionalServices') item.setVisible(serviceType === 'RAILWAY');
			item.setValue((service.getTax(item.taxCode) || {}).amount || 0);
		});
	}

});
