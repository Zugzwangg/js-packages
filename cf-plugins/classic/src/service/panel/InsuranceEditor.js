Ext.define('CFJS.plugins.service.panel.InsuranceEditor', {
	extend		: 'CFJS.plugins.service.panel.Editor',
	xtype		: 'plugins.service.insurance.editor',
	requires	: [ 
		'CFJS.plugins.insurance.civilliability.panel.*',
		'CFJS.plugins.insurance.health.panel.*',
		'CFJS.plugins.service.view.InsuranceModel'
	],
	actions		: {
		writeOut: {
			formBind: true,
			iconCls	: CFJS.UI.iconCls.CERTIFICATE, 
			title	: 'Write out a policy',
			tooltip	: 'Write out an insurance policy',
			handler	: 'onWriteOut'
		}
	},
	actionsMap	: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord', 'writeOut' ] } },
	header		: { items: new Array(5) },
	viewModel	: {	type: 'plugins.service.insurance' },
	
	afterRender: function() {
		var me = this, vm = me.lookupViewModel(),
			programField = me.down('dictionarycombo[name="program"]');
		if (programField) programField.setStore(vm.get('insurancePrograms'));
		me.callParent();
	},

	initComponent: function() {
		var me = this, sections = me.sections || {},
			vm = me.lookupViewModel();
		if (me.isFirstInstance) {
			me.pushItems(sections.mainBlock, { name: 'rowNumbers' }, {
				xtype			: 'dictionarycombo',
				bind			: { value: '{_itemName_.program}' },
				editable		: false,
				emptyText		: 'вид страхования',
				fieldLabel		: 'Вид страхования',
				flex			: 1,
				listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
				name			: 'program',
				publishes		: 'value',
				selectOnFocus	: false,
				typeAhead		: false
			});
		}
		me.callParent();
		if (vm) {
			vm.bind('{_itemName_.id}', function(id) {
				var writeOut = me.actions.writeOut;
				if (writeOut) writeOut.setDisabled(me.readOnly || id <= 0);
			});
		}
	},

	getProgramEditor: function(program) {
		var listFields, properties = (program ? program.properties : null) || {}, value, type;
		value = properties['field_listFields'];
		if( value ) {
			if (value['$']) listFields = value['$'];
		}
		if (Ext.isObject(program)) program = program.isModel ? program.get('code') : program.code;
		if (Ext.isString(program)) {
			switch (program) {
				case 'ACC':
					type = 'casualty.customereditor';
					break;
				case 'BAG':
					type = 'baggage.customereditor';
					break;
				case 'CL': 
					type = 'civilliability.customereditor';
					break;
				case 'CPI':
					break;
				case 'FIN':
					break;
				case 'HI':
					type = 'health.customereditor';
					break;
				default:
					break;
			}
			if (listFields) type = CFJS.merge( { xtype: type, sections: {}}, Ext.decode(listFields));
			return type;
		}
	},
	
	switchProgramEditor: function(editor) {
		var me = this;
		if (Ext.isString(editor)) editor = { xtype: editor, readOnly: me.readOnly };
		if ((me.insuranceEditor||{}).xtype !== (editor||{}).xtype) {
			Ext.suspendLayouts();
			me.remove(me.insuranceEditor);
			me.insuranceEditor = editor ? me.add(2, editor) : null;
			Ext.resumeLayouts();
			return me.insuranceEditor;
		}
	},
	
	switchProgram: function(program) {
		var me = this, cfg = me.getProgramEditor(program);
		if (program) {
			if (!program.isModel) program = new CFJS.model.insurance.InsuranceProgram(program);
			return me.switchProgramEditor(Ext.apply(Ext.isString(cfg) ? { xtype: cfg } : cfg, { viewModel: { program: program } }));
		}
	},
	
	initAdditionalEditors: function(items) {
		items.splice(2, 0, {
			xtype		: 'form',
			defaults	: { anchor: '100%', allowBlank: false, defaultType: 'textfield', layout: 'hbox' },
			title		: 'Условия страхования',
			items		: [{
				combineErrors	: true,
				defaults		: { margin: '0 0 0 5', required: true, selectOnFocus: true },
				items			: [{
					xtype		: 'datetimefield',
					bind		: '{_itemName_.dateFrom}',
					emptyText	: 'начало действия',
					fieldLabel	: 'Начало действия',
					fieldOffset	: 5,
					flex		: 1,
					fieldTime	: { flex: 1 },
					margin		: 0,
					name		: 'dateFrom'
				},{
					xtype		: 'datetimefield',
					bind		: '{_itemName_.dateTo}',
					emptyText	: 'окончание действия',
					fieldLabel	: 'Окончание действия',
					fieldOffset	: 5,
					flex		: 1,
					fieldTime	: { flex: 1 },
					name		: 'dateFrom'
				},{
					xtype		: 'numberfield',
					bind		: '{_itemName_.insuranceAmount}',
					fieldLabel	: 'Страховая сумма',
					flex		: 1,
					name		: 'insuranceAmount',
					width		: 130
				},{
					xtype			: 'dictionarycombo',
					bind			: { value: '{_itemName_.insuranceCurrency}' },
					displayField	: 'code',
					editable		: false,
					emptyText		: 'выберите валюту',
					fieldLabel		: 'Валюта',
					forceSelection	: false,
					name			: 'insuranceCurrency',
					publishes		: 'value',
					selectOnFocus	: false,
					typeAhead		: false,
					store			: { type: 'currencies' },
					width			: 85
				}]
			}]
		});
		return items;
	},
	
	setReadOnly: function(readOnly) {
		var me = this, editor = me.insuranceEditor;
		me.hideComponent(me.actions.writeOut, readOnly);
		if (editor && Ext.isFunction(editor.setReadOnly)) editor.setReadOnly(readOnly);
		me.callParent(arguments);
	}
	
});
