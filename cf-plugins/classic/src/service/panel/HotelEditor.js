Ext.define('CFJS.plugins.service.panel.HotelEditor', {
	extend		: 'CFJS.plugins.service.panel.Editor',
	xtype		: 'plugins.service.hotel.editor',
	requires	: [ 'CFJS.plugins.service.view.HotelModel' ],
	sections	: {
		placement: {
			defaults: { anchor: '100%', allowBlank: false, defaultType: 'textfield', layout: 'hbox' },
			order	: 3,
			title	: 'Размещение',
			items	: [{
				combineErrors	: true,
				defaults		: {
					flex				: 1,
					forceSelection		: false,
					margin				: '0 0 0 5',
					minChars 			: 3,
					pageSize			: 25,
					publishes			: 'value',
					required			: true,
					selectOnFocus		: true
				},
				defaultType		: 'dictionarycombo',
		        items			: [{
					bind				: { value: '{hotel.city}' },
					detailField			: 'dictionarycombo[name=hotel]',
		        	emptyText			: 'выберите город',
					fieldLabel			: 'Город',
					margin				: 0,
					required			: false,
					store				: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'city' } },
						sorters	: [ 'name' ]
					},
					listeners			: {
						change: function(combo, city) {
							combo.lookupViewModel().setCityFilter(combo, city);
						}
					}
		        },{
					bind				: { value: '{_itemName_.hotel}' },
		        	emptyText			: 'выберите отель',
					fieldLabel			: 'Отель',
					name				: 'hotel',
					store				: { type: 'hotels', sorters: [ 'name' ] }
		        },{
					bind		: { value: '{_itemName_.food}' },
		        	emptyText	: 'выберите тип питания',
					fieldLabel	: 'Питание',
					name		: 'food',
					store		: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'hotel_food' } },
						sorters	: [ 'name' ]
					}
		        }]
			},{
				combineErrors	: true,
				defaults		: { format: 'd.m.Y H:i', margin: '0 0 0 5', required: true, selectOnFocus: true },
				defaultType		: 'datefield',
		        items			: [{
		        	xtype			: 'dictionarycombo',
					bind			: { value: '{_itemName_.roomType}' },
					editable		: false,
		        	emptyText		: 'тип номера',
					fieldLabel		: 'Тип номера',
					margin			: 0,
					publishes		: 'value',
					selectOnFocus	: false,
					store			: {
						type	: 'named',
						pageSize: 0,
						proxy	: { loaderConfig: { dictionaryType: 'room_type' } },
						sorters	: [ 'name' ]
					},
					typeAhead		: false
		        },{
		        	xtype			: 'dictionarycombo',
					bind			: { value: '{_itemName_.roomCategory}' },
					editable		: false,
		        	emptyText		: 'категория номера',
					fieldLabel		: 'Категория номера',
					publishes		: 'value',
					selectOnFocus	: false,
					store			: {
						type	: 'named',
						pageSize: 0,
						proxy	: { loaderConfig: { dictionaryType: 'room_category' } },
						sorters	: [ 'name' ]
					},
					typeAhead		: false
		        },{
		        	xtype				: 'textfield',
					bind				: '{_itemName_.roomNumber}',
					fieldLabel			: 'Комната',
					name				: 'roomNumber',
					required			: false,
					width				: 80
		        },{
					bind		: '{_itemName_.checkIn}',
					fieldLabel	: 'Заселение',
					flex		: 1,
					name		: 'checkIn'
		        },{
					bind		: '{_itemName_.checkOut}',
					fieldLabel	: 'Выселение',
					flex		: 1,
					name		: 'checkOut'
		        }]
			}]
		}
	},
	viewModel	: {	type: 'plugins.service.hotel' }

});
