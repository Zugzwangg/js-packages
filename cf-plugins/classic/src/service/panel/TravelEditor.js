Ext.define('CFJS.plugins.service.panel.TravelEditor', {
	extend		: 'CFJS.plugins.service.panel.Editor',
	xtype		: 'plugins.service.travel.editor',
	requires	: [ 
		'CFJS.plugins.service.view.TravelModel'
	],
	sections	: {
		extra: {
			defaults: { anchor: '100%', allowBlank: false, defaultType: 'textfield', layout: 'hbox' },
			title	: 'Дополнительно',
			order	: 3,
			items	: [{
				combineErrors	: true,
				defaults		: { flex: 1, margin: '0 0 0 5', required: true, selectOnFocus: true },
				items			: [{
					xtype		: 'datetimefield',
					bind		: '{_itemName_.dateFrom}',
					emptyText	: 'начало действия',
					fieldLabel	: 'Начало действия',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: .5 },
					fieldTime	: { columnWidth: .5 },
					flex		: 1,
					margin		: 0,
					name		: 'dateFrom'
				},{
					xtype		: 'datetimefield',
					bind		: '{_itemName_.dateTo}',
					emptyText	: 'окончание действия',
					fieldLabel	: 'Окончание действия',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: .5 },
					fieldTime	: { columnWidth: .5 },
					flex		: 1,
					name		: 'dateFrom'
				}]
			},{
				combineErrors	: true,
				defaults		: {
		            allowBlank		: true,
					flex			: 1,
					forceSelection	: false,
					margin			: '0 0 0 5',
					minChars 		: 3,
					pageSize		: 25,
					publishes		: 'value',
					selectOnFocus	: true
				},
				defaultType		: 'dictionarycombo',
				items			: [{
					bind		: { value: '{city.country}' },
					detailField	: 'dictionarycombo[name=city]',
		        	emptyText	: 'выберите страну',
					fieldLabel	: 'Страна',
					margin		: 0,
					store		: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'country' } },
						sorters	: [ 'name' ]
					},
					listeners	: {
						change: function(combo, country) {
							combo.lookupViewModel().setCountryFilter(combo, country);
						}
					}
		        },{
					bind		: { value: '{_itemName_.city}' },
		        	emptyText	: 'выберите город',
					fieldLabel	: 'Город',
					name		: 'city',
					store		: {
						type	: 'base',
						model	: 'CFJS.model.dictionary.City',
						proxy 	: {
							loaderConfig: { dictionaryType: 'city' },
							reader		: { rootProperty: 'response.transaction.list.city' }
						},
						sorters	: [ 'name' ]
					}
				}]
			},{
				xtype		: 'textareafield',
	            allowBlank	: true,
				bind		: '{_itemName_.additionalServices}',
				fieldLabel	: 'Дополнительные услуги',
				height		: 200,
				name		: 'additionalServices',
				tooltip		: 'Просмотр и редактирование дополнительных услуг'
			}]
		}
	},
	viewModel	: {	type: 'plugins.service.travel' }

});
