Ext.define('CFJS.plugins.service.panel.Editor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'plugins.service.editor',
	mixins			: [ 'CFJS.mixin.Sectionable' ],
	requires		: [
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.service.grid.TaxAmountPanel',
		'CFJS.plugins.service.view.ServiceModel',
		'CFJS.store.financial.OrderStates'
	],
	actions			: { back: { tooltip: 'Back to the list of services' } },
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bind			: { readOnly: '{!_itemName_.updatable}', title: 'Service "{_itemName_.sector.name} № {_itemName_.number}". {_itemName_.entityName}' },
	bodyPadding		: '10 10 0 10',
	config			: {
		sections: {
			mainBlock	: {
				defaults: {
					allowBlank		: false,
					anchor			: '100%',
					combineErrors	: true,
					defaults		: { margin: '0 0 0 5', required: true, selectOnFocus: true },
					defaultType		: 'textfield',
					layout			: 'hbox'
				},
				order	: 1,
				title	: 'Основное',
				items	: [{
					name	: 'rowNumbers',
					items	: [{
						bind		: '{_itemName_.number}',
						emptyText	: 'номер документа',
						fieldLabel	: 'Номер документа',
						flex		: 1,
						margin		: 0,
						name		: 'number'
					},{
						bind		: '{_itemName_.extNumber}',
						emptyText	: 'номер заказа',
						fieldLabel	: 'Номер заказа',
						flex		: 2,
						name		: 'extNumber'
					},{
						xtype			: 'dictionarycombo',
						bind			: { value: '{_itemName_.sector}' },
						editable		: false,
						emptyText		: 'выберите сектор',
						fieldLabel		: 'Сектор',
						flex			: 1,
						listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
						name			: 'sector',
						publishes		: 'value',
						selectOnFocus	: false,
						typeAhead		: false,
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'sector' } },
							sorters	: [ 'name' ]
						}
					}]
				},{
					name	: 'rowConsumer',
					items	: [{
						xtype				: 'dictionarypicker',
						closeOnFocusLeave	: false,
						displayField		: 'fullName',
						emptyText			: 'выберите клиента',
						fieldLabel			: 'Клиент',
						flex				: 4,
						labelStyle			: 'font-weight:bold',
						margin				: 0,
						name				: 'consumer',
						publishes			: 'value',
						rawValueData		: false,
						selectorWindow		: { xtype: 'admin-window-customer-picker' }
					}]
				},{
					name	: 'rowState',
					items	: [{
						xtype			: 'combobox',
						bind			: { value: '{_itemName_.state}' },
						displayField	: 'name',
						editable		: false,
						emptyText		: 'выберите статус',
						fieldLabel		: 'Статус',
						flex			: 1,
						forceSelection	: true,
						margin			: 0,
						name			: 'state',
						publishes		: 'value',
						queryMode		: 'local',
						selectOnFocus	: false,
						store			: { type: 'orderstates', filters: [{ property: 'id', operator: '!==', value: 'PAID' }] },
						valueField		: 'id'
					},{
						xtype			: 'numberfield',
						allowExponential: false,
						bind			: '{_itemName_.amount}',
						fieldLabel		: 'К оплате',
						name			: 'amount',
						width			: 130
					},{
						xtype			: 'dictionarycombo',
						bind			: { value: '{_itemName_.currency}' },
						displayField	: 'code',
						editable		: false,
						emptyText		: 'валюта оплаты',
						fieldLabel		: 'Валюта',
						name			: 'currency',
						publishes		: 'value',
						selectOnFocus	: false,
						typeAhead		: false,
						store			: { type: 'currencies' },
						width			: 85
					},{
						xtype		: 'checkbox',
						bind		: '{_itemName_.showVat}',
						fieldLabel	: 'Выделять НДС',
						name		: 'showVat'
					},{
						xtype		: 'datefield',
						bind		: '{_itemName_.validUntil}',
						fieldLabel	: 'Заканчивается',
						format		: Ext.Date.patterns.LongDateTime,
						name		: 'validUntil',
						width		: 175
					}]
				},{
					combineErrors	: true,
					name			: 'rowSupplier',
					items			: [{
						xtype		: 'dictionarycombo',
						bind		: { fieldLabel: '{supplierTitle}', value: '{_itemName_.supplier}' },
						emptyText	: 'выберите компанию',
						flex		: 1,
						margin		: 0,
						minChars 	: 0,
						name		: 'supplier',
						publishes	: 'value',
						store		: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'company' } },
							sorters	: [ 'name' ]
						}
					},{
						xtype			: 'numberfield',
						allowExponential: false,
						bind			: '{_itemName_.baseAmount}',
						fieldLabel		: 'Тариф',
						name			: 'baseAmount',
						width			: 130
					},{
						xtype			: 'dictionarycombo',
						bind			: { value: '{_itemName_.baseCurrency}' },
						displayField	: 'code',
						editable		: false,
						emptyText		: 'валюта тарифа',
						fieldLabel		: 'Валюта',
						name			: 'baseCurrency',
						publishes		: 'value',
						selectOnFocus	: false,
						typeAhead		: false,
						store			: { type: 'currencies' },
						width			: 85
					}]
				}]
			},
			taxes		: {
				xtype		:'taxamountgrid',
				bodyPadding	: 0,
				height		: 250,
				layout		: 'fit',
				name		: 'taxes',
				order		: 2,
				scrollable	: null
			},
			description: {
				bodyPadding	: '10 10 0 10',
				title		: 'Примечания',
				order		: Number.MAX_SAFE_INTEGER,
				items		: {
					xtype		: 'textareafield',
					bind		: '{_itemName_.description}',
					height		: 200,
					hideLabel	: true,
					name		: 'description',
					tooltip		: 'Просмотр и редактирование примечаний'
				}
			}
		}
	},
	defaults		: {
		bodyPadding	: '0 10 0 10',
		collapsible	: true,
		defaults	: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
		defaultType	: 'fieldcontainer',
		frame		: true,
		layout		: 'anchor',
		margin		: '0 0 10 0'
	},
	defaultType 	: 'panel',
	fieldDefaults	: {
		labelAlign	: 'top',
		labelWidth	: 100,
		msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
	},
	header			: { items: new Array(4) },
	itemName		: 'service',
	layout			: { type: 'anchor', reserveScrollbar: true },
	name			: 'serviceEditor',
    scrollable		: 'y',
	viewModel		: {	type: 'plugins.service.edit' },
	
	afterRender: function() {
		var me = this, vm = me.lookupViewModel(),
			picker = me.down('dictionarypicker[name="consumer"]');
		if (picker) {
			vm.bind('{_itemName_.consumer}', function(consumer) {
				picker.setValue(consumer && Ext.clone(consumer));
			});
			picker.on({
				select: function(field, value) {
					var service = vm.getItem();
					if (service && service.isModel) {
						if (value) {
							if (value.isModel) value = value.getData();
							value = Ext.applyIf({
								customer: { id: value.id, name: value.fullName }
							}, CFJS.model.dictionary.Consumer.transform(value));
						}
						service.set('consumer', value);
					}
				}
			});
		}
		me.callParent(arguments);
	},
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel(),
			sections = me.sections || {},
			serviceType = vm.get('serviceType'),
			sector = serviceType && serviceType.isModel ? serviceType.get('sector') : null,
			consumerCombo;
		if (me.isFirstInstance) {
			Ext.apply(me.findConfig(sections.mainBlock, { name: 'supplier' }), me.supplierEditor);
			if (sector > 0)	
				Ext.apply((me.findConfig(sections.mainBlock, { name: 'sector' })).store, {
					filters:[{
						id		: 'parent',
						property: 'parent.id',
						type	: 'numeric',
						operator: 'eq',
						value	: sector 
					}]
				});
		}
		me.callParent();
		if (vm && me.xtype.endsWith('insurance.editor')) {
			vm.bind('{_itemName_.updatable}', function(updatable) {
				me.hideComponent(me.actions.saveRecord, !updatable);
			});
		}
	},

	clearTaxes: function() {
		this.down('taxamountgrid').getStore().removeAll();
	},
	
	setReadOnly: function(readOnly) {
		var me = this, 
			grid = me.down('taxamountgrid'),
			showVat = me.down('checkbox[name="showVat"]');
		if (grid) grid.setReadOnly(readOnly);
		me.callParent(arguments);
		if (showVat) showVat.setReadOnly(false);
	},
	
	updateTaxes: function(service) {
		var me = this, grid = me.down('taxamountgrid'), data;
		if (grid) {
			service = service || me.lookupViewModel().getItem();
			data = service ? (service.isModel ? service.get('taxes') : service.taxes) : null;
			grid.getStore().loadData(data || []);
		}
	}
	
});
