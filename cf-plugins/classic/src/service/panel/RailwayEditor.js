Ext.define('CFJS.plugins.service.panel.RailwayEditor', {
	extend		: 'CFJS.plugins.service.panel.Editor',
	xtype		: 'plugins.service.railway.editor',
	requires	: [
		'CFJS.plugins.service.view.TicketModel',
		'CFJS.store.TicketCategories',
		'CFJS.store.railway.RailwayTicketTypes'
	],
	sections	: {
		route: {
			defaults: { anchor: '100%', allowBlank: false, defaultType: 'textfield', layout: 'hbox' },
			order	: 3,
			title	: 'Маршрут',
			items	: [{
				combineErrors	: true,
				defaults		: { margin: '0 0 0 5', required: true, selectOnFocus: true },
				items			: [{
					xtype			: 'dictionarycombo',
					bind 			: { value: '{route.from}' },
					emptyText		: 'станция отправления',
					fieldLabel		: 'Отправление',
					flex			: 1,
					forceSelection	: false,
					margin			: 0,
					minChars 		: 3,
					name			: 'stationFrom',
					publishes		: 'value',
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'railway_station' } },
						sorters	: [ 'name' ]
					}
				},{
					xtype		: 'datetimefield',
					bind		: '{route.departure}',
					emptyText	: 'время отправления',
					fieldLabel	: 'Время',
					fieldOffset	: 5,
					fieldTime	: { width: 90 },
					name		: 'departure',
					width		: 215
				}]
			},{
				combineErrors	: true,
				defaults		: { margin: '0 0 0 5', required: true, selectOnFocus: true },
				items			: [{
					xtype			: 'dictionarycombo',
					bind 			: { value: '{route.to}' },
					emptyText		: 'станция прибытия',
					fieldLabel		: 'Прибытие',
					flex			: 1,
					forceSelection	: false,
					margin			: 0,
					minChars 		: 3,
					name			: 'stationTo',
					publishes		: 'value',
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'railway_station' } },
						sorters	: [ 'name' ]
					}
				},{
					xtype		: 'datetimefield',
					bind		: '{route.arrive}',
					emptyText	: 'время прибытия',
					fieldLabel	: 'Время',
					fieldOffset	: 5,
					fieldTime	: { width: 90 },
					name		: 'arrive',
					width		: 215
				}]
			},{
				combineErrors	: true,
				defaults		: { flex: 1, margin: '0 0 0 5', required: true, selectOnFocus: true },
				defaultType		: 'textfield',
				items: [{
					bind		: '{route.trainNumber}',
					emptyText	: 'номер поезда',
					fieldLabel	: 'Поезд',
					margin		: 0,
					name		: 'trainNumber'
				},{
					bind		: '{_itemName_.wagonNumber}',
					emptyText	: 'номер вагона',
					fieldLabel	: 'Вагон',
					name		: 'wagonNumber'
				},{
					bind		: '{_itemName_.seatNumber}',
					emptyText	: 'номер места',
					fieldLabel	: 'Место',
					name		: 'seatNumber'
				}]
			},{
				combineErrors	: true,
				defaults		: {
					margin				: '0 0 0 5',
					selectOnFocus		: true
				},
				defaultType		: 'combobox',
				items: [{
					xtype				: 'tagfield',
					bind				: '{_itemName_.service}',
					createNewOnBlur		: true,
					createNewOnEnter	: true,
					emptyText			: 'выберите/добавьте сервис',
					fieldLabel			: 'Дополнительный сервис',
					filterPickList		: true,
					flex				: 2,
					labelStyle			: '',
					margin				: 0,
					name				: 'service',
					queryMode			: 'local',
					store				: {
						data: [{ text: 'Чай' }, { text: '2 чая' }, { text: 'Продуктовый набор' }, { text: 'Постель' }]
					}
				},{
					xtype		: 'checkbox',
					bind		: '{route.electronic}',
					fieldLabel	: 'Электронный',
					name		: 'electronic'
				}]
			}]
		}
	},
	viewModel	: {	type: 'plugins.service.ticket' },

	initComponent: function() {
		var me = this, sections = me.sections || {};
		if (me.isFirstInstance) {
			me.pushItems(sections.mainBlock, { name: 'rowNumbers' }, {
				xtype			: 'combobox',
				bind			: { value: '{_itemName_.type}' },
				displayField	: 'name',
				editable		: false,
				emptyText		: 'тип билета',
				fieldLabel		: 'Тип билета',
				flex			: 1,
				name			: 'type',
				publishes		: 'value',
				queryMode		: 'local',
				selectOnFocus	: false,
				store			: { type: 'railwaytickettypes' },
				valueField		: 'id'
			});
			me.pushItems(sections.mainBlock, { name: 'rowConsumer' }, {
				xtype				: 'combobox',
				bind				: { value: '{_itemName_.ticketCategory}' },
				displayField		: 'name',
				editable			: false,
				emptyText			: 'категория билета',
				fieldLabel			: 'Категория билета',
				flex				: 1,
				name				: 'ticketCategory',
				publishes			: 'value',
				queryMode			: 'local',
				selectOnFocus		: false,
				store				: { type: 'ticketcategories' },
				valueField			: 'id'
			});
		}
		me.callParent();
	}

});
