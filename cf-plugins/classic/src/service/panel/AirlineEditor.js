Ext.define('CFJS.plugins.service.panel.AirlineEditor', {
	extend		: 'CFJS.plugins.service.panel.Editor',
	xtype		: 'plugins.service.airline.editor',
	requires	: [
		'CFJS.plugins.service.view.AirlineModel',
		'CFJS.store.TicketCategories'
	],
	sections		: {
		route: {
			xtype		: 'airlinesegemntsgrid',
			bodyPadding	: 0,
			layout		: 'fit',
			name		: 'segments',
			order		: 3,
			scrollable	: null
		}
	},
	supplierEditor	: {
		listConfig		: { itemTpl: ['<div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{iataCode}: {name}</div>'] },
		queryOperator	: undefined,
		queryParam		: 'iataCode',
		store			: { type: 'airline_suppliers', pageSize: 0, sorters: [ 'iataCode' ] }
	},
	viewModel		: {	type: 'plugins.service.airline' },
	
	initComponent: function() {
		var me = this, sections = me.sections || {};
		if (me.isFirstInstance) {
			me.pushItems(sections.mainBlock, { name: 'rowNumbers' }, {
				xtype			: 'combobox',
				bind			: { value: '{_itemName_.type}' },
				displayField	: 'name',
				editable		: false,
				emptyText		: 'тип билета',
				fieldLabel		: 'Тип билета',
				flex			: 1,
				name			: 'type',
				publishes		: 'value',
				queryMode		: 'local',
				selectOnFocus	: false,
				store			: { type: 'airlinetickettypes' },
				valueField		: 'id'
			});
			me.pushItems(sections.mainBlock, { name: 'rowConsumer' }, {
				xtype			: 'combobox',
				bind			: { value: '{_itemName_.ticketCategory}' },
				displayField	: 'name',
				editable		: false,
				emptyText		: 'категория билета',
				fieldLabel		: 'Категория билета',
				flex			: 1,
				name			: 'ticketCategory',
				publishes		: 'value',
				queryMode		: 'local',
				selectOnFocus	: false,
				store			: { type: 'ticketcategories' },
				valueField		: 'id'
			},{
				bind		: { value: '{_itemName_.consumerType}' },
				emptyText	: 'тип пассажира',
				fieldLabel	: 'Тип пассажира',
				name		: 'consumerType',
				width		: 120
			});
		}
		me.callParent();
	},

	setReadOnly: function(readOnly) {
		var me = this, grid = me.down('airlinesegemntsgrid');
		if (grid) grid.setReadOnly(readOnly);
		me.callParent(arguments);
	}
	
});
