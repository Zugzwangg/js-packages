Ext.define('CFJS.plugins.service.panel.BusEditor', {
	extend			: 'CFJS.plugins.service.panel.Editor',
	xtype			: 'plugins.service.bus.editor',
	requires		: [
		'CFJS.plugins.service.view.TicketModel',
		'CFJS.store.TicketCategories'
	],
	sections		: {
		route: {
			defaults: { anchor: '100%', allowBlank: false, defaultType: 'textfield', layout: 'hbox' },
			order	: 3,
			title	: 'Маршрут',
			items	: [{
				combineErrors	: true,
				defaults		: { margin: '0 0 0 5', required: true, selectOnFocus: true },
				items			: [{
					xtype		: 'fieldcontainer',
					defaultType	: 'dictionarycombo',
					defaults	: {
						flex			: 1,
						forceSelection	: false,
						labelStyle		: 'font-weight:bold',
						margin			: '0 0 0 5',
						minChars 		: 3,
						pageSize		: 25,
						publishes		: 'value',
						selectOnFocus	: true
					},
					fieldLabel	: 'Отправление',
					flex		: 1,
					layout		: 'hbox',
					margin		: 0,
					items	: [{
						bind 			: { value: '{route.from.city}' },
						detailField		: 'dictionarycombo[name=stationFrom]',
						emptyText		: 'город отправления',
						margin			: 0,
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'city' } },
							sorters	: [ 'name' ]
						},
						listeners		: {
							change: function(combo, city) {
								combo.lookupViewModel().setCityFilter(combo, city);
							}
						}
					},{
						allowBlank		: false,
						bind 			: { value: '{route.from}' },
						emptyText		: 'станция отправления',
						name			: 'stationFrom',
						store			: {
							type		: 'base',
							proxy		: { 
								service		: 'iss',
								loaderConfig: { dictionaryType: 'bus_station' }, 
								reader		: { rootProperty: 'response.transaction.list.bus_station' }
							},
							remoteSort	: false,
							sorters		: [ 'name' ]
						}
					}]
					
				},{
					xtype		: 'datetimefield',
					bind		: '{route.departure}',
					emptyText	: 'время отправления',
					fieldLabel	: 'Время',
					fieldOffset	: 5,
					fieldTime	: { width: 90 },
					name		: 'departure',
					width		: 215
				}]
			},{
				combineErrors	: true,
				defaults		: { margin: '0 0 0 5', required: true, selectOnFocus: true },
				items			: [{
					xtype		: 'fieldcontainer',
					defaultType	: 'dictionarycombo',
					defaults	: {
						flex			: 1,
						forceSelection	: false,
						labelStyle		: 'font-weight:bold',
						margin			: '0 0 0 5',
						minChars 		: 3,
						pageSize		: 25,
						publishes		: 'value',
						selectOnFocus	: true
					},
					fieldLabel	: 'Прибытие',
					flex		: 1,
					layout		: 'hbox',
					margin		: 0,
					items		: [{
						bind 			: { value: '{route.to.city}' },
						detailField		: 'dictionarycombo[name=stationTo]',
						emptyText		: 'город прибытия',
						margin			: 0,
						name			: 'cityTo',
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'city' } },
							sorters	: [ 'name' ]
						},
						listeners		: {
							change: function(combo, city) {
								combo.lookupViewModel().setCityFilter(combo, city);
							}
						}
					},{
						allowBlank		: false,
						bind 			: { value: '{route.to}' },
						emptyText		: 'станция прибытия',
						name			: 'stationTo',
						store			: {
							type		: 'base',
							proxy		: { 
								service		: 'iss',
								loaderConfig: { dictionaryType: 'bus_station' }, 
								reader		: { rootProperty: 'response.transaction.list.bus_station' }
							},
							remoteSort	: false,
							sorters		: [ 'name' ]
						}
					}]
				},{
					xtype		: 'datetimefield',
					bind		: '{route.arrive}',
					emptyText	: 'время прибытия',
					fieldLabel	: 'Время',
					fieldOffset	: 5,
					fieldTime	: { width: 90 },
					name		: 'arrive',
					width		: 215
				}]
			},{
				combineErrors	: true,
				defaults		: { flex: 1, margin: '0 0 0 5', required: true, selectOnFocus: true },
				defaultType		: 'textfield',
				items: [{
					bind		: '{route.tripNumber}',
					emptyText	: 'номер рейса',
					fieldLabel	: 'Рейс',
					margin		: 0,
					name		: 'tripNumber'
				},{
					bind		: '{_itemName_.seatNumber}',
					emptyText	: 'номер места',
					fieldLabel	: 'Место',
					name		: 'seatNumber'
				},{
					bind		: '{_itemName_.tariff}',
					emptyText	: 'укажите тариф',
					fieldLabel	: 'Тариф',
					name		: 'tariff'
				}]
			}]
		}
	},
	viewModel		: {	type: 'plugins.service.ticket' },

	initComponent: function() {
		var me = this, sections = me.sections || {};
		if (me.isFirstInstance) {
			Ext.apply(me.findConfig(sections.mainBlock, { name: 'extNumber' }), { flex: 3 });
			me.pushItems(sections.mainBlock, { name: 'rowConsumer' }, {
				xtype			: 'combobox',
				bind			: { value: '{_itemName_.ticketCategory}' },
				displayField	: 'name',
				editable		: false,
				emptyText		: 'категория билета',
				fieldLabel		: 'Категория билета',
				flex			: 1,
				name			: 'ticketCategory',
				publishes		: 'value',
				queryMode		: 'local',
				selectOnFocus	: false,
				store			: { type: 'ticketcategories' },
				valueField		: 'id'
			});
		}
		me.callParent();
	}
	
});
