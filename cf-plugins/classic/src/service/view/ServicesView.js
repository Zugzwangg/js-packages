Ext.define('CFJS.plugins.service.view.ServicesView', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.view.Services',
	xtype				: 'plugins.service.view',
	requires			: [
		'CFJS.plugins.service.grid.ServicePanel',
		'CFJS.plugins.service.panel.AirlineEditor', 
		'CFJS.plugins.service.panel.BusEditor', 
		'CFJS.plugins.service.panel.HotelEditor', 
		'CFJS.plugins.service.panel.InsuranceEditor', 
		'CFJS.plugins.service.panel.RailwayEditor', 
		'CFJS.plugins.service.panel.TravelEditor', 
		'CFJS.plugins.service.view.EditController',
		'CFJS.plugins.service.view.ServicesModel'
	],
	controller			: 'plugins.service.edit',
	layout				: { type: 'card', anchor: '100%' },
	session				: { schema: 'financial' },
	viewModel			: { type: 'plugins.service.list' },

	items				: [{
    	xtype		: 'servicegrid',
    	listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	
	lookupGrid: function() {
		return this.down('servicegrid');
	}
});
