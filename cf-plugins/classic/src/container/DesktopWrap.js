Ext.define('CFJS.plugins.container.DesktopWrap', {
	extend		: 'Ext.container.Container',
	xtype		: 'plugins.desktopwrap',
	requires 	: [ 'Ext.layout.container.Card' ],
	itemId		: 'contentPanel',
	layout		: {
		type			: 'card',
		anchor			: '100%',
		deferredRender	: true
	},
	
	beforeLayout : function() {
		var me = this;
		me.minHeight = Ext.Element.getViewportHeight() - 64;
		me.callParent(arguments);
	},
	
	getContentPanel: function() {
		return this;
	},

	getNavigator: function() {
//		return this.navigator;
	}
	
});
