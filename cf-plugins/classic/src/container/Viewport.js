Ext.define('CFJS.plugins.container.Viewport', {
	extend				: 'CFJS.container.Viewport',
	alternateClassName	: 'CFJS.plugins.Viewport',
	xtype				: 'plugins.viewport',
	requires 			: [ 
		'CFJS.plugins.container.DesktopWrap',
		'CFJS.plugins.view.ViewportController',
		'CFJS.plugins.view.ViewportModel'
	],
	controller			: 'plugins.viewport',
	desktopConfig		: {
		xtype		: 'plugins.desktopwrap',
		id			: 'main-view-detail-wrap',
		flex		: 1
	},
	toolbarConfig		: {
		cls		: 'viewport-headerbar toolbar-btn-shadow buttons-delete-focus plugin',
		height	: 64,
		itemId	: 'headerBar'
	},
	viewModel			: { type: 'plugins.viewport' },
	
	initTBButtons: function(buttons) {
		return [{
			action		: 'settings',
			iconCls		: CFJS.UI.iconCls.COG,
			href		: '#settings',
			hrefTarget	: '_self',
			tooltip		: 'Settings'
		},{
			action		: 'login',
			iconCls		: 'x-fa fa-sign-in',
			href		: '#login',
			hrefTarget	: '_self',
			tooltip		: 'Sign in'
		},{
			action		: 'lockScreen',
			iconCls		: 'x-fa fa-sign-out',
			handler		: 'onLockClick',
			tooltip		: 'Lock screen'
		},{
			xtype		: 'btnlocale',
			hideText	: true,
			minWidth	: 55
		},{
			xtype		: 'tbtext',
			bind		: { html: '<a href="#profile">{user.fullName}</a>', hidden: '{!user.fullName}'},
			cls			: 'top-user-name'
		},{
			xtype		: 'image',
			alt			: 'current user image',
			bind		: { src: '{user.photo}', hidden: '{!user.fullName}' },
			cls			: 'header-right-profile-image',
			height		: 36,
			width		: 36,
			listeners	: {
				click: {
					fn: function(e) {
					},
					element: 'img'
				}
			}
		}];
	}
	
});
