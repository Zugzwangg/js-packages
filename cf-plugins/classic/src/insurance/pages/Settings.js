Ext.define('CFJS.plugins.insurance.pages.Settings', {
	extend		: 'CFJS.panel.BaseBrowser',
	xtype		: 'insurance.page.settings',
	requires 	: [
		'CFJS.store.Named',
		'CFJS.model.insurance.InsuranceProgram',
		'CFJS.plugins.insurance.baggage.view.SettingsPanel',
		'CFJS.plugins.insurance.casualty.view.SettingsPanel',
		'CFJS.plugins.insurance.civilliability.view.SettingsPanel',
		'CFJS.plugins.insurance.health.view.SettingsPanel'
	],
	config: {
		actions		: {
			addRecord	: {
				iconCls		: CFJS.UI.iconCls.ADD,
				title		: 'Add record',
				tooltip		: 'Add record',
				handlerFn	: 'onAddRecord'
			},
			copyRecord	: {
				iconCls		: CFJS.UI.iconCls.COPY,
				title		: 'Copy record',
				tooltip		: 'Copy selected record',
				handlerFn	: 'onCopyRecord'
			},
			reloadStore	: {
				iconCls		: CFJS.UI.iconCls.REFRESH,
    			title		: 'Update the data',
    			tooltip		: 'Update the data',
    			handlerFn	: 'onReloadStore'
			},
			removeRecord: {
				iconCls		: CFJS.UI.iconCls.DELETE,
    			title		: 'Delete records',
    			tooltip		: 'Remove selected records',
				handlerFn	: 'onRemoveRecords'
			},
			saveRecord	: {
				formBind	: true,
				iconCls		: CFJS.UI.iconCls.SAVE,
    			title		: 'Save the changes',
				tooltip		: 'Save the changes',
				handlerFn	: 'onSaveRecords'
			}
		},
		actionsMap	: {
			contextMenu	: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'menuseparator', 'saveRecord' ] },
			toolBar		: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'tbseparator', 'saveRecord', 'reloadStore' ] }
		},
		contextMenu	: { items: new Array(5) },
		toolBar		: { items: new Array(6), padding: 0 },
	},
	filterConfig: null,
	iconCls		: CFJS.UI.iconCls.COGS,
	tabIdPrefix	: 'program',
	title		: 'Settings',
	
	createChildTab: function(record) {
		var me = this, tab = me.callParent(arguments), xtype;
		if (tab) {
			switch (record.id) {
			case 1:
				xtype = 'baggage.view.settings';
				break;
			case 3:
				xtype = 'health.view.settings';
				break;
			case 4:
				xtype = 'casualty.view.settings';
				break;
			case 5:
				xtype = 'civilliability.view.settings';
				break;
			default:
				break;
			}
			tab = me.add(Ext.apply(tab, {
				iconCls	: record.get('iconCls') || CFJS.UI.iconCls.COG,
				xtype	: xtype || tab.xtype
			}));
			tab.on({
				tabchange: function(tabPanel, newCard, oldCard) {
					var controller = tabPanel.lookupController();
					if (controller && Ext.isFunction(controller.initActionable)) controller.initActionable(me);
				}
			});
			me.relayEvents(tab, [ 'itemcontextmenu' ]);
		}
		return tab;
	},

	createBrowserView: function(cfg) {
		return Ext.apply(cfg, {
			iconCls			: CFJS.UI.iconCls.HOME,
			reorderable		: false,
			store			: {
				type	: 'named',
				autoLoad: true,
				model	: 'InsuranceProgram',
				proxy	: { loaderConfig: { dictionaryType: 'insurance_program' }, service: 'iss' },
				sorters	: [ 'name' ]
			}
		})
	}

});