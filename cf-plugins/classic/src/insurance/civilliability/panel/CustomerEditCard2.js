Ext.define('CFJS.plugins.insurance.civilliability.panel.CustomerEditCard2', {
	extend		: 'Ext.form.Panel',
	xtype		: 'civilliability.customeredit-card2',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.container.Container',
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.civilliability.view.CustomerController',
		'CFJS.plugins.insurance.civilliability.view.CustomerModel'
	],
	bodyPadding	: 10,
	defaultType	: 'container',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			detailVIN: {
				defaultType	: 'textfield',
				defaults	: { anchor: '100%', columnWidth: 0.25, defaultType: 'textfield', layout: 'column' },
				layout		: 'column',
				order		: 1,
				items		: [{
					bind			: '{_itemName_.bodyNumber}',
					emptyText		: 'XXXXXXXXXXXXXXXXX',
					enforceMaxLength: true,
					fieldLabel		: 'Body number (VIN)',
					margin			: 0,
					maxLength		: 17,
					name			: 'bodyNumber'
				},{
					bind			: '{_itemName_.typeVehicleCode}',
					fieldLabel		: 'Код',
					readOnly 		: true
				}]
			},
			detailMarka: {
				defaultType	: 'textfield',
				defaults	: { anchor: '100%', columnWidth: 0.25, defaultType: 'textfield', layout: 'column' },
				layout		: 'column',
				order		: 2,
				items		: [{
					xtype			: 'dictionarycombo',
					bind			: { store: '{carBrands}', value : '{carBrand}' },
					emptyText		: 'select car brand',
					fieldLabel		: 'Brand',
					margin			: 0,
					name			: 'carBrand',
					publishes		: 'value'
				},{
					xtype			: 'dictionarycombo',
					bind			: { store: '{carModels}', value: '{_itemName_.carModel}' },
					emptyText		: 'select car model',
					fieldLabel		: 'Model',
					name			: 'carModel',
					publishes		: 'value'
				}]
			},
			detailYearNumber: {
				defaultType	: 'textfield',
				defaults	: { anchor: '100%', columnWidth: 0.25, layout: 'column' },
				layout		: 'column',
				order		: 3,
				items		: [{
					xtype			: 'combobox',
					bind			: { value: '{_itemName_.yearIssue}' },
					displayField	: 'year',
					emptyText		: 'select year of issue',
					fieldLabel		: 'Year of issue',
					forceSelection	: true,
					margin			: 0,
					name			: 'yearIssue',
					publishes		: 'value',
					queryMode		: 'local',
					store			: 'years'
				},{
					bind			: '{_itemName_.plateNumber}',
			    	enforceMaxLength: true,
			    	fieldLabel		: 'License plate',
					flex			: 2,
					maxLength		: 10,
					name			: 'plateNumber'
				}]
			},
			nextControl: {
				defaults	: { anchor: '100%', columnWidth: 0.25, defaultType: 'datefield', layout: 'column' },
				layout		: 'column',
				order	: 4,
				items	: [{
					xtype		: 'datefield',
					bind		: { value: '{_itemName_.dateNextControl}' },
					fieldLabel	: 'Date of the next RTC',
					margin		: 0,
					minValue	: 'dateTodayAddOneDay',
					name		: 'dateNextControl',
					validator	: 'validatorNextControl'
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Individual'

});

