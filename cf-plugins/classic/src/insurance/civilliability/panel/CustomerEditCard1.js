Ext.define('CFJS.plugins.insurance.civilliability.panel.CustomerEditCard1', {
	extend		: 'Ext.form.Panel',
	xtype		: 'civilliability.customeredit-card1',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.container.Container',
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.civilliability.view.CustomerControllerWizard',
		'CFJS.plugins.insurance.civilliability.view.CustomerModelWizard'
	],
	bodyPadding	: 10,
	defaultType	: 'container',
	defaults	: {
		anchor		: '100%',
		defaultType	: 'textfield',
		layout		: 'hbox'
	},
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			computing	: {
				defaultType	: 'displayfield',
				defaults	: { flex: 1, required: false },
				order		: 10,
				items		:[{
					bind		: '{baseAmount}',
					fieldLabel	: 'Base',
					name		: 'baseAmount'
				},{
					bind		: '{tariff.k1}',
					fieldLabel	: 'K1',
					name		: 'k1'
				},{
					bind		: '{zoneK2}',
					fieldLabel	: 'K2',
					name		: 'k2'
				},{
					bind		: '{areaK3}',
					fieldLabel	: 'K3',
					name		: 'k3'
				},{
					bind		: '{experienceK4}',
					fieldLabel	: 'K4',
					name		: 'k4'
				},{
					bind		: '{durationK5}',
					fieldLabel	: 'K5',
					name		: 'k5'
				},{
					bind		: '{fraudK6}',
					fieldLabel	: 'K6',
					name		: 'k6'
				},{
					bind		: '{validityK7Wiz}',
					fieldLabel	: 'K7',
					name		: 'k7'
				},{
					bind		: '{bonusMalusK8}',
					fieldLabel	: 'K8',
					name		: 'k8'
				},{
					bind		: '{volumeDiscountsK9}',
					fieldLabel	: 'K9',
					name		: 'k9'
				},{
					bind		: '{benefitsK10Wiz}',
					fieldLabel	: 'K10',
					name		: 'k10'
				},{
					bind		: '{calcAmountWiz}',
					fieldLabel	: 'Amount',
					name		: 'calcAmount'
				}]
			},
			insuranceObject: {
				defaultType	: 'container',
				defaults	: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
				layout		: 'anchor',
				order		: 8,
				items		: [{
					items: [{
						xtype			: 'fieldcontainer',
						combineErrors	: true,
						flex			: 2,
						layout			: 'column',
						items			: [{
							xtype		: 'dictionarycombo',
							bind		: { store: '{typeVehicles}', value: '{_itemName_.typeVehicle}' },
							fieldLabel	: 'Type of vehicle',
							columnWidth	: 1,
							listConfig	: { itemTpl:'{code}. {name}' },
							margin		: 0,
							name		: 'typeVehicle',
							reference	: 'typeVehicle',
							publishes	: 'value',
							queryMode	: 'local'
						},{
							xtype			: 'numberfield',
							allowDecimals	: false,
							allowExponential: false,
							bind			: { hidden: '{!typeVehicle.value.capacity}', fieldLabel: '{typeVehicle.value.capacity.name}', value: '{_itemName_.capacityVehicle}' },
							hidden			: true,
							hideTrigger		: true,
							minValue		: 0,
							name			: 'capacityVehicle'
						},{
							xtype			: 'displayfield',
							bind			: '{_itemName_.typeVehicleCode}',
							fieldLabel		: 'Код',
							width			: 80
						}]
					}]
				},{
					items: [{
						xtype		: 'dictionarycombo',
						bind		: { value: '{registrationCountry}' },
						emptyText	: 'choose the country',
			            fieldLabel	: 'Country of registration',
						flex		: 2,
						name		: 'country',
						publishes	: 'value',
						store		: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } }
					},{
						xtype		: 'dictionarycombo',
						autoLoadOnValue: true,
						bind		: { store: '{cities}', value: '{_itemName_.registrationCity}' },
						emptyText	: 'select city',
				    	fieldLabel	: 'City of registration',
						flex		: 2,
				    	name		: 'registrationCity',
						publishes	: 'value'
					},{
						xtype		: 'textfield',
						bind		: '{_itemName_.mreo}',
				    	fieldLabel	: 'IRED',
				    	name		: 'mreo',
						width		: 80
					},{
						xtype		: 'displayfield',
						bind		: '{_parentItemName_.zone}',
				    	fieldLabel	: 'Zone',
				    	name		: 'zone',
						width		: 80
					}]
				}]
			},
			period: {
				defaultType	: 'datetimefield',
				defaults	: { flex: 3 },
				order		: 5,
				items		: [{
					bind		: '{_parentItemName_.dateFrom}',
					emptyText	: 'start of action',
					fieldLabel	: 'Start of action',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: 0.5, margin: 0 },
					fieldTime	: { columnWidth: 0.5 },
					name		: 'dateFrom',
					listeners	: { change: 'onChangeDateFrom' }
				},{
					bind		: '{_parentItemName_.dateTo}',
					emptyText	: 'expiration',
					fieldLabel	: 'Expiration',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: 0.5, margin: 0 },
					fieldTime	: { columnWidth: 0.5 },
					name		: 'dateTo'
				},{
					xtype				: 'checkbox',
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="The document is preferential">*</span>'],
					bind				: '{_itemName_.hasPrivilege}',
					name				: 'hasPrivilege',
					fieldLabel			: 'Preferential',
					flex				: 1
				},{
					xtype			: 'dictionarycombo',
					bind			: { store: '{bonusMaluses}', value: '{_itemName_.bonusMalus}' },
					displayField	: 'rate',
					editable		: false,
					emptyText		: 'select the bonus malus',
					fieldLabel		: 'Bonus Malus',
					listConfig		: { itemTpl: '{rate}. {code}' },
					name			: 'bonusMalus',
					publishes		: 'value',
					queryMode		: 'local',
					selectOnFocus	: false,
					typeAhead		: false,
					valueField		: 'code'
				}]
			},
			useProperties: {
				defaultType	: 'checkbox',
				defaults	: { required: false },
				order	: 9,
				items	: [{
					xtype				: 'numberfield',
					bind				: '{_itemName_.vehiclesNumber}',
					fieldLabel			: 'Number of vehicles',
					flex				: 1,
					minValue			: 1,
					name				: 'vehiclesNumber'
				},{
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="The car is used in a taxi">*</span>'],
					bind				: '{_itemName_.isTaxi}',
					fieldLabel			: 'Taxi',
					name				: 'isTaxi'
				},{
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Drivers are allowed to drive with a driving experience of less than 3 years">*</span>'],
					bind				: '{_itemName_.useNovice}',
					fieldLabel			: 'Use beginners',
					name				: 'useNovice'
				},{
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="The insured was spotted in fraud">*</span>'],
					bind				: '{_itemName_.hasFraud}',
					fieldLabel			: 'Fraud',
					name				: 'hasFraud'
				},{
					xtype				: 'manyofmanyfield',
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="For annual contracts">*</span>'],
					bind				: '{_itemName_.useMonths}',
					fieldLabel			: 'Unused months',
					format				: '1;2;3;4;5;6;7;8;9;10;11;12',
					name				: 'useMonths',
					listeners			: { change: 'onChangeUseMonths' }
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Individual'
	
});

