Ext.define('CFJS.plugins.insurance.civilliability.panel.CustomerEditWizard', {
	extend : 'CFJS.plugins.insurance.wizard.BaseForm',
	xtype: 'civilliability.wizard',
	requires: [
		'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard1',	           
		'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard2',	           
		'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard3'
	],

	layout  	: 'card',
	controller	: 'plugins.insurance.civilliability.customer-wizard',
	viewModel	: { type: 'plugins.insurance.civilliability.customer-wizard' },
	items		: [{
    	xtype			: 'civilliability.customeredit-card1'
	},{
    	xtype			: 'civilliability.customeredit-card2'
	},{
    	xtype			: 'civilliability.customeredit-card3',
        isSaveService 	: 1
	}]

});
