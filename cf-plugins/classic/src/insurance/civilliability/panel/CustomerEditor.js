Ext.define('CFJS.plugins.insurance.civilliability.panel.CustomerEditor', {
	extend		: 'Ext.form.Panel',
	xtype		: 'civilliability.customereditor',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.container.Container',
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.civilliability.view.CustomerController',
		'CFJS.plugins.insurance.civilliability.view.CustomerModel'
	],
	bodyPadding	: 10,
	controller	: 'plugins.insurance.civilliability.customer',
	defaultType	: 'container',
	defaults	: {
		anchor		: '100%',
		defaultType	: 'textfield',
		layout		: 'hbox'
	},
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			consumerDetail: {
				order			: 7,
				items: [{
					xtype			: 'combobox',
					bind			: { store: '{schemes}', value: '{_parentItemName_.scheme}'},
					displayField	: 'name',
					editable		: false,
					emptyText		: 'выберите схему',
					fieldLabel		: 'Схема страхования',
					flex			: 1,
					forceSelection	: true,
					margin			: 0,
					name			: 'scheme',
					publishes		: 'value',
					queryMode		: 'local',
					valueField		: 'code'
				},{
					xtype			: 'combobox',
					bind			: { store: '{customerIDs}', value: '{customerId}' },
					displayField	: 'id',
					emptyText		: 'выберите документ',
					fieldLabel		: 'Документ',
					flex			: 1,
					forceSelection	: true,
					listConfig		: { itemTpl: '{type.name}: {id} ' },
					minChars		: 0,
					name			: 'customerId',
					publishes		: 'value',
					selectOnFocus	: true,
					valueField		: 'id'
				},{
					xtype				: 'checkbox',
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Документ является льготным">*</span>'],
					bind				: '{_itemName_.hasPrivilege}',
					name				: 'hasPrivilege',
					fieldLabel			: 'Льготный'
				}]
			},
			computing	: {
				defaultType	: 'displayfield',
				defaults	: { flex: 1, required: false },
				order		: 10,
				items		:[{
					bind		: '{baseAmount}',
					fieldLabel	: 'Base',
					name		: 'baseAmount'
				},{
					bind		: '{tariff.k1}',
					fieldLabel	: 'K1',
					name		: 'k1'
				},{
					bind		: '{zoneK2}',
					fieldLabel	: 'K2',
					name		: 'k2'
				},{
					bind		: '{areaK3}',
					fieldLabel	: 'K3',
					name		: 'k3'
				},{
					bind		: '{experienceK4}',
					fieldLabel	: 'K4',
					name		: 'k4'
				},{
					bind		: '{durationK5}',
					fieldLabel	: 'K5',
					name		: 'k5'
				},{
					bind		: '{fraudK6}',
					fieldLabel	: 'K6',
					name		: 'k6'
				},{
					bind		: '{validityK7}',
					fieldLabel	: 'K7',
					name		: 'k7'
				},{
					bind		: '{bonusMalusK8}',
					fieldLabel	: 'K8',
					name		: 'k8'
				},{
					bind		: '{volumeDiscountsK9}',
					fieldLabel	: 'K9',
					name		: 'k9'
				},{
					bind		: '{benefitsK10}',
					fieldLabel	: 'K10',
					name		: 'k10'
				},{
					bind		: '{calcAmount}',
					fieldLabel	: 'Amount',
					name		: 'calcAmount'
				}]
			},
			insuranceAmount: {
				defaultType	: 'numberfield',
				order		: 6,
				items		: [{
					xtype			: 'numberfield',
					bind			: '{_parentItemName_.insuranceAmount}',
					fieldLabel		: 'Страховая сумма (имущество)',
					flex			: 1,
					margin			: 0,
					name			: 'insuranceAmount'
				},{
					xtype			: 'numberfield',
					bind			: '{_itemName_.healthAmount}',
					fieldLabel		: 'Страховая сумма (здоровье)',
					flex			: 1,
					name			: 'healthAmount'
				},{
					xtype			: 'combobox',
					bind			: { store: '{franchises}', value: '{_parentItemName_.franchise}'},
					displayField	: 'amount',
					editable		: false,
					emptyText		: 'выберите размер франшизы',
					fieldLabel		: 'Размер франшизы',
					flex			: 1,
					forceSelection	: true,
					name			: 'franchise',
					publishes		: 'value',
					queryMode		: 'local'
				},{
					xtype			: 'dictionarycombo',
					bind			: { value: '{_parentItemName_.insuranceCurrency}' },
					displayField	: 'code',
					editable		: false,
					emptyText		: 'выберите валюту',
					fieldLabel		: 'Валюта',
					name			: 'insuranceCurrency',
					publishes		: 'value',
					selectOnFocus	: false,
					typeAhead		: false,
					store			: { type: 'currencies' },
					width			: 85
				},{
					xtype			: 'dictionarycombo',
					bind			: { store: '{bonusMaluses}', value: '{_itemName_.bonusMalus}' },
					displayField	: 'rate',
					editable		: false,
					emptyText		: 'выберите бонус-малус',
					fieldLabel		: 'Бонус-Малус',
					flex			: 1,
					listConfig		: { itemTpl: '{rate}. {code}' },
					name			: 'bonusMalus',
					publishes		: 'value',
					queryMode		: 'local',
					selectOnFocus	: false,
					typeAhead		: false,
					valueField		: 'code'
				}]
			},
			insuranceObject: {
				defaultType	: 'container',
				defaults	: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
				layout		: 'anchor',
				order		: 8,
				items		: [{
					items: [{
						bind			: '{_itemName_.bodyNumber}',
						emptyText		: 'XXXXXXXXXXXXXXXXX',
						enforceMaxLength: true,
						fieldLabel		: 'Номер кузова (VIN)',
						margin			: 0,
						maxLength		: 17,
						name			: 'bodyNumber'
					},{
						xtype			: 'fieldcontainer',
						combineErrors	: true,
						flex			: 2,
						layout			: 'hbox',
						items			: [{
							xtype		: 'dictionarycombo',
							bind		: { store: '{typeVehicles}', value: '{_itemName_.typeVehicle}' },
							fieldLabel	: 'Тип ТТ',
							flex		: 1,
							listConfig	: { itemTpl:'{code}. {name}' },
							margin		: 0,
							name		: 'typeVehicle',
							reference	: 'typeVehicle',
							publishes	: 'value',
							queryMode	: 'local'
						},{
							xtype			: 'numberfield',
							allowDecimals	: false,
							allowExponential: false,
							bind			: { hidden: '{!typeVehicle.value.capacity}', fieldLabel: '{typeVehicle.value.capacity.name}', value: '{_itemName_.capacityVehicle}' },
							hidden			: true,
							hideTrigger		: true,
							minValue		: 0,
							name			: 'capacityVehicle',
							width			: 80
						},{
							xtype		: 'displayfield',
							bind		: { hidden: '{!typeVehicle.value}', value: '{_itemName_.typeVehicleCode}' },
							hidden		: true,
							fieldLabel	: 'Код',
							width		: 80
						}]
					},{
						xtype		: 'dictionarycombo',
						bind		: { store: '{carBrands}', value : '{carBrand}' },
						emptyText	: 'выберите марку автомобиля',
						fieldLabel	: 'Марка',
						flex		: 1,
						name		: 'carBrand',
						publishes	: 'value'
					},{
						xtype		: 'dictionarycombo',
						bind		: { store: '{carModels}', value: '{_itemName_.carModel}' },
						emptyText	: 'выберите модель автомобиля',
						fieldLabel	: 'Модель',
						flex		: 1,
						name		: 'carModel',
						publishes	: 'value'
					}]
				},{
					items: [{
						xtype			: 'combobox',
						bind			: { value: '{_itemName_.yearIssue}' },
						displayField	: 'year',
						emptyText		: 'выберите год выпуска',
						fieldLabel		: 'Год выпуска',
						flex			: 1,
						forceSelection	: true,
						margin			: 0,
						name			: 'yearIssue',
						publishes		: 'value',
						queryMode		: 'local',
						store			: 'years'
					},{
						xtype		: 'dictionarycombo',
						bind		: { value: '{registrationCountry}' },
						emptyText	: 'выберите страну',
			            fieldLabel	: 'Страна регистрации',
						flex		: 2,
						name		: 'country',
						publishes	: 'value',
						store		: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } }
					},{
						xtype		: 'dictionarycombo',
						autoLoadOnValue: true,
						bind		: { store: '{cities}', value: '{_itemName_.registrationCity}' },
						emptyText	: 'выберите город',
				    	fieldLabel	: 'Город регистрации',
						flex		: 2,
				    	name		: 'registrationCity',
						publishes	: 'value'
					},{
						xtype		: 'textfield',
						bind		: '{_itemName_.mreo}',
				    	fieldLabel	: 'МРЭО',
				    	name		: 'mreo',
				    	required	: false,
						width		: 80
					},{
						xtype		: 'displayfield',
						bind		: '{_parentItemName_.zone}',
				    	fieldLabel	: 'Зона',
				    	name		: 'zone',
						width		: 80
					},{
						bind			: '{_itemName_.plateNumber}',
				    	enforceMaxLength: true,
				    	fieldLabel		: 'Номерной знак',
						flex			: 2,
						maxLength		: 10,
						name			: 'plateNumber'
					}]
				}]
			},
			period: {
				defaultType	: 'datetimefield',
				defaults	: { flex: 1 },
				order		: 5,
				items		: [{
					bind		: '{_parentItemName_.dateContract}',
					emptyText	: 'дата договора',
					fieldLabel	: 'Дата договора',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: 0.5, margin: 0 },
					fieldTime	: { columnWidth: 0.5 },
					margin		: 0,
					name		: 'dateContract'
				},{
					xtype			: 'fieldcontainer',
					combineErrors	: true,
					defaultType		: 'textfield',
					fieldLabel		: 'Стикер',
					layout			: 'column',
					items			: [{
						bind			: '{_itemName_.stickerSeries}',
						emptyText		: 'XX',
						enforceMaxLength: true,
						columnWidth		: 0.4,
						margin			: 0,
						maxLength		: 2,
						name			: 'stickerSeries'
					},{
						bind			: '{_itemName_.stickerNumber}',
						emptyText		: 'XXXXXXXXXX',
						enforceMaxLength: true,
						columnWidth		: 0.6,
						maxLength		: 10,
						name			: 'stickerNumber'
					}]
				},{
					bind		: '{_parentItemName_.dateFrom}',
					emptyText	: 'начало действия',
					fieldLabel	: 'Начало действия',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: 0.5, margin: 0 },
					fieldTime	: { columnWidth: 0.5 },
					name		: 'dateFrom',
					listeners	: { change: 'onChangeDateFrom' }
				},{
					bind		: '{_parentItemName_.dateTo}',
					emptyText	: 'окончание действия',
					fieldLabel	: 'Окончание действия',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: 0.5, margin: 0 },
					fieldTime	: { columnWidth: 0.5 },
					name		: 'dateTo'
				}]
			},
			useProperties: {
				defaultType	: 'checkbox',
				defaults	: { required: false },
				order	: 9,
				items	: [{
					xtype		: 'datefield',
					bind		: { value: '{_itemName_.dateNextControl}' },
					fieldLabel	: 'Дата следующего ОТК',
					flex		: 1,
					margin		: 0,
					minValue	: 'dateTodayAddOneDay',
					name		: 'dateNextControl',
					validator	: 'validatorNextControl'
				},{
					xtype				: 'numberfield',
					bind				: '{_itemName_.vehiclesNumber}',
					fieldLabel			: 'Количество ТС',
					flex				: 1,
					name				: 'vehiclesNumber'
				},{
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Автомобиль используется в такси">*</span>'],
					bind				: '{_itemName_.isTaxi}',
					fieldLabel			: 'Такси',
					name				: 'isTaxi'
				},{
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="К вождению допускаются особы с водительским стажем меньше 3-х лет">*</span>'],
					bind				: '{_itemName_.useNovice}',
					fieldLabel			: 'Используют новички',
					name				: 'useNovice'
				},{
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Страхователь был замечен в мошенничестве">*</span>'],
					bind				: '{_itemName_.hasFraud}',
					fieldLabel			: 'Мошенничество',
					name				: 'hasFraud'
				},{
					xtype				: 'manyofmanyfield',
					afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Для годовых договоров">*</span>'],
					bind				: '{_itemName_.useMonths}',
					fieldLabel			: 'Неиспользуемые месяцы',
					format				: '1;2;3;4;5;6;7;8;9;10;11;12',
					name				: 'useMonths',
					listeners			: { change: 'onChangeUseMonths' }
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Individual',
	viewModel	: { type: 'plugins.insurance.civilliability.customer' },

	privates: {
		
		decodeList: function(config, list, processFn) {
			config = config || {};
			if (list) {
				list = Ext.decode(list['$']);
				if (Ext.isArray(list) && list.length > 0) {
					list.forEach(function(item) {
						processFn(config, item);
					});
				}
			}
			return config;
		},
		
		applyViewModel: function(vm) {
			if (vm && !vm.isViewModel) {
				var me = this, formulasCfg, storesCfg, properties = vm.program ? vm.program.get('properties') : null,
					listFormulas = properties ? properties.field_listFormulas : null,
					listStores = properties ? properties.field_listAddStores : null,
					cfg;
				if (properties) {
					cfg = me.decodeList(cfg, listFormulas, function(config, item) {
						(config.formulas = config.formulas || {})[item.name] = item.value;
		    		});
					cfg = me.decodeList(cfg, listStores, function(config, item) {
						if (item.name !== 'properties') (config.stores = config.stores || {})[item.name] = item;
		    		});
				}
				vm = Ext.apply(vm, cfg);
			}
			return this.callParent([vm]);
		}
	}
	
});

