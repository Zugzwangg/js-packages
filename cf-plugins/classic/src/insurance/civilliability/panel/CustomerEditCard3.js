Ext.define('CFJS.plugins.insurance.civilliability.panel.CustomerEditCard3', {
	extend		: 'Ext.form.Panel',
	xtype		: 'civilliability.customeredit-card3',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.container.Container',
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.civilliability.view.CustomerController',
		'CFJS.plugins.insurance.civilliability.view.CustomerModel'
	],
	bodyPadding	: 10,
	defaultType	: 'container',
	defaults	: {
		anchor		: '100%',
		defaultType	: 'textfield',
		layout		: 'hbox'
	},
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			mainBlock	: {
				order		: 2,
				items		: [{
					bind		: '{_parentItemName_.number}',
					emptyText	: 'document number',
					fieldLabel	: 'Document number',
					flex		: 1,
					margin		: 0,
					name		: 'number'
				},{
					bind		: '{_parentItemName_.extNumber}',
					emptyText	: 'order number',
					fieldLabel	: 'Order number',
					flex		: 1,
					name		: 'extNumber'
				}]
			},
			period: {
				defaultType	: 'datetimefield',
				defaults	: { flex: 1 },
				order		: 1,
				items		: [{
					bind		: '{_parentItemName_.dateContract}',
					emptyText	: 'agreement date',
					fieldLabel	: 'Agreement date',
					fieldOffset	: 5,
					fieldDate	: { columnWidth: 0.5, margin: 0 },
					fieldTime	: { columnWidth: 0.5 },
					margin		: 0,
					name		: 'dateContract'
				},{
					xtype			: 'fieldcontainer',
					combineErrors	: true,
					defaultType		: 'textfield',
					fieldLabel		: 'Sticker',
					layout			: 'column',
					items			: [{
						bind			: '{_itemName_.stickerSeries}',
						emptyText		: 'XX',
						enforceMaxLength: true,
						columnWidth		: 0.4,
						margin			: 0,
						maxLength		: 2,
						name			: 'stickerSeries'
					},{
						bind			: '{_itemName_.stickerNumber}',
						emptyText		: 'XXXXXXXXXX',
						enforceMaxLength: true,
						columnWidth		: 0.6,
						maxLength		: 10,
						name			: 'stickerNumber'
					}]
				}]
			},
			client	: {
				name		: 'rowConsumer',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				order		: 3,
				items		: [{
					xtype				: 'dictionarypicker',
					closeOnFocusLeave	: false,
					displayField		: 'fullName',
					emptyText			: 'select client',
					fieldLabel			: 'Client',
					labelStyle			: 'font-weight:bold',
					margin				: 0,
					name				: 'consumer',
					publishes			: 'value',
					rawValueData		: false,
					selectorWindow		: { xtype: 'admin-window-customer-picker' }
				},{
					xtype				: 'combobox',
					bind				: { store: '{customerIDs}', value: '{customerId}' },
					displayField		: 'id',
					emptyText			: 'select a document',
					fieldLabel			: 'Document',
					forceSelection		: true,
					listConfig			: { itemTpl: '{type.name}: {id} ' },
					minChars			: 0,
					publishes			: 'value',
					selectOnFocus		: true,
					valueField			: 'id'
				}]
			},
			consumerDetail: {
				order			: 5,
				defaults		: { flex: 1, margin: '0 0 0 5' },
				items: [{
					xtype		: 'checkbox',
					bind		: '{_itemName_.hasPrivilege}',
					fieldLabel	: 'Preferential',
					margin		: 0,
					name		: 'hasPrivilege',
					required	: true
				},{
					xtype		: 'dictionarycombo',
					bind		: { value: '{_parentItemName_.supplier}' },
					emptyText	: 'select insurer',
					fieldLabel	: 'Insurer',
					minChars 	: 0,
					name		: 'insurer',
					publishes	: 'value',
					store		: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'company' } },
						sorters	: [ 'name' ]
					}
				}]
			},
			insuranceAmount: {
				defaultType	: 'numberfield',
				order		: 7,
				items		: [{
					xtype		: 'numberfield',
					bind		: '{_parentItemName_.insuranceAmount}',
					fieldLabel	: 'Sum insured (property)',
					flex		: 1,
					margin		: 0,
					name		: 'insuranceAmount'
				},{
					xtype		: 'numberfield',
					bind		: '{_itemName_.healthAmount}',
					fieldLabel	: 'Sum insured (health)',
					flex		: 1,
					name		: 'healthAmount'
				},{
					xtype			: 'combobox',
					bind			: { store: '{franchises}', value: '{_parentItemName_.franchise}'},
					displayField	: 'amount',
					editable		: false,
					emptyText		: 'select the size of the franchise',
					fieldLabel		: 'The size of the franchise',
					flex			: 1,
					forceSelection	: true,
					name			: 'franchise',
					publishes		: 'value',
					queryMode		: 'local'
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Individual',

	afterRender: function() {
		var me = this, vm = me.lookupViewModel(),
			picker = me.down('dictionarypicker[name="consumer"]');
		if (picker) {
			vm.bind('{_parentItemName_.consumer}', function(consumer) {
				picker.setValue(consumer && Ext.clone(consumer));
			});
			picker.on({
				select: function(field, value) {
					var service = vm.getService();
					if (service && service.isModel) {
						if (value) {
							if (value.isModel) value = value.getData();
							value = Ext.applyIf({
								customer: { id: value.id, name: value.fullName }
							}, CFJS.model.dictionary.Consumer.transform(value));
						}
						service.set('consumer', value);
					}
				}
			});
		}
		me.callParent(arguments);
	}
	
});

