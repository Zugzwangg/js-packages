Ext.define('CFJS.plugins.insurance.civilliability.view.SettingsPanel', {
	extend			: 'CFJS.plugins.insurance.panel.SettingsPanel',
	xtype			: 'civilliability.view.settings',
	requires		: [
		'CFJS.plugins.insurance.civilliability.view.SettingsController',
		'CFJS.plugins.insurance.civilliability.view.SettingsModel'
	],
	controller		:'plugins.insurance.civilliability.settings',
    filterOptZone	: [
	    [ 1, 'Zone 1' ],
	    [ 2, 'Zone 2' ],
	    [ 3, 'Zone 3' ],
	    [ 4, 'Zone 4' ],
	    [ 5, 'Zone 5' ],
	    [ 6, 'Zone 6' ],
	    [ 7, 'Zone 7' ]
	],
	title			: 'Settings civilliability',
    viewModel		: { type: 'plugins.insurance.civilliability.settings' },

    initEditors: function(me, vm, editors) {
		editors.splice(editors.length - 1, 0, 
			//--- Zones
			CFJS.apply({
				bind	: { store: '{places}' },
				iconCls	: CFJS.UI.iconCls.GLOBE,
				title	: 'Zones',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'zone',
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Zone',
					editor		: { 
						xtype			: 'combobox',
						allowBlank		: false,
						bind			: { store: '{zones}' },
						displayField	: 'name',
						editable		: false,
						forceSelection	: true,
						queryMode		: 'local',
						valueField		: 'id'
					},
					filter		: { type: 'list', options: me.filterOptZone },
					renderer	: me.modelRenderer('zones', 'id', 'name'),
	    			style		: { textAlign: 'center' },
					text     	: 'Zone'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'k2',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text     	: 'K2'
				}]
			}, me.gridPlace),
			//--- Cities
			CFJS.apply({
				bind	: { store: '{cityZones}' },
				iconCls	: 'x-fa fa-map-o',
				title	: 'Cities',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'city',
					editor		: { 
						xtype			: 'dictionarycombo',
						allowBlank		: false,
						emptyText		: 'select a city', 
						selectOnFocus	: true,
						store			: {
							type	: 'base',
							model	: 'CFJS.model.dictionary.City',
							proxy 	: {
								loaderConfig: { dictionaryType: 'city' },
								reader		: { rootProperty: 'response.transaction.list.city' }
							},
							filters	: [{ id: 'country', property: 'country.iataCode', type: 'string', operator: 'eq', value: 'UA' }],
							sorters	: [ 'name' ]
						}
					},
					sorter		: { sorterFn: 'sorterCityFn', direction: 'ASC' },
					filter		: { filterFn: 'filterCityFn', emptyText: 'Search for...' },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text		: 'City',
					tpl			: '<tpl if="city">{city.name}<tpl if="city.country && city.country.name"> ({city.country.name})</tpl></tpl>'
				},{
					align		: 'left',
					dataIndex	: 'zone',
					editor		: { 
						xtype			: 'combobox',
						allowBlank		: false,
						bind			: { store: '{zones}' },
						displayField	: 'name',
						editable		: false,
						forceSelection	: true,
						queryMode		: 'local',
						valueField		: 'id'
					},
					flex		: 1,
					filter		: { type: 'list', options: me.filterOptZone },
					renderer	: me.modelRenderer('zones', 'id', 'name'),
	    			style		: { textAlign: 'center' },
					text		: 'Zone'
				},{
					align		: 'left',
					dataIndex	: 'code',
	        		editor		: { selectOnFocus: true },
					filter		: { type: 'string', emptyText: 'Search for...' },
	        		flex		: 1,
	    			style		: { textAlign: 'center' },
	        		text		: 'Code'
				}]
			}, me.gridCities),
			//--- Type Vehicles
			CFJS.apply({
				bind	: { store: '{typeVehicles}' },
				iconCls	: 'x-fa fa-truck',
				title	: 'Type Vehicles',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'code',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Code'
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Name'
				},{
					align		: 'left',
					dataIndex	: 'capacity',
					editor		: { xtype: 'acefield', fieldLabel: 'Capacity', hideLabel: true },
					flex		: 1,
					renderer	: 'capacityRenderer',
	    			style		: { textAlign: 'center' },
					text     	: 'Capacity'
				}]
			}, me.gridTypeVehicle),
			//--- Car brands
			CFJS.apply({
				autoLoad: true,
				bbar	: [{ xtype: 'pagingtoolbar', bind: { store: '{carBrands}' }, displayInfo: true }],
				bind	: { store: '{carBrands}' },
				iconCls	: 'x-fa fa-trademark',
				title	: 'Car brands',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Name'
				}]
			}, me.gridCarBrands),
			//--- Car models
			CFJS.apply({
				autoLoad: true,
				bbar	: [{ xtype: 'pagingtoolbar', bind: { store: '{carModels}' }, displayInfo: true }],
				bind	: { store: '{carModels}' },
				iconCls	: CFJS.UI.iconCls.CAR,
				title	: 'Car models',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'brand',
					editor		: { 
						xtype			: 'dictionarycombo',
						allowBlank		: false,
						emptyText		: 'select a brand',
						selectOnFocus	: true,
						store			: {
							type	: 'carbrands',
							sorters	: [ 'name' ]
						}
					},
					filter		: { type: 'string', emptyText: 'Search for...' },
					flex		: 1,
					sorter		: 'brand.name',
	    			style		: { textAlign: 'center' },
					text		: 'Brand',
					tpl			: '<tpl if="brand">{brand.name}</tpl>'
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Name'
				}]
			}, me.gridCarModels),
			//--- Validity
			CFJS.apply({
				bind	: { store: '{validity}' },
				iconCls	: CFJS.UI.iconCls.CALENDAR,
				title	: 'Validity',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'period',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					renderer	: me.monthsRenderer,
	    			style		: { textAlign: 'center' },
					text     	: 'Period'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'rate',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Rate'
				}]
			}, me.gridValidity),
			//--- Duration of use
			CFJS.apply({
				bind	: { store: '{duration}' },
				iconCls	: CFJS.UI.iconCls.CALENDAR_CHECK,
				title	: 'Duration of use',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'period',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					renderer	: me.monthsRenderer,
	    			style		: { textAlign: 'center' },
					text		: 'Period'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'rate',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text		: 'Rate'
				}]
			}, me.gridDuration),
			//--- Bonus-Malus
			CFJS.apply({
				bind	: { store: '{bonusMaluses}' },
				iconCls	: 'x-fa fa-th-list',
				title	: 'Bonus-Malus',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'code',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text		: 'Code'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'rate',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Rate'
				}]
			}, me.gridBonusMalus)
		);
		//--- Tariffs
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{tariffs}' },
			iconCls	: CFJS.UI.iconCls.DATABASE,
			title	: 'Tariffs',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'vehicle',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{typeVehiclesCombo}' },
					displayField	: 'name',
					editable		: false,
					forceSelection	: true,
					listConfig		: { itemTpl:'{id}. {name}' },
					queryMode		: 'local',
					valueField		: 'id'
				},
				filter		: 'list',
				flex		: 1,
    			style		: { textAlign: 'center' },
				text     	: 'Type vehicle'
			},{
				align		: 'left',
				dataIndex	: 'zone',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{zones}' },
					displayField	: 'name',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'id'
				},
				flex		: 1,
				filter		: { type: 'list', options : me.filterOptZone },
				renderer	: me.modelRenderer('zones', 'id', 'name'),
    			style		: { textAlign: 'center' },
				text     	: 'Zone'
			},{
				align		: 'left',
				dataIndex	: 'person',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{persons}' },
					displayField	: 'name',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'id'
				},
				filter		: { type: 'list', options: [[0, 'Company'],[1, 'Human' ]] },
				flex		: 1,
				renderer	: me.modelRenderer('persons', 'id', 'name'),
    			style		: { textAlign: 'center' },
				text     	: 'Insurer type'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'k1',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				filter		: 'number',
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'K1'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'k3',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				filter		: 'number',
				format		: '00.00',
				selectOnFocus	: true,
    			style		: { textAlign: 'center' },
				text     	: 'K3'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'k3_taxi',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				filter		: 'number',
				format		: '00.00',
				selectOnFocus	: true,
    			style		: { textAlign: 'center' },
				text     	: 'K3 (taxi)'
			}]
		}, me.gridTariffs));
		//--- Volume discounts
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{volumeDiscounts}' },
			iconCls	: CFJS.UI.iconCls.SORT_ASC,
			title	: 'Volume discounts',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridVolumeDiscounts));
    	return editors;
	}
	
});
