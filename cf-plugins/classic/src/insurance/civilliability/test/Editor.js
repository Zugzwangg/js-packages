Ext.define('CFJS.plugins.insurance.civilliability.test.Editor', {
	extend: 'Ext.tab.Panel',
	xtype: 'civilliability.test.editor',
	requires: [
		'CFJS.plugins.insurance.civilliability.panel.CompanyEditor',
		'CFJS.plugins.insurance.civilliability.panel.CustomerEditor'
	],
	title: 'Civil liability',
	reference: 'civilliabilityTestEditor',
    items: [{
    	title	: 'Client', // заголовок первой закладки
    	iconCls : CFJS.UI.iconCls.FILE,
    	xtype	: 'civilliability.customereditor'
    },{
    	title	: 'Company',
    	iconCls : 'Entity',
    	xtype	: 'civilliability.company-editor'
    }]
});
