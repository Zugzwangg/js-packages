Ext.define('CFJS.plugins.insurance.casualty.view.SettingsPanel', {
	extend		: 'CFJS.plugins.insurance.panel.SettingsPanel',
	xtype		: 'casualty.view.settings',
	requires	: [
		'CFJS.plugins.insurance.casualty.view.SettingsController',
		'CFJS.plugins.insurance.casualty.view.SettingsModel'
	],
	controller	:'plugins.insurance.casualty.settings',
	title		: 'Settings casualty',
    viewModel	: { type: 'plugins.insurance.casualty.settings' },
    
    initEditors: function(me, vm, editors) {
		//--- Duration Trip
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{durationTrip}' },
			iconCls	: CFJS.UI.iconCls.CALENDAR,
			title	: 'Duration Trip',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min day'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max day'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridDurationTrip));
		//--- Scheme data
		editors.push(CFJS.apply({
			bind	: { store: '{schemeData}' },
			iconCls	: CFJS.UI.iconCls.DATABASE,
			title	: 'Tariffs',
			columns	: [{
				xtype		: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'durationId',
				editor		: { 
					xtype			: 'combobox',
					bind			: { store: '{durationTrip}' },
					displayField	: 'title',
					queryMode		: 'local',
					forceSelection	: true,
					selectOnFocus	: true,
					valueField		: 'id'
				},
				flex		: 1,
				renderer	: me.modelRenderer('durationTrip', 'id', 'title'),
    			style		: { textAlign: 'center' },
				text     	: 'Duration trip'
			},{
				align		: 'left',
				dataIndex	: 'amount',
				editor		: { 
					xtype			: 'combobox',
					bind			: { store: '{cashCover}' },
					displayField	: 'amount',
					queryMode		: 'local',
					forceSelection	: true,
					selectOnFocus	: true,
					valueField		: 'amount'
				},
				flex		: 1,
				renderer	: 'sumInsRenderer',
    			style		: { textAlign: 'center' },
				text     	: 'Cash cover'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '000.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridSchemeData));
		//--- Age limits
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{ageLimits}' },
			iconCls	: 'x-fa fa-heartbeat',
			title	: 'Age limits',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min age'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max age'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridAgeLimits));
		//--- Conditions
		editors.push(CFJS.apply({
			bind	: { store: '{conditions}' },
			iconCls	: 'x-fa fa-shield',
			title	: 'Conditions',
			columns	: [{
				xtype: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'name',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
    			style		: { textAlign: 'center' },
				text     	: 'Name condition'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'amount',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Amount'
			}]
		}, me.gridConditions));
		//--- Scheme conditions
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{schemeConditions}' },
			iconCls	: 'x-fa fa-life-ring',
			title	: 'Scheme conditions',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'conditionId',
				editor		: { 
					xtype			: 'combobox',
					bind			: { store: '{conditions}' },
					displayField	: 'name',
					queryMode		: 'local',
					forceSelection	: true,
					selectOnFocus	: true,
					valueField		: 'id'
				},
				flex		: 1,
				renderer	: 'conditionRenderer',
    			style		: { textAlign: 'center' },
				text     	: 'Condition'
			}]
		}, me.gridSchemeConditions));
		//--- Volume discounts
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{volumeDiscounts}' },
			iconCls	: 'x-fa fa-sort-amount-asc',
			title	: 'Volume discounts',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
				text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridVolumeDiscounts));
		//--- Zones
		editors.push(CFJS.apply({
			bind	: { store: '{zones}' },
			iconCls	: CFJS.UI.iconCls.GLOBE,
			title	: 'Zones',
			columns	: [{
				xtype: 'rownumberer'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'zone',
				editor		: { allowBlank: false, allowDecimals: false, selectOnFocus: true },
				format		: '0',
    			style		: { textAlign: 'center' },
				text     	: 'ID',
				width		: 60
			},{
				align		: 'left',
				dataIndex	: 'name',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
    			style		: { textAlign: 'center' },
				text     	: 'Name'
			},{
				xtype		: 'numbercolumn',
				dataIndex	: 'minCashCover',
				editor		: {
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{cashCover}' },
					displayField	: 'amount',
					editable		: false,
					emptyText		: 'specify amount',
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'amount'
				},
				format		: '0.00',
    			style		: { textAlign: 'center' },
				text     	: 'Minimum cash cover',
				width		: 160
			},{
				xtype		: 'templatecolumn',
				align		: 'center',
				dataIndex	: 'currency',
				editor		: { 
					xtype			: 'dictionarycombo',
					allowBlank		: false,
					displayField	: 'code',
					publishes		: 'value',
					selectOnFocus	: true,
					store			: { type: 'currencies' },
					triggerAction	: 'query'
				},
				filter		: { type: 'string', emptyText: 'Currency' },
				text     	: 'Currency',
				tpl			: '<tpl if="currency">{currency.code}</tpl>',
				width		: 90
			}]
		}, me.gridZones));
		return editors;
	}
    
});
