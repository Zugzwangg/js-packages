Ext.define('CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc1', {
	extend		: 'Ext.form.Panel',
	xtype		: 'casualty.customeredit-acc-card1',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.casualty.view.CustomerControllerWizard',
		'CFJS.plugins.insurance.casualty.view.CustomerModelWizard',
		'CFJS.plugins.insurance.view.PolicyConsumers'
	],
	defaultType		: 'fieldcontainer',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			schemeAmount: {
				defaults	: { editable: false, forceSelection: true, columnWidth: 0.25, margin: '0 0 0 5', publishes: 'value' },
				defaultType	: 'combobox',
				layout 		: 'column',
				margin		: '0 5',
				order		: 1,
				items		: [{
					bind			: { store: '{companyScheme}', value: '{_parentItemName_.scheme}'},
					displayField	: 'scheme',
					emptyText		: 'select the scheme',
					fieldLabel		: 'Insurance scheme',
					name			: 'scheme',
					margin			: 0,
					queryMode		: 'local',
					valueField		: 'scheme',
				},{
					bind			: { store: '{zones}', value: '{_parentItemName_.zone}'},
					displayField	: 'name',
					emptyText		: 'select a territory',
					fieldLabel		: 'Territory',
					name			: 'zone',
					queryMode		: 'local',
					valueField		: 'zone'
				},{
					bind			: { store: '{cashCover}', value: '{_parentItemName_.insuranceAmount}'},
					displayField	: 'amount',
					emptyText		: 'select amount',
					fieldLabel		: 'Amount',
					name			: 'insuranceAmount',
					queryMode		: 'local',
					valueField		: 'amount'
				}]
			},
			maindata: {
				defaultType	: 'numberfield',
				defaults	: { allowBlank: false,	allowDecimals: false, allowExponential: false, columnWidth: 0.25, margin: '0 0 0 5' },
				layout 		: 'column',
				margin		: '10 5 0',
				order		: 2,
				items		: [{
					bind		: '{_parentItemName_.duration}',
					emptyText	: 'number of days',
					fieldLabel	: 'Number of days',
					margin		: 0,
					minValue	: 1,
					name		: 'duration'
				},{
					bind		: '{consumersNumber}',
					emptyText	: 'number of persons',
					fieldLabel	: 'Number of persons',
					minValue	: 1,
					name		: 'countsInsurance'
				},{
					bind		: '{averageAge}',
					emptyText	: 'average age',
					fieldLabel	: 'Average age',
					minValue	: 1,
					name		: 'averageAge'
				}]
			},
			conditions: {
				xtype		: 'manyofmanyfield',
				bind		: { format: '{conditionsArr}', value: '{_itemName_.addConditions}' },
				columns		: 1,
				defaultType	: 'checkbox',
				margin		: '0 0 0 5',
				fieldLabel	: 'Additional conditions',
				order		: 3
			},
			totalAmount: {
				defaultType	: 'numberfield',
				defaults	: { allowBlank: false, columnWidth: 0.25, margin: '0 0 0 5' },
				layout 		: 'column',
				margin		: '10 5 0',
				order		: 4,
				items		: [{
					bind		: '{calcQuick}',
					emptyText	: 'amount of insurance',
					fieldLabel	: 'Amount of insurance',
					margin		: 0,
					name		: 'calcQuick',
					readOnly 	: true
				},{
					xtype		: 'textfield',
					bind		: '{_parentItemName_.insuranceCurrency.code}',
					columnWidth	: 0.125,
					emptyText	: 'currency',
					fieldLabel	: 'Currency',
					name		: 'insuranceCurrency',
					readOnly 	: true
				},{
					xtype			: 'dictionarycombo',
					bind			: { fieldLabel: 'Insurer', value: '{_parentItemName_.supplier}' },
					emptyText		: 'select insurer',
					minChars 		: 0,
					name			: 'insurer',
					publishes		: 'value',
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'company' } },
						sorters	: [{ property: 'name'}]
					},
					allowBlank: true
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Casualty insurance calculator'
	
});

