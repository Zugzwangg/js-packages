Ext.define('CFJS.plugins.insurance.casualty.panel.CustomerEditWizard', {
	extend : 'CFJS.plugins.insurance.wizard.BaseForm',
	xtype: 'casualty.wizard',
	requires: [
		'CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc1',	           
		'CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc2'	           
	],

	layout  	: 'card',
	controller	: 'plugins.insurance.casualty.customer-wizard',
	viewModel	: { type: 'plugins.insurance.casualty.customer-wizard' },
	items		: [{
    	xtype			: 'casualty.customeredit-acc-card1'
	},{
    	xtype			: 'casualty.customeredit-acc-card2',
        isSaveService 	: 1
	}]
	
});
