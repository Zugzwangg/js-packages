Ext.define('CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc2', {
	extend		: 'Ext.form.Panel',
	xtype		: 'casualty.customeredit-acc-card2',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.container.Container',
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.casualty.view.CustomerControllerWizard',
		'CFJS.plugins.insurance.casualty.view.CustomerModelWizard'
	],
	bodyPadding	: 10,
	defaultType	: 'container',
	defaults	: {
		anchor		: '100%',
		defaultType	: 'textfield',
		layout		: 'hbox'
	},
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			period: {
				defaultType	: 'datefield',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				layout		: { type: 'hbox', align: 'end' },
				order		: 1,
				items		: [{
					bind		: '{_parentItemName_.dateContract}',
					emptyText	: 'date of contract',
					fieldLabel	: 'Date of contract',
					format		: Ext.Date.patterns.NormalDate,
					margin		: 0,
					name		: 'dateContract'
				},{
					bind		: '{_parentItemName_.dateFrom}',
					emptyText	: 'date of departure',
					fieldLabel	: 'Date of departure',
					format		: Ext.Date.patterns.NormalDate,
					name		: 'dateFrom'
				},{
					bind		: '{_parentItemName_.dateTo}',
					emptyText	: 'date of return',
					fieldLabel	: 'Date of return',
					format		: Ext.Date.patterns.NormalDate,
					name		: 'dateTo'
				},{
					xtype			: 'numberfield',
					allowDecimals	: false,
					allowExponential: false,
					bind			: '{_parentItemName_.duration}',
					emptyText		: 'Duration of stay',
					fieldLabel		: 'Duration of stay',
					maxValue		: 365,
					minValue		: 1,
					name			: 'duration'
				}]
			},
			client	: {
				name	: 'rowConsumer',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				order		: 2,
				items	: [{
					xtype				: 'dictionarypicker',
					closeOnFocusLeave	: false,
					displayField		: 'fullName',
					emptyText			: 'select a client',
					fieldLabel			: 'Client',
					margin				: 0,
					name				: 'consumer',
					publishes			: 'value',
					rawValueData		: false,
					selectorWindow		: { xtype: 'admin-window-customer-picker' }
				},{
					xtype				: 'combobox',
					bind				: { store: '{customerIDs}', value: '{customerId}' },
					displayField		: 'id',
					emptyText			: 'select a document',
					fieldLabel			: 'Document',
					forceSelection		: true,
					listConfig			: { itemTpl: '{type.name}: {id} ' },
					minChars			: 0,
					publishes			: 'value',
					selectOnFocus		: true,
					valueField			: 'id'
				},{
					xtype			: 'dictionarycombo',
					bind			: { fieldLabel: 'Insurer', value: '{_parentItemName_.supplier}' },
					emptyText		: 'select insurer',
					minChars 		: 0,
					name			: 'insurer',
					publishes		: 'value',
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'company' } },
						sorters	: [ 'name' ]
					}
				}]
			},
			conditions: {
				xtype		: 'manyofmanyfield',
				bind		: { format: '{conditionsArr}', value: '{_itemName_.addConditions}' },
				columns		: 1,
				defaultType	: 'checkbox',
				margin		: '0 0 0 5',
				fieldLabel	: 'Additional conditions',
				order		: 3
			},
			customers: {
				xtype	: 'insurance.view.policyconsumers',
				height	: 390,
				layout	: 'card',
				margin	: '10 0',
				order	: 4
			}
		}
	},
	scrollable	: 'y',
	title		: 'Casualty',

	afterRender: function() {
		var me = this, vm = me.lookupViewModel(),
			picker = me.down('dictionarypicker[name="consumer"]');
		if (picker) {
			vm.bind('{_parentItemName_.consumer}', function(consumer) {
				picker.setValue(consumer && Ext.clone(consumer));
			});
			picker.on({
				select: function(field, value) {
					var service = vm.getService();
					if (service && service.isModel) {
						if (value) {
							if (value.isModel) value = value.getData();
							value = Ext.applyIf({
								customer: { id: value.id, name: value.fullName }
							}, CFJS.model.dictionary.Consumer.transform(value));
						}
						service.set('consumer', value);
					}
				}
			});
		}
		me.callParent(arguments);
	}
	
});

