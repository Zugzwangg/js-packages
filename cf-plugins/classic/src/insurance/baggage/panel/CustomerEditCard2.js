Ext.define('CFJS.plugins.insurance.baggage.panel.CustomerEditCard2', {
	extend		: 'Ext.form.Panel',
	xtype		: 'baggage.customeredit-card2',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.container.Container',
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.plugins.insurance.baggage.view.CustomerControllerWizard',
		'CFJS.plugins.insurance.baggage.view.CustomerModelWizard',
		'CFJS.plugins.insurance.view.PolicyConsumers'
	],
	bodyPadding	: 5,
	defaultType	: 'container',
	defaults	: {
		anchor		: '100%',
		defaultType	: 'textfield',
		layout		: 'hbox'
	},
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			period: {
				defaultType	: 'datefield',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				layout		: { type: 'hbox', align: 'end' },
				margin		: '10 5 0',
				order		: 2,
				items		: [{
					bind		: '{_parentItemName_.dateContract}',
					emptyText	: 'date of contract',
					fieldLabel	: 'Date of contract',
					format		: Ext.Date.patterns.NormalDate,
					margin		: 0,
					name		: 'dateContract'
				},{
					bind		: '{_parentItemName_.dateFrom}',
					emptyText	: 'date of departure',
					fieldLabel	: 'Date of departure',
					format		: Ext.Date.patterns.NormalDate,
					name		: 'dateFrom'
				},{
					bind		: '{_parentItemName_.dateTo}',
					emptyText	: 'date of return',
					fieldLabel	: 'Date of return',
					format		: Ext.Date.patterns.NormalDate,
					name		: 'dateTo'
				},{
					xtype			: 'numberfield',
					allowDecimals	: false,
					allowExponential: false,
					bind			: {
						value		: '{_parentItemName_.duration}',
						maxValue	: '{program.maxDuration}'
					},
					emptyText		: 'duration',
					fieldLabel		: 'Duration',
					minValue		: 1,
					name			: 'duration'
				}]
			},
			client	: {
				name		: 'rowConsumer',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				order		: 3,
				items	: [{
					xtype				: 'dictionarypicker',
					closeOnFocusLeave	: false,
					displayField		: 'fullName',
					emptyText			: 'select client',
					fieldLabel			: 'Client',
					labelStyle			: 'font-weight:bold',
					margin				: 0,
					name				: 'consumer',
					publishes			: 'value',
					rawValueData		: false,
					selectorWindow		: { xtype: 'admin-window-customer-picker' }
				},{
					xtype				: 'combobox',
					bind				: { store: '{customerIDs}', value: '{customerId}' },
					displayField		: 'id',
					emptyText			: 'select a document',
					fieldLabel			: 'Document',
					forceSelection		: true,
					listConfig			: { itemTpl: '{type.name}: {id} ' },
					minChars			: 0,
					publishes			: 'value',
					selectOnFocus		: true,
					valueField			: 'id'
				}]
			},
			insuranceAmount: {
				defaultType	: 'combobox',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				order		: 5,
				items		: [{
					bind			: { store: '{cashCover}', value: '{_parentItemName_.insuranceAmount}'},
					displayField	: 'amount',
					emptyText		: 'select amount',
					fieldLabel		: 'Sum insured',
					margin			: 0,
					name			: 'insuranceAmount',
					queryMode		: 'local',
					valueField		: 'amount'
				},{
					bind			: { store: '{franchises}', value: '{_parentItemName_.franchise}'},
					displayField	: 'amount',
					emptyText		: 'the size of the franchise',
					fieldLabel		: 'The size of the franchise',
					name			: 'franchise',
					queryMode		: 'local',
					valueField		: 'amount'
				},{
					xtype			: 'numberfield',
					bind			: '{calcFranchise}',
					fieldLabel		: 'Franchise',
					name			: 'calcFranchise',
					readOnly 		: true
				},{
					xtype			: 'dictionarycombo',
					bind			: { fieldLabel: 'Insurer', value: '{_parentItemName_.supplier}' },
					emptyText		: 'select insurer',
					minChars 		: 0,
					name			: 'insurer',
					publishes		: 'value',
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'company' } },
						sorters	: [ 'name' ]
					}
				}]
			},
			customers: {
				xtype	: 'insurance.view.policyconsumers',
				height	: 390,
				layout	: 'card',
				margin	: '10 5',
				order	: 7
			}
		}
	},
	scrollable	: 'y',
	title		: 'Baggage pay',
	
	afterRender: function() {
		var me = this, vm = me.lookupViewModel(),
			picker = me.down('dictionarypicker[name="consumer"]');
		if (picker) {
			vm.bind('{_parentItemName_.consumer}', function(consumer) {
				picker.setValue(consumer && Ext.clone(consumer));
			});
			picker.on({
				select: function(field, value) {
					var service = vm.getService();
					if (service && service.isModel) {
						if (value) {
							if (value.isModel) value = value.getData();
							value = Ext.applyIf({
								customer: { id: value.id, name: value.fullName }
							}, CFJS.model.dictionary.Consumer.transform(value));
						}
						service.set('consumer', value);
					}
				}
			});
		}
		me.callParent(arguments);
	}

});

