Ext.define('CFJS.plugins.insurance.baggage.panel.CustomerEditWizard', {
	extend : 'CFJS.plugins.insurance.wizard.BaseForm',
	xtype: 'baggage.wizard',
	requires: [
		'CFJS.plugins.insurance.baggage.panel.CustomerEditCard1',	           
		'CFJS.plugins.insurance.baggage.panel.CustomerEditCard2'	           
	],

	layout  	: 'card',
	controller	: 'plugins.insurance.baggage.customer-wizard',
	viewModel	: { type: 'plugins.insurance.baggage.customer-wizard' },
	items		: [{
    	xtype			: 'baggage.customeredit-card1'
	},{
    	xtype			: 'baggage.customeredit-card2',
       	isSaveService 	: 1
	}]

});
