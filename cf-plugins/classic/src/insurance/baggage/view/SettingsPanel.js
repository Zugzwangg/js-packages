Ext.define('CFJS.plugins.insurance.baggage.view.SettingsPanel', {
	extend		: 'CFJS.plugins.insurance.panel.SettingsPanel',
	xtype		: 'baggage.view.settings',
	requires	: [
		'CFJS.plugins.insurance.baggage.view.SettingsController',
		'CFJS.plugins.insurance.baggage.view.SettingsModel'
	],
	controller	: 'plugins.insurance.baggage.settings',
	title		: 'Settings baggage',
    viewModel	: { type: 'plugins.insurance.baggage.settings' },
    
    initEditors: function(me, vm, editors) {
		//--- Duration Trip
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{durationTrip}' },
			iconCls	: CFJS.UI.iconCls.CALENDAR,
			title	: 'Duration Trip',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min day'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max day'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridDurationTrip));
		//--- Scheme data
		editors.push(CFJS.apply({
			bind	: { store: '{schemeData}' },
			iconCls	: CFJS.UI.iconCls.DATABASE,
			title	: 'Tariffs',
			columns	: [{
				xtype: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'durationId',
				editor		: { 
					xtype			: 'combobox',
					bind			: { store: '{durationTrip}' },
					displayField	: 'title',
					queryMode		: 'local',
					forceSelection	: true,
					valueField		: 'id'
				},
				flex		: 1,
				renderer	: me.modelRenderer('durationTrip', 'id', 'title'),
    			style		: { textAlign: 'center' },
				text     	: 'Duration trip'
			},{
				align		: 'left',
				dataIndex	: 'amount',
				editor		: { 
					xtype			: 'combobox',
					bind			: { store: '{cashCover}' },
					displayField	: 'amount',
					queryMode		: 'local',
					forceSelection	: true,
					valueField		: 'amount'
				},
				flex		: 1,
				renderer	: 'sumInsRenderer',
    			style		: { textAlign: 'center' },
				text     	: 'Cash cover'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false },
				flex		: 1,
				format		: '000.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridSchemeData)),
		//--- Zones
		editors.push(CFJS.apply({
			bind	: { store: '{zones}' },
			iconCls	: CFJS.UI.iconCls.GLOBE,
			title	: 'Zones',
			columns	: [{
				xtype: 'rownumberer'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'zone',
				editor		: { allowBlank: false, allowDecimals: false, selectOnFocus: true },
				format		: '0',
    			style		: { textAlign: 'center' },
				text     	: 'ID',
				width		: 60
			},{
				align		: 'left',
				dataIndex	: 'name',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
    			style		: { textAlign: 'center' },
				text     	: 'Name'
			},{
				xtype		: 'numbercolumn',
				dataIndex	: 'minCashCover',
				editor		: {
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{cashCover}' },
					displayField	: 'amount',
					editable		: false,
					emptyText		: 'specify amount',
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'amount'
				},
				format		: '0.00',
    			style		: { textAlign: 'center' },
				text     	: 'Minimum cash cover',
				width		: 160
			},{
				xtype		: 'templatecolumn',
				align		: 'center',
				dataIndex	: 'currency',
				editor		: { 
					xtype			: 'dictionarycombo',
					allowBlank		: false,
					displayField	: 'code',
					publishes		: 'value',
					selectOnFocus	: true,
					store			: { type: 'currencies' },
					triggerAction	: 'query'
				},
				filter		: { type: 'string', emptyText: 'Currency' },
				text     	: 'Currency',
				tpl			: '<tpl if="currency">{currency.code}</tpl>',
				width		: 90
			},{
				xtype		: 'numbercolumn',
				dataIndex	: 'defFranchise',
				editor		: {
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{franchises}' },
					displayField	: 'amount',
					editable		: false,
					emptyText		: 'specify franchise',
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'amount'
				},
				format		: '0.00',
    			style		: { textAlign: 'center' },
				text     	: 'Franchise',
				width		: 160
			}]
		}, me.gridZones));
		//--- Volume discounts
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{volumeDiscounts}' },
			iconCls	: CFJS.UI.iconCls.SORT_ASC,
			title	: 'Volume discounts',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridVolumeDiscounts));
    	return editors;
	}
    
});
