Ext.define('CFJS.plugins.insurance.grid.PolicyConsumerPanel', {
	extend		: 'CFJS.grid.BasePanel',
	xtype		: 'insurance.grid.policyconsumerpanel',
	bind		: '{consumers}',
	actions		: {
		customerPicker	: {
			iconCls	: CFJS.UI.iconCls.USER_ADD,
			title	: 'Add from customer list',
			tooltip	: 'Add a record from the customer list',
			handler	: 'onCustomerPicker'
		}
	},
	actionsMap	: {
		contextMenu	: { items: [ 'addRecord', 'customerPicker', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'customerPicker', 'removeRecord', 'editRecord' ] }
	},
	contextMenu	: { items: new Array(4) },
	header		: { items: new Array(4) },
	iconCls		: CFJS.UI.iconCls.USERS,
	selModel	: { type: 'rowmodel', mode: 'MULTI' },
    title		: 'Insured persons',
	
	columns		: [{
		align		: 'left',
		dataIndex	: 'name',
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Name'
	},{
		align		: 'left',
		dataIndex	: 'middleName',
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Middle name' 
	},{
		align		: 'left',
		dataIndex	: 'lastName',
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Surname'
	},{
		xtype		: 'datecolumn',
		align		: 'center',
		dataIndex	: 'birthday',
		format		: Ext.Date.patterns.NormalDate,
		text		: 'Birthday',
		width		: 100
	},{
		align		: 'left',
		dataIndex	: 'address',
		flex		: 2,
		style		: { textAlign: 'center' },
		text		: 'Address'
	},{
		align		: 'left',
		dataIndex	: 'document',
		style		: { textAlign: 'center' },
		text		: 'Passport',
		width		: 100
	},{
		xtype		: 'numbercolumn',  
		dataIndex	: 'amount',
		format		: '00.00',
		style		: { textAlign: 'center' },
		text     	: 'Amount',
		width		: 150
	}],

	createCustomerPicker: function(listeners) {
		var me = this,
			picker = Ext.apply({
				xtype			: 'admin-window-customer-picker',
				closeAction		: 'hide',
				maximizable		: false,
				minWidth		: 540,
				minHeight		: 200,
				selectorType	: me.selectorType || 'grid'
			}, me.customerPickerConfig);
		picker.selectorConfig = Ext.apply(picker.selectorConfig || {}, me.customerSelectorConfig);
		picker = Ext.create(picker);
		picker.on({ select: function() { picker.close() } });
		if (listeners) picker.on(listeners);
		return picker;
	},
	
	showCustomerPicker: function(listeners) {
		var me = this, picker;
		if (me.rendered && !me.isExpanded && !me.destroyed) {
			picker = me.getCustomerPicker(listeners);
			picker.setMaxHeight(picker.initialConfig.maxHeight);
			picker.show();
		}
	},
	
	initComponent: function() {
		var me = this, vm, descriptor;
		me.callParent();
		if (vm = me.lookupViewModel()) {
			if (Ext.isFunction(vm.serviceName) && (descriptor = vm.serviceName())) {
//				vm.bind('{' + descriptor + '.id}', function(id) { me.setReadOnly(me.readOnly || id <= 0); });
			}
		}
	},
	
	getCustomerPicker: function(listeners) {
		var me = this;
        if (!me.customerPicker) {
        	me.creatingPicker = true;
        	me.customerPicker = me.createCustomerPicker(listeners);
        	me.customerPicker.ownerCmp = me;
        	delete me.creatingPicker;
        }
        return me.customerPicker;
	}

});