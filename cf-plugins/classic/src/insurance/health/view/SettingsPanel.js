Ext.define('CFJS.plugins.insurance.health.view.SettingsPanel', {
	extend		: 'CFJS.plugins.insurance.panel.SettingsPanel',
	xtype		: 'health.view.settings',
	requires	: [
		'CFJS.plugins.insurance.health.view.SettingsController',
		'CFJS.plugins.insurance.health.view.SettingsModel'
	],
	controller	: 'plugins.insurance.health.settings',
	title		: 'Settings health',
    viewModel	: { type: 'plugins.insurance.health.settings' },
    
    initEditors: function(me, vm, editors) {
		editors.splice(editors.length - 1, 0, 
			//--- Multivisa 
			CFJS.apply({
				bind	: { store: '{multivisa}' },
				iconCls	: CFJS.UI.iconCls.GLOBE,
				title	: 'Multivisa',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'numbercolumn', 
					dataIndex	: 'duration',
					editor		: { allowBlank: false, allowDecimals: false, selectOnFocus: true },
					flex		: 1,
					format		: '0',
	    			style		: { textAlign: 'center' },
					text		: 'Duration of stay'
				},{
					xtype		: 'numbercolumn', 
					dataIndex	: 'validity',
					editor		: { allowBlank: false, allowDecimals: false, selectOnFocus: true },
					flex		: 1,
					format		: '0',
	    			style		: { textAlign: 'center' },
					text		: 'Validity'
				}]
			}, me.gridMultivisa),
			//--- Zones
			CFJS.apply({
				bind	: { store: '{zones}' },
				iconCls	: CFJS.UI.iconCls.GLOBE,
				title	: 'Zones',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'zone',
					editor		: { allowBlank: false, allowDecimals: false, selectOnFocus: true },
					format		: '0',
	    			style		: { textAlign: 'center' },
					text     	: 'ID',
					width		: 60
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Name'
				},{
					xtype		: 'numbercolumn',
					dataIndex	: 'minCashCover',
					editor		: {
						xtype			: 'combobox',
						allowBlank		: false,
						bind			: { store: '{cashCover}' },
						displayField	: 'amount',
						editable		: false,
						emptyText		: 'specify amount',
						forceSelection	: true,
						queryMode		: 'local',
						valueField		: 'amount'
					},
					format		: '0.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Minimum cash cover',
					width		: 160
				},{
					xtype		: 'templatecolumn',
					align		: 'center',
					dataIndex	: 'currency',
					editor		: { 
						xtype			: 'dictionarycombo',
						allowBlank		: false,
						displayField	: 'code',
						publishes		: 'value',
						selectOnFocus	: true,
						store			: { type: 'currencies' },
						triggerAction	: 'query'
					},
					filter		: { type: 'string', emptyText: 'Currency' },
					text     	: 'Currency',
					tpl			: '<tpl if="currency">{currency.code}</tpl>',
					width		: 90
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'minDays',
					editor		: { allowBlank: false, selectOnFocus: true },
					format		: '0',
	    			style		: { textAlign: 'center' },
					text     	: 'Minimum day',
					width		: 160
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'addDays',
					editor		: { allowBlank: false, selectOnFocus: true },
					format		: '0',
	    			style		: { textAlign: 'center' },
					text     	: 'Additional days',
					width		: 160
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'maxAge',
					editor		: { allowBlank: false, selectOnFocus: true },
					format		: '0',
	    			style		: { textAlign: 'center' },
					text     	: 'Maximum age',
					width		: 160
				}]
			}, me.gridZones),
			//--- Countries
			CFJS.apply({
				bind	: { store: '{countryZone}' },
				iconCls	: 'x-fa fa-language',
				title	: 'Countries by zone',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'zone',
					editor		: { 
						xtype			: 'combobox',
						allowBlank		: false,
						bind			: { store: '{zones}' },
						displayField	: 'name',
						editable		: false,
						forceSelection	: true,
						queryMode		: 'local',
						valueField		: 'zone'
					},
					flex		: 1,
					renderer	: me.modelRenderer('zones', 'zone', 'name'),
	    			style		: { textAlign: 'center' },
					text     	: 'Zone'
				},{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'country',
					editor		: { 
						xtype			: 'dictionarycombo',
						allowBlank		: false,
						publishes		: 'value',
						selectOnFocus	: true,
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'country' } },
							sorters	: [ 'name' ]
						},
						triggerAction	: 'query'
					},
					filter		: { type: 'string', emptyText: 'Country' },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Country',
					tpl			: '<tpl if="country">{country.name}</tpl>'
				}]
			}, me.gridCountryZone),
			//--- Conditions
			CFJS.apply({
				bind	: { store: '{conditions}' },
				iconCls	: 'x-fa fa-shield',
				title	: 'Conditions',
				columns	: [{
					xtype: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Name'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'rate',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Rate'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'amount',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Amount'
				}]
			}, me.gridConditions)
		);
		//--- Duration Trip
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{durationTrip}' },
			iconCls	: CFJS.UI.iconCls.CALENDAR,
			title	: 'Duration Trip',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min day'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max day'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridDurationTrip));
		//--- Scheme data
		editors.push(CFJS.apply({
			bind	: { store: '{schemeData}' },
			iconCls	: CFJS.UI.iconCls.DATABASE,
			title	: 'Tariffs',
			columns	: [{
				xtype		: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'durationId',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{durationTrip}' },
					displayField	: 'title',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'id'
				},
				flex		: 1,
				renderer	: me.modelRenderer('durationTrip', 'id', 'title'),
    			style		: { textAlign: 'center' },
				text     	: 'Duration trip'
			},{
				align		: 'left',
				dataIndex	: 'amount',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{cashCover}' },
					displayField	: 'amount',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'amount'
				},
				flex		: 1,
				renderer	: 'sumInsRenderer',
    			style		: { textAlign: 'center' },
				text     	: 'Cash cover'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '000.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridSchemeData));
		//--- Age limits
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{ageLimits}' },
			iconCls	: 'x-fa fa-heartbeat',
			title	: 'Age limits',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				align		: 'left',
				dataIndex	: 'zone',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{zones}' },
					displayField	: 'name',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'zone'
				},
				flex		: 1,
				renderer	: me.modelRenderer('zones', 'zone', 'name'),
    			style		: { textAlign: 'center' },
				text     	: 'Zone'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min age'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max age'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridAgeLimits));
		//--- Scheme conditions
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{schemeConditions}' },
			iconCls	: 'x-fa fa-life-ring',
			title	: 'Scheme conditions',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'conditionId',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{conditions}' },
					displayField	: 'name',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'id'
				},
				flex		: 1,
				renderer	: 'conditionRenderer',
    			style		: { textAlign: 'center' },
				text     	: 'Condition'
			}]
		}, me.gridSchemeConditions));
		//--- Volume discounts
		editors.push(me.mergeSchemeDefaults({
			bind	: { store: '{volumeDiscounts}' },
			iconCls	: CFJS.UI.iconCls.SORT_ASC,
			title	: 'Volume discounts',
			columns	: [{},{},{
				align		: 'left',
				dataIndex	: 'title',
				flex		: 1,
    			style		: { textAlign: 'center' },
    			text     	: 'Title'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'min',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Min number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'max',
				editor		: { allowBlank: false, format: '000.', selectOnFocus: true },
				format		: '000.',
				text     	: 'Max number'
			},{
				xtype		: 'numbercolumn',  
				dataIndex	: 'rate',
				editor		: { allowBlank: false, selectOnFocus: true },
				flex		: 1,
				format		: '00.00',
    			style		: { textAlign: 'center' },
				text     	: 'Rate'
			}]
		}, me.gridVolumeDiscounts));
    	return editors;
	}
    
});
