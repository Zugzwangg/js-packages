Ext.define('CFJS.plugins.insurance.health.panel.CustomerEditWizard', {
	extend : 'CFJS.plugins.insurance.wizard.BaseForm',
	xtype: 'health.wizard',
	requires: [
		'CFJS.plugins.insurance.health.panel.CustomerEditCardMed1',	           
		'CFJS.plugins.insurance.health.panel.CustomerEditCardMed2'	           
	],

	layout  	: 'card',
	controller	: 'plugins.insurance.health.customer-wizard',
	viewModel	: { type: 'plugins.insurance.health.customer-wizard' },
	items		: [{
    	xtype			: 'health.customereditor-med-card1'
	},{
    	xtype			: 'health.customereditor-med-card2',
       	isSaveService 	: 1
	}],
	
	setMultivisaMenu: function(menu) {
		var btn = this.down('button[name="multivisa"]');
		if (btn && Ext.isFunction(btn.setMenu)) {
			btn.setMenu({ items: menu });
		}
	}

});
