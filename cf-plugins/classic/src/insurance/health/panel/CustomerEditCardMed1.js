Ext.define('CFJS.plugins.insurance.health.panel.CustomerEditCardMed1', {
	extend		: 'Ext.form.Panel',
	xtype		: 'health.customereditor-med-card1',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.form.field.CheckboxGroupField',
		'CFJS.plugins.insurance.health.view.CustomerControllerWizard',
		'CFJS.plugins.insurance.health.view.CustomerModelWizard'
	],
	defaultType		: 'fieldcontainer',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			programCountry: {
				defaults	: { editable: false, flex: 1, forceSelection: true, margin: '0 0 0 5', publishes: 'value' },
				defaultType	: 'combobox',
				margin		: '0 5',
				order		: 1,
				items		: [{
					bind		: { store: '{companyScheme}', value: '{_parentItemName_.scheme}'},
					displayField: 'scheme',
					emptyText	: 'select a scheme',
					fieldLabel	: 'Insurance scheme',
					name		: 'scheme',
					margin		: 0,
					queryMode	: 'local',
					valueField	: 'scheme',
				},{
					bind		: { store: '{zones}', value: '{_parentItemName_.zone}'},
					displayField: 'name',
					emptyText	: 'select a territory',
					fieldLabel	: 'Territory',
					name		: 'zone',
					queryMode	: 'local',
					valueField	: 'zone'
				},{
					xtype		: 'dictionarycombo',
					bind		: { value: '{_itemName_.country}' },
					editable	: true,
					emptyText	: 'select a country',
		            fieldLabel	: 'Country',
					name		: 'country',
					required	: false,
					store		: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } }
				},{
					bind		: { store: '{cashCover}', value: '{_parentItemName_.insuranceAmount}'},
					displayField: 'amount',
					emptyText	: 'select amount',
					fieldLabel	: 'Insurance cover',
					name		: 'insuranceAmount',
					queryMode	: 'local',
					valueField	: 'amount'
				},{
					xtype		: 'textfield',
					bind		: '{_parentItemName_.insuranceCurrency.code}',
					emptyText	: 'currency',
					fieldLabel	: 'Currency',
					name		: 'insuranceCurrency',
					readOnly 	: true
				}]
			},
			maindata: {
				defaultType	: 'numberfield',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				layout		: { type: 'hbox', align: 'end' },
				margin		: '10 5 0',
				order		: 2,
				items		: [{
					bind			: '{_parentItemName_.duration}',
					allowDecimals	: false,
					allowExponential: false,
					emptyText		: 'number of days',
					fieldLabel		: 'Number of days',
					margin			: 0,
					maxValue		: 365,
					minValue		: 1,
					name			: 'duration'
				},{
					bind			: '{_itemName_.validity}',
					allowDecimals	: false,
					allowExponential: false,
					emptyText		: 'validity',
					fieldLabel		: 'Validity',
					maxValue		: 365,
					minValue		: 1,
					name			: 'validity'
				},{
					bind			: '{consumersNumber}',
					emptyText		: 'number of persons',
					fieldLabel		: 'Number of persons',
					minValue		: 1,
					name			: 'countsInsurance'
				},{
					bind			: '{averageAge}',
					emptyText		: 'average age',
					fieldLabel		: 'Average age',
					minValue		: 0,
					name			: 'averageAge'
				},{
					xtype			: 'button',
					iconCls			: CFJS.UI.iconCls.CALENDAR_CHECK,
					name			: 'multivisa',
					text			: 'Multivisa',
					tooltip			: 'Multivisa',
					ui				: 'default-toolbar'
				}]
			},
			conditions: {
				xtype		: 'manyofmanyfield',
				bind		: { format: '{conditionsArr}', value: '{_itemName_.addConditions}' },
				columns		: 1,
				defaultType	: 'checkbox',
				margin		: '0 0 0 5',
				fieldLabel	: 'Additional conditions',
				order		: 4
			},
			totalAmount: {
				defaultType	: 'numberfield',
				defaults	: { columnWidth: 0.25, margin: '0 0 0 5', required: true },
				layout 		: 'column',
				margin		: '10 5 0',
				order		: 5,
				items		: [{
					bind		: '{calcQuick}',
					emptyText	: 'amount of insurance',
					fieldLabel	: 'Amount of insurance',
					margin		: 0,
					name		: 'calcQuick',
					readOnly 	: true
				},{
					xtype		: 'textfield',
					bind		: '{_parentItemName_.insuranceCurrency.code}',
					emptyText	: 'currency',
					fieldLabel	: 'Currency',
					name		: 'insuranceCurrency',
					readOnly 	: true
				},{
					xtype			: 'dictionarycombo',
					bind			: { fieldLabel: 'Insurer', value: '{_parentItemName_.supplier}' },
					emptyText		: 'select insurer',
					minChars 		: 0,
					name			: 'insurer',
					publishes		: 'value',
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'company' } },
						sorters	: [{ property: 'name'}]
					},
					required: false
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Health insurance calculator'
		
});

