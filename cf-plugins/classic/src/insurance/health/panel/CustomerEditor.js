Ext.define('CFJS.plugins.insurance.health.panel.CustomerEditor', {
	extend		: 'Ext.form.Panel',
	xtype		: 'health.customereditor',
	mixins		: [ 'CFJS.mixin.Sectionable' ],
	requires	: [
		'Ext.form.field.*',
		'Ext.form.FieldContainer',
		'CFJS.form.DateTimeField',
		'CFJS.admin.form.field.CustomerPickerWindow',
		'CFJS.form.field.CheckboxGroupField',
		'CFJS.plugins.insurance.health.view.CustomerController',
		'CFJS.plugins.insurance.health.view.CustomerModel',
		'CFJS.plugins.insurance.view.PolicyConsumers'
	],
	controller		: 'plugins.insurance.health.customer',
	viewModel		: { type: 'plugins.insurance.health.customer' },
	defaultType		: 'fieldcontainer',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: true,
		selectOnFocus	: true
	},
	layout		: 'anchor',
	config		: {
		sections: {
			conditions: {
				xtype		: 'manyofmanyfield',
				bind		: { format: '{conditionsArr}', value: '{_itemName_.addConditions}' },
				columns		: 1,
				defaultType	: 'checkbox',
				margin		: '0 0 0 5',
				fieldLabel	: 'Additional conditions',
				order		: 4
			},
			consumerDetail: {
				margin	: '0 5',
				order	: 3,
				items	: [{
					xtype			: 'combobox',
					bind			: { store: '{customerIDs}', value: '{customerId}' },
					displayField	: 'id',
					emptyText		: 'select a document',
					fieldLabel		: 'Document',
					flex			: 1,
					forceSelection	: true,
					listConfig		: { itemTpl: '{type.name}: {id} ' },
					minChars		: 0,
					publishes		: 'value',
					selectOnFocus	: true,
					valueField		: 'id'
				}]
			},
			customers: {
				xtype	: 'insurance.view.policyconsumers',
				height	: 390,
				layout	: 'card',
				margin	: '10 5',
				order	: 5
			},
			period: {
				defaultType	: 'datefield',
				defaults	: { flex: 1, margin: '0 0 0 5' },
				layout		: { type: 'hbox', align: 'end' },
				margin		: '10 5 0',
				order		: 2,
				items		: [{
					bind			: '{_parentItemName_.dateContract}',
					emptyText		: 'date of contract',
					fieldLabel		: 'Date of contract',
					format			: Ext.Date.patterns.NormalDate,
					margin			: 0,
					name			: 'dateContract'
				},{
					bind			: '{_parentItemName_.dateFrom}',
					emptyText		: 'date of departure',
					fieldLabel		: 'Date of departure',
					format			: Ext.Date.patterns.NormalDate,
					name			: 'dateFrom'
				},{
					bind			: '{_parentItemName_.dateTo}',
					emptyText		: 'date of return',
					fieldLabel		: 'Date of return',
					format			: Ext.Date.patterns.NormalDate,
					name			: 'dateTo'
				},{
					xtype			: 'numberfield',
					allowDecimals	: false,
					allowExponential: false,
					bind			: '{_parentItemName_.duration}',
					emptyText		: 'Duration of stay',
					fieldLabel		: 'Duration of stay',
					maxValue		: 365,
					minValue		: 1,
					name			: 'duration'
				},{
					xtype			: 'numberfield',
					allowDecimals	: false,
					allowExponential: false,
					bind			: '{_itemName_.validity}',
					emptyText		: 'validity',
					fieldLabel		: 'Validity',
					maxValue		: 365,
					minValue		: 1,
					name			: 'validity'
				},{
					xtype			: 'button',
					flex			: null,
					iconCls			: CFJS.UI.iconCls.CALENDAR_CHECK,
					name			: 'multivisa',
					text			: 'Multivisa',
					tooltip			: 'Multivisa',
					ui				: 'default-toolbar'
				}]
			},
			programCountry: {
				defaults	: { editable: false, flex: 1, forceSelection: true, margin: '0 0 0 5', publishes: 'value' },
				defaultType	: 'combobox',
				margin		: '0 5',
				order		: 1,
				items		: [{
					bind		: { store: '{schemes}', value: '{_parentItemName_.scheme}'},
					displayField: 'name',
					emptyText	: 'select a scheme',
					fieldLabel	: 'Insurance scheme',
					name		: 'scheme',
					margin		: 0,
					queryMode	: 'local',
					valueField	: 'code',
				},{
					bind		: { store: '{zones}', value: '{_parentItemName_.zone}'},
					displayField: 'name',
					emptyText	: 'select a territory',
					fieldLabel	: 'Territory',
					name		: 'zone',
					queryMode	: 'local',
					valueField	: 'zone'
				},{
					xtype		: 'dictionarycombo',
					bind		: { value: '{_itemName_.country}' },
					editable	: true,
					emptyText	: 'select a country',
		            fieldLabel	: 'Country',
					name		: 'country',
					required	: false,
					store		: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } }
				},{
					bind		: { store: '{cashCover}', value: '{_parentItemName_.insuranceAmount}'},
					displayField: 'amount',
					emptyText	: 'specify the amount',
					fieldLabel	: 'Insurance cover',
					name		: 'insuranceAmount',
					queryMode	: 'local',
					valueField	: 'amount'
				},{
					xtype		: 'textfield',
					bind		: '{_parentItemName_.insuranceCurrency.code}',
					emptyText	: 'currency',
					fieldLabel	: 'Currency',
					name		: 'insuranceCurrency',
					readOnly 	: true,
					width		: 85
				}]
			}
		}
	},
	scrollable	: 'y',
	title		: 'Health',
	
	setMultivisaMenu: function(menu) {
		var btn = this.down('button[name="multivisa"]');
		if (btn) btn.setMenu({ items: menu });
	},
	
	setReadOnly: function(readOnly) {
		var me = this, consumers = me.down('[name="consumergrid"]'),
			btn = me.down('button[name="multivisa"]');
		if (consumers) consumers.setReadOnly(readOnly);
		if (btn) btn.setDisabled(readOnly);
		me.callParent(arguments);
	},
	
	privates: {
		
		decodeList: function(config, list, processFn) {
			config = config || {};
			if (list) {
				list = Ext.decode(list['$']);
				if (Ext.isArray(list) && list.length > 0) {
					list.forEach(function(item) {
						processFn(config, item);
					});
				}
			}
			return config;
		},
		
		applyViewModel: function(vm) {
			if (vm && !vm.isViewModel) {
				var me = this, formulasCfg, storesCfg, properties = vm.program ? vm.program.get('properties') : null,
					listFormulas = properties ? properties.field_listFormulas : null,
					listStores = properties ? properties.field_listAddStores : null,
					cfg;
				if (properties) {
					cfg = me.decodeList(cfg, listFormulas, function(config, item) {
						(config.formulas = config.formulas || {})[item.name] = item.value;
		    		});
					cfg = me.decodeList(cfg, listStores, function(config, item) {
						if (item.name !== 'properties') (config.stores = config.stores || {})[item.name] = item;
		    		});
				}
				vm = Ext.apply(vm, cfg);
			}
			return this.callParent([vm]);
		}
	}
	
});

