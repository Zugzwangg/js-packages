Ext.define('CFJS.plugins.insurance.wizard.FinishCard', {
	extend	: 'CFJS.plugins.insurance.wizard.BaseForm',
	xtype	: 'wizard.finishcard',
	requires	: [
		'CFJS.plugins.insurance.wizard.ServiceGrid'
	],
	layout		: 'anchor',
	controller	: 'plugins.insurance.wizard.finish',
	items		: [{
		xtype	: 'servicebidlist',
		bind	: '{busket}'
	}],
	session		: { schema: 'financial' },
	viewModel	: { type: 'plugins.insurance.wizard.finish' }
});
