Ext.define('CFJS.plugins.insurance.wizard.ServiceGrid', {
    extend	: 'Ext.grid.Panel',
    xtype	: 'servicebidlist',
    requires: [	'CFJS.model.insurance.InsuranceService' ],
    title	: 'Service bid',
    plugins : 'gridfilters',
	layout	: 'anchor',
	config	: {
		actions		: {
			printRecord		: {
				iconCls	: CFJS.UI.iconCls.PRINT,
				title	: 'Print invoice',
				tooltip	: 'Print selected invoice',
				handler	: 'onPrintRecord'
			},
			removeRecord	: {
				iconCls	: CFJS.UI.iconCls.DELETE,
				title	: 'Delete invoice',
				tooltip	: 'Delete selected invoices',
				handler	: 'onRemoveRecord'
			}
		},
		actionsMap		: {
			contextMenu	: { items: [ 'removeRecord', 'printRecord' ] },
			header		: { items: [ 'removeRecord', 'printRecord' ] }
		},
		actionsDisable	: [ 'editRecord' ]
	},
	contextMenu		: { items: new Array(2) },
	header			: { items: new Array(2) },
	controller		: 'plugins.insurance.wizard.finish',
	features: [{
		ftype: 'summary'
	}],

	renderColumns: function(config) {
		var me = this,
			actions = me.getActions(),
			columns = CFJS.apply([{ 
				xtype		: 'datecolumn',
		    	text		: 'Date from',
		    	dataIndex	: 'dateFrom',
		    	flex		: 1,
		    	summaryType	: function(records){
		    		return '';
		    	}
		    },{ 
				xtype		: 'datecolumn',
		    	text		: 'Date to',
		    	dataIndex	: 'dateTo',
		    	flex		: 1,
		    	summaryType	: function(records){
		    		return '';
		    	}
		    },{ 
		    	xtype		: 'gridcolumn',
		    	text		: 'Name',
		    	flex		: 1,
		    	renderer: function(value, metaData, record) {
		    		return record.getData().program.name;
		    	},
		    	summaryType	: function(records){
		    		return '';
		    	}
		    },{ 
		    	text		: 'extNumber',
		    	dataIndex	: 'extNumber',
		    	flex		: 1,
		    	summaryType	: function(records){
		    		return '';
		    	}
		    },{
		    	text		: 'insuranceAmount',
		    	dataIndex	: 'insuranceAmount',
		    	flex		: 1,
		    	summaryType	: function(records){
		    		return '';
		    	}
		    },{
		    	text		: 'paymentAmount',
		    	dataIndex	: 'paymentAmount',
		    	flex		: 1,
		    	summaryType	: function(records){
		    		return '';
		    	}
		    },{ 
		    	xtype		: 'gridcolumn',
		    	text		: 'insuranceCurrency',
		    	flex		: 1,
		    	renderer: function(value, metaData, record) {
		    		return record.getData().insuranceCurrency.code;
		    	},
		    	summaryType	: function(records){
		    		return '<b>Total</b>: ';
		    	}
		    },{
		    	text			: 'amount',
		    	dataIndex		: 'amount',
		    	flex			: 1,
		    	summaryFormatter: 'number("0,000,000.00")',
		    	summaryType		: 'sum'
		    },{ 
		    	xtype		: 'gridcolumn',
		    	text		: 'currency',
		    	flex		: 1,
		    	renderer: function(value, metaData, record) {
		    		return record.getData().currency.code;
		    	},
		    	summaryType	: function(records){
		    		return (records && Ext.isArray(records) && records[0] && records[0].isModel) ? records[0].get('currency').code : '';
		    	}
		    }], config);
		return columns;
	},
	
	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns(me.columnsConfig);
		me.on('filterchange', me.onFilterChange, me);
		me.callParent();
	},
	
	onFilterChange: function(store, filters) {
		if (store) store.summary();
	},
    
	lookupGrid: function() {
		return this;
	},
	
	lookupGridStore: function() {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	}
	
});
