Ext.define('CFJS.plugins.insurance.wizard.BaseForm', {
	extend	: 'CFJS.wizard.BaseForm',
	xtype	: 'insurance.wizard.base',

	// returns a service save tag
	isSaveService: function() {
		return this.getLayout().getActiveItem().isSaveService;
	}

});
