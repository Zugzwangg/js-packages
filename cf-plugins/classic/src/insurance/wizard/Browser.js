Ext.define('CFJS.plugins.insurance.wizard.Browser', {
	extend				: 'CFJS.plugins.insurance.wizard.BaseForm',
	xtype				: 'insurance.wizard.browser',
	requires			: [
		'CFJS.plugins.insurance.wizard.BrowserController',
		'CFJS.plugins.insurance.wizard.BrowserModel'
	],
	bbar				: {
		margin	: 5,
		defaults: { minWidth: 100, ui: 'soft-blue' },
		items	: [{
			bind		: { hidden: '{isFirst || isLast}' },
			iconCls		: CFJS.UI.iconCls.BACK,
			text		: 'Previous',
			handler		: 'onPrevious'
		},{
			xtype		: 'progressbar',
			bind		: { value: '{progress}', hidden: '{isFirst || isLast}'},
			reference	: 'progressBar',
			flex		: 1,
			ui			: 'default'
		}, {
			bind		: { hidden: '{isFirst || isLast}'},
			iconAlign	: 'right',
			iconCls		: CFJS.UI.iconCls.NEXT,
			text		: 'Next',
			handler		: 'onNext'
		},{
			bind		: { hidden: '{!isLast}' },
			iconCls		: CFJS.UI.iconCls.ADD,
			text		: 'Once more',
			handler		: 'onContinue'
		},{
			bind		: { hidden: '{!canFinish}' },
			xtype		: 'tbfill'
		},{
			bind		: { hidden: '{!canFinish}' },
			iconCls		: CFJS.UI.iconCls.APPLY,
			text		: 'Finish',
			handler		: 'onFinish'
		},{
			bind		: { hidden: '{isFirst}' },
			iconCls		: CFJS.UI.iconCls.CANCEL,
			text		: 'Cancel',
			handler		: 'onCancel'
		}]
	},
	browserViewConfig	: {
		iconCls	: CFJS.UI.iconCls.UMBRELLA,
		title	: 'Select insurance'
	},
	controller			:'plugins.insurance.wizard.browser',
	defaults 			: { defaultFocus: 'textfield:not([value]):focusable:not([disabled])', defaultButton: 'nextbutton' },
	session				: { schema: 'financial' },
	viewModel			: { type: 'plugins.insurance.wizard.browser' },

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		vm.set('serviceType', Ext.getStore('serviceTypes').getById('INSURANCE'));
		if (!me.browserView) {
			me.browserView = Ext.create(Ext.apply({
				xtype			: 'panel-browser-view',
				bind			: '{insurancePrograms}',
				bodyPadding		: 5,
				deferEmptyText	: false,
				listeners		: { itemdblclick: 'onItemDblClick' },
				reorderable		: true,
				scrollable		: true,
				viewConfig		: { itemIconCls: '{iconCls}' }
			}, me.browserViewConfig));
		}
		if (!me.childWizard) {
			me.childWizard = Ext.create({
				xtype		: 'container',
				viewModel	: {	type: 'plugins.service.insurance', itemName: 'wizardService' },
				switchProgram: function(program) {
					return this.items.get(0);
				}
			});
		}
		if (!me.finishCard) {
			me.finishCard = Ext.create({ xtype: 'wizard.finishcard', viewModel: { parent: vm } });
		}
		me.items = [ me.browserView, me.childWizard, me.finishCard ];
		me.callParent();
	},

	createChildWizard: function(record) {
		var me = this, wizard = {}, xtypeName;
		if (record && record.isModel) {
			switch (record.get('code')) {
				case 'CPI':
					xtypeName = 'insurance.wizard.base';
					break;
				case 'ACC':
					xtypeName = 'casualty.wizard';
					break;
				case 'HI':
					xtypeName = 'health.wizard';
					break;
				case 'CL':
					xtypeName = 'civilliability.wizard';
					break;
				case 'FIN':
					xtypeName = 'insurance.wizard.base';
					break;
				case 'BAG':
					xtypeName = 'baggage.wizard';
					break;
				case 'OTHER':
					xtypeName = 'insurance.wizard.base';
					break;
				default:
					xtypeName = 'insurance.wizard.base';
					break;
			};
			wizard = xtypeName;
		}
		return wizard;
	},
	
	getItemsCount: function() {
		return this.lookupViewModel().get('steps');
	},

	last: function() {
		this.lookupViewModel().last();
		return this.getLayout().setActiveItem(this.getItemsCount() - 1);
	},

	first: function() {
		this.lookupViewModel().first();
		return this.getLayout().setActiveItem(0);
	}

});