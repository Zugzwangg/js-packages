Ext.define('CFJS.plugins.insurance.view.PolicyConsumers', {
	extend			: 'Ext.container.Container',
	xtype			: 'insurance.view.policyconsumers',
	requires		: [
		'CFJS.plugins.insurance.grid.PolicyConsumerPanel',
		'CFJS.plugins.insurance.view.PolicyConsumersController',
		'CFJS.plugins.insurance.view.PolicyConsumersModel'
	],
	controller		: 'plugins.insurance.consumers',
	layout			: { type: 'card', anchor: '100%' },
	items			: [{
    	xtype			: 'insurance.grid.policyconsumerpanel',
    	name			: 'insuranceconsumergrid',
    	listeners		: { itemdblclick: 'onGridDblClick' }
	}],
	session			: { schema: 'dictionary' },
	viewModel		: { type: 'plugins.insurance.consumers' },
	
	lookupGrid: function() {
		return this.down('grid-base-panel[name="insuranceconsumergrid"]');
	}
	
});