Ext.define('CFJS.plugins.insurance.panel.SettingsPanel', {
	extend		: 'CFJS.plugins.panel.SettingsPanel',
	xtype		: 'insurance.panel.settings',
	requires	: [ 'Ext.grid.filters.Filters', 'Ext.grid.plugin.RowEditing', 'CFJS.form.field.AceField' ],
	session		: true,

	initEditors: Ext.identityFn,
	
	initComponent: function() {
		var me = this,
			vm = this.lookupViewModel();
		me.defaults = Ext.apply({
			xtype			: 'grid',
			columnLines		: true,
			deferEmptyText	: false,
			emptyText		: 'No data to display',
			enableColumnHide: false,
			enableColumnMove: false,
			iconCls			: CFJS.UI.iconCls.COG,
			isSettingsGrid	: true,
			listeners		: { selectionchange: 'onSelectionChange' },
			loadMask		: true,
			multiColumnSort	: true,
			plugins			: [{
				ptype				: 'rowediting',
				autoUpdate			: true,
				clicksToMoveEditor	: 1,
				listeners			: { beforeedit: 'beforeDicRecordEdit', edit: 'editDicRecord'}
			},{
				ptype				: 'gridfilters'
			}],
			selModel		: { type: 'rowmodel', mode: 'MULTI' },
			stateful		: true,
			tabConfig		: { textAlign: 'left' }
		}, me.gridDefaults);
		// for grid with scheme field
    	me.schemeDefaults = CFJS.merge({
			columns		: [{
				xtype: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'scheme',
				editor		: { 
					xtype			: 'combobox',
					allowBlank		: false,
					bind			: { store: '{schemes}' },
					displayField	: 'name',
					editable		: false,
					forceSelection	: true,
					queryMode		: 'local',
					valueField		: 'code'
				},
				flex		: 1,
				renderer	: me.modelRenderer('schemes', 'code', 'name'),
    			style		: { textAlign: 'center' },
				text     	: 'Scheme'
			}]
    	}, me.schemeDefaults || {});
		me.currency = { decimals: 2 };
		if (vm) vm.bind('{program}', function(program) {
			var currency = (program || {}).insuranceCurrency || {}, end;
	   		if (Ext.isObject(currency)) {
				me.currency.currencySign = currency.name;
				if (currency.code && currency.code !== 'USD') {
					me.currency.end = true;
					me.currency.currencySpacer = ' ';
				}
				me.updateLayout();
	   		}
		});
		me.items = me.initEditors(me, vm, [
			// --- the properties editor add by default
			CFJS.apply({
				bind		: { store: '{properties}' },
				iconCls		: CFJS.UI.iconCls.WRENCH,
				isProperties: true,
				title		: 'Main property',
				columns		: [{
					xtype		: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text		: 'Property'
				},{
					align		: 'left',
					dataIndex	: 'value',
					editor		: { xtype: 'acefield', fieldLabel: 'Value', hideLabel: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text		: 'Value'
				}]
			}, me.gridProperty),
			//--- Cash cover
			CFJS.apply({
				bind	: { store: '{cashCover}' },
				iconCls	: CFJS.UI.iconCls.MONEY,
				title	: 'Cash cover',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'amount',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '000.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Amount'
				}]
			}, me.gridCashCover),
			//--- Franchises
			CFJS.apply({
				bind	: { store: '{franchises}' },
				iconCls	: CFJS.UI.iconCls.MONEY,
				title	: 'Franchises',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'numbercolumn',  
					dataIndex	: 'amount',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
					format		: '00.00',
	    			style		: { textAlign: 'center' },
					text     	: 'Franchise'
				}]
			}, me.gridFranchises),
			//--- Insurance schemes
			CFJS.apply({
				bind	: { store: '{schemes}' },
				iconCls	: 'x-fa fa-calculator',
				title	: 'Insurance schemes',
				columns	: [{
					xtype		: 'rownumberer'
				},{
					align		: 'left',
					dataIndex	: 'code',
					editor		: { allowBlank: false, maskRe: /[0-9a-z-_"]/i, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Code'
				},{
					align		: 'left',
					dataIndex	: 'name',
					editor		: { allowBlank: false, selectOnFocus: true },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Name'
				},{
					xtype		: 'datecolumn',
					align		: 'left',
					dataIndex	: 'dateBegin',
					editor		: { xtype: 'datefield', allowBlank: false, format: Ext.Date.patterns.NormalDate, selectOnFocus: true },
				    flex		: 1,
					format		: Ext.Date.patterns.NormalDate,
	    			style		: { textAlign: 'center' },
					text     	: 'Initial date'
				},{
					xtype		: 'datecolumn',
					align		: 'left',
					dataIndex	: 'dateEnd',
					editor		: { xtype: 'datefield', allowBlank: true, format: Ext.Date.patterns.NormalDate, selectOnFocus: true },
					flex		: 1,
					format		: Ext.Date.patterns.NormalDate,
	    			style		: { textAlign: 'center' },
					text     	: 'Final date'
				}]
			}, me.gridSchemes),
			//--- Company to schemes
			CFJS.apply({
				bind	: { store: '{companyScheme}' },
				iconCls	: 'x-fa fa-language',
				title	: 'Company schemes',
				columns	: [{
					xtype: 'rownumberer'
				},{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'company',
					editor		: { 
						xtype			: 'dictionarycombo',
						allowBlank		: false,
						publishes		: 'value',
						selectOnFocus	: true,
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'company' } },
							sorters	: [{ property: 'name'}]
						},
						triggerAction	: 'query'
					},
					filter		: { type: 'string', emptyText: 'Company' },
					flex		: 1,
	    			style		: { textAlign: 'center' },
					text     	: 'Company',
					tpl			: '<tpl if="company">{company.name}</tpl>'
				},{
					align		: 'left',
					dataIndex	: 'scheme',
					editor		: { 
						xtype			: 'combobox',
						allowBlank		: false,
						bind			: { store: '{schemes}' },
						displayField	: 'name',
						editable		: false,
						forceSelection	: true,
						queryMode		: 'local',
						valueField		: 'code'
					},
					flex		: 1,
					renderer	: me.modelRenderer('schemes', 'code', 'name'),
	    			style		: { textAlign: 'center' },
					text     	: 'Scheme'
				}]
			}, me.gridCompanyScheme),
		]);
		me.callParent();
	},
	
	mergeSchemeDefaults: function(config, options) {
		return CFJS.merge(CFJS.merge(config, this.schemeDefaults||{}), options||{}) ;
	},
	
	schemeRenderer: function(value) {
    	var me = this, vm = me.lookupViewModel(), 
    		store = vm ? vm.getStore('schemes') : null, 
    		record = store && store.isStore ? store.getById(value) : null;
		if (record && record.isModel) value = record.get('name');
		return value;
	},
	
	monthsRenderer: function(value) {
		if (value > 0) {
			if (value <= .5) return '15 days';
			if (value === 1) return '1 month';
			return value + ' months';
		}
	}
	
});