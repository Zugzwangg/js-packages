Ext.define('CFJS.plugins.cellstore.grid.CompanyRentGrid', {
	extend			: 'CFJS.plugins.cellstore.grid.RentGrid',
	xtype			: 'cellstore.companyrentgrid',
	iconCls			: CFJS.UI.iconCls.BUILDING,
	reference		: 'companyrentgrid',
	title			: 'Company rent',
	actions			: {
		addRecord	: {
			handler		: 'onAddRecord',
			menu		: null,
			wizardType	: 'company'
		}
	},
	actionsMap		: {
		contextMenu	: {
			items: [ 'addRecord', 'removeRecord', 'editRecord', 'copyRecord', 'printRecord' ]
		},
		header		: {
			items: [ 'periodPicker', 'addRecord', 'removeRecord', 'editRecord', 'copyRecord', 'printRecord', 'clearFilters', 'reloadStore' ]
		}
	},
	contextMenu		: { items: new Array(5) },
	header			: { items: new Array(8) },

	renderColumns: function() {
		var columns = this.callParent();
		columns.splice(1, 0, {
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'company.name',
			filter		: { type: 'string', dataIndex: 'company.name', operator: 'like' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text     	: 'Company',
			tpl			: '<tpl if="company">{company.name}</tpl>'
		});
		return columns;
	}

});