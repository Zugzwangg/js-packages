Ext.define('CFJS.plugins.cellstore.grid.PriceGrid', {
	extend	: 'CFJS.plugins.cellstore.grid.BaseSettingsGrid',
	xtype	: 'cellstore.pricegrid',
	iconCls	: CFJS.UI.iconCls.MONEY,
	title	: 'Price',
	
	initColumns: function() {
		var me = this;
		return [{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'size',
			editor		: { 
				xtype			: 'dictionarycombo',
				allowBlank		: false, 
				autoLoadOnValue	: true,
				forceSelection	: true,
				selectOnFocus	: true,
				store			: {
					type: 'cell_sizes',
					filters	: [{ id: 'endDate', property: 'endDate', type: 'date-time', operator: 'eq' }]
				}
			},
			filter		: { type: 'string', dataIndex: 'size.name', operator: 'like' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Cell size',
			tpl			: '<tpl if="size">{size.name}</tpl>'
		},{
			xtype		: 'numbercolumn',  
			dataIndex	: 'period',
			editor		: { xtype: 'numberfield', allowBlank: false, allowDecimals: false, minValue: 0, selectOnFocus: true },
			filter		: 'number',
			flex		: 1,
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'Period, days'
		},{
			dataIndex	: 'amount',
			editor		: { xtype: 'numberfield', allowBlank: false, minValue: 0, selectOnFocus: true },
			filter		: 'number',
			flex		: 1,
			renderer	: Ext.util.Format.currencyRenderer('грн.', 2, true, ' '),
			style		: { textAlign: 'center' },
			text     	: 'Amount'
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'created',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Created',
			width		: 170
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'updated',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Updated',
			width		: 170
		}];
	}

});