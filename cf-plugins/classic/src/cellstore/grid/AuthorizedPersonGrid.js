Ext.define('CFJS.plugins.cellstore.grid.AuthorizedPersonGrid', {
	extend				: 'CFJS.plugins.cellstore.grid.BaseSettingsGrid',
	xtype				: 'cellstore.authorizedpersongrid',
	mixins				: [ 'CFJS.mixin.InlineGridEditor' ],
	autoLoad			: false,
	bind				: '{persons}',
	contextMenu			: { items: new Array(4) },
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader({ items: new Array(5) }),
	iconCls				: CFJS.UI.iconCls.USERS,
	title				: 'Authorized persons',

	initColumns: function() {
		var me = this, vm = me.lookupViewModel(),
			rents = vm.get('rents');
		vm.bind('{_itemName_.id}', function(id) { me.setReadOnly(id <= 0) });
		vm.bind('{_itemName_.startDate}', function(startDate) {
			rents.addFilter({ id: 'startDate', property: 'endDate', type: 'date-time', operator: '!before', value: startDate });
		});
		vm.bind('{_itemName_.endDate}', function(endDate) {
			rents.addFilter({ id: 'endDate', property: 'startDate', type: 'date-time', operator: 'before', value: endDate });
		});
		return [{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'customer',
			editor		: {
				xtype			: 'dictionarycombo',
				allowBlank		: false, 
				autoLoadOnValue	: true,
				forceSelection	: true,
				queryParam		: 'fullName',
				selectOnFocus	: true,
				store			: {
					type	: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'customer' } },
					sorters	: [ 'name' ]
				}
			},
			filter		: { type: 'string', dataIndex: 'customer.name', operator: 'like' },
			flex		: 1,
			sorter		: { property: 'customer.name' },
			style		: { textAlign: 'center' },
			text     	: 'Customer',
			tpl			: '<tpl if="customer">{customer.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'rent',
			editor		: {
				xtype			: 'dictionarycombo',
				allowBlank		: false, 
				autoLoadOnValue	: true,
				forceSelection	: true,
				queryParam		: 'cell.number',
				selectOnFocus	: true,
				store			: rents
			},
			filter		: { type: 'string', dataIndex: 'rent.cell.number', operator: 'like' },
			flex		: 1,
			sorter		: { property: 'rent.cell.number' },
			style		: { textAlign: 'center' },
			text     	: 'Cell number',
			tpl			: '<tpl if="rent && rent.cell">{rent.cell.number}<tpl if="rent.cell.cupboard"> ({rent.cell.cupboard.name})</tpl></tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'rent.cell.size.name',
			filter		: { type: 'string', dataIndex: 'rent.cell.size.name', operator: 'like' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Cell size',
			tpl			: '<tpl if="rent && rent.cell && rent.cell.size">{rent.cell.size.name}</tpl>'
		}];
	},
	
	recordConfig: function(record) {
		var document = this.lookupViewModel().getItem() || {};
		return Ext.apply(record, { document: document.isModel ? document.getData() : document, pin: '0000' });
	},
	
	setReadOnly: function(readOnly) {
		var me = this, editor = me.findPlugin('rowediting'),
			key, action;
		if (me.readOnly === readOnly) return;
		for (key in me.actions) {
			action = me.actions[key];
			if (action.isAction) action.setDisabled(readOnly);
		}
		if (readOnly && editor && editor.editing) editor.cancelEdit();
		me.readOnly = readOnly;
	}
	
});