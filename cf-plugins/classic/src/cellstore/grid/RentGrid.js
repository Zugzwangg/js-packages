Ext.define('CFJS.plugins.cellstore.grid.RentGrid', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'cellstore.rentgrid',
	requires		: [ 'Ext.toolbar.TextItem', 'Ext.ux.statusbar.StatusBar' ],
	actions			: {
		addRecord	: {
//			title	: 'Добавить "ручную" услугу',
//			tooltip	: 'Добавить услугу в "ручном" режиме'
			handler	: null,
			menu	: {
				items: [{
					handler		: 'onAddRecord',
					iconCls		: CFJS.UI.iconCls.BUILDING,
					wizardType	: 'company',
					text		: 'Company rent'
				},{
					handler		: 'onAddRecord',
					iconCls		: CFJS.UI.iconCls.USER,
					wizardType	: 'customer',
					text		: 'Human rent'
				}]
			}
		},
		copyRecord	: {
			iconCls	: CFJS.UI.iconCls.COPY,
			title	: 'Copy record',
			tooltip	: 'Copy selected record',
			handler	: 'onCopyRecord'
		},
//		editRecord	: {
//			title	: 'Редактировать выделенную услугу',
//			tooltip	: 'Редактировать выделенную услугу'
//		},
		printRecord	: {
			iconCls		: CFJS.UI.iconCls.PRINT,
			title		: 'Print record',
			tooltip		: 'Print selected record',
			handler		: 'onPrintRecord'
//		},
//		removeRecord: {
//			title	: 'Удалить выделенные услуги',
//			tooltip	: 'Удалить выделенные услуги'
		}
	},
	actionsDisable	: [ 'copyRecord', 'editRecord', 'printRecord', 'removeRecord' ],
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'printRecord' ] },
		header		: { items: [ 'periodPicker', 'addRecord', 'printRecord', 'clearFilters', 'reloadStore' ] }
	},
	bbar			: {
		xtype: 'statusbar',
		items: [{
			xtype		: 'tbtext',
			summary		: 'count',
			text		: '<b>Count</b>: 0',
			renderValue	: function(count) {
				this.setText('<b>Count</b>: ' + Ext.util.Format.round(count || 0, 0));
			}
		},{
			xtype		: 'tbtext',
			summary		: 'amount',
			text		: '<b>Amount</b>: 0',
			renderValue	: function(amount) {
				this.setText('<b>Amount</b>: ' + Ext.util.Format.currency(amount || 0, ' грн.', 2, true));
			}
		},{
        	iconCls		: CFJS.UI.iconCls.REFRESH,
        	handler		: 'onSummaryRefresh'
		}]
	},
	bind			: '{rents}',
	contextMenu		: { items: new Array(2) },
	enableColumnHide: false,
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.SERVER,
	plugins			: 'gridfilters',
	reference		: 'rentgrid',
	selModel		: { type: 'rowmodel', mode: 'MULTI', pruneRemoved: false },
	title			: 'Rent',
	
//	getRowClass: function(record, index, rowParams, store) {
//		if (record.get('state') === 'REFUND') return 'red';
//		var hasParent = record.get('parent'), 
//			hasChilds = record.get('hasChilds');
//		if (hasParent && hasChilds) return 'olive';
//		if (hasParent) return 'green';
//		if (hasChilds) return 'blue';
//	},
	
	initComponent: function() {
		var me = this;
		me.selModel['rowNumbererHeaderWidth'] = 56;
		me.on('filterchange', me.onFilterChange, me);
		me.columns = me.renderColumns();
		me.callParent();
		var vm = me.lookupViewModel();
		if (vm) {
			vm.bind('{hidePeriodPicker}', function(hidden) {
				me.actions.periodPicker.setHidden(hidden);
			});
//			vm.bind('{hidePrintRecord}', function(hidden) {
//				actions.printRecord.setHidden(hidden);
//			});
		}
	},

	renderColumns: function() {
		return 	[{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'cell.number',
			filter		: { type: 'string', dataIndex: 'cell.number', operator: 'like' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Cell number',
			tpl			: '<tpl if="cell">{cell.number}<tpl if="cell.cupboard"> ({cell.cupboard.name})</tpl></tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'cell.size.name',
			filter		: { type: 'string', dataIndex: 'cell.size.name', operator: 'like' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Cell size',
			tpl			: '<tpl if="cell && cell.size">{cell.size.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'microCell.size.name',
			filter		: { type: 'string', operator: 'like' },
			flex		: 1,
			renderer	: function(value, meta, rec) {
				var size = (rec.get('microCell') || {}).size;
				if (size && size.id > 0) return size.name;
			},
			style		: { textAlign: 'center' },
			text     	: 'Micro cell size'
		},{
			dataIndex	: 'amount',
			filter		: 'number',
			flex		: 1,
			renderer	: Ext.util.Format.currencyRenderer('грн.', 2, true, ' '),
			style		: { textAlign: 'center' },
			text     	: 'Amount'
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'startDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Start date',
			width		: 170
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'endDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'End date',
			width		: 170
		}];
	},
	
	onFilterChange: function(store, filters) {
		if (store) store.summary();
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.actions.addVoid.setHidden(readOnly);
		me.actions.copyRecord.setHidden(readOnly);
		me.callParent(arguments);
	},
	
	updateSummary: function(key, value) {
		var tbtext = this.down('tbtext[summary=' + key + ']');
		if (tbtext) tbtext.renderValue(value);
	},
	
	updateSummaries: function(records) {
		var me = this, data = { COUNT: 'count', SUM: 'amount' },
			key, type;
		for (key in records) {
			type = records[key].get('type');
			if (data[type]) {
				me.updateSummary(data[type], records[key].get('value'));
				delete data[type];
			}
		}
		for (key in data) {
			me.updateSummary(data[key]);
		}
	}

});