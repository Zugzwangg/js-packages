Ext.define('CFJS.plugins.cellstore.grid.CupboardGrid', {
	extend	: 'CFJS.plugins.cellstore.grid.BaseSettingsGrid',
	xtype	: 'cellstore.cupboardgrid',
	iconCls	: 'x-fa fa-cube',
	title	: 'Cupboards',
	
	initColumns: function() {
		var me = this;
		return [{
			align		: 'left',
			dataIndex	: 'name',
			editor		: { allowBlank: false, selectOnFocus: true },
			filter		: 'string',
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Name'
		},{
			xtype		: 'numbercolumn',  
			dataIndex	: 'storage',
			editor		: { xtype: 'numberfield', allowBlank: false, allowDecimals: false, minValue: 0, selectOnFocus: true },
			filter		: 'number',
			flex		: 1,
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'Storage number'
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'startDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Start date',
			width		: 170
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'endDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'End date',
			width		: 170
		}];
	}

});