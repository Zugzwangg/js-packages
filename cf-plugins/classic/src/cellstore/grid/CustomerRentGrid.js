Ext.define('CFJS.plugins.cellstore.grid.CustomerRentGrid', {
	extend			: 'CFJS.plugins.cellstore.grid.RentGrid',
	xtype			: 'cellstore.customerrentgrid',
	iconCls			: CFJS.UI.iconCls.USER,
	reference		: 'customerrentgrid',
	title			: 'Human rent',
	actions			: {
		addRecord	: {
			handler		: 'onAddRecord',
			menu		: null,
			wizardType	: 'customer'
		}
	},
	actionsMap		: {
		contextMenu	: {
			items: [ 'addRecord', 'removeRecord', 'editRecord', 'copyRecord', 'printRecord' ]
		},
		header		: {
			items: [ 'periodPicker', 'addRecord', 'removeRecord', 'editRecord', 'copyRecord', 'printRecord', 'clearFilters', 'reloadStore' ]
		}
	},
	contextMenu		: { items: new Array(5) },
	header			: { items: new Array(8) },
	
	renderColumns: function() {
		var columns = this.callParent();
		columns.splice(1, 0, {
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'customer.name',
			filter		: { type: 'string', dataIndex: 'customer.name', operator: 'like' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text     	: 'Customer',
			tpl			: '<tpl if="customer">{customer.name}</tpl>'
		});
		return columns;
	}
	
});