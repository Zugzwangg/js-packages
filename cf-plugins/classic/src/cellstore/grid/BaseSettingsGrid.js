Ext.define('CFJS.plugins.cellstore.grid.BaseSettingsGrid', {
	extend			: 'Ext.grid.Panel',
	xtype			: 'cellstore.basesettings',
	requires		: [ 'Ext.grid.plugin.RowEditing', 'CFJS.util.UI' ],
	autoLoad		: true,
	bbar			: [{
		xtype		: 'pagingtoolbar',
		displayInfo	: true
	}],
	columnLines		: true,
	config			: {
		actions			: {
			addRecord	: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Add record',
				tooltip	: 'Add record',
				handler	: 'onAddRecord'
			},
			copyRecord	: {
				iconCls	: CFJS.UI.iconCls.COPY,
				title	: 'Copy record',
				tooltip	: 'Copy selected record',
				handler	: 'onCopyRecord'
			},
			removeRecord: {
				iconCls	: CFJS.UI.iconCls.DELETE,
    			title	: 'Delete records',
    			tooltip	: 'Remove selected records',
				handler	: 'onRemoveRecords'
			},
			reloadStore	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
    			title	: 'Update the data',
    			tooltip	: 'Update the data',
				handler	: 'onReloadStore'
			},
			saveRecord	: {
				formBind: true,
				iconCls	: CFJS.UI.iconCls.SAVE,
    			title	: 'Save the changes',
				tooltip	: 'Save the changes',
				handler	: 'onSaveRecords'
			}
		},
//		actionsDisable	: [ 'removeRecord', 'saveRecord' ],
		actionsMap		: {
			contextMenu	: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'saveRecord' ] },
			header		: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'saveRecord', 'reloadStore' ] }
		}
	},
	deferEmptyText	: false,
	emptyText		: 'No data to display',
	enableColumnHide: false,
	enableColumnMove: false,
	isSettingsGrid	: true,
	loadMask		: true,
	multiColumnSort	: true,
	plugins			: [{
		ptype				: 'rowediting',
		autoCancel			: true,
		clicksToMoveEditor	: 1
//		listeners			: { beforeedit: 'beforeDicRecordEdit', edit: 'editDicRecord'}
	},{
		ptype				: 'gridfilters'
	}],
	readOnly		: false,

	bindStore: function(store, initial) {
		var me = this;
		if (!initial && !me.destroyed) {
			var paging = me.down('pagingtoolbar');
			if (paging) paging.bindStore(store);
		}
		me.callParent(arguments);
	},
	
	initComponent: function() {
		var me = this;
		me.columns = [{ xtype: 'rownumberer' }].concat(me.initColumns())
		me.callParent();
	},
	
	initColumns: Ext.emptyFn
	
});