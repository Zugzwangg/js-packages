Ext.define('CFJS.plugins.cellstore.grid.CellGrid', {
	extend	: 'CFJS.plugins.cellstore.grid.BaseSettingsGrid',
	xtype	: 'cellstore.cellgrid',
	iconCls	: CFJS.UI.iconCls.BANK,
	title	: 'Cells',
	
	initColumns: function() {
		var me = this;
		return [{
			align		: 'left',
			dataIndex	: 'number',
			editor		: { allowBlank: false, selectOnFocus: true },
			filter		: 'string',
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Number'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'size',
			editor		: {
				xtype			: 'dictionarycombo',
				allowBlank		: false, 
				autoLoadOnValue	: true,
				forceSelection	: true,
				selectOnFocus	: true,
				store			: {
					type	: 'cell_sizes',
					filters	: [{
						id		: 'endDate',
						property: 'endDate',
						type	: 'date-time',
						operator: 'eq'
					}]
				}
			},
			filter		: { type: 'string', dataIndex: 'size.name', operator: 'like' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Cell size',
			tpl			: '<tpl if="size">{size.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'cupboard',
			editor		: { 
				xtype			: 'dictionarycombo',
				autoLoadOnValue	: true,
				forceSelection	: true,
				selectOnFocus	: true,
				store			: {
					type	: 'named',
					proxy	: { service: 'cell_store', loaderConfig: { dictionaryType: 'cell_cupboard' } },
					filters	: [{ id: 'endDate', property: 'endDate', type: 'date-time', operator: 'eq' }]
				}
			},
			filter		: { type: 'string', dataIndex: 'cupboard.name', operator: 'like' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text     	: 'Cupboard',
			tpl			: '<tpl if="cupboard">{cupboard.name}</tpl>'
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'startDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Start date',
			width		: 170
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'endDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'End date',
			width		: 170
		}];
	}
	
});