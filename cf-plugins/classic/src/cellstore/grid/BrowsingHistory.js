Ext.define('CFJS.plugins.cellstore.grid.BrowsingHistory', {
	extend			: 'Ext.grid.Panel',
	xtype			: 'cellstore.browsinghistorygrid',
	mixins			: [ 'CFJS.mixin.PeriodPicker' ],
	requires		: [
		'CFJS.plugins.cellstore.pages.CellSessionWizard',
		'CFJS.plugins.cellstore.view.BrowsingHistoryController',
		'CFJS.plugins.cellstore.view.BrowsingHistoryModel'
	],
	actions			: {
		createSession: {
			iconCls	: CFJS.UI.iconCls.ADD,
			title	: 'Create session',
			tooltip	: 'Create a new session',
			handler	: 'onCreateSession'
		},
		endSession	: {
			iconCls	: CFJS.UI.iconCls.CANCEL,
			title	: 'Close session',
			tooltip	: 'Close the current session',
			handler	: 'onCloseSession'
		},
		periodPicker: {
			iconCls	: CFJS.UI.iconCls.CALENDAR,
			title	: 'Working period',
			tooltip	: 'Set the working period',
			handler	: 'onPeriodPicker'
		},
		reloadStore	: {
			iconCls	: CFJS.UI.iconCls.REFRESH,
			title	: 'Update the data',
			tooltip	: 'Update the data',
			handler	: 'onReloadStore'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'createSession', 'endSession' ] },
		header		: { items: [ 'periodPicker', 'createSession', 'endSession', 'reloadStore' ] }
	},
	actionsDisable	: [ 'endSession' ],
	allowDeselect	: true,
	bind			: '{browsingHistory}',
	columnLines		: true,
	contextMenu		: { items: new Array(2) },
	controller		: 'plugins.cellstore.browsinghistory',
	deferEmptyText	: false,
	emptyText		: 'No data to display',
	enableColumnHide: false,
	enableColumnMove: false,
	header			: CFJS.UI.buildHeader({ items: new Array(4) }),
	iconCls			: CFJS.UI.iconCls.HISTORY,
	listeners		: { selectionchange: 'onSelectionChange' },
	loadMask		: true,
	multiColumnSort	: true,
	plugins			: [{ ptype: 'gridfilters' }],
	title			: 'Browsing History',
	viewModel		: { type: 'plugins.cellstore.browsinghistory' },
	columns			: [{
		xtype: 'rownumberer'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'post.name',
		flex		: 2,
		style		: { textAlign: 'center' },
		text     	: 'Manager',
		tpl			: '<tpl if="post"><tpl if="post.person">{post.person.name}<tpl else>Vakant</tpl></tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'person.customer.name',
		flex		: 2,
		style		: { textAlign: 'center' },
		text     	: 'Customer',
		tpl			: '<tpl if="person && person.customer">{person.customer.name}</tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'person.rent.cell.number',
		flex		: 1,
		filter		: 'string',
		style		: { textAlign: 'center' },
		text     	: 'Cell number',
		tpl			: '<tpl if="person && person.rent && person.rent.cell">{person.rent.cell.number}</tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'stampNumber',
		filter		: 'string',
		flex		: 1,
		style		: { textAlign: 'center' },
		text     	: 'Stamp number',
		tpl			: '<tpl if="url"><a href="{url}" target="_blank">{stampNumber}</a><tpl else>{stampNumber}</tpl>'
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'startDate',
		filter		: 'date',
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'Start date',
		width		: 170
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'endDate',
		filter		: 'date',
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'End date',
		width		: 170
	}],
	
	lookupGrid: function() {
		return this;
	}
	
});