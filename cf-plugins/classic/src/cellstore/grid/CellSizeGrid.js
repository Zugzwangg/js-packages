Ext.define('CFJS.plugins.cellstore.grid.CellSizeGrid', {
	extend	: 'CFJS.plugins.cellstore.grid.BaseSettingsGrid',
	xtype	: 'cellstore.cellsizegrid',
	iconCls	: 'x-fa fa-arrows-alt',
	title	: 'Cell sizes',
	
	initColumns: function() {
		var me = this;
		return [{
			xtype		: 'numbercolumn',  
			dataIndex	: 'height',
			editor		: { xtype: 'numberfield', allowBlank: false, allowDecimals: false, minValue: 0, selectOnFocus: true },
			filter		: 'number',
			flex		: 1,
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'Height, mm'
		},{
			xtype		: 'numbercolumn',  
			dataIndex	: 'width',
			editor		: { xtype: 'numberfield', allowBlank: false, allowDecimals: false, minValue: 0, selectOnFocus: true },
			filter		: 'number',
			flex		: 1,
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'Width, mm'
		},{
			xtype		: 'numbercolumn',  
			dataIndex	: 'depth',
			editor		: { xtype: 'numberfield', allowBlank: false, allowDecimals: false, minValue: 0, selectOnFocus: true },
			filter		: 'number',
			flex		: 1,
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'Depth, mm'
		},{
			xtype		: 'checkcolumn',
			dataIndex	: 'micro',
			filter		: 'boolean',
			flex		: 1,
			text		: 'Micro'
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'startDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Start date',
			width		: 170
		},{
			xtype		: 'datecolumn',   
			align		: 'center',
			dataIndex	: 'endDate',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'End date',
			width		: 170
		}];
	}

});