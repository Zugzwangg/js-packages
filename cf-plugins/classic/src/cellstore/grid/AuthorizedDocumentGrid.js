Ext.define('CFJS.plugins.cellstore.grid.AuthorizedDocumentGrid', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'cellstore.authorizeddocumentgrid',
	actions			: {
		addRecord	: {
			handler : null,
			menu	: [{
				handler		: 'onAddRecord',
				iconCls 	: CFJS.UI.iconCls.BOOKMARK,
				recordType	: 'DOCUMENT',
				text		: 'Internal document'
			},{
				handler		: 'onAddRecord',
				iconCls 	: CFJS.UI.iconCls.FILE_TEXT,
				recordType	: 'FILE',
				text		: 'Upload file'
			},{
				handler		: 'onAddRecord',
				iconCls 	: CFJS.UI.iconCls.LINK,
				recordType	: 'URL',
				text		: 'External link'
			}]
		}
	},
	bind			: '{documents}',
	enableColumnHide: false,
	iconCls			: CFJS.UI.iconCls.CERTIFICATE,
	title			: 'Authorized documents',
	plugins			: 'gridfilters',
	columns			: [{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'description',
		filter		: 'string',
		flex		: 1,
		style		: { textAlign: 'center' },
		text     	: 'Document',
		tpl			: '<span class="{iconCls}" style="margin-right: 5px;"></span><a href="{url}" target="_blank">{description}</a>'
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'startDate',
		filter		: 'date',
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'Start date',
		width		: 170
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'endDate',
		filter		: 'date',
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'End date',
		width		: 170
	}],
	
	initComponent: function() {
		var me = this;
		me.selModel['rowNumbererHeaderWidth'] = 56;
		me.callParent();
	}
});