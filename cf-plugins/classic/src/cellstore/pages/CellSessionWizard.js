Ext.define('CFJS.plugins.cellstore.pages.CellSessionWizard', {
	extend			: 'CFJS.window.LockingWindow',
	xtype			: 'cellstore.sessionwizard',
	requires		: [
		'CFJS.form.field.PinField',
		'CFJS.plugins.cellstore.view.CellSessionsController',
		'CFJS.plugins.cellstore.view.CellSessionsModel',
		'CFJS.ux.Timer'
	],
	buttonConfig: {
		iconAlign	: 'right',
		iconCls		: CFJS.UI.iconCls.ANGLE_RIGHT,
		margin		: '10 0 20 0',
		scale		: 'large',
		ui			: 'soft-green'
	},
	cellNumberText	: 'Cell number',
	closeAction		: 'destroy',
	confirmPinText	: 'PIN confirmation',
	controller		: 'plugins.cellstore.sessions',
	customerText	: 'Customer',
	defaultFocus 	: 'form',  // Focus the Auth Form to force field focus as well
	emptyPinText	: 'PIN can\'t be an empty',
	finishText		: 'Finish session',
	header			: false,
	newPinText		: 'Input new PIN',
	passwordLabel	: 'Please, enter your password to resume',
	passwordText	: 'password',
	pinChangeText	: 'Change PIN',
	pinNotEqualsText: 'PIN not equals',
	pinText			: 'Input a PIN',
	selectCellText	: 'Select a cell',
	selectFileText	: 'Select a file',
	stampNumberText	: 'Stamp number',
	stampPhotoText	: 'Stamp photo',
	startText		: 'Start session',
	stopText		: 'Stop session',
	usernameText	: 'Your phone or email',
	viewModel		: { type: 'plugins.cellstore.sessions' },
	
	initComponent: function() {
		var me = this,
			buttonConfig = Ext.apply({ xtype: 'button', clickOnEnter: true, formBind: true }, me.buttonConfig),
			pinField = {
				xtype		: 'pinfield',
				allowBlank	: false,
				itemConfig	: { cls: 'auth-pin-textbox' },
				itemsOffset	: 35,
				msgTarget	: 'side',
				pinLength	: 4
			},
			authenticate = {
				items: [{
					bind		: '{password}',
					cls			: 'auth-textbox',
					emptyText	: me.passwordText,
					fieldLabel	: me.passwordLabel,
					inputType	: 'password',
					minLenght	: 6,
					name		: 'password',
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-password-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } }
				}, 
					Ext.apply({ handler: 'onLoginClick', text: me.selectCellText }, buttonConfig)
				]
			},
			cellSelector = {
				items: [{
					bind		: '{phoneOrEmail}',
					cls			: 'auth-textbox',
					emptyText	: me.usernameText,
					fieldLabel	: me.usernameText,
					minLenght	: 4,
					name		: 'username',
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-user-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } }
				},{
					bind		: '{cellNumber}',
					cls			: 'auth-textbox cell-number',
					emptyText	: me.cellNumberText,
					fieldLabel	: me.selectCellText,
					minLenght	: 4,
					name		: 'cellNumber',
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-cell-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } }
				}, 
					Ext.apply({ bind: '{pin}', fieldLabel: me.pinText, name: 'pin'}, pinField),
					Ext.apply({ handler: 'onStartClick', text: me.startText }, buttonConfig)
				]
			},
			finishSession = {
				items	: [{
					bind		: '{cell_session.stampNumber}',
					cls			: 'auth-textbox',
					emptyText	: me.stampNumberText.toLowerCase(),
		            fieldLabel	: me.stampNumberText,
		            name		: 'stampNumber'
		        },{
		            xtype		: 'filefield',
					cls			: 'auth-textbox',
		            emptyText	: me.selectFileText.toLowerCase(),
		            fieldLabel	: me.stampPhotoText,
		            name		: 'stampPhoto',
		            buttonMargin: 5,
		            buttonConfig: {
		            	clickOnEnter: false,
						iconCls		: CFJS.UI.iconCls.IMAGE,
						text		: null,
						tooltip		: me.selectFileText
		            }
				},
					Ext.apply({ handler: 'onFinishClick', text: me.finishText }, buttonConfig)
				]
			},
			pinChange = {
				items	: [
					Ext.apply({ bind: '{newPin}', fieldLabel: me.newPinText, name: 'newPin',
						validator: function(value) {
							return me.getViewModel().isEmptyPin(value) ? me.emptyPinText : true;
						}
					}, pinField),
					Ext.apply({ fieldLabel: me.confirmPinText, 
						validator: function(value) {
							return value === me.getViewModel().get('newPin') ? true : me.pinNotEqualsText; 
						}
					}, pinField),
					Ext.apply({ handler: 'onPinChangeClick', text: me.pinChangeText}, buttonConfig)
				]
			},
			profile = {
				xtype	: 'container',
				cls		: 'auth-profile-wrap',
				height	: 120,
				layout	: { type: 'hbox', align: 'center' },
				items: [{
					xtype	: 'image',
					alt		: 'lockscreen-image',
					bind	: { src: '{user.photo}' },
					cls		: 'lockscreen-profile-img auth-profile-img',
					height	: 80,
					margin	: 20,
					width	: 80
				},{
					xtype	: 'box',
					bind	: { html: '<div class=\'user-name-text\'> {user.name} </div><div class=\'user-post-text\'> {user.post.name} </div>' }
				}]
			},
			timer = {
				defaultFocus: 'button',
				items		: [{
					xtype			: 'displayfield',
					bind			: '{cell_session.person.customer.name}',
					cls				: 'display-field',
					fieldLabel		: me.customerText,
					labelAlign		: 'left',
					labelSeparator	: ':',
					labelWidth		: 140,
					margin			: '10 0 0'
				},{
					xtype			: 'displayfield',
					bind			: '{cell_session.person.rent.cell.number}',
					cls				: 'display-field',
					fieldLabel		: me.cellNumberText,
					labelAlign		: 'left',
					labelSeparator	: ':',
					labelWidth		: 140,
					margin			: 0
				},{
					xtype			: 'timer',
					style			: 'background:transparent;',
					interval		: 1,
					margin			: 0,
					sectionsConfig	: { days: false }
				},
					Ext.apply({ handler: 'onStopTimerClick', text: me.stopText }, buttonConfig)
				]
			},
			body = {
				xtype		: 'container',
				defaultType	: 'form',
				defaults	: {
					autoComplete: false,
					defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([value])',
					defaultType	: 'textfield',
					defaults	: {
						allowBlank	: false,
						margin		: '10 0 0'
					},
					fieldDefaults: {
						labelAlign		: 'top',
						labelPad		: 10,
						labelSeparator	: '',
						labelStyle		: 'font-weight:bold;font-size:16px;',
						msgTarget		: Ext.supports.Touch ? 'side' : 'qtip'
					},
					layout	: { type: 'vbox', align: 'stretch' }
				},
				layout	: 'card',
				name	: 'body',
				padding	: '0 20',
				items	: [ authenticate, cellSelector, pinChange, timer, finishSession ]
			};
		me.items = {
			xtype	: 'panel',
			cls		: 'auth-panel',
			layout	: { type: 'vbox', align: 'stretch' },
			width	: 455,
			items	: [ profile, body ]
		};
		me.addCls('cellstore-sessionwizard');
		me.callParent();
	}

});
