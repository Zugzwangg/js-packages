Ext.define('CFJS.plugins.cellstore.pages.CompanyRents', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.cellstore.pages.CompanyRents',
	xtype				: 'cellstore.page.companyrents',
	requires			: [
		'CFJS.plugins.cellstore.grid.CompanyRentGrid',
//		'CFJS.orders.panel.OrderEditor',
		'CFJS.plugins.cellstore.view.CompanyRentsController',
		'CFJS.plugins.cellstore.view.CompanyRentsModel'
//		'CFJS.orders.window.CustomerSelector',
//		'CFJS.orders.window.invoice.InvoicesEditor',
//		'CFJS.plugins.service.window.ServiceSelector'
	],
	controller			: 'plugins.cellstore.companyrents',
	viewModel			: { type: 'plugins.cellstore.companyrents' },
	layout				: { type: 'card', anchor: '100%' },
	items				: [{
		xtype		: 'cellstore.companyrentgrid',
		reference	: 'companyrentgrid',
		listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	
	lookupGrid: function() {
		return this.lookup('companyrentgrid');
	}
	
});