Ext.define('CFJS.plugins.cellstore.pages.CustomerRents', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.cellstore.pages.CustomerRents',
	xtype				: 'cellstore.page.customerrents',
	requires			: [
		'CFJS.plugins.cellstore.grid.CustomerRentGrid',
//		'CFJS.orders.panel.OrderEditor',
		'CFJS.plugins.cellstore.view.CustomerRentsController',
		'CFJS.plugins.cellstore.view.CustomerRentsModel'
//		'CFJS.orders.window.CustomerSelector',
//		'CFJS.orders.window.invoice.InvoicesEditor',
//		'CFJS.plugins.service.window.ServiceSelector'
	],
	controller			: 'plugins.cellstore.customerrents',
	viewModel			: { type: 'plugins.cellstore.customerrents' },
	layout				: { type: 'card', anchor: '100%' },
	items				: [{
		xtype		: 'cellstore.customerrentgrid',
		reference	: 'customerrentgrid',
		listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	
	lookupGrid: function() {
		return this.lookup('customerrentgrid');
	}
	
});