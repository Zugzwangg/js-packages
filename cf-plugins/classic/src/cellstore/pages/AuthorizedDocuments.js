Ext.define('CFJS.plugins.cellstore.pages.AuthorizedDocuments', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.cellstore.pages.AuthorizedDocuments',
	xtype				: 'cellstore.page.authorizeddocuments',
	requires			: [
		'CFJS.plugins.cellstore.grid.AuthorizedDocumentGrid',
		'CFJS.plugins.cellstore.view.AuthorizedDocumentsController',
		'CFJS.plugins.cellstore.view.AuthorizedDocumentsModel'
	],
	controller			: 'plugins.cellstore.authorizeddocuments',
	viewModel			: { type: 'plugins.cellstore.authorizeddocuments' },
	layout				: { type: 'card', anchor: '100%' },
	items				: [{
		xtype		: 'cellstore.authorizeddocumentgrid',
		reference	: 'authorizeddocumentgrid',
		listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	
	lookupGrid: function() {
		return this.lookup('authorizeddocumentgrid');
	}
	
});