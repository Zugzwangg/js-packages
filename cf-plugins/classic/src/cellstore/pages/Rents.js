Ext.define('CFJS.plugins.cellstore.pages.Rents', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.cellstore.pages.Rents',
	xtype				: 'cellstore.page.rents',
	requires			: [
		'CFJS.plugins.cellstore.grid.RentGrid',
		'CFJS.plugins.cellstore.panel.RentWizard',
		'CFJS.plugins.cellstore.view.RentsController',
		'CFJS.plugins.cellstore.view.RentsModel'
//		'CFJS.orders.window.CustomerSelector',
//		'CFJS.orders.window.invoice.InvoicesEditor',
//		'CFJS.plugins.service.window.ServiceSelector'
	],
	controller			: 'plugins.cellstore.rents',
	viewModel			: { type: 'plugins.cellstore.rents' },
	layout				: { type: 'card', anchor: '100%' },
	items				: [{
		xtype		: 'cellstore.rentgrid',
		reference	: 'rentgrid',
		listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	
	lookupGrid: function() {
		return this.lookup('rentgrid');
	}
	
});