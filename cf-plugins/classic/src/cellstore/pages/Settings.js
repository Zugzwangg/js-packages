Ext.define('CFJS.plugins.cellstore.pages.Settings', {
	extend			: 'CFJS.plugins.panel.SettingsPanel',
	xtype			: 'cellstore.page.settings',
	requires		: [ 
		'CFJS.plugins.cellstore.grid.*',
		'CFJS.plugins.cellstore.view.SettingsController',
		'CFJS.plugins.cellstore.view.SettingsModel'
	],
	controller		: 'plugins.cellstore.settings',
	session			: true,
	tabPosition		: 'top',
	viewModel		: { type: 'plugins.cellstore.settings' },

	initComponent: function() {
		var me = this, items = [];
		if (me.isFirstInstance) {
			items.push({ 
				xtype: 'cellstore.cellsizegrid',
				bind: '{cellSizes}'
			},{
				xtype: 'cellstore.pricegrid',
				bind: '{prices}'
			},{
				xtype: 'cellstore.cupboardgrid',
				bind: '{cupboards}'
			},{
				xtype: 'cellstore.cellgrid',
				bind: '{cells}'
			});
			me.items = items;
		}
		me.callParent();
	}

});