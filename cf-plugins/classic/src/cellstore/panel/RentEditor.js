Ext.define('CFJS.plugins.cellstore.panel.RentEditor', {
	extend			: 'Ext.form.Panel',
	xtype			: 'cellstore.renteditor',
	requires		: [
		'Ext.form.FieldContainer',
		'Ext.form.field.Date',
		'Ext.form.field.Display',
		'Ext.form.field.Number',
		'Ext.grid.Panel',
		'Ext.grid.filters.Filters',
		'Ext.layout.container.HBox',
		'Ext.layout.container.VBox',
		'Ext.selection.CheckboxModel',
		'CFJS.plugins.cellstore.view.RentEditorModel'
	],
	bodyPadding		: '10 10 0 10',
	defaultFocus	: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaults		: { anchor: '100%' },
	fieldDefaults	: {
		labelAlign	: 'top',
		labelWidth	: 100,
		msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
	},
	iconCls			: CFJS.UI.iconCls.SERVER,
	layout			: { type: 'anchor', reserveScrollbar: true },
    scrollable		: 'y',
	title			: 'Rent editor',
	viewModel		: { type: 'plugins.cellstore.renteditor' },
	
	initComponent: function() {
		var me = this,
			gridCfg = {
				xtype			: 'grid',
				allowDeselect	: true,
				columnLines		: true,
				deferEmptyText	: false,
				emptyText		: 'No data to display',
				height			: 300,
				loadMask		: true,
				multiColumnSort	: true,
				plugins			: [{ ptype: 'gridfilters' }],
				selModel		: { type: 'rowmodel', mode: 'SINGLE' },
				listeners		: { selectionchange: 'onCellsSelectionChange' }
			},
			columns	= [{
				xtype: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'number',
				filter		: 'string',
				style		: { textAlign: 'center' },
				text     	: 'Number',
				width		: 250
			},{
				xtype		: 'templatecolumn',
				align		: 'left',
				dataIndex	: 'size',
				filter		: { type: 'string', dataIndex: 'size.name', operator: 'like' },
				flex		: 1,
				style		: { textAlign: 'center' },
				text     	: 'Cell size',
				tpl			: '<tpl if="size">{size.name}</tpl>'
			},{
				xtype		: 'templatecolumn',
				align		: 'left',
				dataIndex	: 'cupboard',
				filter		: { type: 'string', dataIndex: 'cupboard.name', operator: 'like' },
				style		: { textAlign: 'center' },
				text     	: 'Cupboard',
				tpl			: '<tpl if="cupboard">{cupboard.name}</tpl>',
				width		: 250
			},{
				xtype		: 'datecolumn',
				align		: 'center',
				dataIndex	: 'freeFrom',
				filter		: 'date',
				renderer	: function(value, meta, record) {
					if (Ext.Date.before(new Date(), value))	{
						meta.tdCls = 'soft-red';
						return Ext.util.Format.date(value, Ext.Date.patterns.LongDateTime);
					}
				},
				text		: 'Free from',
				width		: 170
			}];
		me.items = [{
			xtype			: 'fieldcontainer',
			combineErrors	: true,
			defaults		: { flex: 1, margin: '0 0 0 5', required: true, selectOnFocus: true },
			layout			: 'hbox',
			items			: [{
				xtype		: 'datefield',
				bind		: { value: '{_itemName_.startDate}' },
				fieldLabel	: 'Rent from',
				format		: Ext.Date.patterns.NormalDate,
				margin		: 0,
				minValue	: Ext.Date.today(),
				name		: 'startDate'
			},{
				xtype		: 'numberfield',
				bind		: { value: '{period}' },
				fieldLabel	: 'Period (days)',
				minValue	: 90,
				name		: 'period'
			},{
				xtype			: 'numberfield',
				allowExponential: false,
				bind			: { value: '{_itemName_.amount}' },
				fieldLabel		: 'To pay',
				hideTrigger		: true,
				minValue		: 0,
				name			: 'amount'
			}]
		}, Ext.apply({
			bbar	: [{ xtype: 'pagingtoolbar', bind: { store: '{cells}' }, displayInfo: true }],
			bind	: { store: '{cells}' },
			columns	: columns,
			name	: 'cell',
			title	: 'Select a cell'
		}, gridCfg)];
		me.items.push(Ext.apply({
			bbar	: [{ xtype: 'pagingtoolbar', bind: { store: '{microCells}' }, displayInfo: true }],
			bind	: { store: '{microCells}' },
			columns	: [ columns[0], columns[1], columns[2], columns[4] ],
			name	: 'microCell',
			title	: 'Select a micro cell'
		}, gridCfg));
		me.callParent();
	}
	
});