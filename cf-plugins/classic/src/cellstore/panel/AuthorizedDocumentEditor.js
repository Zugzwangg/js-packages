Ext.define('CFJS.plugins.cellstore.panel.AuthorizedDocumentEditor', {
	extend		: 'CFJS.panel.BaseEditor',
	xtype		: 'plugins.cellstore.authorized-document-editor',
	requires		: [
		'Ext.form.FieldContainer',
		'Ext.form.field.Date',
		'Ext.form.field.TextArea',
		'Ext.layout.container.HBox',
		'Ext.layout.container.VBox',
		'CFJS.plugins.cellstore.grid.AuthorizedPersonGrid',
		'CFJS.plugins.cellstore.view.AuthorizedDocumentModel'
	],
	actions		: { back: { tooltip: 'Back to documents list' } },
	actionsMap	: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bind			: { iconCls: '{_itemName_.iconCls}', title: '<a href="{_itemName_.url}" target="_blank">{_itemName_.description}</a>' },
	bodyPadding		: '10 10 0 10',
	fieldDefaults	: {
		labelAlign	: 'top',
		labelWidth	: 100,
		msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
	},
	layout		: { type: 'vbox', align: 'stretch' },
	name			: 'authorizedDocumentEditor',
	viewModel	: {	type: 'plugins.cellstore.authorizeddocument' },

	initComponent: function() {
		var me = this;
		me.items = [{
			xtype		: 'fieldcontainer',
			defaultType	: 'datefield',
			defaults	: { margin: '0 0 0 5',required: true, selectOnFocus: true },
			layout		: 'hbox',
			items		: [{
				xtype		: 'textfield',
				bind		: '{_itemName_.description}',
				fieldLabel	: 'Description',
				flex		: 1,
				margin		: 0,
				name		: 'description'
			},{
				bind		: '{_itemName_.startDate}',
				fieldLabel	: 'Start date',
				format		: Ext.Date.patterns.NormalDate,
				name		: 'startDate'
			},{
				bind		: '{_itemName_.endDate}',
				fieldLabel	: 'End date',
				format		: Ext.Date.patterns.NormalDate,
				name		: 'endDate'
			}]
		},{
			xtype	: 'cellstore.authorizedpersongrid',
			flex	: 1
		}];
		me.callParent();
	}
	
});