Ext.define('CFJS.plugins.cellstore.panel.RentWizard', {
	extend			: 'Ext.form.Panel',
	xtype			: 'cellstore.rentwizard',
	requires		: [
		'CFJS.plugins.cellstore.panel.RentEditor',
		'CFJS.plugins.cellstore.view.RentWizardController',
		'CFJS.plugins.cellstore.view.RentWizardModel',
		'CFJS.document.view.DocumentView'
	],
	bodyPadding		: 10,
	bbar			: {
		margin	: 5,
		items	: [{
			bind		: { hidden: '{atStart}' },
			direction	: 'prev',
			iconCls		: CFJS.UI.iconCls.BACK,
			minWidth	: 90,
			text		: 'Previous',
			ui			: 'soft-blue',
			handler		: 'onNavigate'
		}, {
			xtype	: 'progressbar',
			bind	: { value: '{progress}' },
			flex	: 1
		}, {
			bind		: { hidden: '{atEnd}' },
			direction	: 'next',
			iconAlign	: 'right',
			iconCls		: CFJS.UI.iconCls.NEXT,
			minWidth	: 90,
			text		: 'Next',
			ui			: 'soft-blue',
			handler		: 'onNavigate'
		}, {
			bind		: { hidden: '{!atEnd}' },
			formBind	: true,
			minWidth	: 90,
			text		: 'Finish',
			ui			: 'soft-blue',
			handler		: 'onFinish'
		}]
	},
	controller		: 'plugins.cellstore.rentwizard',
	defaultType		: 'form',
	defaults 		: { defaultFocus: 'textfield:not([value]):focusable:not([disabled])', defaultButton: 'nextbutton' },
	layout			: 'card',
	viewModel		: { type: 'plugins.cellstore.rentwizard' },
	
	initComponent: function() {
		var me = this, wizardType = me.wizardType || '',
			config = me.lookupViewModel().get('documentConfig'),
			documentSelector = {
				xtype		: 'document-view-document-view',
				controller	: { type: 'document.edit', editor: 'document-panel-editor' },
				gridConfig	: null,
				name		: 'documentSelector',
				viewModel	: { form: { type: 'CFJS.model.document.Form' } }
			},
			rentEditor = { xtype: 'cellstore.renteditor', name: 'rentEditor' },
			rentOwner = { name: 'rentOwner'}, items = [];
		if (config && config.isStore) config = config.getById(wizardType);
		if (config && config.isModel) {
			me.wizardConfig = config = config.getData();
			documentSelector.viewModel.form.id = config.contract.form;
			switch (config.id) {
				case 'company':
					rentOwner.xtype = 'companies-view';
					rentEditor.viewModel = {
						itemModel: 'CFJS.model.cellstore.CompanyRent',
						itemName: 'rent-company-' + (++Ext.idSeed)
					};
					me.items = [rentOwner, documentSelector, rentEditor];
					break;
				case 'customer':
					rentOwner.xtype = 'customers-view';
					rentEditor.viewModel = {
						itemModel: 'CFJS.model.cellstore.CustomerRent',	
						itemName: 'rent-customer-' + (++Ext.idSeed)
					};
					me.items = [rentOwner, documentSelector, rentEditor];
					break;
				default: break;
			}
			
		}
		me.callParent();
	}
	
});