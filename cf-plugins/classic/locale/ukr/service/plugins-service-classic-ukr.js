Ext.define('CFJS.plugins.locale.ukr.service.grid.AirlineSegmentsPanel', {
	override: 'CFJS.plugins.service.grid.AirlineSegmentsPanel',
	config	: {
		bind: { title: 'Маршрут: {routeBySegments}', store: '{segments}' }
	}
});
Ext.define('CFJS.plugins.locale.ukr.service.grid.ServicePanel', {
	override: 'CFJS.plugins.service.grid.ServicePanel',
	config	: {
		actions	: {
			addRecord: {
				title	: 'Додати "ручну" послугу',
				tooltip	: 'Додати послугу в "ручному" режимі'
			},
			addVoid		: {
				title	: 'Додати повернення',
				tooltip	: 'Додати повернення обраної послуги',
			},
			copyRecord	: {
				title	: 'Скопіювати послугу',
				tooltip	: 'Скопіювати обрану послугу'
			},
			editRecord	: {
				title	: 'Редагувати послугу',
				tooltip	: 'Редагувати поточну послугу'
			},
			printRecord	: {
				title	: 'Друкувати послугу',
				tooltip	: 'Друкувати обрану послугу'
			},
			removeRecord: {
				title	: 'Видалити послуги',
				tooltip	: 'Видалити виділені послуги'
			}
		},
		title	: 'Послуги'
	}
});
Ext.define('CFJS.plugins.locale.ukr.service.grid.TaxAmountPanel', {
	override: 'CFJS.plugins.service.grid.TaxAmountPanel',
	config	: {
		actions	: {
			editServices: {
				title	: 'Сервисный збір та знижки',
				tooltip	: 'Редагувати сервісний збір та знижки'
			}
		},
		title	: 'Тарифний блок'
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, { columns:[{},{ text: "Код" },{ text: "Сума" },{ text: "Створено" }] });
		me.callParent();
	}
});
Ext.define('CFJS.plugins.locale.ru.service.panel.Editor', {
	override: 'CFJS.plugins.service.panel.Editor',
	config: {
		actions	: { back: { tooltip: 'Назад до списку послуг' } },
		bind	: { readOnly: '{!_itemName_.updatable}', title: 'Услуга "{_itemName_.sector.name} № {_itemName_.number}". {_itemName_.entityName}' }
	}
});
Ext.define('CFJS.plugins.locale.ukr.service.panel.InsuranceEditor', {
	override: 'CFJS.plugins.service.panel.InsuranceEditor',
	config: {
		actions: {
			writeOut: {
				title	: 'Виписати поліс',
				tooltip	: 'Виписати страховий поліс'
			}
		}
	}
});
Ext.define('CFJS.plugins.locale.ukr.service.view.EditController', {
	override: 'CFJS.plugins.service.view.EditController',
	messages: {
		confirmAction		: 'Підтвердіть дію',
		documentsRedirect	: 'Перейти до документів?',
		documentSign		: 'Накласти підпис на документ?'
	}
});
