Ext.define('CFJS.locale.ukr.plugins.panel.SettingsPanel', {
    override: 'CFJS.plugins.panel.SettingsPanel',
	config	: {
		actions	: {
			addRecord	: { title: 'Додати запис', tooltip: 'Додати запис' },
    		copyRecord	: { title: 'Скопіювати запис', tooltip: 'Скопіювати поточний запис' },
			reloadStore	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeRecord: { title: 'Видалити записи', tooltip: 'Видалити обрані записи' },
			saveRecord	: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' }
		},
		title	: 'Налаштування'
	},
    monthsRenderer: function(value) {
    	if (value > 0) {
    		if (value === 1) return '1 місяць';
    		if (value < 5) return value + ' місяці';
    		return value + ' місяців';
    	}
    }
});
