Ext.define('CFJS.locale.ukr.plugins.insurance.wizard.Browser', {
	override: 'CFJS.plugins.insurance.wizard.Browser',
	initComponent: function() {
		var me = CFJS.merge(this, {
			bbar: {
				items: [{ text: 'Попередня' },{},{ text: 'Далі' },{ text: 'Продовжити' },{},{ text: 'Завершити' },{ text: 'Перервати' }]
			},
			browserViewConfig: { title: 'Виберіть вид страхування' }
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.wizard.ServiceGrid', {
	override: 'CFJS.plugins.insurance.wizard.ServiceGrid',
    config	: {
    	title	: 'Сервісна пропозиція',
    	actions: {
    		printRecord	: {
    			title	: 'Надрукувати вибраний рахунок-фактуру',
    			tooltip	: 'Надрукувати вибраний рахунок-фактуру'
    		},
    		removeRecord: {
    			title	: 'Видалити вибрані рахунки-фактури',
    			tooltip	: 'Видалити вибрані рахунки-фактури'
    		}
    	}
    },
    renderColumns: function() {
    	var columns = this.callParent(arguments);
    	columns[0].text = 'Початкова дата';
    	columns[1].text = 'Кінцева дата';
    	columns[2].text = 'Найменування';
    	columns[3].text = 'Системний номер';
    	columns[4].text = 'Страхова сума';
    	columns[5].text = 'Сума платежу';
    	columns[6].text = 'Валюта платежу';
    	columns[7].text = 'Сума до оплати';
    	columns[8].text = 'Валюта оплати';
    	return columns;
    }
});
Ext.define('CFJS.locale.ukr.plugins.insurance.baggage.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.baggage.view.SettingsPanel',
	gridDurationTrip: {
		title: 'Тривалість поїздки',
		columns: [{},{ text: 'Схема' },{ text: 'Назва' },{ text: 'Мінімум днів' },{ text: 'Максимум днів' },{ text: 'Ставка' }]
	},
	gridSchemeData: {
		title: 'Тарифи',
		columns: [{},{ text: 'Тривалість' },{ text: 'Грошове покриття' },{ text: 'Ставка' }]
	},
	gridZones: {
		title: 'Зони',
		columns: [{},{ text: 'Код' },{ text: 'Назва' },{ editor: { emptyText: 'вкажіть суму' },  text: 'Мінімальне покриття' },{ text: 'Валюта' },{ editor: { emptyText: 'вкажіть франшизу' }, text: 'Франшиза' }]
	},
	gridVolumeDiscounts: {
		title: 'Знижки за об\'єм',
		columns: [{},{},{ text: 'Назва' },{ text: 'Від' },{ text: 'До' },{ text: 'Ставка' }]
	},
	title: 'Багаж'
});
Ext.define('CFJS.locale.ukr.plugins.insurance.casualty.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.casualty.view.SettingsPanel',
	gridAgeLimits: {
		title: 'Вікові обмеження',
		columns: [{},{},{ text: 'Назва' },{ text: 'Мінімальний вік' },{ text: 'Максимальний вік' },{ text: 'Ставка' }]
	},
	gridConditions: {
		title: 'Додаткові умови',
		columns: [{},{ text: 'Назва' },{ text: 'Ставка' },{ text: 'Сума' }]
	},
	gridDurationTrip: {
		title: 'Тривалість поїздки',
		columns: [{},{ text: 'Схема' },{ text: 'Назва' },{ text: 'Мінімум днів' },{ text: 'Максимум днів' },{ text: 'Ставка' }]
	},
	gridSchemeData: {
		title: 'Тарифи',
		columns: [{},{ text: 'Тривалість' },{ text: 'Грошове покриття' },{ text: 'Ставка' }]
	},
	gridSchemeConditions: {
		title: 'Умови схеми',
		columns: [{},{},{ text: 'Умова' }]
	},
	gridVolumeDiscounts: {
		title: 'Знижки за об\'єм',
		columns: [{},{},{ text: 'Назва' },{ text: 'Від' },{ text: 'До' },{ text: 'Ставка' }]
	},
	gridZones: {
		title: 'Зони',
		columns: [{},{ text: 'Код' },{ text: 'Назва' },{ editor: { emptyText: 'вкажіть суму' },  text: 'Мінімальне покриття' },{ text: 'Валюта' }]
	},
	title: 'Нещасний випадок'
});
Ext.define('CFJS.locale.ukr.plugins.insurance.civilliability.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.civilliability.view.SettingsPanel',
	filterOptZone: [[1,'Зона 1'],[2,'Зона 2'],[3,'Зона 3'],[4,'Зона 4'],[5,'Зона 5'],[6,'Зона 6'],[7,'Зона 7']],
	gridBonusMalus: {
		title: 'Бонус-Малус',
		columns: [{},{ text: 'Код' },{ text: 'Ставка' }]
	},
	gridCarBrands: {
		title: 'Марки автомобілей',
		columns: [{},{ text: 'Назва' }]
	},
	gridCarModels: {
		title: 'Моделі автомобілей',
		columns: [{},{ editor: { emptyText: 'оберіть марку' }, filter: { emptyText: 'текст для пошука...' }, text: 'Марка' },{ text: 'Назва' }]
	},
	gridCities: {
		title: 'Міста',
		columns: [{},{ editor: { emptyText: 'оберіть місто' }, filter: { emptyText: 'текст для пошука...' }, text: 'Місто' },{ text: 'Зона' },{ filter: { emptyText: 'текст для пошука...' }, text: 'Код' }]
	},
	gridDuration: {
		title	: 'Період використання',
		columns: [{},{ text: 'Період' },{ text: 'Ставка' }]
	},
	gridPlace: {
		title	: 'Зони',
		columns: [{},{ text: 'Зона' },{ text: 'K2 (Ставка)' }]
	},
	gridTariffs: {
		title	: 'Тарифи',
		columns: [{},{},{ text: 'Тип ТЗ' },{ text: 'Зона' },{ filter: { options: [[0,'Юр.особа'],[1,'Фіз.особа']] }, text: 'Тип страхувальника' },{ text: 'K1' },{ text: 'K3' },{ text: 'K3 (таксі)' }]
	},
	gridTypeVehicle: {
		title: 'Тип ТЗ',
		columns: [{},{ text: 'Код' },{ text: 'Назва' },{ editor: { fieldLabel: 'Місткість' }, text: 'Місткість' }]
	},
	gridValidity: {
		title	: 'Період контракту',
		columns: [{},{ text: 'Період' },{ text: 'Ставка' }]
	},
	gridVolumeDiscounts: {
		title: 'Знижки за об\'єм',
		columns: [{},{},{ text: 'Назва' },{ text: 'Від' },{ text: 'До' },{ text: 'Ставка' }]
	},
	title: 'ОЦВ'
});
Ext.define('CFJS.locale.ukr.plugins.insurance.civilliability.view.SettingsModel', {
    override: 'CFJS.plugins.insurance.civilliability.view.SettingsModel',
	persons	: {
		type: 'enumerated',
		data: [{ id: '0', name: "Юр.особа" },{ id: '1', name: "Фіз.особа" }]
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.civilliability.panel.CustomerEditCard1', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard1',
	config	: { title: 'Фіз.особа' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				computing	: {
					items:[{ fieldLabel: 'Базовый тариф' },{ fieldLabel: 'K1' },{ fieldLabel: 'K2' },{ fieldLabel: 'K3' },{ fieldLabel: 'K4' },{ fieldLabel: 'K5' },
						{ fieldLabel: 'K6' },{ fieldLabel: 'K7' },{ fieldLabel: 'K8' },{ fieldLabel: 'K9' },{ fieldLabel: 'K10' },{ fieldLabel: 'Сума' }]
				},
				insuranceObject: {
					items: [{
						items: [{ items: [{ fieldLabel: 'Тип ТЗ' },{},{ fieldLabel: 'Код' }] }]
					},{
						items: [{ emptyText: 'виберіть країну', fieldLabel: 'Країна реєстрації' },{ emptyText: 'виберіть місто', fieldLabel: 'Місто реєстрації' },{ fieldLabel: 'МРЕВ' },{ fieldLabel: 'Зона' }]
					}]
				},
				period: {
					items: [
						{ emptyText: 'початок дії', fieldLabel: 'Початок дії' },
						{ emptyText: 'закінчення дії', fieldLabel: 'Закінчення дії' },
						{ fieldLabel: 'Пільговий' },
						{ emptyText: 'виберіть бонус-малус', fieldLabel: 'Бонус-Малус' }
					]
				},
				useProperties: {
					items: [
						{ fieldLabel: 'Кількість транспортних засобів' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Автомобіль використовується в таксі">*</span>'], fieldLabel: 'Таксі' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="До водінню допускаються особи з водійським стажем менше 3-х років">*</span>'], fieldLabel: 'Використовують новачки' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Страхувальник був помічений в шахрайстві">*</span>'], fieldLabel: 'Шахрайство' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Для річних договорів">*</span>'], fieldLabel: 'Невикористані місяці' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.civilliability.panel.CustomerEditCard2', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard2',
	config	: { title: 'Фіз.особа' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				detailVIN: {
					items: [{ fieldLabel: 'Номер кузова (VIN)' },{ fieldLabel: 'Код' }]
				},
				detailMarka: {
					items: [{ emptyText: 'виберіть марку автомобіля', fieldLabel: 'Марка' },{ emptyText: 'виберіть модель автомобіля', fieldLabel: 'Модель' }]
				},
				detailYearNumber: {
					items: [{ emptyText: 'виберіть рік випуску', fieldLabel: 'Рік випуску' },{ fieldLabel: 'Номерний знак' }]
				},
				nextControl: {
					items: [{ fieldLabel: 'Дата наступного ОТК' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.civilliability.panel.CustomerEditCard3', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard3',
	config	: { title: 'Фіз.особа' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				mainBlock	: {
					items: [{ emptyText: 'номер документа', fieldLabel: 'Номер документа' },{ emptyText: 'номер замовлення', fieldLabel	: 'Номер замовлення' }]
				},
				period: {
					items: [{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },{ fieldLabel: 'Стікер' }]
				},
				client	: {
					items: [{ emptyText: 'виберіть клієнта', fieldLabel: 'Клієнт' },{ emptyText: 'виберіть документ', fieldLabel: 'Документ' }]
				},
				consumerDetail: {
					items: [{ fieldLabel: 'Пільговий' },{ emptyText: 'виберіть страховика', fieldLabel: 'Страховик' }]
				},
				insuranceAmount: {
					items: [{ fieldLabel: 'Страхова сума (майно)' },{ fieldLabel: 'Страхова сума (здоров\'я)' },{ emptyText: 'виберіть розмір франшизи', fieldLabel: 'Розмір франшизи' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.grid.PolicyConsumerPanel', {
    override: 'CFJS.plugins.insurance.grid.PolicyConsumerPanel',
    config	: {
    	actions: {
    		customerPicker: {
    			title	: 'Додати зі списку клієнтів',
    			tooltip	: 'Додати запис зі списку клієнтів'
    		}
    	},
		title	: 'Застраховані особи'
    },
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{ text: 'Ім\'я' },{ text: 'По батькові' },{ text: 'Прізвище' },{ text: 'День народження' },{ text: 'Адреса' },{ text: 'Паспорт' },{ text: 'Сума' }]
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.health.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.health.view.SettingsPanel',
	gridAgeLimits: {
		title: 'Вікові обмеження',
		columns: [{},{},{ text: 'Назва' },{ text: 'Зона' },{ text: 'Мінімальний вік' },{ text: 'Максимальний вік' },{ text: 'Ставка' }]
	},
	gridConditions: {
		title: 'Додаткові умови',
		columns: [{},{ text: 'Назва' },{ text: 'Ставка' },{ text: 'Сума' }]
	},
	gridCountryZone: {
		title: 'Країни по зонам',
		columns: [{},{ text: 'Зона' },{ filter: { emptyText: 'країна' }, text: 'Країна' }]
	},
	gridDurationTrip: {
		title: 'Тривалість поїздки',
		columns: [{},{},{ text: 'Назва' },{ text: 'Мінімум днів' },{ text: 'Максимум днів' },{ text: 'Ставка' }]
	},
	gridMultivisa: {
		title	: 'Мультивіза',
		columns: [{},{ text: 'Термін перебування' },{ text: 'Термін дії' }]
	},
	gridSchemeData: {
		title: 'Тарифи',
		columns: [{},{ text: 'Тривалість' },{ text: 'Грошове покриття' },{ text: 'Ставка' }]
	},
	gridSchemeConditions: {
		title: 'Додаткові умови схеми',
		columns: [{},{},{ text: 'Умова' }]
	},
	gridVolumeDiscounts: {
		title: 'Знижки за об\'єм',
		columns: [{},{},{ text: 'Назва' },{ text: 'Від' },{ text: 'До' },{ text: 'Ставка' }]
	},
	gridZones: {
		title: 'Зони',
		columns: [{},{ text: 'Код' },{ text: 'Назва' },{ editor: { emptyText: 'вкажіть суму' },  text: 'Мінімальне покриття' },{ text: 'Валюта' },{ text: 'Мінімум днів' },{ text: 'Додаткові дні' },{ text: 'Максимальний вік' }]
	},
	title: 'Медицина'
});
Ext.define('CFJS.locale.ukr.plugins.insurance.pages.Settings', {
    override: 'CFJS.plugins.insurance.pages.Settings',
    config: {
    	actions	: {
			addRecord	: { title: 'Додати запис', tooltip: 'Додати запис' },
    		copyRecord	: { title: 'Скопіювати запис', tooltip: 'Скопіювати поточний запис' },
			reloadStore	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeRecord: { title: 'Видалити записи', tooltip: 'Видалити обрані записи' },
			saveRecord	: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' }
    	},
    	title	: 'Налаштування'
    }
});
Ext.define('CFJS.locale.ukr.plugins.panel.SettingsPanel', {
    override: 'CFJS.plugins.insurance.panel.SettingsPanel',
    config: {
    	gridCashCover	: {
    		title	: 'Грошове покриття',
    		columns	: [{},{ text: 'Сума' }]
    	},
    	gridDefaults	: { emptyText: 'Немає даних для відображення' },
    	gridFranchises	: {
    		title	: 'Франшиза',
    		columns	: [{},{ text: 'Сума' }]
    	},
    	gridProperty	: {
    		title	: 'Основні змінні',
    		columns	: [{},{ text: 'Змінна' },{ editor: { fieldLabel: 'Значення' }, text: 'Значення' }]
    	},
    	gridSchemes		: {
    		title	: 'Схеми страхування',
			columns	: [{},{ text: 'Код' },{ text: 'Назва' },{ text: 'Початкова дата' },{ text: 'Кінцева дата' }]
    	},
    	gridCompanyScheme	: {
    		title	: 'Схеми компанії',
			columns	: [{},{ text: 'Компанії' },{ text: 'Схеми' }]
    	},
    	schemeDefaults	: {
			columns	: [{},{ text: 'Схема' }]
    	}
    },
    monthsRenderer: function(value) {
    	if (value > 0) {
    		if (value <= .5) return '15 днів';
    		if (value === 1) return '1 місяць';
    		if (value < 5) return value + ' місяці';
    		return value + ' місяців';
    	}
    }
});
Ext.define('CFJS.locale.ukr.plugins.insurance.baggage.panel.CustomerEditCard1', {
	override: 'CFJS.plugins.insurance.baggage.panel.CustomerEditCard1',
	config	: { title: 'Багаж' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				schemeAmount: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть територію', fieldLabel: 'Територія' },
						{ emptyText: 'оберіть суму', fieldLabel: 'Страхове покриття' },
						{ emptyText: 'розмір франшизи', fieldLabel: 'Розмір франшизи' }
					]
				},
				maindata: {
					items: [
						{ emptyText: 'кількість днів', fieldLabel: 'Кількість днів' },
						{ emptyText: 'кількість людей', fieldLabel: 'Кількість людей' },
						{ bind : { fieldLabel: 'Страховик' }, emptyText: 'виберіть страховика' }
					]
				},
				totalAmount: {
					items: [
						{ emptyText: 'розмір страхування', fieldLabel: 'Розмір страхування' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.baggage.panel.CustomerEditCard2', {
	override: 'CFJS.plugins.insurance.baggage.panel.CustomerEditCard2',
	config	: { title: 'Багаж' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				period: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ emptyText: 'дата виїзду', fieldLabel: 'Дата виїзду' },
						{ emptyText: 'дата повернення', fieldLabel: 'Дата повернення' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' }
					]
				},
				client	: {
					items: [
						{ emptyText: 'оберіть клієнта', fieldLabel: 'Клієнт' },
						{ emptyText: 'оберіть документ', fieldLabel: 'Документ' }
					]
				},
				insuranceAmount: {
					items: [
						{ emptyText: 'оберіть суму', fieldLabel: 'Сума страхування' },
						{ emptyText: 'розмір франшизи', fieldLabel: 'Розмір франшизи' },
						{ fieldLabel: 'Франшиза' },
						{ bind : { fieldLabel: 'Страховик' }, emptyText: 'виберіть страховика' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.baggage.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.baggage.panel.CustomerEditor',
	config	: { title: 'Багаж' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				consumerDetail: {
					items: [{ emptyText: 'оберіть документ', fieldLabel: 'Документ' }]
				},
				period: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ emptyText: 'дата виїзду', fieldLabel: 'Дата виїзду' },
						{ emptyText: 'дата повернення', fieldLabel: 'Дата повернення' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' }
					]
				},
				programCountry: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть територію', fieldLabel: 'Територія' },
						{ emptyText: 'оберіть суму', fieldLabel: 'Страхове покриття' },
						{ emptyText: 'розмір франшизи', fieldLabel: 'Розмір франшизи' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.casualty.panel.CustomerEditCardAcc1', {
	override: 'CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc1',
	config	: { title: 'Калькулятор страхування від нещасних випадків' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				schemeAmount: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть територію', fieldLabel: 'Територія' },
						{ emptyText: 'оберіть суму', fieldLabel: 'Страхове покриття' }
					]
				},
				maindata: {
					items: [
						{ emptyText: 'кількість днів', fieldLabel: 'Кількість днів' },
						{ emptyText: 'кількість людей', fieldLabel: 'Кількість людей' },
						{ emptyText: 'середній вік', fieldLabel: 'Середній вік' }
					]
				},
				conditions: { fieldLabel: 'Додаткові умови' },
				totalAmount: {
					items: [{ emptyText: 'розмір страхування', fieldLabel: 'Розмір страхування' },{ emptyText: 'валюта', fieldLabel: 'Валюта' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.casualty.panel.CustomerEditCardAcc2', {
	override: 'CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc2',
	config	: { title: 'Нещасний випадок' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				period: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ emptyText: 'дата виїзду', fieldLabel: 'Дата виїзду' },
						{ emptyText: 'дата повернення', fieldLabel: 'Дата повернення' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' }
					]
				},
				client	: {
					items: [{ emptyText: 'оберіть клієнта', fieldLabel: 'Клієнт' },{ emptyText: 'оберіть документ', fieldLabel: 'Документ' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.casualty.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.casualty.panel.CustomerEditor',
	config	: { title: 'Нещасний випадок' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				conditions: { fieldLabel: 'Додаткові умови' },
				consumerDetail: {
					items: [{ emptyText: 'оберіть документ', fieldLabel: 'Документ' }]
				},
				period: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ emptyText: 'дата виїзду', fieldLabel: 'Дата виїзду' },
						{ emptyText: 'дата повернення', fieldLabel: 'Дата повернення' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' }
					]
				},
				programCountry: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть територію', fieldLabel: 'Територія' },
						{ emptyText: 'оберіть суму', fieldLabel: 'Страхове покриття' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.health.panel.CustomerEditCardMed1', {
	override: 'CFJS.plugins.insurance.health.panel.CustomerEditCardMed1',
	config	: { title: 'Калькулятор медичного страхування' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				programCountry: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть територію', fieldLabel: 'Територія' },
						{ emptyText: 'оберіть країну', fieldLabel: 'Країна' },
						{ emptyText: 'оберіть суму', fieldLabel: 'Страхове покриття' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта', }
					]
				},
				maindata: {
					items: [
						{ emptyText: 'кількість днів', fieldLabel: 'Кількість днів' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' },
						{ emptyText: 'кількість людей', fieldLabel: 'Кількість людей' },
						{ emptyText: 'середній вік', fieldLabel: 'Середній вік' },
						{ text: 'Мультвіза', tooltip: 'Мультвіза' }
					]
				},
				conditions: { fieldLabel: 'Додаткові умови' },
				totalAmount: {
					items: [{ emptyText: 'розмір страхування', fieldLabel: 'Розмір страхування' },{ emptyText: 'валюта', fieldLabel: 'Валюта' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.health.panel.CustomerEditCardMed2', {
	override: 'CFJS.plugins.insurance.health.panel.CustomerEditCardMed2',
	config	: { title: 'Медицина' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				period: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ emptyText: 'дата виїзду', fieldLabel: 'Дата виїзду' },
						{ emptyText: 'дата повернення', fieldLabel: 'Дата повернення' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' },
						{ emptyText: 'період дії', fieldLabel: 'Період дії' },
						{ text: 'Мультивіза', tooltip: 'Мультивіза' }
					]
				},
				client	: {
					items: [{ emptyText: 'оберіть клієнта', fieldLabel: 'Клієнт' },{ emptyText: 'оберіть документ', fieldLabel: 'Документ' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.health.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.health.panel.CustomerEditor',
	config	: { title: 'Медицина' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				conditions		: { fieldLabel: 'Додаткові умови' },
				consumerDetail	: { items: [{ emptyText: 'оберіть документ', fieldLabel: 'Документ' }] },
				period			: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ emptyText: 'дата виїзду', fieldLabel: 'Дата виїзду' },
						{ emptyText: 'дата повернення', fieldLabel: 'Дата повернення' },
						{ emptyText: 'термін перебування', fieldLabel: 'Термін перебування' },
						{ emptyText: 'термін дії', fieldLabel: 'Термін дії' },
						{ text: 'Мультвіза', tooltip: 'Мультвіза' }
					]
				},
				programCountry	: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть територію', fieldLabel: 'Територія' },
						{ emptyText: 'оберіть країну', fieldLabel: 'Країна' },
						{ emptyText: 'оберіть суму', fieldLabel: 'Страхове покриття' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта', }
					]	
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.civilliability.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditor',
	config	: { title: 'Фіз.особа' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				consumerDetail	: {
					items: [
						{ emptyText: 'оберіть схему', fieldLabel: 'Схема страхування' },
						{ emptyText: 'оберіть документ', fieldLabel: 'Документ' },
						{ fieldLabel: 'Пільговий' }
					]
				},
				computing	: {
					items: [{ fieldLabel: 'Базовий тариф' },{ fieldLabel: 'K1' },{ fieldLabel: 'K2' },{ fieldLabel: 'K3' },{ fieldLabel: 'K4' },{ fieldLabel: 'K5' },
							{ fieldLabel: 'K6' },{ fieldLabel: 'K7' },{ fieldLabel: 'K8' },{ fieldLabel: 'K9' },{ fieldLabel: 'K10' },{ fieldLabel: 'Сума' }]
				},
				insuranceAmount: {
					items: [
						{ fieldLabel: 'Страхова сума (майно)' },
						{ fieldLabel: 'Страхова сума (здоров\'я)' },
						{ emptyText: 'оберіть розмір франшизи', fieldLabel: 'Розмір франшизи' },
						{ emptyText: 'оберіть валюту', fieldLabel: 'Валюта' },
						{ emptyText: 'оберіть бонус-малус', fieldLabel: 'Бонус-Малус' }
					]
				},
				insuranceObject: {
					items: [{
						items: [
							{ emptyText: 'XXXXXXXXXXXXXXXXX', fieldLabel: 'Номер кузова (VIN)' },
							{ items: [{ fieldLabel: 'Тип ТЗ' },{},{ fieldLabel: 'Код' }] },
							{ emptyText: 'оберіть марку автомобіля', fieldLabel: 'Марка' },
							{ emptyText: 'оберіть модель автомобіля', fieldLabel: 'Модель' }
						]
					},{
						items: [
							{ emptyText: 'оберіть рік випуску', fieldLabel: 'Рік випуску' },
							{ emptyText: 'оберіть країну', fieldLabel: 'Країна реєстрації' },
							{ emptyText: 'оберіть місто', fieldLabel: 'Місто реєстрації' },
							{ fieldLabel: 'МРЕВ' },
							{ fieldLabel: 'Зона' },
							{ fieldLabel: 'Номерний знак' }
						]
					}]
				},
				period: {
					items: [
						{ emptyText: 'дата договору', fieldLabel: 'Дата договору' },
						{ fieldLabel: 'Стікер', items: [{ emptyText: 'XX' },{ emptyText: 'XXXXXXXXXX' }] },
						{ emptyText: 'початок дії', fieldLabel: 'Початок дії' },
						{ emptyText: 'завершення дії', fieldLabel: 'Завершення дії' }
					]
				},
				useProperties: {
					items: [
						{ fieldLabel: 'Дата наступного ОТК' },
						{ fieldLabel: 'Кількість ТС' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Автомобіль використовується в таксі">*</span>'], fieldLabel: 'Таксі' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="До водіння допускаються особи з водійським стажем менше 3-х років">*</span>'], fieldLabel: 'Використовують новачки' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Страхувальник був помічений в шахрайстві">*</span>'], fieldLabel: 'Шахрайство' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Для річних договорів">*</span>'], fieldLabel: 'Невикористовувані місяці' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ukr.plugins.insurance.mixin.DescriptionValue', {
	override: 'CFJS.plugins.insurance.mixin.DescriptionValue',
	attentionMess			: 'Увага: ',
	noDictDataMess			: 'Немає довідкових даних ',
	durationTripMess		: 'за часом перебування.',
	noSchemeAmountMess		: 'за схемою страхування та сумою покриття.',
	maxPeriodMess			: 'Перевищено максимальний період страхування!',
	missingDateBirthMess	: 'Відсутня дата народження.',
	missingAgeRangesMess	: 'за діапазонами віку.',
	missDictAgeRangesMess	: 'Відсутні довідкові дані по діапазонах віку.',
	forSchemeMess			: 'Для схеми:',
	numberOfDaysMess		: ', кількості днів:',
	amountCoverageMess		: ', суми покриття:',
	ageMess					: ', віку: ',
	tariffMess				: ', тариф = ',
	koeffAgeMess			: ' коефіцієнт за віком = ',
	amountAgeMess			: ', сума за віком = ',
	discountForNumberMess	: 'Знижка за кількість застрахованих - ',
	conditionsRateMess		: 'Коефіцієнт за додаткові умови = ',
	insuredMess				: '-й застрахований.\n',
	amountMess				: 'Сума = ',
	totalAmountMess			: 'Загальна сума = '
});