Ext.define('CFJS.plugins.locale.ru.service.grid.AirlineSegmentsPanel', {
	override: 'CFJS.plugins.service.grid.AirlineSegmentsPanel',
	config	: {
		bind: { title: 'Маршрут: {routeBySegments}', store: '{segments}' }
	}
});
Ext.define('CFJS.plugins.locale.ru.service.grid.ServicePanel', {
	override: 'CFJS.plugins.service.grid.ServicePanel',
	config	: {
		actions	: {
			addRecord: {
				title	: 'Добавить "ручную" услугу',
				tooltip	: 'Добавить услугу в "ручном" режиме'
			},
			addVoid		: {
				title	: 'Добавить возврат',
				tooltip	: 'Добавить возврат выделенной услуги',
			},
			copyRecord	: {
				title	: 'Скопировать услугу',
				tooltip	: 'Скопировать выделенную услугу'
			},
			editRecord	: {
				title	: 'Редактировать услугу',
				tooltip	: 'Редактировать выделенную услугу'
			},
			printRecord	: {
				title	: 'Печатать услугу',
				tooltip	: 'Печатать выделенную услугу'
			},
			removeRecord: {
				title	: 'Удалить услуги',
				tooltip	: 'Удалить выделенные услуги'
			}
		},
		title	: 'Услуги'
	}
});
Ext.define('CFJS.plugins.locale.ru.service.grid.TaxAmountPanel', {
	override: 'CFJS.plugins.service.grid.TaxAmountPanel',
	config	: {
		actions	: {
			editServices: {
				title	: 'Сервисный сбор и скидки',
				tooltip	: 'Редактировать сервисный сбор и скидки'
			}
		},
		title	: 'Тарифный блок'
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, { columns:[{},{ text: "Код" },{ text: "Сумма" },{ text: "Создано" }] });
		me.callParent();
	}
});
Ext.define('CFJS.plugins.locale.ru.service.panel.Editor', {
	override: 'CFJS.plugins.service.panel.Editor',
	config: {
		actions	: { back: { tooltip: 'Назад к списку услуг' } },
		bind	: { readOnly: '{!_itemName_.updatable}', title: 'Послуга "{_itemName_.sector.name} № {_itemName_.number}". {_itemName_.entityName}' }
	}
});
Ext.define('CFJS.plugins.locale.ru.service.panel.InsuranceEditor', {
	override: 'CFJS.plugins.service.panel.InsuranceEditor',
	config: {
		actions: {
			writeOut: {
				title	: 'Выписать полис',
				tooltip	: 'Выписать страховой полис'
			}
		}
	}
});
Ext.define('CFJS.plugins.locale.ru.service.view.EditController', {
	override: 'CFJS.plugins.service.view.EditController',
	messages: {
		confirmAction		: 'Подтвердите действие',
		documentsRedirect	: 'Перейти к документам?',
		documentSign		: 'Наложить подпись на документ?'
	}
});
