Ext.define('CFJS.locale.ru.plugins.insurance.wizard.Browser', {
	override: 'CFJS.plugins.insurance.wizard.Browser',
	initComponent: function() {
		var me = CFJS.merge(this, {
			bbar: {
				items: [{ text: 'Предыдущая' },{},{ text: 'Следующая' },{ text: 'Продолжить' },{},{ text: 'Завершить' },{ text: 'Прервать' }]
			},
			browserViewConfig: { title: 'Выберите вид страхования' }
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.wizard.ServiceGrid', {
	override: 'CFJS.plugins.insurance.wizard.ServiceGrid',
    config	: {
    	title	: 'Заявка на обслуживание',
    	actions: {
    		printRecord	: {
    			title	: 'Напечатать выбранний счет-фактуру',
    			tooltip	: 'Напечатать выбранний счет-фактуру'
    		},
    		removeRecord: {
    			title	: 'Удалить выбранние счета-фактуры',
    			tooltip	: 'Удалить выбранние счета-фактуры'
    		}
    	}
    },
    renderColumns: function() {
    	var columns = this.callParent(arguments);
    	columns[0].text = 'Начальная дата';
    	columns[1].text = 'Конечная дата';
    	columns[2].text = 'Наименование';
    	columns[3].text = 'Системный номер';
    	columns[4].text = 'Страховая сумма';
    	columns[5].text = 'Сумма платежа';
    	columns[6].text = 'Валюта платежа';
    	columns[7].text = 'Сумма к оплате';
    	columns[8].text = 'Валюта к оплате';
    	return columns;
    }
});
Ext.define('CFJS.locale.ru.plugins.insurance.baggage.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.baggage.view.SettingsPanel',
	gridDurationTrip: {
		title: 'Продолжительность поездки',
		columns: [{},{ text: 'Схема' },{ text: 'Название' },{ text: 'Минимум дней' },{ text: 'Максимум дней' },{ text: 'Ставка' }]
	},
	gridSchemeData: {
		title: 'Тарифы',
		columns: [{},{ text: 'Продолжительность' },{ text: 'Денежное покрытие' },{ text: 'Ставка' }]
	},
	gridZones: {
		title: 'Зоны',
		columns: [{},{ text: 'Код' },{ text: 'Название' },{ editor: { emptyText: 'укажите сумму' }, text: 'Минимальное покрытие' },{ text: 'Валюта' },{ editor: { emptyText: 'укажите франшизу' }, text: 'Франшиза' }]
	},
	gridVolumeDiscounts: {
		title: 'Скидки за объем',
		columns: [{},{},{ text: 'Название' },{ text: 'От' },{ text: 'До' },{ text: 'Ставка' }]
	},
	title: 'Багаж'
});
Ext.define('CFJS.locale.ru.plugins.insurance.casualty.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.casualty.view.SettingsPanel',
	gridAgeLimits: {
		title: 'Возрастные ограничения',
		columns: [{},{},{ text: 'Название' },{ text: 'Минимальный возраст' },{ text: 'Максимальный возраст' },{ text: 'Ставка' }]
	},
	gridConditions: {
		title: 'Дополнительные условия',
		columns: [{},{ text: 'Название' },{ text: 'Ставка' },{ text: 'Сумма' }]
	},
	gridDurationTrip: {
		title: 'Продолжительность поездки',
		columns: [{},{ text: 'Схема' },{ text: 'Название' },{ text: 'Минимум дней' },{ text: 'Максимум дней' },{ text: 'Ставка' }]
	},
	gridSchemeData: {
		title: 'Тарифы',
		columns: [{},{ text: 'Продолжительность' },{ text: 'Денежное покрытие' },{ text: 'Ставка' }]
	},
	gridSchemeConditions: {
		title: 'Условия схемы',
		columns: [{},{},{ text: 'Условие' }]
	},
	gridVolumeDiscounts: {
		title: 'Скидки за объем',
		columns: [{},{},{ text: 'Название' },{ text: 'От' },{ text: 'До' },{ text: 'Ставка' }]
	},
	gridZones: {
		title: 'Зоны',
		columns: [{},{ text: 'Код' },{ text: 'Название' },{ editor: { emptyText: 'укажите сумму' }, text: 'Минимальное покрытие' },{ text: 'Валюта' }]
	},
	title: 'Несчастный случай'
});
Ext.define('CFJS.locale.ru.plugins.insurance.civilliability.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.civilliability.view.SettingsPanel',
	filterOptZone: [[1,'Зона 1'],[2,'Зона 2'],[3,'Зона 3'],[4,'Зона 4'],[5,'Зона 5'],[6,'Зона 6'],[7,'Зона 7']],
	gridBonusMalus: {
		title: 'Бонус-Малус',
		columns: [{},{ text: 'Код' },{ text: 'Ставка' }]
	},
	gridCarBrands: {
		title: 'Марки автомобилей',
		columns: [{},{ text: 'Название' }]
	},
	gridCarModels: {
		title: 'Модели автомобилей',
		columns: [{},{ editor: { emptyText: 'выберите марку' }, filter: { emptyText: 'текст для поиска...' }, text: 'Марка' },{ text: 'Название' }]
	},
	gridCities: {
		title: 'Города',
		columns: [{},{ editor: { emptyText	: 'выберите город' }, filter: { emptyText: 'текст для поиска...' }, text: 'Город' },{ text: 'Зона' },{ filter: { emptyText: 'текст для поиска...' }, text: 'Код' }]
	},
	gridDuration: {
		title	: 'Период использования',
		columns: [{},{ text: 'Период' },{ text: 'Ставка' }]
	},
	gridPlace: {
		title	: 'Зоны',
		columns: [{},{ text: 'Зона' },{ text: 'K2 (Ставка)' }]
	},
	gridTariffs: {
		title	: 'Тарифы',
		columns: [{},{},{ text: 'Тип ТС' },{ text: 'Зона' },{ filter: { options: [[0,'Юр.лицо'],[1,'Фіз.лицо']] }, text: 'Тип страхователя' },{ text: 'K1' },{ text: 'K3' },{ text: 'K3 (такси)' }]
	},
	gridTypeVehicle: {
		title: 'Тип ТС',
		columns: [{},{ text: 'Код' },{ text: 'Название' },{ editor: { fieldLabel: 'Вместимость' }, text: 'Вместимость' }]
	},
	gridValidity: {
		title	: 'Период контракта',
		columns: [{},{ text: 'Период' },{ text: 'Ставка' }]
	},
	gridVolumeDiscounts: {
		title: 'Скидки за объем',
		columns: [{},{},{ text: 'Название' },{ text: 'От' },{ text: 'До' },{ text: 'Ставка' }]
	},
	title: 'ОСАГО'
});
Ext.define('CFJS.locale.ru.plugins.insurance.civilliability.view.SettingsModel', {
	override: 'CFJS.plugins.insurance.civilliability.view.SettingsModel',
	persons	: {
		type: 'enumerated',
		data: [{ id: '0', name: "Юр.лицо" },{ id: '1', name: "Физ.лицо" }]
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.civilliability.panel.CustomerEditCard1', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard1',
	config	: { title: 'Физ.лицо' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				computing	: {
					items:[{ fieldLabel: 'Базовый тариф' },{ fieldLabel: 'K1' },{ fieldLabel: 'K2' },{ fieldLabel: 'K3' },{ fieldLabel: 'K4' },{ fieldLabel: 'K5' },
							{ fieldLabel: 'K6' },{ fieldLabel: 'K7' },{ fieldLabel: 'K8' },{ fieldLabel: 'K9' },{ fieldLabel: 'K10' },{ fieldLabel: 'Сумма' }]
				},
				insuranceObject: {
					items: [{
						items: [{ items: [{ fieldLabel: 'Тип ТС' },{},{ fieldLabel: 'Код' }] }]
					},{
						items: [{ emptyText: 'выберите страну', fieldLabel: 'Страна регистрации' },{ emptyText: 'выберите город', fieldLabel: 'Город регистрации' },{ fieldLabel: 'МРЭО' },{ fieldLabel: 'Зона' }]
					}]
				},
				period: {
					items: [
						{ emptyText: 'начало действия', fieldLabel: 'Начало действия' },
						{ emptyText: 'окончание действия', fieldLabel: 'Окончание действия' },
						{ fieldLabel: 'Льготный' },
						{ emptyText: 'выберите бонус-малус', fieldLabel: 'Бонус-Малус' }
					]
				},
				useProperties: {
					items: [
						{ fieldLabel: 'Количество транспортных средств' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Автомобиль используется в такси">*</span>'], fieldLabel: 'Такси' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="К вождению допускаются особы с водительским стажем меньше 3-х лет">*</span>'], fieldLabel: 'Используют новички' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Страхователь был замечен в мошенничестве">*</span>'], fieldLabel: 'Мошенничество' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Для годовых договоров">*</span>'], fieldLabel: 'Неиспользуемые месяцы' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.civilliability.panel.CustomerEditCard2', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard2',
	config	: { title: 'Физ.лицо' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				detailVIN: {
					items: [{ fieldLabel: 'Номер кузова (VIN)' },{ fieldLabel: 'Код' }]
				},
				detailMarka: {
					items: [{ emptyText: 'выберите марку автомобиля', fieldLabel: 'Марка' },{ emptyText: 'выберите модель автомобиля', fieldLabel: 'Модель' }]
				},
				detailYearNumber: {
					items: [{ emptyText: 'выберите год выпуска', fieldLabel: 'Год выпуска' },{ fieldLabel: 'Номерной знак' }]
				},
				nextControl: {
					items: [{ fieldLabel: 'Дата следующего ОТК' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.civilliability.panel.CustomerEditCard3', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditCard3',
	config	: { title: 'Физ.лицо' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				mainBlock	: {
					items: [{ emptyText: 'номер документа', fieldLabel: 'Номер документа' },{ emptyText: 'номер заказа', fieldLabel: 'Номер заказа' }]
				},
				period		: {
					items: [{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },{ fieldLabel: 'Стикер' }]
				},
				client		: {
					items: [{ emptyText: 'выберите клиента', fieldLabel: 'Клиент' },{ emptyText: 'выберите документ', fieldLabel: 'Документ' }]
				},
				consumerDetail: {
					items: [{ fieldLabel: 'Льготный' },{ emptyText: 'выберите страховщика', fieldLabel: 'Страховщик' }]
				},
				insuranceAmount: {
					items: [{ fieldLabel: 'Страховая сумма (имущество)' },{ fieldLabel: 'Страховая сумма (здоровье)' },{ emptyText: 'выберите размер франшизы', fieldLabel: 'Размер франшизы' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.grid.PolicyConsumerPanel', {
    override: 'CFJS.plugins.insurance.grid.PolicyConsumerPanel',
    config	: {
    	actions: {
    		customerPicker: {
    			title	: 'Добавить из списка клиентов',
    			tooltip	: 'Добавить запись из списка клиентов'
    		}
    	},
    	config	: { title: 'Застрахованные лица' }
    },
	initComponent: function() {
		var me = this; 
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{ text: 'Имя' },{ text: 'Отчество' },{ text: 'Фамилия' },{ text: 'День рождения' },{ text: 'Адрес' },{ text: 'Паспорт' },{ text: 'Сумма' }]
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.health.view.SettingsPanel', {
	override: 'CFJS.plugins.insurance.health.view.SettingsPanel',
	gridAgeLimits: {
		title: 'Возрастные ограничения',
		columns: [{},{},{ text: 'Название' },{ text: 'Зона' },{ text: 'Минимальный возраст' },{ text: 'Максимальный возраст' },{ text: 'Ставка' }]
	},
	gridConditions: {
		title: 'Дополнительные условия',
		columns: [{},{ text: 'Название' },{ text: 'Ставка' },{ text: 'Сумма' }]
	},
	gridCountryZone: {
		title: 'Страны по зонам',
		columns: [{},{ text: 'Зона' },{ filter: { emptyText: 'страна' }, text: 'Страна' }]
	},
	gridDurationTrip: {
		title: 'Продолжительность поездки',
		columns: [{},{},{ text: 'Название' },{ text: 'Минимум дней' },{ text: 'Максимум дней' },{ text: 'Ставка' }]
	},
	gridMultivisa: {
		title	: 'Мультивиза',
		columns: [{},{ text: 'Срок пребывания' },{ text: 'Срок действия' }]
	},
	gridSchemeData: {
		title: 'Тарифы',
		columns: [{},{ text: 'Продолжительность' },{ text: 'Денежное покрытие' },{ text: 'Ставка' }]
	},
	gridSchemeConditions: {
		title: 'Дополнительные условия схемы',
		columns: [{},{},{ text: 'Условие' }]
	},
	gridVolumeDiscounts: {
		title: 'Скидки за объем',
		columns: [{},{},{ text: 'Название' },{ text: 'От' },{ text: 'До' },{ text: 'Ставка' }]
	},
	gridZones: {
		title: 'Зоны',
		columns: [{},{ text: 'Код' },{ text: 'Название' },{ editor: { emptyText: 'укажите сумму' }, text: 'Минимальное покрытие' },{ text: 'Валюта' },{ text: 'Минимум дней' },{ text: 'Дополнительные дни' },{ text: 'Максимальный возраст' }]
	},
	title: 'Медицина'
});
Ext.define('CFJS.locale.ru.plugins.insurance.pages.Settings', {
    override: 'CFJS.plugins.insurance.pages.Settings',
    config: {
    	actions	: {
			addRecord	: { title: 'Добавить запись', tooltip: 'Добавить новую запись' },
    		copyRecord	: { title: 'Скопировать запись', tooltip: 'Скопировать текущую запись' },
			reloadStore	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeRecord: { title: 'Удалить записи', tooltip: 'Удалить выделенные записи' },
			saveRecord	: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' }
    	},
    	title	: 'Настройки'
    }
});
Ext.define('CFJS.locale.ru.plugins.panel.SettingsPanel', {
    override: 'CFJS.plugins.insurance.panel.SettingsPanel',
    config: {
    	gridCashCover: {
    		title	: 'Денежное покрытие',
    		columns: [{},{ text: 'Сумма' }]
    	},
    	gridDefaults: { emptyText: 'Нет данных для отображения' },
    	gridFranchises: {
    		title	: 'Франшиза',
    		columns: [{},{ text: 'Сумма' }]
    	},
    	gridProperty: {
    		title: 'Основные переменные',
    		columns: [{},{ text: 'Переменная' },{ editor: { fieldLabel: 'Значение' }, text: 'Значение' }]
    	},
    	gridSchemes		: {
    		title	: 'Схемы страхования',
			columns	: [{},{ text: 'Код' },{ text: 'Название' },{ text: 'Начальная дата' },{ text: 'Конечная дата' }]
    	},
    	gridCompanyScheme	: {
    		title	: 'Схемы компании',
			columns	: [{},{ text: 'Компании' },{ text: 'Схемы' }]
    	},
    	schemeDefaults: {
			columns: [{},{ text: 'Схема' }]
    	}
    },
    monthsRenderer: function(value) {
    	if (value > 0) {
    		if (value <= .5) return '15 дней';
    		if (value === 1) return '1 месяц';
    		if (value < 5) return value + ' месяца';
    		return value + ' месяцев';
    	}
    }
});
Ext.define('CFJS.locale.ru.plugins.insurance.baggage.panel.CustomerEditCard1', {
	override: 'CFJS.plugins.insurance.baggage.panel.CustomerEditCard1',
	config	: { title: 'Багаж' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				schemeAmount: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите территорию', fieldLabel: 'Территория' },
						{ emptyText: 'выберите сумму', fieldLabel: 'Сумма' },
						{ emptyText: 'размер франшизы', fieldLabel: 'Размер франшизы' }
					]
				},
				maindata: {
					items: [
						{ emptyText: 'количество дней', fieldLabel: 'Количество дней' },
						{ emptyText: 'количество человек', fieldLabel: 'Количество человек' }
					]
				},
				totalAmount: {
					items: [
						{ emptyText: 'размер страховки', fieldLabel: 'Размер страховки' },
						{ emptyText	: 'валюта', fieldLabel	: 'Валюта' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.baggage.panel.CustomerEditCard2', {
	override: 'CFJS.plugins.insurance.baggage.panel.CustomerEditCard2',
	config	: { title: 'Багаж' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				period: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ emptyText: 'дата выезда', fieldLabel: 'Дата выезда' },
						{ emptyText: 'дата возвращения', fieldLabel: 'Дата возвращения' },
						{ emptyText: 'срок пребывания', fieldLabel: 'Срок пребывания' }
					]
				},
				client	: {
					items: [
						{ emptyText: 'выберите клиента', fieldLabel: 'Клиент' },
						{ emptyText: 'выберите документ', fieldLabel: 'Документ' }
					]
				},
				insuranceAmount: {
					items: [
						{ emptyText: 'выберите сумму', fieldLabel: 'Страховая сумма' },
						{ emptyText: 'размер франшизы', fieldLabel: 'Размер франшизы' },
						{ fieldLabel: 'Франшиза' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.baggage.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.baggage.panel.CustomerEditor',
	config	: { title: 'Багаж' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				consumerDetail: {
					items: [{ emptyText: 'выберите документ', fieldLabel: 'Документ' }]
				},
				period: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ emptyText: 'дата выезда', fieldLabel: 'Дата выезда' },
						{ emptyText: 'дата возвращения', fieldLabel: 'Дата возвращения' },
						{ emptyText: 'срок пребывания', fieldLabel: 'Срок пребывания' }
					]
				},
				programCountry: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите территорию', fieldLabel: 'Территория' },
						{ emptyText: 'выберите сумму', fieldLabel: 'Сумма' },
						{ emptyText: 'размер франшизы', fieldLabel: 'Размер франшизы' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.casualty.panel.CustomerEditCardAcc1', {
	override: 'CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc1',
	config	: { title: 'Калькулятор страхования от несчастных случаев' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				schemeAmount: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите территорию', fieldLabel: 'Территория' },
						{ emptyText: 'выберите сумму', fieldLabel: 'Сумма' }
					]
				},
				maindata: {
					items: [
						{ emptyText: 'количество дней', fieldLabel: 'Количество дней' },
						{ emptyText: 'количество человек', fieldLabel: 'Количество человек' },
						{ emptyText: 'средний возраст', fieldLabel: 'Средний возраст' }
					]
				},
				conditions: { fieldLabel: 'Дополнительные условия' },
				totalAmount: {
					items: [{ emptyText: 'размер страховки', fieldLabel: 'Размер страховки' },{ emptyText: 'валюта', fieldLabel: 'Валюта' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.casualty.panel.CustomerEditCardAcc2', {
	override: 'CFJS.plugins.insurance.casualty.panel.CustomerEditCardAcc2',
	config	: { title: 'Несчастный случай' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				period: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ emptyText: 'дата выезда', fieldLabel: 'Дата выезда' },
						{ emptyText: 'дата возвращения', fieldLabel: 'Дата возвращения' },
						{ emptyText: 'срок пребывания', fieldLabel: 'Срок пребывания' }
					]
				},
				client	: {
					items: [{ emptyText: 'выберите клиента', fieldLabel: 'Клиент' },{ emptyText: 'выберите документ', fieldLabel: 'Документ' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.casualty.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.casualty.panel.CustomerEditor',
	config	: { title: 'Несчастный случай' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				conditions: { fieldLabel: 'Дополнительные условия' },
				consumerDetail: {
					items: [{ emptyText: 'выберите документ', fieldLabel: 'Документ' }]
				},
				period: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ emptyText: 'дата выезда', fieldLabel: 'Дата выезда' },
						{ emptyText: 'дата возвращения', fieldLabel: 'Дата возвращения' },
						{ emptyText: 'срок пребывания', fieldLabel: 'Срок пребывания' }
					]
				},
				programCountry: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите территорию', fieldLabel: 'Территория' },
						{ emptyText: 'выберите сумму', fieldLabel: 'Сумма' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.health.panel.CustomerEditCardMed1', {
	override: 'CFJS.plugins.insurance.health.panel.CustomerEditCardMed1',
	config	: { title: 'Калькулятор медицинского страхования' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				programCountry: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите территорию', fieldLabel: 'Территория' },
						{ emptyText: 'выберите страну', fieldLabel: 'Страна' },
						{ emptyText: 'выберите сумму', fieldLabel: 'Сумма' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]
				},
				maindata: {
					items: [   
						{ emptyText: 'количество дней', fieldLabel: 'Количество дней' },
						{ emptyText: 'срок действия', fieldLabel: 'Срок действия' },
						{ emptyText: 'количество человек', fieldLabel: 'Количество человек' },
						{ emptyText: 'средний возраст', fieldLabel: 'Средний возраст' },
						{ text: 'Мультвиза', tooltip: 'Мультвиза' }
					]
				},
				conditions: { fieldLabel: 'Дополнительные условия' },
				totalAmount: {
					items: [{ emptyText: 'размер страховки', fieldLabel: 'Размер страховки' },{ emptyText: 'валюта', fieldLabel	: 'Валюта' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.health.panel.CustomerEditCardMed2', {
	override: 'CFJS.plugins.insurance.health.panel.CustomerEditCardMed2',
	config	: { title: 'Медицина' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				period: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ emptyText: 'дата выезда', fieldLabel: 'Дата выезда' },
						{ emptyText: 'дата возвращения', fieldLabel: 'Дата возвращения' },
						{ emptyText: 'срок пребывания', fieldLabel	: 'Срок пребывания' },
						{ emptyText	: 'период действия', fieldLabel	: 'Период действия' },
						{ text: 'Мультивиза', tooltip: 'Мультивиза' }
					]
				},
				client: {
					items: [{ emptyText: 'выберите клиента', fieldLabel	: 'Клиент' },{ emptyText: 'выберите документ', fieldLabel: 'Документ' }]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.health.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.health.panel.CustomerEditor',
	config	: { title: 'Медицина' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				conditions		: { fieldLabel: 'Дополнительные условия' },
				consumerDetail	: { items: [{ emptyText: 'выберите документ', fieldLabel: 'Документ' }] },
				period			: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ emptyText: 'дата выезда', fieldLabel: 'Дата выезда' },
						{ emptyText: 'дата возвращения', fieldLabel: 'Дата возвращения' },
						{ emptyText: 'срок пребывания', fieldLabel: 'Срок пребывания' },
						{ emptyText: 'срок действия', fieldLabel: 'Срок действия' },
						{ text: 'Мультвиза', tooltip: 'Мультвиза' }
					]
				},
				programCountry	: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите территорию', fieldLabel: 'Территория' },
						{ emptyText: 'выберите страну', fieldLabel: 'Страна' },
						{ emptyText: 'выберите сумму', fieldLabel: 'Страховое покрытие' },
						{ emptyText: 'валюта', fieldLabel: 'Валюта' }
					]	
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.civilliability.panel.CustomerEditor', {
	override: 'CFJS.plugins.insurance.civilliability.panel.CustomerEditor',
	config	: { title: 'Физ.лицо' },
	initComponent: function() {
		var me = CFJS.merge(this, {
			sections: {
				consumerDetail	: {
					items: [
						{ emptyText: 'выберите схему', fieldLabel: 'Схема страхования' },
						{ emptyText: 'выберите документ', fieldLabel: 'Документ' },
						{ fieldLabel: 'Льготный' }
					]
				},
				computing	: {
					items: [{ fieldLabel: 'Базовый тариф' },{ fieldLabel: 'K1' },{ fieldLabel: 'K2' },{ fieldLabel: 'K3' },{ fieldLabel: 'K4' },{ fieldLabel: 'K5' },
							{ fieldLabel: 'K6' },{ fieldLabel: 'K7' },{ fieldLabel: 'K8' },{ fieldLabel: 'K9' },{ fieldLabel: 'K10' },{ fieldLabel: 'Сумма' }]
				},
				insuranceAmount: {
					items: [
						{ fieldLabel: 'Страховая сумма (имущество)' },
						{ fieldLabel: 'Страховая сумма (здоровье)' },
						{ emptyText: 'выберите размер франшизы', fieldLabel: 'Размер франшизы' },
						{ emptyText: 'выберите валюту', fieldLabel: 'Валюта' },
						{ emptyText: 'выберите бонус-малус', fieldLabel: 'Бонус-Малус' }
					]
				},
				insuranceObject: {
					items: [{
						items: [
							{ emptyText: 'XXXXXXXXXXXXXXXXX', fieldLabel: 'Номер кузова (VIN)' },
							{ items: [{ fieldLabel: 'Тип ТТ' },{},{ fieldLabel: 'Код' }] },
							{ emptyText: 'выберите марку автомобиля', fieldLabel: 'Марка' },
							{ emptyText: 'выберите модель автомобиля', fieldLabel: 'Модель' }
						]
					},{
						items: [
							{ emptyText: 'выберите год выпуска', fieldLabel: 'Год выпуска' },
							{ emptyText: 'выберите страну', fieldLabel: 'Страна регистрации' },
							{ emptyText: 'выберите город', fieldLabel: 'Город регистрации' },
							{ fieldLabel: 'МРЭО' },
							{ fieldLabel: 'Зона' },
							{ fieldLabel: 'Номерной знак' }
						]
					}]
				},
				period: {
					items: [
						{ emptyText: 'дата договора', fieldLabel: 'Дата договора' },
						{ fieldLabel: 'Стикер', items: [{ emptyText: 'XX' },{ emptyText: 'XXXXXXXXXX' }] },
						{ emptyText	: 'начало действия', fieldLabel	: 'Начало действия' },
						{ emptyText	: 'окончание действия', fieldLabel	: 'Окончание действия' }
					]
				},
				useProperties: {
					items: [
						{ fieldLabel: 'Дата следующего ОТК' },
						{ fieldLabel: 'Количество ТС' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Автомобиль используется в такси">*</span>'], fieldLabel: 'Такси' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="К вождению допускаются особы с водительским стажем меньше 3-х лет">*</span>'], fieldLabel: 'Используют новички' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Страхователь был замечен в мошенничестве">*</span>'], fieldLabel: 'Мошенничество' },
						{ afterLabelTextTpl: ['<span style="color:red;font-weight:bold" data-qtip="Для годовых договоров">*</span>'], fieldLabel: 'Неиспользуемые месяцы' }
					]
				}
			}
		});
		me.callParent();
	}
});
Ext.define('CFJS.locale.ru.plugins.insurance.mixin.DescriptionValue', {
	override: 'CFJS.plugins.insurance.mixin.DescriptionValue',
	attentionMess			: 'Внимание: ',
	noDictDataMess			: 'Отсутствуют справочные данные ',
	durationTripMess		: 'по времени пребывания.',
	noSchemeAmountMess		: 'по схеме страхования и сумме покрытия.',
	maxPeriodMess			: 'Превышен максимальный период страхования!',
	missingDateBirthMess	: 'Отсутствует дата рождения.',
	missingAgeRangesMess	: 'по диапазонам возрастов.',
	missDictAgeRangesMess	: 'Отсутствуют справочные данные по диапазонам возрастов.',
	forSchemeMess			: 'Для схемы:',
	numberOfDaysMess		: ', количества дней: ',
	amountCoverageMess		: ', суммы покрытия: ',
	ageMess					: ', возраста: ',
	tariffMess				: ', тариф = ',
	koeffAgeMess			: ', коэффициент по возрасту = ',
	amountAgeMess			: ' сумма по возрасту = ',
	discountForNumberMess	: 'Скидка за количество застрахованных - ',
	conditionsRateMess		: 'Коэффициент за дополнительные условия = ',
	insuredMess				: '-й застрахованный.\n',
	amountMess				: 'Сумма = ',
	totalAmountMess			: 'Общая сумма = '
});
