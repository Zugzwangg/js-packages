Ext.define('CFJS.locale.ru.plugins.panel.SettingsPanel', {
    override: 'CFJS.plugins.panel.SettingsPanel',
	config	: {
		actions	: {
			addRecord	: { title: 'Добавить запись', tooltip: 'Добавить новую запись' },
    		copyRecord	: { title: 'Скопировать запись', tooltip: 'Скопировать текущую запись' },
			reloadStore	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeRecord: { title: 'Удалить записи', tooltip: 'Удалить выделенные записи' },
			saveRecord	: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' }
		},
		title	: 'Налаштування'
	},
    monthsRenderer: function(value) {
    	if (value > 0) {
    		if (value === 1) return '1 місяць';
    		if (value < 5) return value + ' місяці';
    		return value + ' місяців';
    	}
    }
});
