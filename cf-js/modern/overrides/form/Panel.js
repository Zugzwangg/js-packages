Ext.define('CFJS.overrides.form.Panel', {
	override	: 'Ext.form.Panel',
	readOnly	: false,
	
	setReadOnly: function(readOnly) {
		var me = this, 
			fields = me.getFields().items,
			f, field;
		if (me.readOnly !== readOnly) {
			for (f = 0; f < fields.length; f++) {
				fields[f].setReadOnly(readOnly);
			}
			me.readOnly = readOnly;
		}
	}

});