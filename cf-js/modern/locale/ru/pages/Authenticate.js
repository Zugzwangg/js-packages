Ext.define('CFJS.locale.ru.pages.Authenticate', {
	override			: 'CFJS.pages.Authenticate',
	forgotPasswordText	: 'Забыли пароль?',
	loginText			: 'Войти',
	passwordText		: 'пароль',
	rememberMeText		: 'Запомнить меня',
	serviceBlankText	: 'Вы должны выбрать сервис',
	serviceText			: 'укажите сервис',
	titleText			: 'Войдите в свой аккаунт',
	usernameText		: 'идентификатор пользователя'
});
