Ext.define('CFJS.locale.ukr.pages.Authenticate', {
	override			: 'CFJS.pages.Authenticate',
	forgotPasswordText	: 'Забули пароль?',
	loginText			: 'Увійти',
	passwordText		: 'пароль',
	rememberMeText		: 'Запам\'ятати мене',
	serviceBlankText	: 'Ви повинні обрати сервіс',
	serviceText			: 'оберіть сервіс',
	titleText			: 'Увійдіть до свого облікового запису',
	usernameText		: 'ідентифікатор користувача'
});
