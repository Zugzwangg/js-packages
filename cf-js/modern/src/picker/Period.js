Ext.define('CFJS.picker.Period', {
	extend	: 'Ext.picker.Picker',
	requires			: [
		'Ext.Date',
		'Ext.picker.Date'
	],
	xtype				: 'periodpicker',
	alternateClassName	: 'CFJS.PeriodPicker',
	isPeriodPicker		: true,
	statics				: {
		DAY		: 0,
		WEEK	: 1,
		MONTH	: 2,
		QUARTER	: 3,
		YEAR	: 4,
		DATES	: 5
	},
	config				: { value: new Date(), startDate: new Date(), endDate: new Date(), pickerType : 0 },
	datesText			: 'Arbitrary',
	dayText				: 'Day',
	weekText			: 'Week',
	monthText			: 'Month',
	quarterText			: 'Quarter',
	yearText			: 'Year',
	okButtonText		: 'OK',
	cancelButtonText	: 'Cancel',
	iconCls				: CFJS.UI.iconCls.CALENDAR,
	plain				: true,
	selectors			: [ 'DATES', 'DAY', 'MONTH', 'QUARTER', 'YEAR' ],
	showButtons			: true,
	title				: 'Working period'
	
});