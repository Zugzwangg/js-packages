Ext.define('CFJS.container.Viewport', {
	extend				: 'Ext.Container',
	alternateClassName	: 'CFJS.Viewport',
	xtype				: 'mainviewport',
	layout				: 'hbox'
});
