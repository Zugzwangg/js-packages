/**
 * This is the base class for all Authentication related Form dialogs. It optionally
 * enables autoComplete support to any child textfield so that browsers or their plugins
 * may restore/persist username, password and other attributes to/from such forms.
 */
Ext.define('CFJS.panel.Authenticate', {
	extend		: 'Ext.form.Panel',
	xtype		: 'login-panel',
	requires	: [ 'Ext.layout.VBox', 'CFJS.view.AuthenticationModel' ],
	/**
	 * @cfg {Boolean} [autoComplete=false]
	 * Enables browser (or Password Managers) support for autoCompletion of User Id and
	 * password.
	 */
	autoComplete: false,
    baseCls		: 'auth-locked',
	defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([value])',
    layout		: { type: 'vbox', align: 'center', pack: 'center' },
	viewModel	: { type: 'authentication' },

	initialize: function () {
		var me = this, listen;
		if (me.autoComplete) {
			// Use standard FORM tag for detection by browser or password tools
			me.autoEl = Ext.applyIf(me.autoEl || {}, {
				tag		: 'form',
				name	: 'login-panel',
				method	: 'post'
			});
		}
		me.callParent();
		if (me.autoComplete) {
			listen = {
				afterrender	: 'doAutoComplete',
				scope		: me,
				single		: true
			};
			Ext.each(me.query('textfield'), function (field) { field.on(listen); });
		}
	},
	
	doAutoComplete : function(target) {
		if (target.inputEl && target.autoComplete !== false) {
			target.inputEl.set({ autocomplete: 'on' });
		}
	}
});
