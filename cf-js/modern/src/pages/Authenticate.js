Ext.define('CFJS.pages.Authenticate', {
	extend				: 'CFJS.panel.Authenticate',
	alternateClassName	: 'CFJS.LoginView',
	xtype				: 'login-view',
	requires			: [
		'Ext.field.Checkbox',
		'Ext.field.Password',
		'Ext.field.Text',
		'Ext.layout.HBox',
		'CFJS.view.AuthenticationController'
	],
	controller			: 'authentication',
	forgotPasswordText	: 'Forgot password?',
	loginText			: 'Login',
	passwordText		: 'password',
	rememberMeText		: 'Remember me',
	serviceBlankText	: 'You must select the service',
	serviceText			: 'select service',
	titleText			: 'Sign in to your account',
	usernameText		: 'user ID',
	
	initialize: function() {
		var me = this;
		me.add({
			xtype		: 'formpanel',
			items		: [{
				padding	: '15 0 0 15',
				html	:  me.titleText
			},{
				xtype	: 'container',
				defaults: { margin:'0 0 5 0' },
				layout	: 'vbox',
				padding	: 15,
				items	: [{
					xtype			: 'textfield',
					autoCapitalize	: false,
					bind			: '{username}',
					placeHolder		: me.usernameText,
					minLenght		: 4,
					name			: 'user',
					userCls			: 'text-border'
				},{
					xtype		: 'passwordfield',
					bind		: '{password}',
					placeHolder	: me.passwordText,
					minLenght	: 6,
					name		: 'pwd',
					revealable	: true,
					userCls		: 'text-border'
				},{
					layout: 'hbox',
					items: [{
						xtype	: 'checkboxfield',
						bind	: '{persist}'
					},{
						html	: me.rememberMeText,
						userCls	: 'checkbox-text-adjustment',
						style	: 'marginRight:10px'
					},{
						html	: '<a href="#passwordreset">' + me.forgotPasswordText + '</a>',
						userCls	: 'checkbox-text-adjustment'
					}]
				},{
					xtype		: 'button',
					formBind	: true,
					iconAlign	: 'right',
					iconCls		: CFJS.UI.iconCls.ANGLE_RIGHT,
					text		: me.loginText,
					ui			: 'action',
					handler		: 'onLoginClick'
//				},{
//					xtype: 'button',
//					text: 'Create Account',
//					ui: 'gray-button',
//					iconAlign: 'right',
//					iconCls: CFJS.UI.iconCls.USER_ADD,
//					handler: function(){
//						window.location.href= "#register";
//					}
				}]
			}],
			isValid: function() {
				var user = this.down('textfield[name="user"]'),
					username = user.getValue(),
					pwd = this.down('passwordfield[name="pwd"]'),
					password = pwd.getValue();
				return username && username.length >= user.minLenght && password && password.length >= pwd.minLenght;
			}
		})
		me.callParent();
	}

});
