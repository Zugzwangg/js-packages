Ext.define('CFJS.pages.Blank', {
	extend				: 'Ext.Container',
	alternateClassName	: 'CFJS.BlankPage',
	xtype				: 'blank-page',
	bodyText			: 'Stay tuned for updates',
	containerCls		: 'blank-page-container',
	headerText			: 'Coming Soon!',
	layout				: 'fit',
	items				: [{
		layout	: { type: 'vbox', pack: 'center', align: 'center' },
		items	: { name: 'info'} 
	}],
	bodyHtml			: function() {
		return [
			'<div class="fa-outer-class"><span class="' + CFJS.UI.iconCls.CLOCK + '"></span></div>',
		   	'<h1>', this.headerText, '</h1>',
		   	'<span class="blank-page-text">', this.bodyText, '</span>'
		].join('');
	},
	initialize: function() {
		var me = this, html = me.bodyHtml, container = me.down('[name="info"]');
		if (container) {
			if (Ext.isFunction(html)) html = Ext.callback(html, me);
			container.setCls(me.containerCls);
			container.setHtml(html || null);
		}
		me.callParent(arguments);
	}
});