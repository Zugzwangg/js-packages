Ext.define('CFJS.pages.Unsupported', {
	extend				: 'CFJS.pages.Blank',
	alternateClassName	: 'CFJS.Unsupported',
	xtype				: 'unsupportedpage',
	bodyText			: 'This program is not designed for use on the phone.',	
//	bodyText			: 'Эта программа не расчитана на использование в телефоне.',
	containerCls		: 'unsupported-page-container',
	fullscreen			: true,
	bodyHtml			: function() {
		return [
			'<div class="fa-outer-class"><span class="' + CFJS.UI.iconCls.EXCLAMATION_TRIANGLE + '"></span></div>',
		   	'<span class="unsupported-page-text">', this.bodyText, '</span>'
		].join('');
	}

});