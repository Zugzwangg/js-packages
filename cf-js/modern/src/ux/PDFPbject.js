Ext.define('CFJS.ux.PDFObject', {
	extend		: 'Ext.Component',
	alias		: ['widget.uxpdf', 'widget.pdfcomponent'],
	baseCls		: Ext.baseCSSPrefix + 'pdf',
	childEls	: [ 'pdfEl' ],
	config		: {
		embed	: true,
		params	: null
	},
	fallbackText: 'This browser does not support inline PDFs. Please download the PDF to view it: ',
	tpl			: [
		'<tpl if="notSupport">',
			'<p>{fallbackText}<a href="{src}">{pdfName}</a></p>',
		'<tpl elseif="embed">',
			'<object data="{src}" id="{id}-pdfEl" data-ref="pdfEl" name="{pdfName}" type="application/pdf" style="border:none;width:100%;height:100%;overflow:auto;">',
				'<embed src="{src}" id="{id}-pdfEl" data-ref="pdfEl" name="{pdfName}" type="application/pdf" style="border:none;width:100%;height:100%;overflow:auto;">',
					'<p>{fallbackText}<a href="{src}">{pdfName}</a></p>',
				'</embed>',
			'</object>',
		'<tpl else>',
			'<iframe src="{src}" id="{id}-pdfEl" data-ref="pdfEl" name="{pdfName}" style="border:none;width:100%;height:100%;" frameborder="0"></iframe>',
		'</tpl>'
	],
	src			: 'about:blank',
	
	applyEmbed: function(embed) {
		return Ext.isBoolean(embed) && embed;
	},
	
	applyParams: function(params) {
		if (Ext.isObject(params)) params = Ext.Object.toQueryString(params);
		if (Ext.isString(params)) {
			if (!params.startsWith('#')) params = '#' + params;
		} else params = '';
		return params || '';
	},
	
	initialize: function () {
		var me = this,
			hasMimeType = Ext.isDefined(navigator.mimeTypes['application/pdf']),
			activeX = function (type) {
				try { return new ActiveXObject(type); } catch (e) {}
			};
		me.supportsPDFs = hasMimeType || (Ext.isIE && !!(activeX("AcroPDF.PDF") || activeX("PDF.PdfCtrl")));
        me.callParent();
        me.pdfName = me.pdfName || me.id + (me.embed ? '-object' : '-frame');
        me.setData({
        	notSupport	: !me.supportsPDFs,
        	embed		: me.embed,
        	src			: [me.src, me.params].join(''),
        	fallbackText: me.fallbackText,
            pdfName		: me.pdfName
        })
    }
});
