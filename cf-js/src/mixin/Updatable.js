Ext.define('CFJS.mixin.Updatable', {
	extend		: 'Ext.Mixin',
	mixinConfig	: {
		id		: 'updatable',
		after	: { initConfig: 'afterInitConfig' }
	},
	descriptors	: { $value: null, lazy: true },
	
	initDescriptors: function() {
		var descriptors = this.descriptors;
		if (descriptors && !Ext.isObject(descriptors)) {
			var i = 0, ret = {},
				convert = function(descriptor) {
					ret[descriptor] = '{' + descriptor + '}';
				};
			if (Ext.isString(descriptors)) convert(descriptors);
			else if (Ext.isArray(descriptors)) {
				for (; i < descriptors.length; i++) {
					convert(descriptors[i]);
				}
			}
			descriptors = ret;
		}
		return descriptors;
	},
	
	afterInitConfig: function() {
		var me = this, descriptors = me.initDescriptors(), key, descriptor, fn;
		for (key in descriptors) {
			if ((descriptor = descriptors[key]) && Ext.isFunction(fn = me['init' + key.capitalize() + 'Store'])) {
				me.bind(descriptor, fn, me, { single: true });
			}
		}
	}

});