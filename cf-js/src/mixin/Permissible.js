Ext.define('CFJS.mixin.Permissible', {
	extend			: 'Ext.Mixin',
	mixinConfig		: { id: 'permissible' },
	permissions		: { $value: null, lazy: true },
	isPermissible	: true,
	
	applyPermissions: function(permissions) {
		if (permissions && !Ext.isEmpty(permissions.bindTo)) {
			var me = this, callback = permissions.callback;
			permissions.scope = permissions.scope || me;
			permissions.options = permissions.options || { deep: true };
			if (Ext.isString(callback) && !Ext.isEmpty(callback)) callback = permissions.scope[callback];
			permissions.callback = Ext.isFunction(callback) ? callback : me.onPermissionsChange;
		}
		return permissions;
	},

	onPermissionsChange: Ext.emptyFn,
	
	updatePermissions: function(permissions, old) {
		var me = this,
			vm = me.lookupViewModel(),
			binding = (old || {}).binding;
			bindTo = permissions ? permissions.bindTo : null;
		if (binding) binding.destory();
		if (vm && !Ext.isEmpty(bindTo))
			permissions.binding = vm.bind(bindTo, permissions.callback, permissions.scope, permissions.options);
	}

});