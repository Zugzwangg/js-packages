Ext.define('CFJS.mixin.PeriodPicker', {
	extend			: 'Ext.Mixin',
	requires		: [ 'CFJS.picker.Period' ],
	mixinConfig		: { id: 'periodpicker' },
	hasPeriodPicker	: true,
	
	createPeriodPicker: function(action) {
		var me = this, picker = me.periodPicker;
		if (!picker) {
			if ((action=action||{}).initialConfig) action = action.initialConfig;
			me.periodPicker = picker = new CFJS.picker.Period({
				closeAction	: 'hide',
				closable	: false,
				iconCls		: (action||{}).iconCls,
				title		: (action||{}).tooltip,
				hidden		: true,
				hideOnSelect: true,
				showButtons	: false,
				listeners	: {
					focusleave	: function() {
						if (!me.destroyed && !me.destroying && picker) picker.hide();
					},
					select		: function(picker, value) {
						var vm = me.lookupViewModel();
						if (vm && vm.isBaseModel) vm.setWorkingPeriod(value);
					}
				}
            });
		}
		return picker;
	},
	
	showPeriodPicker: function(action) {
		var me = this, vm = me.lookupViewModel(), picker, actions;
		if (me.rendered && !me.disabled) {
			picker = me.createPeriodPicker(action || (actions = me.getActions() ? actions.periodPicker : null));
			picker.setValue((vm && vm.isBaseModel ? vm.getWorkingPeriod() : null) || new Date());
			picker.show();
		}
		return me;
	}

});