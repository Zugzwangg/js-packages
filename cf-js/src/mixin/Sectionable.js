Ext.define('CFJS.mixin.Sectionable', {
	extend			: 'Ext.Mixin',
	mixinConfig		: {
		id		: 'sectionable',
		after	: {
			initComponent	: 'initComponent'
		}
	},
	sections		: { $value: undefined, lazy: true },
	isSectionable	: true,

	initComponent: function() {
		if (!this.isContainer) return;
		var me = this, sections = me.sections,
			parse = CFJS.safeParseInt,
			i = 0, section, items = [];
		if (Ext.isObject(sections)) {
			for (section in sections) {
				section = sections[section];
				if (section) items.push(Ext.applyIf(section, { order: i++ }));
			}
		}
		Ext.suspendLayouts();
		me.removeAll();
		if (items.length > 0) {
			items.sort(function(a, b) {
	    		return parse(a && a.order || a) - parse(b && b.order || b);
	    	});
			me.add(items);
		}
		Ext.resumeLayouts();
	},
	
	findConfig: function(section, options) {
		return this.findElementConfig(Ext.isString(section) ? (this.sections||{})[section] : section, options || {}, true);
	},
	
	pushItems: function(section, options) {
		var cfg = this.findConfig(section, options);
		if (cfg && Ext.isArray(cfg.items)) cfg.items = cfg.items.concat(Array.prototype.slice.call(arguments, 2));
	},
	
	privates: {
		
		findElementConfig: function(obj, opt, recursive) {
			var fn = this.findElementConfig, key, result;
			if (Ext.isObject(obj)) {
				result = true;
				for (key in opt) {
					result = result && obj[key] === opt[key];
				}
				if (!(result = result && obj) && recursive) {
					for (key in obj) {
						if (result = obj.hasOwnProperty(key) && fn.call(this, obj[key], opt, recursive)) break;
					}
				}
			} else if (Ext.isArray(obj)) {
				for (key = 0; key < obj.length && !result; key++) {
					result = fn.call(this, obj[key], opt, recursive);
				}
			}
			return result;
		}
	
	}
	
});