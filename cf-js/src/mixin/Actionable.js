Ext.define('CFJS.mixin.Actionable', {
	extend		: 'Ext.Mixin',
	mixinConfig	: {
		id		: 'actionable',
		after	: { initConfig: 'afterInitConfig' },
		before	: { afterRender: 'afterRender' }
	},
	actions			: { $value: null, lazy: true },
	actionsDisable	: { $value: null, lazy: true },
	actionsHandler	: 'onActionClick',
	actionsMap		: { $value: null, lazy: true },
	actionsScope	: null,
	contextMenu		: true,
	hasActions		: true,

	addAction: function(action) {
		if (action && action.itemId) {
			var me = this;
			me.actions = me.actions || {};
			action = me.actions[action.itemId] = me.createAction(action);
		}
		return action;
	},
	
    addActions: function(object, map) {
    	switch (map) {
			case 'tbfill':
			case 'tbspacer':
			case 'tbseparator':
			case 'menuseparator': 
				return { xtype: map };
			default: break;
		}
    	if (object !== false) {
        	var me = this, action = me.findAction(map);
        	if (action) {
        		if (object && object.constructor === Object) {
            		Ext.applyIf(action.initialConfig, object);
        		}
        		return action;
        	} else if (map) {
        		if (Ext.isObject(map)) {
        			object = object || {};
        			for (var key in map) {
        				object[key] = me.addActions(object[key], map[key]);
        			}
        		} else if (Ext.isArray(map) && Ext.isArray(object)) {
        			for (var i = 0; i < map.length && i < object.length; i++) {
        				object[i] = me.addActions(object[i], map[i]);
        			}
        		}
        	}
    	}
    	return object;
    },

    afterInitConfig: function(config) {
		var me = this, contextMenu = me.contextMenu;
		if (contextMenu && !contextMenu.isMenu) {
			contextMenu = Ext.apply({ xtype: 'menu', floatParent: this }, contextMenu);
		}
		if (me.actionsMap) me.addActions(me, me.actionsMap);
		if (contextMenu && contextMenu.items && contextMenu.items.length > 0) {
			me.contextMenu = Ext.create(contextMenu);
		} else me.contextMenu = false;
    },
    
	afterRender: function() {
		var me = this, disable = me.actionsDisable;
		if (Ext.isArray(disable)) {
			for (var i = 0; i < disable.length; i++ ) {
				var action = Ext.isString(disable[i]) ? me.findAction(disable[i]) : null;
				if (action && action.isAction) action.disable();
			}
		}
		if (me.contextMenu) {
			me.on({ itemcontextmenu: function(view, rec, node, index, e) {
					e.stopEvent();
					me.showContextMenu(e.getXY());
					return false;
				} 
			});
		}
		return me;
	},
	
	applyActions: function(actions, currentActions) {
		if (actions !== currentActions && Ext.isObject(currentActions)) {
			for (var key in currentActions) {
				this.destroyAction(currentActions[key]);
				delete currentActions[key];
			}
		}
		return this.prepareActions(actions);
	},
	
    createAction: function(action) {
		if (action && !action.isAction) {
			if (!action.handler && action.handlerFn) action.handler = this.actionsHandler;
			action = new Ext.Action(action);
		}
		return action;
	},

	disableAction: function(key, disabled) {
		this.disableComponent(this.findAction(key), disabled);
	},
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},
	
	executeChildAction: function(handlerFn, args) {
		if (Ext.isEmpty(handlerFn)) return;
		var controller = this.lookupScope(this.getActiveChild()) || {}; 
		return (controller[handlerFn]||Ext.emptyFn).apply(controller, args);
	},
		
	findAction: function(key) {
		var actions = this.actions,
			action = actions && Ext.isString(key) ? actions[key] : null;
		if (!action && Ext.isString(key)) {
			var props = key.split('=', 2);
			if (props.length === 2) {
				for (key in actions) {
					action = actions[key];
					if (action && action[props[0]] === props[1]) {
						action = action[props[0]];
						break;
					} else action = undefined;
				}
			}
		}
		return action;
	},
	
	hideAction: function(key, hidden) {
		this.hideComponent(this.findAction(key), hidden);
	},

	hideComponent: function(component, hidden) {
		if (component && (component.isComponent || component.isAction)) component[hidden ? 'hide': 'show']();
	},

	lookupScope: function(component) {
		return component && (component.defaultListenerScope ? component : component.lookupController());
	},

	onActionClick: function(component) {
		this.executeChildAction((component||{}).handlerFn, arguments);
	},

	showContextMenu: function(xy) {
		var contextMenu = this.contextMenu,
			items = contextMenu && contextMenu.isMenu ? contextMenu.items : null;
		if (items && items.findIndex('hidden', false) !== -1) contextMenu.showAt(xy);
	},

    privates: {

    	destroyAction: function(action) {
    		if (!action) return;
			if (action.isAction) {
				while (action.items.length > 0) { 
					action.removeComponent(action.items[0]);
				}
			}
			if (action.isInstance) action.destroy();
    	},
    	
    	prepareActions: function(actions) {
    		if (actions) {
    			if (Ext.isArray(actions)) actions = Ext.Array.toValueMap(actions, 'itemId ');
    			else if (Ext.isString(actions)) actions = { itemId: actions };
    			if (Ext.isObject(actions)) {
    				var me = this, items = {}, scope = me.actionsScope || (me.defaultListenerScope ? me : me.lookupController()),
    				defaults = Ext.theme.name === 'Crisp' ? { margin: '0 2', ui: 'default-toolbar' } : {};
    				for (var key in actions) {
    					var action = actions[key];
    					if (Ext.isObject(action)) {
    						if (!action.isAction) action = this.createAction(Ext.apply({ itemId : key, scope: scope }, action));
    						items[key] = Ext.apply(action.initialConfig, defaults);
    					}
    				}
    				actions = items;
    			} else actions = undefined;
    		}
    		return actions;
    	}
    	
    }
	
});
