Ext.define('CFJS.mixin.InlineGridEditor', {
	mixinId: 'inlinegrideditor',
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},
	
	lookupGrid: function() {
		return this; 
	},

	lookupStore: function() {
		return this.lookupGridStore(this.lookupGrid());
	},
	
	lookupGridStore: function(grid) {
		return grid && Ext.isFunction(grid.getStore) ? grid.getStore() : null;
	},
	
	onAddRecord: function(component) {
		var me = this, grid = me.lookupGrid(),
			store = me.lookupGridStore(grid),
			editor, record;
		if (store && store.isStore && !grid.readOnly) {
			me.startEdit(grid, record = store.add(me.recordConfig({}))[0]);
		}
	},
	
	onCopyRecord: function(component) {
		var me = this, grid = me.lookupGrid(),
			store = me.lookupGridStore(grid),
			Model = store && store.isStore ? store.getModel() : null,
			editor, records;
		if (Model && !grid.readOnly && (records = grid.getSelection()) && records.length > 0) {
			records = Ext.clone(records[0].getData());
			delete records[Model.idProperty];
			me.startEdit(grid, records = store.add(me.recordConfig(records))[0]);
		}
	},

	onReloadStore: function(component) {
		var store = this.lookupStore();
		if (store && store.isStore) store.reload();
	},
	
	onRemoveRecords: function(component) {
		var me = this,
			grid = me.lookupGrid(),
			store = me.lookupGridStore(grid),
			i = 0, records;
		if (store && store.isStore && !grid.readOnly
				&& me.fireEvent('beforeremoverecord', records = grid.getSelection()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].erase(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},

	onSaveRecords: function(component, force) {
		var me = this, grid = me.lookupGrid(),
			store = me.lookupGridStore(grid),
			i = 0, records;
		if (store && !grid.readOnly
				&& me.fireEvent('beforesaverecord', records = store.getModifiedRecords()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].save(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},
	
	recordConfig: Ext.identityFn,
	
	startEdit: function(grid, record) {
		var editor = grid.findPlugin('rowediting');
		if (record.isModel && editor) editor.startEdit(record);
	}

});