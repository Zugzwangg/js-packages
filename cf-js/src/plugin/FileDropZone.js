Ext.define('CFJS.plugin.FileDropZone', {
	extend		: 'Ext.plugin.Abstract',
	alias		: 'plugin.filedrop',
	overCls		: 'drag-over',
	readFiles	: true,
	readType	: "DataURL",
	
	init: function(cmp) {
		var me = this;
		me.readFiles = me.readFiles && Ext.isDefined(window.FileReader);
		cmp.on("afterrender", me.initDDEvents, me);
	},
	
	initDDEvents: function(cmp) {
		var me = this,
			el = me.el || cmp.dropEl || cmp.getEl();
		el.on({
			scope		: me,
			dragover	: me.onDragOver,
			dragenter	: me.onDragEnter,
			dragleave	: me.onDragLeave,
			drop		: me.onDrop
		});
	},
	
	fireComponentEvent: function(event, args) {
		var scope = this.cmp;
		return scope.fireEvent.apply(scope, [event, scope].concat(args || []));
	},
	
	onDragOver: function(e) {
		e.stopEvent();
		this.cmp.fireEvent("dragover", this.cmp, e);
	},
	
	onDragEnter: function(e) {
		var me = this;
		me.cmp.addCls(me.overCls);
		e.stopEvent();
	},
	
	onDragLeave: function() {
		this.cmp.removeCls(this.overCls);
	},
	
	onDrop: function(e) {
		e.stopEvent();
		var me = this;
		me.fireComponentEvent('drop', e);
		if (me.readFiles) me.forEachFile(e, me.readFile);
	},
	
	forEachFile: function(e, fn, scope) {
		Ext.Array.forEach(Ext.Array.from(e.browserEvent.dataTransfer.files), fn, scope || this);
	},
	
	readFile: function(file) {
		var me = this, reader = new FileReader();
		if (!me.fireComponentEvent('beforeload', [file])) { return false; }
		reader.onabort = Ext.bind(me.handleFileEvent, me, [file], true);
		reader.onerror = Ext.bind(me.handleFileEvent, me, [file], true);
		reader.onload = Ext.bind(me.handleFileEvent, me, [file], true);
		reader.onloadend = Ext.bind(me.handleFileEvent, me, [file], true);
		reader.onloadstart = Ext.bind(me.handleFileEvent, me, [file], true);
		reader.onprogress = Ext.bind(me.handleFileEvent, me, [file], true);
		reader['readAs' + me.readType](file);
	},

	handleFileEvent: function(e, file) {
		var me = this, type = e.type, args = [e, file];
		if (type === 'abort' || type === 'error') {
			type = 'load' + type;
		}
		me.fireComponentEvent(type, args);
	}
	
});