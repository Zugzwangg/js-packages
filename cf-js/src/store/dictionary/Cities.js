Ext.define('CFJS.store.dictionary.Cities', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.cities',
	model	: 'CFJS.model.dictionary.City',
	storeId	: 'cities',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'city' },
			reader		: { rootProperty: 'response.transaction.list.city' }
		}
	}
});
