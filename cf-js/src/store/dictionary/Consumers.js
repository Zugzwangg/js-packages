Ext.define('CFJS.store.dictionary.Consumers', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.consumers',
	model	: 'CFJS.model.dictionary.Consumer',
	storeId	: 'consumers',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'consumer' },
			reader		: { rootProperty: 'response.transaction.list.consumer' }
		}
	}
});
