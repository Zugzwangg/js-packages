Ext.define('CFJS.store.dictionary.GenderTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.gendertypes',
	storeId	: 'genderTypes',
	data	: [
		{ id: 'MALE',	name: 'Male' }, 
		{ id: 'FEMALE',	name: 'Female' } 
	]
});