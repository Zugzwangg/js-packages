Ext.define('CFJS.store.dictionary.CustomerTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.customertypes',
	storeId	: 'customerTypes',
	data	: [
		{ id: 'CORPORATE',			name: 'Корпорант' }, 
		{ id: 'CUSTOMER_TYPE_1',	name: 'Тип 1' }, 
		{ id: 'CUSTOMER_TYPE_2',	name: 'Тип 2' } 
	]
});