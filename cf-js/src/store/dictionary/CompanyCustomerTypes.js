Ext.define('CFJS.store.dictionary.CompanyCustomerTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.companycustomertypes',
	storeId	: 'companyCustomerTypes',
	data	: [
		{ id: '...',	name: '...' }, 
		{ id: 'TYPE1',	name: 'Тип 1' }, 
		{ id: 'TYPE2',	name: 'Тип 2' } 
	]
});