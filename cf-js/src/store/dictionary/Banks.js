Ext.define('CFJS.store.dictionary.Banks', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.banks',
	model	: 'CFJS.model.dictionary.Bank',
	storeId	: 'banks',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'bank' },
			reader		: { rootProperty: 'response.transaction.list.bank' }
		}
	}
});
