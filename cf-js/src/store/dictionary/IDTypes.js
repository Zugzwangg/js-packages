Ext.define('CFJS.store.dictionary.IDTypes', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.idtypes',
	model	: 'CFJS.model.dictionary.IDType',
	storeId	: 'idTypes',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'id_type' },
			reader		: { rootProperty: 'response.transaction.list.id_type' }
		}
	},
	sorters	: [ 'name' ]
});

