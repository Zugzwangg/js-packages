Ext.define('CFJS.store.dictionary.SupplierTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.suppliertypes',
	storeId	: 'supplierTypes',
	data	: [
		{ id: '...',	name: '...' }, 
		{ id: 'TYPE3',	name: 'Тип 3' }, 
		{ id: 'TYPE4',	name: 'Тип 4' } 
	]
});