Ext.define('CFJS.store.dictionary.PostTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.posttypes',
	storeId	: 'postTypes',
	data	: [
		{ id: 'CHIEF_ACCOUNTANT',	name: 'Chief accountant' },
		{ id: 'DIRECTOR',			name: 'Director' },
		{ id: 'MANAGER',			name: 'Manager' },
		{ id: 'OTHER',				name: 'Other' }
	]
});