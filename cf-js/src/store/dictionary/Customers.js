Ext.define('CFJS.store.dictionary.Customers', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.customers',
	model	: 'CFJS.model.dictionary.Customer',
	storeId	: 'customers',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'customer' },
			reader: { rootProperty: 'response.transaction.list.customer' }
		}
	}
});
