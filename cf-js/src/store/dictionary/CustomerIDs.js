Ext.define('CFJS.store.dictionary.CustomerIDs', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.customerids',
	model	: 'CFJS.model.dictionary.CustomerID',
	storeId	: 'customerIDs',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'customer_id' },
			reader		: { rootProperty: 'response.transaction.list.customer_ID' }
		}
	}
});

