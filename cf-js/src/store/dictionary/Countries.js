Ext.define('CFJS.store.dictionary.Countries', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.countries',
	model	: 'CFJS.model.dictionary.Country',
	storeId	: 'countries',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'country' },
			reader		: { rootProperty: 'response.transaction.list.country' }
		}
	}
});
