Ext.define('CFJS.store.dictionary.Companies', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.companies',
	model	: 'CFJS.model.dictionary.Company',
	storeId	: 'companies',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'company' },
			reader		: { rootProperty: 'response.transaction.list.company' }
		}
	}
});
