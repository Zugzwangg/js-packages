Ext.define('CFJS.store.dictionary.CarModels', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.carmodels',
	model	: 'CFJS.model.dictionary.CarModel',
	storeId	: 'carModels',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'car_model' },
			reader		: { rootProperty: 'response.transaction.list.car_model' }
		}
	},
	sorters	: [ 'name' ]
});
