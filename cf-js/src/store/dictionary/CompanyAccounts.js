Ext.define('CFJS.store.dictionary.CompanyAccounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.companyaccounts',
	model	: 'CFJS.model.dictionary.CompanyAccount',
	storeId	: 'companyAccounts',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'company_account' },
			reader		: { rootProperty: 'response.transaction.list.company_account' }
		}
	}
});
