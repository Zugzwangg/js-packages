Ext.define('CFJS.store.dictionary.CarBrands', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.carbrands',
	model	: 'CFJS.model.dictionary.CarBrand',
	storeId	: 'carBrands',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'car_brand' },
			reader		: { rootProperty: 'response.transaction.list.car_brand' }
		}
	},
	sorters	: [ 'name' ]
});
