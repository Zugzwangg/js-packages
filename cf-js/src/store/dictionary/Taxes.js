Ext.define('CFJS.store.dictionary.Taxes', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.taxes',
	model	: 'CFJS.model.dictionary.Tax',
	storeId	: 'taxes',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'tax' },
			reader		: { rootProperty: 'response.transaction.list.tax' }
		}
	}
});
