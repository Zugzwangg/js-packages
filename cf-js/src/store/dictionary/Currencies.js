Ext.define('CFJS.store.dictionary.Currencies', {
	extend		: 'CFJS.store.Base',
	alias		: 'store.currencies',
	model		: 'CFJS.model.dictionary.Currency',
	storeId		: 'currencies',
	config		: {
		proxy: {
			loaderConfig: { dictionaryType: 'currency' },
			reader		: { rootProperty: 'response.transaction.list.currency' }
		}
	},
	pageSize	: 0,
	sorters		: [ 'code' ]
});
