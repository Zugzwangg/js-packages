Ext.define('CFJS.store.dictionary.Regions', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.regions',
	model	: 'CFJS.model.dictionary.Region',
	storeId	: 'regions',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'region' },
			reader		: { rootProperty: 'response.transaction.list.region' }
		}
	}
});
