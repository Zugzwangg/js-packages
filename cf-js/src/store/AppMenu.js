Ext.define('CFJS.store.AppMenu', {
	extend		: 'Ext.data.TreeStore',
	alias		: 'store.appmenu',
	storeId		: 'appMenu',
	clearOnLoad	: true,
	pageSize	: 0,
	fields		: [ 
		{ name: 'routeId',	type: 'string' },
		{ name: 'leaf',		type: 'boolean', defaultValue: false },
		{ name: 'secure',	type: 'boolean', defaultValue: false },
		{ name: 'text',		type: 'string' },
		{ name: 'view' }
	]
});
