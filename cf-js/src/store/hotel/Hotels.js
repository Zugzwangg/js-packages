Ext.define('CFJS.store.hotel.Hotels', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.hotels',
	model	: 'CFJS.model.hotel.Hotel',
	storeId	: 'hotels',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'hotel' },
			reader		: { rootProperty: 'response.transaction.list.hotel' }
		}
	}
});
