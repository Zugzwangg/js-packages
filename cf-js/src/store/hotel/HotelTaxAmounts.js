Ext.define('CFJS.store.hotel.HotelTaxAmounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.hotel_tax_amounts',
	model	: 'CFJS.model.financial.TaxAmount',
	storeId	: 'hotelTaxAmounts',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'hotel_tax_amount' },
			reader		: { rootProperty: 'response.transaction.list.tax_amount' }
		}
	}
});
