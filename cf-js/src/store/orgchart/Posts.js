Ext.define('CFJS.store.orgchart.Posts', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.posts',
	model	: 'CFJS.model.orgchart.Post',
	storeId	: 'posts',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'post' },
			reader		: { rootProperty: 'response.transaction.list.post' }
		}
	}
});
