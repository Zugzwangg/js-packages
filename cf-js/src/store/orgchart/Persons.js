Ext.define('CFJS.store.orgchart.Persons', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.persons',
	model	: 'CFJS.model.orgchart.Person',
	storeId	: 'persons',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'person' },
			reader		: { rootProperty: 'response.transaction.list.person' }
		}
	}
});
