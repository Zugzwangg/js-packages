Ext.define('CFJS.store.orgchart.Units', {
	extend		: 'CFJS.store.BaseTree',
	alias		: 'store.units',
	model		: 'CFJS.model.orgchart.Unit',
	parentFilter: 'parent.id',
	storeId		: 'units',
	config		: {
		proxy: {
			loaderConfig: { dictionaryType: 'unit' },
			reader		: { rootProperty: 'response.transaction.list.unit' }
		}
	},
	sorters		: [ 'name' ]
});
