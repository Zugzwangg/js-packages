Ext.define('CFJS.store.orgchart.Roles', {
	extend		: 'CFJS.store.BaseTree',
	alias		: 'store.roles',
	model		: 'CFJS.model.orgchart.Role',
	parentFilter: 'parent.id',
	storeId		: 'roles',
	config		: {
		proxy: {
			loaderConfig: { dictionaryType: 'role' },
			reader		: { rootProperty: 'response.transaction.list.role' }
		}
	},
	sorters		: [ 'name' ]
});
