Ext.define('CFJS.store.orgchart.Secretaries', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.secretaries',
	model	: 'CFJS.model.orgchart.Secretary',
	storeId	: 'secretaries',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'secretary' },
			reader		: { rootProperty: 'response.transaction.list.secretary' }
		}
	}
});
