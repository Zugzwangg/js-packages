Ext.define('CFJS.store.financial.ServiceTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.servicetypes',
	storeId	: 'serviceTypes',
	data	: [{
		id		: 'AIR',
		iconCls	: CFJS.UI.iconCls.PLANE,
		model	: 'CFJS.model.airline.AirlineTicket',
		name	: 'Авиация',
		sector	: 1
	},{
		id		: 'BUS',
		iconCls	: CFJS.UI.iconCls.BUS,
		model	: 'CFJS.model.bus.BusTicket',
		name	: 'Наземный транспорт',
		sector	: 3
	},{
		id		: 'HOTEL',
		iconCls	: CFJS.UI.iconCls.HOTEL,
		model	: 'CFJS.model.hotel.HotelService',
		name	: 'Отели',
		sector	: 4
	},{
		id		: 'INSURANCE',
		iconCls	: CFJS.UI.iconCls.UMBRELLA,
		model	: 'CFJS.model.insurance.InsuranceService',
		name	: 'Страхование',
		sector	: 5
	},{
		id		: 'OTHER',
		iconCls	: CFJS.UI.iconCls.TICKET,
		model	: 'CFJS.model.financial.Service', 
		name	: 'Другое',
		sector	: 7,
		showVat	: true
	},{
		id		: 'RAILWAY',
		iconCls	: CFJS.UI.iconCls.TRAIN,
		model	: 'CFJS.model.railway.RailwayTicket',
		name	: 'Железная дорога',
		sector	: 2,
		showVat	: true
	},{
		id		: 'TRAVEL',
		iconCls	: CFJS.UI.iconCls.GLOBE,
		model	: 'CFJS.model.travel.TravelService',
		name	: 'Туризм',
		sector	: 6
	}],
	sorters		: [ 'sector' ],

	findModel: function(id) {
		var me = this, record = me.getById(id || 'OTHER') || me.getById('OTHER');
		return record.get('model');
	}

});
