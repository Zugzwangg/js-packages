Ext.define('CFJS.store.financial.ServiceOrders', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.service_orders',
	model	: 'CFJS.model.financial.ServiceOrder',
	storeId	: 'serviceOrders',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'service_order' },
			reader		: { rootProperty: 'response.transaction.list.service_order' }
		}
	}
});
