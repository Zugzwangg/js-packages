Ext.define('CFJS.store.financial.InternalTaxes', {
	extend		: 'Ext.data.Store',
	alias		: 'store.internal_taxes',
	storeId		: 'internal_taxes',
	pageSize	: 0,
	proxy		: {
		type	: 'ajax',
		url		: 'resources/data/internal_taxes.json',
		noCache	: false
	},
	fields		: [
		{ name: 'id',		type: 'int', 		critical: true },
		{ name: 'name',		type: 'string', 	critical: true },
		{ name: 'code', 	type: 'string', 	critical: true },
		{ name: 'negative', type: 'boolean',	critical: true }
	]
});
