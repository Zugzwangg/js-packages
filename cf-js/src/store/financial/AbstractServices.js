Ext.define('CFJS.store.financial.AbstractServices', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.abstract_services',
	model	: 'CFJS.model.financial.AbstractService',
	storeId	: 'abstract_services',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'abstract_service' },
			reader		: { rootProperty: 'response.transaction.list.abstract_service' }
		}
	}
});
