Ext.define('CFJS.store.financial.Orders', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.orders',
	model	: 'CFJS.model.financial.Order',
	storeId	: 'orders',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'order' },
			reader		: { rootProperty: 'response.transaction.list.order', summaryModel: 'CFJS.model.Summary' }
		}
	}
});
