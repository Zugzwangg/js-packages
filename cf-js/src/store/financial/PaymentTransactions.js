Ext.define('CFJS.store.financial.PaymentTransactions', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.payment_transactions',
	model	: 'CFJS.model.financial.PaymentTransaction',
	storeId	: 'payment_transactions',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'payment_transaction' },
			reader		: { rootProperty: 'response.transaction.list.payment_transaction' }
		}
	}
});
