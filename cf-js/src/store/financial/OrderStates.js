Ext.define('CFJS.store.financial.OrderStates', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.orderstates',
	storeId	: 'orderStates',
	data	: [
		{ id: 'ORDER',		name: 'Предзаказ' },
		{ id: 'BOOKING',	name: 'Бронь' },
		{ id: 'TICKETING',	name: 'Выпущен билет' },
		{ id: 'PAID',		name: 'Оплачен' },
		{ id: 'SALE',		name: 'Продан' },
		{ id: 'EXCHANGE',	name: 'Обмен' },
		{ id: 'REFUND',		name: 'Возврат' },
		{ id: 'VOID',		name: 'Отменен' }
	]
});