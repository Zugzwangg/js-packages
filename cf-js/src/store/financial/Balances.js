Ext.define('CFJS.store.financial.Balances', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.balances',
	model	: 'CFJS.model.financial.Balance',
	storeId	: 'balances',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'balance' },
			reader		: { rootProperty: 'response.transaction.list.balance_data' }
		}
	}
});
