Ext.define('CFJS.store.financial.PayingTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.payingtypes',
	storeId	: 'payingTypes',
	data	: [
		{ id: 'BANK', 			name: 'Bank' },
		{ id: 'CASH',			name: 'Cash' },
		{ id: 'CHEQUE',			name: 'Bank check' },
		{ id: 'CARD',			name: 'Bank card' },
		{ id: 'WEB_MONEY',		name: 'Internet payments' },
		{ id: 'NATIONAL_CARD',	name: 'National card' }
	]
});