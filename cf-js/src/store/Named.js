Ext.define('CFJS.store.Named', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.named',
	model	: 'CFJS.model.NamedModel',
	storeId	: 'named',
	proxy	: {
		type	: 'json.cfapi',
		api		: { read: { method: 'dictionary', transaction: 'named_dictionary_list' } },
		reader	: { rootProperty: 'response.transaction.list.named_entity' }
	}
});
