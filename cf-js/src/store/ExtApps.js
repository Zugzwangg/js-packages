Ext.define('CFJS.store.ExtApps', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.extapps',
	model	: 'CFJS.model.ExtApp',
	storeId	: 'extApps',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'external_application' },
			reader		: { rootProperty: 'response.transaction.list.external_application' }
		}
	}
});
