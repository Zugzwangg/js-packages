Ext.define('CFJS.store.Files', {
	extend				: 'Ext.data.Store',
	requires			: [ 'CFJS.api.JsonProxy' ],
	alias				: 'store.files',
	remote				: true,
	model				: 'CFJS.model.FileModel',
	storeId				: 'files',
	proxy				: {
		type			: 'json.cfapi',
		actionMethods	: { read: 'GET' },
		api				: {	read: 'list_dir' },
		reader			: { 
			type: 'json.cfapi', 
			rootProperty: 'files.file'
		},
		service			: 'resources'
    }

});
