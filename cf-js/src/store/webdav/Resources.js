Ext.define('CFJS.store.webdav.Resources', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.resources',
	model	: 'CFJS.model.webdav.Resource',
	storeId	: 'resources',
	config	: {
		proxy: {
			api			: { create	: { method: 'mkdir', transaction: 'new' } },
			loaderConfig: { dictionaryType: 'resource' },
			reader		: { rootProperty: 'response.transaction.list.resource' },
			service		: 'webdav'
		}
	}
});
