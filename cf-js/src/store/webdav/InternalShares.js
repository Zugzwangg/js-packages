Ext.define('CFJS.store.webdav.InternalShares', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.internal_shares',
	model	: 'CFJS.model.webdav.InternalShare',
	storeId	: 'internalShares',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'internal_share' },
			reader		: { rootProperty: 'response.transaction.list.internal_share' },
			service		: 'webdav'
		}
	}
});
