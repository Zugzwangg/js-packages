Ext.define('CFJS.store.webdav.ResourceVersions', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.resource_versions',
	model	: 'CFJS.model.webdav.ResourceVersion',
	storeId	: 'resourceVersions',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'resource_version' },
			reader		: { rootProperty: 'response.transaction.list.resource_version' },
			service		: 'webdav'
		}
	}
});
