Ext.define('CFJS.store.communion.LocalTaskStatuses', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.local_task_statuses',
	model	: 'CFJS.model.communion.LocalTaskStatus',
	storeId	: 'localTaskStatuses',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'local_task_status' },
			reader		: { rootProperty: 'response.transaction.list.local_task_status' }
		}
	}
});
