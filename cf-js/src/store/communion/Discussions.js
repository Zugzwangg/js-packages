Ext.define('CFJS.store.communion.Discussions', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.discussions',
	model	: 'CFJS.model.communion.Discussion',
	storeId	: 'discussions',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'discussion' },
			reader		: { rootProperty: 'response.transaction.list.discussion' }
		}
	}
});
