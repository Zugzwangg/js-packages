Ext.define('CFJS.store.communion.LocalTasks', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.local_tasks',
	model	: 'CFJS.model.communion.LocalTask',
	storeId	: 'localTasks',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'local_task' },
			reader		: { rootProperty: 'response.transaction.list.local_task' }
		}
	}
});
