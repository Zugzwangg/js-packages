Ext.define('CFJS.store.communion.DiscussionMessages', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.discussion_messages',
	model	: 'CFJS.model.communion.DiscussionMessage',
	storeId	: 'discussionMessages',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'discussion_message' },
			reader		: { rootProperty: 'response.transaction.list.discussion_message' }
		}
	}
});
