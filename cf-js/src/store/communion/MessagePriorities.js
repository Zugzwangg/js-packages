Ext.define('CFJS.store.communion.MessagePriorities', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.messagepriorities',
	storeId	: 'messagePriorities',
	data	: [
		{ id: 'VERY_HIGH',	name: 'Very high' },
		{ id: 'HIGH',		name: 'High' },
		{ id: 'NORMAL',		name: 'Normal' },
		{ id: 'LOW',		name: 'Low' },
		{ id: 'VERY_LOW',	name: 'Very low' }
	],
	sorters	: null
});