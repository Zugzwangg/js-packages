Ext.define('CFJS.store.cellstore.CellSizes', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.cell_sizes',
	model	: 'CFJS.model.cellstore.CellSize',
	storeId	: 'cell_sizes',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_size' },
			reader		: { rootProperty: 'response.transaction.list.cell_size' }
		},
		sorters	: [ 'height', 'width', 'depth', 'micro' ]
	}
});
