Ext.define('CFJS.store.cellstore.AuthorizedPersons', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.cell_authorized_persons',
	model	: 'CFJS.model.cellstore.AuthorizedPerson',
	storeId	: 'cell_authorized_persons',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_authorized_person' },
			reader		: { rootProperty: 'response.transaction.list.cell_authorized_person' }
		}
	}
});
