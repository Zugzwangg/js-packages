Ext.define('CFJS.store.cellstore.AuthorizedDocuments', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.cell_authorized_documens',
	model	: 'CFJS.model.cellstore.AuthorizedDocument',
	storeId	: 'cell_authorized_documens',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_authorized_document' },
			reader		: { rootProperty: 'response.transaction.list.cell_authorized_document' }
		}
	}
});
