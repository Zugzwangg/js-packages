Ext.define('CFJS.store.cellstore.CompanyRents', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.cell_company_rents',
	model	: 'CFJS.model.cellstore.CompanyRent',
	storeId	: 'cell_company_rents',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_company_rent' },
			reader		: { rootProperty: 'response.transaction.list.cell_company_rent' }
		}
	}
});
