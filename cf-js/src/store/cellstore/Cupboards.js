Ext.define('CFJS.store.cellstore.Cupboards', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.cell_cupboards',
	model	: 'CFJS.model.cellstore.Cupboard',
	storeId	: 'cell_cupboards',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_cupboard' },
			reader		: { rootProperty: 'response.transaction.list.cell_cupboard' }
		},
		sorters	: [ 'name' ]
	}
});
