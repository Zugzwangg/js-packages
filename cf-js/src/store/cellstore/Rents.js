Ext.define('CFJS.store.cellstore.Rents', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.cell_rents',
	model	: 'CFJS.model.cellstore.Rent',
	storeId	: 'cell_rents',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_rent' },
			reader		: { rootProperty: 'response.transaction.list.cell_rent' }
		}
	}
});
