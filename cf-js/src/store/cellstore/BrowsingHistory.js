Ext.define('CFJS.store.cellstore.BrowsingHistory', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.browsing_history',
	model	: 'CFJS.model.cellstore.CellBrowsing',
	storeId	: 'browsing_history',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_browsing' },
			reader		: { rootProperty: 'response.transaction.list.cell_browsing' }
		}
	}
});
