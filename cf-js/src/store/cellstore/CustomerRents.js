Ext.define('CFJS.store.cellstore.CustomerRents', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.cell_customer_rents',
	model	: 'CFJS.model.cellstore.CustomerRent',
	storeId	: 'cell_customer_rents',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_customer_rent' },
			reader		: { rootProperty: 'response.transaction.list.cell_customer_rent' }
		}
	}
});
