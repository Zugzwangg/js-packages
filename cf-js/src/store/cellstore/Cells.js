Ext.define('CFJS.store.cellstore.Cells', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.cells',
	model	: 'CFJS.model.cellstore.Cell',
	storeId	: 'cells',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell' },
			reader		: { rootProperty: 'response.transaction.list.cell' }
		}
	}
});
