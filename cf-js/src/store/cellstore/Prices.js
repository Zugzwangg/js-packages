Ext.define('CFJS.store.cellstore.Prices', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.cell_prices',
	model	: 'CFJS.model.cellstore.Price',
	storeId	: 'cell_prices',
	config	: {
		proxy: {
			service		: 'cell_store',
			loaderConfig: { dictionaryType: 'cell_price' },
			reader		: { rootProperty: 'response.transaction.list.cell_price' }
		}
	}
});
