Ext.define('CFJS.store.travel.TravelTaxAmounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.travel_tax_amounts',
	model	: 'CFJS.model.financial.TaxAmount',
	storeId	: 'travelTaxAmounts',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'travel_tax_amount' },
			reader		: { rootProperty: 'response.transaction.list.tax_amount' }
		}
	}
});
