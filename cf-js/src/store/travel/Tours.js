Ext.define('CFJS.store.travel.Tours', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.tours',
	model	: 'CFJS.model.travel.Tour',
	storeId	: 'tours',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'tour' },
			reader		: { rootProperty: 'response.transaction.list.tour' }
		}
	}
});
