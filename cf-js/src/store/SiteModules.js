Ext.define('CFJS.store.SiteModules', {
	extend			: 'Ext.data.Store',
	alias			: 'store.sitemodules',
	clearOnLoad		: true,
	config			: {
		proxy: {
			type				: 'ajax',
			disableExtraParams	: true,
			noCache				: false,
			signParams			: false,
			url					: 'resources/data/services.json',
			reader				: { type: 'json', rootProperty: 'services' }
		}
	},
	isSiteModules	: true,
	model			: 'CFJS.model.SiteModule',
	page			: 0,
	sorters			: [ 'name' ],
	storeId			: 'siteModules',
	
	initGroupField: function(field) {
		if (field && field.isGroupField) {
			var me = this, util = Ext.Array, parse = CFJS.safeParseInt,
				data = util.from(me.data.items, true), idx = 0;
			if (data.length > 0) {
				util.sort(data, function(a, b) {
		    		return parse(a.get('hash')) - parse(b.get('hash'));
				});
				for (; idx < data.length; idx++) {
					if (idx < data[idx].get('hash')) data.splice(idx, 0, '');
					else data[idx] = data[idx].get('name');
				}
			}
			field.setFormat(data);
		}
	}
});