Ext.define('CFJS.store.EventMembers', {
	extend	: 'Ext.data.Store',
	alias	: 'store.eventmembers',
	model	: 'CFJS.model.EventMember',
	storeId	: 'eventMembers',
	sorters	: [ 'priority', 'name' ]
});