Ext.define('CFJS.store.Base', {
	extend		: 'Ext.data.Store',
	requires	: [ 'CFJS.api.JsonProxy', 'CFJS.model.Summary' ],
	alias		: 'store.base',
	model		: 'CFJS.model.EntityModel',
	remote		: true,
	remoteFilter: true,
	remoteSort	: true,
	storeId		: 'base',
	proxy		: {
		type	: 'json.cfapi',
		api		: {
			create	: 'save',
			destroy : 'remove',
			read	: { method: 'dictionary', transaction: 'dictionary_list' },
			sign	: 'sign',
			summary	: { method: 'dictionary', transaction: 'calculate' },
			update  : 'save'
		},
		reader	: { type: 'json.cfapi', summaryModel: 'CFJS.model.Summary' }
	}
});
