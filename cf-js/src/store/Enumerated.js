Ext.define('CFJS.store.Enumerated', {
	extend	: 'Ext.data.Store',
	alias	: 'store.enumerated',
	model	: 'CFJS.model.EnumModel',
	storeId	: 'enumerated',
	sorters	: [ 'name' ]
});