Ext.define('CFJS.store.airline.AirlineTicketTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.airlinetickettypes',
	storeId	: 'airlineTicketTypes',
	data	: [
		{ id: 'EMD', 			name: 'EMD' },
		{ id: 'TRANSPORTATION',	name: 'Багаж' },
		{ id: 'TRAVEL',			name: 'Проезд' }
	]
});