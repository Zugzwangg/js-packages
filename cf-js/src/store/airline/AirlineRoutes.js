Ext.define('CFJS.store.airline.AirlineRoutes', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.airline_routes',
	model	: 'CFJS.model.airline.AirlineRoute',
	storeId	: 'airlineRoutes',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'airline_route' },
			reader		: { rootProperty: 'response.transaction.list.airline_route' },
			service		: 'iss'
		}
	}
});
