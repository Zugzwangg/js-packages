Ext.define('CFJS.store.airline.AirlineTaxAmounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.airline_tax_amounts',
	model	: 'CFJS.model.financial.TaxAmount',
	storeId	: 'airlineTaxAmounts',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'airline_tax_amount' },
			reader		: { rootProperty: 'response.transaction.list.tax_amount' }
		}
	}
});
