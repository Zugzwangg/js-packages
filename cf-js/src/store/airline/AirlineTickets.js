Ext.define('CFJS.store.airline.AirlineTickets', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.airline_tickets',
	model	: 'CFJS.model.airline.AirlineTicket',
	storeId	: 'airlineTickets',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'airline_ticket' },
			reader		: { rootProperty: 'response.transaction.list.airline_ticket' }
		}
	}
});
