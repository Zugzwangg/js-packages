Ext.define('CFJS.store.airline.Airports', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.airports',
	model	: 'CFJS.model.airline.Airport',
	storeId	: 'airports',
	config	: {
		proxy : {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'airport' },
			reader		: { rootProperty: 'response.transaction.list.airport' }
		}
	}
});
