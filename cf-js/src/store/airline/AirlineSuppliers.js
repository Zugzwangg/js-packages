Ext.define('CFJS.store.airline.AirlineSuppliers', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.airline_suppliers',
	model	: 'CFJS.model.airline.AirlineSupplier',
	storeId	: 'airlineSuppliers',
	config	: {
		proxy : {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'airline_supplier' },
			reader		: { rootProperty: 'response.transaction.list.airline_supplier' }
		}
	}
});
