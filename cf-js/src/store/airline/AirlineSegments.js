Ext.define('CFJS.store.airline.AirlineSegments', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.airline_segments',
	model	: 'CFJS.model.airline.AirlineSegment',
	storeId	: 'airlineSegments',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'airline_segment' },
			reader		: { rootProperty: 'response.transaction.list.airline_segment' }
		}
	}
});
