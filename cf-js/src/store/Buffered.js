Ext.define('CFJS.store.Buffered', {
	extend				: 'Ext.data.BufferedStore',
	requires			: [ 'CFJS.api.JsonProxy' ],
	alias				: 'store.basebuffered',
	model				: 'CFJS.model.EntityModel',
	remote				: true,
	leadingBufferZone	: 25,
	trailingBufferZone	: 25,
	pageSize			: 50,
	storeId				: 'buffered',
	proxy				: {
		type	: 'json.cfapi',
		api		: {
			create	: 'save',
			destroy : 'remove',
			read	: { method: 'dictionary', transaction: 'dictionary_list' },
			sign	: 'sign',
			summary	: { method: 'dictionary', transaction: 'calculate' },
			update  : 'save'
		},
		reader	: { type: 'json.cfapi' }
	},

	contains: function(record) {
        return this.indexOf(record) > -1;
    }
	
});
