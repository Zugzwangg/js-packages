Ext.define('CFJS.store.ExtSystems', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.extsystems',
	model	: 'CFJS.model.ExtSystem',
	storeId	: 'extSystems',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'external_system' },
			reader		: { rootProperty: 'response.transaction.list.external_system' }
		}
	}
});
