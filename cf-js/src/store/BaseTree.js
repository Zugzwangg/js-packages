Ext.define('CFJS.store.BaseTree', {
	extend			: 'Ext.data.TreeStore',
	requires		: [ 'CFJS.api.JsonProxy', 'CFJS.model.Summary' ],
	alias			: 'store.basetree',
	config			: { parentFilter: 'parentId' },
	lazyFill		: true,
	model			: 'CFJS.model.TreeModel',
	remoteFilter	: true,
	remoteSort		: true,
	storeId			: 'baseTree',
	proxy			: {
		type	: 'json.cfapi',
		api		: {
			create	: 'save',
			destroy : 'remove',
			read	: { method: 'dictionary', transaction: 'dictionary_list' },
			sign	: 'sign',
			summary	: { method: 'dictionary', transaction: 'calculate' },
			update  : 'save'
		},
		reader	: { type: 'json.cfapi', summaryModel: 'CFJS.model.Summary' }
	},
	
	applyParentFilter: function(parentFilter) {
		if (Ext.isString(parentFilter)) {
			parentFilter = { id: 'parent', property: parentFilter, type: 'numeric', operator: 'eq' }
		}
		return Ext.isObject(parentFilter) ? new Ext.util.Filter(parentFilter) : null;
	},
	
	privates: {
		
		setLoadOptions: function(options) {
			var me = this, filter;
			me.callParent([options]);
			if (!options.addRecords) {
				options.filters = options.filters || [];
				options.filters.push(filter = me.getParentFilter());
				filter.setValue(options.id === me.getDefaultRootId() ? null : options.id);
				delete options.id;
			}
		}
	
	}
	
}, function() {
	Ext.Factory.treeStore = function(config) {
		return new CFJS.store.BaseTree(config);
	}
});
