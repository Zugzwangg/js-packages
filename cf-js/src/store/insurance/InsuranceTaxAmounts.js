Ext.define('CFJS.store.insurance.InsuranceTaxAmounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.insurance_tax_amounts',
	model	: 'CFJS.model.financial.TaxAmount',
	storeId	: 'insuranceTaxAmounts',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'insurance_tax_amount' },
			reader		: { rootProperty: 'response.transaction.list.tax_amount' }
		}
	}
});
