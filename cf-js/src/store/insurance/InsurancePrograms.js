Ext.define('CFJS.store.insurance.InsurancePrograms', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.insurance_programs',
	model	: 'CFJS.model.insurance.InsuranceProgram',
	storeId	: 'insurancePrograms',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'insurance_program' },
			reader		: { rootProperty: 'response.transaction.list.insurance_program' }
		}
	}
});
