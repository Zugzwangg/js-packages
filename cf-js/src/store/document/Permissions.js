Ext.define('CFJS.store.document.Permissions', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.permissions',
	model	: 'CFJS.model.document.Permission',
	storeId	: 'permissions',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'permission_level' },
			reader		: { rootProperty: 'response.transaction.list.permission' }
		}
	},
	sorters	: [{ property: 'level', direction: 'ASC' }]
});
