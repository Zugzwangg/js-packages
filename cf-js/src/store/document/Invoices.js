Ext.define('CFJS.store.document.Invoices', {
	extend		: 'CFJS.store.Base',
	alias		: 'store.invoices',
	model		: 'CFJS.model.document.Invoice',
	storeId		: 'invoices',
	remoteSort	: false,
	sortOnLoad	: true,
	sorters		: [{
		sorterFn: function(rec1, rec2) {
			return parseInt(rec1.get('number')) - parseInt(rec2.get('number'));
		}
	}],
	config		: {
		proxy: {
			api			: { 
				create	: { method: 'save.invoice', transaction: 'save' },
				read	: { method: 'load.invoice', transaction: 'load' },
				sign	: { method: 'sign.invoice', transaction: 'sign' },
				update	: { method: 'save.invoice', transaction: 'save' }
			},
			service		: 'iss',
			reader		: { rootProperty: 'response.transaction.invoice' }
		}
	}
});
