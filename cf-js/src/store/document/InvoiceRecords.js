Ext.define('CFJS.store.document.InvoiceRecords', {
	extend		: 'CFJS.store.Base',
	alias		: 'store.invoicerecords',
	model		: 'CFJS.model.document.InvoiceRecord',
	storeId		: 'invoiceRecords',
	remoteSort	: false,
	sortOnLoad	: true,
	sorters		: 'orderNumber',
	config		: {
		proxy: {
			api				: { read: 'new.invoce_records' },
	    	actionMethods	: { read: 'GET' },
	    	loaderConfig	: { dictionaryType: null },
			reader			: { rootProperty: 'response.transaction.invoice_record' },
			service			: 'iss'
		}
	}
});
