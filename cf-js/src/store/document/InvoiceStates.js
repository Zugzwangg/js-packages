Ext.define('CFJS.store.document.InvoiceStates', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.invoicestates',
	storeId	: 'invoiceStates'
});