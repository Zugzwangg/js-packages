Ext.define('CFJS.store.railway.RailwayTickets', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.railway_tickets',
	model	: 'CFJS.model.railway.RailwayTicket',
	storeId	: 'railwayTickets',
	config	: {
		proxy : {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'railway_ticket' },
			reader		: { rootProperty: 'response.transaction.list.railway_ticket' }
		}
	}
});
