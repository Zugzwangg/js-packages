Ext.define('CFJS.store.railway.RailwayTicketTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.railwaytickettypes',
	storeId	: 'railwayTicketTypes',
	data	: [
		{ id: 'RESERVE', 		name: 'Бронь' },
		{ id: 'TRANSPORTATION',	name: 'Багаж' },
		{ id: 'TRAVEL',			name: 'Проезд' }
	]
});