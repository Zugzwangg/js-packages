Ext.define('CFJS.store.railway.RailwayTaxAmounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.railway_tax_amounts',
	model	: 'CFJS.model.financial.TaxAmount',
	storeId	: 'railwayTaxAmounts',
	config	: {
		proxy : {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'railway_tax_amount' },
			reader		: { rootProperty: 'response.transaction.list.tax_amount' }
		}
	}
});
