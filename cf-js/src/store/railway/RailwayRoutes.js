Ext.define('CFJS.store.railway.RailwayRoutes', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.railway_routes',
	model	: 'CFJS.model.railway.RailwayRoute',
	storeId	: 'railwayRoutes',
	config	: {
		proxy : {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'railway_route' },
			reader		: { rootProperty: 'response.transaction.list.railway_route' }
		}
	}
});
