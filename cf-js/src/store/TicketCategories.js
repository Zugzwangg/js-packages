Ext.define('CFJS.store.TicketCategories', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.ticketcategories',
	storeId	: 'ticketCategories',
	data	: [
		{ id: 'CHILD', 		name: 'Детский' },
		{ id: 'FULL',		name: 'Полный' },
		{ id: 'PRIVILEGE',	name: 'Льготный' },
		{ id: 'MILITARY',	name: 'Льготный (военный)' }
	]
});