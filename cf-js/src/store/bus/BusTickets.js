Ext.define('CFJS.store.bus.BusTickets', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.bus_tickets',
	model	: 'CFJS.model.bus.BusTicket',
	storeId	: 'busTickets',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'bus_ticket' },
			reader		: { rootProperty: 'response.transaction.list.bus_ticket' }
		}
	}
});
