Ext.define('CFJS.store.bus.BusTaxAmounts', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.bus_tax_amounts',
	model	: 'CFJS.model.financial.TaxAmount',
	storeId	: 'busTaxAmounts',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'bus_tax_amount' },
			reader		: { rootProperty: 'response.transaction.list.tax_amount' }
		}
	}
});
