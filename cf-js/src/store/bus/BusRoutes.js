Ext.define('CFJS.store.bus.BusRoutes', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.bus_routes',
	model	: 'CFJS.model.bus.BusRoute',
	storeId	: 'busRoutes',
	config	: {
		proxy: {
			service		: 'iss',
			loaderConfig: { dictionaryType: 'bus_route' },
			reader		: { rootProperty: 'response.transaction.list.bus_route' }
		}
	}
});
