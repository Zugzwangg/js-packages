Ext.define('CFJS.view.AuthenticationController', {
	extend			: 'Ext.app.ViewController',
	alias			: 'controller.authentication',
	loginFailedText	: 'Login failed',

	init: function(view) {
		var me = this, serviceombo;
		if (view.isXType('login-view')) {
			me.loginView = view;
			if (serviceCombo = view.down('dictionarycombo[name="service"]')) serviceCombo.hide();
		}
		if (view.bodyContainer) {
			view.bodyContainer.items.each(me.initForm, me)
		}
	},
	
	initForm: function(panel) {
		var defBtn = panel.defaultButton, 
			btn = defBtn && panel.down('button[action=' + defBtn + ']');
		if (btn) {
			panel.onAfter('render', function() {
				panel.defaultButtonKeyNav = new Ext.util.KeyNav({
					target				: panel.el,
					scope				: panel,
					defaultEventAction	: 'stopEvent',
					enter				: function(e) {
						if (btn && btn.click) {
							btn.click(e);
							e.stopEvent();
							return false;
						}
					}
				});
			}, panel, { single: true, destroyable: true });
			panel.defaultButton = false;
		}
	},

	loginForm: function(view) {
		return this.getView().getActiveItem();
	},
	
	login: function(me, form, options) {
		if (form && form.isValid() && options) {
			if (Ext.isObject(options.service)) options.service = options.service.id;
			CFJS.Api.login(options.username, options.password, options.service, true,
				function(session) {
					if (session.passwordExpired > 0 && session.passwordExpired < Ext.Date.now()) {
						me.switchTo('resetPassword');
						return;
					}
					me.unlock(form, options.persist);
				},
				function(context) { CFJS.errorAlert(me.loginFailedText, context, form); }
			);
		}
	},
	
	unlock: function(form, persist) {
		var me = this, vm = me.getViewModel(), fakesubmit = form ? Ext.fly(form.fakeSubmitId) : null;
		if (persist && fakesubmit) fakesubmit.dom.click();
		if (vm) vm.set('locked', false);
		me.closeView();
		if (vm) me.redirectTo(vm.get('lastRoute'));
	},
	
	onLoginClick: function(button) {
		var me = this, form = me.loginForm(), vm = form && form.lookupViewModel();
		me.login(me, form, vm ? vm.getData() : null);
	},

	onResetPasswordClick: function() {
		var me = this, form = me.loginForm(), params = form && form.lookupViewModel().getData();
		if (form.isValid() && params && !Ext.isEmpty(params.password)) {
			CFJS.Api.resetPassword(params.password, null,
				function(context) { me.unlock(form, params.persist); },
				function(context) { CFJS.errorAlert(me.resetPasswordFailedText, context, form); }
			);
		}
	},
	
	onSendRecoveryEmailClick: function() {
		var me = this, form = me.loginForm(), params = form && form.lookupViewModel().getData();
		if (form.isValid() && params && !Ext.isEmpty(params.email)) {
			CFJS.Api.recoveryEmail(params.email,
				function(context) { me.switchTo('loginPanel'); },
				function(context) { CFJS.errorAlert(me.sendEmailFailedText, context, form); }
			);
		}
	},

	onServicesLoad: function(store, records) {
		var me = this, form = me.loginForm(), 
			vm = form && form.lookupViewModel();
		if (vm && vm.isViewModel) {
			vm.set('service', vm.get('service') || store.getById(CFJS.Api.getService()));
		}
	},
	
	onUnlockClick: function(button) {
		var me = this, form = me.loginForm(), vm = form && form.lookupViewModel(),
			vmData = vm ? vm.getData() : null;
		me.login(me, form, {
			username: vmData.user.get('username'),
			password: vmData.password,
			service: vmData.service,
			persist: vmData.persist
		});
	},
	
	switchTo: function(newCard, e) {
		return this.loginView.setActiveItem(newCard);
	}
	
});