Ext.define('CFJS.view.AuthenticationModel', {
	extend	: 'Ext.app.ViewModel',
	alias	: 'viewmodel.authentication',
	requires: [ 'CFJS.store.SiteModules', 'CFJS.util.UI' ],
	data	: {
		agrees			: false,
		confirmPassword	: '',
		email			: '',
		fullName		: '',
		password		: '',
		persist			: false,
		username		: ''
	},
	stores	: {
		modules: {
			type		: 'sitemodules',
			autoLoad	: true,
			listeners	: { load: 'onServicesLoad' }
		}
	}
});