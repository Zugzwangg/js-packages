Ext.define('CFJS.view.ViewportModel', {
	extend			: 'Ext.app.ViewModel',
	alias			: 'viewmodel.viewport',
	requires		: [
		'CFJS.model.dictionary.Customer',
		'CFJS.model.orgchart.Person',
		'CFJS.store.AppMenu',
		'CFJS.store.SiteModules',
		'CFJS.store.orgchart.Secretaries',
		'CFJS.util.UI'
	],
	data			: {
		allowedUserTypes	: null,
		currentView			: null,
		locked				: false,
		lastRoute			: 'home',
		navigatorCollapsed	: false ,
		permissions			: {},
		session				: {}
	},
	schema			: 'org_chart',
	stores			: {
		appMenu: {
			type: 'appmenu',
			root: { expanded: true }
		},
		modules: {
			type	: 'sitemodules',
			autoLoad: true
		}
	},
	formulas		: {
		appConfig: {
			get: function(get) {
				return CFJS.config;
			}
		},
		companyLogo: {
			bind: { locale: '{appConfig.locale}', collapsed: '{navigatorCollapsed}' },
			get: function(data) {
				var locale = data.locale || {},
					lang = locale.name || 'en',
					appInfo = locale[lang] || {};
				return new Ext.XTemplate(
					'<table class="main-logo" cellpadding="0"><tbody><tr>',
						'<td><img src="resources/images/logo.png"></td>',
						'<tpl if="!collapsed">',
							'<tpl if="!frame">',
								'<td><img src="resources/images/header.png"></td>',
							'</tpl>',
							'<td>{appName}</td>',
						'</tpl>',
					'</tr></tbody></table>'
				).apply({ appName : appInfo.apptitle || this.get('appName'), collapsed: data.collapsed, frame: CFJS.isFrame });
			}
		},
		permissionLevel: function(get) {
			return this.safeLevel(get('userInfo.permissionLevel'));
		}
	},
	
	buildAppMenuUrl: function(utype, platformTag, locale) {
		return 'resources/data/' + [ utype||'person' , platformTag||'desktop', 'app_menu', locale].join('_') + '.json';
	},

	initConfig: function() {
		var me = this;
		me.callParent(arguments);
		me.bind('{session}', me.onSessionChange, me);
		me.bind('{user}', me.onUserChange, me);
		me.bind('{userInfo.bosses}', me.onUserBossesChange, me);
		me.bind('{userInfo.sectors}', me.onUserSectorsChange, me);
	},
	
	isAdministrator: function(user) {
		user = user || this.get('user');
		return user && Ext.isFunction(user.isAdministrator) && user.isAdministrator();
	},
	
	isCurrentUser: function(user) {
		var userInfo = this.userInfo();
		return userInfo && user && userInfo.id === user.id && userInfo.type === user.type; 
	},

	lookupController: function() {
		return this.getView().lookupController();
	},
	
	onSessionChange: function(session) {
		var me = this, controller = me.lookupController(),
			utype = session && session.utype;
		if (!Ext.Array.contains(Ext.Array.from(me.get('allowedUserTypes')), utype)) {
			me.getView().unmask();
			CFJS.errorAlert('Login failed', { code: 1000, text: 'Access denied for this user' }, null, function() {
				var callback = function() { CFJS.Api.clearSessionCookies(); CFJS.$application.showLogin(); };
				CFJS.Api.logout(callback, callback);
			});
			return;
		}
		me.linkTo('user', Ext.apply({ type : utype.capitalize() },
			CFJS.Api.isSessionExpired(session) ? { create: true } : { id: parseInt(session.uid) }));
		Ext.fireEvent('sessionchange', me, session);
	},
	
	onUserChange: function(user) {
		var me = this, api = CFJS.Api, view = me.getView(),
			controller = view.lookupController(),
			app = controller.getApplication(),
			level =  CFJS.Api.getClientPermissionLevel(),
			utype, isAdministrator, isPerson, username, info, unit, sectors;
		if (user) {
			if (user.isModel) {
				utype = me.userInfoType(user);
				isAdministrator = user.isAdministrator();
				if (isPerson = user.isPerson) level = user.permissionLevel();
				user = user.getData();
				info = { id: user.id, name: user.fullName, type: utype, permissionLevel: me.safeLevel(level) };
				if (info.type.length > 0) info.$class = info.type[0];
				if (isPerson) {
					info.sectors = Ext.Array.from(user.post && user.post.sectors);
					if (me.get('appConfig.allowSecretary') === true) {
						Ext.Factory.store({
							type	: 'secretaries',
							filters	: [{
								id		: 'secretary',
								property: 'secretary.id',
								type	: 'numeric',
								operator: 'eq',
								value	: (user.post||{}).id
							},{
								property: 'startDate',
								type	: 'date',
								operator: '!after',
								value	: new Date().getTime()
							},{
								property: 'endDate',
								type	: 'date',
								operator: '!before',
								value	: new Date().getTime()
							}],
							pageSize: 0
						}).load(function(records, operation, success) {
							var bosses = [];
							if (success) Ext.Array.each(records, function(record) {
								bosses.push(record.get('post'));
							});
							me.set('userInfo.bosses', bosses);
						});
						
					}
				} else {
					Ext.Factory.store({
						type	:'named',
						filters	: [{ id: 'parent', property: 'parent.id', type: 'numeric', operator: 'eq' }],
						pageSize: 0,
						proxy	: { loaderConfig: { dictionaryType: 'sector' } }
					}).load(function(records, operation, success) {
						var sectors = [];
						if (success) Ext.Array.each(records, function(record) {
							sectors.push(record.getData());
						});
						me.set('userInfo.sectors', sectors);
					});
				}
			}
			if (unit = user.post && user.post.unit || user.unit) {
				if (unit.isModel) unit = unit.getData();
				unit = { id: unit.id, name: unit.name };
			}
			username = user.username;
		}
		if (api.getUsePermissions() && !Ext.isEmpty(username)) {
			api.loadPermissions(username, function(permissions) {
				me.setPermissions(permissions);
			}, function(context) {
				CFJS.errorAlert('Login failed', context, null, function() {
					var callback = function() { app.showLogin(); };
					api.logout(callback, callback);
				});
			});
		} else me.setPermissions();
		Ext.Ajax.request({
			url					: me.buildAppMenuUrl(info.type, app.platformTag(), CFJS.getLocale()),
			disableExtraParams	: true,
			noCache				: false,
			signParams			: false,
			success: function(response, opts) {
				me.updateMenus(Ext.decode(response.responseText), isAdministrator);
			},
			failure: function(context) {
				CFJS.errorAlert('Application failed', context, null, function() {
					var callback = function() { app.showLogin(); };
					api.logout(callback, callback);
				})
			}
		});
		me.set({ userInfo: info, userUnit: unit });
		Ext.fireEvent('userchange', me, user);
		me.getView().unmask();
	},
	
	onUserBossesChange: function(bosses) {
		this.updateUserProfileMenu(this.getView(), bosses);
	},
	
	onUserSectorsChange: function(sectors) {
		var me = this, types = Ext.getStore('serviceTypes'), ids = [];
		Ext.Array.each(sectors, function(sector) {
			ids.push(sector.id);
		});
		types.addFilter({ 
			id: 'sectors',
			filterFn: function(item) {
				return Ext.Array.contains(ids, item.get('sector'));
			}
		});
	},

	postInfo: function() {
		var me = this, user = me.get('user'), post = user && user.isPerson && user.getPost();
		return post ? Ext.apply({ type: me.userInfoType(post) }, post.isModel ? post.getData() : post) : null;
	},
	
	postInfoAsAuthor: function(postInfo) {
		return this.toUserInfo(postInfo || this.postInfo());
	},

	setPermissions: function(permissions) {
		var me = this;
		permissions = permissions || {};
		if (permissions.isModel) permissions = Ext.apply(permissions, permissions.data);
		me.set('permissions',  permissions);
		Ext.fireEvent('permissionschange', me, permissions);
	},
	
	updateMenus: function(menus, isAdministrator) {
		var me = this, cfg = menus['en'], locale = CFJS.getLocale(),
			appMenu = me.getStore('appMenu'), view = me.getView();
		if (locale !== 'en') cfg = CFJS.merge(cfg, menus[locale]);
		appMenu.setRoot({ expanded: true, children: me.hideSettings(cfg.appMenu, !isAdministrator) });
		if (view.isViewport && cfg.toolButtons) {
			view.setToolBarItems(me.hideSettings(cfg.toolButtons, !isAdministrator));
			me.updateUserProfileMenu(view, me.get('userInfo.bosses'));
		}
		return cfg;
	},
	
	userInfo: function() {
		return this.get('userInfo');
	},

	userInfoAsAuthor: function(userInfo) {
		return this.toUserInfo(userInfo || this.userInfo());
	},

	userInfoType: function(entity) {
		if (entity && Ext.isString(entity.entityName)) {
			var type = entity.entityName.split('.');
			return type[type.length - 1].toLowerCase();
		}
	},

	privates: {
		
		hideSettings: function(array, hide) {
			if (hide && Ext.isArray(array)) {
				for (var i = 0; i < array.length; i++) {
					if (array[i] && (array[i].id === 'settings' || array[i].action === 'settings')) {
						array.splice(i, 1);
					}
				}
			}
			return array;
		},
		
		safeLevel: CFJS.safeLevel,
		
		toUserInfo: function(userInfo) {
			return userInfo ? { id: userInfo.id, name: userInfo.name, type: userInfo.type } : null;
		},
		
		updateUserProfileMenu: function(view, bosses) {
			var items = (view || this.getView()).query('[href="profile"],[action="user-profile"],[id="user-profile"]'),
				menu = [];
			Ext.Array.each(Ext.Array.from(bosses), function(boss, idx) {
				menu.push({
					handler	: 'onLoginAsClick',
					iconCls	: CFJS.UI.iconCls.USER,
					text	: boss.name,
					boss	: boss
				})
			});
			menu = menu.length > 0 ? { items: menu } : null;
			Ext.Array.each(items, function(item, idx) {
				if (item && Ext.isFunction(item.setMenu)) item.setMenu(menu);
			});
		},
	
	}
	
});
