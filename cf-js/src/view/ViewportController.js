Ext.define('CFJS.view.ViewportController', {
	extend			: 'CFJS.app.HistoryController',
	alias			: 'controller.viewport',
	id				: 'viewportController',
	routes			: { ':node': 'onRouteChange' },
	listen			: {
		controller: {
			'#': { unmatchedroute: 'onUnmatchedRoute' }
		}
	},
	hiddenMenu		: {
		home: { view: 'CFJS.pages.Home', system: true },
		lock: { view: 'CFJS.pages.LockScreen', secure: true, system: true }
	},
	infoText		: 'Information',
	goText			: 'go',
	loadingText		: 'Loading...',
	loginFailedText	: 'Login failed',
	lostDataText	: 'All changes will be lost. Are you sure you want to {0}?',
	warningText		: 'Warning',
	
	confirm: function(msg, fn, scope) {
		return Ext.Msg.confirm(this.warningText, msg, fn, scope);
	},
	
	confirmLostData: function(msg, fn, scope) {
		return this.confirm(Ext.String.format(this.lostDataText, msg), fn, scope);
	},

	confirmLostDataAndGo: function(fn, scope) {
		return this.confirmLostData(this.goText, fn, scope);
	},
	
	infoAlert: function(msg, fn, scope, animateTarget) {
		CFJS.infoAlert(this.infoText, msg, fn, scope, animateTarget);
	},
	
	init: function(view) {
		var me = this, navigator = me.getNavigator();
		if (navigator && Ext.isFunction(navigator.setStore)) navigator.setStore(me.getStore('appMenu'));
		if (view.isViewport) view.mask(me.loadingText);
		Ext.on({
			destroyable				: true,
			confirm					: me.confirm,
			confirmlostdata			: me.confirmLostData,
			confirmlostdataandgo	: me.confirmLostDataAndGo,
			infoalert				: me.infoAlert,
			scope					: me
		});
	},
	
	initConfig: function() {
		var me = this;
		me.api = me.getApplication().getApi(); 
		me.callParent(arguments)
	},
	
	initViewModel: function(viewModel) {
		var me = this, api = me.api;
		viewModel.set('appName', me.getApplication().getName());
		viewModel.set('session', api.findSession());
		api.setViewModel(viewModel);
	},
	
	beforeChangeView: function(store, currentView, hashTag) {
		var root = store && store.isTreeStore && store.getRoot();
		// check if menu store is loaded
		if (root && !root.hasChildNodes()) {
			store.on('rootchange', this.setCurrentView, this, { single: true, args: [hashTag]});
			return false;
		}
		return store;
	},
	
	findRecord: function(store, hashTag) {
		return this.getHiddenMenu()[hashTag] || (store ? store.findNode(this.getRouteId(), hashTag) : null);
	},
	
	onLockClick: function() {
		this.api.saveSessionToCookies({});
		this.redirectTo('lock');
	},
	
	onLoginAsClick: function(item) {
		var me = this, boss = (item||{}).boss;
		if (boss && boss.id > 0) {
			CFJS.model.orgchart.Post.load(boss.id, {
				callback: function(record, operation, success) {
					if (success) me.api.loginAs(record.getPerson().id, null, true,
						function(session) { location.reload(); },
						function(context) { CFJS.errorAlert(me.loginFailedText, context); }
					); 
				}
			});
		}
	},
	
	onToggleNavigationSize: function () {
		var me = this,
			view = me.getView(),
			refs = me.getReferences(),
			navigator = me.getNavigator(),
			desktopWrap = view.getDesktopWrap(),
			viewModel = me.getViewModel(),
            collapsing = !navigator.getMicro(),
			new_width = collapsing ? 64 : me.getView().logoWidth;
		viewModel.set('navigatorCollapsed', collapsing);
		if (Ext.isIE9m || !Ext.os.is.Desktop) {
			Ext.suspendLayouts();
			refs.logotype.setWidth(new_width);
			navigator.setWidth(new_width);
			navigator.setMicro(collapsing);
			Ext.resumeLayouts(); // do not flush the layout here...
			desktopWrap.layout.animatePolicy = desktopWrap.layout.animate = null;
			desktopWrap.updateLayout();  // ... since this will flush them
		} else {
			if (!collapsing) navigator.setMicro(false);
			refs.logotype.animate({ dynamic: true, to: { width: new_width } });
			navigator.width = new_width;
			desktopWrap.updateLayout({ isRoot: true });
			if (collapsing) {
				navigator.on({
					afterlayoutanimation: function () {
						navigator.setMicro(true);
					},
					single: true
				});
			}
		}
	},
	
	onUnmatchedRoute: function(hashTag) {
		this.setCurrentView(hashTag);
	},
	
	setCurrentView: function(hashTag) {
		var me = this, api = me.api;
		hashTag = (hashTag || '').toLowerCase();
		if (hashTag === 'logout') {
			var callback = function() { me.getApplication().showLogin(); };
			window.location.hash='';
			api.logout(callback, callback);
		} else me.callParent(arguments);
	}

});
