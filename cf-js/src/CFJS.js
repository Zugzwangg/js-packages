String.prototype.capitalize = function(lc) {
	var ret = this.slice(1);
	if (lc !== false) ret = ret.toLowerCase();
	return this.charAt(0).toUpperCase() + ret;
}
if (!String.prototype.quote) {
    String.prototype.quote = function(single){
        return single ? "'" + this + "'" : '"' + this + '"';
    }
}
if (!String.prototype.rpad) {
	String.prototype.rpad = function(l, s) {
		return this + ((l-=this.length)>0?(s=s||' ').repeat(Math.ceil(l/s.length)):'');
	}
}
if (!String.prototype.lpad) {
	String.prototype.lpad = function(l, s) {
		return ((l-=this.length)>0?(s=s||' ').repeat(Math.ceil(l/s.length)):'') + this;
	}
}
if (!String.prototype.pad) {
	String.prototype.pad = function(l, s, t) {
		if ((l -= this.length) > 0) {
		    var i = Math.floor(l / 2);
			s = (s = s || ' ').repeat(Math.ceil(l / s.length));
			return s.substr(0, i) + this + s.substr(0, l - i);
		} else return this;
	}
}
if (!String.prototype.repeat) {
	String.prototype.repeat = function(n) { return new Array( n + 1 ).join(this); }
}
if (!String.prototype.startsWith) {
	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.lastIndexOf(searchString, position) === position;
	}
}
if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(searchString, position) {
		var subjectString = this.toString();
		if (position === undefined || position > subjectString.length) {
			position = subjectString.length;
		}
		position -= searchString.length;
		var lastIndex = subjectString.indexOf(searchString, position);
		return lastIndex !== -1 && lastIndex === position;
	}
}
if (!String.prototype.beautifyJson) {
	String.prototype.beautifyJson = function() {
		try { return JSON.stringify(JSON.parse(this), null, 4);} catch (err) {}
		return this;
	}
}
Date.parse = function (date) {
	var numericKeys = [ 1, 4, 5, 6, 7, 10, 11 ], timestamp, struct, minutesOffset = 0;
	// ES5 §15.9.4.2 states that the string should attempt to be parsed as a Date Time String Format string
	// before falling back to any implementation-specific date parsing, so that’s what we do, even if native
	// implementations could be faster
	//              1 YYYY                2 MM       3 DD           4 HH    5 mm       6 ss        7 msec        8 Z 9 ±    10 tzHH    11 tzmm
	if ((struct = /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::?(\d{2}))?)?)?$/.exec(date))) {
		// avoid NaN timestamps caused by “undefined” values being passed to Date.UTC
		for (var i = 0, k; (k = numericKeys[i]); ++i) {
			struct[k] = +struct[k] || 0;
		}
		// allow undefined days and months
		struct[2] = (+struct[2] || 1) - 1;
		struct[3] = +struct[3] || 1;
		if (struct[8] !== 'Z' && struct[9] !== undefined) {
			minutesOffset = struct[10] * 60 + struct[11];
			if (struct[9] === '+') minutesOffset = 0 - minutesOffset;
		}
		timestamp = Date.UTC(struct[1], struct[2], struct[3], struct[4], struct[5] + minutesOffset, struct[6], struct[7]);
	} else timestamp = parseDate ? parseDate(date) : NaN;
	return timestamp;
};

//	@tag class
/**
 * @class CFJS
 * @singleton
 */
//	@define CFJS
//	@require Ext.Ajax
//	@require Ext.MessageBox
//	@require Ext.util.Format
var CFJS = Ext.apply(CFJS || window['CFJS'] || {}, {

	name				: 'CFJS',
	hasFile				: new Boolean(window.File && window.FileReader && window.FileList && window.Blob).valueOf(),
	hasSessionStorage	: Ext.isDefined(window.sessionStorage),
	hasStorage			: Ext.isDefined(Storage),
	isFrame				: window.top !== window,
	shortMax			: 32767,
	singleton			: true,

	apply: function(dest, src, config) {
		return Ext.apply(CFJS.merge(dest, src || {}), config);
	},

	asObject: function(path, value) {
		if (value) {
			var nameParts = (path || '').split('.'), i;
			for (i = nameParts.length - 1; i >= 0; i--) {
				value = CFJS.toObject(nameParts[i], value);
			}
		}
		return value;
	},

	applyUniversalAnalytics: function(uaid) {
		if (!uaid) return;
		CFJS.loadScript('https://www.googletagmanager.com/gtag/js?id='+uaid, null, true);
		window.dataLayer = window.dataLayer || [];
		window.gtag = function() { dataLayer.push(arguments); }
		window.gtag('js', new Date());
		window.gtag('config', uaid);
	},

	clearEmptyProperties: function(obj, recurse) {
		var newObj = obj ? {} : obj, key;
		for (key in obj) {
			if (!Ext.isEmpty(obj[key])) {
				if (recurse && typeof obj[key] === 'object') {
					newObj[key] = CFJS.clearEmptyProperties(obj[key], recurse);
				} else newObj[key] = obj[key];
			}
		}
		return newObj;
	},

	createObjectURL: function (file) {
		return Ext.os.is.iOS && (CFJS.getIOSVersion() < 7) ? webkitURL.createObjectURL(file) : URL.createObjectURL(file);
	},

	createTag: function(name, attr) {
		var tag = document.createElement(name);
		for (key in attr) {
			if (attr[key] !== null && attr[key] !== undefined) tag.setAttribute(key, attr[key]); 
		}
		return tag;
	},

	deleteCookie: function(name, path, domain) {
		if (CFJS.getCookie(name)) 
			document.cookie = name + "=" +
				((path)	? ";path=" + path : "") +
				((domain) ? ";domain=" + domain : "") +
				";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	},

	downloadAsBlob: function(response, fileName) {
		if (response && response.responseBytes) {
			var blob = new Blob([response.responseBytes], { type: response.getResponseHeader('Content-Type') });
			if (Ext.isDefined(window.navigator.msSaveBlob)) {
				window.navigator.msSaveBlob(blob, fileName);
			} else {
				var URL = window.URL || window.webkitURL,
					url = URL.createObjectURL(blob),
					a = CFJS.createTag('a', { download: fileName, href: url, style: "display: none"});
				if (Ext.isDefined(a.download)) {
					document.body.appendChild(a);
					a.click();
				} else window.open(url, '_blank');
				setTimeout(function () { URL.revokeObjectURL(url); }, 100);
			}
		}
	},

	errorAlert: function(title, message, animateTarget, fn) {
		if (Ext.isObject(message)) message = CFJS.errorMessage(message);
		CFJS.showMsg({
			title: title,
			message: message,
			buttons: Ext.MessageBox.OK,
			animateTarget: animateTarget,
			scope: this,
			fn: fn || Ext.emptyFn,
			icon: Ext.MessageBox['ERROR'],
			width: 450
		});
		console.error(message);
	},

	errorMessage: function(error) {
		return Ext.isObject(error) ? Ext.util.Format.format('Error {0}: {1}', error.code, error.text) : '';
	},

	extractId: function(obj) {
		if (Ext.isObject(obj)) obj = obj.id;
		if (!Ext.isNumber(obj)) obj = CFJS.parseInt(obj);
		return obj && !isNaN(obj) ? obj : null; 
	},

	findLocale: function(nameCookie) {
		try {
			var locale = CFJS.config.locale, 
				cookie = locale.cookie,
				defaultLocale = locale.name;
			// Look for the locale as a url argument
			locale = location.href.match(/locale=([\w-]+)/);
			locale = locale && locale[1];
			// Look for the locale as a cookie
			if (!locale) { locale = CFJS.getCookie(nameCookie || cookie); }
			// Use the browser's locale
			if (!locale) {
				locale = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
				if (locale) locale = locale.replace(/-/g, '_');
			}
			if (!locale) return defaultLocale;
			if ((lastIndex = locale.lastIndexOf('_')) != -1) locale = locale.substring(0,lastIndex);
			return /^(ru|en|uk|ukr)$/.test(locale) ? locale : defaultLocale;
		} catch(e) {
			console.debug('Unexpected exception in locale detection, using default: ' + e);
		}
	},

	fromQueryString: function(queryString, separator) {
		var params = {}, separator = separator || '&',
			parts = (queryString || '').replace(/^\?|^\#/, '').split(separator),
			i, part, args, name, value;
		for (i = 0; i < parts.length; i++) {
			part = parts[i];
			if (part.length > 0) {
				args = part.split('='), 
				name = args[0].replace(/\+/g, '%20'),
				value = args[1] !== undefined ? args[1].replace(/\+/g, '%20') : '';
				params[decodeURIComponent(name)] = decodeURIComponent(value);
			}
		}
		return params;
	},

	generateUUID: function(mask) {
		var d = new Date().getTime(),
			uuid = (mask || 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx').replace(/[xy]/g, 
					function(c) {
						var r = (d + Math.random() * 16) % 16 | 0;
						d = Math.floor(d / 16);
						return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
					});
		return uuid;
	},

	getCookie: function(c_name) {
		var i, x, y, cookies = document.cookie.split(";");
		for (i = 0; i < cookies.length; i++) {
			x = cookies[i].substr(0, cookies[i].indexOf("="));
			y = cookies[i].substr(cookies[i].indexOf("=") + 1);
			x = x.replace(/^\s+|\s+$/g,"");
			if (x == c_name) return unescape(y);
		}
	},

	getIOSVersion: function () {
		var verString = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
		return parseInt(verString[1], 10);
	},

	getLocale: function() {
		return CFJS.config.locale.name;
	},

	getViewport: function() {
		return CFJS.$application && CFJS.$application.getViewport();
	},

	infoAlert: function(title, message, fn, scope, animateTarget) {
		if (Ext.isObject(message)) message = this.errorMessage(message);
		CFJS.showMsg({
			title: title,
			message: message,
			buttons: Ext.MessageBox.OK,
			animateTarget: animateTarget || CFJS.$application.getViewport(),
			scope: scope || this,
			fn: fn || Ext.emptyFn,
			icon: Ext.MessageBox['INFO'],
			width: 450
		});	
		console.info(message);
	},
	
	isAdministrator: function() {
		var vm = CFJS.lookupViewportViewModel();
		return vm && vm.isAdministrator();
	},
	
	isEmpty: function(object){
		for (var key in object) {
			if (object.hasOwnProperty(key)) return false;
		}
		return true;    
	},

	loadCss: function(url) {
		var link = CFJS.createTag('link', {
				href: url,
				rel: 'stylesheet',
				type: 'text/css'
			}),
		headEl = document.getElementsByTagName('head')[0];
		headEl.appendChild(link);
	},
	
	loadExternalScripts: function(scripts, callback) {
		if (!scripts) return;
		var me = CFJS,
			i = 0, script, params;
		if (!Ext.isArray(scripts)) scripts = [scripts];
		for (; i < scripts.length; i++) {
			if (Ext.isString(script = scripts[i])) script = { url: script };
			else if (!Ext.isObject(script)) continue;
			if (!Ext.isEmpty(script.url)) me.loadScript(script.url, i === (scripts.length - 1) ? callback : null, script.async);
		}
	},

	loadLibraries: function(libs, callback, noCache) {
		if (libs && libs.length > 0) {
			var lib = (CFJS.libsUrl||'') + libs.splice(0, 1);
			if (noCache === true) lib += "?dc_" + new Date().getTime();
			CFJS.loadScript(lib, function() { CFJS.loadLibraries(libs, callback, noCache); });
			return;
		}
		if (!!callback && typeof callback === 'function') callback();
	},

	loadManifest: function(options) {
		options = options || {};
		Ext.Ajax.request(Ext.apply({}, {
			success: function(response) {
				var config = Ext.decode(response.responseText);
				CFJS.updateConfig(config);
				Ext.callback(options.success, CFJS, [config]);
			},
			failure: options.failure
		}, options));
	},

	loadScript: function(url, callback, async) {
		var tag = 'script',
			script = CFJS.createTag(tag, {
				async: async,
				src: url,
				type: 'text/javascript'
			}),
			scriptEl = document.getElementsByTagName(tag)[0];
		if (Ext.isFunction(callback)) {
			script.onreadystatechange = callback;
			script.onload = callback;
		}
		scriptEl.parentNode.insertBefore(script, scriptEl);
	},

	localizedResource: function(path, poolName, packageName) {
		return CFJS.resourcePath(Ext.util.Format.format(path, CFJS.getLocale()), poolName, packageName);
	},

	localizedSharedResource: function(path, packageName) {
		return CFJS.localizedResource(path, 'shared', packageName);
	},

	lookupViewportController: function() {
		var viewport = CFJS.getViewport();
		return viewport && viewport.isComponent && viewport.lookupController();
	},

	lookupViewportViewModel: function() {
		var viewport = CFJS.getViewport();
		return viewport && viewport.isComponent && viewport.lookupViewModel();
	},

	merge: function(dest, src) {
		var mergeFn = CFJS.merge,
			key, value;
		if (!src) return src;
		if (Ext.isArray(src)) {
			dest = dest || [];
			if (!Ext.isArray(dest)) dest = [dest];
			for (key = 0; key < src.length; key++) {
				if (key < dest.length) dest[key] = mergeFn(dest[key], src[key]);
				else dest.splice(dest.length, 0, src[key]);
			}
		} else if (Ext.isObject(src)) {
			dest = dest || {};
			for (key in src) {
				dest[key] = mergeFn(dest[key], src[key]);
			}
		} else dest = Ext.clone(src);
		return dest;
	},

	nestedData: function(path, data) {
		if (!path) return data;
		if (!data) return data;
		if (Ext.isString(path)) path = path.split('.');
		var key = path.shift();
		if (key && key.length > 0) data = data[key];
		return path.length > 0 ? CFJS.nestedData(path, data) : data;
	},

	parseInt: function(value) {
		return parseInt(String(value).replace(/[\$,%]/g, ''), 10);
	},

	portalPath: function() {
		var path = (window.location.pathname||'').split('/'), last = path.pop();
		last = last || path.pop();
		return path.join('/') + '/';
	},

	resourcePath: function(path, poolName, packageName) {
		if (typeof path !== 'string') {
			poolName = path.pool;
			packageName = path.packageName;
			path = path.path;
		}
		var manifest = Ext.manifest,
			paths = (manifest && manifest.resources) || {},
			poolPath = paths[poolName],
			output = [];
		if (poolPath == null) {
			poolPath = paths.path;
			if (poolPath == null) poolPath = 'resources';
		}
		if (poolPath) output.push(poolPath);
		if (packageName) output.push(packageName);
		output.push(path);
		return output.join('/');
	},

	safeLevel: function(level) {
		return CFJS.safeParseInt(level >= 0 ? level : CFJS.shortMax);
	},

	safeParseInt: function(value, def) {
		return Ext.isNumber(value = CFJS.parseInt(value)) && !isNaN(value) ? value : def || 0;
	},

	setCookie: function (c_name, value, expires, path, domain, secure) {
		// set time, it's in milliseconds
		var today = new Date();
		today.setTime(today.getTime());
		if (expires) expires = expires * 1000 * 60;
		var expires_date = new Date(today.getTime() + (expires));
		document.cookie = c_name + "=" + escape(value) +
			((expires) 	? ";expires=" + expires_date.toGMTString() : "") +
			((path) 	? ";path=" + path : "") +
			((domain) 	? ";domain=" + domain : "") +
			((secure) 	? ";secure" : "");	
	},

	sharedResourcePath: function(path, packageName) {
		return CFJS.resourcePath(path, 'shared', packageName);
	},

	showMsg: function(options) {
		options = options || {};
		if (Ext.toolkit === 'modern') {
			options.message = options.message || options.msg;
			delete options.animateTarget;
			delete options.msg;
		}
		return Ext.Msg.show(options);
	},

	toObject: function(name, value) {
		var o = {};
		o[name] = value;
		return o;
	},

	updateConfig: function(config) {
		var me = CFJS, doc = document, msg,
			cfg = me.config = me.apply(me.config || {}, config),
			locale = cfg.locale,
			name = ((Ext.microloaderTags || {}).locale || me.findLocale(locale.cookie)).substr(0, 2);
		if (locale[name]) locale.name = name;
		doc.documentElement.lang = locale.name;
		msg = locale[locale.name];
		doc.title = msg.companyName + ' | ' + msg.apptitle;
		doc.head.appendChild(me.createTag('link', { rel: 'shortcut icon', href: me.config.favicon, type: 'image/x-icon' }));
		me.setCookie(locale.cookie, locale.name, 86400, me.portalPath());
		me.loadExternalScripts(me.config.externalScripts);
		me.applyUniversalAnalytics(me.config.uaid);
	}
	
});
CFJS.config = Ext.applyIf(CFJS.config || {}, {
	buildInfo	: { startDeveloping: '2010' },
	locale		: { cookie: 'LOCALE', name: 'en' }
});
