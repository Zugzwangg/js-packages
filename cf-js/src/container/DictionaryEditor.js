Ext.define('CFJS.container.DictionaryEditor', {
	extend			: 'Ext.Mixin',
	mixinConfig		: {
		id		: 'dictionaryeditor',
		before	: {
//			afterRender		: 'afterRender',
			initComponent	: 'initComponent'
		}
	},
	editingPlugin	: { $value: null, lazy: true },
	gridConfig		: { $value: null, lazy: true },
	
	initComponent: function() {
		var me = this,
			editingPlugin = Ext.apply({ ptype: 'dictionaryediting' }, me.editingPlugin);
		me.grid = me.gridConfig ? Ext.create(me.gridConfig) : null;
		me.items = me.items || [];
		
		if (me.grid) me.items.push(me.grid);
		if (editingPlugin && !editingPlugin.isPlugin) me.editingPlugin = me.addPlugin(editingPlugin);
	},
	
	lookupGrid: function() {
		return this.grid;
	},

	lookupStore: function() {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	},
	
	reloadStore: function() {
		var store = this.lookupStore();
		if (store && store.isStore) store.reload();
	},
	
	startEdit: function(record) {
		var me = this, editingPlugin = me.editingPlugin;
		if (editingPlugin) {
			if (!record) {
				var grid = me.lookupGrid(), selection = grid ? grid.getSelection() : null;
				record = selection && selection.length > 0 ? selection[0] : false;
			}
			editingPlugin.startEdit(record !== true ? record : null);
		}
	}
	
});
