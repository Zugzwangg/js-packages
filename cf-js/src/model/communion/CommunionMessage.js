Ext.define('CFJS.model.communion.CommunionMessage', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [
		'CFJS.model.Attachment',
		'CFJS.model.NamedModel',
		'CFJS.model.UserInfo',
		'CFJS.schema.Communion',
		'CFJS.schema.Document'
	],
    dictionaryType	: 'communion_message',
	schema			: 'communion',
	fields			: [
		{ name: 'author',		type: 'model',		critical: true,	entityName: 'CFJS.model.UserInfo' },
		{ name: 'description',	type: 'string',		critical: true },
		{ name: 'created',		type: 'date',		critical: true },
		{ name: 'persons',		type: 'array',		critical: true, item: { entityName: 'CFJS.model.NamedModel', mapping: 'named_entity' } },
		{ name: 'customers',	type: 'array',		critical: true, item: { entityName: 'CFJS.model.NamedModel', mapping: 'named_entity' } },
		{ name: 'attachments',	type: 'array',		critical: true, item: { entityName: 'CFJS.model.Attachment', mapping: 'attachment' } }
//		{ name: 'viewed',		type: 'boolean',	convert: function (value, record) { return record.isViewed(); } }
	],
	
	validators: {
		customers: function(value, record) { return !Ext.isEmpty(value) || !Ext.isEmpty(record.get('persons')); },
		description: function(value, record) { return !Ext.isEmpty(value); },
		persons: function(value, record) { return !Ext.isEmpty(value) || !Ext.isEmpty(record.get('customers')); }
	},
	
	asRecipient: function(userInfo) {
		userInfo = userInfo || CFJS.lookupViewportViewModel().userInfo();
		if (userInfo && userInfo.type && userInfo.id > 0) {
			var recipients = Ext.Array.from(this.get(userInfo.type + 's')), i = 0;
			for (; i < recipients.length; i++) {
				if (recipients[i].id === userInfo.id) return new CFJS.model.NamedModel(recipients[i]);
			}
		}
	},
	
	isViewed: function(userInfo) {
		var asRecipient = this.asRecipient(userInfo);
		return (asRecipient && asRecipient.isEntity && !Ext.isEmpty(asRecipient.getPropertyValue('viewed'))) === true;
	}

}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});