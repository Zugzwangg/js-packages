Ext.define('CFJS.model.communion.LocalTask', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [
		'CFJS.model.NamedModel',
		'CFJS.model.communion.DiscussionMessage',
		'CFJS.model.communion.LocalTaskStatus',
		'CFJS.schema.Communion',
		'CFJS.schema.Document'
	],
    dictionaryType	: 'local_task',
	schema			: 'communion',
	fields			: [
		{ name: 'producer',		type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'messageId',	reference: { parent: 'DiscussionMessage' } },
		{ name: 'description',	type: 'string',		critical: true },
		{ name: 'numerical',	type: 'boolean',	critical: true },
		{ name: 'deadline',		type: 'date',		critical: true },
		{ name: 'created',		type: 'date',		critical: true },
		{ name: 'status',		type: 'model',		critical: true,	entityName: 'CFJS.model.communion.LocalTaskStatus' },
		{ name: 'statuses',		type: 'array',		critical: true, item: { entityName: 'CFJS.model.communion.LocalTaskStatus', mapping: 'local_task_status' } }
	]
});