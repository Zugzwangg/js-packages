Ext.define('CFJS.model.communion.LocalTaskStatus', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [
		'CFJS.model.NamedModel',
		'CFJS.model.communion.DiscussionMessage',
		'CFJS.model.communion.LocalTaskStatus',
		'CFJS.schema.Communion',
		'CFJS.schema.Document'
	],
    dictionaryType	: 'local_task_status',
	schema			: 'communion',
	fields			: [
		{ name: 'taskId',	allowBlank: false,	reference: { parent: 'LocalTask' } },
		{ name: 'executor',	type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'goal',		type: 'number',		critical: true },
		{ name: 'progress',	type: 'number',		critical: true },
		{ name: 'updated',	type: 'date',		critical: true }
	]
});