Ext.define('CFJS.model.communion.Author', {
    extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Communion', 'CFJS.schema.Document' ],
	schema			: 'communion',
	fields			: [
		{ name: 'photo',	type: 'string',	defaultValue: CFJS.sharedResourcePath('images/default_person_photo.png', 'cf-js') }
	],
	
	utype: function() {
		var data = this.data;
		return data.type || (data.post ? 'person' : 'customer');
	}

});