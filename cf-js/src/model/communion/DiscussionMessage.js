Ext.define('CFJS.model.communion.DiscussionMessage', {
    extend			: 'CFJS.model.communion.CommunionMessage',
    requires		: [ 'CFJS.model.communion.Discussion' ],
    dictionaryType	: 'discussion_message',
	fields			: [
		{ name: 'discussionId',	reference: { parent: 'Discussion' } },
		{ name: 'parentId',		reference: { parent: 'DiscussionMessage' } },
		{ name: 'priority',		type: 'string',		critical: true, defaultValue: 'NORMAL' },
		{ name: 'hasChilds',	type: 'boolean',	persist: false },
		{ name: 'hasTasks',		type: 'boolean',	persist: false },
		{ name: 'sendEmail',	type: 'boolean',	critical: true },
		{ name: 'readBy',		type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'isRoot',		type: 'boolean',	calculate: function (data) { return !data.parentId && !data.discussionId; } },
		{ name: 'colorCls', 	type: 'string', 
			calculate: function(data) {
				return CFJS.schema.Communion.priorityColorCls(data.priority);
			}
		}
	]

}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});