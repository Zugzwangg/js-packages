Ext.define('CFJS.model.communion.Discussion', {
    extend			: 'CFJS.model.communion.CommunionMessage',
    dictionaryType	: 'discussion',
    isDiscussion	: true,
	fields			: [
		{ name: 'subject',	type: 'string',		critical: true },
		{ name: 'shared',	type: 'boolean',	critical: true },
		{ name: 'finished',	type: 'date',		critical: true }
	],
	validators: {
		subject: function(value, record) { return !Ext.isEmpty(value) }
	},
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});