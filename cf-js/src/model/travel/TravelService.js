Ext.define('CFJS.model.travel.TravelService', {
    extend			: 'CFJS.model.financial.Service',
	requires		: [ 'CFJS.model.dictionary.City' ],
	isTravel		: true,
    dictionaryType	: 'travel_service',
	fields			: [
		{ name: 'sector', 				type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 6, name: 'Туризм' } },
		{ name: 'city', 				type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.City' },
		{ name: 'additionalServices',	type: 'string' },
		{ name: 'dateFrom',				type: 'date',	critical: true },
		{ name: 'dateTo',				type: 'date',	critical: true }
	]
});