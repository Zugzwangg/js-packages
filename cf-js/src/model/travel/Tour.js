Ext.define('CFJS.model.travel.Tour', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Travel', 'CFJS.model.dictionary.Currency' ],
	dictionaryType	: 'tour',
	schema			: 'travel',
	service			: 'iss',
	fields			: [
		{ name: 'hotel',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'roomType',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'food',			type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'checkIn',		type: 'date',	critical: true },
		{ name: 'duration',		type: 'int',	critical: true },
		{ name: 'adultCount',	type: 'int',	critical: true },
		{ name: 'childCount',	type: 'int',	critical: true },
		{ name: 'price',		type: 'number',	critical: true },
		{ name: 'currency',		type: 'model',	critical: true,	entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } }
	]
});