Ext.define('CFJS.model.airline.AirlineRoute', {
    extend			: 'CFJS.model.RouteModel',
	requires		: [ 'CFJS.schema.Airline', 'CFJS.model.airline.AirlineSegment', 'CFJS.model.airline.Airport' ],
    dictionaryType	: 'airline_route',
	schema			: 'airline',
	fields			: [
		{ name: 'from',		type: 'model', critical: true, entityName: 'CFJS.model.airline.Airport' },
		{ name: 'to',		type: 'model', critical: true, entityName: 'CFJS.model.airline.Airport' },
		{ name: 'segments',	type: 'array', critical: true, item: { entityName: 'CFJS.model.airline.AirlineSegment', mapping: 'airline_segment' } }
	]
	
});