Ext.define('CFJS.model.airline.Airport', {
    extend	: 'CFJS.model.airline.IataModel',
	requires: [ 'CFJS.schema.Airline' ],
	schema	: 'airline'
});