Ext.define('CFJS.model.airline.AirlineSegment', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Airline', 'CFJS.model.airline.AirlineSupplier', 'CFJS.model.airline.AirlineTariff', 'CFJS.model.airline.Airport' ],
    dictionaryType	: 'airline_segment',
	schema			: 'airline',
	fields			: [
		{ name: 'carrier',			type: 'model',	critical: true, entityName: 'CFJS.model.airline.AirlineSupplier' },
		{ name: 'marketingAirline',	type: 'model',	persist: false,	entityName: 'CFJS.model.airline.AirlineSupplier' },
		{ name: 'from',				type: 'model',	critical: true, entityName: 'CFJS.model.airline.Airport' },
		{ name: 'terminalFrom',		type: 'string',	critical: true },
		{ name: 'departure',		type: 'date',	critical: true },
		{ name: 'to',				type: 'model',	critical: true, entityName: 'CFJS.model.airline.Airport' },
		{ name: 'terminalTo',		type: 'string',	critical: true },
		{ name: 'arrive',			type: 'date',	critical: true },
		{ name: 'duration',			type: 'string',	critical: true },
		{ name: 'tripNumber',		type: 'string' },
		{ name: 'ticketCategory',	type: 'string' },
		{ name: 'seatNumber',		type: 'string' },
		{ name: 'tariffs',			type: 'array',	item: { entityName: 'CFJS.model.airline.AirlineTariff', mapping: 'airline_tariff' }},
		// data from AirlineTariff
		{ name: 'tariff',			type: 'model',	persist: false,	entityName: 'CFJS.model.airline.AirlineTariff' },
		{ name: 'tariffCode',		type: 'string',	persist: false },
		{ name: 'tariffTourCode',	type: 'string',	persist: false },
		{ name: 'tariffBaggage',	type: 'string',	persist: false },
		{ name: 'tariffState',		type: 'string',	persist: false }
	],
	
	getData: function (options) {
		var me = this, 
			ret = me.callParent(arguments),
			service = options ? options.service : null, 
			codes = {
				code	: ret.tariffCode,
				tourCode: ret.tariffTourCode,
				baggage	: ret.tariffBaggage,
				state	: ret.tariffState
			};
		if (service && service.isModel) {
			var tariff = me.fieldsMap['tariff'].serialize(ret.tariff || codes, me),
				consumer = service.modified ? service.modified.consumer : null,
				items = ret.tariffs || [], 
				tariffs = [], 
				i = 0, item;
			tariff.consumer = service.get('consumer');
			if (me.checkTariff(tariff)) tariffs.push(tariff);
			for (i = 0; i < items.length; i++) {
				item = items[i];
				if (me.checkTariff(item) 
						&& !me.equalConsumer(item, consumer) 
						&& !me.equalConsumer(item, tariff.consumer)) tariffs.push(item);
			}
			ret.tariffs = me.fieldsMap['tariffs'].serialize(tariffs, me);
			delete ret.tariff;
			delete ret.tariffCode;
			delete ret.tariffTourCode;
			delete ret.tariffBaggage;
			delete ret.tariffState;
		} else if (ret.tariff) ret.tariff = Ext.apply(ret.tariff, codes);
		return ret;
	},
	
	getTariff: function(consumer) {
		if (consumer && consumer.id) {
			var tariffs = this.get('tariffs') || [], i, tariff;
			for (i = 0; i < tariffs.length; i++) {
				if (this.equalConsumer(tariff = tariffs[i], consumer)) return tariff;
			}
		}
	},

	setConsumer: function(consumer) {
		var me = this, tariff;
		if (!consumer || consumer.imported) {
			me.set({
				tariff			: undefined,
				tariffCode		: undefined,
				tariffTourCode	: undefined,
				tariffBaggage	: undefined,
				tariffState		: undefined
			});
			return;
		}
		if (me.equalConsumer(tariff = me.get('tariff'), consumer)) return;
		tariff = me.fieldsMap['tariff'].convert(me.getTariff(consumer) || { consumer: consumer }, me);
		me.set({
			tariff			: tariff,
			tariffCode		: tariff.code,
			tariffTourCode	: tariff.tourCode,
			tariffBaggage	: tariff.baggage,
			tariffState		: tariff.state
		}, { commit: true });
	},
	
	privates: {
		
		checkTariff: function(tariff) {
			return tariff && tariff.consumer 
				&& (!Ext.isEmpty(tariff.code) || !Ext.isEmpty(tariff.tourCode) || !Ext.isEmpty(tariff.baggage) || !Ext.isEmpty(tariff.state));
		},
		
		equalConsumer: function(tariff, consumer) {
			return consumer && consumer.id === ((tariff || {}).consumer || {}).id;
		}
		
	}
	
});