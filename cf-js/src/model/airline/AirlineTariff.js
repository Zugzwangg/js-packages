Ext.define('CFJS.model.airline.AirlineTariff', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Airline', 'CFJS.model.dictionary.Consumer' ],
    dictionaryType	: 'airline_tariff',
	schema			: 'airline',
	fields			: [
		{ name: 'code',		type: 'string',	critical: true },
		{ name: 'tourCode',	type: 'string',	critical: true },
		{ name: 'baggage',	type: 'string',	critical: true },
		{ name: 'state',	type: 'string',	critical: true },
		{ name: 'consumer',	type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.Consumer' },
		{ name: 'segment',	type: 'auto',	critical: true, reference: 'AirlineSegment', allowBlank: false }
	],
	hasOne			: [
		{ name: 'Segment',		model: 'AirlineSegment',	associationKey: 'segment' }
	]
});