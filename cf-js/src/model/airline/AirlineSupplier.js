Ext.define('CFJS.model.airline.AirlineSupplier', {
    extend			: 'CFJS.model.LocalizableModel',
	requires		: [ 'CFJS.schema.Airline' ],
	dictionaryType	: 'airline_carrier',
	schema			: 'airline',
	fields			: [
		{ name: 'logo',				type: 'string' },
		{ name: 'href',				type: 'string' },
		{ name: 'iataCode', type: 'string', critical: true }
	]
});