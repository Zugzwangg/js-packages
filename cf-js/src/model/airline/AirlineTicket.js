Ext.define('CFJS.model.airline.AirlineTicket', {
    extend			: 'CFJS.model.TicketModel',
    requires		: [ 'CFJS.model.airline.AirlineRoute' ],
    isAirline		: true,
    dictionaryType	: 'airline_ticket',
	fields			: [ 
		{ name: 'sector',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 11, name: 'Авіаквиток' }}, 
		{ name: 'route', 		type: 'model',	critical: true,	entityName: 'CFJS.model.airline.AirlineRoute' },
		{ name: 'consumerType',	type: 'string', critical: true, defaultValue: 'ADT' },
		{ name: 'type',			type: 'string', critical: true, defaultValue: 'TRAVEL' }
	]
});