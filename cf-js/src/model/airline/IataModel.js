Ext.define('CFJS.model.airline.IataModel', {
    extend	: 'CFJS.model.LocalizableModel',
	fields	: [
		{ name: 'iataCode', type: 'string', critical: true },
		{ name: 'extra', 	type: 'string' }
	]
});