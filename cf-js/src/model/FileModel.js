Ext.define('CFJS.model.FileModel', {
	extend		: 'Ext.data.Model',
	idProperty	: 'name',
	fields		: [ 
		{ name: 'name', 		type: 'string', 	critical: true }, 
		{ name: 'path', 		type: 'string',		critical: true },
		{ name: 'message',		type: 'string',		critical: true },
		{ name: 'isFile',		type: 'boolean',	critical: true, defaultValue: true },
		{ name: 'canDelete',	type: 'boolean',	critical: true, defaultValue: true },
		{ name: 'size',			type: 'int',		critical: true },
		{ name: 'position',		type: 'int',		critical: true },
		{ name: 'date',			type: 'date',		critical: true },
		{ name: 'state',		type: 'string',		critical: true, defaultValue: 'OK' },
		{ name: 'absolutePath', type: 'string', 	calculate: function (data) { return data.path + '/' + data.name; } },
		{ name: 'ext',			type: 'string', 	calculate: function (data) { if (data.isFile) { return CFJS.schema.Public.extension(data.name); } } },
		{ name: 'iconCls',		type: 'string',
			calculate: function (data) {
				return data.isFile ? CFJS.schema.Public.iconCls(data.ext) : CFJS.UI.iconCls.FOLDER;
			}
		}
	],
	proxy				: {
		type			: 'json.cfapi',
		api				: {	destroy: 'remove' },
		reader			: {
			type: 'json.cfapi', 
			rootProperty: 'files.file'
		},
		service			: 'resources'
	},

	buildRequest: function(service, method, payload, payloadType, options) {
		var me = this, api = CFJS.Api, request,
			cnt = api.controller(service = service || me.getProxy().getService());
		if (cnt && cnt.url) {
			request = CFJS.apply({
				url			: cnt.url,
				method		: 'POST',
				service		: service,
				signParams	: true,
				params		: { method: [cnt.method, method].join('.') },
			}, options);
			if (payload) request[api.payloadFormat() + 'Data'] = api.buildPayload(payload, payloadType);
		}
		return request;
	},

	download: function(service, params) {
		var me = this,
			callback = function(options, success, response) {
				if (success) CFJS.downloadAsBlob(response, me.get('name'));
				else CFJS.errorAlert('Error downloading data', { code: response.status, text: response.statusText });
			},
			request = me.buildRequest(service, 'download', null, null, { binary: true, callback: callback, method: 'GET', params: Ext.apply({ path : me.get('name') }, params) });
		if (request) CFJS.Api.request(request);
	},
	
	downloadUrl: function(service, params) {
		var me = this, api = CFJS.Api,
			cnt = api.controller(service = service || me.getProxy().getService());
		if (cnt && cnt.url) {
			return api.buildUrl({
				url			: cnt.url,
				service		: service,
				signParams	: true,
				params		: Ext.apply({ method: 'download', path: me.get('name') }, params)
			});
		}
	},
	
	canDelete: function(userInfo) {
		return this.isAuthor(userInfo) && this.get('canDelete'); 
	},

	isAuthor: function(userInfo) {
		return true; 
	}

});