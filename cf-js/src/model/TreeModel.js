Ext.define('CFJS.model.TreeModel', {
	extend	: 'CFJS.model.NamedModel',
	requires: [ 'Ext.data.NodeInterface' ],
	mixins	: [ 'Ext.mixin.Queryable' ],
	fields	: [{ name: 'parentId', type: 'int', allowNull: true }],
	
	getRefItems: function() {
		return this.childNodes;
	},

	getRefOwner: function() {
		return this.parentNode;
	},

	statics: {
		defaultProxy: 'memory'
	}
	
}, function () {
	Ext.data.NodeInterface.decorate(this);
});