Ext.define('CFJS.model.CodeNamePair', {
	extend		: 'Ext.data.Model',
	requires	: [ 'CFJS.data.field.*' ],
	idProperty	: 'code',
	fields		: [
		{ name: 'code',	type: 'string', critical: true },
		{ name: 'name',	type: 'string', critical: true }
	],
	proxy	: 'memory'
});