Ext.define('CFJS.model.bus.BusRoute', {
    extend			: 'CFJS.model.RouteModel',
	requires		: [  'CFJS.schema.Bus', 'CFJS.model.bus.BusStation' ],
    dictionaryType	: 'bus_route',
	schema			: 'bus',
	fields			: [
		{ name: 'tripNumber',	type: 'string',	critical: true },
		{ name: 'carrierName',	type: 'string',	critical: true },
		{ name: 'from',			type: 'model',	critical: true, entityName: 'CFJS.model.bus.BusStation' },
		{ name: 'to',			type: 'model',	critical: true, entityName: 'CFJS.model.bus.BusStation' }
	]
});