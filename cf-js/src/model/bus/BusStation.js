Ext.define('CFJS.model.bus.BusStation', {
    extend			: 'CFJS.model.LocalizableModel',
	requires		: [ 'CFJS.schema.Bus', 'CFJS.model.NamedModel' ],
    dictionaryType	: 'bus_station',
	schema			: 'bus',
	fields			: [
		{ name: 'busCode', 	type: 'string',	critical: true },
		{ name: 'city', 	type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});