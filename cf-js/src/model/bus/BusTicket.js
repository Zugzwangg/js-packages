Ext.define('CFJS.model.bus.BusTicket', {
    extend			: 'CFJS.model.TicketModel',
    isBus			: true,
    dictionaryType	: 'bus_ticket',
	fields			: [
		{ name: 'sector', 		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 31, name: 'Автобусний квиток' } },
		{ name: 'seatNumber', 	type: 'string' },
		{ name: 'tariff', 		type: 'string' }
	]
});