Ext.define('CFJS.model.bus.BusSupplier', {
    extend			: 'CFJS.model.dictionary.Company',
	requires		: [ 'CFJS.schema.Bus' ],
	dictionaryType	: 'bus_carrier',
	schema			: 'bus'
});