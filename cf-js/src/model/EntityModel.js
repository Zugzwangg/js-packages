Ext.define('CFJS.model.EntityModel', {
	extend		: 'Ext.data.Model',
	requires	: [ 'CFJS.schema.Public', 'CFJS.data.field.*' ],
	config		: {
		dictionaryType	: undefined,
		service			: 'iss'
	},
	schema		: 'public',
	fields		: [ { name: 'properties' } ],
	
	getDateProperty: function(field) {
		var value = this.getPropertyValue(field);
		return !value || Ext.isDate(value) ? value : new Date(Date.parse(value));
	},

	getFloatProperty: function(field) {
		var value = this.getPropertyValue(field);
		if (!Ext.isNumber(value)) value = parseFloat(String(value).replace(/[\$,%]/g, '')); 
		return isNaN(value) ? null : value;
	},

	getIntProperty: function(field) {
		var value = this.getPropertyValue(field);
		if (!Ext.isNumber(value)) value = parseFloat(String(value).replace(/[\$,%]/g, '')); 
		return isNaN(value) ? null : value;
	},
	
	getProperty: function(field) {
		var props = this.get('properties');
		return field && Ext.isObject(props) ? props['field_' + field] : null;
	},
	
	getPropertyValue: function(field, key) {
		var prop = this.getProperty(field);
		return prop ? prop[key || '$'] : null;
	},
	
	isAuthor: function(userInfo) {
		userInfo = userInfo || CFJS.lookupViewportViewModel().userInfo();
		var author = this.get('author') || {};
		return userInfo && author && userInfo.id === author.id && userInfo.type === author.type; 
	},
	
	setProperty: function(field, value) {
		if (!field) return;
		var me = this, properties = me.get('properties') || {};
		if (!Ext.isObject(properties)) properties = {};
		if (value) properties['field_' + field] = Ext.apply(properties['field_' + field] || { '@type': 'string' }, value );
		else delete properties['field_' + field];
		me.set('properties', Ext.clone(properties));
	},
	
	setPropertyValue: function(field, value) {
		this.setProperty(field, Ext.isObject(value) ? value : { $: value });
	}

});
