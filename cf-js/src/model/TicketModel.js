Ext.define('CFJS.model.TicketModel', {
	extend	: 'CFJS.model.financial.Service',
	fields	: [
		{ name: 'route', 			type: 'model',	critical: true, entityName: 'CFJS.model.RouteModel' },
		{ name: 'ticketCategory',	type: 'string',	critical: true,	defaultValue: 'FULL' }
	]
});