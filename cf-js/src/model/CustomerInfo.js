Ext.define('CFJS.model.CustomerInfo', {
	extend			: 'CFJS.model.NamedModel',
	idProperty		: 'customId',
	identifier		: 'negative',
	isCustomerInfo	: true,
	fields			: [{ name: 'type', type: 'string', critical: true }]
});