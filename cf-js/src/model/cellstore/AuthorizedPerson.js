Ext.define('CFJS.model.cellstore.AuthorizedPerson', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [
		'CFJS.model.NamedModel',
		'CFJS.model.cellstore.AuthorizedDocument',
		'CFJS.model.cellstore.Rent',
		'CFJS.schema.CellStore'
	],
    dictionaryType	: 'cell_authorized_person',
	schema			: 'cellstore',
	fields			: [
		{ name: 'document',		type: 'model',	critical: true, entityName: 'CFJS.model.cellstore.AuthorizedDocument' },
		{ name: 'customer',		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'rent',			type: 'model',	critical: true, entityName: 'CFJS.model.cellstore.Rent' }
	]
});