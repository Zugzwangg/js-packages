Ext.define('CFJS.model.cellstore.CompanyRent', {
	extend			: 'CFJS.model.cellstore.Rent',
    dictionaryType	: 'cell_company_rent',
	fields			: [
		{ name: 'company',	type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});