Ext.define('CFJS.model.cellstore.Cell', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.CellStore', 'CFJS.model.NamedModel', 'CFJS.model.cellstore.CellSize' ],
	schema			: 'cellstore',
	fields			: [
		{ name: 'number',		type: 'string',	critical: true },
		{ name: 'size',			type: 'model',	critical: true, entityName: 'CFJS.model.cellstore.CellSize' },
		{ name: 'cupboard',		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true },
		{ name: 'freeFrom',		type: 'date',	persist: false }
	]
});