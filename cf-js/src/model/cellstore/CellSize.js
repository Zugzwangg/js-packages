Ext.define('CFJS.model.cellstore.CellSize', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.CellStore' ],
    dictionaryType	: 'cell_size',
	schema			: 'cellstore',
	fields			: [
		{ name: 'name',			type: 'string',
			calculate: function(data) {
				var name = '' + data.height + 'x' + data.width + 'x' + data.depth;
				if (data.micro) name += ' (micro)';
				return name;  
			}
		},
		{ name: 'height',		type: 'int',		critical: true, defaultValue: 0 },
		{ name: 'width',		type: 'int',		critical: true, defaultValue: 0 },
		{ name: 'depth',		type: 'int',		critical: true, defaultValue: 0 },
		{ name: 'micro',		type: 'boolean',	critical: true, defaultValue: false },
		{ name: 'startDate',	type: 'date',		critical: true },
		{ name: 'endDate',		type: 'date',		critical: true }
	]
});