Ext.define('CFJS.model.cellstore.CellBrowsing', {
	extend	: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.CellStore', 'CFJS.model.orgchart.Post', 'CFJS.model.cellstore.AuthorizedPerson' ],
    dictionaryType	: 'cell_browsing',
	schema			: 'cellstore',
	fields			: [
		{ name: 'post',			type: 'model',	critical: true, entityName: 'CFJS.model.orgchart.Post' },
		{ name: 'person',		type: 'model',	critical: true, entityName: 'CFJS.model.cellstore.AuthorizedPerson' },
		{ name: 'stampNumber',	type: 'string',	critical: true },
		{ name: 'stampPhoto',	type: 'string',	critical: true },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true },
		{ name: 'url',			type: 'string',
			calculate: function(data) {
				return data.stampPhoto === 'N/A' ? null 
					: CFJS.Api.buildUrl({
						service		: 'resources',
						signParams: true,
						params		: {
							method	: 'file.download',
							path	: data.stampPhoto
						}
					});
			}
		}
	]
});