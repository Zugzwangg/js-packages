Ext.define('CFJS.model.cellstore.CustomerRent', {
	extend			: 'CFJS.model.cellstore.Rent',
    dictionaryType	: 'cell_customer_rent',
	fields			: [
		{ name: 'customer',	type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});