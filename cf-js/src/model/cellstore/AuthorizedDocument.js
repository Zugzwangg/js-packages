Ext.define('CFJS.model.cellstore.AuthorizedDocument', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.CellStore' ],
    dictionaryType	: 'cell_authorized_document',
	schema			: 'cellstore',
	fields			: [
		{ name: 'type',			type: 'string',	critical: true },
		{ name: 'link',			type: 'string',	critical: true },
		{ name: 'description',	type: 'string',	critical: true },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true },
		{ name: 'iconCls',		type: 'string',	
			calculate: function(data) {
				switch ((data.type||'').toUpperCase()) {
					case 'DOCUMENT'	: return CFJS.UI.iconCls.BOOKMARK;
					case 'FILE'		: return CFJS.UI.iconCls.FILE_TEXT;
					case 'URL'		: return CFJS.UI.iconCls.LINK;
					default			: return CFJS.UI.iconCls.FILE;
				}
			}
		},
		{ name: 'url',		type: 'string',	
			calculate: function(data) {
				var type = data.type, link = data.link;
				switch (type || '') {
					case 'URL': return link;
					case 'DOCUMENT': return CFJS.Api.buildUrl({
						service		: 'svd',
						signParams: true,
						params		: {
							method	: 'preview',
							document: link,
							template: 'template'
						}
					});
					case 'FILE': return CFJS.Api.buildUrl({
						service		: 'resources',
						signParams: true,
						params		: {
							method	: 'file.download',
							path	: link
						}
					});
					default:break;
				}
			}
		}
	]

});