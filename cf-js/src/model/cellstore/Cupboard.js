Ext.define('CFJS.model.cellstore.Cupboard', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.CellStore', 'CFJS.model.cellstore.Cell' ],
    dictionaryType	: 'cell_cupboard',
	schema			: 'cellstore',
	fields			: [
		{ name: 'storage',		type: 'int',	critical: true, defaultValue: 0 },
		{ name: 'cells',		type: 'array',	item: { entityName: 'CFJS.model.cellstore.Cell', mapping: 'cell' }},
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true }
	]
});