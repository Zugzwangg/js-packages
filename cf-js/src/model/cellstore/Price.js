Ext.define('CFJS.model.cellstore.Price', {
	extend	: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.CellStore', 'CFJS.model.cellstore.CellSize' ],
    dictionaryType	: 'cell_price',
	schema			: 'cellstore',
	fields			: [
		{ name: 'size',		type: 'model',	critical: true, entityName: 'CFJS.model.cellstore.CellSize' },
		{ name: 'period',	type: 'int',	critical: true, defaultValue: 0 },
		{ name: 'amount',	type: 'number',	critical: true, defaultValue: 0 },
		{ name: 'created',	type: 'date',	persist: false },
		{ name: 'updated',	type: 'date',	persist: false }
	]
});