Ext.define('CFJS.model.cellstore.Rent', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.CellStore', 'CFJS.model.NamedModel', 'CFJS.model.cellstore.Cell' ],
    dictionaryType	: 'cell_rent',
    isRent			: true,
	schema			: 'cellstore',
	fields			: [
		{ name: 'cell',			type: 'model',	critical: true, entityName: 'CFJS.model.cellstore.Cell' },
		{ name: 'microCell',	type: 'model',	entityName: 'CFJS.model.cellstore.Cell' },
		{ name: 'amount',		type: 'number',	critical: true, defaultValue: 0 },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true },
		{ name: 'name',			type: 'string', 
			convert: function(val, rec) {
				var cell = rec.get('cell'),
					format = Ext.Date.format, pattern = Ext.Date.patterns.NormalDate,
					name = [format(rec.get('startDate'), pattern),'-',format(rec.get('endDate'), pattern)] ;
				if (cell) {
					name.splice(0, 0, cell.number);
					if (cell.cupboard && cell.cupboard.id > 0) name.splice(1, 0, '(' + cell.cupboard.name + ')');
				}
				return name.join(' ');
			}
		}
	],
	
	buildPrintUrl: function(method, service) {
		var me = this,
			options = {
				service		: service || 'iss',
				signParams	: true,
				params		: {
					method	: method || 'print.service',
					type	: me.get('serviceType'),
					service	: me.getId(),
					locale	: CFJS.getLocale()
				}
			};
		return CFJS.Api.buildUrl(options);
	},
	
	print: function(method, service) {
		var url = this.buildPrintUrl(method, service);
		if (!Ext.isEmpty(url)) window.open(url, '_blank');
	}

});