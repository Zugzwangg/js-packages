Ext.define('CFJS.model.ExtApp', {
	extend			: 'CFJS.model.NamedModel',
    dictionaryType	: 'external_application',
	fields			: [
		{ name: 'organization',			type: 'string',	allowNull: true },
		{ name: 'unitName',				type: 'string',	allowNull: true },
		{ name: 'publicKey', 			type: 'string',	critical: true },
		{ name: 'certificateNumber', 	type: 'int',	critical: true },
		{ name: 'active',				type: 'boolean' },
		{ name: 'resetKeys', 			type: 'boolean' }
	]
});