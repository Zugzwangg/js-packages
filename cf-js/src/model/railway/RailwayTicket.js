Ext.define('CFJS.model.railway.RailwayTicket', {
    extend			: 'CFJS.model.TicketModel',
    requires		: [ 'CFJS.model.railway.RailwayRoute' ],
    isRailway		: true,
    dictionaryType	: 'railway_ticket',
	fields			: [
		{ name: 'sector', 			type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 21, name: 'Залізничний квиток' } },
		{ name: 'route', 			type: 'model',	critical: true,	entityName: 'CFJS.model.railway.RailwayRoute' },
		{ name: 'type',				type: 'string', critical: true,	defaultValue: 'TRAVEL' },
		{ name: 'wagonNumber', 		type: 'string' },
		{ name: 'seatNumber', 		type: 'string' },
		{ name: 'service',	 		type: 'string' },
		{ name: 'payDate', 			type: 'date' }
	],
	hasOne: [
		{ name: 'Route', model: 'CFJS.model.railway.RailwayRoute', associationKey: 'route' }
	]

});