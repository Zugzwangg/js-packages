Ext.define('CFJS.model.railway.RailwayStation', {
    extend			: 'CFJS.model.LocalizableModel',
	requires		: [ 'CFJS.schema.Railway', 'CFJS.model.NamedModel' ],
    dictionaryType	: 'railway_station',
	schema			: 'railway',
	fields			: [
		{ name: 'railwayCode', 	type: 'string', critical: true },
		{ name: 'city', 		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'railway', 		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});