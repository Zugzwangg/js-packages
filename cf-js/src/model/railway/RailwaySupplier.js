Ext.define('CFJS.model.railway.RailwaySupplier', {
    extend			: 'CFJS.model.dictionary.Company',
	requires		: [ 'CFJS.schema.Railway' ],
	dictionaryType	: 'railway_carrier',
	schema			: 'railway',
	fields			: [{ name: 'railwayCode', type: 'string', critical: true }]
});