Ext.define('CFJS.model.railway.RailwayRoute', {
    extend			: 'CFJS.model.RouteModel',
	requires		: [ 'CFJS.schema.Railway', 'CFJS.model.railway.RailwayStation' ],
    dictionaryType	: 'railway_route',
	schema			: 'railway',
	fields			: [
		{ name: 'number',		type: 'string',		critical: true },
		{ name: 'trainNumber',	type: 'string',		critical: true },
		{ name: 'from',			type: 'model',		critical: true, entityName: 'CFJS.model.railway.RailwayStation' },
		{ name: 'to',			type: 'model',		critical: true, entityName: 'CFJS.model.railway.RailwayStation' },
		{ name: 'electronic',	type: 'boolean',	critical: true }
	]
});