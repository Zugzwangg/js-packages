Ext.define('CFJS.model.railway.Railway', {
    extend	: 'CFJS.model.LocalizableModel',
	requires: [ 'CFJS.schema.Railway', 'CFJS.model.NamedModel' ],
	schema	: 'railway',
	fields	: [
		{ name: 'railwayCode', 	type: 'string', critical: true },
		{ name: 'country', 		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'company', 		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});