Ext.define('CFJS.model.Attachment', {
	extend		: 'Ext.data.Model',
	requires	: [ 'CFJS.data.field.*' ],
	idProperty	: 'link',
	fields		: [
		{ name: 'type',		type: 'string',	critical: true },
		{ name: 'name',		type: 'string',	critical: true },
		{ name: 'link',		type: 'string',	critical: true },
		{ name: 'iconCls', 	type: 'string', 
			calculate: function(data) {
				return CFJS.model.Attachment.iconCls(data.type);
			}
		},
		{ name: 'url',		type: 'string',	
			calculate: function(data) {
				return CFJS.model.Attachment.buildUrl(data.type, data.link);
			}
		}
	],
	proxy: 'memory',

	statics: {
		
		buildUrl: function(type, link) {
			switch ((type||'').toUpperCase()) {
				case 'DOCUMENT'	: return CFJS.Api.buildUrl({
					service		: 'svd',
					signParams	: true,
					params		: {
						method	: 'print',
						document: link,
						locale	: CFJS.getLocale(),
						template: 'template'
					}
				});
				case 'FILE'		: return CFJS.Api.buildUrl({
					service		: 'resources',
					signParams	: true,
					params		: {
						method	: 'download',
						type	: '',
						path	: link
					}
				});
				case 'URL'		:
				default			: return link;
			}

		},
		
		iconCls	: function (type, link) {
			switch ((type||'').toUpperCase()) {
				case 'DOCUMENT'	: return CFJS.UI.iconCls.BOOKMARK;
				case 'FILE'		: return CFJS.schema.Public.iconCls(CFJS.schema.Public.extension(link));
				case 'URL'		: return CFJS.UI.iconCls.LINK;
				default			: return CFJS.UI.iconCls.FILE_TEXT;
			}
		}
	
	}

});