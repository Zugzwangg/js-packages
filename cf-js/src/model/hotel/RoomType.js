Ext.define('CFJS.model.hotel.RoomType', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Hotel' ],
	dictionaryType	: 'room_type',
	schema			: 'hotel',
	service			: 'iss',
	fields			: [{ name: 'bedNumber', type: 'int', critical: true, defaultValue: 1 }]
});