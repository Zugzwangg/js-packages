Ext.define('CFJS.model.hotel.HotelComment', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Hotel' ],
	dictionaryType	: 'hotel_comment',
	schema			: 'hotel',
	service			: 'iss',
	fields			: [
		{ name: 'hotel',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'author',		type: 'string',	critical: true },
		{ name: 'description',	type: 'string' },
		{ name: 'created',		type: 'date',	persist: false }
	]
});