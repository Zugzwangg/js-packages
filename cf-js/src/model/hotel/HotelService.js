Ext.define('CFJS.model.hotel.HotelService', {
    extend			: 'CFJS.model.financial.Service',
	requires		: [ 'CFJS.model.hotel.Hotel', 'CFJS.model.hotel.RoomType' ],
	isHotel			: true,
    dictionaryType	: 'hotel_service',
	service			: 'iss',
	fields			: [
		{ name: 'sector', 		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 41, name: 'Розміщення в готелі (GTA, Content Inn, Академсервіс і т.д.)' } },
		{ name: 'hotel', 		type: 'model',	critical: true,	entityName: 'CFJS.model.hotel.Hotel' },
		{ name: 'food', 		type: 'model',	entityName: 'CFJS.model.NamedModel' },
		{ name: 'checkIn',		type: 'date',	critical: true },
		{ name: 'checkOut',		type: 'date',	critical: true },
		{ name: 'roomCategory', type: 'model',	entityName: 'CFJS.model.NamedModel' },
		{ name: 'roomType', 	type: 'model',	entityName: 'CFJS.model.hotel.RoomType' },
		{ name: 'roomNumber',	type: 'string' }
	]
});