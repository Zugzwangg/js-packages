Ext.define('CFJS.model.hotel.HotelFood', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Hotel' ],
	dictionaryType	: 'hotel_food',
	schema			: 'hotel',
	service			: 'iss',
	fields			: [{ name: 'sortOrder',	type: 'int' }]
});