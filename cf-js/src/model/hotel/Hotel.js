Ext.define('CFJS.model.hotel.Hotel', {
	extend	: 'CFJS.model.LocalizableModel',
	requires: [ 'CFJS.schema.Hotel', 'CFJS.model.NamedModel' ],
	schema	: 'hotel',
	service	: 'iss',
	fields	: [
 		{ name: 'name_en',					type: 'property',	critical: true, mapping: 'properties.field_name_en' },
 		{ name: 'name_uk',					type: 'property',	critical: true, mapping: 'properties.field_name_uk' },
 		{ name: 'name_ru',					type: 'property',	critical: true, mapping: 'properties.field_name_ru' },
		{ name: 'city',						type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'hotelClass',				type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'beach',					type: 'string', 	allowNull: true },
		{ name: 'fullDescription',			type: 'string', 	allowNull: true },
		{ name: 'shortDescription',			type: 'string', 	allowNull: true },
		{ name: 'latitude',					type: 'number', 	allowNull: true },
		{ name: 'location',					type: 'string', 	allowNull: true },
		{ name: 'longitude',				type: 'number', 	allowNull: true },
		{ name: 'images',					type: 'string', 	allowNull: true },
		{ name: 'room',						type: 'string', 	allowNull: true },
		{ name: 'rating',					type: 'number' },
		{ name: 'mainImage',				type: 'string', 	allowNull: true },
		{ name: 'mainImageUrl',				type: 'string', 	calculate: function(data) { return CFJS.model.hotel.Hotel.imageUrl(null, data.id, data.mainImage); } },
		{ name: 'roomTypes',				type: 'string', 	allowNull: true },
		{ name: 'food',						type: 'string', 	allowNull: true },
		{ name: 'entertainmentAndSports',	type: 'string', 	allowNull: true },
		{ name: 'forKids',					type: 'string', 	allowNull: true },
		{ name: 'territory',				type: 'string', 	allowNull: true },
		{ name: 'site',						type: 'string', 	allowNull: true },
		{ name: 'phone',					type: 'string', 	allowNull: true },
		{ name: 'fax',						type: 'string', 	allowNull: true },
		{ name: 'forToddlers',				type: 'string', 	allowNull: true }
	],
	
	statics: {
		
		imageUrl: function(service, id, image) {
			var api = CFJS.Api, cnt = id && image && api.controller(service = service || 'resources');
			if (cnt && cnt.url) {
				return api.buildUrl({
					url			: cnt.url,
					service		: service,
					signParams	: true,
					params		: { method: 'download', type: 'hotels/' + id, path: 'images/' + image }
				});
			}
		}

	}

});