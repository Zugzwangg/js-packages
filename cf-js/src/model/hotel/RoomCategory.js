Ext.define('CFJS.model.hotel.RoomCategory', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Hotel' ],
	dictionaryType	: 'room_category',
	schema			: 'hotel',
	service			: 'iss'
});