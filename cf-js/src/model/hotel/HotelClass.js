Ext.define('CFJS.model.hotel.HotelClass', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Hotel' ],
	dictionaryType	: 'hotel_class',
	schema			: 'hotel',
	service			: 'iss',
	fields			: [
		{ name: 'rating',		type: 'int' },
		{ name: 'sortOrder',	type: 'int' }
	]
});