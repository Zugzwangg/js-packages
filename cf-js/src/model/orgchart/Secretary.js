Ext.define('CFJS.model.orgchart.Secretary', {
    extend		: 'CFJS.model.EntityModel',
	requires	: [ 'CFJS.model.NamedModel', 'CFJS.schema.OrgChart' ],
	schema		: 'org_chart',
	fields		: [
		{ name: 'post',			type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'secretary',	type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true },
		{ name: 'created',		type: 'date',	persist: false },
		{ name: 'updated',		type: 'date',	persist: false }
	]
});