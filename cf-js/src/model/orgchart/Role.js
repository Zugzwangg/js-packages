Ext.define('CFJS.model.orgchart.Role', {
	extend	: 'CFJS.model.TreeModel',
	requires: [ 'CFJS.schema.OrgChart' ],
	isRole	: true,
	schema	: 'org_chart',
	fields	: [
		{ name: 'hasPosts', 		type: 'boolean',	defaultValue: false },
		{ name: 'accessType',		type: 'array', 		mapping: 'access_type' },
 		{ name: 'permissionLevel',	type: 'property',	critical: true,	mapping: 'properties.field_'+ CFJS.Api.getService() + 'permission_level' },
	],
	
	permissionLevel: function() {
		return CFJS.safeLevel(CFJS.parseInt(this.getPropertyValue(CFJS.Api.getService() + 'permission_level')));
	},
	
	isAdministrator: function() {
		return this.permissionLevel() === 0;
	}

});