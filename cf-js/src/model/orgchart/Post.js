Ext.define('CFJS.model.orgchart.Post', {
	extend	: 'CFJS.model.NamedModel',
	requires: [ 'CFJS.schema.OrgChart', 'CFJS.model.orgchart.Role', 'CFJS.model.orgchart.Unit' ],
	isPost	: true,
	schema	: 'org_chart',
	fields	: [
		{ name: 'active',			type: 'boolean',	persist: false },
		{ name: 'role',				type: 'model',		critical: true,		entityName: 'CFJS.model.orgchart.Role' },
		{ name: 'boss',				type: 'model',		allowNull: true, 	entityName: 'CFJS.model.NamedModel' },
		{ name: 'person',			type: 'model',		allowNull: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'unit',				type: 'model',		critical: true,		entityName: 'CFJS.model.orgchart.Unit' },
 		{ name: 'permissionLevel',	type: 'property',	critical: true,		mapping: 'properties.field_PERMISSION_LEVEL' },
		{ name: 'sectors',			type: 'array',		critical: true,		item: { entityName: 'CFJS.model.NamedModel', mapping: 'named_entity' } },
		{ name: 'fullName',			type: 'string',
			convert: function(v, rec) {
				var pName = (rec.get('person')||{}).name;
				return rec.get('name') + (!Ext.isEmpty(pName) ? ' (' + pName + ')' : '');
			}
		}
	],
	
	getPerson: function() {
		return this.get('person');
	},
	
	getRole: function() {
		var role = this.get('role');
		return role && !Ext.Object.isEmpty(role) ? new CFJS.model.orgchart.Role(role) : role;
	},

	getUnit: function() {
		return this.get('unit');
	},
	
	permissionLevel: function() {
		var me = this, level = CFJS.safeLevel(CFJS.parseInt(me.getPropertyValue('PERMISSION_LEVEL'))), role;
		if (level === CFJS.shortMax && (role = me.getRole()) && role.isRole) level = role.permissionLevel();
		return level;
	},
	
	isAdministrator: function() {
		return this.permissionLevel() === 0;
	}

});