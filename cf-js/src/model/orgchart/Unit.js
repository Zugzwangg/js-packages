Ext.define('CFJS.model.orgchart.Unit', {
	extend	: 'CFJS.model.TreeModel',
	requires: [ 'CFJS.schema.OrgChart' ],
	isUnit	: true,
	schema	: 'org_chart',
	fields	: [
		{ name: 'staffId', 	type: 'string',	critical: true },
		{ name: 'fullName',	type: 'string',	critical: true },
		{ name: 'address', 	type: 'string',	allowNull: true },
		{ name: 'edrpou', 	type: 'string',	critical: true },
		{ name: 'vat', 		type: 'string',	critical: true },
		{ name: 'tax',	 	type: 'string',	critical: true },
		{ name: 'bank', 	type: 'string',	allowNull: true },
		{ name: 'bankCode', type: 'string',	allowNull: true },
		{ name: 'account', 	type: 'string',	allowNull: true },
		{ name: 'phone', 	type: 'string',	allowNull: true },
		{ name: 'fax', 		type: 'string',	allowNull: true },
		{ name: 'director',	type: 'string',	allowNull: true },
		{ name: 'post', 	type: 'string',	allowNull: true },
		{ name: 'base', 	type: 'string',	allowNull: true },
		{ name: 'logo', 	type: 'string',	allowNull: true },
		{ name: 'services', type: 'int',	critical: true, defaultValue: 0 }
	],
	statics: {
		
		logoUrl: function(service, logo) {
			var api = CFJS.Api, cnt = logo && api.controller(service = service || 'resources');
			if (cnt && cnt.url) {
				return api.buildUrl({
					url			: cnt.url,
					service		: service,
					signParams	: true,
					params		: { method: 'download', type: 'unit/photos', path: logo }
				});
			}
			return logo;
		}

	}
});