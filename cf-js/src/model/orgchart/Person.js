Ext.define('CFJS.model.orgchart.Person', {
	extend		: 'CFJS.model.NamedModel',
	requires	: [ 'CFJS.model.UserInfo', 'CFJS.model.orgchart.Post', 'CFJS.schema.OrgChart' ],
	isPerson	: true,
	schema		: 'org_chart',
	fields		: [
		{ name: 'username',			type: 'string',		critical: true },
		{ name: 'password',			type: 'string',		critical: true },
		{ name: 'email',			type: 'string' },
		{ name: 'phone',			type: 'string' },
		{ name: 'post',				type: 'auto' },
		{ name: 'active',			type: 'boolean', 	defaultValue: false },
		{ name: 'resetPassword',	type: 'boolean', 	defaultValue: false },
		{ name: 'aliases', 			type: 'array',		item: { mapping: 'alias' }, critical: true },
		{ name: 'photo',			type: 'string', 	allowNull: true },
		{ name: 'sid',				type: 'string',		persist: false },
		{ name: 'error',			type: 'string',		persist: false },
		{ name: 'timeZoneOffset',	type: 'int',		persist: false },
		{ name: 'lastLogin',		type: 'date',		persist: false },
		{ name: 'created',			type: 'date',		persist: false },
		{ name: 'updated',			type: 'date',		persist: false },
		{ name: 'photoUrl',			type: 'string', 	calculate: function(data) { return CFJS.model.UserInfo.photoUrl(null, 'person', data.photo); } },
		{ name: 'fullName',			type: 'string', 	calculate: function(data) { return data.name; } }
	],
	
	getPost: function() {
		var post = this.get('post');
		return post && !Ext.Object.isEmpty(post) ? new CFJS.model.orgchart.Post(post) : post;
	},

	permissionLevel: function() {
		var post = this.getPost();
		return post && post.isPost ? post.permissionLevel() : null;
	},

	isAdministrator: function() {
		return this.permissionLevel() === 0;
	}

});