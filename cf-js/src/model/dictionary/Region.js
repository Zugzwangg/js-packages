Ext.define('CFJS.model.dictionary.Region', {
	extend	: 'CFJS.model.LocalizableModel',
	requires: [ 'CFJS.schema.Dictionary' ],
	schema	: 'dictionary',
	fields	: [
		{ name: 'name_en',		type: 'property',	critical: true,	mapping: 'properties.field_name_en' },
		{ name: 'name_uk',		type: 'property',	critical: true,	mapping: 'properties.field_name_uk' },
		{ name: 'name_ru',		type: 'property',	critical: true,	mapping: 'properties.field_name_ru' },
		{ name: 'country', 		type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'population',	type: 'int',		allowNull: true }
	]
});
