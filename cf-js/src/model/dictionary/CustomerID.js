Ext.define('CFJS.model.dictionary.CustomerID', {
	extend			: 'CFJS.model.IDModel',
	requires		: [ 'CFJS.schema.Dictionary' ],
    dictionaryType	: 'customer_ID',
	identifier		: { type: 'sequential', prefix: 'ID', seed: 100000 },
	schema			: 'dictionary'
});