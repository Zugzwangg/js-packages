Ext.define('CFJS.model.dictionary.Consumer', {
    extend	: 'CFJS.model.NamedModel',
	requires: [ 'CFJS.data.validator.TaxUA', 'CFJS.schema.Dictionary', 'CFJS.schema.Financial' ],
	schema	: 'dictionary',
	fields	: [ 
 		{ name: 'customer',		type: 'model',	entityName: 'CFJS.model.NamedModel' },
 		{ name: 'namePrefix',	type: 'string' },
 		{ name: 'lastName',		type: 'string',	critical: true },
 		{ name: 'middleName',	type: 'string' },
		{ name: 'birthday',		type: 'date' },
		{ name: 'document',		type: 'string' },
		{ name: 'tax',			type: 'string' },
		{ name: 'email',		type: 'string' },
		{ name: 'phone',		type: 'string' },
		{ name: 'zipCode',		type: 'string' },
 		{ name: 'address',		type: 'string' },
 		{ name: 'fullName',		type: 'string',
 			calculate: function(data) {
 				var fullName = [], fields = ['namePrefix', 'lastName', 'name', 'middleName'], i, v;
 				for (i = 0; i < fields.length; i++) {
 					if (!Ext.isEmpty(v = data[fields[i]])) fullName.push(v);
 				}
 				return fullName.join(' ');
 			}
 		}
	],
	
	statics: {
		
		transform: function(data, options) {
			if (options) return Ext.create('CFJS.model.dictionary.Consumer', data).getData(options);
			if (Ext.isObject(data)) {
				var ret = {},
					fieldsMap = CFJS.model.dictionary.Consumer.fieldsMap,
					field, name, value;
				if (data.isModel) data = data.getData();
				for (name in fieldsMap) {
	                field = fieldsMap[name];
                    if (field.persist) {
    	                value = data[name];
                        if (value && field.serialize) value = field.serialize(value, this);
                        ret[name] = value;
                    }
				}
		        return ret;
			} 
		}
	}
}, function() {
	Ext.data.schema.Schema.get('financial').addEntity(Ext.ClassManager.get(this.$className));
});
