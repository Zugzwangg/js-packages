Ext.define('CFJS.model.dictionary.Company', {
	extend			: 'CFJS.model.LocalizableModel',
	requires		: [ 'CFJS.data.validator.TaxUA', 'CFJS.model.dictionary.MemorableDate' ],
	schema			: 'dictionary',
	fields			: [
		{ name: 'author',			type: 'string',		critical: true },
		{ name: 'customerType',		type: 'enum' },
		{ name: 'supplierType',		type: 'enum' },
 		{ name: 'name_en',			type: 'property',	critical: true, mapping: 'properties.field_name_en' },
 		{ name: 'name_uk',			type: 'property',	mapping: 'properties.field_name_uk' },
 		{ name: 'name_ru',			type: 'property',	mapping: 'properties.field_name_ru' },
		{ name: 'fullName',			type: 'string' },
		{ name: 'address',			type: 'string' },
		{ name: 'registration',		type: 'string',		critical: true },
		{ name: 'vat',				type: 'string' },
		{ name: 'tax',				type: 'string' },
		{ name: 'phone',			type: 'string' },
		{ name: 'fax',				type: 'string' },
		{ name: 'email',			type: 'string' },
		{ name: 'logo',				type: 'string' },
		{ name: 'href',				type: 'string' },
		{ name: 'notes',			type: 'string' },
		{ name: 'parent',			type: 'auto' }, 
		{ name: 'country',			type: 'auto' }, 
		{ name: 'unit',				type: 'auto' }, 
		{ name: 'memorableDates', 	type: 'array',		critical: true, item: { entityName: 'CFJS.model.dictionary.MemorableDate', mapping: 'memorable_date' }}
	],
	validators: { tax: { type: 'taxUA', allowEmpty: true } }

});
