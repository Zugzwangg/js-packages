Ext.define('CFJS.model.dictionary.CarModel', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Dictionary' ],
    dictionaryType	: 'car_model',
	schema			: 'dictionary',
	fields			: [
		{ name: 'brand', type: 'model', critical: true, entityName: 'CFJS.model.dictionary.CarBrand' }
	]
});