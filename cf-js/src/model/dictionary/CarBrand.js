Ext.define('CFJS.model.dictionary.CarBrand', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Dictionary' ],
    dictionaryType	: 'car_brand',
	schema			: 'dictionary'
});