Ext.define('CFJS.model.dictionary.IDType', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Dictionary' ],
    dictionaryType	: 'id_type',
	schema			: 'dictionary',
	fields			: [{ name: 'mask', type: 'string' }]
});