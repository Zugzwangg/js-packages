Ext.define('CFJS.model.dictionary.Customer', {
	extend		: 'CFJS.model.EntityModel',
	requires	: [ 'CFJS.model.dictionary.Consumer', 'CFJS.model.dictionary.MemorableDate', 'CFJS.schema.OrgChart' ],
	schema		: 'dictionary',
	isCustomer	: true,
	fields		: [
 		{ name: 'locale',			type: 'string',		critical: true, defaultValue: CFJS.getLocale() },
 		{ name: 'author',			type: 'string',		critical: true },
 		{ name: 'genderType',		type: 'string',		critical: true, defaultValue: 'MALE' },
 		{ name: 'name',				type: 'string',		critical: true },
 		{ name: 'lastName',			type: 'string',		critical: true },
 		{ name: 'middleName',		type: 'string',		critical: true },
 		{ name: 'name_en',			type: 'property',	critical: true, mapping: 'properties.field_name_en' },
 		{ name: 'last_name_en',		type: 'property',	critical: true,	mapping: 'properties.field_last_name_en' },
 		{ name: 'middle_name_en',	type: 'property',	mapping: 'properties.field_middle_name_en' },
 		{ name: 'name_uk',			type: 'property',	mapping: 'properties.field_name_uk' },
 		{ name: 'last_name_uk',		type: 'property',	mapping: 'properties.field_last_name_uk' },
 		{ name: 'middle_name_uk',	type: 'property',	mapping: 'properties.field_middle_name_uk' },
 		{ name: 'name_ru',			type: 'property',	mapping: 'properties.field_name_ru' },
 		{ name: 'last_name_ru',		type: 'property',	mapping: 'properties.field_last_name_ru' },
 		{ name: 'middle_name_ru',	type: 'property',	mapping: 'properties.field_middle_name_ru' },
 		{ name: 'birthday',			type: 'date',		critical: true },
		{ name: 'tax',				type: 'string' },
 		{ name: 'type',				type: 'string',		critical: true, defaultValue: 'CUSTOMER_TYPE_1' },
 		{ name: 'password',			type: 'string',		critical: true },
		{ name: 'email',			type: 'string' },
		{ name: 'phone',			type: 'string' },
 		{ name: 'address',			type: 'string' },
		{ name: 'newsAgree',		type: 'boolean', 	defaultValue: true },
		// for company
		{ name: 'postType',			type: 'string', 	allowNull: true },
		{ name: 'postName',			type: 'string',		allowNull: true },
		{ name: 'workEmail',		type: 'string', 	allowNull: true },
		{ name: 'onTheBasis',		type: 'string',		allowNull: true },
		{ name: 'contactPerson',	type: 'boolean', 	defaultValue: false },
		{ name: 'companyNewsAgree',	type: 'boolean', 	defaultValue: false },
		// other
		{ name: 'notes',			type: 'string',		allowNull: true },
		{ name: 'photo',			type: 'string',		defaultValue: CFJS.sharedResourcePath('images/default_person_photo.png', 'cf-js') },

		// system
		{ name: 'sid',				type: 'string',		allowNull: true },
		{ name: 'error',			type: 'string',		allowNull: true },
		{ name: 'timeZoneOffset',	type: 'int' },
		{ name: 'notConfirmed',		type: 'boolean', 	defaultValue: true },
		{ name: 'lastLogin',		type: 'date',		persist: false },
		{ name: 'created',			type: 'date',		persist: false },
		{ name: 'updated',			type: 'date',		persist: false },
		{ name: 'company' }, 
		{ name: 'unit' }, 
		{ name: 'memorableDates', 	type: 'array',		critical: true, item: { entityName: 'CFJS.model.dictionary.MemorableDate', mapping: 'memorable_date' }},
		{ name: 'contacts', 		type: 'array', 		critical: true, item: { mapping: 'contact' }},
 		{ name: 'fullName',			type: 'string',
			calculate: function(data) {
 				var fullName = [], fields = this.getDepends(), i, v;
 				for (i = 0; i < fields.length; i++) {
 					if (!Ext.isEmpty(v = data[fields[i]])) fullName.push(v);
 				}
 				return fullName.join(' ');
 			},
 			depends: ['lastName', 'name', 'middleName']
 		}
	],
	hasOne		: [
		{ name: 'Company', 	model: 'NamedModel', associationKey: 'company' },
		{ name: 'Unit', 	model: 'NamedModel', associationKey: 'unit' }
	],
//	validators: {
//		tax: { type: 'taxUA', birthdayField: 'birthday' }
//	},
	
	asConsumer: function() {
		var me = this, 
			record = { customer: { id: me.getId(), name: me.get('fullName') } },
			fields = [ 'address', 'birthday', 'email', 'lastName', 'middleName', 'name', 'phone', 'tax' ],
			Model = 'Consumer', i = 0;
		for (; i < fields.length; i++) {
			record[fields[i]] = me.get(fields[i]);
		}
		if (me.session) record = me.session.createRecord(Model, record);
		else {
			Model = me.schema.getEntity(Model);
			record = new Model(record);
		}
		return record;
	},
	
	permissionLevel: function() {
		CFJS.Api.getClientPermissionLevel();
	},
	
	isAdministrator: function() { 
		return false;
	},
	
	statics: {
		
		toConsumer: function(data) {
			return new CFJS.model.dictionary.Customer(data).asConsumer();
		}
	
	}

}, function() {
	Ext.data.schema.Schema.get('org_chart').addEntity(Ext.ClassManager.get(this.$className));
});