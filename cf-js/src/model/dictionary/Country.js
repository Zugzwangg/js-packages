Ext.define('CFJS.model.dictionary.Country', {
	extend	: 'CFJS.model.airline.IataModel',
	requires: [ 'CFJS.schema.Dictionary' ],
	schema	: 'dictionary',
	fields	: [
		{ name: 'name_en',		type: 'property',	critical: true, mapping: 'properties.field_name_en' },
 		{ name: 'name_uk',		type: 'property',	critical: true, mapping: 'properties.field_name_uk' },
 		{ name: 'name_ru',		type: 'property',	critical: true, mapping: 'properties.field_name_ru' },
		{ name: 'iataCode', 	type: 'string',		allowNull: true },
		{ name: 'railwayCode',	type: 'string',		allowNull: true },
		{ name: 'phoneCode',	type: 'string',		allowNull: true },
		{ name: 'population',	type: 'int',		allowNull: true }
	]
});
