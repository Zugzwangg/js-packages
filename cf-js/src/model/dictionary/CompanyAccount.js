Ext.define('CFJS.model.dictionary.CompanyAccount', {
	extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Dictionary' ],
    dictionaryType	: 'company_account',
	schema			: 'dictionary',
	fields			: [
		{ name: 'companyId',	type: 'int',	critical: true, reference: 'Company' },
		{ name: 'bank',			type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.Bank' },
		{ name: 'code',			type: 'string',	critical: true }
	]
});