Ext.define('CFJS.model.dictionary.MemorableDate', {
	extend	: 'Ext.data.Model',
	requires: [ 'CFJS.schema.Dictionary' ],
	schema	: 'dictionary',
	fields	: [
		{ name: 'date', 		type: 'date', 	critical: true },
		{ name: 'description',	type: 'string', critical: true }
	]
});