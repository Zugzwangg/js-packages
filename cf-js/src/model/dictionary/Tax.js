Ext.define('CFJS.model.dictionary.Tax', {
    extend	: 'CFJS.model.NamedModel',
	requires: [ 'CFJS.schema.Dictionary' ],
	schema	: 'dictionary',
	fields	: [
		{ name: 'code', type: 'string', critical: true }
	]
});