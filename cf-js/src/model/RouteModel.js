Ext.define('CFJS.model.RouteModel', {
    extend: 'CFJS.model.EntityModel',
	fields: [
		{ name: 'departure',	type: 'date' },
		{ name: 'arrive',		type: 'date' },
		{ name: 'created',		type: 'date', persist: false },
		{ name: 'updated',		type: 'date', persist: false }
	]

});