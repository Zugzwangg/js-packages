Ext.define('CFJS.model.SiteModule', {
	extend	: 'Ext.data.Model',
	fields	: [
		{ name: 'id', 	type: 'string' },
		{ name: 'hash',	type: 'int' },
		{ name: 'url',	type: 'string' },
		{ name: 'name', type: 'string', calculate: function (data) { return data['name_' + CFJS.getLocale()] } },
	],
	proxy	: 'memory'
});