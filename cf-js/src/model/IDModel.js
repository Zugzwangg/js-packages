Ext.define('CFJS.model.IDModel', {
	extend		: 'CFJS.model.EntityModel',
	requires	: [ 'CFJS.model.NamedModel' ],
	identifier	: { type: 'sequential', prefix: 'ID_' },
	fields		: [
		{ name: 'id',			type: 'string',		critical: true },
		{ name: 'locale',		type: 'string',		critical: true, defaultValue: CFJS.getLocale() },
		{ name: 'owner',		type: 'auto',		critical: true },
		{ name: 'country',		type: 'model',		critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'type',			type: 'auto',		critical: true },
		{ name: 'internal',		type: 'boolean',	critical: true, defaultValue: false },
		{ name: 'issueDate',	type: 'date',		critical: true },
		{ name: 'validity',		type: 'date',		critical: true },
		{ name: 'issued',		type: 'string',		allowNull: true },
		{ name: 'description',	type: 'string',		allowNull: true }
	],
	hasOne: [
		{ name: 'Owner',	model: 'NamedModel',	associationKey: 'owner' },
		{ name: 'Country',	model: 'Country',		associationKey: 'country' },
		{ name: 'Type',		model: 'IDType',		associationKey: 'type' }
	]

});