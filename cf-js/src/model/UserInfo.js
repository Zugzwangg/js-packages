Ext.define('CFJS.model.UserInfo', {
	extend		: 'CFJS.model.CustomerInfo',
	fields		: [
		{ name: 'photo',	type: 'string', persist: false,	allowNull: true },
		{ name: 'photoUrl',	type: 'string', calculate: function(data) { return CFJS.model.UserInfo.photoUrl(null, data.type, data.photo); } }
	],
	
	statics: {
		
		photoUrl: function(service, type, photo) {
			var api = CFJS.Api, cnt = photo && api.controller(service = service || 'resources');
			if (cnt && cnt.url) {
				return api.buildUrl({
					url			: cnt.url,
					service		: service,
					signParams	: true,
					params		: { method: 'download', type: type + '/photos', path: photo }
				});
			}
			return CFJS.sharedResourcePath('images/default_person_photo.png', 'cf-js');
		}

	}

});