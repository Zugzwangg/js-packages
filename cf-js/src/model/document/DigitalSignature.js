Ext.define('CFJS.model.document.DigitalSignature', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Document', 'CFJS.model.UserInfo' ],
    dictionaryType	: 'digital_signature',
    isSignature		: true,
	schema			: 'document',
	fields			: [
		{ name: 'signer',		type: 'model',	critical: true,	entityName: 'CFJS.model.UserInfo' },
		{ name: 'phase',		type: 'string',	critical: true, allowNull: true },
		{ name: 'resolution',	type: 'string',	critical: true, allowNull: true },
		{ name: 'certificate',	type: 'string',	critical: true },
		{ name: 'value',		type: 'string',	critical: true },
		{ name: 'timestamp',	type: 'date',	critical: true }
	],
	
	buildVerifyRequest: function(service, method, payload, payloadType, options) {
		var me = this, api = CFJS.Api, request,
			cnt = api.controller(service = service || me.getProxy().getService());
		if (cnt && cnt.url) {
			request = Ext.apply({
				url			: cnt.url,
				method		: 'POST',
				service		: service,
				signParams	: true,
				params		: { method: [cnt.method, method].join('.') },
			}, options);
			request[api.payloadFormat() + 'Data'] = api.buildPayload(payload, payloadType);
		}
		return request;
	},
	
	verify: function(service, callback) {
		var me = this, api = CFJS.Api,
			payload = me.getData({ clearNulls: true, persist: true, serialize: true }),
			internalCallback = function(options, success, response) {
				var context = success ? api.extractResponse(response) : { error: { code: response.status, text: response.statusText} };
				if (Ext.Object.isEmpty(context.error)) {
					Ext.callback(callback, me, [context.transaction, me]);
				} else CFJS.errorAlert('Error verify signature', context.error);
			},
			request = me.buildVerifyRequest(service, 'sign.verify', payload, 'VERIFY', { callback: internalCallback });
		if (request) CFJS.Api.request(request);
	}
	
});