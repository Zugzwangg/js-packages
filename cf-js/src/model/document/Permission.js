Ext.define('CFJS.model.document.Permission', {
    extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Document' ],
    dictionaryType	: 'permission_level',
	schema			: 'document',
	fields			: [{ name: 'level', type: 'int', critical: true }]
});