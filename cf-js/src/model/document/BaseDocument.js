Ext.define('CFJS.model.document.BaseDocument', {
    extend		: 'CFJS.model.EntityModel',
	requires	: [ 'CFJS.model.document.DigitalSignature' ],
	schema		: 'document',
	fields		: [
		{ name: 'form',			type: 'int',	critical: true },
		{ name: 'number',		type: 'string',	critical: true },
		{ name: 'date',			type: 'date',	critical: true },
		{ name: 'signatures',	type: 'array',	critical: true, item: { mapping: 'digital_signature' } }
	],
	
	buildPrintUrl: function(template, method, service) {
		return CFJS.model.document.BaseDocument.createPrintUrl(template, method, service, this.getId()); 
	},

	hasSign: function(phase, userInfo) {
		var me = this, signatures = me.get('signatures'), i = 0;
		userInfo = userInfo || CFJS.lookupViewportViewModel().userInfo();
		if (!Ext.isEmpty(signatures) && userInfo) {
			for (; i < signatures.length; i++) {
				if (signatures[i] && me.equalUser(userInfo, signatures[i].signer) && (!phase || phase === signatures[i].phase)) return true;
			}
		}
		return false;
	},
	
	print: function(template, method, service) {
		var url = this.buildPrintUrl(template, method, service);
		if (!Ext.isEmpty(url)) window.open(url, '_blank');
	},
	
	privates: {
		
		equalUser: function(user1, user2) {
			return user1 && user2 && user1.id === user2.id && user1.type === user2.type; 
		}
	
	},
	
	inheritableStatics: {
		
		createPrintUrl: function(template, method, service, document) {
			if (Ext.isObject(document)) document = document.id;
			return CFJS.Api.buildUrl({
				service		: service || 'svd',
				signParams	: true,
				params		: {
					method	: method || 'preview',
					document: parseInt(document, 10),
					template: template || 'template',
					locale	: CFJS.getLocale()
				}
			});
		}
	
	}

});