Ext.define('CFJS.model.document.InvoiceRecord', {
    extend		: 'CFJS.model.NamedModel',
	requires	: [ 'CFJS.schema.Document' ],
	idProperty	: 'recId',
	schema		: 'document',
	fields		: [
		{ name: 'orderNumber',	type: 'string',		critical: true },
		{ name: 'consumerId',	type: 'int', 		critical: true, defaultValue: 0 },
		{ name: 'units',		type: 'string',		critical: true, defaultValue: 'Послуга' },
		{ name: 'quantity',		type: 'float',		critical: true, defaultValue: 1 },
		{ name: 'amount',		type: 'float',		critical: true, defaultValue: 0 },
		{ name: 'total',		type: 'float',		persist: false, calculate: function (data) { return data.quantity * data.amount; } },
		{ name: 'showVat',		type: 'boolean', 	critical: true },
		{ name: 'taxes',		type: 'array',		critical: true, item: { entityName: 'CFJS.model.document.InvoiceRecord', mapping: 'invoice_record' } }
	],

	clearTaxes: function() {
		var me = this, taxes = me.get('taxes'), ret = [];
		if (taxes && taxes.length > 0) {
			var amount = me.get('amount') || 0, tax, taxAmount, i;
			for (i = 0; i < taxes.length; i++) {
				tax = taxes[i];
				taxAmount = Ext.isNumber(tax.amount) ? tax.amount : parseFloat(tax.amount || 0);
				if (!isNaN(taxAmount)) amount -= taxAmount;
				tax.id = undefined;
				ret.push(tax);
			}
			me.set('amount', amount);
			me.set('taxes', []);
		}
		return ret;
	},
	
	removeTaxByCode: function(taxCode) {
		var me = this, taxes = me.get('taxes'), ret = [];
		if (taxes && taxes.length > 0 && (taxCode = '/' + taxCode).length > 1) {
			var amount = me.get('amount') || 0, tax, taxAmount, i;
			for (i = 0; i < taxes.length; i++) {
				if ((tax = taxes[i]).orderNumber && tax.orderNumber.endsWith(taxCode)) {
					taxAmount = Ext.isNumber(tax.amount) ? tax.amount : parseFloat(tax.amount || 0);
					if (!isNaN(taxAmount)) amount -= taxAmount;
					tax.id = undefined;
					ret.push(tax);
					taxes.splice(i--, 1);
				}
			}
			me.set('amount', amount);
			me.set('taxes', taxes, { convert: false });
		}
		return ret;
	}

});