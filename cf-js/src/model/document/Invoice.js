Ext.define('CFJS.model.document.Invoice', {
	extend	: 'CFJS.model.document.BaseDocument',
	requires: [ 'CFJS.model.document.InvoiceRecord' ],
	fields	: [
		{ name: 'order',		type: 'int',		critical: true },
		{ name: 'payer',		type: 'auto',		critical: true }, 
		{ name: 'amount',		type: 'number', 	critical: true, defaultValue: 0 },
		{ name: 'paymentDate',	type: 'date',		critical: true },
		{ name: 'paymentAmount',type: 'number', 	critical: true, defaultValue: 0 },
		{ name: 'state',		type: 'int',		critical: true, defaultValue: 0 },
		{ name: 'records',		type: 'array',		critical: true, item: { entityName: 'CFJS.model.document.InvoiceRecord', mapping: 'invoice_record' } },
		{ 
			name: 'canPay',
			type: 'boolean', 
			calculate: function(invoice) {
				var amount = invoice.amount, residualValue = amount - invoice.paymentAmount;
				return invoice.state === 1 && residualValue !== 0 && (amount > 0 && residualValue > 0 || amount < 0 && residualValue < 0);
			}
		},
		{ 
			name: 'canRemove',
			type: 'boolean', 
			calculate: function(invoice) {
				return invoice.state < 2 && invoice.paymentAmount === 0;
			}
		},
		{ name: 'canSign', type: 'boolean', depends: ['state'],
			convert: function(value, record) {
				return record.get('state') === 0 && !record.hasSign();
			}
		}
	],
	proxy: {
		api			: { 
			create	: { method: 'save.invoice', transaction: 'save' },
			read	: { method: 'load.invoice', transaction: 'load' },
			sign	: { method: 'sign.invoice', transaction: 'sign' },
			update	: { method: 'save.invoice', transaction: 'save' }
		},
		loaderConfig: { dictionaryType: 'document' }
	},
	
	addRecords: function(data) {
		if (!data) return;
		if (!Ext.isArray(data)) data = [data];
		var me = this,
			records = me.get('records'),
			map = Ext.Array.toValueMap(records, 'orderNumber'),
			record;
		for (i = 0; i < data.length; i++) {
			if (record = data[i]) {
				if (record.isModel) record = record.getData();
				if (!map[record.orderNumber]) {
					records.push(record);
					map[record.orderNumber] = record;
				}
			}
		}
		me.set('records', records, { convert: false });
	},
	
	getPaymentAmount: function(field) {
		return this.get('paymentAmount');
	},
	
	getState: function(field) {
		return this.get('state');
	},
	
	load: function(options) {
		var me = this; proxy = me.getProxy();
		proxy.addLoaderConfig({ form: me.get('form') });
		return me.callParent(arguments);
	},
	
	removeRecords: function(data) {
		if (!data) return;
		if (!Ext.isArray(data)) data = [data];
		var me = this,
			records = [],
			map = Ext.Array.toValueMap(me.get('records'), 'orderNumber'), 
			record;
		for (i = 0; i < data.length; i++) {
			if (record = data[i]) {
				if (record.isModel) record = record.getData();
				if (map[record.orderNumber]) delete map[record.orderNumber];
			}
		}
		for (orderNumber in map) {
			records.push(map[orderNumber]);
		}
		me.set('records', records, { convert: false });
	},

	setPayer: function(payer, form) {
		var me = this;
		me.set('payer', payer);
		if (form) me.set('form', form);
	},
	
	setState: function(field, state) {
		this.set('state', state || 0);
	}

});