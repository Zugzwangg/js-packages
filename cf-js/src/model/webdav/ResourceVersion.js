Ext.define('CFJS.model.webdav.ResourceVersion', {
	extend				: 'CFJS.model.EntityModel',
	requires			: [ 'CFJS.model.UserInfo', 'CFJS.schema.WebDAV' ],
	isResourceVersion	: true,
	schema				: 'webdav',
	service				: 'webdav',
	fields				: [
		{ name: 'resourceId',	type: 'int' },
		{ name: 'author',		type: 'model',	critical: true,	entityName: 'CFJS.model.UserInfo' },
		{ name: 'created',		type: 'date',	persist: false }
	],
	proxy				: {
		type		: 'json.svdapi',
		loaderConfig: { dictionaryType: 'resource_version' },
		reader		: { rootProperty: 'response.transaction.list.resource_version' },
		service		: 'webdav',
	},

	buildRequest: function(service, method, payload, payloadType, options) {
		var me = this, api = CFJS.Api, request,
			cnt = api.controller(service = service || me.getProxy().getService());
		if (cnt && cnt.url) {
			request = Ext.apply({
				url			: cnt.url,
				method		: 'POST',
				service		: service,
				signParams	: true,
				params		: { method: [cnt.method, method].join('.') },
			}, options);
			request[api.payloadFormat() + 'Data'] = api.buildPayload(payload, payloadType);
		}
		return request;
	},
	
	canDelete: function(userInfo) {
		return this.isAuthor(userInfo); 
	},

	download: function(fileName, service) {
		var me = this, 
			payload = me.getData({ persist: true, serialize: true }),
			callback = function(options, success, response) {
				if (success) CFJS.downloadAsBlob(response, fileName || 'unknown');
				else CFJS.errorAlert('Error downloading data', { code: response.status, text : response.statusText});
			},
			request = me.buildRequest(service, 'download.version', payload, 'DOWNLOAD', { binary: true,callback: callback });
		if (request) CFJS.Api.request(request);
	}

});