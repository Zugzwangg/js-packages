Ext.define('CFJS.model.webdav.InternalShare', {
	extend		: 'CFJS.model.EntityModel',
	requires	: [ 'CFJS.model.NamedModel', 'CFJS.model.UserInfo', 'CFJS.schema.WebDAV' ],
	schema		: 'webdav',
	fields		: [
		{ name: 'resource',	type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'user',		type: 'model',	critical: true,	entityName: 'CFJS.model.UserInfo' }
	],
	proxy				: {
		type		: 'json.svdapi',
		api			: {
			destroy	: { method: 'remove.internal', transaction: 'remove' }
		},
		loaderConfig: { dictionaryType: 'internal_share' },
		reader		: { rootProperty: 'response.transaction.list.internal_share' },
		service		: 'webdav',
	},

});