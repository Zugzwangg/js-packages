Ext.define('CFJS.model.webdav.Resource', {
	extend		: 'CFJS.model.webdav.SimpleResource',
	requires	: [ 'CFJS.model.webdav.ExternalShare' ],
	fields		: [
		{ name: 'description',		type: 'string' },
		{ name: 'isFile',			type: 'boolean',	critical: true, defaultValue: false },
		{ name: 'canDelete',		type: 'boolean',	critical: true, defaultValue: false },
		{ name: 'shared',			type: 'boolean',	persist: false },
		{ name: 'externalShare',	type: 'model', 		persist: false,	entityName: 'CFJS.model.webdav.ExternalShare' }
	],
	proxy: {
		type		: 'json.svdapi',
		api			: { create: { method: 'mkdir', transaction: 'new' } },
		loaderConfig: { dictionaryType: 'resource' },
		reader		: { rootProperty: 'response.transaction.list.resource' },
		service		: 'webdav',
	},
	
	externalShare: function(service) {
		var me = this, api = CFJS.Api,
			share = me.get('externalShare'),
			payload = {
				type		: 'EXTERNAL_SHARE',
				locale		: CFJS.getLocale(),
				id			: me.getId(),
				disposable	: share.disposable,
				expiryTime	: Ext.isDate(share.expiryTime) ? api.formatDateTime(share.expiryTime) : undefined
			},
			callback = function(options, success, response) {
				if (success) {
					if ((response = CFJS.Api.extractResponse(response)) && response.transaction) {
						me.set('externalShare', Ext.clone(response.transaction));
					}
				} else CFJS.errorAlert('Error creating share link', { code: response.status, text: response.statusText});
			}, 
			request = this.buildRequest(service, 'share', payload, 'NEW', { callback: callback });
		if (request) api.request(request);
	},
	
	canDelete: function(userInfo) {
		return this.callParent(arguments) && this.get('canDelete');
	},
	
	internalShare: function(userInfo, service, callback) {
		var me = this, api = CFJS.Api,
			payload = {
				type	: 'INTERNAL_SHARE',
				locale	: CFJS.getLocale(),
				id		: me.getId(),
				userInfo: userInfo
			},
			internalCallback = function(options, success, response) {
				if (success) Ext.callback(callback, me, [(CFJS.Api.extractResponse(response) || {}).transaction]);
				else CFJS.errorAlert('Error creating share link', { code: response.status, text: response.statusText});
			}, 
			request = this.buildRequest(service, 'share', payload, 'NEW', { callback: internalCallback });
		if (request) api.request(request);
	},
	
	removeExternalShare: function(service) {
		var me = this, api = CFJS.Api,
			payload = new CFJS.model.webdav.ExternalShare(me.get('externalShare')).getData({ persist: true, serialize: true }),
			callback = function(options, success, response) {
				if (success) me.set('externalShare', null);
				else CFJS.errorAlert('Error removing share link', { code: response.status, text: response.statusText });
			}, 
			request = me.buildRequest(service, 'remove.external', payload, 'REMOVE', { callback: callback });
		if (request) CFJS.Api.request(request);
	}
	
});