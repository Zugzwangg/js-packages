Ext.define('CFJS.model.webdav.SimpleResource', {
	extend			: 'CFJS.model.TreeModel',
	requires		: [ 'CFJS.model.FileModel', 'CFJS.model.UserInfo', 'CFJS.schema.WebDAV' ],
    dictionaryType	: 'simple_resource',
	isResource		: true,
    methodDownload	: 'download',
	schema			: 'webdav',
	fields			: [
		{ name: 'author',			type: 'model',		critical: true,	entityName: 'CFJS.model.UserInfo' },
		{ name: 'isFile',			type: 'boolean',	critical: true, defaultValue: true },
		{ name: 'size',				type: 'int',		critical: true },
		{ name: 'created',			type: 'date',		persist: false },
		{ name: 'lastModified',		type: 'date',		persist: false },
		{ name: 'ext',				type: 'string',
			calculate: function (data) {
				if (data.isFile) return CFJS.schema.WebDAV.extension(data.name);
			}
		},
		{ name: 'iconCls',			type: 'string',
			calculate: function (data) {
				return data.isFile ? CFJS.schema.WebDAV.iconCls(data.ext) : CFJS.UI.iconCls.FOLDER;
			}
		}
	],

	buildRequest: function(service, method, payload, payloadType, options) {
		var me = this, api = CFJS.Api, request,
			cnt = api.controller(service = service || me.getProxy().getService());
		if (cnt && cnt.url) {
			request = Ext.apply({
				url			: cnt.url,
				method		: 'POST',
				service		: service,
				signParams	: true,
				params		: { method: [cnt.method, method].join('.') },
			}, options);
			if (me.parentType) request.params.type = me.parentType;
			request[api.payloadFormat() + 'Data'] = api.buildPayload(payload, payloadType);
		}
		return request;
	},
	
	canDelete: function(userInfo) {
		return this.isAuthor(userInfo);
	},
	
	download: function(service, callback) {
		callback = callback || CFJS.downloadAsBlob;
		var me = this,
			payload = me.getData({ clearNulls: true, persist: true, serialize: true }),
			internalCallback = function(options, success, response) {
				if (success) Ext.callback(callback, me, [response, me.get('name')]);
				else CFJS.errorAlert('Error downloading data', { code: response.status, text: response.statusText});
			},
			request = me.buildRequest(service, me.methodDownload, payload, 'DOWNLOAD', { binary: true, callback: internalCallback });
		if (request) CFJS.Api.request(request);
	}

});