Ext.define('CFJS.model.webdav.ExternalShare', {
	extend		: 'CFJS.model.EntityModel',
	requires	: [ 'CFJS.schema.WebDAV' ],
	schema		: 'webdav',
	fields		: [
		{ name: 'resourceId',	type: 'int' },
		{ name: 'token',		type: 'string' },
		{ name: 'disposable',	type: 'boolean' },
		{ name: 'expiryTime',	type: 'date' },
		{ name: 'url',			type: 'string',
			calculate: function (data) {
				if (!Ext.isEmpty(data.token)) {
					return CFJS.Api.controllerUrl('webdav') + '/external/' + data.token;
				}
			}
		},

	]
});