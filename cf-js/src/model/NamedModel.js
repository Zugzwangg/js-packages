Ext.define('CFJS.model.NamedModel', {
    extend: 'CFJS.model.EntityModel',
	fields: [{ name: 'name', type: 'string' }]
});