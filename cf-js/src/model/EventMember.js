Ext.define('CFJS.model.EventMember', {
    extend: 'CFJS.model.UserInfo',
	fields: [{ name: 'priority', type: 'int', critical: true }]
});