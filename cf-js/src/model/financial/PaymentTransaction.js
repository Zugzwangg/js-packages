Ext.define('CFJS.model.financial.PaymentTransaction', {
    extend			: 'CFJS.model.financial.FinancialTransaction',
    requires		: [ 'CFJS.schema.Financial' ],
    dictionaryType	: 'payment_transaction',
	schema			: 'financial',
    fields			: [
		{ name: 'baseDocument',	type: 'auto', 	critical: true },
		{ name: 'description',	type: 'string' },
		{ name: 'updated',		type: 'date',	persist: false },
		{ name: 'debt',			type: 'number', 
			calculate: function(data) {
				return data.amount < 0 ? 0 : data.amount;
			}
		},
		{ name: 'credit',		type: 'number', 
			calculate: function(data) {
				return data.amount < 0 ? -data.amount : 0;
			}
		}
	]
});