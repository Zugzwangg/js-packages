Ext.define('CFJS.model.financial.AbstractService', {
    extend			: 'CFJS.model.financial.FinancialModel',
	requires		: [ 
		'CFJS.schema.Financial',
		'CFJS.model.dictionary.Consumer',
		'CFJS.model.financial.Order'
	],
    dictionaryType	: 'abstract_service',
    isService		: true,
	schema			: 'financial',
    fields			: [
		{ name: 'parent',		type: 'int',		critical: true,	allowNull: true },
		{ name: 'order',		type: 'int',		critical: true,	allowNull: true },
		{ name: 'serviceType',	type: 'string',		critical: true,	defaultValue: 'OTHER' },
		{ name: 'post' },
		{ name: 'state',		type: 'string',		critical: true,	defaultValue: 'ORDER' },
		{ name: 'sector',		type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 71, name: 'Внутрішній PTA' } }, 
		{ name: 'number',		type: 'string',		critical: true },
		{ name: 'extNumber',	type: 'string',		critical: true },
		{ name: 'supplier' },
		{ name: 'consumer',		type: 'model',		critical: true,	entityName: 'CFJS.model.dictionary.Consumer' }, 
		{ name: 'description',	type: 'string' },
		{ name: 'amount',		type: 'number', 	persist: false,	defaultValue: 0 },
		{ name: 'baseCurrency',	type: 'model',		critical: true,	entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } },
		{ name: 'baseAmount',	type: 'number' },
		{ name: 'showVat',		type: 'boolean',	defaultValue: false },
		{ name: 'updatable',	type: 'boolean',	defaultValue: false },
		{ name: 'hasChilds',	type: 'boolean',	persist: false,	defaultValue: false }
	],
	
	buildPrintUrl: function(gds, method, service) {
		var me = this,
			options = {
				service		: service || 'iss',
				signParams	: true,
				params		: {
					method	: method || 'print.service',
					type	: me.get('serviceType'),
					service	: me.getId(),
					locale	: CFJS.getLocale()
				}
			};
		if (gds !== false) options.params.gds = gds || (me.get('extNumber')||'').split(/\s|-/, 1) || undefined;
		return CFJS.Api.buildUrl(options);
	},
	
	print: function(gds, method, service) {
		var url = this.buildPrintUrl(gds, method, service);
		if (!Ext.isEmpty(url)) window.open(url, '_blank');
	}

});