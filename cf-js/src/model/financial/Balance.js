Ext.define('CFJS.model.financial.Balance', {
    extend	: 'CFJS.model.EntityModel',
    requires: [ 'CFJS.schema.Dictionary', 'CFJS.schema.Financial', 'CFJS.model.CustomerInfo', 'CFJS.model.dictionary.Currency' ],
	schema	: 'financial',
    fields	: [
		{ name: 'customer', 		type: 'model',	critical: true, entityName: 'CFJS.model.CustomerInfo' },
		{ name: 'currency', 		type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } },
		{ name: 'limit',			type: 'number', critical: true, defaultValue: 0 },
		{ name: 'daysToPay',		type: 'int',	allowNull: true },
		{ name: 'debtDate',			type: 'date',	allowNull: true },
		{ name: 'balance',			type: 'number', critical: true, defaultValue: 0 },
		{ name: 'availableAmount',	type: 'number', critical: true, defaultValue: 0 }
	]
}, function() {
	Ext.data.schema.Schema.get('dictionary').addEntity(Ext.ClassManager.get(this.$className));
});