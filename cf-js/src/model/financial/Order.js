Ext.define('CFJS.model.financial.Order', {
    extend	: 'CFJS.model.financial.FinancialModel',
	requires: [ 'CFJS.schema.Financial', 'CFJS.model.CustomerInfo', 'CFJS.model.orgchart.Post' ],
	schema	: 'financial',
	fields	: [
		{ name: 'post',			type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'customer',		type: 'model',	critical: true, entityName: 'CFJS.model.CustomerInfo' },
		{ name: 'payments' },
		{ name: 'childs' },
		{ name: 'taxes' },
		{ name: 'paymentAmount',
			calculate: function(data) {
				var amount = 0,
					payments = data.payments;
					transactions = payments ? payments.payment_transaction || [] : [];
				if (!Ext.isArray(transactions)) transactions = [transactions];
				for (var i = 0; i < transactions.length; i++) {
					amount -= transactions[i].amount || 0;
				}
				return amount;
			}
		},
		{ name: 'validUntil',	type: 'date',	critical: true, defaultValue: new Date() }
	],
	
	setCustomer: function(customer, type) {
		this.set('customer', Ext.apply(customer||{}, { type: type || company }));
	}

});