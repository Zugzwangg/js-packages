Ext.define('CFJS.model.financial.FinancialTransaction', {
    extend	: 'CFJS.model.EntityModel',
    requires: [ 'CFJS.model.CustomerInfo', 'CFJS.model.dictionary.Currency' ],
    fields	: [
		{ name: 'customer',			type: 'model',	critical: true, entityName: 'CFJS.model.CustomerInfo' },
		{ name: 'proofDocument',	type: 'model', 	critical: true, entityName: 'CFJS.model.NamedModel' },
		{ name: 'currency', 		type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } },
		{ name: 'amount',			type: 'number', critical: true, defaultValue: 0 },
		{ name: 'created',			type: 'date',	critical: true }
	]
});