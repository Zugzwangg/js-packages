Ext.define('CFJS.model.financial.Service', {
	extend			: 'CFJS.model.financial.AbstractService',
	requires		: [ 'CFJS.model.financial.TaxAmount' ],
    dictionaryType	: 'service',
    fields			: [{ 
    	name: 'taxes', type: 'array', item: { entityName: 'CFJS.model.financial.TaxAmount', mapping: 'tax_amount' }
    }],
	
	getTax: function(code) {
		var taxes = this.get('taxes') || [],
			i, tax, taxAmount;
		for ( i = 0; i < taxes.length; i++) {
			taxAmount = (taxes[i] || {});
			if ((taxAmount.tax || {}).code === code) return taxAmount;
		}
	},
	
	setTax: function(code, amount) {
		var me = this, taxes = me.get('taxes') || [],
			index = -1, i, tax = {};
		if (Ext.isObject(code)) tax = code;
		else if (Ext.isString) tax.code = code;
		for (i = 0; i < taxes.length; i++) {
			if (((taxes[i] || {}).tax || {}).code === tax.code) { index = i; break; }
		}
		if (index >= 0) {
			if (amount === 0) taxes.splice(index, 1);
			else taxes[index].amount = amount;
		} else if (amount !== 0) {
			taxes.push(new CFJS.model.financial.TaxAmount({
				parent	: me.id,
				tax		: tax,
				amount	: amount,
				created	: new Date()
			}).getData());
		}
		me.set('taxes', taxes);
	}
	
});