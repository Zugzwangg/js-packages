Ext.define('CFJS.model.financial.FinancialModel', {
    extend	: 'CFJS.model.EntityModel',
    requires: [ 'CFJS.model.dictionary.Currency' ],
    fields	: [
		{ name: 'currency', 	type: 'model',	critical: true, entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } },
		{ name: 'amount',		type: 'number', critical: true, defaultValue: 0 },
		{ name: 'validUntil',	type: 'date',	critical: true },
		{ name: 'created',		type: 'date',	persist: false },
		{ name: 'updated',		type: 'date',	persist: false }
	]
});