Ext.define('CFJS.model.financial.TaxAmount', {
    extend	: 'CFJS.model.EntityModel',
    requires: [ 'CFJS.model.dictionary.Tax' ],
    fields	: [
		{ name: 'parent',	type: 'int',	critical: true },
		{ name: 'tax', 		type: 'auto',	critical: true },
		{ name: 'amount',	type: 'number', critical: true, defaultValue: 0 },
		{ name: 'created',	type: 'date',	persist: false }
	],
	associations	: [
		{ type: 'hasOne', name: 'TaxAmountTax', model: 'Tax', associationKey: 'tax' }
	]
});