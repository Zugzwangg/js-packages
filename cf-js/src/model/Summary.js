Ext.define('CFJS.model.Summary', {
    extend: 'Ext.data.Model',
	fields: [
	 	{ name: 'field',	type: 'string', critical: true, defaultValue: 'id' },
	 	{ name: 'type',		type: 'string', critical: true, defaultValue: 'COUNT' },
	 	{ name: 'value',	type: 'number', critical: true, defaultValue: 0 }
	]
});