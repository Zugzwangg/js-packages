Ext.define('CFJS.model.LocalizableModel', {
	extend	: 'CFJS.model.NamedModel',
	fields	: [
		{ name: 'locale',		type: 'string',	critical: true, defaultValue: CFJS.getLocale() },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date',	critical: true }
	]
});