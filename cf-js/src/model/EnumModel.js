Ext.define('CFJS.model.EnumModel', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'id', 	type: 'string' },
		{ name: 'name', type: 'string' } 
	]
});