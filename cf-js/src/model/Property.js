Ext.define( 'CFJS.model.Property', {
	extend 	: 'Ext.data.Model',
	requires: [ 'CFJS.schema.Public' ],
	schema	: 'public',
	fields	: [
		{ name: 'name',			type: 'string',	allowBlank: false,	critical: true },
		{ name: 'value',		type: 'string',	allowBlank: false,	critical: true }
		// TODO for thinking
//		{ name: 'description',	type: 'string',	allowBlank: false,	critical: true }
    ]
});