Ext.define('CFJS.model.insurance.InsuranceProgram', {
    extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Insurance' ],
    dictionaryType	: 'insurance_program',
	schema			: 'insurance',
	fields			: [
		{ name: 'code',			type: 'string',	critical: true },
		{ name: 'startDate',	type: 'date',	critical: true },
		{ name: 'endDate',		type: 'date' },
		{ name: 'iconCls', 		type: 'string',
			calculate: function(data) {
				switch (data.code || '') {
				case 'CPI'	: return 'x-fa fa-stack-exchange';
				case 'HI'	: return CFJS.UI.iconCls.MEDKIT;
				case 'ACC'	: return 'x-fa fa-ambulance';
				case 'CL' 	: return CFJS.UI.iconCls.CAR;
				case 'FIN'	: return CFJS.UI.iconCls.MONEY;
				case 'BAG'	: return 'x-fa fa-suitcase';
				case 'OTHER': return 'x-fa fa-bomb';				
				default: break;
				}
			},
			depends: ['code']
		}
	]
});