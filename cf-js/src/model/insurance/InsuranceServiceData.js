Ext.define('CFJS.model.insurance.InsuranceServiceData', {
    extend			: 'Ext.data.Model',
	requires		: [ 'CFJS.model.insurance.InsuranceService' ],
	isInsuranceData	: true,
	schema			: 'financial',
	fields			: [{ name: 'service', allowBlank: false, reference: { parent: 'CFJS.model.insurance.InsuranceService', unique: true } }],
	
	updateServiceProperties: function() {
		var me = this,
			service = me.getService(),
			fieldsMap = me.fieldsMap,
			data = me.data,
			properties, name, field, value;
		if (service && (me.dirty || me.phantom)) {
			properties = service.get('properties') || {};
			for (name in fieldsMap) {
				if (name !== me.idProperty && name !== 'service'
						&& (field = fieldsMap[name]) && (field.persist || field.critical)) {
					value = data[name];
					if (field.serialize) value = field.serialize(value, me);
					if (value === null || value === undefined || (Ext.isBoolean(value) && value === false) || Ext.isEmpty(value) || Ext.isObject(value) && Ext.Object.isEmpty(value)) delete properties['field_' + name];
					else {
						if (!Ext.isString(value)) value = Ext.encode(value); 
						properties['field_' + name] = { '@type': 'string', '$': value };
					} 
				}
			}
			service.set('properties', Ext.clone(properties));
		}
	}
});