Ext.define('CFJS.model.insurance.InsuranceService', {
    extend			: 'CFJS.model.financial.Service',
	requires		: [ 'CFJS.model.insurance.InsuranceProgram' ],
	isInsurance		: true,
    dictionaryType	: 'insurance_service',
	fields			: [
		{ name: 'sector', 			type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel', defaultValue: { id: 5, name: 'Страхування' } },
		{ name: 'program',			type: 'model',	critical: true,	entityName: 'CFJS.model.insurance.InsuranceProgram' },
		{ name: 'insuranceCurrency',type: 'model',	critical: true,	entityName: 'CFJS.model.dictionary.Currency', defaultValue: { id: 1, name: 'грн.', code: 'UAH' } },
		{ name: 'insuranceAmount',	type: 'number',	critical: true },
		{ name: 'dateFrom',			type: 'date',	critical: true, defaultValue: new Date() },
		{ name: 'dateTo',			type: 'date',	critical: true, defaultValue: new Date() },
		{ name: 'dateContract',		type: 'date',	allowBlank: false,	critical: true },
		{ name: 'scheme',			type: 'string',	allowBlank: false,	critical: true },
		{ name: 'duration',			type: 'int',	allowBlank: false,	critical: true, defaultValue: 1 },
		//	zone details
		{ name: 'zone',				type: 'int',	allowBlank: false,	critical: true  },
		//	other details
		{ name: 'franchise',		type: 'number'	},
		//	financial
		{ name: 'paymentAmount',	type: 'number',	critical: true },
		{ name: 'paymentDate',		type: 'date',	critical: true }
	],
	
	getServiceData: function(model) {
		var me = this,
			properties = me.get('properties') || {},
			data = { service: me.getId() }, 
			field, value, fieldsMap = (model && model.isModel) ? model.getFieldsMap() : null, fld;
    	for (field in properties) {
			value = Ext.clone(properties[field]);
			if (value) {
				if (value['$']) value = value['$'];
				else delete value['@type'];
				if (field.startsWith('field_')) field = field.substring(6);
				if ( fieldsMap ) {
					fld = fieldsMap[field];
					if( fld && fld.getType() !== 'string' )
						if (Ext.isString(value)) value = Ext.decode(value, true) || value;
					data[field] = value;
				} else {
					if (Ext.isString(value)) value = Ext.decode(value, true) || value;
					data[field] = value;
				}
			}
		}
    	return data;
	},
	
	setServiceData: function(data) {
		var me = this, properties = {},
			fieldsMap, name, field, value, type;
		if (data && data.isInsuranceData && (data.dirty || data.phantom)) {
			fieldsMap = data.fieldsMap;
			for (name in fieldsMap) {
				if ((field = fieldsMap[name]) && field.persist && field.critical) {
					value = data.get(name);
					if (field.serialize) value = field.serialize(value, data);
					if (!Ext.isEmpty(value) && !Ext.Object.isEmpty(value)) {
						properties['field_' + name] = { '@type': 'string', '$': !Ext.isString(value) ? Ext.encode(value) : value };
					}
				}
			}
			me.set('properties', properties);
		}
	}

});