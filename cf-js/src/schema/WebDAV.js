Ext.define('CFJS.schema.WebDAV', {
	extend		: 'CFJS.schema.Public',
	alias		: 'schema.webdav',
	namespace	: 'CFJS.model.webdav'
});