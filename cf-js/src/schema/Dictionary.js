Ext.define('CFJS.schema.Dictionary', {
	extend		: 'CFJS.schema.Public',
	alias		: 'schema.dictionary',
	namespace	: 'CFJS.model.dictionary'
});