Ext.define('CFJS.schema.Public', {
	extend				: 'Ext.data.schema.Schema',
	requires			: [ 'Ext.data.identifier.Negative' ],
	namespace			: 'CFJS.model',
	alias				: 'schema.public',
	defaultIdentifier	: { type: 'negative' },
	proxy				: {
		type: 'json.cfapi',
		url	: undefined,
		api	: {
			create	: 'save',
			destroy : 'remove',
			read	: 'load',
			update  : 'save'
		},
		loaderConfig: { dictionaryType: '{dictionaryType}' },
		reader	: {
			type: 'json.cfapi',
			rootProperty: 'response.transaction'
		},
		service	: '{service}'
	},
	
	getDictionaryType: function(model) {
		if (!model) return null;
		var entityName = model.entityName.split('.');
		return entityName[entityName.length - 1].toLowerCase();
	},
	
	shareEntitiesTo: function(schema) {
		if (Ext.isString(schema)) schema = Ext.data.schema.Schema.get(schema);
		if (schema) {
			var publicNamespace = Ext.data.schema.Schema.get('public').getNamespace();
			this.eachEntity(function(name, entityType) {
				if (!schema.getEntity(name)) schema.addEntity(entityType);
			});
		}
	},
	
	privates: {
		
		constructProxy: function (Model) {
			var me = this,
				data = Ext.Object.chain(Model),
				configurator = data.getConfigurator(),
				proxy = me.getProxy();
			if (configurator) data = Ext.apply(data, configurator.values);
			if (!data.dictionaryType) data.dictionaryType = me.getDictionaryType(data);
			data.schema = me;
			data.prefix = me.getUrlPrefix();
			return proxy.apply(data);
		}
	
	},
	
	inheritableStatics: {

		extRe: /(?:\.([^.]+))?$/,

		extension: function(name) {
			if (Ext.isString(name)) return CFJS.schema.Public.extRe.exec(name)[1];
		},
		
		iconCls	: function (ext) {
			var types = ['fa', 'file', 'o'];
			ext = ext || '';
			if (ext === 'pdf') types.splice(2,0,'pdf');
			else if (/^(txt|text)$/i.test(ext)) types.splice(2,0,'text');
			else if (/^(jpg|jpeg|png|gif|bmp|tiff|ico|pcx|ai|psd|pdd|eps)$/i.test(ext)) types.splice(2,0,'image');
			else if (/^(bat|js|json|html|css|xml|yaml|php|py|pl|properties|tpl|htpl)$/i.test(ext)) types.splice(2,0,'code');
			else if (/^(rar|zip|7z|tar|gz|dmg|cab|lha|arj|arc|ace|bkf)$/i.test(ext)) types.splice(2,0,'archive');
			else if (/^(amr|aac|acc|au|aiff|wma|wav|mp3|flac|dts|vox)$/i.test(ext)) types.splice(2,0,'audio');
			else if (/^(avi|mpeg|mpg|mpe|mkv|mov|wmv|fla|flr|m4v|dat)$/i.test(ext)) types.splice(2,0,'video');
			else if (/^(doc|dot|wbk)/i.test(ext)) types.splice(2,0,'word');
			else if (/^(xls|xlt|xlm|xla|xlw|xll)/i.test(ext)) types.splice(2,0,'excel');
			else if (/^(ppt|pot|pps|ppa|sld)/i.test(ext)) types.splice(2,0,'powerpoint');
			else if (/^(eml|email|gup|gwi|mmf)/i.test(ext)) types.splice(1,1,'envelope');
			return 'x-fa ' + types.join('-');
		},
		
		priorityColorCls: function(priority) {
			switch ((priority||'').toUpperCase()) {
				case 'VERY_HIGH': return 'soft-red';
				case 'HIGH'		: return 'soft-olive';
				case 'LOW'		: return 'soft-blue';
				case 'VERY_LOW'	: return 'soft-green';
				case 'NORMAL'	:
				default			: return '';
			}
		}

	}
	
});