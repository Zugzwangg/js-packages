Ext.define('CFJS.api.Api', {
	extend				: 'Ext.data.Connection',
	alternateClassName	: 'CFJS.Api',
	singleton			: true,
	autoAbort			: false,
	config				: {
		clientPermissionLevel	: CFJS.shortMax,
		handlerUri				: 'handler/api',
		// 2 minutes
		sessionInterval			: 120000,
		usePermissions			: false
	},

	applySession: function(session) {
		if (session) {
			var expires = session.expires || 0;
			if (!Ext.isDate(expires)) {
				try { expires = Date.parse(expires); } 
				catch (e) { console.error(e); }
			}
			if (!expires || isNaN(expires)) expires = 0;
			session.refreshTime = expires - this.getSessionInterval();
			session.idleTime = session.refreshTime - new Date().getTime();
		}
		return session || {};
	},
	
	applySessionInterval: function(interval) {
		return !isNaN(interval = parseInt(interval)) ? interval : 0;
	},
	
	authorize: function(service) {
		var me = this;
		location.assign(me.controllerUrl('login') + '?' + Ext.Object.toQueryString({
			app_id			: me.getExtraParams().app_id,
			serial_number	: me.getSerial(),
			service			: service,
			next			: location.origin + location.pathname,
			type			: 'query'
		}));
	},

	clearSessionCookies: function(session) {
		CFJS.deleteCookie('SID');
		CFJS.deleteCookie('UID');
		CFJS.deleteCookie('SCID');
		CFJS.deleteCookie('UTYPE');
		CFJS.deleteCookie('SID_EXP');
		CFJS.deleteCookie('SID_RF');
	},
	
	findSession: function() {
		var me = this, session = me.getSessionFromCookies();
		return session.sid && session.uid ? session : me.getSessionFromUrl();
	},

	getSessionFromCookies: function() {
		return {
			sid: CFJS.getCookie('SID'),
			uid: CFJS.getCookie('UID'),
			scid: CFJS.getCookie('SCID'),
			utype: CFJS.getCookie('UTYPE'),
			expires: CFJS.getCookie('SID_EXP'),
			refreshToken: CFJS.getCookie('SID_RF')
		};
	},

    getSessionFromUrl: function() {
    	var params = CFJS.fromQueryString(location.search.length > 0 ? location.search : location.hash);
    	return {
			sid: params.sid,
			uid: params.uid,
			scid: params.scid,
			utype: params.utype,
			expires: params.expires,
			refreshToken: params.refreshToken
    	};
    },

	initialize: function(options) {
		var me = this;
		if (!Ext.isEmpty((options = Ext.clone(options || {})).serial)) me.setSerial(options.serial);
		var callback = { success: options.success || Ext.emptyFn, failure: options.failure || Ext.emptyFn };
		me.request(Ext.apply(options, {
			url			: me.controllerUrl('authenticate') + '/' + me.getService() +'.crt',
			params		: { serial_number: me.getSerial() },
			signParams	: false,
			success		: function(context) {
				try {
					me.setPublicKey(KEYUTIL.getKey(context.responseText));
				} catch (e) {
					callback.failure(context, options, me.extractResponse(context));
					return null;
				}
				me.request(Ext.apply(options, {
					url			: me.controllerUrl('authenticate') + '/init',
					params		: { serial_number: me.getSerial(), password: Math.random().toString(36).slice(-8) },
					signParams	: false,
					success		: function(context) {
						try {
							me.setPrivateKey(KEYUTIL.getKey(context.responseText, options.params.password));
						} catch (e) {
							callback.failure(me.extractResponse(context));
							return null;
						}
						callback.success(context);
					}
				}));
			}
		}));
	},
	
	isSessionExpired: function(session) {
		try { 
			session = session || this.getSession();
			var expires = Date.parse(session.expires);
			return !this.getSecure() && Ext.isEmpty(session.sid) || Ext.isEmpty(session.uid) || (expires || 0) <= new Date().getTime();
		} catch (e) { console.error(e) }
		return false;
	},
	
	loadManifest: function(options) {
		options = options || {};
		var me = this, failure = options.failure, animateTarget = options.animateTarget;
		CFJS.loadManifest(Ext.apply({}, {
			success: function(config) {
				var apiConfig = Ext.isFunction(options.extractApi) ? options.extractApi(config) : (config || {}).api;
				me.updateConfig(apiConfig, {
					success: options.success,
					failure: function(response, options, responseData) {
						if (Ext.isEmpty(responseData)) {
							if (response.status == 0 && Ext.isEmpty(response.statusText)) response.statusText = 'Connection refused';
							responseData = { error: { code: response.status, text: response.statusText } };
						}
						CFJS.errorAlert('Application initialization failed', responseData.error, animateTarget, function() { 
							Ext.callback(failure, me, [responseData]); 
						});
					}
				});
			}
		}, options));
	},
	
	loadPermissions: function(user, success, failure) {
		var me = this,
			callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		if (Ext.isEmpty(user)) {
			callback.failure({ code: 404, text: 'Can\'t find permissions for user with empty user name!' });
			return;
		}
		me.request({
			url					: me.controllerUrl('permissions') + '/' + user,
			disableExtraParams	: true,
			signParams			: false,
			withCredentials		: false,
			success				: function(context) {
				if (Ext.isFunction(callback.success)) {
					context = Ext.decode(context.responseText);
					var data = {}, i = 0, obj, key;
					for(; i < context.length; i++) {
						obj = context[i];
						for (key in obj) {
							if (obj.hasOwnProperty(key)) {
								CFJS.merge(data, CFJS.asObject(key, obj[key]) || {});
							}
						}
					}
					callback.success(data);
				}
			},
			failure	: callback.failure
		});
	},
	
	login: function(user, pwd, service, saveCookie, success, failure) {
		if (Ext.isObject(service)) service = service.id;
		var me = this, callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		me.request({
			url		: me.controllerUrl('authenticate') + '/' + (service || me.getService()) + '/login',
			params	: { username: user, password: pwd },
			success	: function(context) {
				var response = me.extractResponse(context);
				me.updateSessionData({
					saveCookie	: saveCookie,
					session		: response.transaction, 
					error		: response.error, 
					callback	: callback 
				});
			},
			failure	: callback.failure
		});
	},
	
	loginAs: function(uid, service, saveCookie, success, failure) {
		if (Ext.isObject(service)) service = service.id;
		var me = this, callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		me.request({
			url		: me.controllerUrl('authenticate') + '/' + (service || me.getService()) + '/loginAs',
			params	: { uid: uid },
			success	: function(context) {
				var response = me.extractResponse(context);
				me.updateSessionData({
					saveCookie	: saveCookie,
					session		: response.transaction, 
					error		: response.error, 
					callback	: callback 
				});
			},
			failure	: callback.failure
		});
	},

	logout: function(success, failure) {
		var me = this, session = me.getSession(),
			callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		
		if (!Ext.Object.isEmpty(session)) {
			me.request({
				url		: me.controllerUrl('authenticate') + '/logout',
				success	: function(context) {
					me.updateSessionData({
						saveCookie	: true,
						session		: {},
						error		: null, 
						callback	: callback
					});
				},
				failure	: callback.failure
			});
		} else callback.success();
	},
	
	needSessionRefresh: function(session) {
		return (session = session || this.getSession()) && !Ext.isEmpty(session.sid) 
			&& Ext.isNumber(session.refreshTime) && session.refreshTime <= new Date().getTime();
	},

	recoveryEmail: function(email, success, failure) {
		var me = this, callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		me.request({
			url		: me.controllerUrl('authenticate') + '/recovery_password_message',
			params	: { transport: 'email', contact: email },
			success	: function(context) {
				var response = me.extractResponse(context);
				if (Ext.Object.isEmpty(response.error)) {
					Ext.callback(callback.success, me);
				} else Ext.callback(callback.failure, [response.error]); 
			},
			failure	: callback.failure
		});
	},
	
	refreshSession: function(service, saveCookie, success, failure) {
		var me = this, session = me.getSession(),
			callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		if (!Ext.Object.isEmpty(session)) {
			if (!Ext.isEmpty(service)) me.setService(service);
			session.refreshTime = null;
			me.request({
				url: me.controllerUrl('authenticate') + '/' + me.getService() + '/refresh',
				params: { token: session.refreshToken },
				success:  function(context) {
					var response = me.extractResponse(context);
					me.updateSessionData({
						saveCookie	: saveCookie,
						session		: response.transaction,
						error		: response.error,
						callback	: callback 
					});
				},
				failure: callback.failure
			});
		} else callback.success();
	},

	resetPassword: function(password, expiration, success, failure) {
		var me = this, params = { password: password, uid: (me.getSession()||{}).uid },
			callback = { success: success || Ext.emptyFn, failure: failure || Ext.emptyFn };
		if (expiration > 0) params.expiration = expiration;
		me.request({
			url		: me.controllerUrl('authenticate') + '/reset_password',
			params	: params,
			success	: function(context) {
				var response = me.extractResponse(context);
				if (Ext.Object.isEmpty(response.error)) {
					Ext.callback(callback.success, me);
				} else Ext.callback(callback.failure, [response.error]); 
			},
			failure	: callback.failure
		});
	},

	saveSessionToCookies: function(session) {
		var me = this, session = session || me.getSession();
		me.setCookie('SID', session.sid);
		me.setCookie('UID', session.uid);
		me.setCookie('SCID', session.scid || 0);
		me.setCookie('UTYPE', session.utype || '');
		me.setCookie('SID_EXP', session.expires);
		me.setCookie('SID_RF', session.refreshToken);
	},

	setCookie: function(name, value, expires, path) {
		if (value) CFJS.setCookie(name, value, expires || (24 * 60), path || CFJS.portalPath());
		else CFJS.deleteCookie(name, path || CFJS.portalPath());
	},

	updateConfig: function(config, options) {
		var me = CFJS.Api;
		me.setConfig(CFJS.merge(me.getConfig(), config || {}));
		me.initialize(options);
	},

	updateSessionData: function(options) {
		options = options || {};
		var me = options.scope || this,
			session = options.session || {},
			error = options.error,
			callback = options.callback;
		me.setSession(session);
		if (options.saveCookie) me.saveSessionToCookies(session);
		if (!Ext.Object.isEmpty(callback)) {
			if (Ext.Object.isEmpty(error)) {
				Ext.callback(callback.success, callback.scope|| me, [session]);
			} else Ext.callback(callback.failure, callback.scope|| me, [error]); 
		}
	}

}, function() {
	Ext.Ajax = CFJS.Api;
});
