Ext.define('CFJS.api.writer.Json', {
	extend				: 'Ext.data.writer.Json',
	alternateClassName	: ['CFJS.api.JsonWriter'],
	alias				: 'writer.json.cfapi',
	requires			: [ 'CFJS.Api' ],
	config: {
		dateFormat 			: CFJS.Api.dateTimeFormat,
		expandData			: true,
		writeAllFields		: true,
		writeNulls			: false,
		
		transform			: function(data, request) {
			var proxy = request.getProxy();
			if (proxy) {
				var loaderConfig = proxy.getLoaderConfig();
				if (loaderConfig && loaderConfig.dictionaryType) {
					request.getParams()['type'] = loaderConfig.dictionaryType;
				}
				if (data) {
					var me = this,
						model = proxy.getModel(),
						fieldsMap = model ? model.getFieldsMap() : null,
						records = Ext.isArray(data) ? data : [data],
						mapped = {}, key, field, i, record, value;
					for (key in fieldsMap) {
						field = fieldsMap[key];
						if (field.hasMapping()) mapped[key] = field;
					}
					for (i = 0; i < records.length; i++) {
						record = records[i];
						for (key in mapped) {
							field = mapped[key];
							value = record[field.getName()];
							delete record[field.getName()];
							me.setMappedProperty(record, value, field.getMapping());
						}
						if (!me.writeNulls) me.clearNulls(record);
					}
				}
			}
			return data;
		}
	},
	
	clearNulls: function(data) {
		for (var prop in data) {
			if (data[prop] === null) data[prop] = undefined;
			else if (Ext.isObject(data[prop])) this.clearNulls(data[prop]);
		}
	},
	
	setMappedProperty: function(record, value, mapping) {
		if (mapping) {
			var paths = mapping.split('.'), 
				val = record[paths[0]] = record[paths[0]] || {}, 
				length = paths.length - 1,  i;
			for (i = 1; i < length; i++) {
				val = val[paths[i]] = val[paths[i]] || {};
			}
			if (!value || value === '') delete val[paths[length]];
			else val[paths[length]] = value;
		}
	}

});