Ext.define('CFJS.api.proxy.Json', {
	extend				: 'CFJS.api.proxy.Server',
	alternateClassName	: ['CFJS.api.JsonProxy'],
	requires			: [ 'CFJS.api.reader.Json', 'Ext.data.writer.Json' ],
	alias				: 'proxy.json.cfapi',
	reader				: 'json.cfapi',
	writer				: 'json.cfapi',
	
	writeBody: function(request, operation) {
		var me = this, apiParams = me.getApi()[request.getAction()], 
			type = Ext.isString(apiParams) ? apiParams : apiParams['transaction'],
			bodyParams = CFJS.merge(me.getParams(operation), request.getJsonData() || {});
			
		request.setJsonData(CFJS.Api.buildJsonPayload(bodyParams, type.toUpperCase()));
		return request;
	}
	
});