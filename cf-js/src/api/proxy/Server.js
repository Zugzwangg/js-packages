Ext.define('CFJS.api.proxy.Server', {
	extend				: 'Ext.data.proxy.Server',
	alternateClassName	: ['CFJS.api.ServerProxy'],
	alias 				: 'proxy.server-cfapi',
	batchActions		: false,
	cfapi				: CFJS.Api,
	config				: {
		actionMethods	: {
			create	: 'POST',
            destroy	: 'POST',
            read	: 'POST',
            sign	: 'POST',
            summary	: 'POST',
            update	: 'POST'
		},
		api				: {
			create	: 'new',
			destroy	: 'remove',
			read	: 'load',
			sign	: 'sign',
			summary	: 'summary',
			update  : 'save'
		},
		directionParam	: 'sortDir',
		filtersParam	: 'filterConfigs',
		sortersParam	: 'sortInfo',
		summaryParam	: 'summary',
		summariesParam	: 'summaryInfo',
		headers			: undefined,
		noCache			: false,
		service			: undefined,
		startParam		: 'offset',
		loaderConfig	: {
			dictionaryType	: undefined,
			currency		: undefined
		}
	},
	defaultActionMethods: {
		create	: 'POST',
		read	: 'POST',
		update	: 'POST',
        sign	: 'POST',
        summary	: 'POST',
		destroy	: 'POST'    
	},
	isCFJSProxy			: true,

	abort: function(request) {
		request = request || this.lastRequest;
		if (request) this.cfapi.abort(request.getRawRequest());
	},
	
	addLoaderConfig: function(options) {
		this.setLoaderConfig(Ext.apply(this.getLoaderConfig() || {}, options))
	},
	
	afterRequest: function(request, success) {
		var operation = request.getOperation();
		if (operation.error) {
			if (!operation.allowWrite() && operation.error.status < 0) return;
			var str = 'чтения';
			if (operation.isSignOperation) str = 'подписи';
			else if (operation.isUpdateOperation) str = 'записи';  
			else if (operation.isDestroyOperation) str = 'удаления';  
			else if (operation.isCreateOperation) str = 'добавления';
			CFJS.errorAlert('Ошибка ' + str + ' данных', operation.error);
			return;
		}
		if (success && operation.allowWrite()) {
			var str = 'записаны';
			if (operation.isCreateOperation) str = 'добавлены';
			else if (operation.isDestroyOperation) str = 'удалены';  
			else if (operation.isSignOperation) str = 'подписаны';  
			Ext.toast('Данные успешно ' + str, false, 'br');
		}
	},
	
	applyLoaderConfig: function(loaderConfig) {
		return CFJS.clearEmptyProperties(loaderConfig, true);
    },
    
	buildRequest: function(operation) {
		var me = this, api = me.cfapi,
			action = operation.getAction(),
			apiParams = me.getApi()[action],
			apiMethod = api.controllerMethod(me.getService() || api.getService()),
			initialParams = Ext.apply({}, operation.getParams()),
			params = Ext.applyIf(initialParams, me.getExtraParams() || {}),
			request;

		params['method'] = Ext.isString(apiParams) ? apiParams : apiParams['method'];
		if (!Ext.isEmpty(apiMethod)) params['method'] = apiMethod + '.' + params['method'];
		
		request = new Ext.data.Request({
			url					: operation.getUrl(),
			action				: action,
			records				: operation.getRecords(),
			operation			: operation,
			disableExtraParams	: me.disableExtraParams,
			params				: params,
			proxy				: me
		});
		request.setUrl(me.buildUrl(request));
		operation.setRequest(request);
		return request;
	},

	createRequestCallback: function(request, operation) {
		var me = this;
		return function(options, success, response) {
			if (request === me.lastRequest) me.lastRequest = null;
			me.processResponse(success, operation, request, response);
		};
	},
	
	destroy: function() {
		this.lastRequest = null;
		this.callParent();
	},
	
	doRequest: function(operation) {
		var me = this, api = me.cfapi, 
			service = me.getService() || api.getService();
		if (api.isSessionExpired()) location.reload();
		var writer = me.getWriter(),
			request = me.buildRequest(operation),
			method  = me.getMethod(request);
		if (writer && operation.allowWrite()) {
			request = writer.write(request);
		}
		request.setConfig({
			method			: method,
			headers			: api.getDefaultPostHeader(),
			timeout			: me.getTimeout(),
			scope			: me,
			disableCaching	: false
		});
		if (method.toUpperCase() === 'GET') {
			request.setParams(Ext.apply(request.getParams(), me.getLoaderConfig()));
		} else request = me.writeBody(request, operation);
		request.setCallback(me.createRequestCallback(request, operation));
		return me.sendRequest(request);
	},
	
	encodeFilters: function(filters) {
		var out = [], length = filters.length, i, filter;
		for (i = 0; i < length; i++) {
        	if (filter = this.serializeFilter(filters[i])) out.push(filter);
        }
        return out;
    },
    
	encodeSorters: function(sorters) {
		var out = [], length = sorters.length, i, sorter;
		for (i = 0; i < length; i++) {
			if (sorter = this.serializeSorter(sorters[i])) out.push(sorter);
		}
        return out;
    },
    
    encodeSummaries: function(summaries) {
		var out = [], length = summaries.length, i, summary;
		for (i = 0; i < length; i++) {
			if (summary = this.serializeSummary(summaries[i])) out.push(summary);
		}
        return out;
    },
    
	/**
     * @private
     * Copy any sorters, filters etc into the params so they can be sent over the wire
     */
	getParams: function(operation) {
		var me = this,
			params = {};
		if (operation.isReadOperation || operation.isSignOperation) {
			params = Ext.apply(params, me.getLoaderConfig() || {});
			if (operation.getId()) params['id'] = operation.getId();
			if (operation.isReadOperation) {
				var	start = operation.getStart(),
					startParam = me.getStartParam(),
					limit = operation.getLimit(),
					limitParam = me.getLimitParam(),
					grouper = operation.getGrouper(),
					groupParam = me.getGroupParam(),
					sorters = operation.getSorters(),
					sortParam = me.getSortParam(),
					sortersParam = me.getSortersParam(),
					summaries = operation.getSummaries(),
					summaryParam = me.getSummaryParam(),
					summariesParam = me.getSummariesParam(),
					filters = operation.getFilters(),
					filterParam = me.getFilterParam(),
					filtersParam = me.getFiltersParam();
				
				if (startParam && (start || start === 0)) {
					params[startParam] = start;
				}
				if (limitParam && limit) {
					params[limitParam] = limit;
				}
				if (groupParam && grouper) {
					params[groupParam] = me.serializeSorter(grouper);
				}
				if (summariesParam && summaryParam && summaries && summaries.length > 0) {
					(params[summariesParam] = {})[summaryParam] = me.encodeSummaries(summaries);
				}
				if (sortersParam && sortParam && sorters && sorters.length > 0) {
					(params[sortersParam] = {})[sortParam] = me.encodeSorters(sorters);
				}
				if (filtersParam && filterParam && filters && filters.length > 0) {
					(params[filtersParam] = {})[filterParam] = me.encodeFilters(filters);
				}
			}
		}
		return params;
	},
	
    getMethod: function(request) {
    	var actions = this.getActionMethods(),
    		action = request.getAction(),
    		method;
    	if (actions) method = actions[action];
    	return method || this.defaultActionMethods[action];
    },
    
    getUrl: function(request) {
        var me = this, api = me.cfapi, url;
        if (request) url = request.getUrl() || api.controllerUrl(me.getService() || api.getService());
        return url ? url : me.callParent();
    },
    
	sendRequest: function(request) {
		request.setRawRequest(this.cfapi.request(request.getCurrentConfig()));
		this.lastRequest = request;
		return request;
	},

	serializeComparison: function(type, operator) {
		if (type === 'string') {
			switch (operator) {
				case '=': case '==': case '===': return 'eq'; 
				case 'like': return 'contains';
				default: break;
			}
    	} else if (type === 'numeric') {
    		switch (operator) {
				case '=': case '==': case '===': return 'eq'; 
				case '!=': case '!==': return '!eq'; 
				case '<': return 'lt'; 
				case '>=': return '!lt'; 
				case '>': return 'gt'; 
				case '<=': return '!gt'; 
				default: break;
    		}
    	} else if (type === 'date' || type === 'date-time') {
    		switch (operator) {
				case '=': case '==': case '===': case 'eq': return 'on'; 
				case '!=': case '!==': case '!eq': return '!on'; 
				case '<': case 'lt': return 'before'; 
				case '>=': case '!lt': return '!before'; 
				case '>': case 'gt': return 'after'; 
				case '<=': case '!gt': return '!after'; 
				default: break;
			}
    	} else if (type === 'boolean') return 'eq';
		return operator;
    },
    
	serializeFilter: function(filter) {
		var me = this, disabled = filter.getDisabled(),
			field = filter && filter.getProperty(),
			type = (filter && filter.type) || 'string',
			value = filter && filter.getValue(),
			result;
		if (disabled || !field) return null;
		if (type === 'number') type = 'numeric';
		result = { field: field, type: type, comparison: me.serializeComparison(type, filter.getOperator()) };
		if (Ext.isEmpty(value)) return result;
		switch (type) {
			case 'boolean':
				result.value = value === 'false' || value === false ? false : new Boolean(value).valueOf();
				break;
			case 'date':
			case 'date-time':
				if (Ext.isString(value)) value = Date.parse(value);
				if (Ext.isDate(value)) {
					result.value = me.cfapi[type === 'date-time' ? 'formatDateTime' : 'formatDate'](value);
					break;
				}
			// if date filter is time or is number filter
			case 'numeric':
				if (!isNaN(number = parseFloat(value))) result.value = number;
				break;
			case 'string':
			default: 
				result.value = value.toString();
				break;
		}
		return result;
	},
	
	serializeSorter: function(sorter) {
		var field = sorter ? sorter.getProperty() : null;
		return field ? { sortField: field, sortDir: sorter.getDirection() || 'ASC' } : null;
	},
	
	serializeSummary: function(summary) {
		var field = summary ? summary.property : null;
		return field ? { field: field, type: summary.type || 'COUNT' } : null;
	},
	
    sign: function() {
        return this.doRequest.apply(this, arguments);
    },

	updateLoaderConfig: function(loaderConfig) {
		if (loaderConfig && !loaderConfig.locale) loaderConfig.locale = CFJS.getLocale();
	},

	writeBody: Ext.emptyFn
	
});