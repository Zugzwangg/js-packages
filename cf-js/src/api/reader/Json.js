Ext.define('CFJS.api.reader.Json', {
	extend				: 'Ext.data.reader.Json',
	alternateClassName	: ['CFJS.api.JsonReader'],
	alias				: 'reader.json.cfapi',
	config				: {
		rootProperty	: 'response.transaction.list',
		totalProperty	: 'response.transaction.totalLength',
		messageProperty	: 'response.error',
		successProperty : 'response.success',
		summaryProperty	: 'response.transaction.summary_result',
		summaryModel	: 'Ext.data.Model',
		
		transform: function(data) {
			if (data && data.response && data.response.error) data.response.success = false;
			return data;
		}
	},
	
	readRecords: function(data, readOptions, /* private */ internalReadOptions) {
		var me = this, summary = me.getSummary(data);
		if (!summary) return me.callParent(arguments);
		me.buildExtractors();
		var records = me.extractData(summary, Ext.apply(readOptions || {}, {
				model: me.getSummaryModel() || 'Ext.data.Model'
			}));
		return new Ext.data.ResultSet({
			total  : records.length,
			count  : records.length,
			records: records,
			success: true,
			message: undefined
        });
	},
	
	updateSummaryProperty: function() {
        this.forceBuildExtractors();    
    },
    
	buildExtractors : function(force) {
        var me = this, summaryProp;
        // Will only return true if we need to build
        if (force || !me.hasExtractors) {
        	summaryProp = me.getSummaryProperty();
            if (summaryProp) {
                me.getSummary = me.getAccessor(summaryProp);
            } else {
                me.getSummary = Ext.identityFn;
            }
            me.callParent(arguments);
        }
    }
});