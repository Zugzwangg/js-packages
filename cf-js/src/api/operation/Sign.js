Ext.define('CFJS.api.operation.Sign', {
    extend: 'Ext.data.operation.Update',
    alias: 'data.operation.sign',
    action: 'sign',
    isSignOperation: true,
    order: 25,
    _commitSetOptions: { convert: true, commit: true, sign: true },
    
    doExecute: function() {
        return this.getProxy().sign(this);
    }
 
});