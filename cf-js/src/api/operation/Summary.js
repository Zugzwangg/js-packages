Ext.define('CFJS.api.operation.Summary', {
    extend: 'Ext.data.operation.Read',
    alias: 'data.operation.summary',
    action: 'summary',
    isSummaryOperation: true
    
});