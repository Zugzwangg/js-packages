Ext.define('CFJS.exports.XmlToJson', {
	alternateClassName 	: 'CFJS.XmlToJson',
	singleton 			: true,
	requires			: [	'CFJS' ],
	prefixMatch			: new RegExp(/(?!xmlns)^.*:/),
	trimMatch			: new RegExp(/^\s+|\s+$/g),
	config				: {
		mergeCDATA		: true, // extract cdata and merge with text
        grokAttr		: true, // convert truthy attributes to boolean, etc
        grokText		: true, // convert truthy text/attr to boolean, etc
        normalize		: true, // collapse multiple spaces to single space
        xmlns			: true, // include namespaces as attribute in output
        namespaceKey	: '_ns', // tag name for namespace objects
        textKey			: '_text', // tag name for text nodes
        valueKey		: '_value', // tag name for attribute values
        attrKey			: '_attr', // tag for attr groups
        cdataKey		: '_cdata', // tag for cdata nodes (ignored if mergeCDATA is true)
        attrsAsObject	: true, // if false, key is used as prefix to name, set prefix to '' to merge children and attrs.
        stripAttrPrefix	: true, // remove namespace prefixes from attributes
        stripElemPrefix	: true, // for elements of same name in diff namespaces, you can enable namespaces and access the nskey property
        childrenAsArray	: false // force children into arrays
	},
	
	grokType: function (sValue) {
        if (/^\s*$/.test(sValue)) return null;
        if (/^(?:true|false)$/i.test(sValue)) return sValue.toLowerCase() === "true";
        if (isFinite(sValue)) return parseFloat(sValue);
        return sValue;
    },
    
    parseString: function (xmlString, opt) {
    	var me = CFJS.XmlToJson;
        return me.parseXML(me.stringToXML(xmlString), opt);
    },

	parseXML: function (oXMLParent, opt) {
		var me = CFJS.XmlToJson, config = me.config,
			vResult = {}, nLength = 0, sCollectedTxt = "", key;
		// initialize config
		for (key in opt) {
			config[key] = opt[key];
		}
		// parse namespace information
		if (config.xmlns && oXMLParent.namespaceURI) {
			vResult[config.namespaceKey] = oXMLParent.namespaceURI;
		}
		// parse attributes
		// using attributes property instead of hasAttributes method to support older browsers
		if (oXMLParent.attributes && oXMLParent.attributes.length > 0) {
			var vAttribs = {};
			for (nLength; nLength < oXMLParent.attributes.length; nLength++) {
				var oAttrib = oXMLParent.attributes.item(nLength), vContent = {}, attribName = '';
				if (config.stripAttrPrefix) attribName = oAttrib.name.replace(me.prefixMatch, '');
				else attribName = oAttrib.name;
				if (config.grokAttr) vContent[config.valueKey] = me.grokType(oAttrib.value.replace(me.trimMatch, ''));
				else vContent[config.valueKey] = oAttrib.value.replace(me.trimMatch, '');
				if (config.xmlns && oAttrib.namespaceURI) vContent[config.namespaceKey] = oAttrib.namespaceURI;
				// attributes with same local name must enable prefixes
				if (config.attrsAsObject) vAttribs[attribName] = vContent;
				else vResult[config.attrKey + attribName] = vContent;
			}
			if (config.attrsAsObject) vResult[config.attrKey] = vAttribs;
		}
		// iterate over the children
		if (oXMLParent.hasChildNodes()) {
			for (var oNode, sProp, vContent, nItem = 0; nItem < oXMLParent.childNodes.length; nItem++) {
				oNode = oXMLParent.childNodes.item(nItem);
				// nodeType is "CDATASection" (4)
				if (oNode.nodeType === 4) {
					if (config.mergeCDATA) sCollectedTxt += oNode.nodeValue;
					else {
						if (vResult.hasOwnProperty(config.cdataKey)) {
							if (vResult[config.cdataKey].constructor !== Array) vResult[config.cdataKey] = [vResult[config.cdataKey]];
							vResult[config.cdataKey].push(oNode.nodeValue);
						} else {
							if (config.childrenAsArray) {
								vResult[config.cdataKey] = [];
								vResult[config.cdataKey].push(oNode.nodeValue);
							} else vResult[config.cdataKey] = oNode.nodeValue;
						}
					}
					 
				} 
				// nodeType is "Text" (3)
				else if (oNode.nodeType === 3) sCollectedTxt += oNode.nodeValue;
				// nodeType is "Element" (1)
				else if (oNode.nodeType === 1) {
					if (nLength === 0) vResult = {};
					// using nodeName to support browser (IE) implementation with no 'localName' property
					if (config.stripElemPrefix) sProp = oNode.nodeName.replace(me.prefixMatch, '');
					else sProp = oNode.nodeName;
					vContent = me.parseXML(oNode);
					if (vResult.hasOwnProperty(sProp)) {
						if (vResult[sProp].constructor !== Array) vResult[sProp] = [vResult[sProp]];
						vResult[sProp].push(vContent);
					} else {
						if (config.childrenAsArray) {
							vResult[sProp] = [];
							vResult[sProp].push(vContent);
						} else vResult[sProp] = vContent;
						nLength++;
					}
				}
			}
			// no children and no text, return null			
		} else if (!sCollectedTxt) { 
			if (config.childrenAsArray) {
				vResult = [];
				vResult.push(null);
			} else vResult = null;
		} 
		if (sCollectedTxt) {
			 var value = sCollectedTxt.replace(me.trimMatch, '');
			 if (config.grokText) value = me.grokType(value);
			 else if (config.normalize) value = value.replace(/\s+/g, " ");
			 if (Ext.Object.isEmpty(vResult)) vResult = value;
			 else vResult[config.textKey] = value; 
		}
		return vResult;
	},

	// Convert xmlDocument to a string
	// Returns null on failure
	xmlToString: function (xmlDoc) {
		try {
			var xmlString = xmlDoc.xml ? xmlDoc.xml : (new XMLSerializer()).serializeToString(xmlDoc);
			return xmlString;
		} catch (err) {
			return null;
		}
	},

	// Convert a string to XML Node Structure
	// Returns null on failure
	stringToXML: function (xmlString) {
		try {
			var xmlDoc = null;
			if (window.DOMParser) {
				var parser = new DOMParser();
				xmlDoc = parser.parseFromString(xmlString, "text/xml");
				return xmlDoc;
			} else {
				xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = false;
				xmlDoc.loadXML(xmlString);
				return xmlDoc;
			}
		} catch (e) {
			return null;
		}
	}
});