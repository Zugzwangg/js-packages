Ext.define('CFJS.exports.JSToXml', {
	alternateClassName 	: 'CFJS.JSToXml',
	singleton 			: true,
	requires			: [	'CFJS.exports.XmlToJson' ],
	attrKey				: '@',
	documentRoot		: 'list',
	header				: '<?xml version="1.0" encoding="utf-8"?>',
	recordName			: 'record',
	selectorRe			: /[^>\s]+/g,
	valueKey			: '$',
	
	toXML: function(data, header) {
		try {
			return CFJS.XmlToJson.stringToXML(CFJS.JSToXml.toXMLString(data, header));
		} catch (e) {
			return null;
		}
	},
	
	toXMLString: function(data, options) {
		options = options || {};
		if (!Ext.isArray(data)) data = [data];
		var me = CFJS.JSToXml,
			xml = [],
			recordName = options.record || me.recordName,
			record = recordName.match(me.selectorRe),
			recLen = record.length,
			len = data.length, i;
		xml.push(options.header || me.header || '');
		if (len !== 1 && recLen === 1) {
			record.unshift(options.root || me.documentRoot);
			recLen = record.length;
		}
		for (i = 0; i < recLen - 1; i++) {
            xml.push('<', record[i], '>');
        }
		recordName = record[i];
		for (i = 0; i < len; ++i) {
            me.objectToElement(recordName, data[i], xml);
        }
		for (i = recLen - 2; i > -1; i--) {
            xml.push('</', record[i], '>');
        }
		return xml.join('');
	},
	
	objectToElement: function(name, data, output) {
        var me = CFJS.JSToXml,
        	childOutput = [],
        	key, value, 
        	children, childs, childKeys, childKeysLen,
            i, lastChild, lastKey;
        output = output || [];
        output.push('<', name);
        for (key in data) {
        	value = data[key];
            // Attribute node
            if (key[0] === me.attrKey) output.push(' ', key.substr(1), '="', value, '"');
            else if (key[0] === me.valueKey) childOutput.push(value);
            // Child element node 
            // Object properties become child elements 
            else if (typeof value === 'object') me.objectToElement(key, value, childOutput);
            else {
            	// Is it a selector? 
            	childKeys = key.match(me.selectorRe);
            	// key was eg "foo > bar". 
            	// Ensure looks like contains {foo: {bar: {}}} 
            	if ((childKeysLen = childKeys.length) > 1) {
            		childs = childs || {};
            		for (children = childs, i = 0; i < childKeysLen; i++) {
            			lastChild = children;
            			lastKey = childKeys[i];
            			children = children[lastKey] || (children[lastKey] = {});
            		}
            		// lastChild is now the bar property in the above example 
            		lastChild[lastKey] = value;
            	} else childOutput.push('<', key, '>', value, '</', key, '>');
            }
        }
        output.push('>');
        output.push.apply(output, childOutput);
        // Output any embedded nodes that were specified by child element mappings like "SystemInfo>SystemName" 
        if (childs) {
            for (key in childs) {
                me.objectToElement(key, childs[key], output);
            }
        }
        output.push('</', name, '>');
        return output;
    }

});