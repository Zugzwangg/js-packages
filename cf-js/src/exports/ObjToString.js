Ext.define('CFJS.exports.ObjToString', {
	alternateClassName 	: 'CFJS.ObjToString',
	singleton 			: true,
	requires			: [	'CFJS' ],

	objToString: function(obj, allowNulls, key, prevKey) {
		var me = CFJS.ObjToString, fullKey = key || '', ret = [], arr = [], ch = '\\' + '"', fld;
		if (fullKey && prevKey) fullKey = [prevKey, fullKey].join('.');
		if ((obj === null || obj === undefined)) {
			if (allowNulls ) {
				if (fullKey) ret.push(fullKey);
				ret.push('null');
				return ret.join('=');
			} else return;
		}
		if (fullKey && !( key && (key.indexOf('@') !== -1) ) )
			ret.push(fullKey);
		if (Ext.isFunction(obj)) ret.push('');
		else if ( (key === 'parent') || (key === '') || (key === '__proto__') ) return;
		else if ( key && (key.indexOf('@') !== -1) && Ext.isString(obj)) {
			// key include '@'
			if (prevKey) {
				ret.push(prevKey + "['" + key + "']");
				ret.push('"' + obj.replace(/"/g, ch ) + '"');
				return ret.join('=');
			} else {
				console.warn('objToString: field with "@" in root.')
				return;
			}
		}
		else if (Ext.isDate(obj)) ret.push( 'new Date(Date.parse("' + CFJS.Api.formatDateTime(obj) + '"))');
		else if (Ext.isString(obj)) ret.push('"' + obj.replace(/"/g, ch ) + '"');
		else if (Ext.isObject(obj)) {
			if (fullKey) {
				ret.push('{}');
				ret = ret.join('=') + ';';
			} else {
				ret = '';
			}
			Ext.Object.each(obj, function(key, value) {
				if (value = me.objToString(value, allowNulls, key, fullKey)) arr.push(value);
			});
			return ret + arr.join(';');
		} else if (Ext.isArray(obj)) {
			Ext.Array.each(obj, function(key, value) {
				if (value = me.objToString(value, allowNulls, null, null)) arr.push(value);
			});
			ret.push('[' + arr + ']');
		} else ret.push(obj);
		return ret.join('=');
	},
	
	objFromString: function(obj, strObj) {
		if (Ext.isEmpty(strObj)) return obj;
		var string = 'obj.' + strObj.split(';').join(';obj.') + ';';
		try	{
			eval(string);
		} catch(e) 	{
			console.error('Error objFromString: ', e.name, 'string: ', string);
		}
		return obj;
	}
});
