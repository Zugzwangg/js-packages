Ext.define('CFJS.app.InlineEditorViewModel', {
	extend				: 'CFJS.app.BaseEditorModel',
	alias				: 'viewmodel.inlineeditor',
	isInlineEditorModel	: true,
	expressionRe		: /^(?:\{(?:(\d+)|([a-z_][\w\$\@\-\.]*))\})$/i,
	
	getParentItem: function() {
		var parent = this.getParent(); 
		return parent && parent.isBaseModel ? parent.getItem() : null;
	},
	
	getParentItemName: function() {
		var parent = this.getParent(); 
		return parent && parent.isBaseModel ? parent.getItemName() : null;
	},

	privates				: {
		
		parseDescriptor: function(descriptor) {
			var me = this, parent = me.getParent();
			if (parent && parent.isBaseModel) descriptor = descriptor.replace('_parentItemName_', parent.getItemName());
			return me.callParent([descriptor]);
		}
	
	}
});