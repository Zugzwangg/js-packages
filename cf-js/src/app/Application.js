Ext.define('CFJS.app.Application', {
	extend				: 'Ext.app.Application',
	alternateClassName	: 'CFJS.Application',
	name				: 'CFJS',
	requires			: [ 'CFJS.api.*' ],
	config				: {
		api					: CFJS.Api,
		loginView			: undefined,
		manifest			: 'resources/app.json',
		// 1 minute
		checkSessionInterval: 60000,
		saveSession			: true,
		version				: undefined,
		viewport			: undefined
	},

	afterUpdateSession: function(api, session) {
		if (!api.isSessionExpired()) {
			var me = this, viewport = me.getViewport();
			if (!viewport.isComponent) viewport = Ext.create(viewport);
			me.setViewport(viewport);
			Ext.app.route.Router.onStateChange(Ext.util.History.getToken());
			me.refreshSession(me, api, viewport.getViewModel());
		}
    },
    
    destroy: function() {
    	var me = this, timer = me.sessionTimer;
		if (timer) {
			window.clearTimeout(timer);
			time = me.sessionTimer = null;
		}
    	me.callParent();
    },
    
	init: function(application) {
		var me = CFJS.$application = application, api = me.getApi();
		application.setVersion(new Ext.Version(Ext.manifest.version));
		api.setAppName(me.getName());
		api.loadManifest({
			url: me.getManifest(),
			animateTarget: me,
			extractApi: function(config) { return me.onExtractApi(config, me); },
			success: function(response) { me.onLoadManifest(response, me); },
			failure: function(responseData) { me.onErrorRequest(responseData, me); }
		});
		Ext.ariaWarn = Ext.emptyFn;
		if (Ext.platformTags.classic) {
			Ext.setGlyphFontFamily('Pictos');
	        Ext.tip.QuickTipManager.init();
		    Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));
		}
	},
	
	getLoginView: function() {
		var me = this, currentProfile = me.getCurrentProfile();
		return (currentProfile ? currentProfile.getLoginView() : null) || me._loginView;
	},
	
	getViewport: function() {
		var me = this, currentProfile = me.getCurrentProfile();
		return (currentProfile ? currentProfile.getViewport() : null) || me._viewport;
	},
	
	getViewportController: function() {
		return this.viewport && this.viewport.lookupController();
	},
	
	onAppUpdate: function () {
		window.location.reload(true);
	},
	
	onErrorRequest: function(responseData, application) {
		this.removeLoader();
		CFJS.errorAlert('Application error', responseData, application);
	},
	
	onExtractApi: function(config, application) {
		return (config||{}).api;
	},
	
	onLoadManifest: function(response, application) {
		var me = application || this.getApplication(), api = me.getApi(),
			session = api.getSessionFromCookies(),
			pureUrl = location.protocol + "//" + location.host + location.pathname + location.hash;
		api.updateSessionData({
			session		: api.isSessionExpired(session) ? api.getSessionFromUrl() : session,
			saveCookie	: me.getSaveSession(),
			callback	: {
				success: function(session) {
					me.removeLoader();
					window.history.replaceState({}, document.title, pureUrl);
					if (api.isSessionExpired()) me.showLogin(api);
					else me.afterUpdateSession(api, session);
				},
				failure: function(responseData) {
					me.onErrorRequest(responseData, me);
				}
			}
		});
	},

	platformTag: function() {
		return this.getCurrentProfile().platformTag();
	},

	removeLoader: function(selector) {
		var loader = Ext.fly(selector || 'load-screen');
		if (loader) {
			loader.addCls('loaded');
			Ext.defer(function() { Ext.getBody().removeChild(loader); }, 1000);
		}
	},
	
	showInternalLogin: function(api) {
		var me = this, loginView = Ext.create(me.getLoginView(), {
			listeners: {
				destroy: function(loginForm) {
					me.afterUpdateSession(api, api.findSession());
				}
			}
		});
		if (loginView) {
			if (Ext.platformTags.phone) Ext.Viewport.add(loginView);
			else if (!loginView.autoShow) loginView.show();
		}
	},

	showLogin: function(api) {
		if (api) {
			var controller = api.controller('login');
			if (!controller) this.showInternalLogin(api);
			else api.authorize(api.getService());
		} else location.reload();
	},

	privates: {
		
	    refreshSession: function(me, api, vm) {
	    	if (api.needSessionRefresh()) {
	    		var session = api.getSession(), service = api.getService(),
	    			locked = vm && vm.get('locked');
	    		if ((new Date().getTime() - (api.latestTime||0)) > session.idleTime) {
	    			if (!locked) {
			    		api.saveSessionToCookies({});
			    		me.redirectTo('lock');
	    			}
	    		} else api.refreshSession(service, true, null, function() { 
					api.authorize(service);
				});
			}
			me.sessionTimer = Ext.defer(me.refreshSession, me.getCheckSessionInterval(), me, [me, api, vm]);
	    }
		
	}
	
});
