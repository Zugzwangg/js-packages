Ext.define('CFJS.app.profile.Profile', {
	extend: 'Ext.app.Profile',
	config: {
		loginView	: undefined,
		viewport	: undefined
	},
	
	platformTag: function() {
		return this.getName().toLowerCase();
	},
	
	isActive: function() {
		return Ext.platformTags[this.platformTag()];
	},

    launch: function() {
    	console.info('profile: ', this.platformTag());
    }

});