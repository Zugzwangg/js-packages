Ext.define('CFJS.app.EditorViewModel', {
	extend			: 'CFJS.app.InlineEditorViewModel',
	alias			: 'viewmodel.editor',
	isEditorModel	: true,
	
	applyItemModel: function(itemModel) {
		var parent = this.getParent();
		if (Ext.isEmpty(itemModel) && parent && parent.isBaseModel) itemModel = parent.getItemModel();
		return itemModel || 'Ext.data.Model';
	},
	
	applyItemName: function(itemName) {
		var parent = this.getParent();
		if (Ext.isEmpty(itemName) && parent && parent.isBaseModel) itemName = parent.getItemName();
		return itemName || 'selected';
	},

	updateItemModel: function(itemModel) {
		var parent = this.getParent();
		if (!this.isDetail && parent && parent.isBaseModel) parent.setItemModel(itemModel);
	},
	
	updateItemName: function(itemName) {
		var parent = this.getParent();
		if (!this.isDetail && parent && parent.isBaseModel) parent.setItemName(itemName);
	},
	
	statics: {
		
		buildAddressFormula: function(descriptor, index) {
			return {
				bind: '{' + descriptor + '}',
				get: function(address) { return address[index] || ''; },
				set: function(value) {
					var me = this, item = me.getItem(), address = me.get(descriptor);
					address[index] = value || '';
					item.set(descriptor, address.join(','));
				}
			} 
		}

	},
	
	privates: {

	    parseAddress: function(address) {
			address = (address || '').split(',', 7);
			while (address.length < 7) address.push('');
			return address;
		}
		
	}

});