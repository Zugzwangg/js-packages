Ext.define('CFJS.app.BaseEditorModel', {
	extend		: 'Ext.app.ViewModel',
	alias		: 'viewmodel.baseeditor',
	isBaseModel	: true,
	isMaster	: true,
	config		: {
		itemName		: 'selected',
		itemModel		: 'Ext.data.Model',
		workingPeriod	: new Date()
	},
	functions	: {
		locale: CFJS.getLocale()
	},
	
	applyItemModel: function(itemModel) {
		return itemModel || 'Ext.data.Model';
	},
	
	applyItemName: function(itemName) {
		return itemName || 'selected';
	},
	
	applyWorkingPeriod: function(period) {
		period = period || [];
		return Ext.isArray(period) ? period : [period];
	},

	callEach : function(fnName, args){
        Ext.suspendLayouts();
        this.each(function(key, child, children) {
        	if (Ext.isFunction(child[fnName])) child[fnName].apply(child, args);
        });
        Ext.resumeLayouts(true);
    },
    
	createRecord: function(type, data) {
		var me = this, session = me.getSession(),
			Model = type.$isClass ? type : (session || me).getSchema().getEntity(type);
		if (!Model) Ext.raise('Invalid model name: ' + type);
		return session ? session.createRecord(Model, data) : new Model(data);
	},
	
	defaultItemConfig: Ext.emptyFn,

	each : function(fn, scope){
		Ext.Object.each(this.children, fn, scope);
	},

	getItem: function() {
		return this.get(this.getItemName());
	},

	getPermissions: function(key) {
		var permissions = this.get('permissions');
		return (Ext.isString(key) && key.length > 0 ? CFJS.nestedData(key, permissions) : permissions) || {};
	},

	setItem: function(item) {
		var me = this;
		me.linkTo(me.getItemName(), me.configItem.apply(me, arguments));
		return me.get(me.getItemName());
	},
	
	setStoreConfig: function(store, cfg) {
		if (Ext.isString(store)) store = this.getStore(store);
		if (store && store.isStore) {
			var proxy = cfg.proxy;
			delete cfg.type;
			delete cfg.model;
			delete cfg.fields;
			delete cfg.proxy;
			delete cfg.listeners;
			if (proxy) {
				delete proxy.reader;
				delete proxy.writer;
				store.getProxy().setConfig(proxy);
			}
			store.blockLoad();
			store.setConfig(cfg);
			store.unblockLoad(true);
		}
	},
	
	setStoreFilter: function(store, filter, suppressEvent) {
		if (Ext.isString(store)) store = this.getStore(store);
		if (store && store.isStore && filter) {
			if (!Ext.isObject(filter)) filter = { id: filter };
			if (!filter.id) filter.id = filter.property;
			if (filter.filterFn || (filter.property && filter.value != undefined)) store.addFilter(filter, suppressEvent);
			else store.removeFilter(filter.id, suppressEvent);
		}
	},

	setStoreFilters: function(store, filters) {
		if (Ext.isString(store)) store = this.getStore(store);
		if (store && store.isStore) {
			store.getFilters().beginUpdate();
			store.clearFilter(true);
			if (filters = filters || store.getInitialConfig('filters')) store.addFilter(filters);
			store.getFilters().endUpdate();
		}
	},
	
	updateWorkingPeriod: function(period) {
		var workingPeriod = {}, dt = Ext.Date;
		if (period.length > 0 && period[0]) workingPeriod.startDate = new Date(period[0]);
		if (period.length > 1 && period[1]) workingPeriod.endDate = new Date(period[1]);
		if (Ext.isDate(workingPeriod.startDate)) {
			workingPeriod.startDate = dt.clearTime(workingPeriod.startDate);
			if (!Ext.isDate(workingPeriod.endDate)) {
				workingPeriod.endDate = dt.addComplex(workingPeriod.startDate, {days: 1, seconds: -1});
			}
		}
		if (Ext.isDate(workingPeriod.endDate)) {
			workingPeriod.endDate = dt.setEndOfDate(workingPeriod.endDate);
		}
		this.set('workingPeriod', workingPeriod);
	},
	
	privates: {

		asCurrency: function(record) {
			return record && record.isModel ? record : Ext.apply(new CFJS.model.dictionary.Currency(record), { session: this.getSession() });
		},

		asNamedModel: function(record) {
			return record && record.isModel ? record : Ext.apply(new CFJS.model.NamedModel(record), { session: this.getSession() });
		},

		bindExpression: function (descriptor, callback, scope, options) {
			return this.callParent([this.parseDescriptor(descriptor), callback, scope, options]);
	    },
	    
		configItem: function(item) {
			return item || { type: this.getItemModel(), create: this.defaultItemConfig.apply(this, arguments) || true };
		},
		
		configItemRecord: function(item) {
			var me = this, session = me.getSession(), id;
			if ((item && item.$className === me.getItemModel()) || item === false) return item;
			item = item || me.defaultItemConfig.apply(me, arguments);
			if (session) {
				if ((id = item ? item.id : undefined) !== undefined) item = session.getRecord(me.getItemModel(), id, me.itemLoadConfig(item));
				else {
					item = session.createRecord(me.getItemModel(), item);
					item.commit();
					item.phantom = true;
				}
			} else {
				item = Ext.create(me.getItemModel(), item);
				item.phantom = true;
			}
			return item;
		},
		
		itemLoadConfig: Ext.privateFn,
		
		linkItemRecord: function(item) {
			var me = this, stub = me.getStub(me.getItemName());
			stub.set(item = me.configItemRecord.apply(me, arguments));
			return item;
		},
		
		parseDescriptor: function(descriptor) {
			return descriptor.replace('_itemName_', this.getItemName());
		},
		
		updateArrayField: function(obj, field, store, transform) {
			if (obj && field) {
				var data = [];
				if (Ext.isFunction(store)) {
					transform = store; 
					store = field;
				}
				transform = transform || Ext.identityFn;
				if (Ext.isString(store = store || field)) store = this.getStore(store);
				if (store && store.isStore) {
					store.commitChanges();
					store.each(function(record) {
						data.push(transform(record.getData()));
					});
					if (obj.isModel) obj.set(field, data, { silent: true });
					else obj[field] = data;
				}
			}
			return obj;
		},
		
		updateModelField: function(obj, field) {
			if (obj && field) {
				var data = obj.isModel ? obj.get(field) : obj[field];
				if (data && data.isModel) data = data.getData();
				if (obj.isModel) obj.set(field, data, { silent: true });
				else obj[field] = data;
			}
			return obj;
		}

	}

});
