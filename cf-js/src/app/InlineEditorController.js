Ext.define('CFJS.app.InlineEditorController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.inlineeditor',
	config	: { editorPlugin: 'rowediting' },

	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},
	
	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.editRecord, !selection || selection.length !== 1);
		selectable.disableComponent(actions.removeRecord, !selection || selection.length < 1);
	},
	
	findEditor: function() {
		var me = this, grid = me.lookupGrid(), editorPlugin = me.getEditorPlugin();
		return editorPlugin && grid ? grid.findPlugin(editorPlugin) : null;
	},
	
	lookupGrid: function() {
		var view = this.getView();
		return view ? view.lookupGrid() : null;
	},
	
	lookupGridStore: function(name) {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	},
	
	onAddRecord: function() {
		console.log(this.getViewModel(), this.lookupGrid());
		var me = this, grid = me.lookupGrid(),
			store = grid && !grid.readOnly ? grid.getStore() : null,
			record = me.getViewModel().setItem(record);
		if (store && store.isStore && record) me.startEdit(store.add(record)[0]);
	},

	onClearFilters: function(component) {
		var grid = this.lookupGrid();
		if (grid && Ext.isFunction(grid.onClearFilters)) grid.onClearFilters();
	},

	onEditRecord: function(component) {
		var me = this, grid = me.lookupGrid(), editorPlugin = me.getEditorPlugin(),
			editor = editorPlugin && grid ? grid.findPlugin(editorPlugin) : null,
			selection = grid && grid.getSelection();
		if (editor && !grid.readOnly && !editor.editing && selection && selection.length > 0) editor.startEdit(selection[0]);
	},
	
	onReloadStore: function() {
		var store = this.lookupGridStore();
		if (store && store.isStore) {
			if (Ext.isFunction(store.rejectChanges)) store.rejectChanges();
			store.reload();
		}
	},

	onRemoveRecord: function(component) {
		var me = this, grid = me.lookupGrid(), i = 0, selection;
		if (grid && !grid.readOnly 
				&& me.fireEvent('beforeremoverecord', selection = grid.getSelection()) !== false
				&& selection.length > 0) {
			me.disableComponent(component, true);
			for (; i < selection.length; i++) {
				selection[i].erase(i < selection.length - 1 || {
					callback: function(record, operation, success) {
						me.disableComponent(component);
						me.doSelectionChange(grid, [], grid.actions);
					}
				});
			}
		}
	},
	
	onSaveRecords: function(component) {
		var me = this, grid = me.lookupGrid(), records, i = 0,
			record, options;
		if (grid && !grid.readOnly
				&& me.fireEvent('beforesaverecord', records = grid.store.getModifiedRecords()) !== false
				&& records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].save(i < records.length - 1 || {
					callback: function(record, operation, success) {
						me.disableComponent(component);
					}
				});
			}
		}	
	},
	
	onSelectionChange: function(selectable, selection) {
		var me = this, actions;
		if (selectable && selectable.isSelectionModel) selectable = selectable.view.grid;
		if (!selectable || !selectable.isPanel) selectable = me.lookupGrid();
		if (actions = selectable && selectable.hasActions && selectable.actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.doSelectionChange(selectable, selection, actions);
		}
	},

	privates: {

		startEdit: function(record) {
			var editor = this.findEditor();
			if (record.isModel && editor) editor.startEdit(record);
		}

	}
	
});
