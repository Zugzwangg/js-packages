Ext.define('CFJS.app.HistoryController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.history',
	config	: {
		blankPage	: 'CFJS.BlankPage',
		page404		: 'CFJS.Page404',
		routeId		: 'routeId',
		hiddenMenu	: undefined
	},

	applyHiddenMenu: function(hiddenMenu) {
		if (Ext.isObject(hiddenMenu)) {
			for (key in hiddenMenu) {
				this.addHiddenMenuItem(hiddenMenu, key);
			}
		}
		return hiddenMenu || {};
	},
	
	addHiddenMenuItem: function(hiddenMenu, key, item) {
		item = item || hiddenMenu[key] || {};
		if (!item.isModel) item = Ext.create('Ext.data.Model', Ext.clone(item));
		item.set('routeId', key);
		hiddenMenu[key] = item;
	},
	
	beforeChangeView: function(store, currentView, hashTag) {
		return store;
	},
	
	findRecord: function(store, hashTag) {
		return this.getHiddenMenu()[hashTag] || (store ? store.findRecord(this.getRouteId(), hashTag) : null);
	},
	
	getApplication: function() {
		return CFJS.$application;
	},

	getContentPanel: function() {
		return this.getView().getContentPanel();
	},
	
	getDefaultToken: function() {
		return this.getApplication().getDefaultToken();
	},
	
	getDesktopWrap: function() {
		return this.getView().getDesktopWrap();
	},

	getNavigator: function() {
		return this.getView().getNavigator();
	},
	
	onRouteChange: function(node) {
		this.setCurrentView(node);
    },

    onNavigationSelectionChange: function (navigation, selected) {
    	var token = Ext.util.History.getToken() || '',
			hashTag = selected ? selected.get(this.getRouteId()) : null,
			isCurrent = hashTag && token.indexOf(hashTag) === 0;
		if (hashTag && !isCurrent) this.redirectTo(hashTag);
    },

	setCurrentView: function(hashTag) {
		var me = this, token = me.getDefaultToken();
		hashTag = (hashTag || token).toLowerCase();
		if (hashTag === 'undefined') hashTag = token;
		var contentPanel = me.getContentPanel(),
			contentLayout = contentPanel.getLayout(),
			navigator = me.getNavigator(),
			store = navigator ? navigator.getStore() : null,
			viewModel = me.getViewModel(),
			vmData = viewModel.getData(),
			lastView = vmData.currentView,
			routeId = hashTag.split(/[\?/]/, 1)[0],
			existingItem = contentPanel.child('component[routeId=' + routeId + ']'),
			secure = false, view, newView;
		// check menu store
		if (!me.beforeChangeView(store, lastView, hashTag)) return;
		// gets data from node
		var selected = me.findRecord(store, routeId);
		if (selected) {
			view = selected.get('view');
			secure = selected.get('secure');
		} else view = me.getPage404();
		// if view is locked and next not secure view
		if (vmData.locked && !secure) return;
		// save last non secure hash
		if (!secure) vmData.lastRoute = hashTag;
		// Kill any previously routed window
		if (lastView && lastView.isWindow) {
			lastView.destroy();
		}
		lastView = contentLayout.getActiveItem();
		if (!existingItem) {
			if (Ext.isString(view = view || me.getBlankPage())) view = { xclass: view };
			newView = Ext.create(Ext.apply(view, { hideMode: 'offsets', routeId: routeId, routeModel: selected }));
		}
		if (!newView || !newView.isWindow) {
			if (existingItem) {
				if (existingItem !== lastView) contentPanel.setActiveItem(existingItem);
				newView = existingItem;
			} else {
				Ext.suspendLayouts();
				contentPanel.setActiveItem(contentPanel.add(newView));
				Ext.resumeLayouts(true);
			}
		}
		if (navigator) navigator.setSelection(selected);
		if (newView.isFocusable(true)) newView.focus();
		vmData.locked = secure;
		vmData.currentView = newView;
	}
    
});
