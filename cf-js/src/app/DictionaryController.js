Ext.define('CFJS.app.DictionaryController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.dictionary',
	config	: {
		routeId	: '',
		editor	: undefined
	},
	
	applyEditor: function(editor) {
		return editor || 'container';
	},
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},

	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.editRecord, !selection || selection.length !== 1);
		selectable.disableComponent(actions.removeRecord, !selection || selection.length < 1);
	},

	editorConfig: Ext.identityFn,
	
	getEditorComponent: function() {
		var me = this, view = me.getView();
		return view ? view.down(me.getEditor()) : null;
	},

	hideComponent: function(component, hidden) {
		if (component && (component.isComponent || component.isAction)) component[hidden ? 'hide': 'show']();
	},
	
	lookupCurrentItem: function() {
		var me = this, record = me.lookupRecord(), grid;
		if (!record && (grid = me.lookupGrid())) {
			record = grid.getSelection();
			record = record && record.length > 0 ? record[0] : null;
		}
		return record;
	},
	
	lookupGrid: function() {
		var view = this.getView();
		return view ? view.lookupGrid() : null;
	},
	
	lookupGridStore: function(name) {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	},
	
	lookupRecord: function() {
		var editor = this.getEditorComponent();
		return editor ? editor.lookupViewModel().getItem() : null;
	},
	
	maybeEditRecord: function(record, add) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			editor = me.getEditor(), activeItem;
		if (grid && editor) {
			if (view.rendered) Ext.suspendLayouts();
			if (record !== false && !grid.readOnly) {
				var selection = grid.getSelection();
				if (!record && !add && selection && selection.length > 0) record = selection[0];
				if (activeItem = view.down(editor)) view.remove(activeItem, true);
				activeItem = view.add(me.editorConfig({ xtype: editor }));
				me.startEditRecord(record, activeItem.lookupViewModel());
			} else activeItem = grid;
			view.getLayout().setActiveItem(activeItem);
			if (view.rendered) Ext.resumeLayouts(true);
		}
	},
	
	onAddRecord: function(component) {
		this.maybeEditRecord(null, true);
	},

	onBackClick: function(component) {
		var me = this, record = me.lookupRecord();
		if (record && (record.phantom || record.dirty)) {
			Ext.fireEvent('confirmlostdataandgo', function(btn) {
				if (btn === 'yes') {
					record.reject();
					me.maybeEditRecord(false);
				}
			}, me);
		} else me.maybeEditRecord(false);
	},
	
	onClearFilters: function(component) {
		var grid = this.lookupGrid();
		if (grid && Ext.isFunction(grid.onClearFilters)) grid.onClearFilters();
	},
	
	onEditRecord: function(component) {
		this.maybeEditRecord();
	},
	
	onGridDblClick: function(view, record, item, index, e, eOpts) {
		this.maybeEditRecord(record);
	},
	
	onPeriodPicker: function(component) {
		var grid = this.lookupGrid();
		if (grid && grid.hasPeriodPicker) grid.showPeriodPicker(component);
	},
	
	onRefreshRecord: function(component) {
		var record = this.lookupRecord();
		if (record) {
			record.reject();
			if (!record.phantom) record.load();
		}
	},
	
	onReloadStore: function(component) {
		var store = this.lookupGridStore();
		if (store && store.isStore) {
			if (Ext.isFunction(store.rejectChanges)) store.rejectChanges();
			store.reload();
		}
	},

	onRemoveRecord: function(component) {
		var me = this, grid = me.lookupGrid(), i = 0, selection;
		if (grid && !grid.readOnly 
				&& me.fireEvent('beforeremoverecord', selection = grid.getSelection()) !== false
				&& selection.length > 0) {
			me.disableComponent(component, true);
			for (; i < selection.length; i++) {
				selection[i].erase(i < selection.length - 1 || {
					callback: function(record, operation, success) {
						grid.getStore().reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},
	
	onSaveRecord: function(component, force) {
		var me = this, record = me.lookupRecord();
		if (record && me.fireEvent('beforesaverecord', record) !== false && (record.dirty || record.phantom || force)) {
			me.disableComponent(component, true);
			record.save({
				callback: function(record, operation, success) {
					me.onReloadStore();
					me.disableComponent(component);
				}
			});
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		var me = this, actions;
		if (selectable && selectable.isSelectionModel) selectable = selectable.view.grid;
		if (!selectable || !selectable.isPanel) selectable = me.lookupGrid();
		if (actions = selectable && selectable.hasActions && selectable.actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.doSelectionChange(selectable, selection, actions);
		}
	},
	
	privates: {
		
		startEditRecord: function(record, vm) {
			if (!vm) {
				var me = this, view = me.getView(), editor = me.getEditor(),
					activeItem = view && editor && view.down(editor);
				if (activeItem) vm = activeItem.lookupViewModel();
			}
			if (vm && vm.isBaseModel) return vm.setItem(record);
		}

	}

});
