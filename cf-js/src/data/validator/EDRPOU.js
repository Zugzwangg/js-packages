Ext.define('CFJS.data.validator.EDRPOU', {
	extend	: 'Ext.data.validator.Validator',
	alias	: 'data.validator.edrpou',
	type	: 'edrpou',
	config	: {

		allowEmpty: false,

		/**
         * @cfg {String} checkSumMessage 
         * The error message to return when the check sum is wrong.
         */
		checkSumMessage: 'Wrong check sum',
		
		/**
         * @cfg {String} emptyMessage 
         * The error message to return when the value is empty.
         */
		emptyMessage: 'Must be present',
		
		/**
         * @cfg {String} lengthMeassge 
         * The error message to return when the length of value does not match.
         */
		lengthMessage: 'The minimum length for this field is',
		
		/**
		 * @cfg {String} message 
		 * The error message to return when the value does not match the format.
		 */
		message: 'Is in the wrong format'
			
	},

	checkSum: function(value, rates) {
		for (var i = 0, cs = 0; i < rates.length; i++) {
			cs += rates[i] * value[i];
		}
		return cs % 11;
	},
	
	validateCheckSum: function(value, rates1, rates2) {
		var me = this, cs = me.checkSum(value, rates1);
		if (rates2 && cs >= 10) cs = me.checkSum(value, rates2);
		if (cs === 10) cs /= 10;
		return CFJS.parseInt(value.substr(-1)) === cs;
	},

	validate: function(value, record) {
		var me = this, checkSum, rates1, rates2;
		// check for an empty
        if (Ext.isEmpty(value)) return this.getAllowEmpty() || me.getEmptyMessage();
        value = String(value);
		if (value.length !== 8) return me.getLengthMeassge() + ' 8';
		if (checkSum < 30000000 || checkSum > 60000000) {
			rates1 = [1, 2, 3, 4, 5, 6, 7];
			rates2 = [3, 4, 5, 6, 7, 8, 9];
		} else {
			rates1 = [7, 1, 2, 3, 4, 5, 6];
			rates2 = [9, 3, 4, 5, 6, 7, 8];
		}
		if (!me.validateCheckSum(value, rates1, rates2)) return me.getCheckSumMessage();
		return true;
	}
	
});