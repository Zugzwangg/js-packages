Ext.define('CFJS.data.validator.TaxUA', {
	extend	: 'CFJS.data.validator.EDRPOU',
	alias	: 'data.validator.taxUA',
	type	: 'tax',
	config	: {
		
		birthdayField: undefined,

		/**
         * @cfg {String} birthdayMessage 
         * The error message to return when the birthday check sum is wrong.
         */
		birthdayMessage: 'Wrong birthday check sum',
		
		/**
         * @cfg {String} emptyBirthdayMessage 
         * The error message to return when the birthday is empty.
         */
		emptyBirthdayMessage: 'The birthday must be present'

	},
	
//	validateCheckSum: function(value, rates) {
//		var cs = this.checkSum(value, rates);
//		if (cs === 10) cs /= 10;
//		return CFJS.parseInt(value[rates.length]) === cs;
//	},
	
	validate: function(value, record) {
		var me = this,
			message = me.getMessage(),
			birthdayField = me.getBirthdayField(),
			date = new Date(1899,11,31),
			birthday, len, checkSum, result;
		// check for an empty
        if (Ext.isEmpty(value)) return this.getAllowEmpty() || me.getEmptyMessage();
        value = String(value);
        len = value.length;
		// for human
		if (!Ext.isEmpty(birthdayField)) {
			// check length
			if (len !== 10) return me.getLengthMessage() + ' 10';
			if (!Ext.isDate(birthday = record.get(birthdayField))) return me.getEmptyBirthdayMessage();
			if (!me.validateCheckSum(value, [ -1, 5, 7, 9, 4, 6, 10, 5, 7])) return me.getCheckSumMessage();
			date.setDate(date.getDate() + CFJS.safeParseInt(value.substring(0, 5)));
			if (Ext.Date.clearTime(birthday).getTime() !== Ext.Date.clearTime(date).getTime()) return me.getBirthdayMessage();
			result = true;
		// for PE
		} else if (len === 10) {
			if (!me.validateCheckSum(value, [ -1, 5, 7, 9, 4, 6, 10, 5, 7])) return me.getCheckSumMessage();
			result = true;
		// for company
		} else {
			// check length
			if (len < 11 || len > 12) return me.getLengthMessage() + ' 11 or 12';
			if (!me.validateCheckSum(value, [11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47], [17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59])) return me.getCheckSumMessage();
			result = true;
		}
		return result || message;
	}
	
});