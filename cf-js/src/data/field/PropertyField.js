Ext.define('CFJS.data.field.PropertyField', {
	extend		: 'Ext.data.field.Field',
	alias		: 'data.field.property',
	dataType	: 'string',
	isProperty	: true,
	sortType	: 'asUCString',
	type		: 'property',

	convert		: function (value, record) {
		return Ext.isObject(value) ? value.$ : value;
	},
	
	serialize: function (value) {
		return (value && value !== '') || value === false  ? { '$': value, '@type' : this.dataType } : undefined;
	},
	
	getDefaultValue: function() {
        return this.serialize(this.defaultValue);
    },
    
	getType: function() {
        return 'property';
    }
	
});