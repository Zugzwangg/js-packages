Ext.define('CFJS.data.field.Model', {
	extend		: 'Ext.data.field.Field',
	alias		: 'data.field.model',
	isModel		: true,
	entityName	: null,
	
	convert: function(value, record) {
		var me = this, entityName = me.entityName;
		if (!Ext.isEmpty(entityName)) {
			if (!Ext.isEmpty(value) || me.allowNull !== true) value = Ext.create(entityName, value).getData({ associated: true, critical: true });
		} else if (me.critical) value = value || {};
		return value;
	},
	
	serialize: function(value, record) {
		var me = this, entityName = me.entityName;
		if (!Ext.isEmpty(entityName)) {
			if (!Ext.isEmpty(value) || me.allowNull !== true) value = Ext.create(entityName, value).getData({ associated: true, persist: true, serialize: true });
		} else if (me.critical) value = value || {};
		return value;
	}
	
});