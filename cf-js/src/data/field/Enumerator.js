Ext.define('CFJS.data.field.Enumerator', {
	extend		: 'Ext.data.field.String',
	alias		: 'data.field.enum',
	isEnumerator: true,
	defaultValue: '...',

	serialize: function(value, record) {
		return value === this.defaultValue ? null : value;
	},
	
	getType: function() {
        return 'enum';
    }

});