Ext.define('CFJS.data.field.Consumer', {
	extend		: 'Ext.data.field.Field',
	alias		: 'data.field.consumer',
	isConsumer	: true,
	
	convert: function(value, record) {
		return this.transformData(value, { associated: true, critical: true });
	},
	
	serialize: function(value, record) {
		return this.transformData(value, { associated: true, persist: true, serialize: true });
	},
	
	privates: {
		
		transformData: function(data, options) {
			if (data) {
				if (data.isCustomer) {
					data = data.getData();
					data = Ext.applyIf({
						customer: { id: data.id, name: data.fullName }
					}, CFJS.model.dictionary.Consumer.transform(data));
				}
				if (!data.isModel) data = new CFJS.model.dictionary.Consumer(data);
				return data.getData(options);
			}
			if (this.critical) data = data || {};
			return data;
		}
	
	}
});