Ext.define('CFJS.data.field.Array', {
	extend			: 'Ext.data.field.Field',
	alias			: 'data.field.array',
	isArray			: true,
	item			: undefined,
	convertItem		: Ext.identityFn,
	serializeItem	: Ext.identityFn,
	
	constructor: function (config) {
		var me = this;
		if (config.item) {
			var item = config.item, 
				entityName, mapping, convert, serialize;
			if (Ext.isString(item)) entityName = item;
			else {
				entityName = item.entityName;
				mapping = item.mapping;
				convert = item.convert;
				serialize = item.serialize;
			}
			if (!Ext.isEmpty(entityName)) {
				me.itemField = CFJS.data.field.Model.create({ entityName: entityName });
				convert = convert || function(value, record) { return this.itemField.convert(value, record); };
				serialize = serialize || function(value, record) { return this.itemField.serialize(value, record); };
			}
			config.convertItem = convert || config.convertItem || Ext.identityFn;
			config.serializeItem = serialize || config.serializeItem || Ext.identityFn;
			mapping = mapping || config.mapping || config.name;
			if (!config.convert) {
				delete config.mapping;
				me.convert = function(value, record) {
					var item, i;
					if (!Ext.isArray(value)) {
						if (value = CFJS.nestedData(mapping, value)) {
							if (!Ext.isArray(value)) value = [value];
						} else if (me.critical) value = [];
					}
					if (value && value.length > 0) {
						for (i = 0; i < value.length; i++) {
							if (!(item = value[i])) { value.splice(i, 1); i--;}
							else value[i] = me.convertItem(item, record);
						}
					}  
					return value;
				}
			}
			if (!config.serialize) {
				me.serialize = function(value, record) {
					if (Ext.isArray(value)) {
						for (key in value) {
							value[key] = me.serializeItem(value[key], record);
						}
					}
					return CFJS.asObject(mapping, value);
				}
			}
		}
		me.callParent([config]);
	},

	compare: function (value1, value2) {
		var str1 = JSON.stringify(value1 || ''), str2 = JSON.stringify(value2 || '');
		return str1.localeCompare(str2);
	},

	isEqual: function (value1, value2) {
		return this.compare(value1, value2) === 0;
	}
});