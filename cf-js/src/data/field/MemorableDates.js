Ext.define('CFJS.data.field.MemorableDates', {
	extend		: 'CFJS.data.field.Array',
	alias		: 'data.field.memorabledates',
	critical	: true,
	convertItem	: function(item) {
		if (item && Ext.isDate(item.date)) item.date = Ext.Date.format(item.date, CFJS.Api.dateTimeFormat);
		return item;
	}

});