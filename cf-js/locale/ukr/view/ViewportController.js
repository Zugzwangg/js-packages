Ext.define('CFJS.locale.ukr.view.ViewportController', {
    override	: 'CFJS.view.ViewportController',
	infoText	: 'Інформація',
	goText		: 'перейти',
	loadingText	: 'Завантаження...',
	lostDataText: 'Всі зміни будуть втрачені. Ви впевнені, що ви хочете {0}?',
	warningText	: 'Увага'
});
