Ext.onReady(function() {
	CFJS.overrideStoreData('genderTypes', [
		{ id: 'MALE',	name: 'Чоловік' }, 
		{ id: 'FEMALE',	name: 'Жінка' } 
	]);
	CFJS.overrideStoreData('messagePriorities', [
		{ id: 'VERY_HIGH',	name: 'Дуже високий' },
		{ id: 'HIGH',		name: 'Високий' },
		{ id: 'NORMAL',		name: 'Нормальний' },
		{ id: 'LOW',		name: 'Низький' },
		{ id: 'VERY_LOW',	name: 'Дуже низький' }
	]);
	CFJS.overrideStoreData('payingTypes', [
		{ id: 'BANK', 			name: 'Безготівкові' },
		{ id: 'CASH',			name: 'Готівка' },
		{ id: 'CHEQUE',			name: 'Банківський чек' },
		{ id: 'CARD',			name: 'Банківська картка' },
		{ id: 'WEB_MONEY',		name: 'Інтернет платежі' },
		{ id: 'NATIONAL_CARD',	name: 'Національна карта' }
	]);
	CFJS.overrideStoreData('postTypes', [
		{ id: 'CHIEF_ACCOUNTANT',	name: 'Головний бухгалтер' },
		{ id: 'DIRECTOR',			name: 'Директор' },
		{ id: 'MANAGER',			name: 'Менеджер' },
		{ id: 'OTHER',				name: 'Інше' }
	]);
});
Ext.define('CFJS.locale.ukr.ux.PDFObject', {
    override	: 'CFJS.ux.PDFObject',
    fallbackText: 'Цей браузер не підтримує перегляд PDF-файлів. Будь-ласка, завантажте PDF-файл, щоб переглянути його: '
});