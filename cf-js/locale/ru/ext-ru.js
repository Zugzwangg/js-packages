Ext.Date.patterns = {
	FullDateTime				: 'l, d F Y H:i:s',
	ISO8601Long					: 'd.m.Y H:i:s',
	ISO8601Short				: 'd.m.Y',
	LongDate					: 'l, d F Y',
	LongDateTime				: 'd.m.Y H:i:s',
	LongTime					: 'H:i:s',
	MonthDay					: 'd F',
	NormalDate					: 'd.m.Y',
	ShortDate					: 'd.m.y',
	ShortTime					: 'G:i',
	SortableDateTime			: 'd.m.Y\\TH:i:s',
	UniversalSortableDateTime	: 'd.m.Y H:i:sO',
	YearMonth					: 'F, Y'
};
