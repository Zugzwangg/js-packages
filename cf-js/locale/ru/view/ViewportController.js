Ext.define('CFJS.locale.ru.view.ViewportController', {
    override	: 'CFJS.view.ViewportController',
	infoText	: 'Информация',
	goText		: 'перейти',
	loadingText	: 'Загрузка...',
	lostDataText: 'Все изменения будут потеряны. Вы уверены, что вы хотите {0}?',
	warningText	: 'Внимание'
});