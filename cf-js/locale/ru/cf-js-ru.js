Ext.onReady(function() {
	CFJS.overrideStoreData('genderTypes', [
		{ id: 'MALE',	name: 'Мужчина' }, 
		{ id: 'FEMALE',	name: 'Женщина' } 
	]);
	CFJS.overrideStoreData('messagePriorities', [
		{ id: 'VERY_HIGH',	name: 'Очень высокий' },
		{ id: 'HIGH',		name: 'Высокий' },
		{ id: 'NORMAL',		name: 'Нормальный' },
		{ id: 'LOW',		name: 'Низкий' },
		{ id: 'VERY_LOW',	name: 'Очень низкий' }
	]);
	CFJS.overrideStoreData('payingTypes', [
		{ id: 'BANK', 			name: 'Безналичные' },
		{ id: 'CASH',			name: 'Наличные' },
		{ id: 'CHEQUE',			name: 'Банковский чек' },
		{ id: 'CARD',			name: 'Банковская карта' },
		{ id: 'WEB_MONEY',		name: 'Интернет платежи' },
		{ id: 'NATIONAL_CARD',	name: 'Национальная карта' }
	]);
	CFJS.overrideStoreData('postTypes', [
		{ id: 'CHIEF_ACCOUNTANT',	name: 'Главный бухгалтер' },
		{ id: 'DIRECTOR',			name: 'Директор' },
		{ id: 'MANAGER',			name: 'Менеджер' },
		{ id: 'OTHER',				name: 'Другое' }
	]);
});
Ext.define('CFJS.locale.ukr.ux.PDFObject', {
    override	: 'CFJS.ux.PDFObject',
    fallbackText: 'Этот браузер не поддерживает просмотр PDF-файлов. Загрузите PDF-файл, чтобы просмотреть его: '
});