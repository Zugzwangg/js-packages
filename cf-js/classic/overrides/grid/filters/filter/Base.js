Ext.define('CFJS.overrides.grid.filters.filter.Base', {
	override: 'Ext.grid.filters.filter.Base',
	
	getFilterConfig: function(config, key) {
        config.id = this.getBaseIdPrefix();
        config.type = this.dataType || this.type;
        if (!config.property) config.property = this.dataIndex;
        if (!config.root) config.root = this.defaultRoot;
        if (key) config.id += '-' + key;
        return config;
    }
    
});