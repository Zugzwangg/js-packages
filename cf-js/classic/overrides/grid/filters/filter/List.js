Ext.define('CFJS.overrides.grid.filters.filter.List', {
	override	: 'Ext.grid.filters.filter.List',
	nullValue	: null,
	
    initConfig: function(config) {
    	if (config && config.store) config.store = Ext.StoreManager.lookup(config.store);
    	this.callParent([config]);
    },
    
	createMenuItems: function (store) {
		var me = this,
			menu = me.menu,
			len = store.getCount(),
			contains = Ext.Array.contains,
			listeners, itemDefaults, record, gid, idValue, idField, labelValue, labelField, i, item, processed;
		console.log(store, len);
		if (len && menu) {
			listeners = {
				checkchange: me.onCheckChange,
				scope: me
			};
			itemDefaults = me.getItemDefaults();
			menu.suspendLayouts();
			menu.removeAll(true);
			gid = me.single ? Ext.id() : null;
			idField = me.idField;
			labelField = me.labelField;
			processed = [];
			for (i = 0; i < len; i++) {
				record = store.getAt(i);
				idValue = record.get(idField);
				if (me.single && idValue === me.nullValue) continue;
				labelValue = record.get(labelField);
				// Only allow unique values.
				if (labelValue == null || contains(processed, idValue)) continue;
				processed.push(labelValue);
				// Note that the menu items will be set checked in filter#activate() if the value of the menu
				// item is in the cfg.value array.
				item = menu.add(Ext.apply({
					text: labelValue,
					group: gid,
					value: idValue,
					listeners: listeners
				}, itemDefaults));
			}
			menu.resumeLayouts(true);
		}
    }
	
});