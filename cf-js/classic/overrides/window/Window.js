Ext.define('CFJS.overrides.window.Window', {
	override		: 'Ext.window.Window',
	restoreToolText	: 'Restore window size',
	minimizeToolText: 'Minimize window size',
	maximizeToolText: 'Maximize window size',
	
	addTool: function(tools) {
		if (!Ext.isArray(tools)) tools = [tools];
		for (var t = 0; t < tools.length; t++) {
			Ext.apply(tools[t], { xtype: 'tool', tooltip: this[tools[t].type + 'ToolText'] });
		}
		this.callParent([tools]);
	},
	
	toggleMaximize: function() {
		var me = this.callParent(arguments),
			tools = me.tools;
		if (tools.maximize) {
			tools.maximize.setTooltip(me[tools.maximize.type + 'ToolText']);
		}
		return me;
	},

});