Ext.define('CFJS.overrides.layout.component.field.FieldContainer', {
	override: 'Ext.layout.component.field.FieldContainer',
	privates: {
		getHeightAdjustment: function() {
			var owner = this.owner, h = this.callParent();
			if (owner.layout.type.endsWith('box')) h -= owner.labelEl.getHeight();
			return h;
		}
	}
});