Ext.define('CFJS.overrides.form.FieldSet', {
	override: 'Ext.form.FieldSet',
	config	: { readOnly: false },

	applyReadOnly: function(readOnly) {
		return readOnly === true;
	},

	getFields: function() {
		return this.motitor.getItems();
	},

	updateReadOnly: function(readOnly, oldReadOnly) {
		var me = this, fields, f = 0;
		if (!me.motitor) me.fieldDefaults = Ext.apply(me.fieldDefaults || {}, { readOnly: readOnly });
		else {
			if (oldReadOnly !== readOnly) {
				fields = me.getFields().items;
				for (; f < fields.length; f++) {
					fields[f].setReadOnly(readOnly);
				}
			}
		}
	}

});