Ext.define('CFJS.overrides.form.field.Base', {
	override: 'Ext.form.field.Base',
	config	: { required: false },
	
	applyRequired: function(required) {
		return this.mixins.labelable.applyRequired.apply(this, arguments);
	},
	
	updateRequired: function(required) {
		this.mixins.labelable.updateRequired.apply(this, arguments);
	}
	
});