Ext.define('CFJS.overrides.form.field.ComboBox', {
	override: 'Ext.form.field.ComboBox',
	isCombo	: true,
	config	: {
		buttonMargin	: 3,
		buttons			: {
			defaults: { ui: 'default-toolbar' }
		},
		queryOperator	: 'like',
		queryParam		: null 
	},

	addLoaderConfig: function(options) {
		var store = this.getStore(), proxy = store && Ext.isFunction(store.getProxy) && store.getProxy();
		if (proxy && proxy.isCFJSProxy) proxy.addLoaderConfig(options);
	},

	afterQuery: function(queryPlan) {
		var me = this;
		me.callParent(arguments);
		me.fireEvent('afterquery', queryPlan);
	},
	
	applyButtons: function(buttons) {
		if (Ext.isObject(buttons)) {
			var me = this, def = buttons.defaults, items = {}, key,
				style = (me.getInherited().rtl ? 'margin-right:' : 'margin-left:') + me.buttonMargin + 'px';
			for (key in buttons) {
				if (key === 'defaults') continue;
				items[key] = Ext.apply({
					xtype	: 'button',
					combo	: me,
					disabled: me.disabled,
					id		: me.id + '-button-' + key,
					ownerCt	: me,
					style	: style,
					tabIndex: me.tabIndex,
					ui		: me.ui
				}, buttons[key], def);
			}
			buttons = items;
		}
		return buttons;
	},
	
	applyTriggers: function(triggers) {
		var me = this, buttons = me.buttons, defaults, key;
		if (Ext.isObject(buttons)) {
			triggers = triggers || {};
			for (key in buttons) {
				triggers[key] = Ext.apply(triggers[key] || {}, { type: 'component', component: buttons[key] });
			}
		}
		return me.callParent([triggers]);
	},
	
	callEachButton : function(fnName, args) {
		Ext.suspendLayouts();
		this.eachButton(function(name, button, buttons) {
			button[fnName].apply(button, args);
		});
		Ext.resumeLayouts(true);
	},

	doDestroy: function() {
        this.buttons = null;
        this.callParent();
    },

    doRemoteQuery: function(queryPlan) {
        var me = this,
        	queryString = queryPlan.query,
        	store = me.getStore(),
        	filter = me.queryFilter,
            loadCallback = function() {
                if (!me.destroyed) {
                    me.afterQuery(queryPlan);
                }
            };

        // expand before loading so LoadMask can position itself correctly
        me.expand();
        me.queryFilter = null;
        // Must set changingFilters flag for this.checkValueOnChange.
        // the suppressEvents flag does not affect the filterchange event
        me.changingFilters = true;
        if (filter) store.removeFilter(filter, true);
        // Querying by a string...
        if (queryString) {
            filter = me.queryFilter = new Ext.util.Filter({
                id		: me.id + '-filter',
                property: me.queryParam || me.displayField,
                operator: me.queryOperator,
                value	: queryString
            });
            store.addFilter(filter, true);
        } else queryPlan.query = '';
        me.changingFilters = false;
        // In queryMode: 'remote', we assume Store filters are added by the developer as remote filters,
        // and these are automatically passed as params with every load call, so we do *not* call clearFilter.
        if (me.pageSize) {
            // if we're paging, we've changed the query so start at page 1.
            me.loadPage(1, {
                rawQuery: queryPlan.rawQuery,
                callback: loadCallback
            });
        } else {
            me.store.load({
                rawQuery: queryPlan.rawQuery,
                callback: loadCallback
            });
        }
	},
	
	eachButton: function(fn, scope) {
		Ext.Object.each(this.buttons, fn, scope || this);
	},
	
	loadPage: function(pageNum, options) {
    	this.store.loadPage(pageNum, options);
    },
    
	onDisable: function() {
		this.callParent();
		this.callEachButton('disable');
    },
 
    onEnable: function() {
    	this.callParent();
		this.callEachButton('enable');
    },
    
    onRender: function() {
    	var me = this, triggers = me.getTriggers() || {},
    		buttons = me.buttons = {}, key;
    	me.callParent(arguments);
    	for (key in triggers) {
    		if ((triggers[key].component||{}).isButton) buttons[key] = triggers[key].component;
    	}
    	if (Ext.Object.isEmpty(buttons)) me.buttons = null;
    	else if (Ext.theme.name === 'Crisp') me.addCls('buttons-delete-focus');
    },
    
    onShow: function() {
        this.callParent();
		this.callEachButton('updateLayout');
    }

});
