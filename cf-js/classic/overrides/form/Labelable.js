Ext.define('CFJS.overrides.form.Labelable', {
	override: 'Ext.form.Labelable',
	statics	: {
		
		REQUIRED: ['<span style="color:red;font-weight:bold" data-qtip="Required for input">*</span>']
	
	},
	required: { $value: false, lazy: true },

	applyRequired: function(required) {
		return !!required;
	},
	
	updateRequired: function(required) {
		var me = this;
		me.allowBlank = !required;
		if (required) {
			me.afterLabelTextTpl = me.afterLabelTextTpl || Ext.form.Labelable.REQUIRED;
			me.labelStyle = me.labelStyle || 'font-weight:bold';
		}
		else if (me.afterLabelTextTpl === Ext.form.Labelable.REQUIRED) delete me.afterLabelTextTpl;
	}

});