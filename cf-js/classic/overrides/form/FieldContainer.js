Ext.define('CFJS.overrides.form.FieldContainer', {
	override: 'Ext.form.FieldContainer',
	config	: { required: false },
	
	applyRequired: function(required) {
		return this.mixins.labelable.applyRequired.apply(this, arguments);
	},
	
	updateRequired: function(required) {
		this.mixins.labelable.updateRequired.apply(this, arguments);
	}
	
});