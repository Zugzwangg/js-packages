Ext.define('CFJS.overrides.Action', {
	override: 'Ext.Action',
	
	isPressed: function() {
		return !!this.initialConfig.pressed;
	},
	
	setPressed(pressed) {
		this.initialConfig.pressed = pressed = !!pressed;
		this.each(function(item) {
			if (item.isButton) item.toggle(pressed, true);
			else item.pressed = pressed;
		});
	}

});
