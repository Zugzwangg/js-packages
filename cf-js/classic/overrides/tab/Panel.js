Ext.define('CFJS.overrides.tab.Panel', {
	override	: 'Ext.tab.Panel',
	isTabPanel	: true,
	
	getActiveChild: function() {
		return this.getActiveTab();
	},
	
	onAdd: function(item, index) {
		var me = this;
		me.callParent(arguments);
		me.onItemTitleChange(item, item.tab.title);
	},
	
	onItemTitleChange: function(item, newTitle) {
		var me = this, iconsOnly = me.tabIconsOnly === true;
		if (iconsOnly) {
			item.tab.setTooltip(item.tab.tooltip || newTitle);
			newTitle = null;
		}
		me.callParent([item, newTitle]);
    }

});