Ext.define('CFJS.overrides.container.Container', {
	override: 'Ext.container.Container',

	disableChild: function(child, disabled) {
		if (Ext.isString(child)) child = this.lookupChild(child);
		if (child && (child.isComponent || child.isAction)) child[disabled ? 'disable': 'enable']();
	},
	
	ensureChild: function(name) {
		var me = this, child = me[name], method = me['create' + name.capitalize(false)];
		if (!child && Ext.isFunction(method)) me[name] = child = method.call(me, me[name + 'Config']);
		return child;
	},

	hideChild: function(child, hidden) {
		if (Ext.isString(child)) child = this.lookupChild(child);
		if (child && (child.isComponent || child.isAction)) child[hidden ? 'hide': 'show']();
	},
	
	initItems: function() {
		var me = this;
		if (me.items && !me.items.isMixedCollection) {
			me.items = CFJS.apply(me.items, me.itemsConfig);
		}
		me.callParent();
	},
	
	lookupChild: function(name) {
		var me = this, child = Ext.isString(name) ? me[name] : name;
		if (child && !child.isComponent) {
			if (child.itemId) child = me.down('[itemId=' + child.itemId + ']');
			else if (child.name) child = me.down('[name=' + child.name + ']');
		} else if (!child && Ext.isString(name)) {
			child = me.down('[itemId=' + name + ']') || me.down('[name=' + name + ']');
		}
		if (Ext.isArray(child) && child.length > 0) child = child[0];
		if (Ext.isString(name)) me[name] = child;
		return child;
	}
	
});