Ext.define('CFJS.overrides.picker.Date', {
	override: 'Ext.picker.Date',
	onRender: function(container, position) {
		var me = this;
		me.callParent(arguments);
		me.mon(me.eventEl, {
            dblclick: {
                fn: function() {
                	me.fireEvent('datedblclick', me, me.getValue());
                },
                delegate: 'div.' + me.baseCls + '-date'
            }
        });
	}
});