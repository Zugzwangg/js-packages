Ext.define('CFJS.overrides.view.MultiSelector', {
	override: 'Ext.view.MultiSelector',
	initComponent: function () {
        var me = this, fieldConfig = !me.columns && me.fieldConfig;
        if (fieldConfig) {
        	me.hideHeaders = !me.fieldTitle;
        	me.columns = [ Ext.apply({ dataIndex: me.fieldName, text: me.fieldTitle,  flex: 1 }, fieldConfig), me.makeRemoveRowColumn() ];
        }
        me.callParent();
	},
	
	privates: {
	
		onShowSearch: function (panel, tool, event) {
			var me = this, search = me.searchPopup, 
				store = me.getStore(), records = store.getRange();
			if (search) {
				if (records.length > 0) search.selectRecords(records);
				else search.lookupReference('searchGrid').getSelectionModel().deselectAll();
			}
			me.callParent(arguments);
		}
	
	}
	
});