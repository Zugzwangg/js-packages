Ext.define('CFJS.locale.ru.panel.FilesEditor', {
	override		: 'CFJS.panel.FilesEditor',
	editorsConfig	: {
		tbar: { items: [{ tooltip: 'Записать файл' },{ tooltip: 'Обновить файл' }] }
	},
	navigatorConfig	: {
		actions		: {
			addFile		: { title: 'Добавить файл', tooltip: 'Добавить новый файл' },
			downloadFile: { title: 'Скачать файл', tooltip: 'Скачать выбранный файл' },
			editFile	: { title: 'Редактировать файл', tooltip: 'Редактировать выбранный файл' },
			levelUp		: { title: 'Каталогом выше', tooltip: 'Каталогом выше' },
			refreshStore: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeFile	: { title: 'Удалить файл', tooltip: 'Удалить выделенный файл' }
		},
		columns		: [{ text: 'Название' }],
		header		: { title: 'Просмотр файлов' },
		viewConfig	: { emptyText: 'Выберите файлы или их перетащите сюда.' },
	},
	newFileTitle	: 'Имя файла',
	newFileMsg		: 'Введите имя файла:',
	uploadMsg		: 'Загрузка файлов...',
	confirmRemove: function(path, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить ' + path + '?', fn, this);
	}
});