Ext.define('CFJS.locale.ru.panel.ResourceProperties', {
	override			: 'CFJS.panel.ResourceProperties',
	config				: {
		actions				: {
			downloadResource: { tooltip: 'Скачать выбранный ресурс' },
			saveResource	: { tooltip: 'Записать внесенные изменения' }
		},
		bind				: { title: 'Свойства ресурса: {resource.name}' },
		descriptionConfig	: {
			items: [
				{ emptyText: 'название ресурса', fieldLabel: 'Название' },
				{ emptyText: 'описание ресурса', fieldLabel: 'Описание' }
			],
			title: 'Описание'
		}
	},
	sharingConfig		: {
		items: [{
			items: [{
				items: [{ emptyText: 'срок действия', fieldLabel: 'Срок действия' },{ boxLabel: 'Одноразовая' },{ text: 'Создать', tooltip: 'Создать ссылку'}],
			},{
				items: [{ fieldLabel: 'Ссылка' },{ text: 'Удалить', tooltip: 'Удалить ссылку'}]
			}],
			title: 'Внешняя ссылка'
		}],
		title: 'Доступ'
	},
	versionsConfig	: { columns: [{ text: 'Автор' },{ text: 'Создан' }], title: 'Версии' },
	confirmRemove: function(fileName, fn) {
		return Ext.fireEvent('confirm', 'Вы действительно хотите удалить файл ' + fileName + '?', fn, this);
	}
});
