Ext.define('CFJS.locale.ru.panel.BaseEditor', {
	override: 'CFJS.panel.BaseEditor',
	config	: {
		actions: {
			back			: { tooltip: 'Назад к списку' },
			addRecord		: { tooltip: 'Добавить новую запись' },
			refreshRecord	: {	tooltip: 'Обновить данные' },
			removeRecord	: { tooltip: 'Удалить текущую запись' },
			saveRecord		: { tooltip: 'Записать внесенные изменения' }
		}
	}
});
