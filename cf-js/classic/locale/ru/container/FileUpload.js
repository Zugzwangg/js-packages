Ext.define('CFJS.locale.ru.container.FileUpload', {
	override	: 'CFJS.container.FileUpload',
	buttonText	: 'Обзор',
	config		: {
		title		: 'Выберите файл или перетащите его сюда',
		uploadText	: 'Выберите файл или перетащите его сюда'
	}
});
