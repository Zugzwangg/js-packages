Ext.define('CFJS.locale.ru.picker.Period', {
	override: 'CFJS.picker.Period',
	config	: {
		datesText		: 'Произвольный',
		dayText			: 'День',
		weekText		: 'Неделя',
		monthText		: 'Месяц',
		quarterText		: 'Квартал',
		yearText		: 'Год',
		okButtonText	: 'Применить',
		cancelButtonText: 'Отмена',
		title			: 'Рабочий период'
	}
});
