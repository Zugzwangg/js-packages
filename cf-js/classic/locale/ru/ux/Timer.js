Ext.define('CFJS.locale.ru.ux.Timer', {
	override: 'CFJS.ux.Timer',
	config	: {
		sectionsConfig: {
			days	: 'ДНИ',
			hours	: 'ЧАСЫ',
			minutes	: 'МИНУТЫ',
			seconds	: 'СЕКУНДЫ'
		}
	}
});
