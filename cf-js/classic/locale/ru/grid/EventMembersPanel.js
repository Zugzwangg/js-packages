Ext.define('CFJS.locale.ru.grid.EventMembersPanel', {
	override	: 'CFJS.grid.EventMembersPanel',
	config		: { title: 'Участники события'},
	nameText	: 'Пользователь',
	removeText	: 'Удалить выбранного пользователя',
	valueText	: 'Очередность',
	confirmRemove: function(userName, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить пользователя ' + userName + '?', fn, this);
	},
	initComponent: function() {
		CFJS.merge(this, {
			headerConfig	: { items: [{ tooltip: 'Добавить нового пользователя' }] },
			userSelectors	: { customer: { title: 'Клиент' }, person: { title: 'Работник' }, post: { title: 'Должность' }, role: { title: 'Роль' } },
		}).callParent();
	}
});