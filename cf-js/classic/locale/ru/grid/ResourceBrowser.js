Ext.define('CFJS.locale.ru.grid.ResourceBrowser', {
	override	: 'CFJS.grid.ResourceBrowser',
	config		: {
		actions	: {
			downloadFile: { tooltip: 'Скачать выбранный ресурс' },
			levelUp		: { title: 'На уровень выше', tooltip: 'Перейти в папку на уровень выше' },
			makeFolder	: { title: 'Создать папку', tooltip: 'Создать новую папку' },
			reloadStore	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeFile	: { title: 'Удалить ресурс', tooltip: 'Удалить выделенный ресурс' }
		},
		columnsText	: [{ filter: { emptyText: 'название ресурса' }, text: 'Название' },{ text: 'Размер' },{ text: 'Созданный' },{ text: 'Последнее изменение' }],
		fileUpload	: { buttonConfig: { tooltip: 'Загрузить файл' }, index: 2 },
		title		: 'Просмотр файлов',
	    waitMsg		: 'Загрузка файлов...'
	},
	viewConfig	: { emptyText: 'Выберите файлы или их перетащите сюда.', deferEmptyText: false },
	confirmRemove: function(fileName, isFile, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить ' + (isFile ? 'файл ' : 'папку ') + fileName + '?', fn, this);
	}
});
