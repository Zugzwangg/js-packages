Ext.define('CFJS.locale.ru.grid.FileBrowser', {
	override	: 'CFJS.grid.FileBrowser',
	config		: {
		actions	: {
			downloadFile: { tooltip: 'Скачать выбранный файл' },
			levelUp		: { title: 'На уровень выше', tooltip: 'Перейти в папку на уровень выше' },
			reloadStore	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeFile	: { title: 'Удалить файл', tooltip: 'Удалить выделенный файл' }
		},
		columnsText	: [{ filter: { emptyText: 'имя файла' }, text: 'Имя файла' },{ text: 'Размер' },{ text: 'Последние изменения' },{ text: 'статус' }],
		fileUpload	: { buttonConfig: { tooltip: 'Загрузить файл' }, index: 1 },
		title		: 'Просмотр файлов',
	    waitMsg		: 'Загрузка файлов...'
	},
	viewConfig	: { emptyText: 'Выберите файлы или их перетащите сюда.', deferEmptyText: false },
	confirmRemove: function(fileName, isFile, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить ' + (isFile ? 'файл ' : 'папку ') + fileName + '?', fn, this);
	},
	rendererStatus: function(value, metaData, record, rowIndex, colIndex, store) {
		var color = 'grey';
		if (value === 'IN_QUEUE') {
			color = 'blue'; value = 'В очереди';
		} else if (value === 'UPLOADING') {
            color = 'orange'; value = 'Загружается';
        } else if (value === 'ERROR') {
            color = "red"; value = 'Ошибка';
        }
        metaData.tdStyle = 'color:' + color + ";";
        return value;
	}
});
