Ext.define('CFJS.locale.ru.grid.BasePanel', {
	override: 'CFJS.grid.BasePanel',
	config	: {
		actions: {
			addRecord	: { title: 'Добавить запись', tooltip: 'Добавить новую запись' },
			clearFilters: { title: 'Очистить фильтры', tooltip: 'Очистить все фильтры' },
			editRecord	: { title: 'Редaктировать запись', tooltip: 'Редaктировать текущую запись' },
			periodPicker: { title: 'Рабочий период', tooltip: 'Выбор рабочего периода' },
			reloadStore	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeRecord: { title: 'Удалить записи', tooltip: 'Удалить выделенные записи' }
		}
	},
	emptyText: 'Нет данных для отображения'
});
