Ext.define('CFJS.locale.ru.grid.InternalSharesPanel', {
	override: 'CFJS.grid.InternalSharesPanel',
	config	: { title: 'Внутренний доступ'},
	confirmRemove: function(userName, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите запретить доступ для' + userName + '?', fn, this);
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{},{ items: [{ tooltip: 'Удалить доступ для текущего пользователя' }] }],
			header	: { items: [{ tooltip: 'Добавить доступ к ресурсу', menu: [{ title: 'Работник' },{ title: 'Клиент' }] }] }
		});
		me.callParent();
	}
});
