Ext.define('CFJS.locale.ru.grid.SimpleResourceBrowser', {
	override	: 'CFJS.grid.SimpleResourceBrowser',
	columnsText	: [
		{ filter: { emptyText: 'имя файла' }, text: 'Имя файла' },
		{ filter: { emptyText: 'имя автора' }, text: 'Автор' },
		{ filter: { emptyText: 'размер в байтах' }, text: 'Размер' },
		{ text: 'Последние изменения' }
	],
	config		: {
		actions		: {
			downloadFile: { tooltip: 'Скачать выбранный файл' },
			reloadStore	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeFile	: { title: 'Удалить файл', tooltip: 'Удалить выделенный файл' }
		},
		fileUpload	: { buttonConfig: { tooltip: 'Загрузить файл' }, index: 0 },
	},
	viewConfig	: { emptyText: 'Выберите файлы или их перетащите сюда.', deferEmptyText: false },
	waitMsg		: 'Загрузка файлов...',
	confirmRemove: function(fileName, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить ' + fileName + '?', fn, this);
	}

});