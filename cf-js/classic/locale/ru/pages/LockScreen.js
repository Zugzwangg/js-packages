Ext.define('CFJS.locale.ru.pages.LockScreen', {
	override			: 'CFJS.pages.LockScreen',
	unlockPanelConfig	: {
		items: [{},{
			items: [
				{ emptyText	: 'пароль', fieldLabel: 'Прошло много времени. Введите пароль для продолжения' },
				{ text		: 'Разблокировать' },
				{ html		: '<div style="text-align:right"><a href="#logout" class="link-logout">или, войдите в систему, используя другие учетные данные</a></div>'}
			]
		}]
	}

});
