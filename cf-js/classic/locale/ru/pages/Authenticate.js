Ext.define('CFJS.locale.ru.pages.Authenticate', {
	override			: 'CFJS.pages.Authenticate',
	pwdСomplexityText	: [
		'Пароль должен содержать латинские буквы',
		'Пароль должен содержать большие и малые латинские буквы',
		'Пароль должен содержать цифры, большие и малые латинские буквы',
		'Пароль должен содержать цифры, специальные символы, большие и малые латинские буквы'
	],
	loginPanelConfig	: {
		items: [
			{ text		: 'Войдите в свой аккаунт'},
			{ emptyText	: 'идентификатор пользователя' },
			{ emptyText	: 'пароль' },
			{ blankText	: 'Вы должны выбрать сервис', emptyText: 'укажите сервис'},
			{ 
				items: [
					{ boxLabel: 'Запомнить меня' },
					{ html: '<div class="link-forgot-password" style="text-align:right">Забыли пароль?</div>'}
				]
			},
			{ text		: 'Войти' }
		]
	},
	recoveryEmailConfig	: {
		items: [
			{ text: 'Введите свой электронный адрес для дальнейших инструкций' },
			{},
			{ text: 'Отправить письмо' },
			{ html: '<div class="link-login" style="text-align:right">Вернуться к регистрации</div>' }
		]
	},
	resetPasswordConfig	: {
		items: [
			{ text		: 'Установить новый пароль' },
			{ emptyText	: 'пароль' },
			{ emptyText	: 'подтвердите пароль' },
			{ text		: 'Установить пароль' }
		]
	}
});
