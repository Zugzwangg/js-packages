Ext.define('CFJS.locale.ru.pages.Error500', {
	override: 'CFJS.pages.Error500',
	bodyText: 'Что-то пошло не так, и сервер не смог обработать ваш запрос.'
});
