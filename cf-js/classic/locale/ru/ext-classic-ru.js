Ext.define('Ext.locale.ru.form.Labelable', {
	override: 'Ext.form.Labelable',
	statics	: {
		REQUIRED: ['<span style="color:red;font-weight:bold" data-qtip="Обязательное для заполнения">*</span>']
	}
});
Ext.define('Ext.locale.ru.grid.RowEditor', {
	override		: 'Ext.grid.RowEditor',
	saveBtnText		: 'Применить',
	cancelBtnText	: 'Отменить',
	errorsText		: 'Ошибки',
	dirtyText		: 'Вы должны применить или отменить внесенные изменения'
});
Ext.define('Ext.locale.ru.grid.column.Boolean', {
	override	: 'Ext.grid.column.Boolean',
	falseText	: 'нет',
	trueText	: 'да'
});
Ext.define('Ext.locale.ru.grid.filters.Filters', {
	override		: 'Ext.grid.filters.Filters',
    menuFilterText	: 'Фильтры'
});
Ext.define('Ext.locale.ru.panel.Panel', {	
	override		: 'Ext.panel.Panel',
	closeToolText	: 'Закрыть панель',
	collapseToolText: 'Свернуть панель',
	expandToolText	: 'Развернуть панель'
});
Ext.define('Ext.locale.ru.window.Window', {
	override		: 'Ext.window.Window',
	closeToolText	: 'Закрыть окно',
	restoreToolText	: 'Восстановить размер окна',
	minimizeToolText: 'Минимизировать размер окна',
	maximizeToolText: 'Максимизировать размер окна'
});
