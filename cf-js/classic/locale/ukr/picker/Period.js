Ext.define('CFJS.locale.ukr.picker.Period', {
	override: 'CFJS.picker.Period',
	config	: {
		datesText		: 'Довільний',
		dayText			: 'День',
		weekText		: 'Тиждень',
		monthText		: 'Місяць',
		quarterText		: 'Квартал',
		yearText		: 'Рік',
		okButtonText	: 'Застосувати',
		cancelButtonText: 'Відмінити',
		title			: 'Робочий період'
	}
});
