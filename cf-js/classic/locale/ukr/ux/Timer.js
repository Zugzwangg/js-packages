Ext.define('CFJS.locale.ukr.ux.Timer', {
    override: 'CFJS.ux.Timer',
    config	: {
    	sectionsConfig: {
    		days	: 'ДНІ',
    		hours	: 'ГОДИНИ',
    		minutes	: 'ХВИЛИНИ',
    		seconds	: 'СЕКУНДИ'
    	}
    }
});
