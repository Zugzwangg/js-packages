Ext.define('CFJS.locale.ukr.container.FileUpload', {
	override	: 'CFJS.container.FileUpload',
	buttonText	: 'Перегляд',
	config		: {
		title		: 'Оберіть файл або перетягніть його сюди',
		uploadText	: 'Оберіть файл або перетягніть його сюди'
	}
});
