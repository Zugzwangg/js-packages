Ext.define('CFJS.locale.ukr.pages.LockScreen', {
	override			: 'CFJS.pages.LockScreen',
	unlockPanelConfig	: {
		items: [{},{
			items: [
				{ emptyText	: 'пароль', fieldLabel: 'Пройшло багато часу. Введіть пароль для продовження' },
				{ text		: 'Розблокувати' },
				{ html		: '<div style="text-align:right"><a href="#logout" class="link-logout">або, увійдіть в систему, використовуючи інші облікові дані</a></div>'}
			]
		}]
	}
});
