Ext.define('CFJS.locale.ukr.pages.Authenticate', {
	override			: 'CFJS.pages.Authenticate',
	pwdСomplexityText	: [
		'Пароль повинен містити латинські літери',
		'Пароль повинен містити великі та малі латинські літери',
		'Пароль повинен містити цифри, великі та малі латинські літери',
		'Пароль повинен містити цифри, спеціальні символи, великі та малі латинські літери'
	],
	loginPanelConfig	: {
		items: [
			{ text		: 'Увійдіть до свого облікового запису'},
			{ emptyText	: 'ідентифікатор користувача' },
			{ emptyText	: 'пароль' },
			{ blankText	: 'Ви повинні обрати сервіс', emptyText: 'оберіть сервіс'},
			{ 
				items: [
					{ boxLabel: 'Запам\'ятати мене' },
					{ html: '<div class="link-forgot-password" style="text-align:right">Забули пароль?</div>'}
				]
			},
			{ text		: 'Увійти' }
		]
	},
	recoveryEmailConfig	: {
		items: [
			{ text: 'Введіть свою електронну адресу для відправки подальших інструкцій' },
			{},
			{ text: 'Відправити лист' },
			{ html: '<div class="link-login" style="text-align:right">Повернутися до реєстрації</div>' }
		]
	},
	resetPasswordConfig	: {
		items: [
			{ text		: 'Встановити новий пароль' },
			{ emptyText	: 'пароль' },
			{ emptyText	: 'підтвердіть пароль' },
			{ text		: 'Встановити пароль' }
		]
	}
});
