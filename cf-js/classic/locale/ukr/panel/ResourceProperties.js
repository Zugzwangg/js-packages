Ext.define('CFJS.locale.ukr.panel.ResourceProperties', {
	override			: 'CFJS.panel.ResourceProperties',
	config				: {
		actions				: {
			downloadResource: { tooltip: 'Завантажити обраний ресурс' },
			saveResource	: { tooltip: 'Записати внесені зміни' }
		},
		bind				: { title: 'Властивості ресурсу: {resource.name}' },
		descriptionConfig	: {
			items: [
				{ emptyText: 'назва ресурсу',	fieldLabel: 'Назва' },
				{ emptyText: 'опис ресурсу',	fieldLabel: 'Опис' }
			],
			title: 'Опис'
		}
	},
	sharingConfig		: {
		items: [{
			items: [{
				items: [{ emptyText: 'термін дії', fieldLabel: 'Термін дії' },{ boxLabel: 'Одноразове' },{ text: 'Створити', tooltip: 'Створити посилання'}],
			},{
				items: [{ fieldLabel: 'Посилання' },{ text: 'Видалити', tooltip: 'Видалити посилання'}]
			}],
			title: 'Зовнішнє посилання'
		}],
		title: 'Доступ'
	},
	versionsConfig	: { columns: [{ text: 'Автор' },{ text: 'Створений' }], title: 'Версії' },
	confirmRemove: function(fileName, fn) {
		return Ext.fireEvent('confirm', 'Ви дійсно бажаєте видалити файл ' + fileName + '?', fn, this);
	}
});
