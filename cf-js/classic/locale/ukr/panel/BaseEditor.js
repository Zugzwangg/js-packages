Ext.define('CFJS.locale.ukr.panel.BaseEditor', {
	override: 'CFJS.panel.BaseEditor',
	config	: {
		actions: {
			back			: { tooltip: 'Назад до списку' },
			addRecord		: { tooltip: 'Додати новий запис' },
			refreshRecord	: {	tooltip: 'Оновити дані' },
			removeRecord	: { tooltip: 'Видалити поточний запис' },
			saveRecord		: { tooltip: 'Записати внесені зміни' }
		}
	}
});
