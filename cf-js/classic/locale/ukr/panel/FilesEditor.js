Ext.define('CFJS.locale.ukr.panel.FilesEditor', {
	override		: 'CFJS.panel.FilesEditor',
	editorsConfig	: {
		tbar: { items: [{ tooltip: 'Записати файл' },{ tooltip: 'Оновити файл' }] } 
	},
	navigatorConfig	: {
		actions		: {
			addFile		: { title: 'Додати файл', tooltip: 'Додати новий файл' },
			downloadFile: { title: 'Завантажити файл', tooltip: 'Завантажити обраний файл' },
			editFile	: { title: 'Редагувати файл', tooltip: 'Редагувати обраний файл' },
			levelUp		: { title: 'Каталогом вище', tooltip: 'Каталогом вище' },
			refreshStore: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeFile	: { title: 'Видалити файл', tooltip: 'Видалити обраний файл' }
		},
		columns		: [{ text: 'Назва' }],
		header		: { title: 'Перегляд файлів' },
		viewConfig	: { emptyText: 'Оберіть файли або їх перетягніть сюди.' },
	},
	newFileTitle	: 'Ім\'я файлу',
	newFileMsg		: 'Будь ласка, введіть ім \'я файлу:',
	uploadMsg		: 'Завантаження файлів...',
	confirmRemove: function(path, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити файл ' + path + '?', fn, this);
	}
});