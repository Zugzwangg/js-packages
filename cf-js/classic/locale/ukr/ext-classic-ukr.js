Ext.define('Ext.locale.ukr.form.Labelable', {
	override: 'Ext.form.Labelable',
	statics	: {
		REQUIRED: ['<span style="color:red;font-weight:bold" data-qtip="Обов\'язкове для заповнення">*</span>']
	}
});
Ext.define('Ext.locale.ukr.grid.RowEditor', {
	override		: 'Ext.grid.RowEditor',
	saveBtnText		: 'Застосувати',
	cancelBtnText	: 'Скасувати',
	errorsText		: 'Помилки',
	dirtyText		: 'Ви повинні застосувати чи скасувати внесені зміни'
});
Ext.define('Ext.locale.ukr.grid.column.Boolean', {
	override	: 'Ext.grid.column.Boolean',
	falseText	: 'ні',
	trueText	: 'так'
});
Ext.define('Ext.locale.ukr.grid.filters.Filters', {
	override		: 'Ext.grid.filters.Filters',
	menuFilterText	: 'Фільтри'
});
Ext.define('Ext.locale.ukr.panel.Panel', {
	override		: 'Ext.panel.Panel',
	closeToolText	: 'Закрити панель',
	collapseToolText: 'Згорнути панель',
	expandToolText	: 'Розгорнути панель'
});
Ext.define('Ext.locale.ukr.window.Window', {
	override		: 'Ext.window.Window',
	closeToolText	: 'Закрити вікно',
	restoreToolText	: 'Відновити розмір вікна',
	minimizeToolText: 'Мінімізувати розмір вікна',
	maximizeToolText: 'Максимізувати розмір вікна'
});
