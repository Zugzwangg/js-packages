Ext.define('CFJS.locale.ukr.grid.EventMembersPanel', {
	override	: 'CFJS.grid.EventMembersPanel',
	config		: { title: 'Учасники події' },
	nameText	: 'Користувач',
	removeText	: 'Видалити вибраного користувача',
	valueText	: 'Послідовність',
	confirmRemove: function(userName, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити користувача ' + userName + '?', fn, this);
	},
	initComponent: function() {
		CFJS.merge(this, {
			headerConfig	: { items: [{ tooltip: 'Додати нового користувача' }] },
			userSelectors	: { customer: { title: 'Клієнт' }, person: { title: 'Працівник' }, post: { title: 'Посада' }, role: { title: 'Роль' } },
		}).callParent();
	}
});