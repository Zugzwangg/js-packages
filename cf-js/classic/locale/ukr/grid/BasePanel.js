Ext.define('CFJS.locale.ukr.grid.BasePanel', {
	override: 'CFJS.grid.BasePanel',
	config	: {
		actions: {
			addRecord	: { title: 'Додати запис', tooltip: 'Додати новий запис' },
			clearFilters: { title: 'Очистити фільтри', tooltip: 'Видалити всі фільтри' },
			editRecord	: { title: 'Редагувати запис', tooltip: 'Редагувати поточний запис' },
			periodPicker: { title: 'Робочий період', tooltip: 'Вибір робочого періоду' },
			reloadStore	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeRecord: { title: 'Видалити записи', tooltip: 'Видалити обрані записи' }
		}
	},
	emptyText: 'Дані для відображення відсутні'
});
