Ext.define('CFJS.locale.ukr.grid.InternalSharesPanel', {
	override: 'CFJS.grid.InternalSharesPanel',
	config	: { title: 'Внутрішній доступ'},
	confirmRemove: function(userName, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете заборонити доступ для ' + userName + '?', fn, this);
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{},{ items: [{ tooltip: 'Видалити доступ для поточного користувача' }] }],
			header	: { items: [{ tooltip: 'Додати доступ до ресурсу', menu: [{ title: 'Працівник' },{ title: 'Клієнт' }] }] }
		});
		me.callParent();
	}
});
