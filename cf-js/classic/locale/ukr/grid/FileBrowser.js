Ext.define('CFJS.locale.ukr.grid.FileBrowser', {
	override	: 'CFJS.grid.FileBrowser',
	config		: {
		actions	: {
			downloadFile: { tooltip: 'Завантажити обраний файл' },
			levelUp		: { title: 'На рівень вище', tooltip: 'Перейти в теку на рівень вище' },
			reloadStore	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeFile	: { title: 'Видалити файл', tooltip: 'Видалити обраний файл' }
		},
		columnsText	: [{ filter: { emptyText: 'ім\'я файлу' }, text: 'Ім\'я файлу' },{ text: 'Розмір' },{ text: 'Останні зміни' },{ text: 'Статус' }],
		fileUpload	: { buttonConfig: { tooltip: 'Завантажити файл' }, index: 1 },
		title		: 'Перегляд файлів',
	    waitMsg		: 'Завантаження файлів...'
	},
	viewConfig	: { emptyText: 'Оберіть файли або їх перетягніть сюди.', deferEmptyText: false },
	confirmRemove: function(fileName, isFile, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити ' + (isFile ? 'файл ' : 'теку ') + fileName + '?', fn, this);
	},
	rendererStatus: function(value, metaData, record, rowIndex, colIndex, store) {
		var color = 'grey';
		if (value === 'IN_QUEUE') {
			color = 'blue'; value = 'В черзі';
		} else if (value === 'UPLOADING') {
            color = 'orange'; value = 'Завантажується';
        } else if (value === 'ERROR') {
            color = "red"; value = 'Помилка';
        }
        metaData.tdStyle = 'color:' + color + ";";
        return value;
	}
});
