Ext.define('CFJS.locale.ukr.grid.ResourceBrowser', {
	override	: 'CFJS.grid.ResourceBrowser',
	config		: {
		actions	: {
			downloadFile: { tooltip: 'Завантажити обраний ресурс' },
			levelUp		: { title: 'На рівень вище', tooltip: 'Перейти в теку на рівень вище' },
			makeFolder	: { title: 'Створити теку', tooltip: 'Створити нову теку' },
			reloadStore	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeFile	: { title: 'Видалити ресурс', tooltip: 'Видалити обраний ресурс' }
		},
		columnsText	: [{ filter: { emptyText: 'назва ресурсу' }, text: 'Назва' },{ text: 'Розмір' },{ text: 'Створений' },{ text: 'Останнє змінення' }],
		fileUpload	: { buttonConfig: { tooltip: 'Завантажити файл' }, index: 2 },
		title		: 'Перегляд файлів',
	    waitMsg		: 'Завантаження файлів...'
	},
	viewConfig	: { emptyText: 'Оберіть файли або їх перетягніть сюди.', deferEmptyText: false },
	confirmRemove: function(fileName, isFile, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити ' + (isFile ? 'файл ' : 'теку ') + fileName + '?', fn, this);
	}
});
