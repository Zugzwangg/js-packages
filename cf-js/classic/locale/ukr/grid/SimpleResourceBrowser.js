Ext.define('CFJS.locale.ukr.grid.SimpleResourceBrowser', {
	override	: 'CFJS.grid.SimpleResourceBrowser',
	columnsText	: [
		{ filter: { emptyText: 'ім\'я файлу' }, text: 'Ім\'я файлу' },
		{ filter: { emptyText: 'ім\'я автора' }, text: 'Автор' },
		{ filter: { emptyText: 'розмір в байтах' }, text: 'Розмір' },
		{ text: 'Останні зміни' }
	],
	config		: {
		actions		: {
			downloadFile: { tooltip: 'Завантажити вибраний файл' },
			reloadStore	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeFile	: { title: 'Видалити файл', tooltip: 'Видалити вибраний файл' }
		},
		fileUpload	: { buttonConfig: { tooltip: 'Завантажити файл' }, index: 0 },
	},
	viewConfig	: { emptyText: 'Оберіть файли або їх перетягніть сюди.', deferEmptyText: false },
	waitMsg		: 'Завантаження файлів...',
	confirmRemove: function(fileName, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити файл ' + fileName + '?', fn, this);
	}

});