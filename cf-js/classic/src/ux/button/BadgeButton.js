Ext.define('CFJS.ux.button.BadgeButton', {
	extend	: 'Ext.button.Button',
	alias	: 'widget.badgebutton',
	mixins	: [ 'CFJS.ux.mixin.Badge' ]
});
