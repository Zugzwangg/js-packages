Ext.define('CFJS.ux.mixin.Badge', {
	extend		: 'Ext.Mixin',
	mixinConfig	: {
		id		: 'badge',
		after	: {
			afterRender: 'afterRender'
		}
	},
	
	afterRender: function() {
		var me = this;
			me.badgeText = me.badgeText || '';
		if (!me.el) {
			me.on('render', Ext.pass(me.setBadgeText, [me.badgeText, true], me), me, { single: true });
			return;
		}
		me.el.addCls('button-badge');
		me.setBadgeText(me.badgeText, true);
	},

	setBadgeText: function(text, force) {
		var me = this,
			oldBadgeText = me.badgeText,
			el = me.el,
			dom = el && el.dom;
		if (el && dom) {
			if (force || oldBadgeText !== text) {
				me.badgeText = text;
				dom.setAttribute('data-button-badge', me.badgeText || '');
				if (Ext.isEmpty(me.badgeText)) el.addCls('hide-badge');
				else el.removeCls('hide-badge');
				me.fireEvent('badgetextchange', me, oldBadgeText, text);
			}
		} else {
			console.warn('Unable to set badge without valid el and dom references');
		}
	},
	
	getBadgeText: function() {
		return me.badgeText;
	}
	
});
