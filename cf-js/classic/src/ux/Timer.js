Ext.define('CFJS.ux.Timer', {
	extend			: 'Ext.Component',
	alias			: 'widget.timer',
	baseCls			: Ext.baseCSSPrefix + 'timer',
	config			: {
		endAt			: null,
		interval		: 1,
		startAt			: null,
		value			: null,
		sectionsConfig	: {
			days	: 'DAYS',
			hours	: 'HOURS',
			minutes	: 'MINUTES',
			seconds	: 'SECONDS'
		}
	},
	liquidLayout	: true,
	_timerWrapCls	: Ext.baseCSSPrefix + 'timer-wrap',
	
	renderTpl		: [
		'<div id="{id}-timerWrap" data-ref="timerWrap" role="presentation" class="{timerWrapCls}">',
			'<tpl for="sections">',
				'<span class="number-wrap" >',
					'<div class="line"></div>',
					'<div class="caption">{caption}</div>',
					'<span class="{cls} number"></span>',
				'</span>',
			'</tpl>',
		'</div>'
	],
	
	applyInterval: function(interval) {
		return interval > 0 ? interval : 1;
	},
	
	applySectionsConfig: function(config) {
		var keys = ['days', 'hours', 'minutes', 'seconds' ],
			sections = [], i = 0, section;
		config = config || {};
		for (; i < keys.length; i++) {
			if (section = config[keys[i]]) {
				if (Ext.isString(section)) section = { text: section };
				sections.push({
					caption	: section.text,
					cls		: Ext.baseCSSPrefix + keys[i]
				});
			}
		}
		this.sections = sections;
		return config;
	},
	
	applyStartAt: function(startAt) {
		if (!Ext.isDate(startAt)) {
			try { startAt = Date.parse(startAt); }
			catch (e) { startAt = null }
		}
		return startAt || new Date();
	},
	
	afterRender: function() {
		var me = this;
		me.startAt = me.startAt || new Date();
		me.run();
		me.callParent();
	},
	
	getTemplateArgs: function() {
		var me = this;
		return {
			timerWrapCls: me._timerWrapCls,
			sections	: me.sections
		}
	},
	
	initRenderData: function () {
		return Ext.apply(this.callParent(), this.getTemplateArgs());
	},
	
	run: function() {
		var me = this, date = Ext.Date, value = {}, now = new Date();
			diff = Math.floor((now - me.getStartAt()) / 1000);
		if (!me.destroyed && !Ext.Date.after(now, me.getEndAt())) {
			value.seconds = diff % 60;
			value.minutes = (diff = Math.floor((diff - value.seconds)/ 60)) % 60;
			value.hours = (diff = Math.floor((diff - value.minutes)/ 60)) % 24;
			value.days = (diff = Math.floor((diff - value.hours) / 24));
			me.setValue(value);
			setTimeout(function() { me.run(); }, me.interval * 1000);
		} 
	},
	
	updateValue: function(value) {
		var me = this, selector = '.' + Ext.baseCSSPrefix,
		 	el, section, key;
		value = value || {};
		if (me.rendered && (el = me.getEl())) {
			for (key in value) {
				section = el.down(selector + key);
				if (section) section.setText(String(value[key]).lpad(2, '0'));
			}
		}
	}
    
});