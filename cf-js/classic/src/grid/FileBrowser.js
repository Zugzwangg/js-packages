Ext.define('CFJS.grid.FileBrowser', {
	extend			: 'CFJS.grid.FilesPanel',
	xtype			: 'grid-file-browser',
	requires		: [ 'CFJS.store.Files' ],
	config			: {
		actions		: {
			downloadFile: {
				isDisabled: function(view, rowIdx, colIdx, item, record) { var state = record.get('state'); return  state !== 'OK' && state !== 'IN_QUEUE'; }
			},
			removeFile: {
				isDisabled: function(view, rowIdx, colIdx, item, record) { var state = record.get('state'); return  state !== 'OK' && state !== 'IN_QUEUE'; }
			}
		},
		fileType	: 'customers',
		pathPrefix	: null,
	},
	disableSelection: true,
	reference		: 'filesGrid',
	service			: 'resources',
    
	applyFolder: function(folder) {
		if (!folder || !Ext.isString(folder)) folder = '';
		return folder[0] !== '/' ? folder : folder.substr(1);
	},
	
	applyPathPrefix: function(pathPrefix) {
		pathPrefix = '' + (pathPrefix || '');
		if (!pathPrefix.endsWith('/')) pathPrefix = pathPrefix + '/';
		return pathPrefix[0] !== '/' ? '/' + pathPrefix : pathPrefix;
	},
	
	renderColumns: function() {
		var me = this, actions = me.getActions();
		return CFJS.apply([{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'file name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name',
			tpl			: '<span class="{iconCls}" style="margin-right: 5px;"></span>{name}',
			xtype		: 'templatecolumn'
		},{
			align		: 'right',
			dataIndex	: 'size',
			renderer	: Ext.util.Format.fileSize,
			style		: { textAlign: 'center' },
			text		: 'Size',
			width		: 150
		},{
			align		: 'center',
			dataIndex	: 'date',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Last modified',
			width		: 170,
			xtype		: 'datecolumn'
		},{
			align		: 'left',
			dataIndex	: 'state',
			renderer	: me.rendererStatus,
			style		: { textAlign: 'center' },
			text		: 'State',
			width		: 150
		},{
			align		: 'center',
			items		: [ actions.downloadFile, actions.removeFile ],
			menuDisabled: true,
			sortable	: false,
			width		: 50,
			xtype		: 'actioncolumn'
		}], me.columnsText);
	},
	
	createStore: function() {
		var me = this,
			store = new CFJS.store.Files({ autoDestroy: true, autoLoad: true }),
			proxy = store.getProxy(),
			extraParams = proxy.extraParams = proxy.extraParams || {};	
		extraParams.path = me.getPathPrefix() + (me.getFolder() || '');
		if (!Ext.isEmpty(me.getFileType())) extraParams.type = me.getFileType();
		return store;
	},
	
	initComponent: function() {
		var me = this;
		me.disableComponent(me.getActions().removeFile, !(me.getPermissions() || {})['delete']);
		me.callParent();
	},
	
	onLevelUp: function(button) {
		var me = this, paths = (me.getFolder() || '').split('/');
		paths.pop();
		me.setFolder(paths.join('/'));
	},
	
	rendererStatus: function(value, metaData, record, rowIndex, colIndex, store) {
		var color = 'grey';
		if (value === 'IN_QUEUE') {
			color = 'blue'; value = 'In queue';
		} else if (value === 'UPLOADING') {
            color = 'orange';
        } else if (value === 'ERROR') {
            color = "red";
        }
        metaData.tdStyle = 'color:' + color + ";";
        return value;
	},
	
	updateFolder: function(folder) {
		this.setProxyExtraParam('path', this.pathPrefix + (folder || ''));
		this.callParent(arguments)
	},
	
	updatePathPrefix: function(pathPrefix) {
		this.setProxyExtraParam('path', pathPrefix + (this.getFolder() || ''));
	},
	
	privates: {
		
		download: function(record) {
			if (record && Ext.isFunction(record.download)) {
				var me = this;
				record.download(null, { type: (me.getFileType() || '') + me.getPathPrefix() + (me.getFolder() || '') });
			}
		},
		
		goFolder: function(record) {
			this.setFolder([this.getFolder(), record.get('name')].join('/'));
		},
		
		removeFile: function(record, callback) {
			var me = this, path = record.get('name'), 
				type = (me.getFileType() || '') + me.pathPrefix + (me.getFolder() || '');
			if (Ext.isString(path) && !Ext.isEmpty(path)) {
				record.erase({ params: { path: path, type: type }, callback: callback });
			}
		},
		
		setProxyExtraParam: function(key, value, autoLoad) {
			var store = this.getStore(), proxy, extraParams;
			if (store) {
				proxy = store.getProxy();
				extraParams = proxy.extraParams = proxy.extraParams || {};
				if (Ext.isEmpty(value)) delete extraParams[key];
				else extraParams[key] = value;
				if (autoLoad !== false) store.load();
			}
		},

		uploadParams: function() {
			var me = this, params = me.callParent();
			params.path = me.getPathPrefix() + (me.getFolder() || '') ;
			if (!Ext.isEmpty(me.getFileType())) params.type = me.getFileType();
			return params;
		}

	}
	
});
