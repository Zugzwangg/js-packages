Ext.define('CFJS.grid.EventMembersPanel', {
	extend				: 'CFJS.grid.EnumGrid',
	xtype				: 'grid-event-members-panel',
	requires			: [ 'Ext.button.Button', 'Ext.form.field.Number' ],
	defaultListenerScope: true,
	loadMask			: true,
	nameField			: 'name',
	nameText			: 'User',
	removeText			: 'Remove selected user',
	selectors			: [ 'person', 'customer', 'post', 'role' ],
	sortableColumns		: false,
	title				: 'Event members',
	userSelectors		: {
		customer: { iconCls: CFJS.UI.iconCls.USER,			title: 'Client' },
		person	: { iconCls: CFJS.UI.iconCls.USER_MALE,		title: 'Person' },
		post	: { iconCls: CFJS.UI.iconCls.USER_SECRET,	title: 'Post' },
		role	: { iconCls: CFJS.UI.iconCls.USERS,			title: 'Role' }
	},
	valueEditor			: {
		xtype			: 'numberfield',
		allowDecimals	: false,
		allowExponential: false,
		autoStripChars	: true,
		minValue		: 0,
		name			: 'priority'
	},
	valueField			: 'priority',
	valueText			: 'Priority',

	beforeSelectUser: function(selector, record) {
		return record ? true : false;
	},
	
	buildHeaderConfig: function(config, selectors) {
		var me = this, i = 0, userType,
			btn = Ext.theme.name === 'Crisp' ? { margin: '0 2', ui: 'default-toolbar' } : {};
		if (selectors) {
			config = CFJS.UI.buildHeader(CFJS.apply({
				items			: [{
					action		:  'addMember',
					arrowVisible: false,
					iconCls		: CFJS.UI.iconCls.USER_ADD,
					tooltip		: 'Add new member'
				}]
			}, config));
			if (selectors.length > 1) {
				delete btn.handler;
				btn.menu = [];
				for (; i < selectors.length; i++) {
					userType = (selectors[i] || '').toLowerCase();
					if (userType && userSelectors[userType]) {
						btn.menu.push(Ext.apply(userSelectors[userType], { handler: 'onAddMember', userType: userType }));
					}
				}
			} else if (selectors.length === 1) {
				userType = (selectors[0] || '').toLowerCase();
				if (userType) Ext.apply(btn, { handler: 'onAddMember', userType: userType });
			} else btn.disabled = true;
			CFJS.apply(config.items[0], btn);
		}
		return config;
	},
	
	confirmRemove: function(userName, fn) {
		return Ext.fireEvent('confirm', 'Are you sure you want to remove ' + userName + '?', fn, this);
	},

	renderColumns: function() {
		var me = this, columns = CFJS.apply(me.callParent(), [{},{
			xtype	: 'numbercolumn',
			align	: 'right',
			format	: '0'
		}]);
		columns.push({
			align		: 'center',
			items		: [{
				handler		: 'onRemoveMember',
				iconCls		: CFJS.UI.iconCls.CANCEL,
				isDisabled	: me.isRemoveDisabled,
				tooltip		: me.removeText
			}],
			menuDisabled: true,
			sortable	: false,
			tdCls		: Ext.baseCSSPrefix + 'multiselector-remove',
			width		: 30,
			xtype		: 'actioncolumn'
		})
		return columns;
	},
	
	createUserPicker: function(type, listeners) {
		var me = this,
			picker = Ext.apply({
				xtype			: 'admin-window-' + type + '-picker',
				closeAction		: 'hide',
				maximizable		: false,
				minWidth		: 540,
				minHeight		: 200,
				selectorType	: 'grid'
			}, me[type + 'PickerConfig']);
		picker.selectorConfig = Ext.apply(picker.selectorConfig || {}, me[type + 'SelectorConfig']);
		picker = Ext.create(picker);
		picker.on({ select: function() { picker.close() } });
		if (listeners) picker.on(listeners);
		return picker;
	},

	getUserPicker: function(type) {
		type = (type || 'person');
		var me = this, pName = type + 'Picker';
        if (!me[pName]) {
        	me.creatingPicker = true;
        	me[pName] = me.createUserPicker(type, { beforeselect: me.beforeSelectUser, select: me.onSelectUser, scope: me });
        	me[pName].ownerCmp = me;
        	delete me.creatingPicker;
        }
        return me[pName];
	},

	initComponent: function() {
		var me = this, selectors = me.selectors,
			userSelectors = me.userSelectors || {};
		if (selectors) {
			if (Ext.isString(selectors)) selectors = [selectors];
			else if (selectors === true) selectors = [ 'person', 'customer', 'post', 'role' ];
			else if (Ext.isObject(selectors)) {
				obj = selectors; selectors = [];
				for (key in obj) {
					if (obj[key] && obj.hasOwnProperty(key)) selectors.push(String(key));
				}
			}
			me.headerConfig = me.buildHeaderConfig(me.headerConfig, selectors);
		}
		me.callParent();
	},
	
	onAddMember: function(component) {
		if (component && component.userType) {
			var me = this, picker;
			if (!me.readOnly && me.rendered && !me.isExpanded && !me.destroyed) {
				picker = me.getUserPicker(component.userType);
				picker.setMaxHeight(picker.initialConfig.maxHeight);
				picker.show();
			}
		}
	},

	onRemoveMember: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this;
		if (!me.readOnly && record && record.isEntity) {
			me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					me.getStore().remove(record);
					me.disableComponent(component);
				}
			});
		}
	},
	
	onSelectUser: function(selector, record) {
		var me = this, store = me.getStore();
		if (!me.readOnly && store && store.isStore) {
			store.add(me.transformUserRecord(record));
		}
	},

	setReadOnly: function(readOnly) {
		var me = this;
		me.hideComponent(me.down('button[action=addMember]'), readOnly);
		me.callParent(arguments);
		me.view.refreshView();
	},
	
	privates: {
		
		isRemoveDisabled: function(view, rowIndex, colIndex, item, record) {
			return view.grid.readOnly;
		},
		
		transformUserRecord: function(record) {
			if (record && record.isModel) {
				var utype = record.entityName.split('.');
				return {
					id	: record.getId(),
					name: record.get('fullName'),
					type: utype[utype.length - 1].toLowerCase()
				}
			}
		}
	
	}

});