Ext.define('CFJS.grid.FilesPanel', {
	extend				: 'Ext.grid.Panel',
	xtype				: 'grid-files-panel',
	requires			: [
		'Ext.button.Button',
		'Ext.form.Panel',
		'Ext.grid.filters.Filters',
		'CFJS.plugin.FileDropZone'
	],
	autoUpload			: true,
	columnLines			: true,
	config				: {
		actions		: {
			downloadFile: {
				handler	: 'onDownloadFile',
				iconCls	: CFJS.UI.iconCls.DOWNLOAD,
				tooltip	: 'Download selected file'
			},
			levelUp		: {
				iconCls	: CFJS.UI.iconCls.ELLIPSIS_V,
				handler	: 'onLevelUp',
				title	: 'Directory above',
				tooltip	: 'Directory above'
			},
			reloadStore	: {
				handler	: 'onReloadStore',
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data'
			},
			removeFile	: {
				handler	: 'onRemoveFile',
				iconCls	: CFJS.UI.iconCls.CANCEL,
				title	: 'Remove file',
				tooltip	: 'Remove selected file'
			}
		},
		actionsMap	: {
			contextMenu	: { items: [ 'levelUp', 'removeFile' ] },
			header		: { items: [ 'levelUp', 'reloadStore' ] }
		},
		author		: null,
		contextMenu	: { items: new Array(2) },
		fileUpload	: {
			buttonConfig: {
				iconCls: CFJS.UI.iconCls.UPLOAD,
				tooltip: 'Upload file'
			},
			index	: 1
		},
		folder		: null,
		headerConfig: CFJS.UI.buildHeader(),
		permissions	: { create: true, 'delete': true },
		service		: 'ics'
	},
	defaultListenerScope: true,
	enableColumnHide	: false,
	height				: 200,
	iconCls				: CFJS.UI.iconCls.FILES,
	loadMask			: true,
	plugins				: [{ ptype: 'gridfilters' },{ ptype: 'filedrop' }],
	scrollable			: 'y',
	title				: 'View files',
	viewConfig			: { emptyText: 'Choose a files or drag them here', deferEmptyText: false },
    waitMsg				: 'Uploading files...',

    applyService: function(service) {
		var me = this, api = CFJS.Api;
		if (Ext.isString(service)) service = api.controller(service);
		return service || api.controller(api.getService());
	},
	
	// template function for before load event
	beforeFileLoad	: Ext.emptyFn,

    confirmRemove: function(fileName, isFile, fn) {
		return Ext.fireEvent('confirm', 'Are you sure to delete a ' + (isFile ? 'file ' : 'folder ') + fileName + '?', fn, this);
	},

	renderColumns: Ext.emptyFn,
	
	createHeader: function() {
		var me = this, actions = me.getActions(),
			uploadForm = me.createUploadForm(),
			header = Ext.apply({ items: [] }, me.headerConfig), 
			map = me.actionsMap && me.actionsMap.header, i = 0;
		if (map && Ext.isArray(map.items)) {
			for (; i < map.items.length; i++) {
				header.items.push(actions[map.items[i]]);
			}
		}
		if (uploadForm) header.items.splice(me.fileUpload.index, 0, uploadForm);
		return header;
	},
	
	createStore: Ext.emptyFn,
	
	createUploadForm: function() {
		var me = this, fileUpload = me.fileUpload, permissions = me.getPermissions() || {};
		if (fileUpload && permissions.create) {
			me.uploadForm = Ext.create({
				xtype		: 'form',
				bodyStyle	: 'background:transparent !important;margin:0;',
				header		: false,
				layout		: 'fit',
				items		: Ext.apply({
					xtype		: 'filefield',
					cls			: 'file-field',
					allowBlank	: false,
					buttonOnly	: true,
					buttonText	: '',
					listeners	: { change: 'onFileChange', scope: me },
					name		: 'upload',
					width		: 32
				}, fileUpload)
			});
		}
		return me.uploadForm;
	},
	
	// template function for selection change event
	doSelectionChange: Ext.emptyFn,
	
	initComponent: function() {
		var me = this, actions = me.getActions(),
			listeners = {
				destroyable		: true,
				scope			: me,
				itemdblclick	: me.onItemDblClick,
				selectionchange	: me.onSelectionChange
			};
		// initialize paths and store
		me.store = me.createStore() || me.store || 'ext-empty-store';
		// initialize header
		if (me.header !== false) me.header = me.createHeader();
		// initialize columns
		me.columns = me.renderColumns();
		me.callParent();
		me.dropZone = me.findPlugin('filedrop');
		if (Ext.isDefined(window.FormData)) Ext.apply(listeners, {
			beforeload	: me.beforeFileLoad,
			dragover	: me.onFileDragOver,
			drop		: me.onFileDrop,
			load		: me.onFileLoad,
			loadabort	: me.onFileLoadAbort,
			loadend		: me.onFileLoadEnd,
			loaderror	: me.onFileLoadError,
			loadstart	: me.onFileLoadStart
		});
		me.on(listeners);
		me.updateFolder(me.getFolder());
	},

	isFolder: function(folder) {
		return !Ext.isEmpty(folder);
	},
	
	lookupRecord: function(record) {
		record = Ext.Array.from(record || this.getSelection());
		return record.length > 0 ? record[0] : null;
	},
	
	onDownloadFile: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this;
		record = me.lookupRecord(record);
		if (record && record.isEntity) {
			if (!component || !component.isTableView) component = me.view;
			me.onItemDblClick(component, record);
		}
	},

	onFileChange: function(field, value) {
		var me = this;
		if (me.autoUpload) me.submitForm(me.uploadForm);
	},
	
	// template function for drag over event
	onFileDragOver	: Ext.emptyFn,

	onFileDrop: function(me, e) {
		var dropZone = me.dropZone;
		if (me.autoUpload) {
			var formData = new FormData();
			dropZone.forEachFile(e, function(file) {
				formData.append('upload', file, file.name);
			}, me);
			me.uploadFormData(formData);
		}
	},
	
	onFileLoad: function(me, e, file) {
		file.data = e.target.result;
	},

	// template function for file load abort event
	onFileLoadAbort: Ext.emptyFn,
	// template function for file load end event
	onFileLoadEnd: Ext.emptyFn,
	// template function for file load error event
	onFileLoadError: Ext.emptyFn,
	// template function for file load start event
	onFileLoadStart: Ext.emptyFn,

	onItemDblClick: function(view, record) {
		if (record && record.isEntity) {
			this[record.get('isFile') ? 'download' : 'goFolder' ](record);
		}
	},

	onLevelUp: function(component) {
		var me = this, folder = me.getFolder();
		if (folder && folder.isResource) me.setFolder(folder.get('parentId'));
	},
	
	onReloadStore: function(button) {
		this.getStore().reload();
	},

	onRemoveFile: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this;
		record = me.lookupRecord(record);
		if (record && record.isEntity && record.canDelete(me.getAuthor())) {
			me.confirmRemove(record.get('name'), record.get('isFile'), function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					me.removeFile(record, function(record, operation, success) {
						me.disableComponent(component);
						me.getStore().reload();
					});
				}
			});
		}
	},

	onSelectionChange: function(selectable, selection) {
		var me = this, actions = me.getActions();
		if (actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.doSelectionChange(selectable, selection, actions);
		}
	},
		
	submitForm: function(form) {
		if (form && form.isValid()) {
			var me = this;
			form.submit({
				url			: me.service.url,
				extraParams	: me.uploadParams(),
				errorReader	: { type: 'json.cfapi', rootProperty: 'file' },
				waitMsg		: me.waitMsg,
				success		: function(form, action) { me.getStore().reload(); },
				failure		: function(form, action) { me.getStore().reload(); }
			});
		}
	},
	
	updateFolder: function(folder) {
		this.hideComponent(this.getActions().levelUp, !this.isFolder(folder));
	},
	
	uploadFormData: function(formData) {
		var me = this;
		me.setLoading(me.waitMsg);
		CFJS.Api.request({
			url		: me.service.url,
			headers	: { 'Content-Type': undefined },
			method	: 'POST',
			params	: me.uploadParams(),
			rawData	: formData,
			callback: function(options, success, response) {
				me.setLoading(false);
				me.getStore().reload();
			}
		});
	},
	
	privates: {
		
		download: function(record) {
			if (record && Ext.isFunction(record.download)) record.download();
		},
		
		goFolder: function(record) {
			this.setFolder(record);
		},

		removeFile: function(record, callback) {
			record.erase({ callback: callback });
		},
		
		uploadParams: function() {
			return { method: [this.service.method, 'upload'].join('.') };
		}

	}

});