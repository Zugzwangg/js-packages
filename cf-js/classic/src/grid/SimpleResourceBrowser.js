Ext.define('CFJS.grid.SimpleResourceBrowser', {
	extend			: 'Ext.grid.Panel',
	xtype			: 'grid-simple-resource-browser',
	requires		: [
		'Ext.button.Button',
		'Ext.form.Panel',
		'Ext.grid.filters.Filters',
		'CFJS.plugin.FileDropZone'
	],
	autoUpload			: true,
	columnLines			: true,
	config				: {
		actions		: {
			downloadFile: {
				handler	: 'onDownloadFile',
				iconCls	: CFJS.UI.iconCls.DOWNLOAD,
				tooltip	: 'Download selected file'
			},
			reloadStore	: {
				handler	: 'onReloadStore',
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data'
			},
			removeFile	: {
				handler	: 'onRemoveFile',
				iconCls	: CFJS.UI.iconCls.CANCEL,
				text	: 'Remove file',
				tooltip	: 'Remove selected file'
			}
		},
		actionsMap	: {
			contextMenu	: { items: [ 'removeFile' ] },
			header		: { items: [ 'reloadStore' ] }
		},
		contextMenu	: { items: new Array(1) },
		document	: null,
		fileUpload	: {
			buttonConfig: {
				iconCls: CFJS.UI.iconCls.UPLOAD,
				tooltip: 'Upload file'
			},
			index	: 0
		},
		headerConfig: CFJS.UI.buildHeader(),
		parentField	: 'parentId',
		service		: null
	},
	defaultListenerScope: true,
	disableSelection	: true,
	enableColumnHide	: false,
	enableColumnMove	: false,
	loadMask			: true,
	plugins				: [{ ptype: 'gridfilters' },{ ptype: 'filedrop' }],
	readOnly			: false,
	scrollable			: 'y',
	viewConfig			: { emptyText: 'Choose a files or drag them here', deferEmptyText: false },
    waitMsg				: 'Uploading files...',
	
	applyService: function(service) {
		var me = this, api = CFJS.Api;
		if (Ext.isString(service)) service = api.controller(service);
		return service || api.controller(api.getService());
	},

	// template function for before load event
	beforeFileLoad	: Ext.emptyFn,
	
    confirmRemove: function(fileName, fn) {
		return Ext.fireEvent('confirm', 'Are you sure to delete a file ' + fileName + '?', fn, this);
	},

	renderColumns: function() {
		var me = this, actions = me.getActions(), vm = me.lookupViewModel(),
			author = CFJS.lookupViewportViewModel().userInfoAsAuthor(),
			removeDisabled = function(view, rowIdx, colIdx, item, record) {
				return !vm.canDeleteResource(record, author);
			};
		actions.removeFile.initialConfig.isDisabled = removeDisabled;
		actions.removeFile.each(function(item) {
			item.isDisabled = removeDisabled; 
		});
		return CFJS.apply([{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'file name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'File name',
			tpl			: '<span class="{iconCls}" style="margin-right: 5px;"></span>{name}',
			xtype		: 'templatecolumn'
		},{
			align		: 'left',
			dataIndex	: 'author',
			filter		: { type: 'string', emptyText: 'author name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Author',
			tpl			: '<tpl if="author">{author.name}</tpl>',
			xtype		: 'templatecolumn'
		},{
			align		: 'right',
			dataIndex	: 'size',
			filter		: { type: 'number', emptyText: 'size in bytes' },
			renderer	: Ext.util.Format.fileSize,
			style		: { textAlign: 'center' },
			text		: 'Size',
			width		: 150
		},{
			align		: 'center',
			dataIndex	: 'lastModified',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Last modified',
			width		: 170,
			xtype		: 'datecolumn'
		},{
			align		: 'center',
			items		: [ actions.downloadFile, actions.removeFile ],
			menuDisabled: true,
			sortable	: false,
			width		: 65,
			xtype		: 'actioncolumn'
		}], me.columnsText);
	},
	
	createHeader: function() {
		var me = this, actions = me.getActions(),
			uploadForm = me.createUploadForm(),
			header = Ext.apply({ items: [] }, me.headerConfig), 
			map = me.actionsMap && me.actionsMap.header, i = 0;
		if (map && Ext.isArray(map.items)) {
			for (; i < map.items.length; i++) {
				header.items.push(actions[map.items[i]]);
			}
		}
		if (uploadForm) header.items.splice(me.fileUpload.index, 0, uploadForm);
		return header;
	},

	createUploadForm: function() {
		var me = this, fileUpload = me.fileUpload;
		if (fileUpload) {
			me.uploadForm = Ext.create({
				xtype		: 'form',
				bodyStyle	: 'background:transparent !important;margin:0;',
				header		: false,
				layout		: 'fit',
				items		: Ext.apply({
					xtype		: 'filefield',
					cls			: 'file-field',
					allowBlank	: false,
					buttonOnly	: true,
					buttonText	: '',
					listeners	: { change: 'onFileChange', scope: me },
					name		: 'upload',
					width		: 32
				}, fileUpload)
			});
		}
		return me.uploadForm;
	},

	initComponent: function() {
		var me = this, actions = me.getActions(),
			listeners = {
				destroyable		: true,
				scope			: me,
				itemdblclick	: me.onItemDblClick
			};
		// initialize paths and store
		me.store = me.store || 'ext-empty-store';
		// initialize header
		if (me.header !== false) me.header = me.createHeader();
		// initialize columns
		me.columns = me.renderColumns();
		me.callParent();
		me.dropZone = me.findPlugin('filedrop');
		if (Ext.isDefined(window.FormData)) Ext.apply(listeners, {
			beforeload	: me.beforeFileLoad,
			dragover	: me.onFileDragOver,
			drop		: me.onFileDrop,
			load		: me.onFileLoad,
			loadabort	: me.onFileLoadAbort,
			loadend		: me.onFileLoadEnd,
			loaderror	: me.onFileLoadError,
			loadstart	: me.onFileLoadStart
		});
		me.on(listeners);
	},

	lookupRecord: function(record) {
		record = Ext.Array.from(record || this.getSelection());
		return record.length > 0 ? record[0] : null;
	},
	
	onDownloadFile: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this;
		record = me.lookupRecord(record);
		if (record && record.isEntity) {
			if (!component || !component.isTableView) component = me.view;
			me.onItemDblClick(component, record);
		}
	},

	onFileChange: function(field, value) {
		var me = this;
		if (!me.readOnly && me.autoUpload) me.submitForm(me.uploadForm);
	},
	
	// template function for drag over event
	onFileDragOver	: Ext.emptyFn,

	onFileDrop: function(me, e) {
		var dropZone = me.dropZone;
		if (!me.readOnly && me.autoUpload) {
			var formData = new FormData();
			dropZone.forEachFile(e, function(file) {
				formData.append('upload', file, file.name);
			}, me);
			me.uploadFormData(formData);
		}
	},
	
	onFileLoad: function(me, e, file) {
		file.data = e.target.result;
	},

	// template function for file load abort event
	onFileLoadAbort: Ext.emptyFn,
	// template function for file load end event
	onFileLoadEnd: Ext.emptyFn,
	// template function for file load error event
	onFileLoadError: Ext.emptyFn,
	// template function for file load start event
	onFileLoadStart: Ext.emptyFn,

	onItemDblClick: function(view, record) {
		if (record && Ext.isFunction(record.download)) record.download();
	},

	onReloadStore: function(button) {
		this.getStore().reload();
	},

	onRemoveFile: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this, vm = me.lookupViewModel();
		record = me.lookupRecord(record);
		if (!me.readOnly && vm && vm.canDeleteResource(record)) {
			me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					record.erase({
						callback: function(record, operation, success) {
							me.disableComponent(component);
							me.getStore().reload();
						}
					});
				}
			});
		}
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.readOnly = readOnly;
		me.hideComponent(me.uploadForm, readOnly);
		me.hideAction('removeFile', readOnly);
	},
	
	submitForm: function(form) {
		var me = this;
		if (!me.readOnly && form && form.isValid()) {
			form.submit({
				url			: me.service.url,
				extraParams	: me.uploadParams(),
				errorReader	: { type: 'json.cfapi', rootProperty: 'errors' },
				waitMsg		: me.waitMsg,
				success		: function(form, action) { me.getStore().reload(); },
				failure		: function(form, action) { me.getStore().reload(); }
			});
		}
	},
	
	uploadFormData: function(formData) {
		var me = this;
		if (!me.readOnly) {
			me.setLoading(me.waitMsg);
			CFJS.Api.request({
				url		: me.service.url,
				headers	: { 'Content-Type': undefined },
				method	: 'POST',
				params	: me.uploadParams(),
				rawData	: formData,
				callback: function(options, success, response) {
					me.setLoading(false);
					me.getStore().reload();
				}
			});
		}
	},

	privates: {
		
		uploadParams: function() {
			var me = this, item = me.lookupViewModel().getItem(), field = me.parentField,
				params = { method: [me.service.method, 'resource.upload'].join('.') }; 
			if (item && item.id && item.id > 0 && field) params[field] = item.getId();
			return params;
		}

	}

});