Ext.define('CFJS.grid.BasePanel', {
	extend					: 'Ext.grid.Panel',
	xtype					: 'grid-base-panel',
	requires				: [
		'Ext.button.Button',
		'Ext.grid.column.RowNumberer',
		'Ext.grid.filters.Filters',
		'CFJS.util.UI'
	],
	mixins					: [ 'CFJS.mixin.PeriodPicker', 'CFJS.mixin.Permissible' ],
	config					: {
		actions		: {
			addRecord	: {
				handler	: 'onAddRecord',
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Add a record',
				tooltip	: 'Add new record'
			},
			clearFilters: {
				handler	: 'onClearFilters',
				iconCls	: CFJS.UI.iconCls.CLEAR,
				title	: 'Clear filters',
				tooltip	: 'Clear all filters'
			},
			editRecord	: {
				handler	: 'onEditRecord',
				iconCls	: CFJS.UI.iconCls.EDIT,
				title	: 'Edit the record',
				tooltip	: 'Edit the current record'
			},
			periodPicker: {
				handler	: 'onPeriodPicker',
				iconCls	: CFJS.UI.iconCls.CALENDAR,
				title	: 'Working period',
				tooltip	: 'Set the working period'
			},
			reloadStore	: {
				handler	: 'onReloadStore',
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data'
			},
			removeRecord: {
				handler	: 'onRemoveRecord',
				iconCls	: CFJS.UI.iconCls.DELETE,
				title	: 'Delete records',
				tooltip	: 'Remove selected records'
			}
		},
		actionsMap		: {
			contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
			header		: { items: [ 'periodPicker', 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
		},
		actionsDisable	: [ 'removeRecord', 'editRecord' ],
		contextMenu		: { items: new Array(3) },
		headerConfig	: CFJS.UI.buildHeader(),
		permissions		: null
	},
	columnLines				: true,
    emptyText				: 'No data to display',
	header					: { items: new Array(6) },
	height					: 250,
	loadMask				: true,
	listeners				: { selectionchange: 'onSelectionChange' },
	plugins					: [{ ptype: 'gridfilters' }],
	readOnly				: false,
	rowNumberer				: true,
	rowNumbererHeaderWidth	: 46,
	scrollable				: 'y',
	selModel				: { mode: 'MULTI', pruneRemoved: false },

	bindStore: function(store, initial) {
		var me = this;
		if (!initial && !me.destroyed) {
			var paging = me.down('pagingtoolbar');
			if (paging) paging.bindStore(store);
		}
		me.callParent(arguments);
	},
	
	getRowClass: Ext.emptyFn,

	initComponent: function() {
		var me = this, columns = me.columns;
		if (me.header) Ext.applyIf(me.header, me.headerConfig);
		me.viewConfig = Ext.applyIf(me.viewConfig || {}, {
			deferEmptyText			: false, 
			getRowClass				: me.getRowClass,
			preserveScrollOnReload	: true
		});
		if (me.rowNumberer && columns && !columns.isRootHeader) {
			me.columns = columns = Ext.Array.from(columns.items || columns);
			if (columns.length > 0 && columns[0].xtype !== 'rownumberer') {
				columns.unshift({ xtype: 'rownumberer', width: me.rowNumbererHeaderWidth });
			}
		}
		me.callParent();
	},
	
	onClearFilters: function(component) {
		var me = this, store = me.store, vm = me.lookupViewModel(),
			gridfilters = me.findPlugin('gridfilters');
		if (gridfilters) gridfilters.clearFilters();
		else {
			me.filters.clearFilters();
			if (vm && vm.isBaseModel) vm.setStoreFilters(store);
		}
	},
	
	onPermissionsChange: function(permissions) {
		permissions = permissions || {};
		var me = this, actions = me.actions || {},
			hide = me.permissions.hideUnmapped,
			editor = permissions.editor || { create: !hide, edit: !hide, 'delete': !hide, view: !hide },	
			map = me.permissions.columns || {},
			columns = me.getColumns(),
			i = 0, column, key;
		me.readOnly = me.readOnly || !editor.edit;
		me.hideComponent(actions.addRecord, !editor.create || !editor.edit);
		me.hideComponent(actions.editRecord, !editor.edit);
		me.hideComponent(actions.removeRecord, !editor['delete']);
		for (; i < columns.length; i++) {
			column = columns[i] = columns[i].initialConfig;
			key = map[column.dataIndex] || column.dataIndex;
			if (!Ext.isEmpty(key) && column.xtype !== 'rownumberer') {
				column.hideable = permissions[key] ? permissions[key].view : !hide;
				column.menuDisable = !column.hideable;
				column.hidden = !column.hideable;
			} else columns.splice(i--, 1);
		}
		me.reconfigure(columns);
	}, 
	
	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.hideComponent(actions.addRecord, readOnly);
		me.hideComponent(actions.editRecord, readOnly);
		me.hideComponent(actions.removeRecord, readOnly);
		me.readOnly = readOnly;
	}
	
});
