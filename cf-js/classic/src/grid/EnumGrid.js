Ext.define('CFJS.grid.EnumGrid', {
	extend			: 'Ext.grid.Panel',
	alias			: 'widget.grid-enum-panel',
	requires		: [ 'Ext.grid.plugin.CellEditing' ],
	columnLines		: true,
	enableColumnHide: false,
	enableColumnMove: false,
	gridCls			: Ext.baseCSSPrefix + 'property-grid',
	readOnly		: false,
	stripeRows		: false,
	trackMouseOver	: false,
	clicksToEdit	: 1,
	
	renderColumns: function() {
		var me = this;
		return [{
			align		: 'left',
			dataIndex	: me.nameField,
			flex		: 1,
			innerCls	: Ext.baseCSSPrefix + 'grid-cell-inner-property-name',
			itemId		: me.nameItemId || me.nameField,
			menuDisabled: true,
			renderer	: me.nameRenderer,
			sortable	: me.sortableColumns,
			style		: { textAlign: 'center' },
			text		: me.nameText,
			tdCls		: Ext.baseCSSPrefix + 'grid-property-name'
		},{
			align		: 'left',
			dataIndex	: me.valueField,
			editor		: me.valueEditor,
			flex		: 1,
			itemId		: me.valueItemId || me.valueField,
			menuDisabled: true,
			renderer	: me.valueRenderer,
			sortable	: me.sortableColumns,
			style		: { textAlign: 'center' },
			text		: me.valueText,
		}];
	},
	
	initComponent: function() {
		var me = this;
		if (me.header !== false) me.header = Ext.apply(me.header || {}, me.headerConfig);
		me.addCls(me.gridCls);
		me.plugins = me.plugins || [];
		me.plugins.push(new Ext.grid.plugin.CellEditing({
			clicksToEdit: me.clicksToEdit,
			shouldStartEdit: function(editor) {
		        return !me.readOnly && !!editor;
		    },
            startEdit: function(record, column) {
            	return this.self.prototype.startEdit.call(this, record, valueColumn);
            }
		}));
		me.selModel = {
			type: 'cellmodel',
			onCellSelect: function(position) {
				position.column = me.valueColumn;
				position.colIdx = me.valueColumn.getVisibleIndex();
				return this.self.prototype.onCellSelect.call(this, position);
			}
		};
		me.columns = me.renderColumns();
		me.callParent();
		var view = me.getView(), columns = me.getColumns();
		me.nameColumn = columns[0];
		me.valueColumn = columns[1];
		if (me.sortableColumns) {
			if (me.nameSorter) me.nameColumn.setSorter(me.nameSorter);
			if (me.valueSorter) me.valueColumn.setSorter(me.valueSorter);
		}
		view.walkCells = me.walkCells;
		view.getDefaultFocusPosition = me.getDefaultFocusPosition;
	},

	walkCells: function(pos, direction, e, preventWrap, verifierFn, scope) {
		var me = this, valueColumn = me.ownerCt.valueColumn;
		if (direction === 'left') direction = 'up';
		if (direction === 'right') direction = 'down';
		pos = Ext.view.Table.prototype.walkCells.call(me, pos, direction, e, preventWrap, verifierFn, scope);
		pos.column = valueColumn;
		pos.colIdx = valueColumn.getVisibleIndex();
		return pos;
	},

	getDefaultFocusPosition: function() {
		var view = this, focusPosition;
		focusPosition = new Ext.grid.CellContext(view).setColumn(1);
		return focusPosition;
	},
	
	setReadOnly: function(readOnly) {
		this.readOnly = readOnly;
	}
	
});