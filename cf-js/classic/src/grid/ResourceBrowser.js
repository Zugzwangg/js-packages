Ext.define('CFJS.grid.ResourceBrowser', {
	extend			: 'CFJS.grid.FilesPanel',
	xtype			: 'grid-resource-browser',
	requires		: [ 'CFJS.store.webdav.Resources' ],
	config			: {
		actions		: {
			downloadFile: {
				isDisabled	: function(view, rowIdx, colIdx, item, record) { return !record.get('isFile') || !record.canDelete(); },
				tooltip		: 'Download selected resource'
			},
			makeFolder	: {
				handler	: 'onMakeFolder',
				iconCls	: CFJS.UI.iconCls.FOLDER,
				title	: 'Make folder',
				tooltip	: 'Make new folder'
			},
			removeFile	: {
				title		: 'Remove resource',
				tooltip		: 'Remove selected resource',
				isDisabled	: function(view, rowIdx, colIdx, item, record) { return !record.canDelete(); },
			}
		},
		actionsMap	: {
			contextMenu	: { items: [ 'levelUp', 'makeFolder', 'removeFile' ] },
			header		: { items: [ 'levelUp', 'makeFolder', 'reloadStore' ] }
		},
		contextMenu	: { items: new Array(3) },
		fileUpload	: { index: 2 },
		folder		: 0,
		service		: 'webdav'
	},
	multiColumnSort	: true,
	permissions		: { create: true, 'delete': true },
	plugins			: [{ ptype: 'filedrop' }],
	selModel		: { type: 'rowmodel', allowDeselect: true, mode: 'SINGLE', pruneRemoved: false },

	applyFolder: function(folder) {
		if (Ext.isNumber(folder)) {
			if (folder > 0) {
				folder = new CFJS.model.webdav.Resource({ id: folder });
				folder.load();
			} else folder = null
		}
		return folder;
	},
	
	renderColumns: function() {
		var me = this, actions = me.getActions();
		return CFJS.apply([{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'resource name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name',
			tpl			: '<span class="{iconCls}" style="margin-right: 5px;"></span>{name}',
			xtype		: 'templatecolumn'
		},{
			align		: 'right',
			dataIndex	: 'size',
			renderer	: Ext.util.Format.fileSize,
			style		: { textAlign: 'center' },
			text		: 'Size',
			width		: 150
		},{
			align		: 'center',
			dataIndex	: 'created',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Created',
			width		: 170,
			xtype		: 'datecolumn'
		},{
			align		: 'center',
			dataIndex	: 'lastModified',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Last modified',
			width		: 170,
			xtype		: 'datecolumn'
		},{
			align		: 'center',
			items		: [ actions.downloadFile, actions.removeFile ],
			menuDisabled: true,
			sortable	: false,
			width		: 65,
			xtype		: 'actioncolumn'
		}], me.columnsText);
	},
	
	createStore: function() {
		var me = this, vm = me.lookupViewModel(),
			folder = me.getFolder(),
			isFolder = folder && folder.isResource && folder.getId() > 0,
			store = new CFJS.store.webdav.Resources({
				autoDestroy	: true,
				filters		: [{
					id		: 'parent',
					property: 'parent.id',
					operator: 'eq',
					type	: 'numeric',
					value	: isFolder ? folder.getId() : null
				}],
				sorters		: [ 'file', 'name' ]
			});
		vm.bind('{userInfo}', function(userInfo) {
			me.setAuthor(CFJS.lookupViewportViewModel().userInfoAsAuthor(userInfo));
			me.addPlugin('gridfilters');
		});
		return store;
	},
	
	isFolder: function(folder) {
		return folder && folder.isResource && folder.getId() && folder.getId() > 0;
	},
	
	onMakeFolder: function(component) {
		var me = this, folder = me.getFolder();
		new CFJS.model.webdav.Resource({
			author	: me.getAuthor(),
			parentId: folder && folder.isResource ? folder.getId() : null,
			isFile	: false
		}).save({
			callback: function(record, operation, success) {
				me.getStore().reload();
			}
		});
	},
	
	updateFolder: function(folder) {
		var me = this, store = me.getStore();
		if (store && store.isStore) store.addFilter({
			id		: 'parent',
			property: 'parent.id',
			operator: 'eq',
			type	: 'numeric',
			value	: me.isFolder(folder) ? folder.getId() : null
		});
		me.callParent(arguments)
	},
	
	updateAuthor: function(author) {
		var me = this, store = me.getStore();
		if (store && store.isStore) {
			author = author || {};
			store.addFilter([{
				id		: 'authorId',
				property: 'author.id',
				operator: 'eq',
				type	: 'numeric',
				value	: author.id
			},{
				id		: 'authorClass',
				property: 'author.class',
				operator: 'eq',
				type	: 'string',
				value	: author.type && author.type.charAt && author.type.charAt(0)
			}]);
		}
	},

	privates: {
		
		uploadParams: function() {
			var me = this, params = me.callParent(), folder = me.getFolder();
			if (folder && folder.isResource) params.folder = folder.getId();
			return params;
		}
	
	}
	
});