Ext.define('CFJS.grid.InternalSharesPanel', {
	extend				: 'Ext.grid.Panel',
	xtype				: 'grid-internal-shares-panel',
	requires			: [ 'CFJS.store.webdav.InternalShares' ],
	bodyStyle			: Ext.theme.name === 'Crisp' ? { borderTopWidth: '1px !important;' } : null,
	defaultListenerScope: true,
	disableSelection	: true,
	enableColumnHide	: false,
	enableColumnMove	: false,
	header				: CFJS.UI.buildHeader({
		items			: [{
			arrowVisible: false,
			iconCls		: CFJS.UI.iconCls.USER_ADD,
			tooltip		: 'Add access to the resource',
			menu		: [{
				handler	: 'onAddShare',
				iconCls	: CFJS.UI.iconCls.USER_MALE,
				title	: 'Person',
				userType: 'person'
			},{
				handler	: 'onAddShare',
				iconCls	: CFJS.UI.iconCls.USER,
				title	: 'Client',
				userType: 'customer'
			}]
		}]
	}),
	hideHeaders			: true,
	loadMask			: true,
	scrollable			: 'y',
	title				: 'Internal access',
	columns				: [{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'user',
		flex		: 1,
		menuDisabled: true,
		sortable	: false,
		style		: { textAlign: 'center' },
		text		: 'User',
		tpl			: '<tpl if="user">{user.name}</tpl>'
	},{
		align		: 'center',
		items		: [{
			iconCls	: CFJS.UI.iconCls.CANCEL,
			tooltip	: 'Remove access for selected user',
			handler	: 'onRemoveShare'
		}],
		menuDisabled: true,
		sortable	: false,
		width		: 30,
		xtype		: 'actioncolumn'
	}],

	beforeSelectUser: function(selector, record) {
		return record ? true : false;
	},

	confirmRemove: function(userName, fn) {
		return Ext.fireEvent('confirm', 'Are you sure you want to deny access for ' + userName + '?', fn, this);
	},
	
	createUserPicker: function(type, listeners) {
		var me = this,
			picker = Ext.apply({
				xtype			: 'admin-window-' + type + '-picker',
				closeAction		: 'hide',
				maximizable		: false,
				minWidth		: 540,
				minHeight		: 200,
				readOnly		: type === 'person',
				selectorType	: 'grid'
			}, me[type + 'PickerConfig']);
		picker.selectorConfig = Ext.apply(picker.selectorConfig || {}, me[type + 'SelectorConfig']);
		picker = Ext.create(picker);
		picker.on({ select: function() { picker.close() } });
		if (listeners) picker.on(listeners);
		return picker;
	},
	
	getUserPicker: function(type) {
		type = (type || 'person');
		var me = this, pName = type + 'Picker';
        if (!me[pName]) {
        	me.creatingPicker = true;
        	me[pName] = me.createUserPicker(type, { beforeselect: me.beforeSelectUser, select: me.onSelectUser, scope: me });
        	me[pName].ownerCmp = me;
        	delete me.creatingPicker;
        }
        return me[pName];
	},

	onAddShare: function(component) {
		if (component && component.userType) {
			var me = this, picker;
			if (me.rendered && !me.isExpanded && !me.destroyed) {
				picker = me.getUserPicker(component.userType);
				picker.setMaxHeight(picker.initialConfig.maxHeight);
				picker.show();
			}
		}
	},
	
	onRemoveShare: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this, user;
		if (record && record.isEntity) {
			me.confirmRemove((record.get('user')||{}).name, function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					record.erase({
						callback: function(record, operation, success) {
							me.disableComponent(component);
							me.getStore().reload();
						}
					});
				}
			});
		}
	},
	
	onSelectUser: function(selector, record) {
		var me = this, store = me.getStore(),
			resource = me.lookupViewModel().get('resource'),
			utype = record.entityName.split('.');
		if (resource && resource.isResource) {
			resource.internalShare({
				id	: record.getId(),
				name: record.get('fullName'),
				type: utype[utype.length - 1].toLowerCase()
			}, null, function(data) { store.add(data); });
		}
	}

});