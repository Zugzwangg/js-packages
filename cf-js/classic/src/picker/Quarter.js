Ext.define('CFJS.picker.Quarter', {
	extend				: 'Ext.picker.Month',
	alias				: 'widget.quarterpicker',
	alternateClassName	: 'CFJS.QuarterPicker',
	isQuarterPicker		: true,
	monthOffset			: 4, // 4 quarters 
	quarterText			: 'Quarter',
	
	beforeRender: function() {
		var me = this, i = 1, quarters = [];
		me.callParent();
		for (; i <= me.monthOffset; ++i) {
			quarters.push(me.quarterText + ' ' + Ext.Number.toRoman(i));
		}
		me.renderData.months = quarters;
	},
	
	afterRender: function() {
		var me = this;
		me.monthEl.addCls(me.baseCls + '-quarters')
		me.callParent();
	},
	
	calculateMonthMargin: Ext.emptyFn,
	
	setValue: function(value){
		var me = this,
			active = me.activeYear,
			year;
		if (!value) me.value = [null, null];
		else if (Ext.isDate(value)) me.value = [Math.floor(value.getMonth() / 3), value.getFullYear()];
		else me.value = [value[0], value[1]];
		if (me.rendered) {
			year = me.value[1];
			if (year !== null) {
				if ((year < active || year > active + me.yearOffset)) {
					me.activeYear = year - me.yearOffset + 1;
				}
			}
			me.updateBody();
		}
		return me;
	},

	updateBody: function(){
		var me = this,
			years = me.years,
			months = me.months,
			yearNumbers = me.getYears(),
			cls = me.selectedCls,
			value = me.getYear(null),
			month = me.value[0],
			monthOffset = me.monthOffset,
			year, yearItems, y, yLen, el;
		if (me.rendered) {
			years.removeCls(cls);
			months.removeCls(cls);
			yearItems = years.elements;
			yLen = yearItems.length;
			for (y = 0; y < yLen; y++) {
				el = Ext.fly(yearItems[y]);
				year = yearNumbers[y];
				el.dom.innerHTML = year;
				if (year === value) el.addCls(cls);
			}
			if (month !== null) {
				if (monthOffset <= month) month = monthOffset - 1;
				months.item(month).addCls(cls);
			}
		}
	},
 
	onMonthClick: function(target, isDouble){
		var me = this;
		me.value[0] = me.months.indexOf(target);
		me.updateBody();
		me.fireEvent('month' + (isDouble ? 'dbl' : '') + 'click', me, me.value);
		me.fireEvent('select', me, me.value);
	}
	
});