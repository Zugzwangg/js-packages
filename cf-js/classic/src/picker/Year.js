Ext.define('CFJS.picker.Year', {
	extend				: 'Ext.picker.Month',
	alias				: 'widget.yearpicker',
	alternateClassName	: 'CFJS.YearPicker',
	isYearPicker		: true,
	totalYears			: 20, // 20 years
	yearColumns			: 4, 
	monthOffset			: 0,
	
	beforeRender: function() {
		var me = this, i = 1, quarters = [];
		me.callParent();
		for (; i <= me.monthOffset; ++i) {
			quarters.push('Quarter ' + i);
		}
		me.renderData.months = quarters;
	},
	
	afterRender: function() {
		var me = this;
		me.bodyEl.addCls(me.baseCls + '-yearpicker')
		me.callParent();
	},

	calculateMonthMargin: Ext.emptyFn,
	
	hasSelection: function(){
		return this.getValue() !== null;
	},
	
	getValue: function() {
		return this.value ? this.value[1] : null;
	},
    
	setValue: function(value) {
		var me = this;
		if (Ext.isDate(value)) value = [null, value.getFullYear()];
		else if (Ext.isNumber(value)) value = [null, value];
		else if (value && value.length > 1) value = [null, value[1]];
		return me.callParent([value]);
	},
	
	getYears: function() {
		var me = this,
			offset = me.yearOffset,
			start = me.activeYear, // put the "active" year on the left 
			end = start + offset,
			i = start,
			years = [], col;
		for (; i < end; ++i) {
			for (col = 0; col < me.yearColumns; col++) {
				years.push(i + col * offset);
			}
		}
		return years;
    },
    
    resolveOffset: function(index, offset) {
    	var yearColumns = this.yearColumns;
    	return (index % yearColumns) * offset +  Math.floor(index / yearColumns);
    }
    
});