Ext.define('CFJS.picker.Dates', {
	extend				: 'Ext.container.Container',
	requires			: [
		'Ext.Date',
		'Ext.picker.Date'
	],
	alias				: 'widget.datespicker',
	alternateClassName	: 'CFJS.DatesPicker',
	layout 				: 'column',
	isDatesPicker		: true,
	defaultListenerScope: true,
	startDate			: null,
	endDate				: null,

	correctEndDate: function(date) {
		var eDate = Ext.Date, endDate = eDate.clearTime(date, true);
		endDate = eDate.add(endDate, eDate.DAY, 1);
		return eDate.add(endDate, eDate.MILLI, -1);
	},
	
	setValue: function(value){
		var me = this, eDate = Ext.Date;
		if (Ext.isDate(value)) {
		    me.startDate = eDate.clearTime(value, true);
			me.endDate = me.correctEndDate(value);
		} else if (Ext.isArray(value)) {
			me.startDate = eDate.clearTime(new Date(value[0]));
			me.endDate = me.correctEndDate(value[1]);
		}
		me.pickStart.setValue(me.startDate);
		me.pickEnd.setValue(me.endDate);
		// set min/max date
		me.pickStart.setMaxDate(me.endDate);
		me.pickEnd.setMinDate(me.startDate);
	},
	
	getValue: function(){
		var me = this;
		me.pickStart.setMaxDate(me.endDate);
		me.pickEnd.setMinDate(me.startDate);
		return [me.startDate = Ext.Date.clearTime(me.startDate), me.endDate = me.correctEndDate(me.endDate)];
	},
	
	onPeriodChange: function(picker, date) {
		var me = this;
		if (picker && picker.dateType) {
			me[picker.dateType + 'Date'] = date;
		}
		me.fireEvent('select', me, me.getValue());
	},
	
	initComponent: function() {
		var me = this;
		if (!me.pickStart || !me.pickStart.isComponent) {
			me.pickStart = Ext.create(Ext.apply({
				xtype		: 'datepicker',
				columnWidth	: 0.5,
				dateType	: 'start',
				fieldLabel	: 'Date start'
			}, me.startConfig));
			me.pickStart.on({
				select	: me.onPeriodChange,
				scope	: me
			});
			me.relayEvents(me.pickStart, [ 'datedblclick' ]); 
		}
		if (!me.pickEnd || !me.pickEnd.isComponent) {
			me.pickEnd = Ext.create(Ext.apply({
				xtype		: 'datepicker',
				columnWidth	: 0.5,
				dateType	: 'end',
				fieldLabel	: 'Date end'
			}, me.endConfig));
			me.pickEnd.on({
				select	: me.onPeriodChange,
				scope	: me
			});
			me.relayEvents(me.pickEnd, [ 'datedblclick' ]); 
		}
		me.items = [ me.pickStart, me.pickEnd ];
		me.callParent(arguments);
	}

});