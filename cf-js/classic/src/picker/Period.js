Ext.define('CFJS.picker.Period', {
	extend				: 'Ext.window.Window',
	requires			: [
		'Ext.Date',
		'Ext.button.Button',
		'Ext.button.Split',
		'Ext.layout.container.Card',
		'Ext.picker.Date',
		'Ext.picker.Month',
		'CFJS.picker.Quarter',
		'CFJS.picker.Year',
		'CFJS.picker.Dates'
	],
	alias				: 'widget.periodpicker',
	alternateClassName	: 'CFJS.PeriodPicker',
	isPeriodPicker		: true,
	statics				: {
		DAY		: 0,
		WEEK	: 1,
		MONTH	: 2,
		QUARTER	: 3,
		YEAR	: 4,
		DATES	: 5
	},
	bodyPadding			: '0 5',
	config				: { value: new Date(), startDate: new Date(), endDate: new Date(), pickerType : 0 },
	datesText			: 'Arbitrary',
	dayText				: 'Day',
	weekText			: 'Week',
	monthText			: 'Month',
	quarterText			: 'Quarter',
	yearText			: 'Year',
	okButtonText		: 'OK',
	cancelButtonText	: 'Cancel',
	iconCls				: CFJS.UI.iconCls.CALENDAR,
	plain				: true,
	selectors			: [ 'DATES', 'DAY', 'MONTH', 'QUARTER', 'YEAR' ],
	showButtons			: true,
	title				: 'Working period',
	
	afterRender: function() {
		var me = this, width = 0;
		me.callParent();
		me.items.each(function(item, index, len) {
			width = Math.max(width, item.getWidth());	
		});
		me.setWidth(width);
	},
	
	initComponent: function() {
		var me = this,
			selectors = me.selectors,
			buttons = [],
			picker, obj, key, value, i;
		me.cls = Ext.baseCSSPrefix + 'periodpicker';
		me.layout = 'card';
		me.tbar = null;
		if (selectors) {
			if (Ext.isString(selectors)) selectors = [selectors];
			else if (selectors === true) selectors = [ 'DATES', 'DAY', 'WEEK', 'MONTH', 'QUARTER', 'YEAR' ];
			else if (Ext.isObject(selectors)) {
				obj = selectors; selectors = [];
				for (key in obj) {
					if (obj[key] && obj.hasOwnProperty(key)) selectors.push(String(key));
				}
			}
			for (i = 0; i < selectors.length; i++) {
				key = (selectors[i] || '').toUpperCase();
				value = me.self[key];
				if (Ext.isNumber(value)) {
					key = key.toLowerCase();
					switch (key) {
					case 'day':
						picker = { xtype: 'datepicker' };
						break;
					case 'week':
						break;
					default:
						picker = {
							xtype: key + 'picker', showButtons: false,
							listeners: {
								scope			: me,
								yeardblclick	: me.onOkClick,
								monthdblclick	: me.onOkClick,
								datedblclick	: me.onOkClick
							}
						};
						break;
					}
					buttons.push({ text: me[key + 'Text'], value: value, picker: picker });
				}
			}
			if (buttons.length > 0) {
				me.tbar = {
					defaultButtonUI	: 'soft-blue',
					layout			: { pack: 'center' },
					items			: {
						xtype	: 'segmentedbutton',
						items	: buttons, 
						value	: me.pickerType || buttons[0].value,
						listeners: {
							change: function(button, value) {
								me.setActiveView(me.id + '-picker-type-' + (me.pickerType = Ext.isArray(value) ? value[0] : value), me.startDate, me.endDate,(me.pickerType = Ext.isArray(value) ? value[0] : value));
							}
						}
					}
				};
				if (me.showButtons) {
					me.bbar = {
						layout	: { pack: 'center' },
						items	: [{
							scope	: me,
							handler	: me.onOkClick,
							text	: me.okButtonText
						},{
							scope	: me,
							handler	: me.onCancelClick,
							text	: me.cancelButtonText
						}]
					}
				}
			}
		}
		me.callParent();
		if (buttons.length > 0) {
			for (i = 0; i < buttons.length; i++) {
				picker = Ext.apply(buttons[i].picker, { id: me.id + '-picker-type-' + buttons[i].value });
				picker.listeners = Ext.apply(picker.listeners || {}, { scope: me, select: me.onSelectPeriod });
				me.add(picker);
			}
		}
	},
	
	getValue: function() {
		var me = this, self = me.self,
			eDate = Ext.Date,
			startDate = eDate.clearTime(me.startDate, true),
			endDate = eDate.clearTime(me.endDate, true),
			pickerType = me.pickerType || self.DAY;
		if (pickerType == self.DATES) { 
			if (!endDate) {
				endDate = startDate;
			}
			endDate = eDate.add(endDate, eDate.DAY, 1);
			endDate = Ext.Date.add(endDate, eDate.MILLI, -1);
		}
		switch (pickerType) {
			case self.DATES: 	break;
			case self.DAY: 		endDate = eDate.add(me.startDate, eDate.DAY,   1); endDate = Ext.Date.add(endDate, eDate.MILLI, -1);break;
			case self.WEEK: 	endDate = eDate.add(me.startDate, eDate.DAY,   7); endDate = Ext.Date.add(endDate, eDate.MILLI, -1);break;
			case self.MONTH: 	endDate = eDate.add(me.startDate, eDate.MONTH, 1); endDate = Ext.Date.add(endDate, eDate.MILLI, -1);break;
			case self.QUARTER: 	endDate = eDate.add(me.startDate, eDate.MONTH, 3); endDate = Ext.Date.add(endDate, eDate.MILLI, -1);break;
			case self.YEAR: 	endDate = eDate.add(me.startDate, eDate.YEAR,  1); endDate = Ext.Date.add(endDate, eDate.MILLI, -1);break;
			default: 			break;
		}
		me.startDate = startDate;
		me.endDate = endDate;
		return [startDate, endDate];
	},
	
	onSelectPeriod: function(picker, value) {
		var me = this, startDate = me.startDate, endDate = me.endDate, self = me.self; 
		if (picker.isDatePicker) {
			me.startDate = Ext.Date.clearTime(value || startDate);
			me.pickerType = self.DAY; 
			me.onOkClick();
			return;
		}
		if (picker.isDatesPicker) {
			me.startDate = Ext.Date.clearTime(value[0] || startDate);
			me.endDate = Ext.Date.clearTime(value[1] || endDate);
			me.pickerType = self.DATES;
			return;
		}
		if (picker.isMonthPicker) {
			var month = value[0] || 0, 
				year = value[1] || startDate.getFullYear();
			me.pickerType = self.MONTH; 
			if (picker.isQuarterPicker) {
				month = month * 3;
				me.pickerType = self.QUARTER; 
			} 
			if (picker.isYearPicker) {
				me.pickerType = self.YEAR;
			} 
			value = new Date(year, month);
		}
		me.startDate = Ext.Date.clearTime(value);
	},
	
	onOkClick: function() {
		var me = this,
			handler = me.handler, 
			value = me.getValue();
		if (!me.disabled) {
			me.fireEvent('select', me, value);
			if (handler) {
				handler.call(me.scope || me, me, value);
			}
			me.onSelect();
		}
	},
	
	onCancelClick: function() {
		var me = this;
		me.fireEvent('cancelclick', me);
		me.onSelect();
	},
	
	onSelect: function() {
		if (this.hideOnSelect) this.hide();
	},
    
	onShow: function() {
		var me = this;
		me.callParent();
		if (me.pickerField) {
			me.setValue(me.pickerField.getValue());
		}
	},
	
	setValue: function(value) {
		var me = this,
			self = me.self,
			eDate = Ext.Date,
			segmented = me.rendered ? me.down('segmentedbutton') : null;
		
		if (Ext.isDate(value)) {
		    me.startDate = eDate.clearTime(value, true);
		} else if (Ext.isArray(value)) {
			me.startDate = eDate.clearTime(new Date(value[0]));
		}
		//---------------
		if (segmented) {
			if (segmented.items.findIndex('value', me.pickerType) === -1) {
				me.pickerType = segmented.items.getAt(0).value;
			}
			segmented.setValue(me.pickerType);
		}
		return me;
	},
	
	privates: {
		setActiveView: function(id, startDate, endDate, pickerType) {
			var me = this, l = me.layout, self = me.self;
	        Ext.suspendLayouts();
			l.setActiveItem(id);
			if (pickerType == self.DATES) {
				l.activeItem.setValue([startDate, endDate]);
			} else {
				l.activeItem.setValue(startDate);
			}
	        Ext.resumeLayouts(true);
		}
	}

});