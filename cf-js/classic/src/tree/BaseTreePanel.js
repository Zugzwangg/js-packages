Ext.define('CFJS.tree.BaseTreePanel', {
	extend			: 'Ext.tree.Panel',
	xtype			: 'basetreepanel',
	requires		: [
		'Ext.button.Button',
		'Ext.grid.filters.Filters',
		'CFJS.util.UI'
	],
	mixins			: [ 'CFJS.mixin.PeriodPicker', 'CFJS.mixin.Permissible' ],
	config			: {
		actions		: {
			addRecord	: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Add a record',
				tooltip	: 'Add new record',
				handler	: 'onAddRecord'
			},
			clearFilters: {
				iconCls	: CFJS.UI.iconCls.CLEAR,
				title	: 'Clear filters',
				tooltip	: 'Clear all filters',
				handler	: 'onClearFilters'
			},
			editRecord	: {
				iconCls	: CFJS.UI.iconCls.EDIT,
				title	: 'Edit the record',
				tooltip	: 'Edit the current record',
				handler	: 'onEditRecord'
			},
			periodPicker: {
				iconCls	: CFJS.UI.iconCls.CALENDAR,
				title	: 'Working period',
				tooltip	: 'Set the working period',
				handler	: 'onPeriodPicker'
			},
			reloadStore	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data',
				handler	: 'onReloadStore'
			},
			removeRecord: {
				iconCls	: CFJS.UI.iconCls.DELETE,
				title	: 'Delete records',
				tooltip	: 'Remove selected records',
				handler	: 'onRemoveRecord'
			}
		},
		actionsMap		: {
			contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
			header		: { items: [ 'periodPicker', 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
		},
		actionsDisable	: [ 'removeRecord', 'editRecord' ],
		contextMenu		: { items: new Array(3) },
		headerConfig	: CFJS.UI.buildHeader(),
		permissions		: null
	},
	columnLines		: true,
    emptyText		: 'No data to display',
	header			: { items: new Array(6) },
	height			: 250,
	loadMask		: true,
	listeners		: { selectionchange: 'onSelectionChange' },
	plugins			: [{ ptype: 'gridfilters' }],
	readOnly		: false,
	reserveScrollbar: true,
	rootVisible		: false,
	scrollable		: 'y',

	bindStore: function(store, initial) {
		var me = this;
		if (!initial && !me.destroyed) {
			var paging = me.down('pagingtoolbar');
			if (paging) paging.bindStore(store);
		}
		me.callParent(arguments);
	},
	
	getRowClass: Ext.emptyFn,

	initComponent: function() {
		var me = this;
		if (me.header) Ext.applyIf(me.header, me.headerConfig);
		me.viewConfig = Ext.applyIf(me.viewConfig || {}, {
			deferEmptyText			: false, 
			getRowClass				: me.getRowClass,
			preserveScrollOnReload	: true
		});
		me.callParent();
	},

	initItems: function() {
		var me = this;
		if (me.items && !me.items.isMixedCollection) me.items = CFJS.apply(me.items, me.itemsConfig);
		if (me.dockedItems && !me.dockedItems.isMixedCollection) me.dockedItems = CFJS.apply(me.dockedItems, me.dockedItemsConfig);
		me.callParent();
	},
	
	onClearFilters: function(component) {
		var me = this, store = me.store, vm = me.lookupViewModel(),
			gridfilters = me.findPlugin('gridfilters');
		if (gridfilters) gridfilters.clearFilters();
		else {
			me.filters.clearFilters();
			if (vm && vm.isBaseModel) vm.setStoreFilters(store);
		}
	},
	
	onPermissionsChange: function(permissions) {
		permissions = permissions || {};
		var me = this, actions = me.actions || {},
			hide = me.permissions.hideUnmapped,
			editor = permissions.editor || { create: !hide, edit: !hide, 'delete': !hide, view: !hide },	
			map = me.permissions.columns || {},
			columns = me.getColumns(),
			i = 0, column, key;
		me.readOnly = me.readOnly || !editor.edit;
		me.hideComponent(actions.addRecord, !editor.create || !editor.edit);
		me.hideComponent(actions.editRecord, !editor.edit);
		me.hideComponent(actions.removeRecord, !editor['delete']);
		for (; i < columns.length; i++) {
			column = columns[i] = columns[i].initialConfig;
			key = map[column.dataIndex] || column.dataIndex;
			if (!Ext.isEmpty(key) && column.xtype !== 'rownumberer') {
				column.hideable = permissions[key] ? permissions[key].view : !hide;
				column.menuDisable = !column.hideable;
				column.hidden = !column.hideable;
			} else columns.splice(i--, 1);
		}
		me.reconfigure(columns);
	}, 
	
	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.hideComponent(actions.addRecord, readOnly);
		me.hideComponent(actions.editRecord, readOnly);
		me.hideComponent(actions.removeRecord, readOnly);
		me.readOnly = readOnly;
	}
	
});
