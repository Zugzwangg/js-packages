Ext.define('CFJS.window.ResourceProperties', {
	extend				: 'Ext.window.Window',
	xtype				: 'window-resource-properties',
	requires			: [ 'CFJS.panel.ResourceProperties', 'CFJS.store.webdav.ResourceVersions' ],
	bind				: { title: 'Resource properties: {resource.name}' },
	defaultFocus		: 'name',
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader(),
	height				: 400,
	iconCls				: CFJS.UI.iconCls.COG,
	layout				: 'fit',
	plain				: true,
	maximizable			: true,
	minHeight			: 350,
	minWidth			: 500,
	modal				: true,

	initComponent: function() {
		var me = this;
		if (!me.editor || !me.editor.isPanel) {
			me.editor = Ext.create(CFJS.apply({
				xtype: 'panel-resource-properties',
				header: false,
				viewModel: { parent: me.lookupViewModel() }
			}, me.editorConfig));
		}
		me.header.items = [ me.editor.getActions().saveResource ];
		me.items = [ me.editor ];
		me.callParent();
	}
	
});