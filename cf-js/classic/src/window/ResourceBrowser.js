Ext.define('CFJS.window.ResourceBrowser', {
	extend		: 'Ext.window.Window',
	xtype		: 'window-resource-browser',
	requires 	: [ 'CFJS.grid.ResourceBrowser' ],
	autoShow	: true,
	contextMenu	: false,
	layout		: 'fit',
	maximized	: true,
	modal		: true,
	
	initComponent: function() {
		var me = this, browser;
		me.items = me.browser = Ext.create(Ext.applyIf({
			xtype	: 'grid-resource-browser',
			header	: false
		}, me.browserConfig));
		me.header = me.browser.createHeader();
		me.callParent();
	}

});
