Ext.define('CFJS.window.LockingWindow', {
	extend		: 'Ext.window.Window',
	xtype		: 'lockingwindow',
	requires 	: [ 'Ext.layout.container.VBox' ],
	cls			: 'locked-window',
	closable	: false,
	resizable	: false,
	autoShow	: true,
	titleAlign	: 'center',
	maximized	: true,
	modal		: true,
	frameHeader	: false,
	layout		: { type: 'vbox', align: 'center', pack: 'center' }
});
