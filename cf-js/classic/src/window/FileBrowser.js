Ext.define('CFJS.window.FileBrowser', {
	extend		: 'Ext.window.Window',
	xtype		: 'window-file-browser',
	requires 	: [ 'CFJS.grid.FileBrowser' ],
	autoShow	: true,
	contextMenu	: false,
	layout		: 'fit',
	maximized	: true,
	modal		: true,
	
	initComponent: function() {
		var me = this, browser;
		me.items = me.browser = Ext.create(Ext.apply({
			xtype	: 'grid-file-browser',
			header	: false
		}, me.browserConfig));
		me.header = me.browser.createHeader();
		me.callParent();
	}

});
