Ext.define('CFJS.wizard.BaseForm', {
	extend		: 'Ext.form.Panel',
	xtype		: 'base-wizard',
	layout 		: 'card',
	defaultType	: 'form',
	
	getItemsCount: function() {
		return this.items.length;
	},
	
	getNext: function() {
		return this.getLayout().getNext();
	},

	getPrev: function() {
		return this.getLayout().getPrev();
	},
	
	next: function() {
		this.stepTo('next', this.lookupViewModel());
	},
	
	prev: function() {
		this.stepTo('prev', this.lookupViewModel());
	},
	
	privates: {
		
		stepTo: function(direction, vm) {
			if (vm && vm.isViewModel) {
				var me = this;
				if (Ext.isFunction(vm[direction])) {
					vm[direction]();
					me.getLayout()[direction]();
				}
				else me.stepTo(direction, vm.getParent());
			}
		}
		
	}

});
