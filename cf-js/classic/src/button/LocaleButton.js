Ext.define('CFJS.model.Locale', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'locale',	type: 'string' },
		{ name: 'text',		type: 'string' },
		{ name: 'icon',		type: 'string' },
		{ name: 'iconSrc',	type: 'string', 
			calculate: function(data) {
				return CFJS.sharedResourcePath('images/flags/' + (data.icon || data.locale + '.png'), 'cf-js');
			} 
		}
	]
});
Ext.define('CFJS.button.LocaleButton', {
	extend				: 'Ext.button.Button',
	alternateClassName	: 'CFJS.LocaleButton',
	alias				: ['widget.btnlocale', 'widget.buttonlocale'],
	requires			: [ 'Ext.data.StoreManager' ],
	mixins				: [ 'Ext.util.StoreHolder' ],
	arrowVisible		: false,
	config				: {
		locale	: CFJS.getLocale(),
		locales	: [{ locale: 'en', text: 'ENG' },{ locale: 'uk', text: 'УКР' },{ locale: 'ru', text: 'РУС' }]
	},
	iconField	: 'iconSrc',
	idField		: 'locale',
	labelField	: 'text',
	
	afterRender: function(){
		var me = this, menu = me.getMenu();
		me.callParent(arguments);
		if (menu && menu.isMenu) menu.setWidth(me.getWidth());
	},
	
	applyLocale: function(locale) {
		var me = this, store = me.store;
		locale = me.toLocale(locale);
		if (!locale.isModel) {
			locale = locale.locale;
			if (store && store.isStore) {
				locale = store.byLocale.get(locale);
				if (!locale && store.getCount()) locale = store.getAt(0);
			}
		}
		return locale;
	},

	createMenu: function(store) {
		var me = this,
			items = [], menu;
		store.each(function(record) {
			items.push({
				value		: record.get(me.idField),
				text		: me.hideText ? record.get(me.idField).toUpperCase() : record.get(me.labelField),
				icon		: me.hideIcon ? null : record.get(me.iconField),
				listeners	: { click: me.onChangeLocale, scope: me }
			})
		});
		if (!Ext.isEmpty(items)) {
			menu = { items: items, minWidth: me.minWidth };
			if (me.rendered) menu.width = me.getWidth();
		}
		me.setMenu(menu);
		me.setLocale(me.locale);
	},

	initComponent: function() {
		var me = this, store = me.store;
		me.bindStore(store || 'locales-store', true);
		me.callParent();
	},
	
	getStoreListeners: function(store) {
		var me = this;
		return { add: me.createMenu, refresh: me.createMenu, remove: me.createMenu, update: me.createMenu };
	},
	
	onBindStore: function(store, initial) {
		if (store) {
			var me = this,
				locales = me.locales,
				i = 0, locale;
			store.setExtraKeys({ byLocale: { rootProperty: 'data', property: me.idField, unique: true } });
			if (store.getProxy().type === 'memory') {
				if (locales && !store.getCount()) {
					store.beginUpdate();
					if (!Ext.isArray(locales)) locales = [locales];
					for (; i < locales.length; i++) {
						if (Ext.isEmpty(locales[i]) || Ext.Object.isEmpty(locales[i])) locales.splice(i--, 1);
						else locales[i] = me.toLocale(locales[i]);
					}
					store.add(locales);
					store.endUpdate();
				} else me.createMenu(store);
			}
		}
	},
	
	onChangeLocale: function(item) {
		var me = this,
			current = me.getLocale(),
			locale = item && item.value ? me.store.byLocale.get(item.value) : null;
		if (locale && locale !== current) {
			location.replace(location.origin + location.pathname + '?locale=' + locale.get(me.idField) + location.hash);
		}
	},
	
    onDestroy: function() {
    	var me = this;
    	me.bindStore(null);
    	me.callParent();
    },
    
	toLocale: function(obj) {
		if (Ext.isObject(obj || '')) return obj;
		var me = this, str = obj;
		obj = {};
		obj[me.idField] = str;
		obj[me.labelField] = str;
		return obj;
	},
	
	updateLocale: function(locale) {
		var me = this;
		if (locale.isModel) locale = locale.getData();
		if (!me.hideText) me.setText(locale[me.labelField]);
		if (!me.hideIcon) me.setIcon(locale[me.iconField]);
	}
    
}, function() {
	Ext.regStore('locales-store', { proxy: 'memory', model: 'CFJS.model.Locale' });
});