Ext.define('CFJS.panel.ResourceProperties', {
	extend				: 'Ext.tab.Panel',
	xtype				: 'panel-resource-properties',
	requires			: [
		'Ext.button.Button',
		'Ext.grid.Panel',
		'Ext.form.FieldSet',
		'Ext.form.Panel',
		'Ext.form.field.*',
		'CFJS.grid.InternalSharesPanel',
		'CFJS.store.webdav.ResourceVersions'
	],
	bind				: { hidden: '{!resource}', title: 'Resource properties: {resource.name}' },
	bodyPadding			: 10,
	config				: {
		actions		: {
			downloadResource: {
				handler		: 'onDownloadResource',
				iconCls		:  CFJS.UI.iconCls.DOWNLOAD,
				tooltip		: 'Download selected resource'
			},
			saveResource: {
				bind		: { disabled: '{!resource}' },
				iconCls		: CFJS.UI.iconCls.SAVE,
				tooltip		: 'Save the changes',
				handler		: 'onSaveResource'
			}
		},
		actionsMap	: { header: { items: [ 'saveResource' ] } },
		headerConfig: CFJS.UI.buildHeader()
	},
	contextMenu			: false,
	defaultFocus		: 'name',
	defaultListenerScope: true,
	hidden				: true,
	header				: { items: new Array(1) },
	height				: 400,
	iconCls				: CFJS.UI.iconCls.COG,
	minHeight			: 350,
	minWidth			: 400,
	width				: 600,
	viewModel			: {
		type	: 'default',
		schema	: 'webdav',
		formulas: {
			externalShare: {
				bind: '{resource.externalShare}',
				get	: function(externalShare) { return externalShare; }
			}
		},
		stores	: {
			shares	: {
				type		: 'internal_shares',
				autoLoad	: true,
				pageSize	: 0,
				remoteSort	: false,
				filters		: [{
					id		: 'resource',
					property: 'resource.id',
					type	: 'numeric',
					operator: 'eq',
					value	: '{resource.id}'
				}]
			},
			versions: {
				type	: 'resource_versions',
				autoLoad: true,
				pageSize: 0,
				filters	: [{
					id		: 'resource',
					property: 'resource.id',
					type	: 'numeric',
					operator: 'eq',
					value	: '{resource.id}'
				}],
				sorters	: ['created']
			}
		}
	},
    
	confirmRemove: function(fileName, fn) {
		return Ext.fireEvent('confirm', 'Are you sure to delete a file ' + fileName + '?', fn, this);
	},
	
	createDescriptionTab: function() {
		return CFJS.apply({
			xtype			: 'form',
			fieldDefaults	: { labelAlign: 'top', labelWidth: 100, msgTarget: Ext.supports.Touch ? 'side' : 'qtip' },
			iconCls			: CFJS.UI.iconCls.HOME,
			layout			: { type: 'vbox', pack: 'start', align: 'stretch' },
			name			: 'description',
			title			: 'Description',
			items			: [{
				xtype		: 'textfield',
				bind		: '{resource.name}',
				emptyText	: 'file name',
				fieldLabel	: 'Name',
				itemId		: 'name',
		        maskRe		: /[a-z0-9.\s\-_#()]/i,
				name		: 'name',
				required	: true
			},{
				xtype		: 'textareafield',
				anchor		: '100%',
				bind		: '{resource.description}',
				emptyText	: 'file description',
				fieldLabel	: 'Description',
				flex		: 1,
				name		: 'description'
			}]
		}, this.descriptionConfig);
	},
	
	createSharingTab: function() {
		return CFJS.apply({
			xtype			: 'form',
			fieldDefaults	: { labelWidth: 90, msgTarget: Ext.supports.Touch ? 'side' : 'qtip' },
			iconCls			: CFJS.UI.iconCls.SHARE_ALT,
			layout			: { type: 'vbox', pack: 'start', align: 'stretch' },
			name			: 'access',
			title			: 'Access',
			items			: [{
				xtype		: 'fieldset',
				title		: 'External link',
				layout		: 'anchor',
				padding		: '5 15 0',
				defaultType	: 'container',
				defaults	: { anchor: '100%', layout: 'hbox', margin: '5 0' },
				items		: [{
		            items	: [{
						xtype		: 'datetimefield',
		            	bind		: { readOnly: '{externalShare.token}', value: '{externalShare.expiryTime}' },
						emptyText	: 'expiry time',
						fieldLabel	: 'Expiry time',
						fieldOffset	: 5,
						fieldTime	: { width: 90 },
						flex		: 1,
						name		: 'expiryTime'
		            },{
						xtype		: 'checkbox',
						bind		: { readOnly: '{externalShare.token}', value: '{externalShare.disposable}' },
						boxLabel	: 'Disposable',
						hideLabel	: true,
						margin		: '0 0 0 10',
						name		: 'disposable'
		            },{
		            	xtype	: 'button',
		            	bind	: { hidden: '{externalShare.token}' },
		            	handler	: 'onCreateExternalLink',
		            	iconCls	: CFJS.UI.iconCls.LINK,
						margin	: '0 0 0 10',
		            	text	: 'Create',
		            	tooltip	: 'Create link',
		            	ui		: 'default-toolbar'
		            }]
				},{
		            items	: [{
						xtype		: 'displayfield',
						bind		: '{externalShare.url}',
						fieldLabel	: 'Link',
						flex		: 1,
						renderer	: function(value, field) {
							var style = [
								'display:block','height:2em','line-height:1em',
								'margin-right:10px','overflow:hidden',
								'text-overflow:ellipsis','white-space:pre-wrap'
							].join(';');
							return Ext.isEmpty(value) ? '' : Ext.util.Format.format('<a href="{0}" download style="{1};">{0}</a>', value, style);
						}
		            },{
		            	xtype	: 'button',
		            	bind	: { hidden: '{!externalShare.token}' },
		            	handler	: 'onRemoveExternalLink',
		            	iconCls	: CFJS.UI.iconCls.CANCEL,
		            	text	: 'Remove',
		            	tooltip	: 'Remove link',
		            	ui		: 'default-toolbar'
		            }]
				}]
			},{	
				xtype	: 'grid-internal-shares-panel',
				bind	: '{shares}',
				flex	: 1,
				margin	: 5,
				name	: 'internalShares',
				userCls	: 'shadow-panel'
			}]
		}, this.sharingConfig);
	},
	
	createVersionsTab: function() {
		var me = this, actions = me.getActions();
		return CFJS.apply({
			xtype			: 'grid',
			bind			: '{versions}',
			columns			: [{
				xtype		: 'templatecolumn',
				align		: 'left',
				dataIndex	: 'author',
				flex		: 1,
				menuDisabled: true,
				sortable	: false,
				style		: { textAlign: 'center' },
				text		: 'Author',
				tpl			: '<tpl if="author">{author.name}</tpl>'
			},{
				align		: 'center',
				dataIndex	: 'created',
				format		: Ext.Date.patterns.LongDateTime,
				menuDisabled: true,
				text		: 'Created',
				width		: 170,
				xtype		: 'datecolumn'
			},{
				align		: 'center',
				items		: [ actions.downloadResource ],
				menuDisabled: true,
				sortable	: false,
				width		: 30,
				xtype		: 'actioncolumn'
			}],
			disableSelection: true,
			enableColumnHide: false,
			enableColumnMove: false,
			flex			: 1,
			header			: false,
			headerBorders	: false,
			height			: 200,
			iconCls			: CFJS.UI.iconCls.CODE_FORK,
			listeners		: { itemdblclick: me.onItemDblClick, scope: me },
			loadMask		: true,
			name			: 'versions',
			scrollable		: 'y',
			style			: Ext.theme.name === 'Crisp' ? { border: '1px solid silver;' } : null,
			title			: 'Versions',
			userCls			: 'shadow-panel'
		}, me.versionsConfig);
	},
	
	createTabs: function() {
		return [ this.createDescriptionTab(), this.createVersionsTab(), this.createSharingTab() ];
	},
	
	initComponent: function() {
		var me = this, actions = me.getActions();
		if (me.header) Ext.applyIf(me.header, me.headerConfig);
		me.items = me.createTabs();
		me.callParent();
		me.lookupViewModel().bind('{resource.isFile}', me.onResourceTypeChange, me);
	},
	
	lookupResource: function() {
		return this.lookupViewModel().get('resource');
	},
	
	onResourceTypeChange: function(isFile) {
		var me = this, selectors = ['[name=access]', '[name=versions]'], 
			i = 0, item;
		for (; i < selectors.length; i++) {
			if ((item = me.down(selectors[i])) && item.tab) {
				item.tab.setVisible(isFile);
				if (!isFile && item === me.getActiveTab()) me.setActiveTab(0);
			}
		}
	},
	
	onCreateExternalLink: function(component) {
		var me = this, resource = me.lookupResource();
		if (resource && resource.isResource) resource.externalShare();
	},
	
	onDownloadResource: function(view, rowIndex, colIndex, item, e, record, row) {
		this.onItemDblClick(view, record);
	},

	onItemDblClick: function(view, record) {
		if (record && record.isResourceVersion) {
			var resource = this.lookupResource();
			record.download(resource && resource.isEntity && resource.get('name'));
		}
	},
	
	onRemoveExternalLink: function(component) {
		var me = this, resource = me.lookupResource();
		if (resource && resource.isResource) resource.removeExternalShare();
	},

	onSaveResource: function(component) {
		var me = this, record = me.lookupResource();
		if (record && record.isEntity && record.isValid() && record.dirty) {
			me.disableComponent(component, true);
			record.save({
				callback: function(record, operation, success) {
					me.disableComponent(component);
				}
			});
		}
	}
	
});