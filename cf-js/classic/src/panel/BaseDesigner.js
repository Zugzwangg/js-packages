Ext.define('CFJS.panel.BaseDesigner', {
	extend				: 'Ext.panel.Panel',
	xtype				: 'panel-base-designer',
	requires 			: [ 'Ext.layout.container.Border' ],
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	defaults			: { collapsible: true, split: true },
	defaultType			: 'panel',
	layout				: 'border',

	afterRecordSave: Ext.emptyFn,
	
	isDirty: function() {
		return this.isDirtyRecord();
	},
	
	isDirtyRecord: function() {
		var record = this.lookupRecord();
		return record && (record.dirty || record.phantom);
	},
	
	isValid: function() {
		return this.explorer.isValid();
	},

	lookupRecord: function() {
		var vm = this.lookupViewModel();
		return vm && vm.isBaseModel && vm.getItem();
	},

	onRefreshRecord: function(component) {
		var me = this, record = me.lookupRecord(),
			refresh = function() { record.reject(); if (!record.phantom) record.load(); };
		if (record) {
			if (record.dirty || record.phantom)
				Ext.fireEvent('confirmlostdata', me.refreshDataText + ' "'+ record.get('name') + '"', function(btn) { if (btn === 'yes') refresh(); }, me);
			else refresh();
		}
	},

	onSaveRecord: function(component, force) {
		var me = this, record = me.lookupRecord(), browser = me.actionable;
		if (record && me.fireEvent('beforesaverecord', record) !== false && (record.dirty || record.phantom || force)) {
			browser.disableComponent(component, true);
			record.save({
				callback: function(record, operation, success) {
					if (success) me.afterRecordSave(component, force, record);
					browser.disableComponent(component);
					browser.reloadStore();
				}
			});
		}
	},
	
	onValidityChange: function(panel, valid) {
		this.initActionable(this.actionable, valid);
	}
	
});