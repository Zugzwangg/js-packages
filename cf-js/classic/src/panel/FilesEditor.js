Ext.define('CFJS.panel.FilesEditor', {
	extend				: 'Ext.panel.Panel',
	xtype				: 'panel-files-editor',
	requires			: [
		'Ext.Img',
		'Ext.button.Button',
		'Ext.grid.Panel',
		'Ext.tab.Panel',
		'Ext.toolbar.Toolbar',
		'CFJS.form.AceEditor',
		'CFJS.plugin.FileDropZone',
		'CFJS.store.Files',
		'CFJS.util.UI',
		'CFJS.ux.PDFObject'
	],
	mixins				: [ 'CFJS.mixin.Permissible' ],
	config				: {
		folder		: null,
		fileType	: null,
		pathPrefix	: null,
		service		: 'resources'
	},
	defaultListenerScope: true,
	layout				: 'border',
	newFileTitle		: 'New file name',
	newFileMsg			: 'Please enter file name:',
	uploadMsg			: 'Uploading files...',
	
	applyFileType: function(fileType) {
		return this.clearSlashes(fileType);
	},

	applyFolder: function(folder) {
		return this.clearSlashes(folder); 
	},
	
    applyService: function(service) {
		var me = this, api = CFJS.Api;
		if (Ext.isString(service)) service = api.controller(service);
		return service || api.controller(api.getService());
	},
	
	applyPathPrefix: function(pathPrefix) {
		return this.clearSlashes(pathPrefix);
	},

	// template function for before load event
	beforeFileLoad	: Ext.emptyFn,
	
	confirmRemove: function(path, fn) {
		return Ext.fireEvent('confirm', 'Are you sure to delete a file '+ path + '?', fn, this);
	},

	createEditors: function() {
		var me = this,
			editors = CFJS.apply({
				xtype		: 'tabpanel',
				tabPosition	: 'bottom',
				region		: 'center',
				tbar		: {
					disabled: true,
					items	: [{
						action	: 'saveFile',
						handler	: 'onSaveFile',
						iconCls	: CFJS.UI.iconCls.SAVE,
						tooltip	: 'Save File'
					},{
						action	: 'reloadFile',
						handler	: 'onReloadFile',
						iconCls	: CFJS.UI.iconCls.REFRESH,
						tooltip	: 'Reload File'
					}]
				} 
			}, me.editorsConfig);
		editors = Ext.create(editors);
		editors.on({
			tabchange: function(tabPanel, card) {
				var toolbar = tabPanel && tabPanel.down('toolbar[dock="top"]');
				if (toolbar) toolbar.setDisabled(!(card && card.editable));
			},
			remove: function(tabPanel, card) {
				var toolbar = tabPanel && tabPanel.down('toolbar[dock="top"]');
				if (toolbar && tabPanel.items.getCount() === 0) toolbar.setDisabled(true);
			}
		});
		return editors;
	},
	
	createNavigator: function() {
		var me = this, 
			navigator = CFJS.apply({
				xtype			: 'grid',
				columnLines		: true,
				actions			: {
					addFile		: {
						handler	: 'onAddClick',
						iconCls	: CFJS.UI.iconCls.FILES,
						title	: 'Add File',
						tooltip	: 'Add new file'
					},
					downloadFile: {
						handler	: 'onDownloadClick',
						iconCls	: CFJS.UI.iconCls.DOWNLOAD,
						title	: 'Download File',
						tooltip	: 'Download selected file'
					},
					editFile		: {
						handler	: 'onEditClick',
						iconCls	: CFJS.UI.iconCls.EDIT,
						title	: 'Edit File',
						tooltip	: 'Edit selected file'
					},
					levelUp		: {
						handler	: 'onLevelUp',
						hidden	: true,
						iconCls	: CFJS.UI.iconCls.ELLIPSIS_V,
						title	: 'Directory up',
						tooltip	: 'Directory up'
					},
					refreshStore	: {
						handler	: 'onReloadStore',
						iconCls	: CFJS.UI.iconCls.REFRESH,
						title	: 'Update Data',
						tooltip	: 'Update the data' 
					},
					removeFile	: {
						handler	: 'onRemoveClick',
						iconCls	: CFJS.UI.iconCls.CANCEL,
						title	: 'Remove File',
						tooltip	: 'Remove selected file'
					}
				},
				actionsDisable	: [ 'editFile', 'downloadFile', 'removeFile' ],
				actionsMap		: {
					contextMenu	: { items: [ 'editFile', 'downloadFile', 'removeFile' ] },
					header		: { items: [ 'levelUp', 'addFile', 'editFile', 'downloadFile', 'removeFile', 'refreshStore' ] }
				},
				actionsScope	: me,
				contextMenu		: { items: new Array(3) },
				columns			: [{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'name',
					flex		: 1,
					style		: { textAlign: 'center' },
					text		: 'Name',
					tpl			: '<span class="{iconCls}" style="padding-right:10px"></span>{name}',
				}],
				enableColumnHide: false,
				header			: {
					cls			: CFJS.UI.defHeaderCls,
					defaultType	: 'button',
					items		: new Array(6),
					title		: 'View files'
				},
				iconCls			: CFJS.UI.iconCls.FILES,
				loadMask		: true,
				plugins			: [{ ptype: 'filedrop' }],
				viewConfig		: { emptyText: 'Choose a files or drag them here', deferEmptyText: false },
			}, me.navigatorConfig);
		navigator = Ext.create(navigator);
		navigator.setStore(me.getStore());
		navigator.on({
			itemdblclick	: me.onItemDblClick,
			selectionchange	: me.onSelectionChange,
			scope			: me
		});
		if (Ext.isDefined(window.FormData)) navigator.on({
			beforeload	: me.beforeFileLoad,
			dragover	: me.onFileDragOver,
			drop		: me.onFileDrop,
			load		: me.onFileLoad,
			loadabort	: me.onFileLoadAbort,
			loadend		: me.onFileLoadEnd,
			loaderror	: me.onFileLoadError,
			loadstart	: me.onFileLoadStart,
			scope		: me
		});
		return navigator;
	},
	
	createNavigatorWrap: function() {
		var me = this;
		// initialize navigator
		if (!me.navigator || !me.navigator.isPanel) me.navigator = me.createNavigator();
		return CFJS.apply({
		        border		: false,
		        items		: me.navigator,
				layout		: 'fit',
				region		: 'west',
		        split		: true,
		        width		: 350,
		        maxWidth	: 500,
		        minWidth	: 300
			}, me.navigatorWrapConfig);
	},
	
	createStore: function() {
		var me = this,
			store = new CFJS.store.Files({ autoDestroy: true, autoLoad: true }),
			proxy = store.getProxy(),
			extraParams = proxy.extraParams = proxy.extraParams || {};	
		extraParams.path = me.getProxyPath(me.getPathPrefix(), me.getFolder());
		if (!Ext.isEmpty(me.getFileType())) extraParams.type = me.getFileType();
		return store;
	},
	
	getStore: function() {
		return this.store;
	},
	
	initComponent: function() {
		var me = this;
		// initialize paths and store
		me.store = me.createStore();
		// initialize navigator wrapper
		if (!me.navigatorWrap || !me.navigatorWrap.isPanel) me.navigatorWrap = me.createNavigatorWrap();
		// initialize editors
		if (!me.editors || !me.editors.isPanel) me.editors = me.createEditors();
		me.items = [ me.navigatorWrap,  me.editors ];
		me.callParent();
		me.navigator.dropZone = me.navigator.findPlugin('filedrop');
		me.updateFolder(me.getFolder());
	},

	isFolder: function(folder) {
		return !Ext.isEmpty(folder);
	},

	lookupRecord: function() {
		var navigator = this.navigator, selection = navigator && navigator.getSelection();
		return selection && selection.length > 0 && selection[0];
	},
	
	onAddClick: function() {
		var me = this;
		Ext.Msg.prompt(me.newFileTitle, me.newFileMsg, function(btn, fileName){
		    if (btn == 'ok') me.saveFile(fileName, ' ');
		});
	},
	
	onDownloadClick: function(component) {
		var me = this, record = me.lookupRecord();
		if (record && Ext.isFunction(record.download)) {
			record.download(null, { type: me.buildTypePath() });
		}
	},

	onEditClick: function(component) {
		this.onItemDblClick(this.navigator.view, this.lookupRecord());
	},

	// template function for drag over event
	onFileDragOver	: Ext.emptyFn,
	
	onFileDrop: function(grid, e) {
		var me = this, dropZone = grid.dropZone;
		if (me.autoUpload) {
			var formData = new FormData();
			dropZone.forEachFile(e, function(file) {
				formData.append('upload', file, file.name);
			}, me);
			me.uploadFormData(formData);
		}
	},
	
	onFileLoad: function(grid, e, file) {
		file.data = e.target.result;
	},

	// template function for file load abort event
	onFileLoadAbort	: Ext.emptyFn,
	// template function for file load end event
	onFileLoadEnd	: Ext.emptyFn,
	// template function for file load error event
	onFileLoadError	: Ext.emptyFn,
	// template function for file load start event
	onFileLoadStart	: Ext.emptyFn,

	onItemDblClick: function(view, record) {
		if (record && record.isModel) this.openFile(this.editors, record);
	},
	
	onLevelUp: function(button) {
		var me = this, paths = (me.getFolder() || '').split('/');
		paths.pop();
		me.setFilePath(paths.join('/'));
	},
	
	onPermissionsChange: function(permissions) {
		var me = this, actions = me.navigator && me.navigator.getActions() || {},
			hide = me.permissions.hideUnmapped;
		permissions = permissions || { create: !hide, edit: !hide, 'delete': !hide, view: !hide };
		me.hideComponent(actions.downloadFile, !permissions.view);
		me.hideComponent(actions.addFile, !permissions.view || !permissions.edit || !permissions.create);
		me.hideComponent(actions.editFile, !permissions.view || !permissions.edit);
		me.hideComponent(actions.removeFile, !permissions.view || !permissions['delete']);
	},
	
	openFile: function(tabPanel, record) {
		if (tabPanel && tabPanel.isPanel && record) {
			var me = this, data = record.getData(), editor,
				url = record.downloadUrl(null, { type: me.buildTypePath() }),
				panel = tabPanel.down('panel[absolutePath="' + data.absolutePath + '"]');
			Ext.suspendLayouts();		
			if (!panel) {
				if (data.iconCls.indexOf('image') > -1) {
					panel = { xtype: 'container', scrollable: true, items: { xtype: 'image', alt: data.name, src: url } }
				} else if (data.ext === 'pdf') {
					panel = { xtype: 'uxpdf', pdfName: data.name, src: url };
				} else {
					panel = { xtype	: 'aceeditor', fontSize: 12, mode: /^(css|html|json|xml|yaml|sass)$/i.test(data.ext) ? data.ext : 'text' };
				}
				if (panel) {
					panel = tabPanel.add({
						xtype		: 'panel',
						absolutePath: data.absolutePath,
						closable	: true,
						iconCls		: data.iconCls,
						items		: panel,
						layout		: 'fit',
						resizable	: true,
						title		: data.name
					});
					if (editor = panel.child('aceeditor')) {
						editor.loadFile({ url: url });
						panel.editable = true;
					};
				}
			}
			if (panel) tabPanel.setActiveTab(panel);
			Ext.resumeLayouts(true);
		}
	},
	
	onReloadFile: function(component) {
		var me = this, tabPanel = me.editors,
			panel = tabPanel && tabPanel.getActiveTab(),
			editor = panel && panel.child('aceeditor');
		if (editor) editor.reloadFile();
	},
	
	onReloadStore: function(component) {
		this.getStore().reload();
	},
	
	onRemoveClick: function(component) {
		var me = this, record = this.lookupRecord(),
			path = record.get('name'); 
		if (path && Ext.isString(path)) {
			me.confirmRemove(path, function(btn) {
				if (btn === 'yes') record.erase({
					params: { path: path, type: me.buildTypePath() },
					callback: function(record, operation, success) { 
						var tabPanel = me.editors,
							editor = tabPanel && tabPanel.down('panel[absolutePath="' + record.get('absolutePath') + '"]');
						Ext.suspendLayouts();
						if (editor) {
							tabPanel.remove(editor);
							tabPanel.setActiveTab(0);
						}
						me.getStore().remove(record);
						Ext.resumeLayouts(true);
					} 
				})
		    });
		}
	},
	
	onSaveFile: function(button) {
		var me = this, tabPanel = me.editors,
			panel = tabPanel && tabPanel.getActiveTab(),
			editor = panel && panel.child('aceeditor');
		if (editor) {
			me.saveFile(panel.title, editor.getValue(), 'text/' + (editor.mode === 'text' ? 'plain': editor.mode));
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		if (Ext.isObject(selection)) selection = selection.selectedRecords;
		if (selection.length > 0) selection = selection[0];
		var me = this, actions = me.navigator && me.navigator.getActions() || {}, 
			isSelected = selection && selection.isModel,
			isFile = isSelected && selection.get('isFile');
		me.disableComponent(actions.downloadFile, !isFile);
		me.disableComponent(actions.editFile, !isFile);
		me.disableComponent(actions.removeFile, !isSelected);
	},
	
	saveFile: function(fileName, data, type) {
		var formData = new FormData();
		if (!Ext.isString(data)) data = JSON.stringify(data, null, 4);
		formData.append('upload', new Blob([data], { type : type || 'text/html' }), fileName);
		this.uploadFormData(formData);
	},

	updateFolder: function(folder) {
		this.setProxyPath(this.pathPrefix, (folder || ''));
	},
	
	updatePathPrefix: function(pathPrefix) {
		this.setProxyPath(pathPrefix, (this.getFolder() || ''));
	},

	uploadFormData: function(formData) {
		var me = this;
		me.setLoading(me.uploadMsg);
		CFJS.Api.request({
			url		: me.service.url,
			headers	: { 'Content-Type': undefined },
			method	: 'POST',
			params	: me.uploadParams(),
			rawData	: formData,
			callback: function(options, success, response) {
				me.setLoading(false);
				me.getStore().reload();
			}
		});
	},

	privates: {
		
		buildTypePath: function() {
			var me = this, type = [''];
			if (!Ext.isEmpty(me.folder)) type.unshift(me.folder);
			if (!Ext.isEmpty(me.pathPrefix)) type.unshift(me.pathPrefix);
			if (!Ext.isEmpty(me.fileType)) type.unshift(me.fileType);
			return type.join('/');
		},
		
		clearSlashes: function(path) {
			if (!Ext.isString(path)) path = '';
			if (path.length > 0) {
				if (path[0] === '/') path = path.substr(1);
				if (path[path.length - 1] === '/') path = path.substr(0, path.length - 1);
			}
			return path;
		},
		
		getProxyPath: function(pathPrefix, folder) {
			var path = [pathPrefix, folder].join('/');
			if (path[0] !== '/') path = '/' + path; 
			return path;
		},
		
		setProxyExtraParam: function(key, value, autoLoad) {
			var store = this.getStore(), proxy, extraParams;
			if (store) {
				proxy = store.getProxy();
				extraParams = proxy.extraParams = proxy.extraParams || {};
				if (Ext.isEmpty(value)) delete extraParams[key];
				else extraParams[key] = value;
				if (autoLoad !== false) store.load();
			}
		},

		setProxyPath: function(pathPrefix, folder) {
			var me = this, path = me.getProxyPath(pathPrefix, folder),
				actions = me.navigator && me.navigator.getActions() || {};
			me.setProxyExtraParam('path', path);
			me.hideComponent(actions.levelUp, !me.isFolder(folder));
			return path;
		},
		
		uploadParams: function() {
			var me = this,
				params = { method: [me.service.method, 'upload'].join('.'), path: me.getProxyPath(me.getPathPrefix(), me.getFolder()) };
			if (!Ext.isEmpty(me.getFileType())) params.type = me.getFileType();
			return params;
		}

	}
	
});