Ext.define('CFJS.panel.BaseEditor', {
	extend		: 'Ext.form.Panel',
	xtype		: 'panel-base-editor',
	requires	: [
		'Ext.form.CheckboxGroup',
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'Ext.form.RadioGroup',
		'Ext.form.field.*',
		'Ext.layout.container.VBox',
		'CFJS.form.DateTimeField',
		'CFJS.form.field.*',
		'CFJS.util.UI'
	],
	mixins		: [ 'CFJS.mixin.Permissible' ],
	bodyPadding	: '10 10 0 10',
	defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultType	: 'textfield',
	config		: {
		actions		: {
			addRecord	: {
				handler	: 'onAddRecord',
				iconCls	: CFJS.UI.iconCls.ADD,
				tooltip	: 'Add a record'
			},
			back		: {
				handler	: 'onBackClick',
				iconCls	: CFJS.UI.iconCls.BACK,
				tooltip	: 'Back to the list'
			},
			removeRecord: {
				handler	: 'onRemoveRecord',
				iconCls	: CFJS.UI.iconCls.DELETE,
				tooltip	: 'Delete a record'
			},
			refreshRecord	: {
				handler	: 'onRefreshRecord',
				iconCls	: CFJS.UI.iconCls.REFRESH,
				tooltip	: 'Update the data'
			},
			saveRecord	: {
				formBind: true,
				handler	: 'onSaveRecord',
				iconCls	: CFJS.UI.iconCls.SAVE,
				tooltip	: 'Save the changes'
			}
		},
		actionsMap	: {
			header: { items: [ 'back', 'tbspacer', 'addRecord', 'removeRecord', 'refreshRecord', 'saveRecord' ] }
		},
		headerConfig: CFJS.UI.buildHeader({ titlePosition: 2}),
		permissions	: null
	},
	header		: { items: new Array(6) },
	layout		: { type: 'vbox', pack: 'start', align: 'stretch' },

	initComponent: function() {
		var me = this;
		if (me.header) Ext.applyIf(me.header, me.headerConfig);
		me.callParent();
	},
	
	onPermissionsChange: function(permissions) {
		permissions = permissions || {};
		var me = this, actions = me.actions || {},
			vm = me.lookupViewModel(),
			hide = me.permissions.hideUnmapped,
			editor = permissions.editor || { create: !hide, edit: !hide, 'delete': !hide, view: !hide },
			map = me.permissions.fields || {},
			record = vm && vm.isBaseModel ? vm.getItem() : null,
			phantom = record ? record.phantom : false,
			fields = me.getFields().items,
			f = 0, field, permission, editable, visible;
		me.readOnly = me.readOnly || !editor.edit;
		me.hideComponent(actions.addRecord, !editor.create || me.readOnly)
		me.hideComponent(actions.saveRecord, me.readOnly);
		me.hideComponent(actions.removeRecord, !editor['delete'] || me.readOnly);
		for (; f < fields.length; f++) {
			field = fields[f];
			permission = permissions[map[field.name] || field.name];
			visible = editable = !hide;
			if (permission) {
				editable = phantom && permission.create || !phantom && permission.edit;
				visible = permission.view;
			}
			field.setVisible(visible);	
			field.setReadOnly(me.readOnly || !editable);
		}
	},
	
	startEdit: function(record) {
		var vm = this.lookupViewModel();
		if (vm && vm.isBaseModel) vm.setItem(record);
	}
	
});
