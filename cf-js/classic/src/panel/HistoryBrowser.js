Ext.define('CFJS.panel.HistoryBrowser', {
	extend			: 'CFJS.panel.BaseBrowser',
	xtype			: 'panel-history-browser',
	routeSeparator	: '/',
	
	initComponent: function() {
		var me = this;
		Ext.util.History.on('change', me.onHistoryStateChange, me);
		me.callParent();
		me.onHistoryStateChange(Ext.util.History.getToken());
	},
	
	onHistoryStateChange: function(token) {
		token = token || '';
		var me = this, store = me.lookupBrowserStore(), item = me.getActiveTab(), cardToken, card;
		if (me.hashToken() === token || token.indexOf(me.routeId) !== 0) return;
		if (store && store.isStore) {
			if (store.isLoading() || !store.isLoaded()) {
				store.on({ load: function() { me.onHistoryStateChange(token) }, single: true });
				return;
			}
			item = CFJS.safeParseInt(token.replace(cardToken = me.tabHashToken(), ''));
			if (item > 0) item = store.getById(item);
			if (item && item.isModel) {
				me.historyChange = true;
				me.startEdit(item);
				Ext.callback((card = me.getActiveTab()).setToken, card, [ token.replace(cardToken + me.tabIdSeparator + item.id + me.routeSeparator, '') ]);
				delete me.historyChange;
			} else {
				me.setActiveTab(0);
				me.redirectTo(me.routeId);
			}
		}
	},

	onTabChange: function(tabPanel, newCard, oldCard) {
		var me = this;
		me.callParent(arguments);
		if (!me.historyChange) me.redirectTo(me.cardRoute(newCard));
	},

	redirectTo: function(token) {
		var isCurrent = Ext.util.History.getToken() === token;
		if (!isCurrent) Ext.util.History.add(token);
	},

	hashToken: function() {
		return this.cardRoute(this.getActiveTab());
	},

	privates: {

    	cardRoute: function(card) {
    		var me = this;
    		return card && card.itemId ? me.tabHashToken(card.itemId) : me.routeId;
    	},
    	
    	tabHashToken: function(id) {
    		var me = this;
    		return [me.routeId, me.tabSelector(id)].join(me.routeSeparator);
		}

    }
    
});