Ext.define('CFJS.panel.BaseBrowser', {
	extend				: 'Ext.tab.Panel',
	xtype				: 'base-browser',
	requires 			: [
		'Ext.form.field.Text',
		'Ext.toolbar.Toolbar',
		'Ext.ux.TabReorderer',
		'CFJS.panel.BrowserView',
		'CFJS.pages.Blank',
		'CFJS.pages.Error404',
		'CFJS.util.UI'
	],
	config				: {
		contextMenu		: true,
		filterConfig	: true,
		toolBar			: {
			xtype	: 'toolbar',
			cls		: [ 'toolbar-btn-header', CFJS.UI.defHeaderCls ].join(' '),
			hidden	: true,
			padding	: '0 5'
		}
	},
    defaultListenerScope: true,
	homeIdSuffix		: 'home',
	plugins				: 'tabreorderer',
	readOnly			: false,
	searchText			: 'Search',
	tabIdPrefix			: 'item',
	tabIdSeparator		: '-',
	tabPosition			: 'bottom',
	tabWidth			: 20,

	afterRender: function() {
		var me = this, title = me.getHeader().getTitle();
		me.callParent(arguments);
		title.on({ element: 'el', click: function() { me.setActiveTab(0); } });
	},

	applyBrowserStoreFilter: function(filter, suppressEvent) {
		var store = this.lookupBrowserStore();
		if (store && store.isStore && filter) {
			filter.id = filter.id || filter.property;
			if (filter.value) store.addFilter(filter, suppressEvent);
			else store.removeFilter(filter.id, suppressEvent);
		}
	},

	createChildTab: function(record) {
		var me = this, toolBar = me.toolBar, searchFilter = me.searchFilter(), tab;
		if (record && record.isModel) {
			tab = CFJS.apply({
				xtype		: 'blank-page',
				actionable	: me,
				iconCls		: CFJS.UI.iconCls.FILE_TEXT,
				itemId		: me.tabSelector(record.id),
				tabConfig	: {
					closable: true,
					text	: Ext.isNumber(me.tabWidth) ? Ext.util.Format.ellipsis(record.get('name'), me.tabWidth) : record.get('name'),
					tooltip	: record.get('name')
				},
				scrollable	: true,
				title		: record.get('name'),
				listeners	: { editingchange: me.onEditingChange, scope: me }
			}, me.tabDefaults);
		}
		return tab;
	},
	
	createBrowserView: Ext.identityFn,
	
	initComponent: function() {
		var me = this, items = [], title = me.getTitle();
		if (Ext.isString(title)) title = { text: title };
		me.setTitle(Ext.apply({ style: { cursor: 'pointer' } }, title));
		if (me.toolBar && !me.toolBar.isToolBar) me.toolBar = Ext.create(me.toolBar);
		if (!me.header || !me.header.isHeader) {
			if (me.toolBar) items.push(me.toolBar);
			if (me.filterConfig) items.push(Ext.apply({
				xtype		: 'textfield',
				emptyText	: me.searchText,
				listeners	: { change: 'onFilterFieldChange', buffer: 300, scope	: me },
				name		: 'searchFilter',
				triggers	: {
					clear	: {
						cls		: Ext.baseCSSPrefix + 'form-clear-trigger',
						handler	: 'onFilterClearTriggerClick',
						hidden	: true
					},
					search	: {
						cls		: Ext.baseCSSPrefix + 'form-search-trigger',
						weight	: 1,
						handler	: 'onFilterSearchTriggerClick'
					}
				},
				width		: 250
			}, me.filterConfig));
			me.header = { items: items, titlePosition: 0 };
		}
		if (!me.browserView) {
			me.browserView = Ext.create(me.createBrowserView({
				xtype			: 'panel-browser-view',
				bodyPadding		: 5,
				deferEmptyText	: false,
				isHome			: true,
				itemId			: me.tabSelector(me.homeIdSuffix),
				reorderable		: true,
				scrollable		: true,
				tabConfig		: { hidden: true }
			}));
		}
		me.items = [ me.browserView ];
		me.callParent();
		me.browserView.on({ itemdblclick: me.onItemDblClick, scope: me });
		me.on({ tabchange: me.onTabChange, scope: me });
	},
	
	lookupBrowserStore: function() {
		return this.browserView && this.browserView.getStore();
	},
	
	onEditingChange: function(card, editing, record) {
		var me = this, toolBar = me.toolBar, searchFilter = me.searchFilter();
		if (toolBar) toolBar.setHidden(editing);
		if (searchFilter) searchFilter.setHidden(editing);
	},
	
	onFilterClearTriggerClick: function() {
		this.searchFilter().setValue();
	},

	onFilterFieldChange: function(field, value) {
		var me = this, view = me.browserView,
			store = view && view.getStore();
		if (value && value.length > 0) {
			view.preFilterSelection = view.getSelectionModel().getLastSelected();
			field.getTrigger('clear').show();
			store.getFilters().add(me.createSearchFilter(value));
		} else {
			store.removeFilter('search');
			field.getTrigger('clear').hide();
			if (view.preFilterSelection) view.ensureVisible(view.preFilterSelection, { select: true });
        }
	},
	
	onFilterSearchTriggerClick: function() {
		var me = this, searchFilter = me.searchFilter();
		me.onFilterFieldChange(searchFilter, searchFilter.getValue());
	},

	onItemDblClick: function(view, record, item, index, e) {
		this.startEdit(record);
	},
	
	onTabChange: function(tabPanel, newCard, oldCard) {
		var me = this, searchFilter = me.searchFilter();
		if (me.toolBar) me.toolBar.setHidden(newCard.isHome || newCard.editing);
		if (searchFilter) searchFilter.setHidden(newCard.editing);
		if (!newCard.isHome && !newCard.initializing) me.executeChildAction('initActionable', [me]);
	},
	
	reloadStore: function() {
		this.lookupBrowserStore().reload();
	},
	
	startEdit: function(record) {
		var me = this, tab = record && me.itemTab(record.id);
		if (!tab) tab = me.createChildTab(record);
		me.setActiveTab(tab || 0);
	},
	
	privates: {
		
		createSearchFilter: function(value) {
			return {
				id		: 'search',
				property: 'name',
				type	: 'string',
				operator: 'like',
	        	value	: value
			}
		},
		
		searchFilter: function() {
			return this.down('textfield[name=searchFilter]');
		},
	
		itemTab: function(id) {
			return this.child('#' + this.tabSelector(id));
		},

		tabSelector: function(id) {
			return [this.tabIdPrefix, id].join(this.tabIdSeparator);
		}

	}

});
