/**
 * This is the base class for all Authentication related Form dialogs. It optionally
 * enables autoComplete support to any child textfield so that browsers or their plugins
 * may restore/persist username, password and other attributes to/from such forms.
 */
Ext.define('CFJS.panel.Authenticate', {
	extend		: 'Ext.form.Panel',
	xtype		: 'login-panel',
	requires	: [ 'CFJS.view.AuthenticationModel' ],
	/**
	 * @cfg {Boolean} [autoComplete=false]
	 * Enables browser (or Password Managers) support for autoCompletion of User Id and
	 * password.
	 */
	autoComplete: false,
	defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([value])',
	fakeSubmitId: 'fakesubmit',
	viewModel	: { type: 'authentication' },

	afterRender: function() {
		var me = this;
		if (me.autoComplete) {
			me.el.createChild({ tag: 'input', type: 'submit', id: me.fakeSubmitId, style: 'display:none' });
		}
		me.callParent(arguments);
	},
	
	initComponent: function () {
		var me = this, listen;
		if (me.autoComplete) {
			// Use standard FORM tag for detection by browser or password tools
			me.autoEl = Ext.applyIf(me.autoEl || {}, {
				method	: 'post',
				name	: 'login-panel',
				tag		: 'form',
				target	: '_self'
			});
		}
		me.addCls('auth-panel');
		me.callParent();
		if (me.autoComplete) {
			listen = {
				afterrender	: 'doAutoComplete',
				scope		: me,
				single		: true
			};
			Ext.each(me.query('textfield'), function (field) { field.on(listen); });
		}
	},
	
	doAutoComplete : function(target) {
		if (target.inputEl && target.autoComplete !== false) {
			target.inputEl.set({ autocomplete: 'on' });
		}
	}
});
