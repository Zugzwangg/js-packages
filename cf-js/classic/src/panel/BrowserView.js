Ext.define('CFJS.panel.BrowserView', {
	extend				: 'Ext.panel.Panel',
	xtype				: 'panel-browser-view',
	requires			: [ 'Ext.data.Store', 'Ext.layout.container.Fit', 'Ext.view.View' ],
	autoLoad			: false,
	config				: { selection: null },
	defaultBindProperty	: 'store',
	emptyText			: 'No data to display',
	focusable			: true,
	isBrowserView		: true,
	layout				: 'fit',
	publishes			: ['selection'],
	twoWayBindable		: ['selection'],
	viewCls				: Ext.baseCSSPrefix + 'browser-view',
	viewType			: 'dataview',

	applyScrollable: function(scrollable) {
		if (this.view) this.view.setScrollable(scrollable);
		return scrollable;
	},
	
	bindStore: function(store, initial) {
		var me = this, view = me.getView();
		if (store) {
			me.store = store;
			if (view.store !== store) view.bindStore(store, false);
			me.mon(store, { load: me.onStoreLoad, scope: me });
			me.storeRelayers = me.relayEvents(store, [ 'filterchange', 'groupchange' ]);
		} else me.unbindStore();
    },
    
	constructor: function (config) {
		var me = this, viewCls = me.viewCls, store;
		me = Ext.apply(me, {
			emptyCls			: viewCls + '-empty',
			itemCls				: viewCls + '-item',
			itemIconCls			: viewCls + '-item-icon',
			itemOverCls			: viewCls + '-item-over',
			itemSelectedCls		: viewCls + '-item-selected',
			itemTitleCls		: viewCls + '-item-title',
			viewCls				: viewCls
		}),
		me.callParent([config]);
		store = me.store;
		store.trackStateChanges = true;
		if (me.autoLoad && !store.isEmptyStore) store.load();
	},
	
	destroy: function() {
		var me = this;
		me.callParent();
		if (me.destroyed) me.view = me.selModel = null;
	},
	
	ensureVisible: function(record, options) {
		this.doEnsureVisible(record, options);
	},
    
	focus: function() {
		this.getView().focus();
	},

	getSelection: function () {
		return this.getSelectionModel().getSelection();
	},
	
	getSelectionModel: function() {
		return this.getView().getSelectionModel();
	},
	
	getStore: function(){
		return this.store;
	},
	
	getView: function() {
		var me = this, scrollable, viewConfig;
		if (!me.view) {
			scrollable = me.scrollable;
			viewConfig = Ext.apply({
				xtype					: me.viewType,
				cls						: me.viewCls,
				deferEmptyText			: me.deferEmptyText !== false,
				deferInitialRefresh		: me.deferInitialRefresh === true,
				emptyText				: me.emptyText || '',
				itemCls					: me.itemCls,
				itemIconCls				: CFJS.UI.iconCls.FOLDER,
				overItemCls				: me.itemOverCls,
				preserveScrollOnRefresh	: true,
				selectedItemCls			: me.itemSelectedCls,
				singleSelect			: true,
				store					: me.store,
				trackOver				: me.trackMouseOver !== false,
				throttledUpdate			: me.throttledUpdate === true
			}, me.viewConfig);
			viewConfig.itemTpl = viewConfig.itemTpl || [
				'<div class="' + [me.itemIconCls, viewConfig.itemIconCls].join(' ') + '"></div>',
				'<span class="' + me.itemTitleCls + '" data-qtip="{name}">{name}</span>'
			];
			if (scrollable != null) {
				viewConfig.scrollable = scrollable;
				me.scrollable = null;
			}
			me.view = Ext.create(viewConfig);
			if (me.view.emptyText) {
				me.view.emptyText = '<div class="' + me.emptyCls + '">' + me.view.emptyText + '</div>';
			}
			if (me.hasListeners.viewcreated) {
				me.fireEvent('viewcreated', me, me.view);
			}
		}
		return me.view;
	},

	initComponent: function() {
		if (!this.viewType) Ext.raise("You must specify a viewType config.");
		var me = this,
			store = me.store = Ext.data.StoreManager.lookup(me.store || 'ext-empty-store');
		if (me.plugins) me.plugins = me.constructPlugins();
		if (!me.hasView) {
			me.viewConfig = me.viewConfig || {};
			view = me.getView();
			me.items = [view];
			me.hasView = true;
			me.bindStore(store, true);
			me.mon(view, { viewready: me.onViewReady, scope: me });
		}
		me.selModel = me.view.getSelectionModel();
		me.selModel.on({
			scope: me,
			lastselectedchanged: me.updateBindSelection,
			selectionchange: me.updateBindSelection
		});
		me.relayEvents(me.view, [ 'beforecontainerclick', 'beforecontainercontextmenu', 'beforecontainerdblclick', 'beforecontainerkeydown',
			'beforecontainerkeypress', 'beforecontainerkeyup', 'beforecontainermousedown', 'beforecontainermouseout', 'beforecontainermouseover',
			'beforecontainermouseup', 'beforedeselect', 'beforeitemclick', 'beforeitemcontextmenu', 'beforeitemdblclick', 'beforeitemkeydown',
			'beforeitemkeypress', 'beforeitemkeyup', 'beforeitemmousedown', 'beforeitemmouseenter', 'beforeitemmouseleave', 'beforeitemmouseup',
			'beforeselect', 'containerclick', 'containercontextmenu', 'containerdblclick', 'containerkeydown', 'containerkeypress', 'containerkeyup',
			'containermousedown', 'containermouseout', 'containermouseover', 'containermouseup', 'deselect', 'itemclick', 'itemcontextmenu',
			'itemdblclick', 'itemkeydown', 'itemkeypress', 'itemkeyup', 'itemmousedown', 'itemmouseenter', 'itemmouseleave', 'itemmouseup', 'select',
			'selectionchange']);
		me.callParent();
	},

	onDestroy: function(){
		var me = this;
		me.unbindStore();
		me.callParent();
		me.storeRelayers = null;
	},
	
	onStoreLoad: Ext.emptyFn,
	
	onViewReady: function() {
		this.fireEvent('viewready', this);   
	},
	
	setStore: function (store) {
		var me = this,
			oldStore = me.store;
		if (store) store = Ext.StoreManager.lookup(store);
		Ext.suspendLayouts();
		if (store && store !== oldStore) {
			me.unbindStore();
			me.bindStore(store);
		}
		Ext.resumeLayouts(true);
		if (this.autoLoad && !store.isEmptyStore && !(store.loading || store.isLoaded())) store.load();
	},

	updateBindSelection: function(selModel, selection) {
		var me = this, selected = null;
		if (!me.ignoreNextSelection) {
			me.ignoreNextSelection = true;
			if (selection.length) {
				selected = selModel.getLastSelected();
				me.hasHadSelection = true;
			}
			if (me.hasHadSelection) me.setSelection(selected);
			me.ignoreNextSelection = false;
		}
	},

	updateSelection: function(selection) {
		var me = this, sm;
		if (!me.ignoreNextSelection) {
			me.ignoreNextSelection = true;
			sm = me.getSelectionModel();
			if (selection) sm.select(selection);
			else sm.deselectAll();
			me.ignoreNextSelection = false;
		}
	},
	
	unbindStore: function() {
		var me = this, store = me.store, view = me.getView();
		if (store) {
			store.trackStateChanges = false;
			me.store = null;
			me.mun(store, { load: me.onStoreLoad, scope: me });
			Ext.destroy(me.storeRelayers);
			if (view.store) view.bindStore(null);
		}
	},
	
	privates: {

		doEnsureVisible: function(record, options) {
			if (typeof record !== 'number' && !record.isEntity) record = this.store.getById(record);
			var me = this,
				view = me.getView(),
				domNode = view.getNode(record),
				callback, scope, animate,
				highlight, select, doFocus, scrollable;
			if (options) {
				callback = options.callback;
				scope = options.scope;
				animate = options.animate;
				highlight = options.highlight;
				select = options.select;
				doFocus = options.focus;
			}
			if (me.deferredEnsureVisible) me.deferredEnsureVisible.destroy();
			if (!view.componentLayoutCounter) {
				me.deferredEnsureVisible = view.on({
					resize: me.doEnsureVisible,
					args: Ext.Array.slice(arguments),
					scope: me,
					single: true,
					destroyable: true
				});
				return;
			}
			if (domNode) {
				scrollable = view.getScrollable();
				if (scrollable) scrollable.scrollIntoView(domNode, false, animate, highlight);
				if (!record.isEntity) record = view.getRecord(domNode);
				if (select) view.getSelectionModel().select(record);
				if (doFocus) view.getNavigationModel().setPosition(record, 0);
				Ext.callback(callback, scope || me, [true, record, domNode]);
			} else {
				Ext.callback(callback, scope || me, [false, null]);
            }
		}

	}

});