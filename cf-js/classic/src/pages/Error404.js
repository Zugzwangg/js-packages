Ext.define('CFJS.pages.Error404', {
	extend				: 'CFJS.pages.ErrorBase',
	alternateClassName	: 'CFJS.Page404',
	xtype				: 'page404',
	bodyText			: 'Seems you\'ve hit a wall!',
	headerText			: '404'
});
