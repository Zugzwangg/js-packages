Ext.define('CFJS.pages.ErrorBase', {
	extend		: 'CFJS.pages.Blank',
	cls			: 'error-page-container',
	containerCls: 'error-page-inner-container',
	iconCls		: CFJS.UI.iconCls.EXCLAMATION_TRIANGLE,
	homeText	: 'Try going back to our <a href="#home"> Home page </a>',
	renderHtml: function() {
		var me = this;
		return [
			'<div class="error-page-header">',
				'<span class="', me.iconCls, '"></span>',
			   	'<span class="error-page-header-text">', this.headerText, '</span>',
		   	'</div>',
		   	'<div class="error-page-text">', me.bodyText, '</div>',
		   	'<div class="error-page-text">', me.homeText, '</div>'
		].join('');
	}
});
