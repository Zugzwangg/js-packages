Ext.define('CFJS.pages.LockScreen', {
	extend				: 'CFJS.window.LockingWindow',
	alternateClassName	: 'CFJS.LockWindow',
	xtype				: 'lock-screen',
	requires			: [
		'Ext.Img',
		'Ext.container.Container',
		'Ext.form.field.Text',
		'Ext.button.Button',
		'CFJS.panel.Authenticate',
		'CFJS.view.AuthenticationController'
	],
	controller		: 'authentication',
	defaultFocus 	: 'login-panel',
	header			: false,
	pwdMinLength	: 8,

	createBodyContainer: function(config) {
		var me = this,
			container = CFJS.apply({
				xtype			: 'container',
				items			: [ me.ensureChild('unlockPanel') ],
				layout			: 'fit',
				name			: 'bodyContainer'
			}, config);
		return container;
	},
	
	createUnlockPanel: function(config) {
		var me = this,
			panel = CFJS.apply({
				xtype			: 'login-panel',
				autoComplete	: false,
				cls				: 'auth-panel-lock',
				defaultButton	: 'unlock',
				defaultFocus	: 'textfield[name=password]',
				itemId			: 'unlockPanel',
				items			: [{
					xtype	: 'container',
					cls		: 'auth-profile-wrap',
					height	: 120,
					layout	: { type: 'hbox', align: 'center' },
					items	: [{
						xtype	: 'image',
						height	: 80,
						margin	: 20,
						width	: 80,
						alt		: 'lockscreen-image',
						cls		: 'lockscreen-profile-img auth-profile-img',
						bind	: { src: '{user.photoUrl}' }
					},{
						xtype	: 'box',
						bind	: { html: '<div class=\'user-name-text\'> {user.name} </div><div class=\'user-post-text\'> {user.post.name} </div>' }
					}]
				},{
					xtype	: 'container',
					padding	: '0 20',
					layout	: { type: 'vbox', align: 'stretch' },
					defaults: { margin: '10 0' },
					items	: [{
						xtype			: 'textfield',
						allowBlank		: false,
						bind			: '{password}',
						cls				: 'lock-screen-password-textbox auth-textbox',
						emptyText		: 'password',
						fieldLabel		: 'It\'s been a while. Please enter your password to resume',
						inputType		: 'password',
						labelAlign		: 'top',
						labelSeparator	: '',
						minLength		: me.pwdMinLength,
						name			: 'password',
						triggers		: { glyphed: { cls: Ext.baseCSSPrefix + 'form-password-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } }
					},{
						xtype		: 'button',
						action		: 'unlock',
						formBind	: true,
						handler		: 'onUnlockClick',
						iconAlign	: 'right',
						iconCls		: CFJS.UI.iconCls.ANGLE_RIGHT,
						scale		: 'large',
						text		: 'Unlock',
						ui			: 'soft-blue'
					},{
						xtype	: 'box',
						html	: '<div style="text-align:right"><a href="#logout" class="link-logout">or, sign in using other credentials</a></div>'
					}]
				}],
				layout			: { type: 'vbox', align: 'stretch' },
				width			: 455,
			}, config);
		return panel;
	},
	
	initComponent: function() {
		var me = this, panel = 'bodyContainer';
		me.items = [ me.ensureChild(panel) ];
		me.callParent();
		me[panel] = me.lookupChild(panel);
	},
	
	getActiveItem: function() {
		return this.lookupChild('unlockPanel');
	}
	
});
