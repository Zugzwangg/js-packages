Ext.define('CFJS.pages.Authenticate', {
	extend				: 'CFJS.window.LockingWindow',
	alternateClassName	: 'CFJS.LoginView',
	xtype				: 'login-view',
	requires			: [
		'Ext.button.Button',
		'Ext.form.field.Checkbox',
		'Ext.layout.container.HBox',
		'Ext.layout.container.VBox',
		'Ext.toolbar.TextItem',
		'CFJS.form.field.DictionaryComboBox',
		'CFJS.panel.Authenticate',
		'CFJS.view.AuthenticationController'
	],
	controller			: 'authentication',
	defaultFocus		: 'login-panel',
	header				: false,
	pwdСomplexity		: 2,
	pwdСomplexityText	: [
		'Password must contain Latin letters',
		'Password must contain Latin uppercase and lowercase',
		'Password must contain digits, Latin uppercase and lowercase',
		'Password must contain digits, special chars, Latin uppercase and lowercase'
	],
	pwdMeterRe			: [
		/(?=.*[a-z])/,
		/(?=.*[a-z])(?=.*[A-Z])/,
		/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/,
		/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!,@,#,$,%,^,&,*,?,_,~])/
	],
	pwdMinLength		: 8,
	samePasswordText	: 'Passwords must be the same',

	createBodyContainer: function(config) {
		var me = this,
			container = CFJS.apply({
				xtype			: 'container',
				defaults		: {
					autoComplete: true,
					bodyPadding	: '20 20',
					cls			: 'auth-panel-login',
					defaults 	: { margin: '5 0', selectOnFocus: true },
					defaultType	: 'textfield',
					header		: false,
					layout		: { type: 'vbox', align: 'stretch' },
					width		: 415,
					url			: ''
				},
				defaultType		: 'login-panel',
				items			: [
					me.ensureChild('loginPanel'),
					me.ensureChild('recoveryEmail'), 
					me.ensureChild('resetPassword'),
					me.ensureChild('passwordChanged')
				],
				layout			: 'card',
				name			: 'bodyContainer'
			}, config);
		return container;
	},
	
	createLoginPanel: function(config) {
		var me = this,
			panel = CFJS.apply({
				defaultButton	: 'login',
				defaultFocus	: 'textfield[name=user]',
				fakeSubmitId	: 'fakesubmit-login',
				itemId			: 'loginPanel',
				items			: [{
					xtype		: 'label',
					text		: 'Sign in to your account'
				},{
					bind		: '{username}',
					cls			: 'auth-textbox',
					emptyText	: 'user ID',
					height		: 55,
					hideLabel	: true,
					name		: 'user',
					required	: true,
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-user-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } }
				},{
					bind		: '{password}',
					cls			: 'auth-textbox',
					emptyText	: 'password',
					height		: 55,
					hideLabel	: true,
					inputType	: 'password',
					minLength	: me.pwdMinLength,
					name		: 'pwd',
					required	: true,
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-password-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } }
				},{
					xtype			: 'dictionarycombo',
					bind			: { store: '{modules}', value: '{service}' },
					blankText		: 'You must select the service',
					cls				: 'auth-combobox',
					editable		: false,
					emptyText		: 'select service',
					name			: 'service',
					queryMode		: 'local',
					selectOnFocus	: false,
					typeAhead		: false
				},{
					xtype	: 'container',
					height	: 30,
					items	: [{
						xtype	: 'checkboxfield',
						flex	: 1,
						cls		: 'form-panel-font-color rememberMeCheckbox',
						bind	: '{persist}',
						boxLabel: 'Remember me'
					},{
						xtype		: 'tbtext',
						html		: '<div class="link-forgot-password" style="text-align:right">Forgot password?</div>',
						listeners	: { click: { element: 'el', fn: 'switchTo', args: [ 'recoveryEmail' ] } }
					}],
					layout	: { type: 'hbox', align: 'center' },
				},{
					xtype		: 'button',
					action		: 'login',
					cls			: 'auth-login-button',
					formBind	: true,
					handler		: 'onLoginClick',
					iconAlign	: 'right',
					iconCls		: CFJS.UI.iconCls.ANGLE_RIGHT,
					scale		: 'large',
					text		: 'Login',
					ui			: 'soft-blue'
//				},{
//					xtype	: 'box',
//					html	: '<div class="outer-div"><div class="seperator">OR</div></div>',
//					margin	: '10 0'
//				},{
//					xtype		: 'button',
//					scale		: 'large',
//					ui			: 'gray',
//					iconAlign	: 'right',
//					iconCls		: CFJS.UI.iconCls.USER_ADD,
//					text		: 'Create Account',
//					listeners	: {
//						click: 'onNewAccount'
//					}
				}]
			}, config);
		return panel;
	},

	createPasswordChanged: function(config) {
		var me = this,
			panel = CFJS.apply({
				autoComplete	: false,
				defaultButton	: 'backLogin',
				defaultType		: 'tbtext',
				itemId			: 'passwordChanged',
				items			: [{
					cls			: 'message-info',
					html		: '<b>Operation was successful!</b><br/>Your Password has been changed!',
					margin		: 0
				},{
					html		: '<div class="link-login" style="text-align:left">Back to login</div>',
					listeners	: { click: { element: 'el', fn: 'switchTo', args: [ 'loginPanel' ] } },
					margin		: 0
				}]
			}, config);
		return panel;
	},
	
	createRecoveryEmail: function(config) {
		var me = this,
			panel = CFJS.apply({
				defaultButton	: 'sendEmail',
				defaultFocus	: 'textfield[name=email]',
				fakeSubmitId	: 'fakesubmit-email',
				itemId			: 'recoveryEmail',
				items			: [{
					xtype		: 'label',
					text		: 'Enter your email address to send further instructions'
				},{
					bind		: '{email}',
					cls			: 'auth-textbox',
					emptyText	: 'user@clever-forms.com',
					height		: 55,
					hideLabel	: true,
					name		: 'email',
					required	: true,
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-envelope-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } },
					vtype		: 'email'
				},{
					xtype		: 'button',
					action		: 'sendEmail',
					cls			: 'auth-send-email-button',
					formBind	: true,
					handler		: 'onSendRecoveryEmailClick',
					iconAlign	: 'right',
					iconCls		: CFJS.UI.iconCls.ANGLE_RIGHT,
					scale		: 'large',
					text		: 'Send email',
					ui			: 'soft-blue'
				},{
					xtype		: 'tbtext',
					html		: '<div class="link-login" style="text-align:right">Back to login</div>',
					listeners	: { click: { element: 'el', fn: 'switchTo', args: [ 'loginPanel' ] } }
				}]
			}, config);
		return panel;
	},

	createResetPassword: function(config) {
		var me = this,
			panel = CFJS.apply({
				defaultButton	: 'resetPassword',
				defaultFocus	: 'textfield[name=password]',
				fakeSubmitId	: 'fakesubmit-reset',
				itemId			: 'resetPassword',
				items			: [{
					xtype		: 'label',
					text		: 'Set a new password'
				},{
					bind		: '{password}',
					cls			: 'auth-textbox',
					emptyText	: 'password',
					height		: 55,
					hideLabel	: true,
					inputType	: 'password',
					minLength	: me.pwdMinLength,
					name		: 'password',
					required	: true,
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-password-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } },
					validator	: function(pwd) {
						return pwd && pwd.match(me.pwdMeterRe[me.pwdСomplexity]) ? true : me.pwdСomplexityText[me.pwdСomplexity];
					}
				},{
					bind		: { value: '{confirmPassword}' },
					cls			: 'auth-textbox',
					emptyText	: 'confirm password',
					height		: 55,
					hideLabel	: true,
					inputType	: 'password',
					minLength	: me.passwordMinLength,
					name		: 'confirmPassword',
					required	: true,
					triggers	: { glyphed: { cls: Ext.baseCSSPrefix + 'form-password-trigger', extraCls: Ext.baseCSSPrefix + 'form-trigger-glyph-noop' } },
					validator	: function(pwd) {
						return pwd === this.lookupViewModel().get('password') ? true : me.samePasswordText;
					}
				},{
					xtype		: 'button',
					action		: 'resetPassword',
					cls			: 'auth-resetpassword-button',
					formBind	: true,
					handler		: 'onResetPasswordClick',
					iconAlign	: 'right',
					iconCls		: CFJS.UI.iconCls.ANGLE_RIGHT,
					scale		: 'large',
					text		: 'Set password',
					ui			: 'soft-blue'
				}]
			}, config);
		return panel;
	},
	
	initComponent: function() {
		var me = this, panel = 'bodyContainer';
		me.items = [ me.ensureChild(panel) ];
		me.callParent();
		me[panel] = me.lookupChild(panel);
	},

	getActiveItem: function() {
		return this.bodyContainer.getLayout().getActiveItem();
	},
	
	setActiveItem: function(newCard) {
		return this.bodyContainer.getLayout().setActiveItem(newCard);
	}

});
