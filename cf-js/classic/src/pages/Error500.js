Ext.define('CFJS.pages.Error500', {
	extend				: 'CFJS.pages.ErrorBase',
	alternateClassName	: 'CFJS.Page500',
	xtype				: 'page500',
	bodyText			: 'Something went wrong and server could not process your request.',
	headerText			: '500'
});
