Ext.define('CFJS.pages.Home', {
	extend	: 'Ext.container.Container',
	xtype	: 'homepage',
	cls		: Ext.baseCSSPrefix + 'home-page',
	id		: 'homepage',
	routeId	: 'home',
});
