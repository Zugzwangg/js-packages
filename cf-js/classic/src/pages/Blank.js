Ext.define('CFJS.pages.Blank', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.BlankPage',
	xtype				: 'blank-page',
	bodyText			: 'Stay tuned for updates',
	containerCls		: 'blank-page-container',
	headerText			: 'Coming Soon!',

	initComponent: function() {
		var me = this;
		me.anchor = '100% -1';
		me.layout = { type: 'vbox', pack: 'center', align: 'center' };
		me.items = { xtype: 'box', cls: me.containerCls, html: me.renderHtml() }
		me.callParent();
	},
	
	renderHtml: function() {
		return [
			'<div class="fa-outer-class"><span class="' + CFJS.UI.iconCls.CLOCK + '"></span></div>',
		   	'<h1>', this.headerText, '</h1>',
		   	'<span class="blank-page-text">', this.bodyText, '</span>'
		].join('');
	}
});
