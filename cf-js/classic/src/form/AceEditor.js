Ext.define('CFJS.form.AceEditor', {
	extend				: 'Ext.Component',
	alias				: 'widget.aceeditor',
    config				: {
		displayIndentGuides			: true,
		enableBasicAutocompletion	: true,
		enableLiveAutocompletion	: false,
		enableSnippets				: true,
		fontSize					: 12,
		fullLineSelection			: true,
		highlightActiveLine			: true,
		highlightSelectedWord		: true,
		mode						: 'text',
		readOnly					: false,
		showInvisibles				: false,
		showPrintMargin				: false,
		theme						: 'eclipse',
		useSoftTabs					: true,
		useWrapMode					: true,
		value						: undefined	
	},
	childEls			: [ 'wrapEl' ],
	defaultBindProperty	: 'value',
	focusable			: true,
	height				: 100,
	liquidLayout		: true,
	publishes			: [ 'value' ],
	renderTpl			: ['<div id="{id}-wrapEl" data-ref="aceEditorWrapEl" class="{[values.$comp.wrapCls]}" role="presentation"></div>'],
	twoWayBindable		: [ 'value' ],
	validateOnChange	: false,
	width				: 200,
	wrapCls				: Ext.baseCSSPrefix + 'ace-editor',
	
	afterRender: function() {
		this.renderEditor();
		this.callParent(arguments);
	},

	applyEnableBasicAutocompletion: function(enableBasicAutocompletion) {
		return !!enableBasicAutocompletion;
	},
	
	applyEnableLiveAutocompletion: function(enableLiveAutocompletion) {
		return !!enableLiveAutocompletion;
	},
	
	applyEnableSnippets: function(enableSnippets) {
		return !!enableSnippets;
	},
	
	applyDisplayIndentGuides: function(displayIndentGuides) {
		return !!displayIndentGuides;
	},
	
	applyFontSize: function(fontSize) {
		return CFJS.safeParseInt(fontSize, 12);
	},
	
	applyFullLineSelection: function(fullLineSelection) {
		return !!fullLineSelection;
	},

	applyHighlightActiveLine: function(highlightActiveLine) {
		return !!highlightActiveLine;
	},

	applyHighlightSelectedWord: function(highlightSelectedWord) {
		return !!highlightSelectedWord;
	},
	
	applyMode: function(mode) {
		return Ext.isString(mode) && !Ext.isEmpty(mode) ? mode : 'text';
	},
	
	applyReadOnly: function(readOnly) {
		return !!readOnly;
	},
	
	applyShowInvisibles: function(showInvisibles) {
		return !!showInvisibles;
	},

	applyShowPrintMargin: function(showPrintMargin) {
		return !!showPrintMargin;
	},

	applyTheme: function(theme) {
		return Ext.isString(theme) && !Ext.isEmpty(theme) ? theme : 'eclipse';
	},

	applyUseSoftTabs: function(useSoftTabs) {
		return !!useSoftTabs;
	},

	applyUseWrapMode: function(useWrapMode) {
		return !!useWrapMode;
	},
	
	getValue: function() {
		return this.value;
	},
	
	renderEditor: function() {
		var me = this, ace = window.ace, editor;
		if (ace && !me.editor) {
			me.editor = editor = ace.edit(me.wrapEl.getId());
			me.updateTheme(me.theme);
			me.updateMode(me.mode);
			me.updateReadOnly(me.readOnly);
			me.updateEnableBasicAutocompletion(me.enableBasicAutocompletion);
			me.updateEnableLiveAutocompletion(me.enableLiveAutocompletion);
			me.updateEnableSnippets(me.enableSnippets);
			me.updateFontSize(me.fontSize);
			me.updateDisplayIndentGuides(me.displayIndentGuides);
			me.updateFullLineSelection(me.fullLineSelection);
			me.updateHighlightActiveLine(me.highlightActiveLine);
			me.updateHighlightSelectedWord(me.highlightSelectedWord);
			me.updateShowInvisibles(me.showInvisibles);
			me.updateShowPrintMargin(me.showPrintMargin);
			me.updateUseSoftTabs(me.useSoftTabs);
			me.updateUseWrapMode(me.useWrapMode);
			editor.setOptions({
				autoScrollEditorIntoView: false,
				wrapBehavioursEnabled: false
			});
			editor.$blockScrolling = Infinity;
			editor.setValue(me.value);
			editor.getSession().on('change', function(e) {
				me.value = editor.getValue();
			});
		}
		return me.editor;
	},

	loadFile: function(options) {
		options = options || {};
		var me = this, requestOptions = Ext.apply({ disableExtraParams: true, noCache: false, signParams: false }, options);
		if (me.ownerCt && requestOptions.loadMask !== false) me.ownerCt.setLoading(requestOptions.loadMask);
		requestOptions.callback = function(opts, success, response) {
			if (success) {
				me.setValue(response.responseText);
				me.lastLoadOptions = opts;
			}
			else CFJS.errorAlert('Load file failed', response);
			if (me.ownerCt) me.ownerCt.setLoading(false);
			Ext.callback(options.callback, opts.scope, [opts, success, response]);
		};
		return Ext.Ajax.request(requestOptions);
	},
	
	reloadFile: function() {
		return this.loadFile(this.lastLoadOptions);
	},
	
	onResize: function(width, height, oldWidth, oldHeight) {
		var me = this;
		me.callParent(arguments);
		me.editor.resize(true);
	},

	setEditorOptions: function() {
		if (this.editor) this.editor.setOption(arguments);
	},
	
	setValue: function(value) {
		var me = this, obj;
		if (value === null || value === undefined) value = '';
		if (me.value !== value) {
			if (value && Ext.isString(value) && me.mode === 'json') value = value.beautifyJson();
			me.value = value;
			if (me.editor) me.editor.setValue(me.value);
		}
		return me;
	},
	
	updateEnableBasicAutocompletion: function(enableBasicAutocompletion) {
		this.setEditorOptions("enableBasicAutocompletion", enableBasicAutocompletion);
	},
	
	updateEnableLiveAutocompletion: function(enableLiveAutocompletion) {
		this.setEditorOptions("enableLiveAutocompletion", enableLiveAutocompletion);
	},
	
	updateEnableSnippets: function(enableSnippets) {
		this.setEditorOptions("enableSnippets", enableSnippets);
	},

	updateDisplayIndentGuides: function(displayIndentGuides) {
		if (this.editor) this.editor.setDisplayIndentGuides(displayIndentGuides);
	},
	
	updateFontSize: function(fontSize) {
		if (this.editor) this.editor.setFontSize(fontSize);
	},

	updateFullLineSelection: function(fullLineSelection) {
		this.setEditorOptions("selectionStyle", fullLineSelection ? "line" : "text");
	},

	updateHighlightActiveLine: function(highlightActiveLine) {
		if (this.editor) this.editor.setHighlightActiveLine(highlightActiveLine);
	},

	updateHighlightSelectedWord: function(highlightSelectedWord) {
		if (this.editor) this.editor.setHighlightSelectedWord(highlightSelectedWord);
	},
	
	updateMode: function(mode) {
		if (this.editor) this.editor.getSession().setMode('ace/mode/' + mode);
	},
	
	updateReadOnly: function(readOnly) {
		if (this.editor) this.editor.setReadOnly(readOnly);
	},

	updateShowInvisibles: function(showInvisibles) {
		if (this.editor) this.editor.setShowInvisibles(showInvisibles);
	},

	updateShowPrintMargin: function(showPrintMargin) {
		if (this.editor) this.editor.setShowPrintMargin(showPrintMargin);
	},

	updateTheme: function(theme) {
		if (this.editor) this.editor.setTheme('ace/theme/' + theme);
	},

	updateUseSoftTabs: function(useSoftTabs) {
		if (this.editor) this.editor.getSession().setUseSoftTabs(useSoftTabs);
	},

	updateUseWrapMode: function(useWrapMode) {
		if (this.editor) this.editor.getSession().setUseWrapMode(useWrapMode);
	}

}, function() {
	CFJS.loadScript('/scripts/ace-editor/ace.js', function() {
		if (Ext.isObject(ace) && ace.config) {
			ace.config.loadModule('ace/ext/language_tools');
		}
	})
});
