Ext.define('CFJS.form.DateTimeField', {
	extend				:'Ext.form.FieldContainer',
	mixins				: { field: 'Ext.form.field.Field' },
	alias				: 'widget.datetimefield',
	requires			: [
		'Ext.form.field.Date',
		'Ext.form.field.Time',
		'Ext.layout.container.Column'
	],
	config				: { editable: true, fieldOffset: 2 },
	allowBlank			: true,
	combineErrors		: true,
	defaultBindProperty	: 'value',
	focusable			: true,
	format				: null,
	layout				: 'column',
	selectOnFocus		: false,
	
	formatDate: function(date, format) {
		return Ext.isDate(date) ? Ext.Date.dateFormat(date, format || this.format) : date;
	},
	
	initComponent: function() {
		var me = this, max = me.maxValue, min = me.minValue; 
		me.name = me.name || me.id;
		me.items = me.setupItems();
		if (min) me.setMinValue(min);
		if (max) me.setMaxValue(max);
		me.callParent();
		me.initField();
	},
	
	initValue: function() {
		var me = this,
			valueCfg = me.value;
		me.originalValue = me.lastValue = valueCfg || me.getValue();
		if (valueCfg) me.setValue(valueCfg);
	},
	
	getFields: function() {
		return this.monitor.getItems();
	},
	
	getErrors: function(value) {
		var me = this,
			format = Ext.String.format,
			minValue = me.minValue,
			maxValue = me.maxValue,
			validator = me.validator,
			errors = [].concat(me.dateField.getErrors() || []).concat(me.timeField.getErrors() || []),
			msg, time;
		if (Ext.isDate(value)) {
			time = value.getTime();
			if (minValue && time < minValue.getTime()) {
				errors.push(format(me.dateField.minText, me.formatDate(minValue)));
			}
			if (maxValue && time > maxValue.getTime()) {
				errors.push(format(me.dateField.maxText, me.formatDate(maxValue)));
			}
			if (Ext.isFunction(validator)) {
				msg = validator.call(me, value);
				if (msg !== true) errors.push(msg);
			}
		}
		return errors;
	},
	
	getValue: function() {
		var me = this, 
			date = me.dateField.getValue(),
			time = me.timeField.getValue();
		if (Ext.isDate(date)) {
			date = new Date(date.getTime());
			if (!Ext.isDate(time)) date = Ext.Date.clearTime(date);
			else {
				date.setHours(time.getHours());
				date.setMinutes(time.getMinutes());
				date.setSeconds(time.getSeconds());
				date.setMilliseconds(time.getMilliseconds());
			}
		}
		return date;
	},
	
	isDirty: function(){
		return this.dateField.isDirty() || this.timeField.isDirty();
	},
	
	leadDateFormat: function(value, format) {
		var me = this, dt = Ext.Date;
		format = format || this.format;
		if (value && !Ext.isEmpty(format)) {
			if (Ext.isDate(value)) value = dt.dateFormat(value, format);
			value = Ext.Date.parse(Ext.isString(value) ? value: new Date(value), format);
		}
		return value;
	},
	
	onAdd: function(field) {
		var me = this,
			items,
			len, i;
		if (field.isPickerField) {
			if (!field.name) field.name = me.name;
			me.mon(field, 'change', me.checkChange, me);
			me.mon(field, 'blur', me.onBlur, me);
			me.mon(field, 'focus', me.onFocus, me);
		}
		else if (field.isContainer) {
			items = field.items.items;
			for (i = 0, len = items.length; i < len; i++) {
				me.onAdd(items[i]);
			}
		}
		me.callParent(arguments);
	},

	onChange: function (newVal) {
		var me = this;
		if (me.validateOnChange) me.validate(newVal);
		me.checkDirty();
	},
	
	onRemove: function(item) {
		var me = this,
			items,
			len, i;
		if (item.isPickerField) {
			me.mun(item, 'change', me.checkChange, me);
			me.mun(item, 'blur', me.onBlur, me);
			me.mun(item, 'focus', me.onFocus, me);
		} else if (item.isContainer) {
			items = item.items.items;
			for (i = 0, len = items.length; i < len; i++) {
				me.onRemove(items[i]);
			}
		}
		me.callParent(arguments);
	},

	parseDate : function(value) {
		if (value && !Ext.isDate(value) && !Ext.isEmpty(this.format)) value = Ext.Date.parse(value, this.format);
		return value;
    },
    
	reset: function() {
		var me = this,
			hadError = me.hasActiveError(),
			preventMark = me.preventMark;
		me.preventMark = true;
		me.batchChanges(function() {
			me.dateField.reset();
			me.timeField.reset();
		});
		me.preventMark = preventMark;
		me.unsetActiveError();
		if (hadError) me.updateLayout();
	},
	
	resetOriginalValue: function() {
		var me = this;
		me.dateField.resetOriginalValue();
		me.timeField.resetOriginalValue();
		me.originalValue = me.getValue();
		me.checkDirty();
	},
	
	setMaxValue: function(value) {
		var me = this, maxValue = me.leadDateFormat(value);
		if (me.dateField) me.dateField.setMaxValue(maxValue);
		me.maxValue = Ext.isDate(maxValue) ? Ext.Date.clone(maxValue) : maxValue;
	},
	
	setMinValue: function(value) {
		var me = this, minValue = me.leadDateFormat(value);
		if (me.dateField) me.dateField.setMinValue(minValue);
		me.minValue = Ext.isDate(minValue) ? Ext.Date.clone(minValue) : minValue;
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.readOnly = readOnly || !me.editable;
		if (me.dateField) me.dateField.setReadOnly(me.readOnly);
		if (me.timeField) me.timeField.setReadOnly(me.readOnly);
	},

	setValue: function(value) {
		var me = this;
		value = me.parseDate(value);
		me.batchChanges(function() {
			Ext.suspendLayouts();
			me.dateField.setValue(value);
			me.timeField.setValue(value);
            Ext.resumeLayouts(true);
        });
		return me;
    },
    
	setupItems: function() {
		var me = this,
			writeablechange = function(field, readOnly) {
				if (me.readOnly != readOnly) Ext.defer(function(){
					field.setReadOnly(me.readOnly);
				}, 50);
			},
			dateCfg = CFJS.apply({
				xtype			: 'datefield',
				allowBlank		: me.allowBlank,
				column			: me.column,
				columnWidth		: 1,
				editable		: me.editable,
				format			: Ext.Date.patterns.NormalDate,
				hideLabel		: true,
				readOnly		: me.readOnly || !me.editable,
				selectOnFocus	: me.selectOnFocus
			}, me.fieldDate),
			timeCfg = CFJS.apply({
				xtype			: 'timefield',
				allowBlank		: me.allowBlank,
				column			: me.column,
				editable		: me.editable,
				format			: Ext.Date.patterns.ShortTime,
				hideLabel		: true,
				margin			: ['0', '0', '0', me.fieldOffset].join(' '),
				readOnly		: me.readOnly || !me.editable,
				selectOnFocus	: me.selectOnFocus,
				width			: 80
			}, me.fieldTime);
		me.dateField = Ext.create(dateCfg);
		me.timeField = Ext.create(timeCfg);
		me.dateField.on('writeablechange', writeablechange);
		me.timeField.on('writeablechange', writeablechange);
		return [me.dateField, me.timeField];
	},
    
	updateEditable: function(editable, oldEditable) {
		var me = this;
		if (me.dateField) me.dateField.setEditable(editable);
		if (me.timeField) me.timeField.setEditable(editable);
        me.setReadOnly(!editable || me.readOnly);
    },

	validate: function(value) {
		var me = this,
			errors,
			isValid,
			wasValid;
		if (me.disabled) isValid = true;
		else {
			errors = me.getErrors(value);
			isValid = Ext.isEmpty(errors);
			wasValid = me.wasValid;
			if (isValid) me.unsetActiveError();
			else me.setActiveError(errors);
		}
		if (isValid !== wasValid) {
			me.wasValid = isValid;
			me.fireEvent('validitychange', me, isValid);
			me.updateLayout();
		}
		return isValid;
	}
    
},
function() {
	this.borrow(Ext.form.field.Base, ['markInvalid', 'clearInvalid', 'setError']);
});