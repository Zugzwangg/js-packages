Ext.define('CFJS.form.CKEditor', {
	extend				: 'Ext.Component',
	alias				: 'widget.ckeditor',
	childEls			: [ 'wrapCt', 'toolbarCt', 'inputWrapCt', 'inputEl' ],
	config				: {
		alignment		: undefined,
		balloonToolbar	: undefined,
		cloudServices	: null,
		fontFamily		: undefined,
		fontSize		: [ 8, 9, 10, 12, 'default', 16, 18, 20, 22, 24, 26 ],
		heading			: undefined,
		highlight		: undefined,
		image			: null,
		toolbar			: undefined,
		typing			: undefined,
		name			: "content",
		value			: undefined	
    },
	defaultBindProperty	: 'value',
	focusable			: true,
	height				: 200,
	inputCls			: Ext.baseCSSPrefix + 'ck-editor-input',
	inputWrapCls		: Ext.baseCSSPrefix + 'ck-editor-input-wrap',
	liquidLayout		: true,
	publishes			: [ 'value' ],
	renderTpl			: [
		'<div id="{id}-wrapCt" data-ref="wrapCt" class="{wrapCls}" role="presentation">',
			'<div id="{id}-toolbarCt" data-ref="toolbarCt" class="{toolbarCls}" role="presentation"></div>',
			'<div id="{id}-inputWrapCt" data-ref="inputWrapCt" class="{inputWrapCls}" role="presentation">',
				'<textarea id="{id}-inputEl" data-ref="inputEl"><tpl if="value">{[Ext.util.Format.htmlEncode(values.value)]}</tpl></textarea>',
			'</div>',
		'</div>'
	],
	toolbarCls			: Ext.baseCSSPrefix + 'ck-editor-toolbar',
	twoWayBindable		: [ 'value' ],
	validateOnChange	: false,
	width				: 200,
	wrapCls				: Ext.baseCSSPrefix + 'ck-editor',

	afterRender: function() {
		this.renderEditor();
		this.callParent(arguments);
	},

	applyAlignment: function(alignment) {
		return Ext.isDefined(alignment) ? Ext.Array.from(alignment) : undefined;
	},

	applyBalloonToolbar: function(balloonToolbar) {
		return Ext.isDefined(balloonToolbar) ? Ext.Array.from(balloonToolbar) : undefined;
	},

	applyFontFamily: function(fontFamily) {
		return Ext.isDefined(fontFamily) ? Ext.Array.from(fontFamily) : undefined;
	},
	
	applyFontSize: function(fontSize) {
		return Ext.isDefined(fontSize) ? Ext.Array.from(fontSize) : undefined;
	},

	applyHeading: function(heading) {
		return Ext.isDefined(heading) ? Ext.Array.from(heading) : undefined;
	},
	
	applyHighlight: function(highlight) {
		return Ext.isDefined(highlight) ? Ext.Array.from(highlight) : undefined;
	},

	applyToolbar: function(toolbar) {
		return Ext.isDefined(toolbar) ? Ext.Array.from(toolbar) : undefined;
	},
	
	getValue: function() {
		var me = this;
		if (me.editor) me.value = me.editor.getData();
		return me.value;
	},

	initRenderData: function() {
		var me = this;
		return Ext.apply(me.callParent(arguments), {
			inputCls	: me.inputCls,
			inputWrapCls: me.inputWrapCls,
			name		: me.name,
			toolbarCls	: me.toolbarCls,
			value		: me.value,
			wrapCls		: me.wrapCls
		});
	},
	
	loadFile: function(options) {
		options = options || {};
		var me = this, requestOptions = Ext.apply({ disableExtraParams: true, noCache: false, signParams: false }, options);
		if (me.ownerCt && requestOptions.loadMask !== false) me.ownerCt.setLoading(requestOptions.loadMask);
		requestOptions.callback = function(opts, success, response) {
			if (success) {
				me.setValue(response.responseText);
				me.lastLoadOptions = opts;
			}
			else CFJS.errorAlert('Load file failed', response);
			if (me.ownerCt) me.ownerCt.setLoading(false);
			Ext.callback(options.callback, opts.scope, [opts, success, response]);
		};
		return Ext.Ajax.request(requestOptions);
	},
	
	onResize: function(width, height, oldWidth, oldHeight) {
		var me = this;
		if (me.editor) {
			me.wrapCt.setHeight(height);
			me.inputWrapCt.setHeight(Math.round(me.wrapCt.getHeight(true) - me.toolbarCt.getHeight()));
			me.inputEl.setHeight(Math.round(me.inputWrapCt.getHeight(true)));
		}
		me.callParent(arguments);
	},
	
	reloadFile: function() {
		return this.loadFile(this.lastLoadOptions);
	},

	renderEditor: function() {
		var me = this;
		if (ClassicEditor && !me.editor) {
			ClassicEditor.create(me.inputEl.dom, Ext.apply(me.editorCfg||{}, { language: CFJS.getLocale() }))
				.then(editor => {
					me.editor = editor;
					me.toolbarCt.appendChild(editor.ui.view.toolbar.element);
					me.inputEl.destroy();
					me.inputEl = Ext.get(editor.ui.view.element);
					me.inputEl.addCls(me.inputCls);
					me.inputEl.setId(me.id + '-inputEl');
					me.inputEl.dom.setAttribute('data-ref', 'inputEl');
					me.inputEl.dom.setAttribute('role', me.inputEl.dom.getAttribute('role') + ' presentation');
					me.onResize(me.getWidth(), me.getHeight());
				})
				.catch(error => { console.error( error ); });
		}
		return me.editor;
	},

	setValue: function(value) {
		var me = this, obj;
		if (value === null || value === undefined) value = '';
		if (me.value !== value) {
			me.value = value;
			if (me.editor) me.editor.setData(me.value);
		}
		return me;
	},
	
	updateAlignment: function(alignment) {
		this.updateEditorConfigOptions('alignment', alignment);
	},
	
	updateBalloonToolbar: function(balloonToolbar) {
		this.updateEditorConfig('balloonToolbar', balloonToolbar);
	},
	
	updateCloudServices: function(cloudServices) {
		this.updateEditorConfig('cloudServices', cloudServices);
	},
	
	updateEditorConfig: function(key, value) {
		var cfg = this.editorCfg = this.editorCfg||{};
		if (value !== undefined) cfg[key] = value;
		else delete cfg[key];
	},
	
	updateEditorConfigOptions: function(key, value) {
		if (value !== undefined && value !== null) value = { options: value };
		this.updateEditorConfig(key, value);
	},
	
	updateFontFamily: function(fontFamily) {
		this.updateEditorConfigOptions('fontFamily', fontFamily);
	},

	updateFontSize: function(fontSize) {
		this.updateEditorConfigOptions('fontSize', fontSize);
	},

	updateHeading: function(heading) {
		this.updateEditorConfigOptions('heading', heading);
	},

	updateHighlight: function(highlight) {
		this.updateEditorConfigOptions('highlight', highlight);
	},
	
	updateImage: function(image) {
		this.updateEditorConfig('image', image);
	},
	
	updateToolbar: function(toolbar) {
		if (toolbar !== undefined && toolbar !== null) toolbar = { items: toolbar };
		this.updateEditorConfig('toolbar', toolbar);
	},

	updateTyping: function(typing) {
		this.updateEditorConfig('typing', typing);
	}

}, function() {
	CFJS.loadLibraries([
		'/scripts/ckeditor/ckeditor.js',
		'/scripts/ckeditor/translations/' + CFJS.getLocale() + '.js'
	]);
});
