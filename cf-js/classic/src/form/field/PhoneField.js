Ext.define('CFJS.form.field.PhoneField', {
	extend				: 'Ext.form.FieldContainer',
	xtype				: 'phonefield',
	mixins				: [ 'Ext.util.StoreHolder', 'Ext.form.field.Field' ],
	requires			: [ 'Ext.form.field.Display', 'Ext.form.field.Text', 'Ext.form.field.ComboBox' ],
	allowBlank			: false,
	combineErrors		: true,
	config				: { defaultCountry: '380' },
	defaultBindProperty	: 'value',
	defaults 			: {
		enforceMaxLength: true,
		selectOnFocus	: true,
		hideLabel		: true
	},
	layout				: 'column',
	readOnly			: false,
	
	clearValue: function(){
		this.setValue([]);    
	},

	getFields: function() {
		return this.monitor.getItems();
	},

	getSubmitData: function() {
		var me = this,
			data = null,
			val;
		if (!me.disabled && me.submitValue && !me.isFileUpload()) {
			val = me.getSubmitValue();
			if (val !== null) {
				data = {};
				data[me.getName()] = val;
			}
		}
		return data;
	},

	getSubmitValue: function() {
		return this.getValue();
	},
	
	getValue: function() {
		return (this.value || []).join(' ').substr(0, 17);
	},
	
	initComponent: function() {
		var me = this;
		me.name = me.name || me.id;
		me.items = me.setupItems();
		me.bindStore(me.store, true);
		me.callParent();
		me.initField();
	},

	onDestroy: function(){
		var me = this;
		me.bindStore(null);
		me.callParent();
	},
	
	onBindStore: function(store){
		var countryField = this.countryField;
		if (countryField) countryField.bindStore(store);
	},
	
	setReadOnly: function(readOnly) {
		var me = this, fields = me.getFields().items, f, field;
		for (f = 0; f < fields.length; f++) {
			fields[f].setReadOnly(readOnly);
		}
		me.readOnly = readOnly;
	},
	
	setupItems: function() {
		var me = this,
			changeHandler = function(field, newValue, oldValue) {
				if (field.isValid()) {
					var me = this, value = me.setupValue(me.getValue());
					value[field.valueIndex] = newValue;
					me.mixins.field.setValue.call(me, value);
				}
			};
		me.countryField = Ext.create(Ext.apply({
			xtype			: 'combobox',
			allowBlank		: me.allowBlank,
			displayField	: 'code',
			emptyText		: 'XXXXX',
			columnWidth		: .35,
			minChars 		: 1,
			publishes		: 'value',
			regex			: /^[0-9]{1,5}$/,
			regexText		: 'Можно вводить только цифры',
			store			: me.store,
//			triggerAction	: 'query',
			typeAhead		: true,
			valueIndex		: 0
		}, me.phoneCountry));
		me.codeField = Ext.create(Ext.apply({
			xtype			: 'textfield',
			allowBlank		: me.allowBlank,
			emptyText		: 'XXX',
			maskRe			: /[0-9]/,
			maxLength		: 3,
			minLength		: 2,
			width			: 45,
			valueIndex		: 1
		}, me.phoneCity));
		me.bodyField = Ext.create(Ext.apply({
			xtype			: 'textfield',
			allowBlank		: me.allowBlank,
			emptyText		: 'XXXXXXXXXXXX',
			columnWidth		: .65,
			maskRe			: /[0-9]/,
			maxLength		: 12,
			minLength		: 6,
			valueIndex		: 2
		}, me.phoneBody));
		
		me.countryField.on('change', changeHandler, me);
		me.codeField.on('change', changeHandler, me);
		me.bodyField.on('change', changeHandler, me);

		return [
			{ xtype: 'displayfield', margin: '0 2 0 0', value: '+' }, 
			me.countryField,
			{ xtype: 'displayfield', margin: '0 2 0 2', value: '(' }, 
			me.codeField,
			{ xtype: 'displayfield', margin: '0 2 0 2', value: ')' },
			me.bodyField
		]
	},
	
	setupValue: function(value) {
		var me = this,
			out = [];
		if (!value || value === '') value = me.defaultCountry + ' ';
		out = value.rpad(12, '0').split(' ', 3);
		if (out.length === 1) {
			value = out[0];
			out.push(value.replace(out[0] = out[0].substr(0, 3), ''));
		}
		if (out.length === 2) {
			value = out[1];
			out.push(value.replace(out[1] = out[1].substr(0, 2), ''));
		}
		return out;
	},
	
	setValue: function(value) {
		var me = this;
		value = me.setupValue(value);
		me.mixins.field.setValue.call(me, value);
		me.countryField.setValue(value[0]);
		me.codeField.setValue(value[1]);
		me.bodyField.setValue(value[2]);
		return me;
	}

});