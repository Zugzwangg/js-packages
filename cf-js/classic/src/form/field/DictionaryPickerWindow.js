Ext.define('CFJS.form.field.DictionaryPickerWindow', {
	extend				: 'Ext.window.Window',
	xtype				: 'dictionarypickerwindow',
	requires			: [ 'Ext.grid.Panel', 'Ext.grid.selection.SpreadsheetModel' ],
	config				: {
		actions			: {
			addRecord	: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Добавить запись',
				tooltip	: 'Добавить новую запись',
				handler	: 'onAddRecord'
			},
			editRecord	: {
				iconCls	: CFJS.UI.iconCls.EDIT,
				title	: 'Редaктировать запись',
				tooltip	: 'Редaктировать текущую запись',
				handler	: 'onEditRecord'
			},
			selectRecord: {
				iconCls	: CFJS.UI.iconCls.APPLY,
				title	: 'Выбрать запись',
				tooltip	: 'Выбрать текущую запись',
				handler	: 'onSelectRecord'
			}
		},
		actionsDisable	: [ 'editRecord', 'selectRecord' ],
		actionsMap		: {
			contextMenu	: { items: [ 'selectRecord', 'editRecord', 'addRecord' ] },
			header		: { items: [ 'selectRecord', 'editRecord', 'addRecord' ] }
		},
		contextMenu		: { items: new Array(3) },
		readOnly		: false
	},
	defaultListenerScope: true,
	headerConfig		: CFJS.UI.buildHeader({ items: new Array(3) }),
	layout				: 'fit',
	modal				: true,
	referenceHolder		: true,
	resizable			: true,
	selectorType		: 'grid',
	
	applyReadOnly: function(readOnly) {
		return !!readOnly;
	},
	
	initComponent: function() {
		var me = this, selectorConfig;
		if (!me.selector && me.selectorConfig != false) {
			selectorConfig = Ext.apply({
				xtype		: me.selectorType,
				contextMenu : false,
				header		: false,
				readOnly	: me.getReadOnly(),
				selModel	: {
					type		: 'spreadsheet',
					cellSelect	: false,
					dragSelect	: false,
					mode		: 'SINGLE', 
					pruneRemoved: false 
				}
			}, me.selectorConfig);
			if (me.scrollable != null) {
				selectorConfig.scrollable = scrollable;
				me.scrollable = null;
			}
			me.items = [ selectorConfig ];
		}
		me.callParent();
		me.selector = me.selector || me.items.first();
		if (me.selector) {
			me.setTitle(me.title || me.selector.title);
			me.setIconCls(me.iconCls || me.selector.iconCls);
		}
	},

	afterRender: function() {
		var me = this;
		me.selector.on({ itemcontextmenu: 'onItemContextMenu', itemdblclick: 'onItemDblClick' });
		me.callParent();
	},
	
	doSelectionChange: function(selectable, selection, actions) {
		var me = this;
		me.disableComponent(actions.selectRecord, !selection || selection.length !== 1);
		me.disableComponent(actions.editRecord, !selection || selection.length !== 1);
	},
	
	onAddRecord: function(component) {
		this.fireEvent('addrecord', this.selector, component);
	},
	
	onEditRecord: function(component) {
		this.fireEvent('editrecord', this.selector, component);
	},
	
	onItemContextMenu: function(view, rec, node, index, e) {
		var me = this, header = me.getHeader();
		e.stopEvent();
		me.showContextMenu([e.getX() - me.getX(), e.getY() - me.getY() - (header ? header.getHeight() : 0)]);
		return false;
	},
	
	onItemDblClick: function(selectable, record) {
		this.select([record]);
	},

	onSelectionChange: function(selectable, selection) {
		if (selection) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			this.doSelectionChange(selectable, selection, this.actions);
		}
	},
	
	onSelectRecord: function(component) {
		this.select(this.selector.getSelection())
	},
	
	select: function(selection) {
		var me = this, record;
		if (selection) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			record = selection[0];
		}
		if (me.fireEvent('beforeselect', me, record) !== false) me.fireEvent('select', me, record);
	},

	updateReadOnly: function(readOnly) {
		var me = this, selector = me.selector, actions = me.getActions();
		if (selector && Ext.isFunction(selector.setReadOnly)) selector.setReadOnly(readOnly);
		me.hideComponent(actions.addRecord, readOnly);
		me.hideComponent(actions.editRecord, readOnly);
	}
	
});