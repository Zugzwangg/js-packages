Ext.define('CFJS.form.field.DictionaryComboBox', {
	extend				: 'Ext.form.field.ComboBox',
	alias				: [ 'widget.dictionarycombobox', 'widget.dictionarycombo' ],
	displayField		: 'name',
	forceSelection  	: true,
	isDictionaryCombo	: true,
	minChars			: 0,
	typeAhead			: true,
	valueField			: 'id',
	
	doSetValue: function(value, add) {
		var me = this, store = me.getStore(), val = {};
		if (Ext.isArray(value) && value.length > 0) value = value[0];
		if (!value.isModel && store && !store.isEmptyStore) {
			if (!Ext.isObject(value)) {
				val[me.valueField] = value;
			} else val = value;
			value = Ext.create(store.getModel(), val);
		}
		return me.callParent([value, add]);
	},
	
	getValue: function() {
		var me = this, value = me.callParent();
		if (value) {
			var model = me.findRecordByValue(value);
			if (model && model.isModel) value = model.data;
			else if (!me.allowBlank) return me.originalValue;
		}
		return me.allowBlank ? value : value || me.originalValue;
	},

	setValue: function(value) {
		var me = this;
		me.originalValue = value;
		return me.callParent(arguments);
	}

});