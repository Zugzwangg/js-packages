Ext.define('CFJS.form.field.CheckboxGroupField', {
	extend				: 'Ext.form.CheckboxGroup',
	alias				: [ 'widget.checkboxgroupfield', 'widget.manyofmanyfield' ],
	alternateClassName	: 'CFJS.form.CheckboxGroupField',
	mixins				: [ 'CFJS.form.field.GroupField' ],

	getErrors: function(value) {
		return this.mixins.groupfield.getErrors.call(this, value);
	},

	getInputValue: function(index) {
		return Math.pow(2, index);
	},
	
	getMaxValue: function() {
		return this.getInputValue((this.getFormat()||[]).length) - 1 + this.getMinValue();
	},

	getModelData: function() {
		return this.mixins.groupfield.getModelData.call(this);
	},
	
	getSubmitData: function() {
		return this.mixins.groupfield.getSubmitData.call(this);
	},
	
	getValue: function() {
		return this.mixins.groupfield.getValue.call(this);
	},
	
	incMaxValue: function(maxValue, delta) {
		return maxValue += delta; 
	},
	
	initField: function() {
		this.mixins.groupfield.initField.call(this);
	},

	initValue: function() {
		this.mixins.groupfield.initValue.call(this);
	},

	isEqual: function(value1, value2) {
		return this.isEqualAsString(value1, value2);
	},
	
	setValue: function(value) {
		return this.mixins.groupfield.setValue.apply(this, arguments);
    }

});