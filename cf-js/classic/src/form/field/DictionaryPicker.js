Ext.define('CFJS.form.field.DictionaryPicker', {
	extend			: 'Ext.form.field.Picker',
	xtype			: 'dictionarypicker',
	mixins			: [ 'Ext.util.StoreHolder' ],
	requires		: [
		'Ext.window.Window',
		'Ext.layout.container.Fit',
		'CFJS.form.field.DictionaryPickerWindow',
		'CFJS.form.field.InlineDictionaryPickerWindow'
	],
	config: {
		closeOnFocusLeave	: true,
		displayField		: 'name',
		store				: null
	},
	editable		: false,
	rawValueData	: true,
	triggerCls		: Ext.baseCSSPrefix + 'form-arrow-trigger',
	
	initComponent: function() {
		var me = this;
		if (me.picker && !me.picker.isComponent) {
			me.selector = Ext.apply(me.picker, me.selector);
			delete me.picker;
		}
		me.bindStore(me.store || 'ext-empty-store', true);
		me.callParent(arguments);
	},
	
	alignPicker: function() {
		var me = this, picker = me.getPicker();
		if (picker && !picker.maximized) me.callParent(arguments);
	},
	
	beforeSelectItem: function(selector, record) {
		return record ? true : false;
	},

	bindStore: function(store, initial) {
		var me = this;
		me.mixins.storeholder.bindStore.call(me, store, initial);
		store = me.getStore();
		if (!initial && store && !store.isEmptyStore) me.setValue(me.value);
	},
	
	checkValueOnChange: function() {
		var me = this, store = me.getStore();
		if (!me.destroying && store.isLoaded() && !store.getById(me.value)) me.clearValue();
	},
	
	clearValue: function() {
		this.setValue(null);
	},
    
	createPicker: function() {
		var me = this,
			selectorWindow = Ext.apply({
				xtype		: 'dictionarypickerwindow',
				closeAction	: 'hide',
				minWidth	: 540,
				minHeight	: 200,
				selectorType: me.selectorType || 'grid'
			}, me.selectorWindow),
			selector = selectorWindow.selectorConfig = Ext.apply(selectorWindow.selectorConfig || {}, me.selectorConfig);

		me.selectorWindow = selectorWindow = Ext.create(selectorWindow);
		me.selector = selector = selectorWindow.selector;
		
		if (selector.getStore() && !selector.getStore().isEmptyStore) me.bindStore(selector.getStore());
		else selector.bindStore(me.store);
		
		selectorWindow.on({
			beforeselect: 'beforeSelectItem',
			close		: 'onPickerCancel',
			select		: 'selectItem',
			scope		: me
		});
		return me.selectorWindow;
	},
	
	getSelection: function() {
		var selModel = this.selector.getSelectionModel(), selection = selModel.getSelection();
		return selection.length ? selModel.getLastSelected() : null;
	},
	
	getStoreListeners: function(store) {
		if (!store.isEmptyStore) {
			var me = this;
			return {
				load		: me.onLoad,
				exception	: me.onException,
				update		: me.onStoreUpdate,
				remove		: me.checkValueOnChange
			};
        }
    },
	
	getValue: function() {
		var me = this, record = me.toRecord(me.value, me.getStore()),
			value = record || me.value;
		if (me.rawValueData && value && value.isModel) value = value.getData();
		return value;
	},

	isEqualAsString: function(value1, value2) {
		if (value1 && value1.isModel) value1 = value1.getData();
		if (value2 && value2.isModel) value2 = value2.getData();
		return JSON.stringify(value1 || '') === JSON.stringify(value2 || '');
	},

	onBindStore: function(store, initial) {
		var me = this, selector = me.selector;
		if (selector && selector.store !== store) selector.bindStore(store);
	},
	
	onException: function() {
		this.collapse();
	},
	
	onExpand: function() {
		var me = this, record = me.toRecord(me.value, me.getStore());
		if (record) me.selector.getSelectionModel().select(record);
	},
	
	onFocusLeave: function(e) {
		if (this.closeOnFocusLeave) this.callParent([e]);
    },
    
	onLoad: function() {
		var me = this;
		if (me.value) me.setValue(me.value);
	},
	
	onPickerCancel: function () {
        this.collapse();
    },
    
	onStoreUpdate: function(store, rec, type, modifiedFieldNames){
		var me = this, display = me.displayField;
		if (type === 'edit' && modifiedFieldNames && Ext.Array.contains(modifiedFieldNames, display) && me.value === rec.getId()) me.setRawValue(rec.get(display));
	},
    
	onUnbindStore: function() {
		var me = this;
		if (me.selector) me.selector.bindStore(null);
	},

	selectItem: function(selector, record) {
		var me = this;
		me.setValue(record.getData());
		me.fireEvent('select', me, record);
		me.collapse();
	},
    
	setValue: function(value) {
		var me = this, store = me.getStore();
		me.value = value;
		if (store.loading) return me;
		var record = me.toRecord(value, store);
		if (record) {
			me.value = record.getId();
			me.setRawValue(record.get(me.displayField));
		} else me.setRawValue('');
		me.checkChange();
		return me;
	},

	toRecord: function(value, store) {
		var me = this, record;
		if (value && (store = store || me.getStore())) {
			if (value.isModel) record = store.getById(value.id);
			else if (Ext.isNumber(value)) record = store.getById(value);
			else record = Ext.create(store.getModel(), value);
		}
		return record;
	}

});