Ext.define('CFJS.form.field.ReferenceComboBox', {
	extend		: 'CFJS.form.field.DictionaryComboBox',
	alias		: [ 'widget.referencecombobox', 'widget.referencecombo' ],
	requires	: [ 'CFJS.store.Named' ],
	config		: {
		hideAddTrigger	: false,
		loaderConfig	: null
	},
	publishes	: [ 'loaderConfig', 'value' ],
	store		: {
		type		: 'named',
		proxy		: { type: 'json.svdapi', service: 'svd' },
		remoteSort	: false
	},
	triggers	: {
		add		: { cls: Ext.baseCSSPrefix + 'form-fa-trigger', extraCls: CFJS.UI.iconCls.ADD,	handler: function() { this.fireEvent('addrecord', this) },  tooltip: 'Add new record' },
	},
	
	applyLoaderConfig: function(loaderConfig) {
		return Ext.apply(loaderConfig || {}, {
			sortInfo: { sort: [{ sortField: '', sortDir: 'ASC' }] }
		});
	},
	
	applyTriggers: function(triggers) {
        var me = this, addTrigger = (triggers||{}).add;
        if (addTrigger) addTrigger.hidden = me.getHideAddTrigger() === true;
        return me.callParent(arguments);
    },
	
	doRemoteQuery: function(queryPlan) {
		var me = this, proxy = me.store.proxy;
		if (proxy.isSvdProxy) {
	        // Querying by a string...
			Ext.apply(proxy.loaderConfig || {}, { query: queryPlan.query });
			delete queryPlan.query;
		}
		me.callParent(arguments);
	},
	
	onBindStore: function(store, initial) {
		var me = this;
		me.updateLoaderConfig(me.loaderConfig);
		me.callParent(arguments);
	},
	
	updateHideAddTrigger: function(hideTrigger) {
		var me = this, triggers = me.getTriggers();
		if (triggers && triggers.add) {
			triggers.add[hideTrigger ? 'hide' : 'show'].apply(triggers.add);
		}
    },
    
	updateLoaderConfig: function(loaderConfig) {
		var store = this.store, proxy = store ? store.proxy : null;
		if (proxy) proxy.loaderConfig = Ext.apply(proxy.loaderConfig || {}, loaderConfig);
	}
	
});