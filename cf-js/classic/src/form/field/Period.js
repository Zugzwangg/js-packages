Ext.define('CFJS.form.field.Period', {
	extend	:'Ext.form.field.Date',
	alias	: 'widget.periodfield',
	requires: [ 'CFJS.picker.Period' ]
});