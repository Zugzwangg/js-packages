Ext.define('CFJS.form.field.RadioGroupField', {
	extend				: 'Ext.form.RadioGroup',
	alias				: [ 'widget.radiogroupfield', 'widget.oneofmanyfield' ],
	alternateClassName	: 'CFJS.form.RadioGroupField',
	mixins				: [ 'CFJS.form.field.GroupField' ],

	checkChange: function() {
		return this.mixins.groupfield.checkChange.call(this);
	},
	
	getErrors: function(value) {
		return this.mixins.groupfield.getErrors.call(this, value);
	},

	getModelData: function() {
		return this.mixins.groupfield.getModelData.call(this);
	},
	
	getSubmitData: function() {
		return this.mixins.groupfield.getSubmitData.call(this);
	},

	getValue: function() {
		return this.mixins.groupfield.getValue.call(this);
	},

	initField: function() {
		this.mixins.groupfield.initField.call(this);
	},
	
	initValue: function() {
		this.mixins.groupfield.initValue.call(this);
	},
	
	isEqual: function(value1, value2) {
		return this.isEqualAsString(value1, value2);
	},
	
	setValue: function(value) {
		return this.mixins.groupfield.setValue.apply(this, arguments);
	}

});