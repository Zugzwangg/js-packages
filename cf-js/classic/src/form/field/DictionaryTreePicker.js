Ext.define('CFJS.form.field.DictionaryTreePicker', {
	extend		: 'Ext.form.field.Picker',
	xtype		: 'dictionarytreepicker',
	mixins		: [ 'Ext.util.StoreHolder' ],
	requires	: [ 'Ext.data.StoreManager', 'Ext.tree.Panel' ],
	config		: {
		displayField		: 'name',
		closeOnFocusLeave	: true,
		columns				: null,
		maxPickerHeight		: 300,
		minPickerHeight		: 100,
		rootVisible			: false,
		selectOnTab			: true
	},
	editable	: false,
	rawValueData: true,
	triggerCls	: Ext.baseCSSPrefix + 'form-arrow-trigger',

	bindStore: function(store, initial) {
		var me = this;
		me.mixins.storeholder.bindStore.call(me, store, initial);
		store = me.getStore();
		if (!initial && store && !store.isEmptyStore) me.setValue(me.value);
	},

	clearValue: function() {
		this.setValue(null);
	},

	completeEdit: function(e) {
        var me = this;
        me.callParent([e]);
		if (Ext.isEmpty(me.getRawValue())) me.selectItem();
    },
    
	createPicker: function() {
		var me = this,
			autoTree = !me.columns,
			picker = new Ext.tree.Panel({
				baseCls: Ext.baseCSSPrefix + 'boundlist',
				isAutoTree: autoTree,
				columns: me.columns || [{
					xtype		: 'treecolumn',
	                align		: 'left',
					dataIndex	: me.displayField,
					flex		: 1
				}],
				floating: true,
				lines: false,
				minHeight: me.minPickerHeight,
				maxHeight: me.maxPickerHeight,
				rootVisible: me.rootVisible,
				shadow: false,
				shrinkWrapDock: 2,
				store: me.store,
				listeners: {
					scope: me,
					itemclick: me.onItemClick,
					itemkeydown: me.onPickerKeyDown
				}
			});
		if (autoTree) picker.addCls(picker.autoWidthCls);
		if (Ext.isIE9 && Ext.isStrict) {
			picker.getView().on({
				scope: me,
				highlightitem: me.repaintPickerView,
				unhighlightitem: me.repaintPickerView,
				afteritemexpand: me.repaintPickerView,
				afteritemcollapse: me.repaintPickerView
			});
		}
		return picker;
	},
	
	getStoreListeners: function(store) {
		if (store && !store.isEmptyStore) {
			var me = this;
			return {
				scope		: me,
				load		: me.onLoad,
				exception	: me.onException,
				update		: me.onStoreUpdate
			};
        }
    },

    getValue: function() {
		var me = this, value = me.toRecord(me.value, me.getStore()) || me.value;
		if (me.rawValueData && value && value.isNode) value = value.getData();
		return value;
	},

	initComponent: function() {
		var me = this;
		me.bindStore(me.store || 'ext-empty-tree-store', true);
		me.callParent(arguments);
	},

	isEqualAsString: function(value1, value2) {
		if (value1 && value1.isNode) value1 = value1.getData();
		if (value2 && value2.isNode) value2 = value2.getData();
		return JSON.stringify(value1 || '') === JSON.stringify(value2 || '');
	},

	onBindStore: function(store, initial) {
		var me = this, picker = me.picker;
		if (picker && me.picker.store !== store) picker.bindStore(store);
	},

	onException: function() {
		this.collapse();
	},
	
	onExpand: function() {
		var me = this, picker = me.picker, store = me.getStore();
		picker.ensureVisible(me.toRecord(me.value, store) || store.getRoot(), { select: true, focus: true });
	},

	onFocusLeave: function(e) {
		if (this.closeOnFocusLeave) this.callParent([e]);
    },

	onItemClick: function(view, record, node, rowIndex, e) {
		this.selectItem(record);
	},
	
	onLoad: function() {
		var me = this;
		if (me.value) me.setValue(me.value);
	},
    
	onPickerCancel: function () {
        this.collapse();
    },
    
	onPickerKeyDown: function(treeView, record, item, index, e) {
		var key = e.getKey();
		if (key === e.ENTER || (key === e.TAB && this.selectOnTab)) this.selectItem(record);
	},
	
	onStoreUpdate: function(store, rec, type, modifiedFieldNames){
		var me = this, display = me.displayField;
		if (type === 'edit' && modifiedFieldNames && Ext.Array.contains(modifiedFieldNames, display) && me.value === rec.getId()) me.setRawValue(rec.get(display));
    },
	
	onUnbindStore: function() {
		var me = this;
		if (me.picker) me.picker.bindStore(null);
	},
	
	repaintPickerView: function() {
		var style = this.picker.getView().getEl().dom.style;
		style.display = style.display;
	},
	
	selectItem: function(record) {
		var me = this;
		me.setValue(record && record.getData());
        me.fireEvent('select', me, record);
        me.collapse();
    },
   
    setValue: function(value) {
		var me = this, store = me.getStore();
		me.value = value;
		if (store.loading) return me;
		var record = me.toRecord(value, store);
		if (record) {
			me.value = record.getId();
			me.setRawValue(record.get(me.displayField));
		} else me.setRawValue('');
		me.checkChange();
		return me;
    },

    toRecord: function(value, store) {
		var me = this, record;
		if (value && (store = store || me.getStore())) {
			if (value.isNode) record = store.getNodeById(value.id);
			else if (Ext.isNumber(value)) record = store.getNodeById(value);
			else record = Ext.create(store.getModel(), value);
		}
		return record;
	}
 
});