Ext.define('CFJS.form.field.DictionaryTag', {
	extend		: 'Ext.form.field.Tag',
	alias		: ['widget.dictionarytag', 'widget.dictionarytagfield'],
	displayField: 'name',
	minChars	: 0,
	typeAhead	: true,
	valueField	: 'id',

	setValue: function(value, add, skipLoad) {
		var me = this,
			valueStore = me.valueStore,
			valueField = me.valueField,
			Model = valueStore.getModel(),
			record, len, i, valueRecord;
		if (Ext.isObject(value) || Ext.isArray(value)) {
			value = Ext.Array.from(value, true);
			for (i = 0, len = value.length; i < len; i++) {
				record = value[i];
				if (record && !record.isModel) {
					if (!Ext.isObject(record)) {
						record = {};
						record[valueField] = value[i];
					}
					valueRecord = valueStore.findBy(function(rec, id) {
						return rec.get(valueField) === record[valueField];
					});
					if (valueRecord > -1) valueRecord = valueStore.getAt(valueRecord);
					else valueRecord = me.findRecord(valueField, record[valueField]);
					if (!valueRecord) valueRecord = new Model(record);
                    value[i] = valueRecord;
                }
			}
		}
		me.callParent([value, add, skipLoad]);
	},
	
	updateValue: function() {
		var me = this, valueArray = me.valueCollection.getRange(),
			len = valueArray.length, i;
		for (i = 0; i < len; i++) {
			valueArray[i] = valueArray[i].getData();
		}
		me.setHiddenValue(valueArray);
		me.value = me.multiSelect ? valueArray : valueArray[0];
		if (!Ext.isDefined(me.value))  me.value = undefined;
		me.applyMultiselectItemMarkup();
		me.applyAriaListMarkup();
		me.applyAriaSelectedText();
		me.checkChange();
	}

});