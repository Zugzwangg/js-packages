Ext.define('CFJS.form.field.AceField', {
	extend	: 'Ext.form.field.Picker',
	alias	: 'widget.acefield',
	requires: [ 'Ext.window.Window', 'Ext.layout.container.Fit', 'CFJS.form.AceEditor' ],
	config	: {
		editor	: {
			lazy: true,
			$value: {
				fontSize: 12,
				mode	: 'json'
			}
		},
		popup	: {
			lazy: true,
			$value: {
				xtype		: 'window',
				closeAction	: 'hide',
				iconCls		: CFJS.UI.iconCls.FILE_TEXT,
				layout		: 'fit',
				maximizable	: true,
				minHeight	: 250,
				minWidth	: 540,
				resizable	: true
			}
		},
		tools: [{
			type	: 'save',
			tooltip	: 'Update',
			handler	: 'onUpdateValue'
		}]
	},
	triggers: { picker: { cls: Ext.baseCSSPrefix + 'form-fa-trigger', extraCls: CFJS.UI.iconCls.EDIT } },

	alignPicker: function() {
		var me = this, picker = me.getPicker();
		if (picker && !picker.maximized) me.callParent(arguments);
	},
		
	createPicker: function() {
		var me = this,
			popup = me.getPopup(),
			editor = me.getEditor(),
			tools = me.getTools(),
    		i = 0, handler;
		if (tools) {
			if (!Ext.isArray(tools)) buttons = [tools];
			for (; i < tools.length; i++) {
				if (Ext.isString(handler = tools[i].handler)) {
					tools[i] = Ext.apply(tools[i], { handler: me[handler], scope: me });
				}
			}
		}
		editor.mode = me.mode || editor.mode;
		me.pickerWindow = popup = Ext.create(Ext.apply({ tools: tools, title: me.getFieldLabel() || me.getName() }, popup));
		me.aceEditor = editor = popup.add(Ext.apply({ xtype: 'aceeditor', id: me.getId() + '-aceEditor-component' }, editor));
		popup.on({ close: me.onCancelValue, scope: me });
		editor.setValue(me.getValue());
		return me.pickerWindow;
	},
    
    onCancelValue: function () {
    	this.collapse();
    },
    
    onCollapse: function() {
    	var me = this, win = me.pickerWindow;
    	if (win && win.maximized) win.restore();
    },
    
    onExpand: function () {
    	this.aceEditor.setValue(this.getValue());
    },
    
	onUpdateValue: function() {
    	var me = this; 
    	me.setValue(me.aceEditor.getValue());
    	me.collapse();
    },
 
    setValue: function(value) {
    	var me = this;
    	if (me.aceEditor) {
    		me.aceEditor.setValue(value);
    		value = me.aceEditor.getValue();
    	}
    	me.callParent([value]);
    }
    
});
