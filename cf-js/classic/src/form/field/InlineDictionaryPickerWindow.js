Ext.define('CFJS.form.field.InlineDictionaryPickerWindow', {
	extend	: 'CFJS.form.field.DictionaryPickerWindow',
	xtype	: 'window-inline-dictionary-picker',
	config	: {
		actions			: {
			saveRecords: {
				iconCls	: CFJS.UI.iconCls.SAVE,
				title	: 'Записать изменения',
				tooltip	: 'Записать внесенные изменения',
				handler	: 'onSaveRecords'
			}
		},
		actionsDisable	: [ 'selectRecord' ],
		actionsMap		: {
			contextMenu	: { items: [ 'selectRecord', 'addRecord', 'saveRecords' ] },
			header		: { items: [ 'selectRecord', 'addRecord', 'saveRecords' ] }
		}
	},
	
	getSaveParams: Ext.emptyFn,
	
	onSaveRecords: function(component) {
		var me = this, store = me.selector.getStore();
		if (store && store.isStore) {
			component.disable();
			var records = store.getModifiedRecords(), i, record;
			for (i = 0; i < records.length; i++) {
				record = records[i];
				if (me.fireEvent('beforesaverecord', store, record) !== false
						&& (record.dirty || record.phantom)) {
					record.save({ params: me.getSaveParams(record) })
				} else record.reject();
			}
			component.enable();
		}
	}
	
});