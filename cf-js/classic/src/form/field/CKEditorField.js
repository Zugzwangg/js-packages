Ext.define('CFJS.form.field.CKEditorField', {
	extend				: 'Ext.form.field.Base',
	alias				: 'widget.ckeditorfield',
	requires			: [ 'Ext.util.Format', 'Ext.XTemplate' ],
	alternateClassName	: [ 'CFJS.form.CKEditorField' ],
	allowBlank			: true,
	allowOnlyWhitespace	: true,
	ariaRole			: 'textbox',
	blankText			: 'This field is required',
	childEls			: [ 'wrapCt', 'toolbarCt', 'inputWrap', 'inputEl' ],
	config				: {
		alignment		: undefined,
		balloonToolbar	: undefined,
		cloudServices	: null,
		fontFamily		: undefined,
		fontSize		: [ 8, 9, 10, 12, 'default', 16, 18, 20, 22, 24, 26 ],
		heading			: undefined,
		highlight		: undefined,
		image			: null,
		toolbar			: undefined,
		typing			: undefined,
		name			: "content",
		value			: undefined	
	},
    emptyCls			: Ext.baseCSSPrefix + 'form-empty-field',
	emptyText			: '',
	fieldSubTpl			: [
		'<textarea id="{cmpId}-inputEl" data-ref="inputEl" role="presentation">',
			'<tpl if="value">{[Ext.util.Format.htmlEncode(values.value)]}</tpl>',
		'</textarea>',
	],
	height				: 200,
	inputCls			: Ext.baseCSSPrefix + 'form-ckfield-input',
	inputWrapCls		: Ext.baseCSSPrefix + 'form-ckfield-input-wrap',
	maxLength			: Number.MAX_VALUE,
	maxLengthText		: 'The maximum length for this field is {0}',
	minHeight			: 200,
	minLength			: 0,
	minLengthText		: 'The minimum length for this field is {0}',
	preSubTpl			: [
		'<div id="{cmpId}-wrapCt" data-ref="wrapCt" class="{wrapCls}" role="presentation">',
			'<div id="{cmpId}-toolbarCt" data-ref="toolbarCt" class="{toolbarCls}" role="presentation"></div>',
			'<div id="{cmpId}-inputWrap" data-ref="inputWrap" class="{inputWrapCls}" role="presentation">'
	],
	postSubTpl			: [ '</div>', '</div>' ],
	renderConfig		: {
		editable: true
	},
	suspendValueChange	: 0,
	toolbarCls			: Ext.baseCSSPrefix + 'form-ckfield-toolbar',
	validateBlank		: false,
	width				: 200,
	wrapCls				: Ext.baseCSSPrefix + 'form-ckfield',
	
	afterRender: function() {
		this.callParent(arguments);
		this.renderEditor();
	},

	applyAlignment: function(alignment) {
		return Ext.isDefined(alignment) ? Ext.Array.from(alignment) : undefined;
	},

	applyBalloonToolbar: function(balloonToolbar) {
		return Ext.isDefined(balloonToolbar) ? Ext.Array.from(balloonToolbar) : undefined;
	},

	applyFontFamily: function(fontFamily) {
		return Ext.isDefined(fontFamily) ? Ext.Array.from(fontFamily) : undefined;
	},
	
	applyFontSize: function(fontSize) {
		return Ext.isDefined(fontSize) ? Ext.Array.from(fontSize) : undefined;
	},

	applyHeading: function(heading) {
		return Ext.isDefined(heading) ? Ext.Array.from(heading) : undefined;
	},
	
	applyHighlight: function(highlight) {
		return Ext.isDefined(highlight) ? Ext.Array.from(highlight) : undefined;
	},

	applyToolbar: function(toolbar) {
		return Ext.isDefined(toolbar) ? Ext.Array.from(toolbar) : undefined;
	},
	
	doDestroy: function() {
		var me = this;
		if (me.editor) {
			me.bindChangeEvents(false);
			me.editor.destroy();
			me.editor = null;
		}
		me.callParent(arguments);
	},
	
	getRawValue: function() {
		var me = this;
		me.rawValue = me.editor ? me.editor.getData() : Ext.valueFrom(me.rawValue, '');
		return me.rawValue;
    },
    
	getSubTplData: function(fieldData) {
		var me = this;
		return Ext.apply(me.callParent(arguments), {
			inputWrapCls: me.inputWrapCls,
			toolbarCls	: me.toolbarCls,
			wrapCls		: me.wrapCls
		});
	},

	initComponent: function () {
		var me = this, emptyCls = me.emptyCls;
		if (me.allowOnlyWhitespace === false) me.allowBlank = false;
		me.callParent();
		if (me.readOnly) me.setReadOnly(me.readOnly);
		me.addStateEvents('change');
	},

	isEqual: function(value1, value2) {
		return this.isEqualAsString(value1, value2);
	},

	loadFile: function(options) {
		options = options || {};
		var me = this, requestOptions = Ext.apply({ disableExtraParams: true, noCache: false, signParams: false }, options);
		if (me.ownerCt && requestOptions.loadMask !== false) me.ownerCt.setLoading(requestOptions.loadMask);
		requestOptions.callback = function(opts, success, response) {
			if (success) {
				me.setValue(response.responseText);
				me.lastLoadOptions = opts;
			}
			else CFJS.errorAlert('Load file failed', response);
			if (me.ownerCt) me.ownerCt.setLoading(false);
			Ext.callback(options.callback, opts.scope, [opts, success, response]);
		};
		return Ext.Ajax.request(requestOptions);
	},

	onDocumentChange: function(document, deltas) {
		var me = this;
		if (me.editor && deltas && deltas.length > 0) {
			me.suspendValueChange++;
			me.setValue(me.editor.getData());
			me.suspendValueChange--;
		}
	},

	onResize: function(width, height, oldWidth, oldHeight) {
		var me = this;
		if (me.editor) {
			me.wrapCt.setHeight(height);
			me.inputWrap.setHeight(Math.round(me.wrapCt.getHeight(true) - me.toolbarCt.getHeight()));
			me.inputEl.setHeight(Math.round(me.inputWrap.getHeight(true)));
		}
		me.callParent(arguments);
	},
	
	reloadFile: function() {
		return this.loadFile(this.lastLoadOptions);
	},

	renderEditor: function() {
		var me = this, document;
		if (ClassicEditor && !me.editor) {
			ClassicEditor.create(me.inputEl.dom, Ext.apply(me.editorCfg||{}, { language: CFJS.getLocale() }))
				.then(editor => {
					if (me.editor = editor) {
						me.toolbarCt.appendChild(editor.ui.view.toolbar.element);
						me.textAreaEl = me.inputEl;
						me.inputEl = Ext.get(editor.ui.view.element);
						me.inputEl.addCls(me.inputCls);
						me.inputEl.setId(me.id + '-inputEl');
						me.inputEl.dom.setAttribute('data-ref', 'inputEl');
						me.inputEl.dom.setAttribute('role', me.inputEl.dom.getAttribute('role') + ' presentation');
					} 
					me.bindChangeEvents();
					me.onResize(me.getWidth(), me.getHeight());
				})
				.catch(error => { console.error( error ); });
		} else if (me.editor) {
			me.onResize(me.getWidth(), me.getHeight());
		}
		return me.editor;
	},
	
	setRawValue: function(value) {
		var me = this;
		if (!me.transformRawValue.$nullFn) value = me.transformRawValue(value);
		value = Ext.valueFrom(value, '');
		if (me.rawValue === undefined || me.rawValue !== value) {
			me.rawValue = value;
			if (!me.suspendValueChange && me.editor) {
				me.bindChangeEvents(false);
				me.editor.setData(me.rawValue);
				me.bindChangeEvents(true);
			}
		}
		if (me.rendered && me.reference) {
			me.publishState('rawValue', me.value);
		}
		return value;
	},
	
	setReadOnly: function(readOnly) {
		var me = this, old = me.readOnly;
		me.readOnly = !!readOnly;
		if (me.rendered) {
			me.setReadOnlyAttr(me.readOnly || !me.editable);
		} else if (me.rendering) {
			me.setReadOnlyOnBoxReady = true;
		}
		if (readOnly !== old) me.fireEvent('writeablechange', me, me.readOnly);
	},
	
	setReadOnlyAttr: function(readOnly) {
		var editor = this.editor;
		if (editor) editor.isReadOnly = readOnly;
	},
	
	updateAlignment: function(alignment) {
		this.updateEditorConfigOptions('alignment', alignment);
	},
	
	updateBalloonToolbar: function(balloonToolbar) {
		this.updateEditorConfig('balloonToolbar', balloonToolbar);
	},
	
	updateCloudServices: function(cloudServices) {
		this.updateEditorConfig('cloudServices', cloudServices);
	},

	updateEditable: function(editable) {
		this.setReadOnlyAttr(!editable || this.readOnly);
	},

	updateEditorConfig: function(key, value) {
		var cfg = this.editorCfg = this.editorCfg||{};
		if (value !== undefined) cfg[key] = value;
		else delete cfg[key];
	},
	
	updateEditorConfigOptions: function(key, value) {
		if (value !== undefined && value !== null) value = { options: value };
		this.updateEditorConfig(key, value);
	},
	
	updateFontFamily: function(fontFamily) {
		this.updateEditorConfigOptions('fontFamily', fontFamily);
	},

	updateFontSize: function(fontSize) {
		this.updateEditorConfigOptions('fontSize', fontSize);
	},

	updateHeading: function(heading) {
		this.updateEditorConfigOptions('heading', heading);
	},

	updateHighlight: function(highlight) {
		this.updateEditorConfigOptions('highlight', highlight);
	},
	
	updateImage: function(image) {
		this.updateEditorConfig('image', image);
	},
	
	updateToolbar: function(toolbar) {
		if (toolbar !== undefined && toolbar !== null) toolbar = { items: toolbar };
		this.updateEditorConfig('toolbar', toolbar);
	},

	updateTyping: function(typing) {
		this.updateEditorConfig('typing', typing);
	},

	privates: {
		
		bindChangeEvents: function(active) {
			var me = this, editor = me.editor,
				method = active === 'false' ? 'off' : 'on',
				document = editor ? editor.model.document : null;
			if (document) {
				document[method]('change', function(eventInfo, batch) {
					me.onDocumentChange(document, batch && batch.deltas);
				});
			}
		}

	}
}, function() {
	CFJS.loadLibraries([
		'/scripts/ckeditor/ckeditor.js',
		'/scripts/ckeditor/translations/' + CFJS.getLocale() + '.js'
	]);
});
