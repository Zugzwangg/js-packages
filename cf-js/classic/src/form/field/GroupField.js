Ext.define('CFJS.form.field.GroupField', {
	extend				: 'Ext.form.field.Field',
    mixinId				: 'groupfield',
	combineErrors		: true,
	config				: { format: null, formatOffset: 0, separator: ';' },
	defaultBindProperty	: 'value',
	isGroupField		: true,
	publishes			: [ 'format', 'value' ],
	safeParseInt		: CFJS.safeParseInt,
	twoWayBindable		: [ 'format', 'value' ],
	validateOnChange	: false,
	
	applyFormat: function(format) {
		if (Ext.isString(format)) format = format.split(this.getSeparator() || ';');
		return Ext.Array.from(format);
	},
	
	applyFormatOffset: function(formatOffset) {
		return this.safeParseInt(formatOffset);
	},

    getErrors: function(value) {
        var me = this,
        	errors = me.callParent([value]),
        	validator = me.validator, msg;
        if (Ext.isFunction(validator)) {
            msg = validator.call(me, value);
            if (msg !== true) {
                errors.push(msg);
            }
        }
        return errors;
    },

	getInputValue: Ext.identityFn,

	getMaxValue: function() {
		return (this.getFormat()||[]).length + this.getMinValue();
	},
	
	getMinValue: function() {
		return this.getFormatOffset();
	},
	
	getValue: function() {
		var me = this, boxes = me.getBoxes(), i = 0, b, value;
		for (; i < boxes.length; i++) {
			b = boxes[i];
			if (b.getValue()) {
				value = (value || 0) + me.safeParseInt(b.inputValue);
				if (me.isRadioGroup) break;
			}
		}
		me.value = value;
		return value;
	},

	initField: function() {
		var me = this, i = 0,
			offset = me.getFormatOffset(),
			format = me.getFormat() || [], label;
		if (me.items && me.items.isMixedCollection) me.removeAll();
		else me.initItems();
		for (; i < format.length; i++) {
			label = format[i];
			if (Ext.isObject(label)) label = label.fieldLabel || label.boxLabel || label.title || label.text || label.name;
			if (Ext.isEmpty(label)) continue;
			me.add({
				boxLabel	: label,
				excludeForm	: true,
				inputValue	: me.getInputValue(i) + offset,
				name		: me.name,
				readOnly	: me.readOnly
			});
		}
		me.callParent();
	},
	
    initValue: function() {
		var me = this, valueCfg = me.value;
		me.originalValue = me.lastValue = valueCfg || me.getValue();
		if (valueCfg && me.getFormat()) {
			me.setValue(valueCfg, true);
		}
    },
	
	setValue: function(value, force) {
		var me = this, maxValue = me.getMaxValue(),
			i = 0, boxes, mask, cb;
		value = me.safeParseInt(Ext.valueFrom(value, 0));
		if (value > maxValue) value = value & maxValue;
		if (force || me.value !== value) {
			boxes = me.getBoxes();
			me.batchChanges(function() {
				Ext.suspendLayouts();
				for (; i < boxes.length; i++) {
					cb = boxes[i];
					mask = me.safeParseInt(cb.inputValue);
					cb.setValue((value & mask) == mask);
				}
				Ext.resumeLayouts(true);
			});
		}
		return me.callParent();
	},
	
	validate: function() {
        var me = this,
            errors,
            isValid,
            wasValid;
        if (me.disabled) {
            isValid = true;
        } else {
            errors = me.getErrors();
            isValid = Ext.isEmpty(errors);
            wasValid = me.wasValid;
            if (isValid) {
                me.unsetActiveError();
            } else {
                me.setActiveError(errors);
            }
        }
        if (isValid !== wasValid) {
            me.wasValid = isValid;
            me.fireEvent('validitychange', me, isValid);
            me.updateLayout();
        }
        return isValid;
    },
    
	updateFormat: function(format) {
		if (!this.destroyed) this.initField();
	}

});