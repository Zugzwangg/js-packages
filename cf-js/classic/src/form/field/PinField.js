Ext.define('CFJS.form.field.PinField', {
	extend				: 'Ext.form.FieldContainer',
	xtype				: 'pinfield',
	mixins				: [ 'Ext.form.field.Field' ],
	requires			: [ 'Ext.form.field.Text' ],
	allowBlank			: true,
	config				: {
		itemsOffset	: 5,
		pinLength	: 0
	},
	combineErrors		: true,
	defaultBindProperty	: 'value',
	defaultType			: 'textfield',
	layout				: { type: 'hbox', align: 'stretch' },
	readOnly			: false,
	
	clearValue: function(){
		this.setValue(null);    
	},
	
    createFields: function(count) {
		var me = this,
			itemConfig = Ext.apply({
				enforceMaxLength: true,
				flex			: 1,
				hideLabel		: true,
				inputType		: 'password',
				maskRe			: /[0-9]/,
				maxLength		: 1,
				selectOnFocus	: true
			}, me.itemConfig),
			i = 0, items = [], item;
		if (me.combineErrors) itemConfig.msgTarget = 'none';
		for (; i < count; i++) {
			item = Ext.apply({}, {
				allowBlank	: me.allowBlank,
				digitNumber	: i,
				margin		: '0 0 0 ' + (i && me.itemsOffset),
				name		: 'pin' + i
			}, itemConfig);
			items.push(item);
		}
		return items;
	},
	
    fieldChange: function(field) {
    	var me = this, fields = me.getFields().items,
    		next = field.digitNumber + 1;
    	if (field.isValid() && next > 0 && next < fields.length ) fields[next].focus();
    	me.checkChange();
    },
    
    getErrors: function() {
        var me = this, errors = [],
        	validator = me.validator, msg;
        if (Ext.isFunction(validator)) {
            msg = validator.call(me, me.getValue());
            if (msg !== true) {
                errors.push(msg);
            }
        }
        return errors;
    },
	
	getFields: function() {
		return this.monitor.getItems();
	},

	getSubmitData: function() {
		var me = this,
			data = null,
			val;
		if (!me.disabled && me.submitValue) {
			val = me.getSubmitValue();
			if (val !== null) {
				data = {};
				data[me.getName()] = val;
			}
		}
		return data;
	},

	getSubmitValue: function() {
		return this.getValue();
	},

	getValue: function() {
		var me = this, fields = me.getFields().items, 
			f = 0, value = [];
		for (; f < fields.length; f++) {
			value.push(fields[f].isValid() ? fields[f].getValue() : '');
		}
		me.value = value.join('');
		return me.value;
	},

	initComponent: function() {
		var me = this;
		me.name = me.name || me.id;
		me.items = me.createFields(me.pinLength || 0);
		me.callParent();
		me.initField();
	},
	
	initValue: function() {
		var me = this, valueCfg = me.value;
		me.originalValue = me.lastValue = valueCfg || me.getValue();
		if (valueCfg) me.setValue(valueCfg);
	},
	
	isDirty: function(){
		var me = this, fields = me.getFields().items, f = 0;
		for (; f < fields.length; f++) {
			if (fields[f].isDirty()) return true;
		}
    },
    
	onAdd: function(field) {
		var me = this;
		if (field.isFormField) me.mon(field, 'change', me.fieldChange, me);
        me.callParent(arguments);
    },
 
    onRemove: function(field) {
        var me = this;
        if (field.isFormField) me.mun(field, 'change', me.fieldChange, me);
        me.callParent(arguments);
    },
    
	setReadOnly: function(readOnly) {
		var me = this, fields = me.getFields().items, f = 0;
		for (; f < fields.length; f++) {
			fields[f].setReadOnly(readOnly);
		}
		me.readOnly = readOnly;
	},
	
	reset: function() {
        var me = this, 
        	hadError = me.hasActiveError(), 
        	preventMark = me.preventMark,
        	fields = me.getFields().items, f = 0;
        me.preventMark = true;
        me.batchChanges(function() {
            for (; f < fields.length; f++) {
            	fields[f].reset();
            }
        });
        me.preventMark = preventMark;
        me.unsetActiveError();
        if (hadError) me.updateLayout();
    },
	
    resetOriginalValue: function(){
        var me = this, fields = me.getFields().items, f = 0;
        for (; f < fields.length; f++) {
        	fields[f].resetOriginalValue();
        }
        me.originalValue = me.getValue();
        me.checkDirty();
    },
    
	setValue: function(value) {
		var me = this, fields = me.getFields().items, 
			i = 0, items = [];
		me.batchChanges(function() {
			Ext.suspendLayouts();
			if (Ext.isArray(value)) value = value.join('');
			if (!Ext.isString(value)) value = '';
			for (; i < fields.length; i++) {
				fields[i].setValue(i < value.length && Ext.isNumeric(value[i]) ? value[i] : null);
				items.push(fields[i].getValue());
			}
			Ext.resumeLayouts(true);
		});
		return me.mixins.field.setValue.call(me, items.join(''));
	}

});