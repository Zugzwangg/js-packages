Ext.define('CFJS.container.FileUpload', {
	extend		: 'Ext.container.Container',
	alias		: 'widget.fileupload',
	requires	: [
		'Ext.Img',
		'Ext.form.Panel',
		'Ext.form.field.File',
		'Ext.layout.container.Table',
		'Ext.toolbar.TextItem',
		'CFJS.plugin.FileDropZone'
	],
	baseCls		: Ext.baseCSSPrefix + 'file-upload',
	buttonText	: 'Browse',
	layout		: 'fit',
	padding		: '15 10 10 10',
	plugins		: [{ ptype: 'filedrop' }],
	title		: 'Choose a file or drag it here',
	uploadImgUrl: 'images/cloud-upload-128x128.gif',
	uploadText	: 'Choose a file or drag it here',

	createForm: function(config) {
		var me = this,
			style = { borderRadius: '10px', borderStyle: 'dashed', borderWidth: '2px' },
			fileField = CFJS.apply({
				xtype		: 'filefield',
				buttonConfig: { width: 100 },
				buttonOnly	: true,
				buttonText	: me.buttonText,
				listeners	: { change: me.onFileChange, scope: me },
				margin		: 0,
				name		: 'upload',
				width		: 100
			}, me.fileField);
		if (!me.header) style.borderRadius = '10px';
		me.form = Ext.create(CFJS.apply({
			xtype		: 'form',
			header		: me.header,
			iconCls		: me.iconCls,
			items		: [{
				xtype		: 'container',
				cls			: 'upload-img',
				items		: {
					xtype	: 'container',
					itemId	: 'table',
					items	: [{
						xtype	: 'image',
						alt		: 'upload-img',
						cellCls	: 'image-cell',
						colspan	: 2,
						height	: 128,
						src		: '<shared@cf-js>' + me.uploadImgUrl,
						width	: 128
					},{
						xtype	: 'tbtext',
						cellCls	: 'file-input-label',
						html	: me.uploadText
					}, fileField],
					layout	: {
						type	: 'table',
						columns	: 2,
						tdAttrs	: { style: 'vertical-align:middle;padding:0 5px 5px 5px;' }
					},
					padding	: 10
				},
				layout		: 'center',
				scrollable	: me.scrollable,
				style		: style
			}],
			layout		: 'fit',
			method		: 'POST',
			title		: me.title
		}, config));
		delete me.scrollable;
		return me.form; 
	},
	
	initComponent: function() {
		var me = this,
			listen = { destroyable: true, scope: me, dragover: me.onFileDragOver },
			dropZone;
		me.items = me.createForm(me.formConfig);
		me.callParent();
		dropZone = me.findPlugin('filedrop');
		if (Ext.isDefined(window.FormData)) {
			if (dropZone.readFiles) {
				Ext.apply(listen, {
					beforeload	: me.beforeFileLoad,
					load		: me.onFileLoad,
					loadabort	: me.onFileLoadAbort,
					loadend		: me.onFileLoadEnd,
					loaderror	: me.onFileLoadError,
					loadstart	: me.onFileLoadStart
				});
			} else listen.drop = me.onFileDrop;
		}
		me.on(listen);
	},

	afterRender: function() {
		var me = this;
		me.dropEl = me.getEl().selectNode('.upload-img', false);
		me.callParent(arguments);
	},
	
	// template function for before load event
	beforeFileLoad	: Ext.emptyFn,

	onFileChange: function(field, value) {
		var files = field.fileInputEl.dom.files;
		if (files.length > 0) this.uploadFile(files[0]);
	},
	
	// template function for drag over event
	onFileDragOver	: Ext.emptyFn,

	onFileDrop: function(me, e) {
		var files = e.browserEvent.dataTransfer.files;
		if (files.length > 0) me.uploadFile(files[0]);
	},

	onFileLoad: function(me, e, file) {
		file.data = e.target.result;
		me.uploadFile(file);
	},
	
	// template function for file load abort event
	onFileLoadAbort	: Ext.emptyFn,
	// template function for file load end event
	onFileLoadEnd	: Ext.emptyFn,
	// template function for file load error event
	onFileLoadError	: Ext.emptyFn,
	// template function for file load start event
	onFileLoadStart	: Ext.emptyFn,

	uploadFile: function(file) {
		console.log(file);
	}

});