Ext.define('CFJS.container.DesktopWrap', {
	extend		: 'Ext.container.Container',
	xtype		: 'desktopwrap',
	requires 	: [
		'Ext.layout.container.Card',
		'Ext.layout.container.HBox', 
		'Ext.list.*',
	],
	layout		: {
		type			: 'hbox',
		align			: 'stretch',
		animate			: true,
		animatePolicy	: { x: true, width: true }
	},
	navigatorConfig: {
		xtype			: 'treelist',
		expanderFirst	: false,
		expanderOnly	: false,
		highlightPath	: true,
		itemId			: 'navigator',
		listeners		: { selectionchange: 'onNavigationSelectionChange' },
		publishes		: [ 'selection', 'micro' ],
		scrollable		: 'y',
		ui				: 'navigation',
		width			: 250
	},
	
	beforeLayout : function() {
		var me = this,
			height = Ext.Element.getViewportHeight() - 64,
			holder = me.lookupReferenceHolder() || me.ownerCt || me,
			logotype = holder.lookup('logotype'),
			navigator = me.navigator; 
		me.minHeight = height;
		if (navigator) {
			navigator.setStyle({ 'min-height': height + 'px' });
			if (logotype) navigator.setWidth(logotype.width);
		}
		me.callParent(arguments);
	},
	
	initComponent: function() {
		var me = this;
		me.items = [];
		if (!CFJS.isFrame && me.navigatorConfig) me.items.push(me.navigator = Ext.create(me.navigatorConfig));
		me.items.push(me.contentPanel = Ext.create(Ext.apply({
			xtype		: 'container',
			flex		: 1,
			itemId		: 'contentPanel',
			layout		: { type: 'card', anchor: '100%', deferredRender: true }
		}, me.desktopConfig || {})));
		me.callParent();
	},
	
	getContentPanel: function() {
		return this.contentPanel;
	},

	getNavigator: function() {
		return this.navigator;
	}
	
});
