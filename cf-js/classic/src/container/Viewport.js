Ext.define('CFJS.container.Viewport', {
	extend				: 'Ext.container.Viewport',
	alternateClassName	: 'CFJS.Viewport',
	xtype				: 'mainviewport',
	requires 			: [ 
		'Ext.button.Button',
		'Ext.layout.container.VBox',
		'Ext.toolbar.Fill',
		'Ext.toolbar.Toolbar',
		'Ext.window.Toast',
		'CFJS.container.DesktopWrap',
		'CFJS.pages.Blank',
		'CFJS.pages.Error*',
		'CFJS.pages.Home',
		'CFJS.pages.LockScreen',
		'CFJS.view.ViewportController',
		'CFJS.view.ViewportModel'
	],
	baseCls				: Ext.baseCSSPrefix + 'viewport',
	controller			: 'viewport',
	defaultType			: 'container',
	itemId				: 'mainView',
	layout				: { type: 'vbox', align: 'stretch' },
	logoConfig			: {
		xtype		: 'component',
		autoEl		: { tag: 'a', href: '#home', target: '_self' },
		bind		: { html: '{companyLogo}' },
		cls			: 'company-logo ' + (CFJS.isFrame ? ' in-frame' : ''),
		reference	: 'logotype',
		publishes	: 'width'
	},
	logoWidth			: CFJS.isFrame ? 64 : 250,
	toolbarConfig		: {
		cls		: 'viewport-headerbar toolbar-btn-shadow buttons-delete-focus',
		height	: 64,
		itemId	: 'headerBar'
	},
	viewModel			: { type: 'viewport' },
	
	getContentPanel: function() {
		return this.getDesktopWrap().getContentPanel();
	},
	
	getDesktopWrap: function() {
		return this.desktopWrap;
	},
	
	getNavigator: function() {
		return this.getDesktopWrap().getNavigator();
	},
	
	getToolBar: function() {
		return this.toolbar;
	},

	initComponent: function() {
		var me = this;
		me.initDesktop(Ext.app.route.Router.application);
		me.callParent();
		me.desktopWrap = Ext.getCmp('viewport-detail-wrap');
	},
	
	initDesktop: function(app) {
		var me = this; 
		me.desktopWrap = Ext.apply({
			xtype		: 'desktopwrap',
			id			: 'viewport-detail-wrap',
			flex		: 1
		},  me.desktopConfig);
		me.toolbar = new Ext.toolbar.Toolbar(me.toolbarConfig);
		me.setToolBarItems();
		me.items = [me.toolbar, me.desktopWrap ];
		return me;
	},

	initToolBarItems: function(items) {
		var me = this,
			logo = Ext.apply({}, { width: me.logoWidth }, me.logoConfig),
			toggle = {
				action		: 'navigation-size',
				handler		: 'onToggleNavigationSize',
				hidden		: CFJS.isFrame,
				iconCls		: CFJS.UI.iconCls.NAVICON,
				id			: 'main-navigation-btn',
				margin		: '0 0 0 8'
			};
		return [ logo, toggle, '->'].concat(items||[]);
	},
	
	setToolBarItems: function(items) {
		var me = this, toolbar = me.getToolBar();
		if (toolbar.rendered) Ext.suspendLayouts();
		toolbar.removeAll();
		toolbar.add(me.initToolBarItems(items));
		if (toolbar.rendered) Ext.resumeLayouts(true);
	}
	
});
