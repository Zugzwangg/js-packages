Ext.define('CFJS.container.ImageUpload', {
	extend				: 'CFJS.container.FileUpload',
	alias				: 'widget.imageupload',
	config				: { fileType: null, filePath: null, service: 'resources', src: null },
	defaultBindProperty	: 'src',
	iconCls				: CFJS.UI.iconCls.IMAGE,
	publishes			: 'src',
	twoWayBindable		: 'src',
	
    applyService: function(service) {
		var me = this, api = CFJS.Api;
		if (Ext.isString(service)) service = api.controller(service);
		return service || api.controller(api.getService());
	},
	
	applySrc: function(src) {
		return src || '<shared@cf-js>' + this.uploadImgUrl;
	},

	onFileChange: function(field, value) {
		this.submitForm(this.form);
	},

	onFileDrop: function(me, e) {
		var files = e.browserEvent.dataTransfer.files,
			formData = new FormData();
		if (files.length > 0) {
			formData.append('upload', files[0], files[0].name);
			me.uploadFormData(formData);
		}
	},

	onFileLoad: function(me, e, file) {
		file.data = e.target.result;
	},

	submitForm: function(form) {
		var me = this;
		if (form && form.isValid()) {
			form.submit({
				url			: me.service.url,
				extraParams	: me.uploadParams(),
				errorReader	: { type: 'json.cfapi', rootProperty: 'errors' },
				waitMsg		: me.waitMsg,
				success		: function(form, action) { Ext.callback(me.afterUpload, me, [action.response, form, true]); },
				failure		: function(form, action) { Ext.callback(me.afterUpload, me, [action.response, form, false]); }
			});
		}
	},

	updateSrc: function(src) {
		var image = this.form.down('image[alt="upload-img"]');
		if (image) image.setSrc(src);
	},
	
	uploadFormData: function(formData) {
		var me = this;
		CFJS.Api.request({
			url		: me.service.url,
			headers	: { 'Content-Type': undefined },
			method	: 'POST',
			params	: me.uploadParams(),
			rawData	: formData,
			callback: function(options, success, response) {
				me.setLoading(false);
				Ext.callback(me.afterUpload, me, [response, options, success]);
			}
		});
	},

	privates: {
		
		uploadParams: function() {
			var me = this,
				params = {
					method: [ me.service.method, 'upload'].join('.'),
					path	: me.getFilePath(),
					type	: me.getFileType()
				};
			if (Ext.isEmpty(params.type)) delete params.type;
			if (Ext.isEmpty(params.path)) delete params.path;
			return params;
		}

	}

});