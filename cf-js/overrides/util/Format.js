Ext.applyIf(Ext.Number, {
	numArabic	: [1,4,5,9,10,40,50,90,100,400,500,900,1000,4000,5000,9000,10000],
	numRomam	: ['I','IV','V','IX','X','XL','L','XC','C','CD','D','CM','M','M&#8577;','&#8577;','&#8577;&#8578;','&#8578;'],
	toRoman		: function (arabic) {
		var me = Ext.Number,
			i = me.numArabic.length - 1,
			roman = '';
		if (arabic) {
			while (arabic > 0) {
				if (arabic >= me.numArabic[i]) {
					roman += me.numRomam[i];
					arabic -= me.numArabic[i];
				} else i--;
			}
		}
		return roman;
	},
	
	toArabic	: function(roman) {
		var me = Ext.Number,
			numRomam = me.numRomam,
			i = me.numArabic.length - 1,
			pos = 0,
			arabic = 0;
		roman = roman.toUpperCase();
		while (i >= 0 && pos < roman.length) {
			if (roman.substr(pos, numRomam[i].length) == numRomam[i]) {
				arabic += me.numArabic[i];
		        pos += numRomam[i].length;
			} else i--;
		}
		return arabic;
	}
});
Ext.applyIf(Ext.Date, {
	
	patterns			: {
		FullDateTime				: 'l, F d, Y g:i:s A',
		ISO8601Long					: 'Y-m-d H:i:s',
		ISO8601Short				: 'Y-m-d',
		LongDate					: 'l, F d, Y',
		LongDateTime				: 'Y/m/d h:i:s A',
		LongTime					: 'g:i:s A',
		MonthDay					: 'F d',
		NormalDate					: 'm/d/Y',
		ShortDate					: 'n/j/Y',
		ShortTime					: 'g:i A',
		SortableDateTime			: 'Y-m-d\\TH:i:s',
		UniversalSortableDateTime	: 'Y-m-d H:i:sO',
		YearMonth					: 'F, Y'
	},
	
	extDateFormats		: {},
	
	extFormatMapping	: {
		j: 'd',
		d: 'dd',
		z: 'D',
		o: 'y',
		y: 'yy',
		Y: 'yyyy',
		n: 'M',
		m: 'MM',
		M: 'MMM',
		F: 'MMMM',
		W: 'ww',
		l: 'EEEE',
		N: 'u',
		a: 'A',
		A: 'A',
		G: 'H',
		H: 'HH',
		g: 'h',
		h: 'hh',
		i: 'mm',
		s: 'ss',
		u: 'SSS',
		T: 'z',
		O: 'Z',
		P: 'XXX'
	},
	
	javaDateFormats		: {},
	
	javaFormatMapping	: {
		d		: 'j',
		dd		: 'd',
		D		: 'z',
		y		: 'o',
		yy		: 'y',
		yyy		: 'y',
		yyyy	: 'Y',
		M		: 'n',
		MM		: 'm',
		MMM		: 'M',
		MMMM	: 'F',
		MMMMM	: 'F',
        w		: 'W',
        ww		: 'W',
        E		: 'D',
        EE		: 'D',
        EEE		: 'D',
        EEEE	: 'l',
        EEEEE	: 'l',
        EEEEEE	: 'l',
        u		: 'N',
		a		: 'a',
		A		: 'A',
		H		: 'G',
		HH		: 'H',
		h		: 'g',
		hh		: 'h',
		m		: 'i',
		mm		: 'i',
		s		: 's',
		ss		: 's',
		S		: 'u',
		SS		: 'u',
		SSS		: 'u',
		z		: 'T',
		zz		: 'T',
		zzz		: 'T',
		zzzz	: 'TP',
		Z		: 'O',
		X		: 'P',
		XX		: 'O',
		XXX		: 'P'
	},
	
	unitNames			: {
		day		: 'Day',
		hour	: 'Hour',
		minute	: 'Minute',
		month	: 'Month',
		second	: 'Second',
		week	: 'Week',
		year	: 'Year',
	},
	
	addComplex: function(dt, o) {
		if (!o) return dt;
		var me = Ext.Date,
			newDt = me.clone(dt);
		if (o.years) newDt = me.add(newDt, me.YEAR, o.years);
		if (o.months) newDt = me.add(newDt, me.MONTH, o.months);
		if (o.weeks) o.days = (o.days || 0) + (o.weeks * 7);
		if (o.days) newDt = me.add(newDt, me.DAY, o.days);
		if (o.hours) newDt = me.add(newDt, me.HOUR, o.hours);
		if (o.minutes) newDt = me.add(newDt, me.MINUTE, o.minutes);
		if (o.seconds) newDt = me.add(newDt, me.SECOND, o.seconds);
		if (o.millis) newDt = me.add(newDt, me.MILLI, o.millis);
		return o.clearTime ? me.clearTime(newDt) : newDt;
	},
	
	after: function(dt1, dt2, precise) {
		return Ext.isDate(dt1) && Ext.isDate(dt2) && Ext.Date.compare(dt1, dt2, precise) < 0;
	},
	
	before: function(dt1, dt2, precise) {
		return Ext.isDate(dt1) && Ext.isDate(dt2) && Ext.Date.compare(dt1, dt2, precise) > 0;
	},
	
	compare: function(dt1, dt2, precise) {
		if (precise !== true) {
			dt1 = Ext.Date.clone(dt1);
			dt1.setMilliseconds(0);
			dt2 = Ext.Date.clone(dt2);
			dt2.setMilliseconds(0);
		}
		return dt2.getTime() - dt1.getTime();
	},

	copyTime: function(fromDt, toDt) {
		var dt = Ext.Date.clone(toDt);
		dt.setHours(
			fromDt.getHours(),
			fromDt.getMinutes(),
			fromDt.getSeconds(),
			fromDt.getMilliseconds());
		return dt;
	},

	diffDays: function(start, end) {
		var me = Ext.Date,
			day = 1000 * 60 * 60 * 24,
			diff = me.clearTime(end, true).getTime() - me.clearTime(start, true).getTime();
		return Math.ceil(diff / day);
	},
	
	elapsed: function(date1, date2) {
		var time1 = (date1 || new Date()).getTime(),
			time2 = (date2 || new Date()).getTime(),
			seconds = Math.floor((time2 - time1) / 1000),
			minutes = Math.floor(seconds / 60),
			hours = Math.floor(minutes / 60),
			days = Math.floor(hours / 24),
			weeks = Math.floor(days / 7),
			months = Math.floor(days / 30),
			years = Math.floor(days / 365),
			inflector = Ext.util.Inflector,
			units = Ext.Date.unitNames,
			part = function(value, singular, gap) {
	            return value ? (gap || '') +  value + ' ' + (value === 1 ? singular : Ext.util.Inflector.pluralize(singular)) : '';
	        },
			ret;
		months %= 12;
		weeks %= 52;
		days %= 365;
		hours %= 24;
		minutes %= 60;
		seconds %= 60;

		if (years) {
			ret = part(years, units.year);
			ret += part(months, units.month, ' ');
		} else if (months) {
			ret = part(months, units.month);
			ret += part(days, units.day, ' ');
		} else if (weeks) {
			ret = part(weeks, units.week);
			ret += part(days, units.day, ' ');
		} else if (days) {
			ret = part(days, units.day);
			ret += part(hours, units.hour, ' ');
		} else if (hours) {
			ret = part(hours, units.hour);
		} else if (minutes) {
			ret = part(minutes, units.minute);
		} else {
			ret = part(seconds, units.second);
		}
        return ret;

	},

	extToJavaFormat: function(formatString) {
		return Ext.Date.translateFormatString(formatString, 'ext');
	},
	
	getFirstDateOfYear: function(date) {
		return new Date(date.getFullYear(), 0, 1);
	},
	
	isMidnight: function(dt) {
		return dt.getHours() === 0 &&
			dt.getMinutes() === 0 &&
			dt.getSeconds() === 0 && 
			dt.getMilliseconds() === 0;    
	},

	javaToExtFormat: function(formatString) {
		return Ext.Date.translateFormatString(formatString, 'java');
	},
	
	max: function() {
		return Ext.Date.maxOrMin.apply(this, [true, arguments]);
	},

    // private helper fn
	maxOrMin: function(max) {
		var dt = (max ? 0: Number.MAX_VALUE),
			i = 0,
			args = arguments[1],
			ln = args.length;
		for (; i < ln; i++) {
			dt = Math[max ? 'max': 'min'](dt, args[i].getTime());
		}
		return new Date(dt);
	},

	min: function() {
		return Ext.Date.maxOrMin.apply(this, [false, arguments]);
	},
    
	setEndOfDate: function(date) {
		var dt = new Date(date);
		dt.setHours(23, 59, 59, 999);
		return dt;
	},
	
	today: function() {
		return Ext.Date.clearTime(new Date());
	},

	translateFormatString: function(formatString, toType) {
		if (Ext.isEmpty(formatString)) return formatString;
		var me = Ext.Date, dateFormats = me[toType + 'DateFormats'];
		if (!dateFormats[formatString]) 
			dateFormats[formatString] = Ext.Date.translateFormat(formatString, me[toType + 'FormatMapping']);
		return dateFormats[formatString];
	},
	
	translateFormat: function(formatString, mapping) {
		var parts = formatString ? formatString.split(/\W+/) : [],
			result = '', lastPos = 0, i = 0, part, pos;
		for (; i < parts.length; i++) {
			part = parts[i];
			pos = formatString.indexOf(part);
			result += formatString.substring(lastPos, pos) + (mapping[part] || part);
			lastPos = pos + part.length;
		}
		return result;
	}
});
/**
 * @class CFJS.util.Format
 * @singleton
 */
Ext.define('CFJS.overrides.util.Format', {
	override	: 'Ext.util.Format',
	singleton 	: true,
	requires	: [
		'Ext.data.StoreManager',
		'Ext.data.TreeStore',
		'CFJS.store.TicketCategories',
		'CFJS.store.airline.AirlineTicketTypes',
		'CFJS.store.communion.MessagePriorities',
		'CFJS.store.dictionary.CompanyCustomerTypes',
		'CFJS.store.dictionary.CustomerTypes',
		'CFJS.store.dictionary.GenderTypes',
		'CFJS.store.dictionary.PostTypes',
		'CFJS.store.dictionary.SupplierTypes',
		'CFJS.store.financial.OrderStates',
		'CFJS.store.financial.PayingTypes',
		'CFJS.store.financial.ServiceTypes',
		'CFJS.store.railway.RailwayTicketTypes'
	],
	
	translitTypes: function(locale) {
		var map = { 'DIACRITIC': 1, 'BY': 2, 'BG': 3, 'MK':4, 'RU': 5, 'UK': 6 };
		return map[(locale || '').toUpperCase()] || 0;
	},
	
	// Map for translit operation by ISO 9:1995 
	translitMap	: {
		//	Name - cyrilic
		//	0 - for all
		//	1 - diacritic
		//	2 - BY|BLR
		//	3 - BG|BGR
		//	4 - MK|MKD
		//	5 - RU|RUS
		//	6 - UA|UKR
		/*-Name-----0-,----1-,-------2-,-----3-,-----4-,-----5-,-----6--*/
		'\u2116': ['#'],													// '№'
		'\u2019': ['\'','\u02BC'],											// '’'
		'\u0430': ['a'],													// 'а'
		'\u0431': ['b'],													// 'б'
		'\u0432': ['v'],													// 'в'
		'\u0433': ['g',	'',			'',		'',		'',		'',		'h'],	// 'г'
		'\u0491': ['',	'g\u0300',	'',		'',		'',		'',		'g'],	// 'ґ'
		'\u0434': ['d'],													// 'д'
		'\u0435': ['e'],													// 'е'
		'\u0451': ['',	'\u00EB',	'yo',	'',		'',		'yo',	''],	// 'ё'
		'\u0454': ['',	'\u00EA',	'',		'',		'',		'',		'ye'],	// 'є'
		'\u0436': ['zh','\u017E'],											// 'ж'
		'\u0437': ['z'],													// 'з'
		'\u0438': ['',	'i',		'',		'i',	'i',	'i',	'y'],	// 'и'
		'\u0456': ['',	'\u00EC',	'i',	'i`',	'',		'i`',	'i'],	// 'і'
		'\u0457': ['',	'\u00EF',	'',		'',		'',		'',		'yi'],	// 'ї'
		'\u0439': ['',	'j',		'j',	'j',	'',		'j',	'y'],	// 'й'
		'\u043A': ['k'],													// 'к'
		'\u043B': ['l'],													// 'л'
		'\u043C': ['m'],													// 'м'
		'\u043D': ['n'],													// 'н'
		'\u043E': ['o'],													// 'о'
		'\u043F': ['p'],													// 'п'
		'\u0440': ['r'],													// 'р'
		'\u0441': ['s'],													// 'с'
		'\u0442': ['t'],													// 'т'
		'\u0443': ['u'],													// 'у'
		'\u0444': ['f'],													// 'ф'
		'\u0445': ['x',	'h',		'',		'',		'',		'',		'kh'],	// 'х'
		'\u0446': ['cz','c',		'',		'',		'',		'',		'ts'],	// 'ц'
		'\u0447': ['ch','\u010D'],											// 'ч'
		'\u0448': ['sh','\u0161'],											// 'ш'
		'\u0449': ['',	'\u015D',	'',		'sth',	'',		'shh',	'shch'],// 'щ'
		'\u044E': ['',	'\u00FB',	'yu',	'yu',	'',		'yu',	'yu'],	// 'ю'
		'\u044F': ['',	'\u00E2',	'ya',	'ya',	'',		'ya',	'ya'],	// 'я'
		'\u044C': ['',	'\u02B9',	'`',	'`',	'',		'`',	'`'],	// 'ь'
		'\u044A': ['',	'\u02BA',	'',		'a`',	'',		'``'],			// 'ъ'
		'\u044B': ['',	'y',		'y`',	'',		'',		'y`'],			// 'ы'
		'\u044D': ['',	'\u00E8',	'e`',	'',		'',		'e`'],			// 'э'
		'\u0453': ['',	'\u01F5',	'',		'',		'g`'],					// 'ѓ'
		'\u0463': ['',	'\u011B',	'',		'ye',	'',		'ye'],			//  ять
		'\u0473': ['',	'f\u0300',	'',		'fh',	'',		'fh'],			//  фита
		'\u045F': ['',	'd\u0302',	'',		'',		'dh'],					// 'џ'
		'\u0455': ['',	'\u1E91',	'',		'',		'z`'],					// 'ѕ'
		'\u045C': ['',	'\u1E31',	'',		'',		'k`'],					// 'ќ'
		'\u0459': ['',	'l\u0302',	'',		'',		'l`'],					// 'љ'
		'\u045A': ['',	'n\u0302',	'',		'',		'n`'],					// 'њ'
		'\u045E': ['',	'\u01D4',	'u`'],									// 'ў'
		'\u046B': ['',	'\u01CE',	'',		'o`'],							//  юс
		'\u0475': ['',	'\u1EF3',	'',		'yh',	'',		'yh'],			//  ижица
		'\u0458': ['',	'j\u030C',	'',		'',		'j']					// 'ј'
		/*-Имя------0-,----1-,-------2-,-----3-,-----4-,-----5-,-----6--*/
	},
	
	booleanRenderer: function(mask) {
		return function(value) {
			return Ext.util.Format.bool(value, mask);
		};
	},
	
	currencyRenderer: function(currencySign, decimals, end, currencySpacer) {
		return function(value) {
			return Ext.util.Format.currency(value, currencySign, decimals, end, currencySpacer);
		};
	},
	
	enumRenderer: function(store) {
		if (Ext.isString(store)) store = Ext.getStore(store);
		return function(value) {
			return Ext.util.Format.formatEnum(value, store);
		};
	},
	
	manyOfManyRenderer: function(mask) {
		return function(value) {
			return Ext.util.Format.manyOfMany(value, mask);
		};
	},
	
	objectRenderer: function(field) {
		return function(value) {
			return Ext.util.Format.objectField(value, field);
		};
	},
	
	oneOfManyRenderer: function(mask) {
		return function(value) {
			return Ext.util.Format.oneOfMany(value, mask);
		};
	},
	
	bool: function(value, mask) {
		return mask && mask.length > 1 ? (value ? mask[1] : mask[0]) : value;
	},
	
	elapsed: function(value) {
		return Ext.Date.elapsed(value);
	},
	
	formatEnum: function(value, store) {
		return Ext.util.Format.transformEnum(value, store, 'id', 'name');
	},
	
	oneOfMany: function(value, mask) {
		return mask && Ext.isNumber(value) ? mask[value] : value;
	},
	
	manyOfMany: function(value, mask) {
		if (Ext.isArray(mask) && Ext.isNumber(value)) {
			var result = '', i, bit;
			for (i = 0; i < mask.length; i++) {
				bit = Math.pow(2, i);
				if ((value & bit) === bit) result += ', ' + mask[i];
			}
			value = result.replace(/^, /, '');
		}
		return value;
	},
	
	objectField: function(value, field) {
		return Ext.isObject(value) && !Ext.isEmpty(field) ? value[field] : value;
	},
	
	parseEnum: function(value, store) {
		return Ext.util.Format.transformEnum(value, store, 'name', 'id');
	},
	
	parseOneOfMany: function(value, mask) {
		if (Ext.isString(value) && mask) {
			for (var key in mask) {
				if (mask[key] === value) return key;
			}
		}
		return null;
	},

	transformEnum: function(value, store, searchKey, valueKey) {
		if (Ext.isString(store)) store = Ext.getStore(store);
		if (store && store.isStore) {
			var record = store.findRecord(searchKey, value);
			if (record) value = record.get(valueKey);
		}
		return value;
	},

	translit: function(str, type) {
		var me = Ext.util.Format, map = me.translitMap,
			absType = Math.abs(type),
			reverse = type !== absType,
			transmap = {}, regarr = [], row, 
			prepareFn = me.makeTranslitPrepareFn(absType, transmap, regarr, reverse),
			postFn = me.makeTranslitPostFn(str, reverse),
			regFn = me.makeTranslitRegFn(transmap);
		
		for (row in map) {
			if (map.hasOwnProperty(row)) {
				prepareFn(map[row], row);
			}
		}
		return postFn(str.replace(new RegExp(regarr.join('|'), 'gi'), regFn));
	},
	
	makeTranslitPrepareFn: function(type, transmap, regarr, reverse) {
		var write = reverse === true
			? function(row, chr) { transmap[row] = chr; regarr.push(row); }
			: function(chr, row) { transmap[row] = chr; regarr.push(row); };
		return function(col, row) {
			var chr = col[type] || col[0];
			if (chr) write(chr, row);
		}
	},
		
	makeTranslitPostFn: function(str, reverse) {
		if (reverse === true) {
			str = str.replace(/(c)(?=[ieyj])/ig, '$1z');
			return function(str) { return str; }
		}
		str = str.replace(/(i(?=.[^аеиоуъ\s]+))/ig, '$1`');
		return function(str) {
			return str.replace(/i``/ig, 'i`').replace(/((c)z)(?=[ieyj])/ig, '$1'); 
		}
	},
		
	makeTranslitRegFn: function(transmap) {
		return function(substr) {
			if (substr.toLowerCase() === substr) {
				return transmap[substr];
			}
			return transmap[substr.toLowerCase()].capitalize();
		}
	}
	
}, function() {
	regStore = function(type) {
		Ext.regStore(Ext.Factory.store({type: type}));
	}
	regStore('airlinetickettypes');
	regStore('companycustomertypes');
	regStore('customertypes');
	regStore('gendertypes');
	regStore('messagepriorities');
	regStore('orderstates');
	regStore('payingtypes');
	regStore('posttypes');
	regStore('servicetypes');
	regStore('ticketcategories');
	regStore('railwaytickettypes');
	regStore('suppliertypes');
	// years store
	var data = [], yearCurr = new Date().getFullYear(), i = 1900, emptyTreeStore;
	// years store
	for(; i <= yearCurr ; i++ ) {
		data.push( { year: i } );
	}
	Ext.regStore(Ext.Factory.store({
		alias	: 'store.years',
		data	: data,
		fields	: [{ name: 'year', type: 'int' }],
		storeId	: 'years',
	    sorters	: [{ property: 'year', direction: 'DESC' }]
	}));
	// empty tree store
	emptyTreeStore = Ext.regStore(Ext.Factory.store({ type: 'tree', proxy:'memory', storeId: 'ext-empty-tree-store', useModelWarning: false }));
	emptyTreeStore.isEmptyStore = true;
	emptyTreeStore.add = emptyTreeStore.remove = emptyTreeStore.insert = emptyTreeStore.loadData = function () { Ext.raise('Cannot modify ext-empty-tree-store'); };
	CFJS.overrideStoreData = function(store, data) {
		var store = Ext.getStore(store);
		if (store && store.isStore) store.setData(data);
	}
});
