Ext.define('CFJS.overrides.data.Request', {
	override: 'Ext.data.Request',
	config	: {
		disableExtraParams	: false,
		signParams			: undefined
	}
});