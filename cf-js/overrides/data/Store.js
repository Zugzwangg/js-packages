Ext.define('CFJS.overrides.data.Store', {
	override: 'Ext.data.Store',
	
	isDirty: function() {
		var me = this;
		return [].concat(me.getNewRecords(), me.getUpdatedRecords(), me.getRemovedRecords()).length > 0;
	}

});