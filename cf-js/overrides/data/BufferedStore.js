Ext.define('CFJS.overrides.data.BufferedStore', {
	override	: 'Ext.data.BufferedStore',
	
    reload: function(options) {
        var me = this,
            data = me.getData(),
            // If we don't have a known totalCount, use a huge value 
            lastTotal = Number.MAX_VALUE,
            startIdx, endIdx, startPage, endPage,
            i, waitForReload, bufferZone, records;
 
        if (!options) {
            options = {};
        }
 
        // Prevent re-entering the load process if we are already in a wait state for a batch of pages. 
        if (me.loading || me.fireEvent('beforeload', me, options) === false) {
            return;
        }
 
        waitForReload = function() {
            var newCount = me.totalCount,
                oldRequestSize = endIdx - startIdx;
            // If the dataset has now shrunk leaving the calculated request zone unavailable, 
            // re-evaluate the request zone. Start as close to the end as possible. 
            if (endIdx >= newCount) {
                endIdx = newCount - 1;
                startIdx = Math.max(endIdx - oldRequestSize, 0);
            }
            if (me.rangeCached(startIdx, endIdx, false)) {
                me.loadCount = (me.loadCount || 0) + 1;
                me.loading = false;
                data.un('pageadd', waitForReload);
                // Fix bug when endIdx === startIdx
                records = data.getRange(startIdx,  endIdx || 1);
                me.fireEvent('load', me, records, true);
                me.fireEvent('refresh', me);
            }
        };
        bufferZone = Math.ceil((me.getLeadingBufferZone() + me.getTrailingBufferZone()) / 2);

        // Decide what reload means. 
        // If the View was configured preserveScrollOnReload, then it will 
        // inject that setting here. This means that reload means 
        // load the last requested range. 
        if (me.lastRequestStart && me.preserveScrollOnReload) {
            startIdx = me.lastRequestStart;
            endIdx = me.lastRequestEnd;
            lastTotal = me.getTotalCount();
        }
        // Otherwise, reload means start from page 1 
        else {
            startIdx = options.start || 0;
            endIdx = startIdx + (options.count || me.getPageSize()) - 1;
        }
 
        // Clear page cache 
        data.clear(true);
 
        // So that prefetchPage does not consider the store to be fully loaded if the local count is equal to the total count 
        delete me.totalCount;
 
        // Calculate a page range which encompasses the Store's loaded range plus both buffer zones 
        startIdx = Math.max(startIdx - bufferZone, 0);
        endIdx = Math.min(endIdx + bufferZone, lastTotal);

        // We must wait for a slightly wider range to be cached. 
        // This is to allow grouping features to peek at the two surrounding records 
        // when rendering a *range* of records to see whether the start of the range 
        // really is a group start and the end of the range really is a group end. 
        startIdx = startIdx === 0 ? 0 : startIdx - 1;
        endIdx = endIdx === lastTotal ? endIdx : endIdx + 1;
 
        startPage = me.getPageFromRecordIndex(startIdx);
        endPage = me.getPageFromRecordIndex(endIdx);

        me.loading = true;
        options.waitForReload = waitForReload;
 
        // Wait for the requested range to become available in the page map 
        // Load the range as soon as the whole range is available 
        data.on('pageadd', waitForReload);
 
        // Recache the page range which encapsulates our visible records 
        for (i = startPage; i <= endPage; i++) {
            me.prefetchPage(i, options);
        }
    }

});