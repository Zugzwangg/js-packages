Ext.define('CFJS.overrides.data.Model', {
	override: 'Ext.data.Model',

	addSign: Ext.emptyFn,
	
	hasSign: function() {
		return false;
	},
	
	getData: function (options) {
		var me = this, ret = me.callParent(arguments),
			clearNulls = Ext.isObject(options) && options.clearNulls;
		if (clearNulls) {
			if (!Ext.isFunction(clearNulls)) clearNulls = me.clearNulls;
			Ext.callback(clearNulls, me, [ret]);
		}
		return ret;
	},
	
	set: function (fieldName, newValue, options) {
		var me = this,
			single = Ext.isString(fieldName),
			opt = (single ? options : newValue);
		return opt && opt.sign ? me.addSign(single ? newValue : fieldName, options) : me.callParent(arguments);
	},
	
	sign: function(options) {
		options = Ext.apply({}, options);
		var me = this,
			scope  = options.scope || me,
			callback = options.callback,
			proxy = me.getProxy(),
			operation;
		options.records = [me];
		options.internalCallback = function(operation) {
			var args = [me, operation],
			success = operation.wasSuccessful();
			if (success) {
				Ext.callback(options.success, scope, args);
			} else {
				Ext.callback(options.failure, scope, args);
			}
			args.push(success);
			Ext.callback(callback, scope, args);
		};
		delete options.callback;
		operation = proxy.createOperation('sign', options);
		operation.execute();
		return operation;
	},
	
	privates: {

		clearNulls: function(data) {
			for (var prop in data) {
				if (data[prop] === null) delete data[prop];
				else if (Ext.isObject(data[prop])) this.clearNulls(data[prop]);
			}
		}

	},
	
	inheritableStatics: {
		
		persistableFields: function() {
			var cls = this.prototype, ret = cls.persistFields;
			if (!ret) {
				var fields = cls.fields, i;
				cls.persistFields = ret = [];
				for (i = 0; i < fields.length; ++i) {
					if (fields[i].persist) ret.push(fields[i]);
				}
			}
			return ret;
		}
	
	}

});
