Ext.define('CFJS.overrides.data.proxy.Server', {
	override: 'Ext.data.proxy.Server',
	
	buildRequest: function(operation) {
		var me = this, request = me.callParent(arguments);
		request.setConfig({ 
			disableExtraParams	: me.disableExtraParams,
			signParams			: me.signParams
		});
		return request; 
	},
	
	getExtraParams: function() {
		return this.disableExtraParams ? {} : this.extraParams;
	}
	
});
