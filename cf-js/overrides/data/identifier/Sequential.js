Ext.define('CFJS.overrides.data.identifier.Sequential', {
	override: 'Ext.data.identifier.Sequential',
    config	: {
        increment	: 1,
        mask		: null,
        prefix		: null,
        seed		: 1
    },
    generate: function () {
        var me = this,
            seed = me._seed,
            prefix = me._prefix;
        me._seed += me._increment;
        return (prefix !== null ? prefix : "") + Ext.util.Format.number(seed, me._mask);
    }
});