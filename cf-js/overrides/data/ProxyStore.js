Ext.define('CFJS.overrides.data.ProxyStore', {
	override	: 'Ext.data.ProxyStore',
	config: {
		summaries: null
	},
	
	applySummaries: function(summaries) {
		summaries = summaries || [];
		if (!Ext.isArray(summaries)) summaries = [summaries];
		return summaries;
	},
	
	flushSummary: function() {
		var me = this,
			options = me.pendingSummaryOptions,
			operation;
		// If it gets called programatically before the timer fired, the listener will need cancelling.
		me.clearSummaryTask();
		if (!options) return;
		me.setSummaryOptions(options);
		operation = Ext.apply({
			internalScope	: me,
			internalCallback: me.onProxySummary,
			scope			: me
		}, options);
		me.lastOptions = operation;
		operation = me.createOperation('summary', operation);
		if (me.fireEvent('beforesummary', me, operation) !== false) {
			me.loading = true;
			operation.execute();
		}
    },
    
    summary: function(options) {
        var me = this;
        // Legacy option. Specifying a function was allowed.
        if (typeof options === 'function') options = { callback: options };
        // We may mutate the options object in setSummaryOptions.
        else options = options ? Ext.Object.chain(options) : {};
        me.pendingSummaryOptions = options;
        // If we are configured to load asynchronously (the default for async proxies)
        // then schedule a flush, unless one is already scheduled.
        if (me.getAsynchronousLoad()) {
            if (!me.summaryTimer) me.summaryTimer = Ext.asap(me.flushSummary, me);
        }
        // If we are configured to load synchronously (the default for sync proxies) then flush the load now.
        else me.flushSummary();
        return me;
    },
	
	onProxySummary: function(operation) {
		var me = this,
			resultSet = operation.getResultSet(),
			records = operation.getRecords(),
			successful = operation.wasSuccessful();
		if (me.destroyed) return;
		me.loading = false;
		if (me.hasListeners.summary) me.fireEvent('summary', me, records, successful, operation);
	},

	privates: {
		
		clearSummaryTask: function() {
            Ext.asapCancel(this.summaryTimer);
            this.pendingSummaryOptions = this.summaryTimer = null;
        },
        
		setSummaryOptions: function(options) {
			var me = this, filters, summaries;
			if (me.getRemoteFilter()) {
				filters = me.getFilters(false);
				if (filters && filters.getCount()) {
					options.filters = filters.getRange();
				}
			}
			// Only add grouping options if grouping is remote
			if (options.sorters) delete options.sorters;
			if (me.getRemoteSort() && !options.grouper) {
				grouper = me.getGrouper();
				if (grouper) options.grouper = grouper;
			}
			if (!options.summaries) {
				summaries = me.getSummaries();
				if (summaries) options.summaries = summaries;
			}
		}
	}
});