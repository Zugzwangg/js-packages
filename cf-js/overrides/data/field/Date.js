Ext.define('CFJS.overrides.data.field.Date', {
	override: 'Ext.data.field.Date',
	getDateWriteFormat: function() {
        var me = this;
        if (me.hasOwnProperty('dateWriteFormat')) return me.dateWriteFormat;
        if (me.hasOwnProperty('dateFormat')) return me.dateFormat;
        return me.dateWriteFormat || me.dateFormat || CFJS.Api.dateTimeFormat;
    }
});