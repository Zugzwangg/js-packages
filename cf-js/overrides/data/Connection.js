Ext.data.Connection.override({
	dateTimeFormat		: 'Y-m-d\\TH:i:s.uO',
	requires			: [	'Ext.data.identifier.Uuid', 'CFJS.JSToXml' ],
	generator			: new Ext.data.identifier.Uuid(),
	config				: {
		apiMethod			: '',
		appName				: undefined,
		autoAbort			: false,
		controllers			: undefined,
		disableCaching		: false,
		defaultXhrHeader	: 'cf-api',
		extraParams			: {},
		handlerUri			: null,
		hashOptions			: { alg: 'sha1', prov: 'cryptojs' },
		host				: null,
		method				: 'GET',
		next				: undefined,
		portalUri			: null,
		privateKey			: undefined,
		publicKey			: undefined,
		secure				: undefined,
		serial				: undefined,
		service				: 'ics',
		session				: {},
		sessionHeader		: 'X-Requested-With',
		signOptions			: { alg: 'SHA1withRSA' },
		version				: '1.0',
		viewModel			: undefined
	},

	applyControllers: function(controllers) {
		var me = this, key, config;
		controllers = controllers || {};
		for (key in controllers) {
			if (controllers.hasOwnProperty(key)) {
				if (config = controllers[key]) {
					if (!Ext.isObject(controllers[key])) controllers[key] = config = { url: config, method: '' };
					config.url = !Ext.isString(config.url) || Ext.isEmpty(config.url) ? me.getUrl() : me.safeUrl(config.url);
				} else delete controllers[key];
			}
		}
		controllers[me.getService()] = { url: me.getUrl(), method: me.getApiMethod() };
		return controllers;
	},
	
	applyHost: function(host) {
		if (Ext.isEmpty(host)) {
			var loc = window.location;
			if (Ext.isObject(host)) host = (host.protocol || loc.protocol) + '//' + (host.host || window.location.host);
		}
		return host;
	},
	
	applyPortalUri: function(uri) {
		if (uri && Ext.isString(uri)) {
			return uri.length > 1 && uri.endsWith('/') ? uri.substr(0, uri.length - 1) : uri;
		}
		return null;
	},
	
	applySecure: function(secure) {
		return secure ? 1 : undefined;
	},

	applySession: function(session) {
		return session || {};
	},
	
	applyUrl: function(url) {
		return this.safeUrl(url);
	},
	
	buildPayload: function(data, type) {
		return this['build' + this.payloadFormat().capitalize() + 'Payload'](data, type);
	},
	
	buildJsonPayload: function(data, type) {
		var me = this, payload = me.payloadData(data, type);
		payload.request.xmlsign = encodeURIComponent(stob64(hextorstr(me.sign(Ext.encode(payload)))));
		return payload;
	},

	buildXmlPayload: function(data, type) {
		var me = this, payload = CFJS.JSToXml.toXML(me.payloadData(data, type).request, { record: 'request' });
		payload.getElementsByTagName('xmlsign').item(0).textContent = encodeURIComponent(stob64(hextorstr(me.sign(CFJS.XmlToJson.xmlToString(payload)))));
		return payload;
	},

	buildUrl: function(options) {
		options = options || {};
		var me = this,
			extraParams = Ext.apply(me.getExtraParams(), options.extraParams),
			params = options.params || {},
			service = options.service || me.getService(),
			controller = me.controller(service),
			apiMethod = '', apiUrl = me.getUrl();
		if (!Ext.isEmpty(controller)) {
			if (controller.method) apiMethod = controller.method;
			if (!Ext.isEmpty(controller.url)) apiUrl = controller.url;
		}
		if (Ext.isObject(params)) {
			if (!Ext.isEmpty(apiMethod)) params.method = apiMethod + (!Ext.isEmpty(params.method) ? '.' + params.method : '');
			params = Ext.Object.toQueryString(params);	
		}
		if (Ext.isObject(extraParams)) extraParams = Ext.Object.toQueryString(extraParams);
		params = me.signParams(params + ((extraParams) ? ((params) ? '&' : '') + extraParams : ''), options.signParams);
		return Ext.String.urlAppend(apiUrl, params);
	},
	
	controller: function(name) {
		var controllers = this.getControllers();
		return !Ext.isEmpty(name) && !Ext.isEmpty(controllers) ? controllers[name] : null;
	},
	
	controllerMethod: function(name) {
		var me = this, controller = me.controller(name);
		return !Ext.isEmpty(controller) && controller.method ? controller.method : '';
	},
	
	controllerUrl: function(name) {
		var me = this, controller = me.controller(name);
		return !Ext.isEmpty(controller) && !Ext.isEmpty(controller.url) ? controller.url : me.getUrl();
	},

	extractResponse: function(context, root) {
		if (Ext.isEmpty(context)) return {};
		context = (this.payloadFormat() === 'xml' ? CFJS.XmlToJson.parseXML(context.responseXML) : Ext.decode(context.responseText)) || {};
		if (root === undefined || root === null) root = 'response';
		if (root) context = context[root] || {};
		return context;
	},
	
	formatDate: function(date) {
		return this.formatDateTime(Ext.Date.clearTime(date, true));
	},
	
	formatDateTime: function(date) {
		return Ext.util.Format.date(date, this.dateTimeFormat);
	},

	hash: function(message) {
		return new KJUR.crypto.MessageDigest(this.getHashOptions()).digestString(message);
	},
	
	getExtraParams: function() {
		return this.disableExtraParams ? {} : this._extraParams;
	},

	payloadData: function(data, type) {
		var me = this, service = me.getAppName() || me.getService(),
			transaction = Ext.apply({
				'@id'	: service.toUpperCase() + '-' + me.generator.generate(), 
				'@type'	: type
			}, data || {});
		return {
			request: {
				"@version"	: me.getVersion(),
				datetime	: me.formatDateTime(new Date()),
				xmlsign 	: ' ',
				transaction	: transaction
			}
		};
	},

	payloadFormat: function() {
		return (this.getExtraParams()||{}).format || 'json';
	},

	createRequest: function(options, requestOptions) {
		this.latestTime = new Date().getTime(); 
		return this.callParent(arguments);
	},
	
	setOptions: function(options, scope) {
		var me = this, disableExtraParams = me.disableExtraParams;
		me.disableExtraParams = options.disableExtraParams;
		if (options.urlParams) {
			options.params = Ext.apply(options.params || {}, options.urlParams);
			delete options.urlParams;
		}
		var requestOptions = me.callParent(arguments);
		if (me.isFormUpload(options)) {
			requestOptions.url = Ext.urlAppend(requestOptions.url, requestOptions.data);
			requestOptions.data = null;
			delete options.params;
		}
		me.disableExtraParams = disableExtraParams;
		return requestOptions;
	},

	setupParams: function(options, params) {
		var me = this,
			form = me.getForm(options);
		if (options.signParams !== false) options.signParams = options.signParams || true; 
		if (form && !me.isFormUpload(options)) {
			var serializedForm = Ext.Element.serializeForm(form);
			params = params ? (params + '&' + serializedForm) : serializedForm;
		}
		return me.signParams(params, options.signParams);
	},
    
	sign: function(message) {
		var me = this, privateKey = me.getPrivateKey(), password = me.getPassword(),
			sig = new KJUR.crypto.Signature(me.getSignOptions());
		sig.init(privateKey, password);
		sig.updateString(message)
		return sig.sign();		
	},
	
	signParams: function(params, signParams) {
		var me = this, publicKey = me.getPublicKey();
		if (params && publicKey && signParams) {
			var object = Ext.isString(params) ? Ext.Object.fromQueryString(params) : params,
				paramObjects = [], i, j, ln, paramObject, value;
			for (i in object) {
				if (object.hasOwnProperty(i)) {
					paramObjects = paramObjects.concat(Ext.Object.toQueryObjects(i, object[i]));
				}
			}
			params = [];
			for (j = 0, ln = paramObjects.length; j < ln; j++) {
				paramObject = paramObjects[j];
				value = paramObject.value;
				if (Ext.isEmpty(value)) value = '';
				else if (Ext.isDate(value)) value = Ext.Date.toString(value);
				params.push(paramObject.name + '=' + String(value));
			}
			params = params.sort();
			params.push('sig=' + me.hash(params.join('') + hextob64(new KJUR.asn1.x509.SubjectPublicKeyInfo(publicKey).getEncodedHex())));
			params = params.join('&');
		}
		return params;
	},
	
	updateExtraParams: function(newParams, oldParams) {
		var me = this;
		me.setDefaultXdrContentType('application/' + ((newParams||{}).format || 'json'));
		me.setDefaultPostHeader(me.getDefaultXdrContentType() + '; charset=UTF-8');
	},

	updateSecure: function(secure) {
		var me = this, 
			extraParams = me.getExtraParams() || {};
		if (secure) extraParams.secure = secure;
		else delete extraParams.secure;
		me.setExtraParams(extraParams);
	},
	
	updateSession: function(session) {
		var me = this,  vm = me.getViewModel(), sessionHeader = me.getSessionHeader(), headers = me.getDefaultHeaders() || {},
			extraParams = Ext.apply(me.getExtraParams() || {}, { sid: (session || {}).sid, uid: (session || {}).uid });
		if (me.getSecure() || !extraParams.sid) delete extraParams.sid;
		if (!(me.getSecure() && extraParams.uid)) delete extraParams.uid;
		me.setExtraParams(extraParams);
		if (session && session.refreshToken && sessionHeader) {
			headers[sessionHeader] = session.refreshToken;
			me.setDefaultHeaders(headers);
		}
		if (vm) vm.set('session', session);
	},

	privates: {
		
		safeUrl: function(url) {
			if (Ext.isString(url) && url.startsWith('http')) return url;
			var me = this,
				concat = function(left, right) {
					left = String(left||'').valueOf();
					if (!(right = String(right||'').valueOf())) return left;
					if (left) {
						if (!left.endsWith('/')) left = left + '/';
						if (right && right[0] === '/') right = right.substr(1);
					}
					return left + right;
				}
			url = concat(me.getPortalUri(), String(url || [me.getService(), me.getHandlerUri()].join('/')).valueOf());
			url = concat(me.getHost(), url);
			return url; 
		}
	
	}
	
});
