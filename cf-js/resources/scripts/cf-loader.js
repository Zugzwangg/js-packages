var	parseDate = Date.parse, Ext = Ext || {}, CFJS = CFJS || {};
	
if (this.debug != 'function') {
	this.debug = function(message) { if (typeof console != 'undefined') { console.debug(message); } };
}
if (!window.console || !console.firebug) {
	var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml",
	             "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
	window.console = window.console || {};
	for (var i = 0; i < names.length; ++i)
		window.console[names[i]] = window.console[names[i]] || function() {}
}
(function() {
	CFJS.apply = function(object, config, defaults) {
		if (defaults) { Ext.apply(object, defaults); }
		if (object && config && typeof config === 'object') {
			var i, j, k;
			for (i in config) {
				if (object[i] === undefined) object[i] = config[i];
			}
		}
		return object;
	};
	CFJS.apply(CFJS, {
		config	: {
			buildInfo	: { startDeveloping: '2010' },
			locale		: {
				cookie: 'ICS_LOCALE',
				name: 'en',
				en: { msgtitle: 'Please wait', msgwait: 'Loading modules...' },
				uk: { msgtitle: 'Зачекайте', msgwait: 'Йде завантаження...' },
				ru: { msgtitle: 'Подождите', msgwait: 'Идет загрузка...' }
			}
		},
	    isFrame	: window.top !== window,
		shortMax: 32767,
		libs	: [
			'/scripts/Blob.min.js',
			'/scripts/FileSaver.js',
			'/scripts/reedsolomon.js',
			'/scripts/qrcodedecode.js',
			'/scripts/jsrsasign-latest-all-min.js'
		],
		
		findLocale: function(nameCookie) {
			try {
				var locale = CFJS.config.locale, 
					cookie = locale.cookie,
					defaultLocale = locale.name;
				// Look for the locale as a url argument
				locale = location.href.match(/locale=([\w-]+)/);
				locale = locale && locale[1];
				// Look for the locale as a cookie
				if (!locale) { locale = CFJS.getCookie(nameCookie || cookie); }
				// Use the browser's locale
				if (!locale) {
					locale = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
					if (locale) locale = locale.replace(/-/g, '_');
				}
				if (!locale) return defaultLocale;
				if ((lastIndex = locale.lastIndexOf('_')) != -1) locale = locale.substring(0,lastIndex);
				return /^(ru|en|uk|ukr)$/.test(locale) ? locale : defaultLocale;
			} catch(e) {
				console.debug('Unexpected exception in locale detection, using default: ' + e);
			}
		},

		getCookie: function(c_name) {
			var i, x, y, cookies = document.cookie.split(";");
			for (i = 0; i < cookies.length; i++) {
				x = cookies[i].substr(0, cookies[i].indexOf("="));
				y = cookies[i].substr(cookies[i].indexOf("=") + 1);
				x = x.replace(/^\s+|\s+$/g,"");
				if (x == c_name) return unescape(y);
			}
		},

		createTag: function(name, attr) {
			var tag = document.createElement(name);
			for (key in attr) {
				if (attr[key] !== null && attr[key] !== undefined) tag.setAttribute(key, attr[key]); 
			}
			return tag;
		},
		
		loadCss: function(url) {
			var link = CFJS.createTag('link', {
					href: url,
					rel: 'stylesheet',
					type: 'text/css'
				}),
			headEl = document.getElementsByTagName('head')[0];
			headEl.appendChild(link);
		},
		
		loadLibraries: function(libs, callback, noCache) {
			if (libs && libs.length > 0) {
				var lib = (CFJS.libsUrl||'') + libs.splice(0, 1);
				if (noCache === true) lib += "?dc_" + new Date().getTime();
				CFJS.loadScript(lib, function() { CFJS.loadLibraries(libs, callback, noCache); });
				return;
			}
			if (!!callback && typeof callback === 'function') callback();
		},
		
		loadScript: function(url, callback, async) {
			var tag = 'script',
				script = CFJS.createTag(tag, {
					async: async,
					src: url,
					type: 'text/javascript'
				}),
				scriptEl = document.getElementsByTagName(tag)[0];
			if (!!callback && typeof callback === 'function') {
				script.onreadystatechange = callback;
				script.onload = callback;
			}
			scriptEl.parentNode.insertBefore(script, scriptEl);
		},
		
		writeLoader: function() {
			var head  = document.getElementsByTagName('head')[0],
				loader = document.getElementById("cf-loader"),
				indicator = loader && loader.getAttribute("data-indicator"),
				locale = CFJS.findLocale('ICS_LOCALE').substr(0, 2) || 'en',
				msg = CFJS.config.locale[locale];
			if (indicator === 'modern') {
				document.write([
					'<div class="load-screen" id="load-screen" role="presentation">',
						'<div class="indicator"></div>',
						'<div class="message">',
			 				'<div class="inner">',
								'<h1>' + msg.msgtitle + '</h1>',
								'<h2>' + msg.msgwait + '</h2>',
							'</div>',
						'</div>',
						'<div class="сurtain left"></div>',
						'<div class="сurtain right"></div>',
					'</div>'
				].join(''));
			} else {
				indicator = 'normal';
				document.write([
					'<div class="load-screen" id="load-screen" role="presentation">',
					    '<div class="indicator">',
							'<div></div><div></div><div></div>',
						'</div>',
						'<div class="сurtain left"></div>',
						'<div class="сurtain right"></div>',
					'</div>'
				].join(''));
			}
			head.appendChild(CFJS.createTag('link', { rel: 'stylesheet', type: 'text/css', href: 'resources/cf-js/css/' + indicator + '-loader.css' }));
		}
	});
	CFJS.writeLoader();
	CFJS.loadLibraries(CFJS.libs);
}());	

Ext.beforeLoad = Ext.beforeLoad || function (tags) {
	// load app config
	var profile = location.href.match(/profile=([\w-]+)/),
		locale = CFJS.findLocale('ICS_LOCALE');
	profile = (profile && profile[1]) || (tags.phone ? 'phone' : (tags.tablet ? 'tablet' : 'desktop'));
	Ext.manifest = profile + "-" + (locale === 'uk' ? 'ukr' : locale);
	tags.test = /testMode=true/.test(location.search);
	CFJS.config.locale.name = locale.substr(0, 2);
	Ext.microloaderTags = tags;
};
