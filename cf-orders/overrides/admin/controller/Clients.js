Ext.define('CFJS.orders.overrides.admin.controller.Clients', {
	override: 'CFJS.admin.controller.Clients',

	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.purchaseHistory, !selection || selection.length !== 1);
		this.callParent(arguments);
	},
	
	onShowPurchaseHistory: function(component) {
		var me = this, 
			vm = me.getViewModel().getParent(),
			store = vm ? vm.get('appMenu') : null,
			node = component && store ? store.getNodeById(component.viewId) : null,
			routeId = node ? node.get('routeId') : null,
			client = routeId ? me.lookupCurrentItem() : null;
		if (client && client.isModel) {
			vm.set('purchaseHistoryFilter', { type: client.entityName.toLowerCase(), id: client.id, view: node.id });
			me.redirectTo(routeId);
		} 
	}

});
