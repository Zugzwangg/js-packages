Ext.define('CFJS.orders.grid.invoice.ServiceCertificatesPanel', {
	extend			: 'CFJS.document.grid.ObjectPanel',
	xtype			: 'servicecertificatesgrid',
	requires		: [ 'Ext.button.Segmented', 'CFJS.document.util.Forms' ],
	config			: {
		actions			: {
			printDocument	: {
				iconCls	: CFJS.UI.iconCls.PRINT,
				title	: 'Print document',
				tooltip	: 'Print selected document',
				handler	: 'onPrintDocument'
			},
			reloadDocuments	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data',
				handler	: 'onReloadDocuments'
			},
			removeDocuments	: {
				iconCls	: CFJS.UI.iconCls.DELETE,
				title	: 'Cancel a document',
				tooltip	: 'Cancel selected documents',
				handler	: 'onRemoveDocuments'
			},
			signDocument	: {
				iconCls	: CFJS.UI.iconCls.SIGN,
				title	: 'Sign document',
				tooltip	: 'Sign selected document',
				handler	: 'onSignDocument'
			}
		},
		actionsMap		: {
			contextMenu	: { items: [ 'removeDocuments', 'signDocument', 'printDocument' ] },
			header		: { items: [ null, 'periodPicker', 'removeDocuments', 'signDocument', 'printDocument', 'clearFilters', 'reloadDocuments' ] }
		},
		actionsDisable	: [ 'printDocument', 'removeDocuments', 'signDocument' ]
	},
	bind			: { form: '{config.serviceCertificate.form}', store: '{serviceCertificates}', title: 'Акты и накладные {customerTypeName}' },
	contextMenu		: { items: new Array(3) },
	enableColumnHide: false,
	header			: {
		items		: [{
			xtype	: 'segmentedbutton',
			bind	: '{customerType}',
			margin	: '0 24 0 0',
			items	: [{
				iconCls	: CFJS.UI.iconCls.BUILDING,
				tooltip	: 'Компании',
				value	: 'company'
			},{
				iconCls: CFJS.UI.iconCls.USER,
				tooltip: 'Физ. лица',
				value	: 'customer'
			}]
		}].concat(new Array(6))
	},
	iconCls			: CFJS.UI.iconCls.FILE_TEXT,
	reference		: 'serviceCertificatesGrid',
	plugins			: 'gridfilters',
	selModel		: { type: 'rowmodel', mode: 'SINGLE', pruneRemoved: false },

	initComponent: function() {
		var me = this, actions = me.actions, vm;
		me.callParent();
		if (vm = me.lookupViewModel()) {
			vm.bind('{_itemName_.id}', function(id) {
				actions.printDocument.setDisabled(id <= 0);
			});
			vm.bind('{_itemName_.state}', function(state) {
				actions.removeDocuments.setHidden(!state || state === 3);
				actions.signDocument.setHidden(!state || state === 3);
			});
			vm.bind('{_itemName_.canSign}', function(canSign) {
				actions.removeDocuments.setDisabled(!canSign);
				actions.signDocument.setDisabled(!canSign);
			});
		}
		vm.bind({ period: '{workingPeriod}', store: '{serviceCertificates}'}, function(data) {
			var period = data.period, store = data.store,
				startDate = (period || {}).startDate,
				endDate = (period || {}).endDate;
			store.getFilters().beginUpdate();
			if (startDate) store.filter({ id: 'startDate', property: me.dateField, type: 'date-time', operator: '!before', value: startDate }); 
			else store.removeFilter('startDate');
			if (endDate) store.filter({ id: 'endDate', property: me.dateField, type: 'date-time', operator: '!after', value: endDate }); 
			else store.removeFilter('endDate');
			store.getFilters().endUpdate();
		});
	},

	renderColumns: function(form) {
		var me = this, columns = [],
			config = CFJS.orders.util.Invoice.findFormConfig(form),
			fields,	i, field, column;
		if (config && config.isServiceCertificate) {
			me.dateField = config.dateField;
			if (fields = config.fields) {
				if (Ext.isObject(fields)) fields = [fields];
				for (i = 0; i < fields.length; i++) {
					column = me.renderColumn(field = fields[i]);
					if (!Ext.isEmpty(column)) {
						if (field.id === me.dateField) column.filter = false;
						columns.push(column);
					}
				}
				columns.sort(CFJS.document.util.Forms.columnSorter);
			}
		} else delete me.dateField;
		columns.push({
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'author.name',
			style		: { textAlign: 'center' },
			text		: 'Автор',
			tpl			: '<tpl if="author">{author.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'unit.name',
			style		: { textAlign: 'center' },
			text		: 'Подразделение',
			tpl			: '<tpl if="unit">{unit.name}</tpl>'
		});
		me.lookupController().onSelectionChange(null, []);
		return columns;
	}

});
