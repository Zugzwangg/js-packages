Ext.define('CFJS.orders.grid.invoice.InvoicePanel', {
	extend			: 'CFJS.document.grid.ObjectPanel',
	xtype			: 'invoicegrid',
	requires		: [ 
		'Ext.button.Segmented',
		'CFJS.document.util.Forms',
		'CFJS.orders.window.invoice.PaymentView',
		'CFJS.orders.window.invoice.ReferredDocuments'
	],
	config			: {
		actions			: {
			paying			: {
				iconCls	: CFJS.UI.iconCls.MONEY,
				title	: 'Make a payment',
				tooltip	: 'Make an invoice payment',
				handler	: 'onPaying'
			},
			printRecord			: {
				iconCls	: CFJS.UI.iconCls.PRINT,
				title	: 'Print invoice',
				tooltip	: 'Print selected invoice',
				handler	: 'onPrintRecord'
			},
			referredDocuments	: {
				iconCls	: CFJS.UI.iconCls.REFERRED,
				title	: 'Subordinate documents',
				tooltip	: 'Working with subordinate documents',
				handler	: 'onReferredDocuments'
			},
			removeRecord		: {
				title	: 'Cancel invoice',
				tooltip	: 'Cancel selected invoices'
			},
			signRecord			: {
				iconCls	: CFJS.UI.iconCls.SIGN,
				title	: 'Sign invoice',
				tooltip	: 'Sing selected invoice',
				handler	: 'onSignRecord'
			}
		},
		actionsMap		: {
			contextMenu	: { items: [ 'removeRecord', 'signRecord', 'paying', 'printRecord', 'referredDocuments' ] },
			header		: { items: [ null, 'periodPicker', 'removeRecord', 'signRecord', 'paying', 'printRecord', 'referredDocuments', 'clearFilters', 'reloadStore' ] }
		},
		actionsDisable	: [ 'removeRecord', 'signRecord', 'paying', 'printRecord', 'referredDocuments' ]
	},
	bind			: { form: '{config.invoice.form}', store: '{invoices}', title: 'Счета {customerTypeName}' },
	contextMenu		: { items: new Array(5) },
	enableColumnHide: false,
	header			: { 
		items: [{
			xtype	: 'segmentedbutton',
			bind	: '{customerType}',
			margin	: '0 24 0 0',
			items	: [{
				iconCls	: CFJS.UI.iconCls.BUILDING,
				tooltip	: 'Компании',
				value	: 'company'
			},{
				iconCls	: CFJS.UI.iconCls.USER,
				tooltip	: 'Физ. лица',
				value	: 'customer'
			}]
		}].concat(new Array(8))
	},
	iconCls			: CFJS.UI.iconCls.FILE_TEXT,
	reference		: 'invoiceGrid',
	plugins			: 'gridfilters',
	selModel		: { type: 'rowmodel', mode: 'SINGLE', pruneRemoved: false },
	getRowClass: function(record, index, rowParams, store) {
		var me = this.ownerGrid;
		switch (me.parseIntProperty(me.stateField, record)) {
			case 1: return me.parseFloatProperty(me.paymentAmountField, record) === 0 ? 'red' : 'olive';
			case 2: return 'green';
			case 3: return 'blue';
			default: break;
		}
	},

	initComponent: function() {
		var me = this, actions = me.actions, vm;
		me.callParent();
		if (vm = me.lookupViewModel()) {
			vm.bind('{_itemName_.id}', function(id) {
				actions.printRecord.setDisabled(id <= 0);
				actions.referredDocuments.setDisabled(id <= 0);
			});
			vm.bind('{_itemName_Rules.canPay}', function(canPay) {
				actions.paying.setDisabled(!canPay);
			});
			vm.bind('{_itemName_Rules.canRemove}', function(canRemove) {
				actions.removeRecord.setDisabled(!canRemove);
			});
			vm.bind('{_itemName_Rules.canSign}', function(canSign) {
				actions.signRecord.setDisabled(!canSign);
			});
			vm.bind({ period: '{workingPeriod}', store: '{invoices}'}, function(data) {
				var period = data.period, store = data.store,
					startDate = (period || {}).startDate,
					endDate = (period || {}).endDate;
				store.getFilters().beginUpdate();
				if (startDate) store.filter({ id: 'startDate', property: me.dateField, type: 'date-time', operator: '!before', value: startDate }); 
				else store.removeFilter('startDate');
				if (endDate) store.filter({ id: 'endDate', property: me.dateField, type: 'date-time', operator: '!after', value: endDate }); 
				else store.removeFilter('endDate');
				store.getFilters().endUpdate();
			});
		}
	},
	
	renderColumns: function(form) {
		var me = this, columns = [],
			config = CFJS.orders.util.Invoice.findFormConfig(form),
			fields,	i, field, column;
		if (config && config.isInvoice) {
			me.amountField = config.amountField;
			me.dateField = config.dateField;
			me.paymentAmountField = config.paymentAmountField;
			me.stateField = config.stateField;
			if (fields = config.fields) {
				if (Ext.isObject(fields)) fields = [fields];
				for (i = 0; i < fields.length; i++) {
					column = me.renderColumn(field = fields[i]);
					if (!Ext.isEmpty(column)) {
						if (field.id === me.dateField) column.filter = false;
//						if (field.reference && field.reference.name === "Order") {
//							column.filter = { type: 'numeric' }
//						}
						columns.push(column);
					}
				}
				columns.sort(CFJS.document.util.Forms.columnSorter);
			}
		} else {
			delete me.amountField;
			delete me.dateField;
			delete me.paymentAmountField;
			delete me.stateField;
		}
		columns.push({
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'author.name',
			style		: { textAlign: 'center' },
			text		: 'Автор',
			tpl			: '<tpl if="author">{author.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'unit.name',
			style		: { textAlign: 'center' },
			text		: 'Подразделение',
			tpl			: '<tpl if="unit">{unit.name}</tpl>'
		});
		return columns;
	}
	
});
