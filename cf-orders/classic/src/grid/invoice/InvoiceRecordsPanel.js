Ext.define('CFJS.orders.grid.invoice.InvoiceRecordsPanel', {
	extend			: 'Ext.grid.Panel',
	xtype			: 'invoicerecordsgridpanel',
	requires		: [
		'Ext.grid.plugin.CellEditing',
		'Ext.grid.plugin.DragDrop',
		'Ext.ux.DataTip'
	],
	bind			: '{invoiceRecords}',
	columnLines		: true,
	config			: { readOnly: { $value: false, lazy: true } },
	emptyText		: 'No data to display',
	enableColumnHide: false,
	enableColumnMove: false,
	height			: 200,
	loadMask		: true,
	plugins			: [{
		ptype	: 'datatip', 
		tpl		: '{name}'
	},{ 
		ptype		: 'cellediting',
		clicksToEdit: 1,
		listeners	: {
			beforeedit: function(editor, context) {
				return !context.grid.readOnly;
			}
		}
	}],
	scrollable		: 'y',
	selModel		: { type: 'rowmodel', mode: 'MULTI', pruneRemoved: false },
	viewConfig		: { 
		plugins: { 
			dragGroup	: 'records-out',
			dropGroup	: 'records-in',
			ptype		: 'gridviewdragdrop' 
		} 
	},
	columns			: [{
		align				: 'left',
		dataIndex			: 'name',
		editor				: { allowBlank: false, selectOnFocus: true },
		flex				: 1,
		style				: { textAlign: 'center' },
		text				: 'Name',
		variableRowHeight	: true
	},{
		align		: 'center',
		dataIndex	: 'units',
		text		: 'Units',
		width		: 90
	},{
		dataIndex	: 'quantity',
		style		: { textAlign: 'center' },
		text		: 'Quantity',
		xtype		: 'numbercolumn',
		width		: 120
	},{
		dataIndex	: 'amount',
		formatter	: 'currency(" грн.", 2, true)',
		style		: { textAlign: 'center' },
		text		: 'Price',
		xtype		: 'numbercolumn',
		width		: 140
	},{
		dataIndex	: 'total',
		style		: { textAlign: 'center' },
		text		: 'Amount',
		tpl			: '{total:currency(" грн.", 2, true)}',
		xtype		: 'templatecolumn',
		width		: 160
	}],
	
	updateReadOnly: function(readOnly) {
		var dd = this.view.findPlugin('gridviewdragdrop');
		if (readOnly) dd.disable(); 
		else dd.enable();
	}
	
});