Ext.define('CFJS.orders.grid.OrderPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'ordergridpanel',
	requires		: [ 'Ext.toolbar.TextItem', 'Ext.ux.statusbar.StatusBar' ],
	bbar			: {
		xtype: 'statusbar',
		items: [{
			xtype		: 'tbtext',
			summary		: 'count',
			format		: '<b>Quantity</b>: {0}',
			renderValue	: function(value) {
				return Ext.util.Format.round(value || 0, 0);
			}
		},{
			xtype		: 'tbtext',
			summary		: 'amount',
			format		: '<b>Amount</b>: {0}',
			renderValue	: function(value) {
				return Ext.util.Format.currency(value || 0, ' грн.', 2, true);
			}
		},{
        	iconCls		: CFJS.UI.iconCls.REFRESH,
        	handler		: 'onSummaryRefresh'
		}]
	},
	bind			: '{orders}',
	enableColumnHide: false,
	iconCls			: CFJS.UI.iconCls.SHOPPING_CART,
	plugins			: 'gridfilters',
	reference		: 'orderGridPanel',
	title			: 'Orders',
	
	columns			: [{ 
		align		: 'center',
		dataIndex	: 'id',
		filter		: { type: 'number', emptyText: 'order number', itemDefaults: { minValue: 1 } },
		text		: 'Number'
	},{
		xtype		: 'datecolumn',   
		align		: 'center',
		dataIndex	: 'created',
		format		: Ext.Date.patterns.NormalDate,
		text		: 'Date',
		width		: 100
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'customer.fullName',
		filter		: { type: 'string', dataIndex: 'customer.fullName', emptyText: 'customer name' },
		flex		: 1, 
		style		: { textAlign: 'center' },
		text		: 'Customer',
		tpl			: '<tpl if="customer">{customer.name}</tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'post.name',
//		filter		: {
//			type		: 'string',
//			dataIndex	: 'post.employment.person.name',
//			itemDefaults: { emptyText: 'Укажите имя агента' }
//		},
		flex		: 1,
//		sortable	: false,
		style		: { textAlign: 'center' },
		text		: 'Agent',
		tpl			: '<tpl if="post"><tpl if="post.person">{post.person.name}<tpl else>Вакантно</tpl></tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'post.unit',
//		filter		: { type: 'string', emptyText: 'название подразделения' },
		flex		: 1,
//		sortable	: false,
		style		: { textAlign: 'center' },
		text		: 'Unit',
		tpl			: '<tpl if="post && post.unit">{post.unit.name}</tpl>'
	},{
		xtype		: 'templatecolumn',
		dataIndex	: 'amount',
		filter		: { type: 'number', emptyText: 'order amount', itemDefaults: { minValue: 0 } },
		style		: { textAlign: 'center' },
		text		: 'Amount',
		tpl			: '{amount:number("0,000,000.00##")} <tpl if="currency">{currency.name}</tpl>',
		width		: 150
	},{
		xtype		: 'datecolumn',
		align		: 'center',
		dataIndex	: 'validUntil',
		filter		: { type: 'date', dataType: 'date-time' },
		format		: Ext.Date.patterns.LongDateTime,
		text		: 'Pay deadline',
		width		: 170
	}],
	
	initComponent: function() {
		var me = this;
		me.on('filterchange', me.onFilterChange, me);
		me.callParent();
	},
	
	onFilterChange: function(store, filters) {
		if (store) store.summary();
	},
	
	renderValue: function(tbtext, value) {
	},
	
	updateSummary: function(key, value) {
		var tbtext = this.down('tbtext[summary=' + key + ']');
		if (tbtext) tbtext.setText(Ext.util.Format.format(tbtext.format || '{0}', tbtext.renderValue(value)));
	},
	
	updateSummaries: function(records) {
		var me = this, data = { COUNT : 'count', SUM: 'amount' },
			key, type;
		for (key in records) {
			type = records[key].get('type');
			if (data[type]) {
				me.updateSummary(data[type], records[key].get('value'));
				delete data[type];
			}
		}
		for (key in data) {
			me.updateSummary(data[key]);
		}
	}
	
});
