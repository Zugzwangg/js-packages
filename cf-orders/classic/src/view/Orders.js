Ext.define('CFJS.orders.view.Orders', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.view.Orders',
	xtype				: 'ordersview',
	requires			: [
		'CFJS.orders.grid.OrderPanel',
		'CFJS.orders.panel.OrderEditor',
		'CFJS.orders.view.OrdersController',
		'CFJS.orders.view.OrdersModel',
		'CFJS.orders.window.CustomerSelector',
		'CFJS.orders.window.invoice.InvoicesEditor',
		'CFJS.plugins.service.window.ServiceSelector'
	],
	controller			: 'orders.main',
	viewModel			: { type: 'orders.main' },
	session				: { schema: 'financial' },
	layout				: { type: 'card', anchor: '100%' },
	items				: [{
		xtype		: 'ordergridpanel',
		listeners	: { itemdblclick: 'onGridDblClick' }
	}],

	lookupGrid: function() {
		return this.down('ordergridpanel');
	}
	
});