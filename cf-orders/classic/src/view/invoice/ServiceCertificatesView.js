Ext.define('CFJS.orders.window.invoice.ServiceCertificatesView', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.view.ServiceCertificates',
	xtype				: 'servicecertificatesview',
	requires			: [
		'CFJS.orders.grid.invoice.ServiceCertificatesPanel',
		'CFJS.orders.view.invoice.ServiceCertificatesController',
		'CFJS.orders.view.invoice.ServiceCertificateDocumentsModel'
	],
	controller			: 'orders.service_certificates',
	layout				: { type: 'card', anchor: '100%' },
	session				: { schema: 'document' },
	viewModel			: { type: 'orders.service_certificates' },
	items				: [{
    	xtype		: 'servicecertificatesgrid'
	}],
	
	findAction: function(key) {
		var grid = this.lookupGrid();
		return grid.findAction.apply(grid, arguments);
	},
	
	lookupGrid: function() {
		return this.down('servicecertificatesgrid');
	}
});
