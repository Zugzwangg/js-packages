Ext.define('CFJS.orders.view.invoice.InvoicesListView', {
	extend			: 'Ext.view.View',
	xtype			: 'invoices.listview',
	bind			: '{invoices}',
	cls				: 'invoice-view',
	ddGroup 		: "InvoiceDD",
	enableDrop		: false,
	itemTpl			:[
		'<table><tbody>',
			'<tr><td class="label">Счет №</td><td class="data">{number} от {date:date("d.m.Y")} (<i>{[CFJS.orders.util.Invoice.renderState(values)]}</i>)</td></tr>',
			'<tr><td class="label">Плательщик</td><td class="data">{payer.name}</td></tr>',
			'<tr><td class="label">Сумма</td><td class="data amount">{amount:currency(" грн.", 2, true)}</td></tr>',
		'</tbody></table>'
	],
	itemCls			: 'invoice-source',
	itemSelector	: 'div.invoice-source',
	minWidth		: 310,
	overItemCls		: 'invoice-source-over',
	reference		: 'invoicesList',
	scrollable		: 'y',
	selectedItemCls	: 'invoice-source-selected',
	selectionModel	: { mode: 'MULTI' },
	trackOver		: true,
	width			: 310,
	
	initComponent: function() {
		var me = this;
		me.on('render', me.onListRender, me, { single: true });
		me.callParent();
	},
	
	enable: function() {
		var me = this;
		if (me.dropZone) me.dropZone.unlock();
		me.callParent();
	},

	disable: function() {
		var me = this;
		if (me.dropZone) me.dropZone.lock();
		me.callParent();
	},

	onListRender: function(view) {
		if (view.enableDrop) {
			view.dropZone = new Ext.dd.DropZone(view.el, Ext.apply({
				ddGroup: view.dropGroup || view.ddGroup,

				addRecords: function(targetNode, data, dropHandlers) {
					return this.fireViewEvent(view, ['addinvoicerecords', view.getRecord(targetNode), data, dropHandlers]);
				},

				canDrop: function(targetNode) {
					var invoice = view.getRecord(targetNode);
					return invoice && invoice.get('canSign') && !invoice.hasSign() && invoice !== view.lookupViewModel().getItem();
				},

				fireViewEvent: function(view, arguments) {
					var me = this, result;
					me.lock();
					result = view.fireEvent.apply(view, arguments);
					me.unlock();
					return result;
				},
				
				getTargetFromEvent: function(e) { 
					return e.getTarget(view.getItemSelector()); 
				},

				onContainerOver : function(dd, e, data) { 
					return this.dropAllowed; 
				},

				onContainerDrop : function(dd, e, data) {
					return this.removeRecords(data, true);
				},

				onNodeOver: function(node, dragZone, e, data) {
					var me = this;
					return me.canDrop(node) ? me.dropAllowed : me.dropNotAllowed;
				},

				onNodeDrop: function(targetNode, dragZone, e, data) {
					var me = this,
						dropHandled = false,
						dropHandlers = {
							wait: false,
							processDrop: function(reload) {
			                    dropHandled = true;
			                    return me.removeRecords(data, reload);
							},
							cancelDrop: function() {
			                    dropHandled = true;
			                }
						},
						performOperation = false;
					if (me.canDrop(targetNode)) {
						performOperation = me.addRecords(targetNode, data, dropHandlers);
						if (dropHandlers.wait) return;
						if (performOperation !== false && !dropHandled) performOperation = dropHandlers.processDrop();
					}
					return performOperation;
				},
				
				removeRecords: function(data, reload) {
					return this.fireViewEvent(data.view, ['removeinvoicerecords', data, reload]);
				}
				
			}), this.dropZone);
		}
	},

	destroy: function(){
		var me = this;
		me.dropZone = Ext.destroy(me.dropZone);
		me.callParent();
	}
    
});