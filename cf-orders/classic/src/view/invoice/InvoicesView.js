Ext.define('CFJS.orders.window.invoice.InvoicesView', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.view.Invoices',
	xtype				: 'invoicesview',
	requires			: [
		'CFJS.orders.grid.invoice.InvoicePanel',
		'CFJS.orders.view.invoice.ViewController',
		'CFJS.orders.view.invoice.ViewModel'
	],
	controller			: 'orders.invoices.view',
	layout				: 'card',
	session				: { schema: 'document' },
	viewModel			: { type: 'orders.invoices.view' },
	items				: [{
    	xtype		: 'invoicegrid',
    	listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	
	findAction: function(key) {
		var grid = this.lookupGrid();
		return grid.findAction.apply(grid, arguments);
	},
	
	lookupGrid: function() {
		return this.down('invoicegrid');
	}
});
