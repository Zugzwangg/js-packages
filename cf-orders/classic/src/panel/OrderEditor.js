Ext.define('CFJS.orders.panel.OrderEditor', {
	extend		: 'CFJS.panel.BaseEditor',
	xtype		: 'ordereditor',
	requires	: [
		'Ext.form.FieldContainer',
		'Ext.form.field.ComboBox',
		'Ext.form.field.Checkbox',
		'Ext.toolbar.Fill',
		'CFJS.orders.view.OrderModel',
		'CFJS.plugins.service.view.EditInOrderController',
		'CFJS.plugins.service.view.ServicesView'
	],
	actions		: {
		back			: { tooltip: 'Back to the list of orders' },
		documents		: {
			bind	: { disabled: '{!hasId}' },
			iconCls	: CFJS.UI.iconCls.BOOK,
			tooltip	: 'Documents',
			handler	: 'onDocumentsClick'
		},
		importService	: {
			bind	: { disabled: '{!hasId}' },
			iconCls	: CFJS.UI.iconCls.SEARCH,
			tooltip	: 'Find and import services',
			handler	: 'onImportService'
		}, 
		saveRecord		: {
			bind	: { disabled: '{!_itemName_.customer}' },
			formBind: false
		}
	},
	actionsMap	: {
		header: { items: [ 'back', 'tbspacer', 'importService', 'saveRecord', 'refreshRecord', 'documents' ] }
	},
	bind		: { title: 'Order  № {_itemName_.id} dd. {_itemName_.created:date("Y/m/d")}.' },
	bodyPadding	: '5 0 0 10',
	header		: { items: new Array(6) },
    reference	: 'orderEditor',
	viewModel	: {	type: 'orders.edit'	},
	items		: [{
		xtype			: 'fieldcontainer',
		defaultType		: 'displayfield',
		fieldDefaults	: { labelStyle: 'font-weight:bold', labelWidth: 80 },
		layout			: 'hbox',
		items			: [{
			bind		: '{customerName}',
			fieldLabel	: 'Customer',
			margin		: '0 5 0 0',
			required	: true
		},{
			xtype	: 'button',
			action	: 'editCustomer',
			border	: false,
			cls		: 'delete-focus-bg btn-fieldcontainer',
			tooltip	: 'Edit customer',
			iconCls	: CFJS.UI.iconCls.EDIT,
			handler	: 'onEditCustomer'
		},{
			xtype		: 'tbfill'
		},{
			bind		: '{_itemName_.amount:currency(" грн.", 2, true)}',
			fieldLabel	: 'To pay'
		},{
			bind		: '{_itemName_.paymentAmount:currency(" грн.", 2, true)}',
			fieldLabel	: 'Paid',
			margin		: '0 10 0 10'
		}]
	},{
		xtype		: 'plugins.service.view',
		controller	: 'plugins.service.editinorder',
		flex		: 1,
		viewModel	: {
			isMaster: false,
			itemName: 'serviceOrder',
//			data	: { hidePeriodPicker: true, hidePrintRecord: false },
			data	: { hidePeriodPicker: true },
			stores	: {
				services: {
					filters	: [{
						id		: 'order',
						property: 'order.id',
						type	: 'numeric',
						operator: 'eq',
						value	: '{_parentItemName_.id}'
					}]
				}
			}
		},
		listeners: { serviceschange: 'onServicesChange' }
	}],
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.down('button[action=editCustomer]').setHidden(readOnly);
		me.down('servicegrid').setReadOnly(readOnly);
		me.actions.importService.setHidden(readOnly);
		me.callParent(arguments);
	}

});
