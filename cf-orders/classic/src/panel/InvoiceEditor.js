Ext.define('CFJS.orders.panel.InvoiceEditor', {
	extend		: 'Ext.form.Panel',
	xtype		: 'invoiceeditor',
	requires	: [
		'Ext.form.FieldContainer',
		'Ext.form.field.ComboBox',
		'Ext.form.field.Checkbox',
		'Ext.toolbar.TextItem',
		'CFJS.orders.grid.invoice.InvoiceRecordsPanel'
	],
	bodyPadding	: '0 10 0 10',
	fieldDefaults	: {
		labelAlign	: 'top',
		labelWidth	: 100,
		msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
	},
	layout			: { type: 'vbox', pack: 'start', align: 'stretch' },
	bbar			: {
		xtype		: 'statusbar',
		defaultType	: 'tbtext',
		items		: [{
			bind: { text: '<b>To pay</b>: {_itemName_.amount:currency(" грн.", 2, true)}'}
		},{
			bind: { text: '<b>Paid</b>: {_itemName_.paymentAmount:currency(" грн.", 2, true)}'}
		},{
			bind: { text: '<b>Last payment</b>: {_itemName_.paymentDate:date("d.m.Y H:i:s")}'}
		}]
	},
	items			: [{
		xtype	: 'fieldcontainer',
		defaults: { margin: '0 0 0 10', required: true },
		layout	: 'hbox',
		items	: [{
			xtype		: 'displayfield',
			bind		: '{_itemName_.number}',
			fieldLabel	: 'Number',
			name		: 'number',
			reference	: 'invoiceNumber',
			width		: 90
		},{
			xtype		: 'datefield',
			bind		: { minValue: '{_orderName_.created}', readOnly: '{!_itemName_.canSign}', value	: '{_itemName_.date}' },
			blankText	: 'You must specify the date of the invoice',
			emptyText	: 'enter the date of the invoice',
			fieldLabel	: 'Date',
			format		: Ext.Date.patterns.NormalDate,
			margin		: 0,
			name		: 'date',
			width		: 120 
		},{
			xtype		: 'fieldcontainer',
			fieldLabel	: 'Payer',
			flex		: 1,
			layout		: 'hbox',
			items		: [{
				xtype		: 'displayfield',
				bind		: '{_itemName_.payer.name}',
				margin		: '0 5 0 0',
				name		: 'payer',
				reference	: 'invoicePayer'
			},{
				xtype	: 'button',
				bind	: { disabled: '{!_itemName_.canSign}' },
				border	: false,
				cls		: 'delete-focus-bg btn-fieldcontainer',
				tooltip	: 'Edit payer',
				iconCls	: CFJS.UI.iconCls.EDIT,
				handler	: 'onEditCustomer'
			}]
		},{
			xtype		: 'displayfield',
			bind		: '{_itemName_.state}',
			fieldLabel	: 'State',
			name		: 'state',
			reference	: 'invoiceState',
			renderer	: function (value, field) {
				var record = field.lookupViewModel().getItem();
				return record && record.isModel ? CFJS.orders.util.Invoice.renderState(record.getData(), value) : value;
			},
			width		: 90
		}]
	},{
		xtype		: 'invoicerecordsgridpanel',
		bind		: { readOnly: '{!_itemName_.canSign}', store: '{invoiceRecords}' },
		flex		: 1,
		header		: false,
		reference	: 'invoiceRecordsEditor',
		style		: Ext.theme.name === 'Crisp' ? { border: '1px solid silver;' } : null
	}]

});
