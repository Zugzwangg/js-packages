Ext.define('CFJS.orders.window.CustomerSelector', {
	extend		: 'Ext.window.Window',
	xtype		: 'customer-selector',
	requires	: [
		'Ext.tab.Panel',
		'CFJS.admin.grid.CompanyPanel',
		'CFJS.admin.grid.CustomerPanel',
		'CFJS.admin.panel.CompanyEditor',
		'CFJS.admin.panel.CustomerEditor',
		'CFJS.admin.view.CompaniesModel',
		'CFJS.admin.view.CustomersModel'
	],
	controller	: 'orders.customer-selector',
	config		: {
		actions			: {
			addRecord	: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Добавить клиента',
				tooltip	: 'Добавить нового клиента',
				handler	: 'onAddRecord'
			},
			clearFilters: {
				iconCls	: CFJS.UI.iconCls.CLEAR,
				title	: 'Очистить фильтры',
				tooltip	: 'Очистить все фильтры',
				handler	: 'onClearFilters'
			},
			editRecord	: {
				iconCls	: CFJS.UI.iconCls.EDIT,
				title	: 'Редaктировать клиента',
				tooltip	: 'Редaктировать текущего клиента',
				handler	: 'onEditRecord'
			},
			selectRecord: {
				iconCls	: CFJS.UI.iconCls.APPLY,
				title	: 'Выбрать клиента',
				tooltip	: 'Выбрать текущего клиента',
				handler	: 'onSelectRecord'
			}
		},
		actionsDisable	: [ 'editRecord', 'selectRecord' ],
		actionsMap		: {
			contextMenu	: { items: [ 'selectRecord', 'editRecord', 'addRecord' ] },
			header		: { items: [ 'selectRecord', 'editRecord', 'addRecord', 'clearFilters' ] }
		},
		contextMenu		: { items: new Array(3) }
	},
	layout		: 'fit',
	minWidth	: 600,
	minHeight	: 300,
	modal		: true,
	title		: 'Выбор клиента',
	header		: CFJS.UI.buildHeader({ items: new Array(4) }), 
	items		: [{
		xtype		: 'tabpanel',
		ui			: 'navigation',
		defaults	: {
			iconAlign	: 'top',
			bodyPadding	: 5,
			border		: false
		},
		reference	: 'tabPanel',
		tabBar		: { border: false },
		tabPosition	: 'left',
		tabRotation	: 0,
		items: [{
			xtype		: 'container',
			editorConfig: 'admin-panel-company-editor',
			layout		: { type: 'card', anchor: '100%' },
			session		: { schema: 'dictionary' },
			tabConfig	: { iconCls: CFJS.UI.iconCls.BUILDING, title: 'Компании', tooltip: 'Выбор компаний' },
			viewModel	: {	type: 'admin.companies', itemName: 'companySelector', stores: { companies: { filters: [{ property: 'customerType', type: 'string', operator: '!eq' }] } } },
			items		: [{
				xtype		: 'admin-gird-company-panel',
    	    	contextMenu	: false,
				header		: false,
				listeners	: { itemdblclick: 'onGridDblClick', itemcontextmenu: 'onShowContextMenu' }
			}],
			lookupGrid: function() {
				return this.down('admin-gird-company-panel');
			}
		},{
			xtype		: 'container',
			editorConfig: 'admin-customer-editor',
			layout		: { type: 'card', anchor: '100%' },
			tabConfig	: { iconCls: CFJS.UI.iconCls.USER, title: 'Физ. лица', tooltip: 'Выбор физ. лиц' },
			viewModel	: { type: 'admin.customers', itemName: 'customerSelector', stores: { customers: { filters: [{ property: 'type', type: 'string', operator: '!eq' }] } } },
			session		: { schema: 'dictionary' },
			items		: [{
				xtype		: 'admin-grid-customer-panel',
				contextMenu	: false,
				header		: false,
				listeners	: { itemdblclick: 'onGridDblClick', itemcontextmenu: 'onShowContextMenu' }
			}],
			lookupGrid: function() {
				return this.down('admin-grid-customer-panel');
			}
		}],
		listeners: { beforetabchange: 'onBeforeTabChange', tabchange: 'onTabChange' },

		lookupGrid: function() {
			var tab = this.getActiveTab();
			return tab ? tab.lookupGrid() : null;
		}
	}],
	listeners	: {
		afterrender: function(me) {
			var controller = me.getController();
    		if (controller) {
    			var lockActiveTab = controller.lockActiveTab;
    			controller.lockActiveTab = false;
    			me.getTabs().setActiveTab(controller.customerType || 0);
    			controller.lockActiveTab = lockActiveTab;
    		}
		}
	},

    getActiveTab: function() {
    	var tabs = this.getTabs();
    	return tabs ? tabs.getActiveTab() : null;
    },
    
    lookupGrid: function() {
    	var activeTab = this.getActiveTab();
		return activeTab ? activeTab.lookupGrid() : null;
    },
    
    getTabs: function() {
    	return this.lookup('tabPanel');
    }
    
});