Ext.define('CFJS.orders.window.invoice.ReferredDocuments', {
	extend				: 'Ext.window.Window',
	xtype				: 'referreddocuments',
	requires			: [
		'Ext.grid.Panel',
		'Ext.grid.plugin.CellEditing',
		'Ext.grid.selection.SpreadsheetModel',
		'CFJS.orders.view.invoice.ReferredDocumentsModel'
	],
	bind				: {
		title: 'Подчиненные документы счета № {_parentItemName_.number} от {_parentItemName_.date:date("d.m.Y")}.'
	},
	config				: {
		actions			: {
			addDocument		: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Добавить документ',
				tooltip	: 'Добавить новый документ',
				handler	: 'onAddDocument'
			},
			printDocument	: {
				iconCls	: CFJS.UI.iconCls.PRINT,
				title	: 'Печать документа',
				tooltip	: 'Печать выделенного документа',
				handler	: 'onPrintDocument'
			},
			reloadDocuments	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Обновить данные',
				tooltip	: 'Обновить данные',
				handler	: 'onReloadDocuments'
			},
			removeDocuments	: {
				iconCls	: CFJS.UI.iconCls.DELETE,
				title	: 'Анулировать документ',
				tooltip	: 'Анулировать выделенные документы',
				handler	: 'onRemoveDocuments'
			},
			saveDocuments	: {
				iconCls	: CFJS.UI.iconCls.SAVE,
				title	: 'Записать изменения',
				tooltip	: 'Записать внесенные изменения',
				handler	: 'onSaveDocuments'
			},
			signDocument	: {
				iconCls	: CFJS.UI.iconCls.SIGN,
				title	: 'Подписать документ',
				tooltip	: 'Подписать выделенный документ',
				handler	: 'onSignDocument'
			}
		},
		actionsDisable	: [ 'printDocument', 'removeDocuments', 'signDocument' ],
		contextMenu		: false
	},
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader(),
	height				: 400,
	iconCls				: CFJS.UI.iconCls.REFERRED,
	layout				: 'fit',
	session				: { schema: 'document' },
	viewModel			: { type: 'orders.invoices.referreddocuments' },
	width				: 800,
	items				: {
		xtype			: 'grid',
		bind			: '{referredDocuments}',
		columnLines		: true,
		emptyText		: 'Нет данных для отображения',
		loadMask		: true,
		plugins			: [{ 
			ptype		: 'cellediting',
			clicksToEdit: 1,
			pluginId	: 'editor',
			listeners	: {
				beforeedit: function(editor, context, eOpts) {
					if (context.record.hasSign()) return false;
					context.value = CFJS.orders.util.Invoice.getFieldValue(context.record, context.field);
				},
				validateedit: function(editor, context, eOpts) {
					CFJS.orders.util.Invoice.setFieldValue(context.value, context.record, context.field);
					return true;
				}
			}
		}],
		scrollable		: 'y',
		selModel		: { type: 'spreadsheet', cellSelect: false, mode: 'MULTI', pruneRemoved: false },
		sortableColumns	: false,
		columns			: [{
			align		: 'left',
			dataIndex	: 'name',
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Название'
		},{
			dataIndex	: 'numberField',
			renderer	: function(value, metaData, record) {
				return CFJS.orders.util.Invoice.renderDocumentNumber(record);
			},
			style		: { textAlign: 'center' },
			text		: 'Номер',
			width		: 90
		},{
			align		: 'center',
			dataIndex	: 'dateField',
			editor		: { xtype: 'datefield', allowBlank: false, emptyText: 'дата документа', format: Ext.Date.patterns.NormalDate, selectOnFocus: true }, 
			renderer	: function(value, metaData, record) {
				return CFJS.orders.util.Invoice.renderDocumentDate(record, Ext.Date.patterns.NormalDate);
			},
			text		: 'Дата',
			width		: 110
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'author.name',
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Автор',
			tpl			: '<tpl if="author">{author.name}</tpl>'
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'created',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Создан',
			width		: 170
		}]
	},
	
	afterRender: function() {
		var me = this; 
		me.callParent();
		me.onSelectionChange(null, []);
	},
	
	commitDocument: function(document, component, callback) {
		if (!document || document.hasSign()) return false;
		this.disableComponent(component, true);
		document.save({
			params	: { locale: CFJS.getLocale() },
			callback: callback || Ext.emptyFn
		});
	},
	
	commitSelectedDocument: function(component, callback) {
		this.commitDocument(this.lookupDocument(), component, callback);
	},

	lookupGrid: function() {
		return this.down('grid');
	},
	
	initComponent: function() {
		var me = this, actions = me.actions;
		me.header.items = [ actions.addDocument, actions.removeDocuments, actions.saveDocuments, actions.signDocument, actions.printDocument, actions.reloadDocuments ];
		me.callParent();
		var grid = me.lookupGrid();
		if (grid) {
			var vm = me.lookupViewModel(),
				editor = grid.getPlugin('editor');
			grid.view.menu = new Ext.menu.Menu({ 
				items: [
					actions.addDocument,
					actions.removeDocuments, 
					actions.saveDocuments, 
					'-',
					actions.signDocument,
					actions.printDocument
				] 
			});
			grid.on({ itemcontextmenu: 'onItemContextMenu', selectionchange: 'onSelectionChange' });
			if (vm) {
				vm.bind('{parentState}', 
					function(state) {
						actions.addDocument.setHidden(state === 3);
						actions.removeDocuments.setHidden(state === 3);
						actions.saveDocuments.setHidden(state === 3);
						actions.signDocument.setHidden(state === 3);
					});
				vm.bind('{_itemName_.id}', function(id) {
					actions.printDocument.setDisabled(id <= 0);
				});
				vm.bind({ canSign: '{_itemName_.canSign}', state: '{parentState}' }, 
					function(data) {
						var canSign = data.canSign && data.state < 3;
						actions.signDocument.setDisabled(!canSign);
					}
				);
			}
		}
	},
	
	lookupDocument: function() {
		return this.lookupViewModel().getItem();
	},
	
	onAddDocument: function(component) {
		if (!component || !component.form) return;
		var me = this, grid = me.lookupGrid(), store = grid.getStore(),
			match = function(record) {
				var form = record.get('form');
				return form && form.id === component.form.id;
			};
		if (store.findBy(match) === -1) {
			var document = me.lookupViewModel().setItem(component.form);
			if (document) {
				if (document.phantom) grid.getStore().add(document);
				if (document.isModel) grid.getSelectionModel().select(document, false, true);
			}
		} else Ext.Msg.alert('Добавление документа', 'Документ такого типа уже выписан.<br/>Нельзя выписывать несколько документов такого типа.'); 
	},

	onFormTemplatesLoad: function(store, records, successful) {
		var me = this, items = [], printDocument = me.actions.printDocument;
		if (successful && records && records.length > 0) {
			for (var i = 0; i < records.length; i++ ) {
				var rec = records[i];
				if (rec.get('type') === 'PDF_PRINT') {
					items.push({
						scope	: me,
						iconCls	: printDocument.initialConfig.iconCls,
						text	: rec.get('name'),
						template: rec,
						handler	: printDocument.initialConfig.handler
					});
				}
			}
		}
		var menu = items.length > 1 ? { items: items } : null,
			template = items.length === 1 ? items[0].template : null;
		printDocument.each(function(component) {
			component.template = template;
			if (Ext.isFunction(component.setMenu)) component.setMenu(menu, true);
		})
	},
	
	onItemContextMenu: function(panel, record, item, index, e) {
		e.stopEvent();
		if (panel.menu && panel.menu.isMenu) panel.menu.showAt(e.getXY());
		return false;
	},

	onPrintDocument: function(component) {
		var template = component && component.template;
		if (template && template.isPrintTemplate) template.print(this.lookupDocument());
	},
	
	onReferrerFormsLoad: function(store, records, successful) {
		var me = this, items = [], addDocument = me.actions.addDocument;
		if (successful && records && records.length > 0) {
			for (var i = 0; i < records.length; i++ ) {
				var rec = records[i];
				items.push({
					scope	: me,
					iconCls	: CFJS.UI.iconCls.FILE,
					text	: rec.get('name'),
					form	: rec,
					handler	: addDocument.initialConfig.handler
				});
			}
		}
		var menu = items.length > 1 ? { items: items } : null,
			form = items.length === 1 ? items[0].form : null;
		addDocument.each(function(component) {
			component.form = form;
			if (Ext.isFunction(component.setMenu)) component.setMenu(menu, true);
		})
	},
	
	onReloadDocuments: function(component) {
		var store = this.lookupGrid().getStore();
		store.rejectChanges();
		store.reload();
	},
	
	removeDocument: function(document, callback) {
		if (document && document.get('canSign')) {
			document.erase({callback: callback});
		} else if (Ext.isFunction(callback)) callback.call(this);
	},
	
	onRemoveDocuments: function(component) {
		var me = this, grid = me.lookupGrid(), selection,
			callback, i, last;
		if (grid && (selection = grid.getSelection())) {
			me.disableComponent(component, true);
			last = selection.length - 1;
			for (i = 0; i < selection.length; i++) {
				if (i === last) {
					callback = function(record, operation, success) {
						grid.getStore().reload();
						me.disableComponent(component);
					};
				} 
				me.removeDocument(selection[i], callback);
			}
		}
	},

	onSaveDocuments: function(component) {
		var me = this, documents = me.lookupGrid().getStore().getModifiedRecords(),
			disabled = component.disabled, i;
		for (i = 0; i < documents.length; i++) {
			me.commitDocument(documents[i], component, function(record, operation, success) {
				component.setDisabled(disabled);
			});
		}
	},

	onSelectionChange: function(selectable, selection) {
		var me = this;
		if (Ext.isObject(selection)) selection = selection.selectedRecords ? selection.selectedRecords.items : null;
		me.disableComponent(me.actions.removeDocuments, !selection || selection.length < 1);
		me.lookupViewModel().setItem(selection && selection.length > 0 ? selection[0] : false);
	},

	onSignDocument: function(component) {
		var document = this.lookupDocument(), disabled = component.disabled;
		if (!document || document.hasSign()) return false;
		component.disable();
		document.sign({ params: { locale: CFJS.getLocale() } });
	}

});