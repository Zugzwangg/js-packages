Ext.define('CFJS.orders.window.invoice.PaymentEditor', {
	extend				: 'Ext.window.Window',
	xtype				: 'paymenteditor',
	requires			: [ 'Ext.form.Panel', 'CFJS.orders.view.invoice.PaymentModel' ],
	bodyPadding			: '10 10 0 10',
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader({
		items		: [{
			action	: 'save',
			disabled: true,
			iconCls	: CFJS.UI.iconCls.SAVE,
			tooltip	: 'Записать внесенные изменения',
			handler	: 'onSaveTransaction'
		}]
	}), 
	layout				: 'anchor',
	title				: 'Оплата по счету',
	width				: 600,
	items				: {
		xtype		: 'form',
		anchor		: '100%',
		defaults	: {
			allowBlank		: false,
			anchor			: '100%',
			combineErrors	: true,
			defaults		: { margin: '0 0 0 5', required: true },
			defaultType		: 'textfield',
			layout			: 'hbox'
		},
		defaultType	: 'fieldcontainer',
		fieldDefaults	: {
			labelAlign	: 'top',
			labelWidth	: 100,
			msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
		},
		layout		: 'anchor',
		items		: [{
			items: [{
				bind			: '{_itemName_.number}',
				fieldLabel		: 'Номер документа',
				flex			: 1,
				margin			: 0,
				name			: 'number'
			},{
				xtype			: 'datefield',
				bind			: '{_itemName_.created}',
				fieldLabel		: 'Дата оплаты',
				format			: 'd.m.Y H:i:s',
				name			: 'created',
				width			: 180
			}]
		},{
			defaults		: {
				displayField	: 'name',
				editable		: false,
				forceSelection	: true,
				margin			: '0 0 0 5',
				publishes		: 'value',
				required		: true,
				valueField		: 'id'
			},
			defaultType	: 'combobox',
			items		: [{
				bind			: { value: '{_itemName_.payingType}' },
				emptyText		: 'выберите тип оплаты',
				fieldLabel		: 'Тип оплаты',
				margin			: 0,
				name			: 'payingType',
				queryMode		: 'local',
				store			: { type: 'payingtypes' }
			},{
				bind			: { value: '{_itemName_.system}' },
				autoLoadOnValue : true,
				emptyText		: 'выберите систему платежей',
				fieldLabel		: 'Система платежей',
				flex			: 1,
				name			: 'system',
				store			: { 
					type: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'proof_system' } },
					sorters	: [ 'name' ]
				}
			}]
		},{
			items: [{
					xtype			: 'numberfield',
					allowExponential: false,
					autoStripChars	: true,
					bind			: '{_itemName_.amount}',
					fieldLabel		: 'Сумма',
					flex			: 1,
					margin			: 0,
					name			: 'amount'
				},{
					xtype			: 'combobox',
					autoLoadOnValue : true,
					bind			: { value: '{_itemName_.currency}' },
					displayField	: 'code',
					editable		: false,
					emptyText		: 'выберите валюту',
					fieldLabel		: 'Валюта',
					forceSelection	: true,
					name			: 'currency',
					publishes		: 'value',
					store			: { type: 'currencies' },
					valueField		: 'code',
					width			: 85
			}]
		}],
		listeners: { validitychange: 'onValidityChange' }
	},
	
	onSaveTransaction: function(component) {
		var me = this;
		if (component) component.disable();
		me.getViewModel().getItem().save({ 
			success: function(record, operation) {
				me.fireEvent('paymentschange', record);
				me.close();
			}
		});		
	},
	
	onValidityChange: function(form, valid) {
		var button = this.down('button[action=save]');
		if (button) button.setDisabled(!valid);
	}
	
});