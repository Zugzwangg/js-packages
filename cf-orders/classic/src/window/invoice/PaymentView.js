Ext.define('CFJS.orders.window.invoice.PaymentView', {
	extend				: 'Ext.window.Window',
	xtype				: 'paymentview',
	requires			: [ 
		'Ext.grid.Panel',
		'CFJS.orders.view.invoice.PaymentModel',
		'CFJS.orders.window.invoice.PaymentEditor'
	],
	bind		: {
		title: 'Оплаты счета № {_parentItemName_.number} от {_parentItemName_.date:date("d.m.Y")}.'
	},
	config				: {
		actions		: {
			addPayment		: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Добавить оплату',
				tooltip	: 'Добавить новую оплату',
				handler	: 'onAddPayment'
			},
			reloadPayments	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Обновить данные',
				tooltip	: 'Обновить данные',
				handler	: 'onReloadPayments'
			}
		},
		actionsMap	: {
			header: { items: [ 'addPayment', 'reloadPayments' ] }
		},
		contextMenu	: false
	},
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader({ items: new Array(2) }), 
	height				: 400,
	layout				: 'fit',
	session				: { schema: 'financial' },
	viewModel			: { type: 'orders.invoices.payment' },
	width				: 800,
	items				: {
		xtype			: 'grid',
		bind			: '{transactions}',
		columnLines		: true,
		disableSelection: true,
		emptyText		: 'Нет данных для отображения',
		enableColumnHide: false,
		enableColumnMove: false,
		features: [{ ftype: 'summary', dock: 'bottom' }],
		loadMask		: true,
		plugins			: [{ 
			ptype	: 'datatip', 
			tpl		: [
				'<b>Платеж №</b>: <tpl if="proofDocument">{proofDocument.name}</tpl>',
				'<br/><b>Сумма</b>: {[this.formatAmount(values)]}', 
				'<br/><b>Описание</b>: {description}',
				{
					formatAmount: function(data) {
						var currency = (data.currency ? data.currency.name : null) || 'грн.',
							amount = data.amount < 0 ? -data.amount : data.amount;
						return Ext.util.Format.number(amount, "0,000,000.00") + ' ' + currency;
					}
				}
			] 
		}],
		scrollable		: 'y',
		sortableColumns	: false,
		columns			: [{
			xtype		: 'rownumberer'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'proofDocument.name',
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Номер',
			tpl			: '<tpl if="proofDocument">{proofDocument.name}</tpl>'
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'created',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Дата',
			width		: 170
		},{
			text		: 'Сумма',
			columns		: [{
				align			: 'right',
				dataIndex		: 'debt',
				renderer		: function(value, metaData, record) {
					if (!value) return '';
					var currency = record.get('currency');
					currency = (currency ? currency.name : null) || 'грн.'
					return Ext.util.Format.number(value, "0,000,000.00") + ' ' + currency;
				},
				style		: { textAlign: 'center' },
				summaryType		: 'sum',
				summaryFormatter: 'number("0,000,000.00")',
				text			: 'Дебет',
				width			: 190
			},{
				align			: 'right',
				dataIndex		: 'credit',
				renderer		: function(value, metaData, record) {
					if (!value) return '';
					var currency = record.get('currency');
					currency = (currency ? currency.name : null) || 'грн.'
					return Ext.util.Format.number(value, "0,000,000.00") + ' ' + currency;
				},
				style		: { textAlign: 'center' },
				summaryType		: 'sum',
				summaryFormatter: 'number("0,000,000.00")',
				text			: 'Кредит',
				width			: 190
			}]
		},{
			align		: 'left',
			dataIndex	: 'description',
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Описание'
		}]
	},
	
	lookupGrid: function() {
		return this.down('grid');
	},
	
	onAddPayment: function(component) {
		var me = this, vm = me.getViewModel();
		Ext.create({
			xtype		: 'paymenteditor',
			autoShow	: true,
			modal		: true,
			viewModel	: { type: 'editor', parent: vm, itemModel: null, itemName: null },
			listeners	: {
				paymentschange: function(transaction) {
					me.onReloadPayments();
					me.fireEvent('paymentschange', transaction);
				}
			}
		});
		vm.setItem();
	},
	
	onReloadPayments: function(component) {
		this.lookupGrid().getStore().reload();
	}
	
});