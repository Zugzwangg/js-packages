Ext.define('CFJS.orders.window.invoice.InvoicesEditor', {
	extend			: 'Ext.window.Window',
	xtype			: 'invoiceseditor',
	requires		: [
		'Ext.layout.container.Border',
		'CFJS.orders.panel.InvoiceEditor',
		'CFJS.orders.view.invoice.EditController',
		'CFJS.orders.view.invoice.EditModel',
		'CFJS.orders.view.invoice.InvoiceModel',
		'CFJS.orders.view.invoice.InvoicesListView',
		'CFJS.orders.window.invoice.PaymentView',
		'CFJS.orders.window.invoice.ReferredDocuments'
	],
	bind			: {
		title: 'Documents of order  # {_parentItemName_.id} dd {_parentItemName_.created:date("d.m.Y")}.'
	},
	config			: {
		actions			: {
			addRecord			: {
				iconCls	: CFJS.UI.iconCls.ADD,
				title	: 'Add new invoice',
				tooltip	: 'Add new invoice',
				handler	: 'onAddRecord'
			},
			addRecords	: {
				iconCls	: 'x-fa fa-long-arrow-up',
				title	: 'Add a record',
				tooltip	: 'Add selected records',
				handler	: 'onAddInvoiceRecords'
			},
			expandRecords		: {
				iconCls	: 'x-fa fa-expand',
				title	: 'Divide into components',
				tooltip	: 'Divide into service components'
			},
			paying				: {
				iconCls	: CFJS.UI.iconCls.MONEY,
				title	: 'Make a payment',
				tooltip	: 'Make an invoice payment',
				handler	: 'onPaying'
			},
			printRecord			: {
				iconCls	: CFJS.UI.iconCls.PRINT,
				title	: 'Print invoice',
				tooltip	: 'Print selected invoice',
				handler	: 'onPrintRecord'
			},
			referredDocuments	: {
				iconCls	: CFJS.UI.iconCls.REFERRED,
				title	: 'Subordinate documents',
				tooltip	: 'Working with subordinate documents',
				handler	: 'onReferredDocuments'
			},
			reloadRecords		: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data',
				handler	: 'onReloadInvoiceRecords'
			},
			reloadStore			: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				title	: 'Update the data',
				tooltip	: 'Update the data',
				handler	: 'onReloadStore'
			},
			removeRecord		: {
				iconCls	: CFJS.UI.iconCls.DELETE,
				title	: 'Cancel invoice',
				tooltip	: 'Cancel selected invoices',
				handler	: 'onRemoveRecord'
			},
			removeRecords		: {
				iconCls	: CFJS.UI.iconCls.CANCEL,
    			title	: 'Delete records',
    			tooltip	: 'Remove selected records',
				handler	: 'onRemoveInvoiceRecords'
			},
			saveRecord			: {
				iconCls	: CFJS.UI.iconCls.SAVE,
				title	: 'Save the changes',
				tooltip	: 'Save making changes',
				handler	: 'onSaveRecord'
			},
			signRecord			: {
				iconCls	: CFJS.UI.iconCls.SIGN,
				title	: 'Sign invoice',
				tooltip	: 'Sing selected invoice',
				handler	: 'onSignRecord'
			}
		},
		actionsDisable	: [ 'addRecords', 'expandRecords', 'paying', 'printRecord', 
		              	    'referredDocuments', 'removeRecord', 'removeRecords', 'saveRecord', 'signRecord' ],
		contextMenu		: false
	},
	controller		: 'orders.invoices.edit',
	defaults		: { collapsible: true, split: true },
	header			: CFJS.UI.buildHeader(),
	layout			: 'border',
	minWidth		: 600,
	minHeight		: 300,
	modal			: true,
	session			: { schema: 'document' },
	viewModel		: { type: 'orders.invoices.edit' },
	unspecifiedText	: 'Unspecified services',
	
	initComponent: function() {
		var me = this, actions = me.actions,
			taxes = Ext.getStore('internal_taxes'),
			expandRecords = [], grid, vm;
		if (taxes) {
			taxes.each(function(record) {
				if (record) expandRecords.push({
					iconCls		: CFJS.UI.iconCls.MONEY,
					handler		: 'onExpandRecords',
					hideOnClick	: false, 
					scope		: me.lookupController(),
					text		: record.get('name'),
					taxCode		: record.get('code')
				});
			});
		}
		
		if (expandRecords.length > 0) actions.expandRecords.initialConfig.menu = {items: expandRecords};

		me.header.items = [ actions.addRecord, actions.removeRecord, actions.saveRecord, actions.removeRecords, actions.signRecord, actions.paying, 
							actions.printRecord, actions.referredDocuments, actions.reloadStore ];
		me.items = [{
			xtype		: 'invoices.listview',
			dropGroup	: 'records-out',
			enableDrop	: true,
			listeners	: { addinvoicerecords: 'addRecords', itemcontextmenu: 'onItemContextMenu', selectionchange: 'onSelectionChange' },
			maxWidth	: 400,
			menu		: new Ext.menu.Menu({ 
				items: [ actions.addRecord, actions.removeRecord, '-', actions.signRecord, actions.paying, actions.printRecord, actions.referredDocuments ] 
			}),
			region		: 'west'
		},{
			xtype		: 'container',
		    defaults	: { collapsible: true, split: true },
		    layout		: 'border',
			reference	: 'invoiceEditor',
			region		: 'center',
			viewModel	: { type: 'orders.invoice' },
			items		: [{
				xtype		: 'invoiceeditor',
		        header		: false,
				region		: 'center'
			},{
				xtype		: 'invoicerecordsgridpanel', 
				bind		: '{unlinkedRecords}',
				id			: 'newRecordsPanel',
				height		: 190,
				header		: CFJS.UI.buildHeader({
					items			: [ actions.addRecords, actions.expandRecords, actions.reloadRecords ],
					itemPosition	: 2,
					titlePosition	: 1
				}),
				minHeight	: 125,
				maxHeight	: 350,
				reference	: 'newRecordsPanel',
				region		: 'south',
				title		: me.unspecifiedText,
				viewConfig	: {
					menu		: new Ext.menu.Menu({ items: [ actions.addRecords, actions.expandRecords, actions.reloadRecords ] }),
					plugins		: {
						dragGroup	: 'records-in',
						dropGroup	: 'records-out',
						ptype		: 'gridviewdragdrop'
					},
					listeners	: { beforedrop: 'beforeDropRecords', itemcontextmenu: 'onItemContextMenu', selectionchange: 'onNewRecordsSelectionChange' }
				}
			}],
			
			getItem: function() { return this.getViewModel().getItem(); },
			
			setItem: function(invoice) {
				var me = this,
					editor = me.down('invoiceeditor'), 
					grid = editor.down('invoicerecordsgridpanel'),
					panel = me.down('#newRecordsPanel');
				grid.getSelectionModel().deselectAll();
				invoice = me.getViewModel().setItem(invoice);
				editor.setHidden(!invoice);
				actions.removeRecord.setDisabled(!invoice || !invoice.get('canRemove'));
				actions.addRecords.setDisabled(!panel.getSelection() || panel.getSelection().length < 1 || !invoice || !invoice.get('canSign'));
				return invoice;
			}
		}];
		me.callParent();
		if (grid = me.lookup('invoiceRecordsEditor')) {
			grid.view.menu = new Ext.menu.Menu({ 
				items: [
					actions.removeRecords, 
					actions.saveRecord, 
					actions.removeRecord,
					'-',
					actions.signRecord,
					actions.paying,
					actions.printRecord,
					actions.referredDocuments
				] 
			});
			grid.view.on({ beforedrop: 'beforeDropNewRecords', removeinvoicerecords: 'removeRecords' });
			grid.on({ itemcontextmenu: 'onItemContextMenu', selectionchange: 'onRecordsSelectionChange' });
			if (vm = grid.lookupViewModel()) {
				vm.bind('{_itemName_.id}', function(id) {
					actions.printRecord.setDisabled(id <= 0);
					actions.referredDocuments.setDisabled(id <= 0);
				});
				vm.bind('{_itemName_.canPay}', function(canPay) {
					actions.paying.setDisabled(!canPay);
				});
				vm.bind('{_itemName_.canSign}', function(canSign) {
					actions.removeRecords.setHidden(!canSign);	
					actions.saveRecord.setDisabled(!canSign);
					actions.signRecord.setDisabled(!canSign);
				});
			}
		}
	},
	
	afterRender: function() {
		var me = this;
		me.callParent();
		me.lookupController().onSelectionChange(null, []);
	}

});