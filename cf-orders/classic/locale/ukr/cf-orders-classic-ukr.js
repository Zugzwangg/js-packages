Ext.define('CFJS.orders.locale.ukr.admin.grid.ClientPanel', {
	override: 'CFJS.admin.grid.ClientPanel',
	config	: {
		actions			: {
			purchaseHistory	: {
				title	: 'Кошик покупок', 
				tooltip	: 'Переглянути історію покупок',
				menu	: {
					items: [{
						iconCls	: CFJS.UI.iconCls.SHOPPING_CART,
						handler	: 'onShowPurchaseHistory',
						text	: 'Замовлення',
						tooltip	: 'Переглянути історію замовлень',
						viewId	: 'orders'
					},{
						iconCls	: CFJS.UI.iconCls.TAGS,
						handler	: 'onShowPurchaseHistory',
						text	: 'Послуги',
						tooltip	: 'Переглянути історію покупок послуг',
						viewId	: 'services'
					}]
				}
			}
		}
	}
});
Ext.define('CFJS.orders.locale.ukr.grid.invoice.InvoicePanel', {
	override: 'CFJS.orders.grid.invoice.InvoicePanel',
	config	: {
		actions: {
			paying			: {
				title	: 'Внести оплату',
				tooltip	: 'Внести оплату за рахунком'
			},
			printRecord			: {
				title	: 'Друк рахунку',
				tooltip	: 'Друк виділеного рахунка'
			},
			referredDocuments	: {
				title	: 'Підпорядковані документи',
				tooltip	: 'Робота з підпорядкованими документами'
			},
			removeRecord		: {
				title	: 'Анулювати рахунок',
				tooltip	: 'Анулювати виділені рахунки'
			},
			signRecord			: {
				title	: 'Підписати рахунок',
				tooltip	: 'Підписати виділений рахунок'
			}
		}
	}
});
Ext.define('CFJS.orders.locale.ukr.grid.invoice.InvoiceRecordsPanel', {
	override	: 'CFJS.orders.grid.invoice.InvoiceRecordsPanel',
	emptyText	: 'Немає даних для відображення',
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns: [{ text: 'Найменування' },{ text: 'Одиниці' },{ text: 'Кількість' },{ text: 'Ціна' },{ text: 'Сума' }]
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ukr.grid.invoice.ServiceCertificatesPanel', {
	override: 'CFJS.orders.grid.invoice.ServiceCertificatesPanel',
	config	: {
		actions: {
			printDocument	: {
				title	: 'Друк документа',
				tooltip	: 'Друк виділеного документа'
			},
			reloadDocuments	: {
				title	: 'Оновити дані',
				tooltip	: 'Оновити дані'
			},
			removeDocuments	: {
				title	: 'Анулювати документ',
				tooltip	: 'Анулювати виділені документи'
			},
			signDocument	: {
				title	: 'Підписати документ',
				tooltip	: 'Підписати виділений документ'
			}
		}
	}
});
Ext.define('CFJS.orders.locale.ukr.grid.OrderPanel', {
	override: 'CFJS.orders.grid.OrderPanel',
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			bbar	: {
				items: [{ format: '<b>Кількість</b>: {0}' }, { format: '<b>Сума</b>: {0}' }]
			},
			columns	: [{
				filter	: { emptyText: 'номер замовлення' },
				text	: 'Номер'
			},{
				text	: 'Дата'
			},{
				filter	: { emptyText: 'найменування замовника' },
				text	: 'Замовник'
			},{
				text	: 'Агент'
			},{
				text	: 'Підрозділ'
			},{
				filter	: { emptyText: 'сума замовлення' },
				text	: 'Сума'
			},{
				text	: 'Термін оплати'
			}],
			title	: 'Замовлення'
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ru.panel.InvoiceEditor', {
	override: 'CFJS.orders.panel.InvoiceEditor',
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			bbar: {
				items: [{
					bind: { text: '<b>До сплати</b>: {_itemName_.amount:currency(" грн.", 2, true)}'}
				},{
					bind: { text: '<b>Сплачено</b>: {_itemName_.paymentAmount:currency(" грн.", 2, true)}'}
				},{
					bind: { text: '<b>Останній платіж</b>: {_itemName_.paymentDate:date("d.m.Y H:i:s")}'}
				}]
			},
			items: [{
				items: [{
					fieldLabel	: 'Номер'
				},{
					blankText	: 'Ви повинні вказати дату рахунку',
					emptyText	: 'введіть дату рахунку',
					fieldLabel	: 'Дата'
				},{
					fieldLabel	: 'Платник',
					items		: [{},{ tooltip: 'Редагувати платника' }]
				},{
					fieldLabel	: 'Статус'
				}]
			}]
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ukr.panel.OrderEditor', {
	override: 'CFJS.orders.panel.OrderEditor',
	config	: {
		actions	: {
			back			: { tooltip: 'Повернутись до списку замовлень' },
			documents		: { tooltip: 'Документи' },
			importService	: { tooltip: 'Знайти та імпортувати послуги' }
		},
		bind	: { title: 'Замовлення № {_itemName_.id} від {_itemName_.created:date("d.m.Y")}.' }
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			items	: [{
				items: [{ fieldLabel: 'Замовник' },{ tooltip: 'Редагувати клієнта' },{},{ fieldLabel: 'До оплати' },{ fieldLabel: 'Сплачено' }]
			}]
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ru.window.invoice.InvoicesEditor', {
	override: 'CFJS.orders.window.invoice.InvoicesEditor',
	config	: {
		actions: {
			addRecord			: {
				title	: 'Додати рахунок',
				tooltip	: 'Додати новий рахунок'
			},
			addRecords	: {
    			title	: 'Додати запис',
				tooltip	: 'Додати виділені записи'
			},
			expandRecords		: {
				title	: 'Виділити складові',
				tooltip	: 'Виділити складові послуги'
			},
			paying				: {
				title	: 'Внести оплату',
				tooltip	: 'Внести оплату за рахунком'
			},
			printRecord			: {
				title	: 'Друк рахунку',
				tooltip	: 'Друк виділеного рахунка'
			},
			referredDocuments	: {
				title	: 'Підпорядковані документи',
				tooltip	: 'Робота з підпорядкованими документами'
			},
			reloadRecords		: {
    			title	: 'Оновити дані',
    			tooltip	: 'Оновити дані'
			},
			reloadStore			: {
    			title	: 'Оновити дані',
    			tooltip	: 'Оновити дані'
			},
			removeRecord		: {
				title	: 'Анулювати рахунок',
				tooltip	: 'Анулювати виділені рахунки'
			},
			removeRecords		: {
    			title	: 'Видалити записи',
    			tooltip	: 'Видалити обрані записи'
			},
			saveRecord			: {
				title	: 'Записати зміни',
				tooltip	: 'Записати внесені зміни'
			},
			signRecord			: {
				title	: 'Підписати рахунок',
				tooltip	: 'Підписати виділений рахунок'
			}
		},
		bind		: {
			title: 'Документи замовлення № {_parentItemName_.id} від {_parentItemName_.created:date("d.m.Y")}.'
		},
		unspecifiedText: 'Не виставлені послуги'
	}
});
