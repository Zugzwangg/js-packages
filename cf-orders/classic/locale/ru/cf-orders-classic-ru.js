Ext.define('CFJS.orders.locale.ru.admin.grid.ClientPanel', {
	override: 'CFJS.admin.grid.ClientPanel',
	config	: {
		actions			: {
			purchaseHistory	: {
				title	: 'Корзина покупок', 
				tooltip	: 'Просмотреть историю покупок',
				menu	: {
					items: [{
						iconCls	: CFJS.UI.iconCls.SHOPPING_CART,
						handler	: 'onShowPurchaseHistory',
						text	: 'Заказы',
						tooltip	: 'Просмотреть историю заказов',
						viewId	: 'orders'
					},{
						iconCls	: CFJS.UI.iconCls.TAGS,
						handler	: 'onShowPurchaseHistory',
						text	: 'Услуги',
						tooltip	: 'Просмотреть историю покупок услуг',
						viewId	: 'services'
					}]
				}
			}
		}
	}
});
Ext.define('CFJS.orders.locale.ru.grid.invoice.InvoicePanel', {
	override: 'CFJS.orders.grid.invoice.InvoicePanel',
	config	: {
		actions: {
			paying			: {
				title	: 'Внести оплату',
				tooltip	: 'Внести оплату по счету'
			},
			printRecord			: {
				title	: 'Печать счета',
				tooltip	: 'Печать выделенного счета'
			},
			referredDocuments	: {
				title	: 'Подчиненные документы',
				tooltip	: 'Работа с подчиненными документами'
			},
			removeRecord		: {
				title	: 'Аннулировать счет',
				tooltip	: 'Аннулировать выделенные счета'
			},
			signRecord			: {
				title	: 'Подписать счет',
				tooltip	: 'Подписать выделенный счет'
			}
		}
	}
});
Ext.define('CFJS.orders.locale.ru.grid.invoice.InvoiceRecordsPanel', {
	override	: 'CFJS.orders.grid.invoice.InvoiceRecordsPanel',
	emptyText	: 'Нет данных для отображения',
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns: [{ text: 'Наименование' },{ text: 'Единицы' },{ text: 'Количество' },{ text: 'Цена' },{ text: 'Сумма' }]
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ru.grid.invoice.ServiceCertificatesPanel', {
	override: 'CFJS.orders.grid.invoice.ServiceCertificatesPanel',
	config	: {
		actions: {
			printDocument	: {
				title	: 'Печать документа',
				tooltip	: 'Печать выделенного документа'
			},
			reloadDocuments	: {
				title	: 'Обновить данные',
				tooltip	: 'Обновить данные'
			},
			removeDocuments	: {
				title	: 'Аннулировать документ',
				tooltip	: 'Аннулировать выделенные документы'
			},
			signDocument	: {
				title	: 'Подписать документ',
				tooltip	: 'Подписать выделенный документ'
			}
		}
	}
});
Ext.define('CFJS.orders.locale.ru.grid.OrderPanel', {
	override: 'CFJS.orders.grid.OrderPanel',
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			bbar	: {
				items: [{ format: '<b>Количество</b>: {0}' }, { format: '<b>Сумма</b>: {0}' }]
			},
			columns	: [{
				filter	: { emptyText: 'номер заказа' },
				text	: 'Номер'
			},{
				text	: 'Дата'
			},{
				filter	: { emptyText: 'наименование заказчика' },
				text	: 'Заказчик'
			},{
				text	: 'Агент'
			},{
				text	: 'Подразделение'
			},{
				filter	: { emptyText: 'сумма заказа' },
				text	: 'Сумма'
			},{
				text	: 'Срок оплаты'
			}],
			title	: 'Заказы'
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ru.panel.InvoiceEditor', {
	override: 'CFJS.orders.panel.InvoiceEditor',
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			bbar: {
				items: [{
					bind: { text: '<b>К оплате</b>: {_itemName_.amount:currency(" грн.", 2, true)}'}
				},{
					bind: { text: '<b>Оплачено</b>: {_itemName_.paymentAmount:currency(" грн.", 2, true)}'}
				},{
					bind: { text: '<b>Последний платеж</b>: {_itemName_.paymentDate:date("d.m.Y H:i:s")}'}
				}]
			},
			items: [{
				items: [{
					fieldLabel	: 'Номер'
				},{
					blankText	: 'Вы должны указать дату счета',
					emptyText	: 'введите дату счета',
					fieldLabel	: 'Дата'
				},{
					fieldLabel	: 'Плательщик',
					items		: [{},{ tooltip: 'Редактировать плательщика' }]
				},{
					fieldLabel	: 'Статус'
				}]
			}]
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ru.panel.OrderEditor', {
	override: 'CFJS.orders.panel.OrderEditor',
	config	: {
		actions	: {
			back			: { tooltip: 'Назад к списку заказов' },
			documents		: { tooltip: 'Документы' },
			importService	: { tooltip: 'Найти и импортировать услуги' }
		},
		bind	: { title: 'Заказ № {_itemName_.id} от {_itemName_.created:date("d.m.Y")}.' }
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			items	: [{
				items: [{ fieldLabel: 'Заказчик' },{ tooltip: 'Редактировать клиента' },{},{ fieldLabel: 'К оплате' },{ fieldLabel: 'Оплачено' }]
			}]
		});
		me.callParent();
	}
});
Ext.define('CFJS.orders.locale.ru.window.invoice.InvoicesEditor', {
	override: 'CFJS.orders.window.invoice.InvoicesEditor',
	config	: {
		actions: {
			addRecord			: {
				title	: 'Добавить счет',
				tooltip	: 'Добавить новый счет'
			},
			addRecords	: {
				title	: 'Добавить запись',
				tooltip	: 'Добавить выделенные записи'
			},
			expandRecords		: {
				title	: 'Выделить составляющие',
				tooltip	: 'Выделить составляющие услуги'
			},
			paying				: {
				title	: 'Внести оплату',
				tooltip	: 'Внести оплату по счету'
			},
			printRecord			: {
				title	: 'Печать счета',
				tooltip	: 'Печать выделенного счета'
			},
			referredDocuments	: {
				title	: 'Подчиненные документы',
				tooltip	: 'Работа с подчиненными документами'
			},
			reloadRecords		: {
				title	: 'Обновить данные',
				tooltip	: 'Обновить данные'
			},
			reloadStore			: {
				title	: 'Обновить данные',
				tooltip	: 'Обновить данные'
			},
			removeRecord		: {
				title	: 'Анулировать счет',
				tooltip	: 'Анулировать выделенные счета'
			},
			removeRecords		: {
				title	: 'Удалить запись',
				tooltip	: 'Удалить выделенные записи'
			},
			saveRecord			: {
				title	: 'Записать изменения',
				tooltip	: 'Записать внесенные изменения'
			},
			signRecord			: {
				title	: 'Подписать счет',
				tooltip	: 'Подписать выделенный счет'
			}
		},
		bind		: {
			title: 'Документы заказа № {_parentItemName_.id} от {_parentItemName_.created:date("d.m.Y")}.'
		},
		unspecifiedText	: 'Не выставленные услуги'
	}
});
