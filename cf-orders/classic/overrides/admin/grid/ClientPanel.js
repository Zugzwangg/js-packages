Ext.define('CFJS.orders.overrides.admin.grid.ClientPanel', {
	override: 'CFJS.admin.grid.ClientPanel',
	config	: {
		actions			: {
			purchaseHistory	: {
				iconCls	: CFJS.UI.iconCls.HISTORY,
				title	: 'Shopping basket', 
				tooltip	: 'View purchase history',
				menu	: {
					items: [{
						iconCls	: CFJS.UI.iconCls.SHOPPING_CART,
						handler	: 'onShowPurchaseHistory',
						text	: 'Orders',
						tooltip	: 'View order history',
						viewId	: 'orders'
					},{
						iconCls	: CFJS.UI.iconCls.TAGS,
						handler	: 'onShowPurchaseHistory',
						text	: 'Services',
						tooltip	: 'View purchase history service',
						viewId	: 'services'
					}]
				}
			}
		},
		actionsMap		: {
			contextMenu	: {
				items: [ 'addRecord', 'removeRecord', 'editRecord', 'purchaseHistory', 'browseFiles' ]
			},
			header		: {
				items: [ 'addRecord', 'removeRecord', 'editRecord', 'purchaseHistory', 'browseFiles', 'clearFilters', 'reloadStore' ]
			}
		},
		actionsDisable	: [ 'removeRecord', 'editRecord', 'purchaseHistory', 'browseFiles' ],
		contextMenu		: { items: new Array(5) }
	},
	header	: { items: new Array(7) }
	
});