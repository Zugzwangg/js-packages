Ext.define('CFJS.orders.overrides.admin.panel.ClientEditor', {
	override		: 'CFJS.admin.panel.ClientEditor',
	config			: {
		actions			: {
			purchaseHistory	: {
				iconCls	: CFJS.UI.iconCls.HISTORY,
				title	: 'Корзина покупок', 
				tooltip	: 'Просмотреть историю покупок',
				menu	: {
					items: [{
						iconCls	: CFJS.UI.iconCls.SHOPPING_CART,
						handler	: 'onShowPurchaseHistory',
						text	: 'Заказы',
						tooltip	: 'Просмотреть историю заказов',
						viewId	: 'orders'
					},{
						iconCls	: CFJS.UI.iconCls.TAGS,
						handler	: 'onShowPurchaseHistory',
						text	: 'Услуги',
						tooltip	: 'Просмотреть историю покупок услуг',
						viewId	: 'services'
					}]
				}
			}
		},
		actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord', 'purchaseHistory', 'browseFiles' ] } }
	},
	header			: { items: new Array(6) },

	onChangeClient: function(id) {
		var purchaseHistory = this.actions.purchaseHistory;
		if (purchaseHistory) purchaseHistory.setDisabled(id <= 0);
		this.callParent(arguments);
	}
    
});
