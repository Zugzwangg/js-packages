Ext.define('CFJS.orders.store.DocumentConfig', {
	extend		: 'Ext.data.Store',
	alias		: 'store.document_config',
	model		: 'CFJS.orders.model.DocumentConfig',
	storeId		: 'documentConfig',
	proxy		: {
		type	: 'ajax',
		url		: 'resources/data/documents_config.json',
		noCache	: false
	}
});
