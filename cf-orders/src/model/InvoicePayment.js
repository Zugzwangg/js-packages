Ext.define('CFJS.orders.model.InvoicePayment', {
	extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Financial' ],
	dictionaryType	: 'invoice_payment',
	identifier		: { type: 'sequential', prefix: 'IP_' },
	idProperty		: 'storeId',
	schema			: 'financial',
	proxy: {
		api			: { 
			create	: { method: 'payment.invoice', transaction: 'payment' },
			update	: { method: 'payment.invoice', transaction: 'payment' }
		}
	},
	fields			: [
		{ name: 'storeId',		persist: false },
		{ name: 'form',			type: 'int',	critical: true, defaultValue: 0 },
		{ name: 'locale',		type: 'string',	critical: true, defaultValue: CFJS.getLocale() },
		{ name: 'payingType',	type: 'string',	critical: true, defaultValue: 'CASH' },
		{ name: 'system',		type: 'int',	critical: true, defaultValue: 1 },
		{ name: 'number',		type: 'string',	critical: true },
		{ name: 'currency', 	type: 'string',	critical: true, defaultValue: 'UAH' },
		{ name: 'amount',		type: 'number', critical: true, defaultValue: 0 },
		{ name: 'created',		type: 'date',	critical: true }
	]
});