Ext.define('CFJS.orders.model.ServiceCertificateDocument', {
	extend	: 'CFJS.model.document.Document',
	fields	: [
		{ name: 'number',	type: 'int',	persist: false },
		{ name: 'date',		type: 'date',	persist: false },
		{ name: 'state',	type: 'int',	persist: false, allowNull: true }
	]
});