Ext.define('CFJS.orders.model.DocumentConfig', {
	extend		: 'Ext.data.Model',
	idProperty	: 'customerType',
	fields		: [
		{ name: 'customerType',				type: 'string' }, 
		{ name: 'invoice',					type: 'auto' }, 
		{ name: 'invoiceForm', 				type: 'int', mapping: 'invoice.form' },
		{ name: 'invoiceStates', 			type: 'auto',
			calculate: function(data) {
				var invoice = data.invoice || {},
					fields = invoice && invoice.fields,
					stateField = invoice.stateField,
					value = [], field, i = 0, key;
				if (fields && stateField) {
					if (!Ext.isArray(fields)) fields = [fields];
					for (; i < fields.length; i++) {
						if ((field = fields[i]) && field.id === stateField) {
							value = (field.mask || '').split(';');
							for (key = 0; key < value.length; key++) {
								value[key] = { id: key, name: value[key] };
							}
							break;
						}
					}
				}
				return value;
			}
		},
		{ name: 'serviceCertificate',		type: 'auto' }, 
		{ name: 'serviceCertificateForm', 	type: 'int', mapping: 'serviceCertificate.form' }
	]
});