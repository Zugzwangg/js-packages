Ext.define('CFJS.orders.model.InvoiceDocument', {
	extend	: 'CFJS.orders.model.ServiceCertificateDocument',
	
	getPaymentAmount: function(field) {
		return this.getFloatProperty(field);
	},
	
	getState: function(field) {
		return this.getIntProperty(field);
	},
	
	setPayer: function(payer, form) {
		var me = this;
		me.set('fieldPayer', payer);
		if (form) me.set('form', form);
	},

	setState: function(field, state) {
		this.setPropertyValue(field, state || 0);
	}
	
});