Ext.define('CFJS.orders.api.reader.InvoiceJson', {
	extend				: 'CFJS.api.reader.Json',
	alias				: 'reader.json.invoice',
	config				: {
		rootProperty	: 'response.transaction.list.document',
		summaryModel	: 'CFJS.model.Summary',
		transform: function(data) {
			data = this.callParent([data]);
			if (data && data.response && data.response.success) {
				
			}
			return data;
		}
	}
	
});