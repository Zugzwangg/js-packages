//	@define CFJS.orders.util.Invoice
//	@require Ext.util.Format
//	@require CFJS
//	@require CFJS.api.Api
Ext.define('CFJS.orders.util.Invoice', {
	singleton	: true,
	requires	: [ 
		'Ext.data.StoreManager',
		'CFJS.model.document.Form',
		'CFJS.store.financial.InternalTaxes',
		'CFJS.orders.store.DocumentConfig' 
	],
	
	store		: {
		type	: 'document_config',
		pageSize: 0
	},
	
	stripRe		: /[\$,%]/g,
	
	constructor: function() {
		var me = this;
		me.bindStore(me.store || 'ext-empty-store', true);
		Ext.on('userchange', function() { me.store.load(); });
	},
	
	bindStore: function(store, initial) {
        var me = this,
            oldStore = initial ? null : me.store;
        if (store !== oldStore) {
            if (oldStore) {
                if (oldStore.autoDestroy) oldStore.destroy();
                else me.unbindStoreListeners(oldStore);
            }
            if (store) {
                me.store = store = Ext.data.StoreManager.lookup(store);
                me.bindStoreListeners(store);
            } else me.store = null;
        }
        return me;
    },
	
    bindStoreListeners: function(store) {
        var listeners = this.getStoreListeners(store);
        if (listeners) {
            listeners = Ext.apply({}, listeners);
            if (!listeners.scope) listeners.scope = this;
            this.storeListeners = listeners;
            store.on(listeners);
        }
    },
    
	findFormConfig: function(form) {
		if (!isNaN(form = CFJS.parseInt(Ext.isObject(form) ? form.id : form))) {
			var keys = ['invoice', 'serviceCertificate'], i, config, data;
			data = this.store.getData().findBy(function(item, id) {
				for (i = 0; i < keys.length; i++) {
					config = item.get(keys[i]);
					if (config && config.form === form) return true;
				}
			});
			if (data) return config;
		}
	},

	findConfigByCustomer: function(customerType) {
		customerType = customerType || 'company';
		return this.store.getData().findBy(function(item, id) {
			return item && item.get('customerType') === customerType;
		});
	},
	
	getDocumentDate: function(record) {
		var value = this.getDocumentValue(record, 'dateField');
		if (value && !Ext.isDate(value)) value = new Date(Date.parse(value));
		return value;
	},
	
	getDocumentNumber: function(record) {
		var value = this.getDocumentValue(record, 'numberField');
		if (value && !Ext.isNumber(value)) value = CFJS.parseInt(value);
		return isNaN(value) ? 0 : value;
	},

	getDocumentValue: function(record, field) {
		var me = this, config = me.findFormConfig(record.get('form')), 
			value = config ? record.getProperty(config[field]) : null;
		return Ext.isObject(value) ? value.$ : value;	
	},

	getFieldValue: function(record, field) {
		if (field === 'date' || field === 'dateField') return this.getDocumentDate(record);
		if (field === 'number' || field === 'numberField') return this.getDocumentNumber(record);
		return null;
	},
	
	getFormDefaults: function(form, reference) {
		if (!form || !reference) return null;
		var config = this.findFormConfig(form.isModel ? form.getData() : form),
			fields = config && config.fields,
			defaults = {}, field, defaultValue, i;
		if (fields) {
			if (!Ext.isArray(fields)) fields = [fields];
			for (i = 0; i < fields.length; i++) {
				field = fields[i] || {};
				if (!field || !field.id) continue;
				if (field.id === config.dateField) {
					defaultValue = { '@type': field.type || 'date', $: Ext.Date.format(new Date(), CFJS.Api.dateTimeFormat) }
				} else if (field.id === config.invoiceField) {
					defaultValue = { '@type': field.type || 'reference', id: reference }
				} else defaultValue = (field.properties || {}).field_defaultValue;
				if (defaultValue) defaults['field_' + field.id] = defaultValue;
			}
		}
		return defaults;
	},

	getStore: function () {
        return this.store;
    },
	
    getStoreListeners: function() {
    	return { load: 'onConfigLoad', scope: this };
    },
    
    loadForm: function(record, field, data) {
		if ((data = data && data[field]) && Ext.isNumber(data.form)) {
			new CFJS.model.document.Form({ id: data.form }).load({
				callback: function(form, options, successful) {
					if (successful) record.set(field, Ext.apply({}, form.getData(), data));
				}
			})
		}
    },
    
    onConfigLoad: function(store, records, successful, operation) {
    	if (successful && store && store.isStore) {
        	var me = this, data, field;
        	store.each(function(record) {
        		data = record.getData();
        		for (field in data) { 
        			me.loadForm(record, field, data); 
        		}
        	});
    	}
    },
    
	renderDocumentDate: function(record, format) {
		return Ext.util.Format.date(this.getDocumentValue(record, 'dateField'), format);
	},
	
	renderDocumentNumber: function(record, format) {
		return Ext.util.Format.number(this.getDocumentValue(record, 'numberField'), format);
	},
	
	renderState: function(record, state) {
		var me = this;
		state = CFJS.parseInt(state || record.state);
		if (!isNaN(state)) {
			var form = CFJS.parseInt(Ext.isObject(record.form) ? record.form.id : record.form), config;
			if (!isNaN(form)) config = me.store.getData().findBy(function(item, id) {
				return (item.get('invoice') || {}).form === form;
			});
			if (config) {
				if (config.isModel)	config = config.get('invoiceStates') || [];
				return (config[state] || {}).name;
			}
		}
		return '';
	},

	setDocumentDate: function(value, record) {
		if (Ext.isDate(value)) value = Ext.Date.format(value, CFJS.Api.dateTimeFormat);
		this.setDocumentValue({ "@type": "date", $: value }, record, 'dateField');
	},
	
	setDocumentNumber: function(value, record) {
		this.setDocumentValue({ "@type": "long", $: value ? value : 0 }, record, 'numberField');
	},

	setFieldValue: function(value, record, field) {
		if (field === 'date' || field === 'dateField') this.setDocumentDate(value, record);
		else if (field === 'number' || field === 'numberField') this.setDocumentNumber(value,record);
	},

	setDocumentValue: function(value, record, field) {
		var me = this, config = me.findFormConfig(record.get('form'));
		if (config) record.setProperty(config[field], value);
	},
	
	setStore: function (store) {
        this.bindStore(store);
    },
    
	unbindStoreListeners: function(store) {
        var listeners = this.storeListeners;
        if (listeners) store.un(listeners);
    }
	
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'internal_taxes', autoLoad: true }));
});
