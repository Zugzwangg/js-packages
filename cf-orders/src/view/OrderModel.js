Ext.define('CFJS.orders.view.OrderModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.orders.edit',
	requires	: [ 'CFJS.store.Named', 'CFJS.store.financial.AbstractServices' ],
	itemName	: undefined,
	itemModel	: 'Order',
	formulas: {
		customerName: {
			bind: '{_itemName_.customer}',
			get: function(customer) {
				var name = [], fields = ['lastName', 'name', 'middleName'], field;
				if (customer && Ext.isObject(customer)) {
					for (key in fields) {
						if (!Ext.isEmpty(customer[field = fields[key]])) name.push(customer[field]);
					}
				}
				return name.join(' ');
			}
		},
		customerType: {
			bind:'{_itemName_.customer.type}',
			get: function(type) { return type === 'customer' ? 1 : 0; }
		},
		hasId: {
			bind: '{_itemName_.id}',
			get: function(id) { return id > 0 }
		}
	},
	
	defaultItemConfig: function() {
		var me = this, user = me.get('user');
		return {
			post 	: user && user.isPerson ? user.get('post') : null,
			created	: new Date()
		}
	}
});
