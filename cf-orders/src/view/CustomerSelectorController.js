Ext.define('CFJS.orders.view.CustomerSelectorController', {
	extend			: 'Ext.app.ViewController',
	alias			: 'controller.orders.customer-selector',
	lockActiveTab	: false,
	listeners		: { beforesaverecord: 'onBeforeSaveRecord' },
	
	activateEditor: function(activate) {
		var me = this, view = me.getView();
		view.findAction('addRecord').setHidden(activate);
		view.findAction('editRecord').setHidden(activate);
		view.findAction('selectRecord').setHidden(activate);
	},
	
	applyCustomer: function(record) {
		if (record) {
			var	me = this;
			me.fireEvent('applycustomer', record.getData(), record.entityName.endsWith('Customer') ? 'customer' : 'company');
			me.closeView();
		}
	},

	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},

	capitalizeFIO: function(record, locale) {
		var me = this;
		me.capitalizeName(record, 'name_' + locale);
		me.capitalizeName(record, 'last_name_' + locale);
		me.capitalizeName(record, 'middle_name_' + locale);
	},
	
	capitalizeName: function(record, field) {
		var name = record.get(field);
		if (name && name !== '') record.set(field, name.capitalize()); 
	},

	getContactsStore: function() {
		var panel = this.lookupContacts();
		return panel ? panel.getStore() : null;
	},

	getMemorableDatesStore: function() {
		var panel = this.lookupMemorableDates();
		return panel ? panel.getStore() : null;
	},
	
	lookupRecord: function() {
		var panel = this.getView().getActiveTab();
		if (panel) panel = panel ? panel.getLayout().getActiveItem() : null;
		return panel && panel.isEditor ? panel.getViewModel().getItem() : null;
	},
	
	lookupContacts: function() {
		return this.getView().getActiveTab().down('grid-base-panel[name=contacts]');
	},
	
	lookupMemorableDates: function() {
		return this.getView().getActiveTab().down('grid-base-panel[name=memorableDates]');
	},
	
	maybeEditRecord: function(record, add) {
		var view = this.getView().getActiveTab(),
			grid = view ? view.lookupGrid() : null,
			editor = view ? view.editorConfig : null,
			activeItem;
		if (grid && editor) {
			if (view.rendered) Ext.suspendLayouts();
			if (record !== false && !grid.readOnly) {
				var selection = grid.getSelection();
				if (!record && !add && selection && selection.length > 0) record = selection[0];
				if (activeItem = view.down(editor)) view.remove(activeItem, true);
				activeItem = view.add({
					xtype		: editor,
					isEditor	: true,
					actionsMap	: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord', 'browseFiles' ] } },
					header		: { items: new Array(5) }
				});
				activeItem.getViewModel().setItem(record);
			} else activeItem = grid;
			view.getLayout().setActiveItem(activeItem);
			if (view.rendered) Ext.resumeLayouts(true);	
		}
	},
	
	onAddContact: function(component) {
		var store = this.getContactsStore();
		if (store) store.add({ name: 'Название контакта', contact: 'Значение контакта' })
	},
	
	onAddMemoriableDate: function(component) {
		var store = this.getMemorableDatesStore();
		if (store) store.add({ date: new Date(), description: 'Описание памятной даты' })
	},

	onAddRecord: function(component) {
		this.maybeEditRecord(null, true);
	},

	onBackClick: function(component) {
		var me = this, record = me.lookupRecord();
		if (record && (record.phantom || record.dirty)) {
			Ext.fireEvent('confirmlostdataandgo', function(btn) {
				if (btn === 'yes') {
					record.reject();
					me.maybeEditRecord(false);
				}
		    }, me);
		} else me.maybeEditRecord(false);
	},

	onBeforeTabChange: function(tabs, newTab, oldTab) {
		if (this.lockActiveTab) return false;
		var activeItem = oldTab ? oldTab.getLayout().getActiveItem() : null;
		return activeItem && !activeItem.isEditor;
	},
	
	onBeforeSaveRecord: function(record) {
		var me = this, store = me.getMemorableDatesStore(), data;
		if (store && store.isDirty()) {
			data = [];
			store.commitChanges();
			store.each(function(record) {
				data.push({ date: record.get('date'), description: record.get('description') });
			});
			record.set('memorableDates', { memorable_date: data }, { silent: true });
		}
		if (record.entityName.endsWith('Customer')) {
			var	locales = ['uk', 'ru', 'en'], index;
			if ((store = me.getContactsStore()) && store.isDirty()) {
				data = [];
				store.commitChanges();
				store.each(function(record) { 
					data.push({ name: record.get('name'), contact: record.get('contact')}); 
				});
				record.set('contacts', data, { silent: true });
			}
			for (index in locales) {
				me.capitalizeFIO(record, locales[index]);
			}
		} else if (record.entityName.endsWith('Company')) {
			var country = record.get('country');
			if (country && country.isModel) record.set('country', country.getData());
		}
	},

	onBrowseFiles: function(component) {
		var record = this.lookupRecord();
		if (record && record.id > 0) {
			Ext.create({
				xtype		: 'window',
				autoShow	: true,
				title		: 'Клиент - ' + record.get('fullName'),
				maximized	: true,
				items		: [{
					xtype		: 'grid-file-browser',
					fileType	: record.entityName.endsWith('Customer') ? 'customer' : 'company',
					startPath	: record.id.toString()
				}]
			});
		}
	},
	
	onClearFilters: function(component) {
		var activeTab = this.getView().getActiveTab(), 
			grid = activeTab ? activeTab.lookupGrid() : null;
		if (grid && Ext.isFunction(grid.onClearFilters)) grid.onClearFilters();
	},

	onEditRecord: function(component) {
		this.maybeEditRecord();
	},
	
	onGridDblClick: function(view, record, item, index, e, eOpts) {
		this.applyCustomer(record);
	},
	
	onRefreshRecord: function(component) {
		var me = this, record = me.lookupRecord();
		if (record) {
			record.reject();
			if (!record.phantom) record.load();
			else {
				var memorableDates = me.getMemorableDatesStore(),
					contacts = me.getContactsStore();
				if (memorableDates) memorableDates.rejectChanges(); 
				if (contacts) contacts.rejectChanges(); 
			}
		}
	},
	
	onRemoveContact: function(component) {
		var contacts = this.lookupContacts();
		if (contacts && (selection = contacts.getSelection())) {
			contacts = contacts.getStore();
			contacts.remove(selection);
		}
	},
	
	onRemoveMemoriableDate: function(component) {
		var memorableDates = this.lookupMemorableDates();
		if (memorableDates && (selection = memorableDates.getSelection())) {
			memorableDates = memorableDates.getStore();
			memorableDates.remove(selection);
		}
	},
	
	onSaveRecord: function(component, force) {
		var me = this, record = me.lookupRecord();
		if (record) {
			var view = me.getView(), 
				grid = view ? view.lookupGrid() : null;
			if (me.fireEvent('beforesaverecord', record) !== false 
					&& (record.dirty || record.phantom || force)) {
				me.disableComponent(component, true);
				record.save({
					callback: function(record, operation, success) {
						grid.getStore().reload();
						me.disableComponent(component);
					}
				})
			}
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		var me = this, actions;
		if (selectable && selectable.isSelectionModel) selectable = selectable.view.grid;
		if (!selectable || !selectable.isWindow) selectable = me.getView();
		if (actions = selectable && selectable.hasActions && selectable.actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			selectable.disableComponent(actions.editRecord, !selection || selection.length !== 1);
			selectable.disableComponent(actions.selectRecord, !selection || selection.length !== 1);
		}
	},
	
	onSelectRecord: function() {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			selection = grid ? grid.getSelection() : null;
		if (selection && selection.length === 1) me.applyCustomer(selection[0]);
	},
	
	onShowContextMenu: function(view, rec, node, index, e) {
		var mainView = this.getView();
		if (mainView.contextMenu) {
			var header = mainView.getHeader();
			e.stopEvent();
			mainView.showContextMenu([e.getX() - mainView.getX(), e.getY() - mainView.getY() - (header ? header.getHeight() : 0)]);
		}
		return !view.contextMenu;
	},
	
	onTabChange: function(tabs, newTab, oldTab) {
		var grid = newTab ? newTab.lookupGrid() : null;
		this.onSelectionChange(this.getView(), grid ? grid.getSelection() : null);
	}
	
});