Ext.define('CFJS.orders.view.OrdersModel', {
	extend			: 'CFJS.app.BaseEditorModel',
	alias			: 'viewmodel.orders.main',
	requires		: [ 'CFJS.orders.util.Invoice', 'CFJS.store.financial.Orders' ],
	itemName		: 'editOrder',
	itemModel		: 'Order',
	stores			: {
		orders: {
			type		: 'orders',
			autoLoad	: true,
			session		: { schema: 'financial' },
			filters		: [{
				id		: 'startDate',
				property: 'created',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'created',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			}],
			sorters		: [{ property: 'created', direction: 'DESC' }],
			summaries	: [{ property: 'id' }, { property: 'amount', type: 'sum' }],
			listeners	: { summary: 'onLoadSummary' }
		}
	}
	
});
