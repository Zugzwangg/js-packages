Ext.define('CFJS.orders.view.OrdersController', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.orders.main',
	id			: 'ordersMain',
	routeId		: 'orderlist',
	editor		: 'ordereditor',
	listeners	: { beforeremoverecord: 'onBeforeRemoveRecord' },

	initViewModel: function(viewModel) {
		if (viewModel && viewModel.isMaster) 
			viewModel.bind('{purchaseHistoryFilter}', this.setPurchaseHistoryFilter, this, { deep: true });
		this.callParent(arguments);
	},
	
	applyCustomer: function(customer, type) {
		var editor = this.getEditorComponent(),
			order = editor ? editor.getViewModel().getItem() : null;
		if (order && order.isModel) order.setCustomer(customer, type);
		editor.getViewModel().setItem(order);
	},
	
	onBeforeRemoveRecord: Ext.emptyFn,
	
	onDocumentsClick: function(component) {
		var vm = this.getViewModel(), editor = this.getEditorComponent(),
			order = editor ? editor.getViewModel().getItem() : null;
		Ext.create({
			xtype		: 'invoiceseditor',
			autoShow	: true,
			maximized	: true,
			viewModel	: { parent: editor ? editor.lookupViewModel() : vm },
			listeners	: {
				paymentschange: function(transaction) {
					if (order) order.load();
				}
			}
		});
	},

	onEditCustomer: function(component) {
		var me = this;
		Ext.create({
			xtype		: 'customer-selector', 
			autoShow	: true, 
			controller	: { 
				type		: 'orders.customer-selector', 
				customerType: me.getEditorComponent().getViewModel().get('customerType'),
				listeners	: { applycustomer: { fn: 'applyCustomer', scope: me } }
			},
			maximized	: true
		});
	},
	
	onImportService: function(component) {
		var me = this,
			editor = me.getEditorComponent(),
			grid = me.getView().down('servicegrid');
		Ext.create({
			xtype		: 'plugins.service.selector', 
			autoShow	: true,
			maximized	: true,
			listeners	: {
				close: function(panel) { me.onServicesChange(grid); }
			},
			viewModel	: {
				parent: editor ? editor.getViewModel() : null
			}
		});
	},

	onLoadSummary: function(store, records, successful, operation) {
		if (successful) this.lookupGrid().updateSummaries(records);
	},

	onServicesChange: function(grid) {
		var me = this, editor = me.getEditorComponent();
		if (grid) grid.getStore().reload();
		if (editor) me.onSaveRecord(editor.findAction('saveRecord'), true);
	},
	
	onSummaryRefresh: function(component) {
		var store = this.lookupGridStore();
		if (store) store.summary();
	},
	
	setPurchaseHistoryFilter: function(filter) {
		var me = this, vm = me.getViewModel();
		if (filter.view === 'orders' && filter.type && filter.id > 0) {
			filter = [{
				id		: filter.type,
				property: filter.type + '.id',
				type	: 'numeric',
				operator: 'eq',
				value	: filter.id
			}];
		} else filter = null;
		me.onBackClick();
		vm.setStoreFilters('orders', filter);
	}

});
