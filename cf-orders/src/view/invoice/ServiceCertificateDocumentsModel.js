Ext.define('CFJS.orders.view.invoice.ServiceCertificateDocumentsModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.orders.service_certificates',
	requires	: [
		'CFJS.api.proxy.SvdJson',
		'CFJS.orders.model.InvoiceDocument',
		'CFJS.orders.model.ServiceCertificateDocument',
		'CFJS.store.Buffered'
	],
	itemModel	: 'CFJS.orders.model.ServiceCertificateDocument',
	itemName	: 'viewServiceCertificate',
	data		: {
		customerType: 'company',
		customerTypeName: 'компаний',
		workingPeriod: null
	},
	stores		: {
		serviceCertificates: {
			type		: 'basebuffered',
			autoLoad	: true,
			model		: 'CFJS.orders.model.ServiceCertificateDocument',
			session		: { schema: 'document' },
			proxy		: {
				type		: 'json.svdapi',
				loaderConfig: { 
					formId	: '{config.serviceCertificate.form}',
					userInfo: '{userInfo}'
				},
				service		: 'svd'
			}
		},
		templates: {
			type		: 'formtemplates',
			autoLoad	: true,
			form		: '{config.serviceCertificate.form}',
			pageSize	: 0,
			listeners	: { load: 'onFormTemplatesLoad' }
		}
	},
	formulas	: {
		config	: {
			bind: '{customerType}',
			get	: function(customerType) {
				this.set('customerTypeName', customerType === 'company' ? 'компаний' : 'физ. лиц');
				return CFJS.orders.util.Invoice.findConfigByCustomer(customerType);
			}
		}
	},
	
	setItem: function(item) {
		var me = this, stub = me.getStub(me.getItemName());
		stub.set(item = me.configItem(item, me.get('config')));
		return item;
	},
	
	privates: {
		
		configItem: function(item, config) {
			var me = this, rules = {}, invoice, cfg;
			if (item && config && config.isModel) {
				cfg = config.get('serviceCertificate') || {};
				item.set('number', item.getIntProperty(cfg.numberField));
				item.set('date', item.getDateProperty(cfg.dateField));
				if (!item.get('state') 
					&& (invoice = item.getProperty(cfg.invoiceField))
					&& invoice.id > 0) {
					cfg = config.get('invoice') || {};
					if (cfg.form) {
						CFJS.orders.model.InvoiceDocument.load(invoice.id, {
							form	: cfg.form,
							scope	: me,
							success	: function(record, operation) {
								item.set('state', record.getState(cfg.stateField));
						    }
						});	
					} 
				}
			}
			return item || { type: me.getItemModel(), create: me.defaultItemConfig() || true };
		}
	
	}
});
