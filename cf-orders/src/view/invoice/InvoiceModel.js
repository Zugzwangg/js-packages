Ext.define('CFJS.orders.view.invoice.InvoiceModel', {
	extend	: 'CFJS.app.EditorViewModel',
	alias	: 'viewmodel.orders.invoice',
	requires: [
		'CFJS.model.document.Invoice',
		'CFJS.orders.util.Invoice',
		'CFJS.store.document.FormTemplates',
		'CFJS.store.document.InvoiceRecords'
	],
	config	: { index: -1, orderName: 'editOrder' },
	itemName: undefined,
	itemModel: 'Invoice',
	stores	: {
		invoiceRecords: {
			type: 'invoicerecords',
			data: '{_itemName_.records}'
		},
		templates: {
			type		: 'formtemplates',
			autoLoad	: true,
			form		: '{_itemName_.form}',
			pageSize	: 0,
			listeners	: { load: 'onFormTemplatesLoad' }
		}
	},
	
	defaultItemConfig: function(item) {
		var me = this,
			order = me.get(me.getOrderName()),
			customer = order ? order.get('customer') : null,
			invoiceConfig = CFJS.orders.util.Invoice.findConfigByCustomer((customer||{}).type || 'company');
		return {
			form	: invoiceConfig ? invoiceConfig.get('invoiceForm') : null,
			number	: 'Новый счет',
			date	: new Date(),
			order	: order.get('id'),
			payer	: customer,
			records : []
		};
	},

	setItem: function(item) {
		return this.linkItemRecord(item);
	},
	
	privates: {
		
		parseDescriptor: function(descriptor) {
			return this.callParent([descriptor.replace('_orderName_', this.getOrderName())]); 
		}
		
	}

});
