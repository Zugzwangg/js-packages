Ext.define('CFJS.orders.view.invoice.ViewModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.orders.invoices.view',
	requires	: [
		'CFJS.api.proxy.SvdJson',
		'CFJS.orders.model.InvoiceDocument',
		'CFJS.store.Buffered',
		'CFJS.store.document.Invoices'
	],
	itemModel	: 'CFJS.orders.model.InvoiceDocument',
	itemName	: 'viewInvoice',
	data		: {
		customerType: 'company',
		customerTypeName: 'компаний',
		workingPeriod: null
	},
	stores		: {
		invoices: {
			type		: 'basebuffered',
			autoLoad	: true,
			model		: 'CFJS.orders.model.InvoiceDocument',
			session		: { schema: 'document' },
			proxy		: {
				type		: 'json.svdapi',
				loaderConfig: { 
					formId	: '{config.invoice.form}',
					userInfo: '{userInfo}'
				},
				service		: 'svd'
			}
//			listeners	: { load: 'onInvoicesLoad' }
		},
		templates: {
			type		: 'formtemplates',
			autoLoad	: true,
			form		: '{config.invoice.form}',
			pageSize	: 0,
			listeners	: { load: 'onFormTemplatesLoad' }
		}
	},
	formulas	: {
		config		: {
			bind: '{customerType}',
			get	: function(customerType) {
				this.set('customerTypeName', customerType === 'company' ? 'компаний' : 'физ. лиц');
				return CFJS.orders.util.Invoice.findConfigByCustomer(customerType);
			}
		}
	},
	
	setItem: function(item) {
		var me = this, stub = me.getStub(me.getItemName());
		stub.set(item = me.configItem(item, me.get('config')));
		return item;
	},
	
	privates: {
		
		configItem: function(item, config) {
			var me = this, rules = {};
			if (item && config && config.isModel) {
				config = config.get('invoice') || {};
				item.set('number', item.getIntProperty(config.numberField));
				item.set('date', item.getDateProperty(config.dateField));
				var state = item.getState(config.stateField),
					amount = item.getFloatProperty(config.amountField),
					paymentAmount = item.getPaymentAmount(config.paymentAmountField),
					residualValue = amount - paymentAmount;
				item.set('state', state);
				rules.canPay = state === 1 && residualValue !== 0 && (amount > 0 && residualValue > 0 || amount < 0 && residualValue < 0);
				rules.canRemove = state < 2 && paymentAmount === 0;
				rules.canSign = config.stateField && state === 0 && !item.hasSign();
			}
			me.set(me.getItemName() +'Rules', rules);
			return item || { type: me.getItemModel(), create: me.defaultItemConfig() || true };
		}
	
	}
});
