Ext.define('CFJS.orders.view.invoice.EditController', {
	extend	: 'CFJS.orders.view.invoice.ViewController',
	alias	: 'controller.orders.invoices.edit',
	id		: 'ordersEditInvoices',
	
	addRecords: function(invoice, data, dropHandlers, reload) {
		var me = this; 
		return me.commitInvoice(me.getView().actions.addRecords, function(currentInvoice) {
			if (!invoice || !data || invoice.hasSign() || invoice === currentInvoice) return false;
			var grid = me.lookupRecordsEditor();
			data.records = grid ? me.getChainedRecords(grid.getStore(), data.records) : [];
			if (data.records.length === 0) return false;
			invoice.addRecords(data.records);
			if (dropHandlers) dropHandlers.wait = true;
			return currentInvoice = invoice;
		}, function(record, operation, success) {
			if (success) dropHandlers.processDrop(reload);
			else dropHandlers.cancelDrop();
		});
	},

	applyPayer: function(payer, type) {
		var invoice = this.lookupInvoice();
		if (invoice && invoice.isModel) {
			var invoiceConfig = CFJS.orders.util.Invoice.findConfigByCustomer(type);
			invoice.setPayer(payer, invoiceConfig ? invoiceConfig.get('invoiceForm') : null);
		}
	},
	
	beforeDropNewRecords: function(node, data, overModel, dropPosition, dropHandlers) {
		var me = this;
		dropHandlers.wait = true;
		me.onAddInvoiceRecords(me.getView().actions.addRecords);
		dropHandlers.cancelDrop();
	},
	
	beforeDropRecords: function(node, data, overModel, dropPosition, dropHandlers) {
		var me = this;
		dropHandlers.wait = true;
		me.onRemoveInvoiceRecords(me.getView().actions.removeRecords);
		dropHandlers.cancelDrop();
	},
	
	commitInvoice: function(component, update, callback) {
		var me = this, invoice = me.lookupInvoice();
		if (!invoice || invoice.hasSign()) return false;
		me.disableComponent(component, true);
		var updated = Ext.isFunction(update) ? update.call(this, invoice) : invoice;
		if (updated === false) return updated;
		else if (updated && updated.isModel) invoice = updated;
		invoice.save({
			params	: { locale: CFJS.getLocale() },
			callback: callback || Ext.emptyFn
		});
	},
	
	getChainedRecords: function(store, data) {
		var services = {}, records = [], orderNumber, i, item;
		if (store && data) {
			if (!Ext.isArray(data)) data = [data];
			store.each(function(record) {
				orderNumber = (record.get('orderNumber') || '').split('/').slice(0,2).join('/');
				if (!Ext.isEmpty(orderNumber)) {
					if (!services[orderNumber]) services[orderNumber] = [record.getData()];
					else services[orderNumber].push(record.getData());
				}
			});
			for (i = 0; i < data.length; i++) {
				item = data[i];
				if (item && item.isModel) item = item.getData();
				if (item) {
					orderNumber = (item.orderNumber || '').split('/').slice(0,2).join('/');
					if (!Ext.isEmpty(orderNumber) && services[orderNumber]) {
						records = records.concat(services[orderNumber]);
						delete services[orderNumber];
					}
				}
			}
		}
		return records;
	},

	getRecords: function() {
		var grid = this.lookupRecordsEditor();
		return grid ? grid.getStore().getRange() : [];
	},

	getSelectedRecords: function() {
		var grid = this.lookupRecordsEditor();
		return grid && grid.getSelection() ? grid.getSelection() : [];
	},

	getSelectedNewRecords: function() {
		var grid = this.lookupRecordsPanel();
		return grid && grid.getSelection() ? grid.getSelection() : [];
	},

	lookupEditor: function() {
		return this.lookup('invoiceEditor');
	},
	
	lookupListView: function() {
		return this.lookup('invoicesList');
	},

	lookupRecordsEditor: function() {
		return this.lookup('invoiceRecordsEditor');
	},

	lookupRecordsPanel: function() {
		return this.lookup('newRecordsPanel');
	},
	
	onAddRecord: function(component) {
		var me = this, 
			editor = me.lookupEditor(), 
			list = me.lookupListView(),
			invoice = editor ? editor.setItem() : null,
			store, records = [];
		if (invoice && list) {
			if (invoice.phantom) {
				list.getStore().add(invoice);
				store = me.getViewModel().getStore('unlinkedRecords');
				if (store) {
					store.each(function(record) {
						records = records.concat(record, record.clearTaxes());
					});
					me.commitInvoice(me.getView().actions.addRecords, function(invoice) { 
						invoice.addRecords(records);
					}, function(record, operation, success) {
						me.onReloadInvoiceRecords();
					});
				}
			}
			if (invoice.isModel) list.getSelectionModel().select(invoice, false, true);
		}
	},
	
	onAddInvoiceRecords: function(component) {
		var me = this, 
			grid = me.lookupRecordsPanel();
		return me.commitInvoice(component, function(invoice) { 
			invoice.addRecords(grid ? me.getChainedRecords(grid.getStore(), grid.getSelection()) : []);
		}, function(record, operation, success) {
			me.onReloadInvoiceRecords();
		});
	},
	
	onEditCustomer: function(component) {
		var me = this;
		Ext.create({
			xtype		: 'customer-selector', 
			autoShow	: true, 
			controller	: { 
				type			: 'orders.customer-selector',
				lockActiveTab	: true,
				customerType	: me.lookupEditor().getViewModel().get('customerType'),
				listeners		: { applycustomer: { fn: 'applyPayer', scope: me } }
			},
			maximized	: true
		});
	},
	
	onExpandRecords: function(component) {
		var me = this, taxCode = component.taxCode,
			grid = me.lookupRecordsPanel(),
			selection = grid && grid.getSelection() ? grid.getSelection() : [];
		if (taxCode && selection.length > 0) {
			var store = grid.getStore(), i;
			for(i = 0; i < selection.length; i++) {
				store.add(selection[i].removeTaxByCode(taxCode));
			}
			store.commitChanges();
		}
	},
	
	onInvoicesLoad: function(store, records, successful) {
		var selection = this.lookupListView().getSelectionModel();
		if (successful && !selection.hasSelection()) {
			selection.select(store.getAt(0)); 
		}
	},
	
	onItemContextMenu: function(panel, record, item, index, e) {
		e.stopEvent();
		if (panel.menu && panel.menu.isMenu) panel.menu.showAt(e.getXY());
		return false;
	},
	
	onNewRecordsSelectionChange: function(selectable, selection) {
		var me = this, actions = me.getView().actions;
		if (actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			var invoice = this.lookupInvoice(), 
				disable = !(selection && selection.length > 0 && invoice && invoice.get('canSign'));
			me.disableComponent(actions.addRecords, disable);
			me.disableComponent(actions.compressRecords, disable);
			me.disableComponent(actions.expandRecords, disable);
		}
	},
	
	onRecordsSelectionChange: function(selectable, selection) {
		var me = this, actions = me.getView().actions;
		if (actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.disableComponent(actions.removeRecords, !selection || selection.length < 1);
		}
	},

	onReloadStore: function(component) {
		this.callParent(arguments);
		this.onReloadInvoiceRecords();
	},

	onReloadInvoiceRecords: function(component) {
		var grid = this.lookupRecordsPanel();
		grid.getSelectionModel().deselectAll();
		grid.getStore().reload();
	},
	
	onRemoveInvoiceRecords: function(component) {
		var me = this, 
			grid = me.lookupRecordsEditor();
		return me.commitInvoice(component, function(invoice) { 
			invoice.removeRecords(grid ? me.getChainedRecords(grid.getStore(), grid.getSelection()) : []); 
		}, function(record, operation, success) {
			me.onReloadInvoiceRecords();
		});
	},

	onSaveRecord: function(component) {
		var me = this, disabled = component.disabled;
		return me.commitInvoice(component, function(invoice) { 
			invoice.addRecords(me.getRecords());
		}, function(record, operation, success) {
			component.setDisabled(disabled);
		});
	},
	
	onSignRecord: function(component) {
		var me = this, invoice = me.lookupInvoice();
		if (!invoice || invoice.hasSign()) return false;
		me.disableComponent(component, true);
		invoice.setState('state', 1);
		invoice.addRecords(me.getRecords());
		invoice.sign({ params: { locale: CFJS.getLocale() } });
	},
	
	removeRecords: function(data, reload) {
		var me = this;
		return me.commitInvoice(me.getView().actions.removeRecords, function(invoice) {
			var grid = me.lookupRecordsEditor();
			data.records = grid && data ? me.getChainedRecords(grid.getStore(), data.records) : [];
			if (data.records.length === 0) return false;
			invoice.removeRecords(data.records);
		}, function(record, operation, success) {
			if (reload) me.onReloadInvoiceRecords();
		});
	}
	
});