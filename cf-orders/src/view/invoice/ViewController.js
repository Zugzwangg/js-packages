Ext.define('CFJS.orders.view.invoice.ViewController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.orders.invoices.view',
	id		: 'ordersViewInvoices',

	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},

	lookupEditor: function() {
		return this.lookupListView();
	},
	
	lookupInvoice: function() {
		var editor = this.lookupEditor();
		return editor ? editor.getItem() : null;
	},

	lookupListView: function() {
		return this.getView().lookupGrid();
	},
	
	onClearFilters: function(component) {
		var grid = this.lookupListView();
		if (grid && Ext.isFunction(grid.onClearFilters)) grid.onClearFilters();
	},

	onFormTemplatesLoad: function(store, records, successful) {
		var me = this, items = [], printRecord = me.getView().findAction('printRecord');
		if (successful && records && records.length > 0) {
			for (var i = 0; i < records.length; i++ ) {
				var rec = records[i];
				if (rec.get('type') === 'PDF_PRINT') {
					items.push({
						scope	: me,
						iconCls	: printRecord.initialConfig.iconCls,
						text	: rec.get('name'),
						template: rec,
						handler	: printRecord.initialConfig.handler
					});
				}
			}
		}
		var menu = items.length > 1 ? { items: items } : null,
			template = items.length === 1 ? items[0].template : null;
		printRecord.each(function(component) {
			component.template = template;
			if (Ext.isFunction(component.setMenu)) component.setMenu(menu, true);
		})
	},
	
	onGridDblClick: function(view, record, item, index, e, eOpts) {
		this.onReferredDocuments((this.getView().findAction('referredDocuments') || {}).initialConfig);
	},
	
	onPaying: function(component) {
		var me = this;
		Ext.create({
			xtype		: 'paymentview',
			autoShow	: true,
			iconCls		: component.iconCls,
			modal		: true,
			viewModel	: { parent: me.lookupEditor().lookupViewModel() },
			listeners	: {
				paymentschange: function(transaction) {
					me.lookupInvoice().load();
					me.getView().fireEvent('paymentschange', transaction);
				}
			}
		});
	},

	onPeriodPicker: function(component) {
		var view = this.lookupListView();
		if (view && view.hasPeriodPicker) view.showPeriodPicker(component);
	},
	
	onPrintRecord: function(component) {
		var template = component && component.template;
		if (template && template.isPrintTemplate) template.print(this.lookupInvoice());
	},

	onReferredDocuments: function(component) {
		var me = this;
		Ext.create({
			xtype		: 'referreddocuments', 
			autoShow	: true,
			iconCls		: component.iconCls,
			modal		: true,
			viewModel	: { parent: me.lookupEditor().lookupViewModel() }
		});
	},

	onReloadStore: function(component) {
		var store = this.lookupListView().getStore();
		if (!store.isBufferedStore) store.rejectChanges();
		store.reload();
	},

	onRemoveRecord: function(component) {
		var me = this, view = me.lookupListView(), 
			selection = view ? view.getSelection() : null;
		if (selection && selection.length > 0) {
			me.disableComponent(component, true);
			for (var i = 0; i < selection.length; i++) {
				var invoice = selection[i], state = invoice.getState(view.stateField), callback = Ext.emptyFn;
				if (i === selection.length - 1) { 
					callback = function(record, operation, success) {
						me.onReloadStore();
						me.disableComponent(component);
					};
				}
				if (!invoice.phantom && invoice.getId() > 0 && state < 2) {
					if (state === 0) invoice.erase({ callback: callback });
					else if (state === 1 && invoice.getPaymentAmount(view.paymentAmountField) === 0) {
						invoice.setState(view.stateField, 3);
						invoice.save({ callback: callback });
					}
				} 
				callback();
			}
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		var editor = this.lookupEditor();
		if (editor) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			editor.setItem(selection && selection.length > 0 ? selection[0] : false);
		}
	},

	onSignRecord: function(component) {
		var me = this, view = me.lookupListView(),
			selection = view ? view.getSelection() : null;
			invoice = selection && selection.length > 0 ? selection[0] : null;
		if (!invoice || invoice.hasSign()) return false;
		me.disableComponent(component, true);
		invoice.setState(view.stateField, 1);
		invoice.save({
			params	: { locale: CFJS.getLocale() },
			success: function(record, operation) {
				record.sign({ params: { locale: CFJS.getLocale() } });
			}
		});
	}

});