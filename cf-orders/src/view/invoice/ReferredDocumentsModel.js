Ext.define('CFJS.orders.view.invoice.ReferredDocumentsModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.orders.invoices.referreddocuments',
	requires	: [ 'CFJS.store.Named', 'CFJS.store.document.Documents' ],
	itemName	: 'referred',
	itemModel	: 'Document',
	stores		: {
		referrerForms: {
			type		: 'named',
			autoLoad	: true,
			filters		: [{ property: 'id', type: 'numeric', operator: 'eq', value: '{parentForm}' }],
			listeners	: { load: 'onReferrerFormsLoad' },
			pageSize	: 0,
			proxy		: { loaderConfig: { dictionaryType: 'referrer_form' } },
			sorters		: [ 'name' ]
		},
		referredDocuments: {
			type	: 'documents',
			autoLoad: true,
			filters	: [{ property: 'id', type: 'numeric', operator: 'eq', value: '{_parentItemName_.id}' }],
			pageSize: 0,
			proxy	: {
				type		: 'json.cfapi',
				loaderConfig: { dictionaryType: 'referred' } 
			},
			session	: { schema: 'document' }
		},
		referredTemplates: {
			type		: 'formtemplates',
			autoLoad	: true,
			form		: '{_itemName_.form}',
			pageSize	: 0,
			listeners	: { load: 'onFormTemplatesLoad' }
		}
	},
	formulas	: {
		parentForm: {
			bind: '{_parentItemName_.form}',
			get	: function(form) {
				return form && form.id ? form.id : form;
			}
		},
		parentState: {
			bind: '{_parentItemName_}',
			get: function(parentItem) {
				if (parentItem && parentItem.isModel) {
					var config = this.get('config');
					return parentItem.getState(config && config.isModel? (config.get('invoice') || {}).stateField : null);
				}
				return null;
			}
		}
	},
	
	defaultItemConfig: function(form) {
		if (!form) return null;
		var me = this, invoice = me.getParentItem(),
		form = form.isModel ? form.getData() : form;
		return {
			author		: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
			form		: form,
			name		: form ? form.name : "",
			unit 		: me.get('userUnit'),
			properties  : CFJS.orders.util.Invoice.getFormDefaults(form, invoice ? invoice.id : null) || "",
			locale		: CFJS.getLocale(),
			startDate	: new Date(),
			created		: new Date()
		}
	},

	setItem: function(item) {
		return this.linkItemRecord(item);
	}

});