Ext.define('CFJS.orders.view.invoice.ServiceCertificatesController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.orders.service_certificates',
	id		: 'ordersServiceCertificates',

	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},

	lookupEditor: function() {
		return this.lookupGrid();
	},
	
	lookupDocument: function() {
		var editor = this.lookupEditor();
		return editor ? editor.getItem() : null;
	},

	lookupGrid: function() {
		return this.getView().lookupGrid();
	},
	
	onClearFilters: function(component) {
		var grid = this.lookupGrid();
		if (grid && Ext.isFunction(grid.onClearFilters)) grid.onClearFilters();
	},

	onFormTemplatesLoad: function(store, records, successful) {
		var me = this, items = [], printDocument = me.getView().findAction('printDocument');
		if (successful && records && records.length > 0) {
			for (var i = 0; i < records.length; i++ ) {
				var rec = records[i];
				if (rec.get('type') === 'PDF_PRINT') {
					items.push({
						scope	: me,
						iconCls	: printDocument.initialConfig.iconCls,
						text	: rec.get('name'),
						template: rec,
						handler	: printDocument.initialConfig.handler
					});
				}
			}
		}
		var menu = items.length > 1 ? { items: items } : null,
			template = items.length === 1 ? items[0].template : null;
		printDocument.each(function(component) {
			component.template = template;
			if (Ext.isFunction(component.setMenu)) component.setMenu(menu, true);
		})
	},
	
	onPeriodPicker: function(component) {
		var view = this.lookupGrid();
		if (view && view.hasPeriodPicker) view.showPeriodPicker(component);
	},
	
	onPrintDocument: function(component) {
		var template = component && component.template;
		if (template && template.isPrintTemplate) template.print(this.lookupDocument());
	},

	onReloadDocuments: function(component) {
		var store = this.lookupGrid().getStore();
		if (!store.isBufferedStore) store.rejectChanges();
		store.reload();
	},

	onRemoveDocuments: function(component) {
		var me = this, view = me.lookupGrid(), 
			selection = view ? view.getSelection() : null;
		if (selection && selection.length > 0) {
			me.disableComponent(component, true);
			for (var i = 0; i < selection.length; i++) {
				var document = selection[i], callback = Ext.emptyFn;
				if (i === selection.length - 1) {
					callback = function(record, operation, success) {
						view.getStore().reload();
						me.disableComponent(component);
					};
				}
				if (document && document.get('canSign')) {
					document.erase({callback: callback});
				} else callback.call(this);
			}
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		var me = this, editor = me.lookupEditor();
		if (editor) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			editor.disableAction('removeDocuments', !selection || selection.length < 1);
			editor.setItem(selection && selection.length > 0 ? selection[0] : false);
		}
	},

	onSignDocument: function(component) {
		var document = this.lookupDocument(), disabled = component.disabled;
		if (!document || document.hasSign()) return false;
		component.disable();
		document.sign({ params: { locale: CFJS.getLocale() } });
	}

});