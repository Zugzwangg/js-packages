Ext.define('CFJS.orders.view.invoice.EditModel', {
	extend	: 'CFJS.app.InlineEditorViewModel',
	alias	: 'viewmodel.orders.invoices.edit',
	requires: [
		'CFJS.orders.store.DocumentConfig',
		'CFJS.store.document.Invoices'
	],
	itemName: 'editInvoice',
	config: { orderName: 'editOrder' },
	stores	: {
		invoices: {
			type		: 'invoices',
			autoLoad	: true,
			session		: { schema: 'document' },
			proxy		: { loaderConfig: { id: '{_parentItemName_.id}' } },
			listeners	: { load: 'onInvoicesLoad' }
		},
		unlinkedRecords: {
			type	: 'invoicerecords',
			autoLoad: true,
			proxy	: { loaderConfig: { order: '{_parentItemName_.id}' } }
		}
	}

});
