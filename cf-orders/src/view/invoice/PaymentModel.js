Ext.define('CFJS.orders.view.invoice.PaymentModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.orders.invoices.payment',
	requires	: [ 'CFJS.store.Named', 'CFJS.store.financial.PaymentTransactions', 'CFJS.orders.model.InvoicePayment' ],
	itemName	: 'transaction',
	itemModel	: 'CFJS.orders.model.InvoicePayment',
	stores	: {
		transactions: {
			type	: 'payment_transactions',
			autoLoad: true,
			filters	: [{ id: 'baseDocument', property: 'baseDocument.id', type: 'numeric', operator: 'eq', 	value: '{_parentItemName_.id}' }],
			session	: { schema: 'financial' },
			sorters	: [ 'created' ]
		}
	},
	
	defaultItemConfig: function() {
		var invoice = this.getParentItem();
		if (!invoice) return null;
		return {
			id		: invoice.get('id'),
			form	: invoice.get('form'),
			amount	: invoice.get('amount') - invoice.get('paymentAmount'),
			locale	: CFJS.getLocale(),
			created	: new Date()
		};
	}

});