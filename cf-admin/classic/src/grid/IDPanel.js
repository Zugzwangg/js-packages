Ext.define('CFJS.admin.grid.IDPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-id-panel',
	requires		: [
		'Ext.grid.plugin.RowEditing',
		'CFJS.app.InlineEditorController',
		'CFJS.form.field.DictionaryComboBox'
	],
	actions		: {
		clearFilters: false,
		periodPicker: false,
		saveRecords	: {
			handler	: 'onSaveRecords',
			iconCls	: CFJS.UI.iconCls.SAVE,
			title	: 'Save the changes',
			tooltip	: 'Save the changes'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'saveRecords' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'saveRecords', 'reloadStore' ] }
	},
	bind			: '{ids}',
	contextMenu		: { items: new Array(4) },
	controller		: 'inlineeditor',
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(5) },
	height			: 250,
	iconCls			: CFJS.UI.iconCls.LIST,
	plugins			: [{
		ptype: 'gridfilters'
	},{
		ptype		: 'rowediting', 
		clicksToEdit: 2, 
		listeners	: { beforeedit: function(editor, context) { return !this.grid.readOnly; } }
	}],
	reference		: 'idGridPanel',
    title			: 'Identity cards',
    
    afterRender: function() {
		var me = this, viewModel = me.lookupViewModel();
		me.callParent();
		if (viewModel) viewModel.bind('{_parentItemName_.id}', this.bindCustomerId, this);
	},

	bindCustomerId: function(id) {
		var saveRecords = this.actions.saveRecords;
		if (saveRecords) saveRecords.setDisabled(id <= 0);
	},

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	lookupGrid: function() {
		return this;
	},
	
	renderColumns: function() {
		var me = this;
		return [{ 
			dataIndex	: 'owner',
			editor		: { allowBlank: false, emptyText: 'select owner' },
			filter		: { type: 'string', dataIndex: 'owner.name', emptyText: 'select owner' },
			flex		: 1,
			hideable	: false,
			hidden		: !!me.hideOwner,
			text		: 'Owner',
			tpl			: '<tpl if="owner">{owner.name}</tpl>',
		},{
			align		: 'left',
			dataIndex	: 'id',
			editor		: { allowBlank: false, emptyText: 'series and number', selectOnFocus: true },
			filter		: { type: 'string', emptyText: 'series and number' },
			style		: { textAlign: 'center' },
			text		: 'Series and number',
			width		: 120
		},{
			align		: 'left',
			dataIndex	: 'country',
			editor		: { 
				allowBlank		: false,
				autoLoadOnValue	: true,
				emptyText		: 'country name',
				minChars 		: 1,
				publishes		: 'value',
				selectOnFocus	: true,
				store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } },
				xtype			: 'dictionarycombo'
			},
			filter		: { type: 'string', dataIndex: 'contry.name', emptyText: 'country name' },
			sorter		: 'contry.name',
			style		: { textAlign: 'center' },
			text		: 'Country',
			tpl			: '<tpl if="country">{country.name}</tpl>',
			width		: 150,
			xtype		: 'templatecolumn'
		},{
			align		: 'left',
			dataIndex	: 'type',
			editor		: { 
				allowBlank		: false,
				autoLoadOnValue	: true,
				emptyText		: 'document type',
				minChars 		: 3,
				publishes		: 'value',
				selectOnFocus	: true,
				store			: { type: 'idtypes' },
				xtype			: 'dictionarycombo'
			},
			filter		: { type: 'string', dataIndex: 'type.name', emptyText: 'document type' },
			sorter		: 'type.name',
			style		: { textAlign: 'center' },
			text		: 'Type',
			tpl			: '<tpl if="type">{type.name}</tpl>',
			width		: 150,
			xtype		: 'templatecolumn'
		},{
			dataIndex	: 'internal',
			editor		: { xtype: 'checkbox' },
			filter		: { type: 'boolean' },
			text		: 'Internal',
			xtype		: 'checkcolumn'
		},{
			align		: 'center',
			dataIndex	: 'issueDate',
			editor		: { xtype: 'datefield', emptyText: 'date of issue', format: Ext.Date.patterns.NormalDate, selectOnFocus: true }, 
			filter		: { type: 'date' },
			format		: Ext.Date.patterns.NormalDate,
			text		: 'Issued date',
			width		: 120,
			xtype		: 'datecolumn'
		},{
			align		: 'left',
			dataIndex	: 'issued',
			editor		: { xtype: 'textfield', emptyText: 'name of issuing authority', selectOnFocus: true },
			flex		: 2,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Issued by'
		},{
			align		: 'center',
			dataIndex	: 'validity',
			editor		: { xtype: 'datefield', emptyText: 'expiration date', format: Ext.Date.patterns.NormalDate, selectOnFocus: true }, 
			filter		: { type: 'date' },
			format		: Ext.Date.patterns.NormalDate,
			text		: 'Expires',
			width		: 120,
			xtype		: 'datecolumn'
		},{
			align		: 'left',
			dataIndex	: 'description',
			editor		: { xtype: 'textfield', emptyText: 'description', selectOnFocus: true },
			flex		: 2,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Description'
		}];
	},

	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.callParent(arguments);
		me.hideComponent(actions.saveRecords, readOnly);
	}

});
