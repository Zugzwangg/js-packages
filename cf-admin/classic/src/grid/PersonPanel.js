Ext.define('CFJS.admin.grid.PersonPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-person-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{persons}',
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.USERS,
	plugins			: [{ ptype: 'gridfilters' }],
    title			: 'Сompany employees',

	getRowClass: function(record, rowIndex, rowParams, store) {
		if (!record.get('active')) return 'gray';
	},

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	renderColumns: function() {
		return [{ 
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'post.name',
			filter		: { emptyText: 'post' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Post', 
			tpl			: '<tpl if="post">{post.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'username',
			filter		: { emptyText: 'username' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'User name'
		},{
			align		: 'left',
			dataIndex	: 'phone',
			filter		: { emptyText: 'phone number', itemDefaults : { maskRe: /[0-9]/ } },
			style		: { textAlign: 'center' },
			text		: 'Phone',
			width		: 130
		},{
			align		: 'left',
			dataIndex	: 'email',
			filter		: { emptyText: 'e-mail', itemDefaults: { vtype: 'email' } } ,
			style		: { textAlign: 'center' },
			text		: 'E-mail',
			width		: 130
		},{
			xtype		: 'booleancolumn',
			align		: 'center',
			dataIndex	: 'active',
			falseText	: 'No',
			filter		: { type: 'boolean' },
			text		: 'Active',
			trueText	: 'Yes',
			width		: 80
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'lastLogin',
			filter		: { type: 'date', dataType: 'date-time' },
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Last login',
			width		: 170
		}];
	}

});