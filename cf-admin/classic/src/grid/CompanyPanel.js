Ext.define('CFJS.admin.grid.CompanyPanel', {
	extend			: 'CFJS.admin.grid.ClientPanel',
	xtype			: 'admin-gird-company-panel',
	bind			: '{companies}',
	datatipTpl		: '<b>ID:</b> {id}</br><b>Author:</b> {author}</br><b>Created:</b> {startDate:date("d.m.Y")}', 
	iconCls			: CFJS.UI.iconCls.BUILDING,
	reference		: 'companyGridPanel',
    title			: 'Companies',
	
    renderColumns: function() {
		return [{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'registrationNumber',
			filter		: { type: 'string', emptyText: 'registration number', itemDefaults: { maskRe: /[0-9]/ } },
			style		: { textAlign: 'center' },
			text		: 'Reg. number',
			tpl			: '<tpl if="registration">{registration}</tpl>',
			width		: 90
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'country.name',
			filter		: { emptyText: 'country name' },
			style		: { textAlign: 'center' },
			text		: 'Country',
			tpl			: '<tpl if="country">{country.name}</tpl>',
			width		: 150
		},{
			dataIndex	: 'name',
			align		: 'left',
			filter		: { emptyText: 'name' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'parent.name',
			filter		: { emptyText: 'parent company name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Subordinates',
			tpl			: '<tpl if="parent">{[values.parent.name]}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'unit.name',
			filter		: { emptyText: 'unit' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Unit',
			tpl			: '<tpl if="unit">{unit.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'phone',
			filter		: { emptyText: 'phone', itemDefaults: { maskRe: /[0-9]/ } },
			style		: { textAlign: 'center' },
			text		: 'Phone',
			width		: 130
		},{
			align		: 'left',
			dataIndex	: 'email',
			filter		: { emptyText: 'e-mail', itemDefaults: { vtype: 'email' } },
			style		: { textAlign: 'center' },
			text		: 'E-mail',
			width		: 130
		},{
			align		: 'left',
			dataIndex	: 'customerType',
			filter		: { type: 'list', dataType: 'string', itemDefaults: { hideOnClick: true }, labelField: 'name', nullValue: '...', operator: 'eq', store: 'companyCustomerTypes', single: true },
			renderer	: Ext.util.Format.enumRenderer('companyCustomerTypes'),
			style		: { textAlign: 'center' },
			text		: 'Customer type',
			width		: 120
		},{
			align		: 'left',
			dataIndex	: 'supplierType',
			filter		: { type: 'list', dataType: 'string', itemDefaults: { hideOnClick: true }, labelField: 'name', nullValue: '...', operator: 'eq', store: 'supplierTypes', single: true },
			renderer	: Ext.util.Format.enumRenderer('supplierTypes'),
			style		: { textAlign: 'center' },
			text		: 'Supplier type',
			width		: 130
		}];
	}

});
