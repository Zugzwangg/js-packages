Ext.define('CFJS.admin.grid.MemorableDatesPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-memorable-dates-panel',
	requires		: [ 'Ext.grid.plugin.RowEditing' ],
	actions			: {
		addRecord	: { handler: 'onAddMemoriableDate' },
		clearFilters: false,
		editRecord	: { handler	: 'onEditMemoriableDate' },
		periodPicker: false,
		reloadStore	: false,
		removeRecord: { handler: 'onRemoveMemoriableDate' }
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] }
	},
	bbar			: undefined,
	contextMenu		: { items: new Array(3) },
	defDescription	: 'Description anniversary',
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(3) },
	height			: 250,
	iconCls			: CFJS.UI.iconCls.CALENDAR,
	plugins			: {
		ptype		: 'rowediting',
		clicksToEdit: 2,
		listeners	: { beforeedit: function(editor, context) { return !this.grid.readOnly; } }
	},
	title			: 'Memorable dates',

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	renderColumns: function() {
		var me = this;
		return [{ 
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'date',
			editor		: { xtype: 'datefield', allowBlank: false, format: Ext.Date.patterns.NormalDate, selectOnFocus: true },
			format		: Ext.Date.patterns.NormalDate,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Date',
			width		: 150
		},{
			align		: 'left',
			dataIndex	: 'description',
			editor		: { allowBlank: false, selectOnFocus: true },
			flex		: 1,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Description'
		}];
	}

});