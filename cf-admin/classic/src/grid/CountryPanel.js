Ext.define('CFJS.admin.grid.CountryPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-country-panel',
	bind			: '{countries}',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.GLOBE,
	plugins			: [{ ptype: 'gridfilters' }],
    title			: 'Countries',
	
    initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	renderColumns: function() {
		return [{ 
			align		: 'left',
			dataIndex	: 'name',
			filter		: { emptyText: 'specify the country name' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			xtype		: 'numbercolumn',
			align		: 'left',
			dataIndex	: 'phoneCode',
			filter		: { type: 'number', emptyText: 'specify the phone code' },
			format		:'+0',
			style		: { textAlign: 'center' },
			text		: 'Phone code',
			width		: 150
		},{
			align		: 'center',
			dataIndex	: 'iataCode',
			filter		: { emptyText: 'specify the IATA code' },
			style		: { textAlign: 'center' },
			text		: 'IATA code',
			width		: 100
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'population',
			filter		: { type: 'number', emptyText: 'population size' },
			format		:'0',
			style		: { textAlign: 'center' },
			text		: 'Population',
			width		: 140
		}];
	}
});