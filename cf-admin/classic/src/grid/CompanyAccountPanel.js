Ext.define('CFJS.admin.grid.CompanyAccountPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-company-account-panel',
	requires		: [
		'Ext.grid.plugin.RowEditing',
		'CFJS.app.InlineEditorController',
		'CFJS.admin.grid.BankPanel',
		'CFJS.admin.view.CompanyAccountsModel'
	],
	actions			: {
		clearFilters: false,
		periodPicker: false,
		saveRecords	: {
			handler	: 'onSaveRecords',
			iconCls	: CFJS.UI.iconCls.SAVE,
			title	: 'Save the changes',
			tooltip	: 'Save the changes'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'saveRecords' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'saveRecords', 'reloadStore' ] }
	},
	contextMenu		: { items: new Array(4) },
	controller		: 'inlineeditor',
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(5) },
	height			: 250,
	iconCls			: CFJS.UI.iconCls.BANK,
	plugins			: [{
		ptype: 'gridfilters'
	},{
		ptype		: 'rowediting', 
		clicksToEdit: 2, 
		listeners	: { beforeedit: function(editor, context) { return !this.grid.readOnly; } }
	}],
	reference		: 'companyAccountGridPanel',
    title			: 'Bank accounts',
	viewModel		: {	type: 'admin.company-accounts' },

	afterRender: function() {
		var me = this, viewModel = me.lookupViewModel();
		me.callParent();
		if (viewModel) viewModel.bind('{_parentItemName_.id}', this.bindCompanyId, this);
	},

	bindCompanyId: function(id) {
		var saveRecords = this.actions.saveRecords;
		if (saveRecords) saveRecords.setDisabled(id <= 0);
	},

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	lookupGrid: function() {
		return this;
	},
	
	renderColumns: function() {
		return [{ 
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'bank',
			editor		: {
				xtype			: 'dictionarypicker',
				name			: 'bank',
				selectorWindow	: {
					xtype		: 'window-inline-dictionary-picker',
			    	selectorType: 'admin-grid-bank-panel',
					listeners	: {
						addrecord: function(selector, component) {
							if (selector) selector.store.add({ code: '000000', name: 'Новый банк' });
						}
					}
				},
				selectOnFocus	: true,
				store			: { type: 'banks', autoLoad: true }
			},
			filter		: { type: 'string', dataIndex: 'bank.name', emptyText: 'specify the bank' },
			flex		: 1,
			sorter		: 'bank.name',
			style		: { textAlign: 'center' },
			text		: 'Bank',
			tpl			: '<tpl if="bank">{bank.name} (code {bank.code})</tpl>'
		},{	
			align		: 'left',
			dataIndex	: 'code',
			editor		: { allowBlank: false, emptyText: 'account number', maskRe: /[0-9]/, maxLength: 14, minLength: 5, selectOnFocus: true },
			filter		: { type: 'string', emptyText: 'account number', maskRe: /[0-9]/ },
			style		: { textAlign: 'center' },
			text		: 'S/a',
			width		: 150
		},{
			align		: 'left',
			dataIndex	: 'name',
			editor		: { allowBlank: false, emptyText: 'account name' },
			filter		: { type: 'string', emptyText: 'account name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		}];
	},

	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.callParent(arguments);
		me.hideComponent(actions.saveRecords, readOnly);
	}

});
