Ext.define('CFJS.admin.grid.TourPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-tour-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{tours}',
	header			: { items: new Array(5) },
	iconCls			: 'x-fa fa-ticket',
	plugins			: [{ ptype: 'gridfilters' }],
    title			: 'Tours',

    initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	renderColumns: function() {
		return [{ 
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'hotel.name',
			filter		: { emptyText: 'hotel name' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Hotel', 
			tpl			: '<tpl if="hotel">{hotel.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'roomType.name',
			filter		: { emptyText: 'room type' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Room type', 
			tpl			: '<tpl if="roomType">{roomType.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'hotelFood.name',
			filter		: { emptyText: 'food' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Food', 
			tpl			: '<tpl if="hotelFood">{hotelFood.name}</tpl>'
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'checkIn',
			filter		: { type: 'date' },
			format		: Ext.Date.patterns.NormalDate,
			text		: 'Check in',
			width		: 120
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'duration',
			filter		: { type: 'number', emptyText: 'specify duration' },
			format		: '0',
			style		: { textAlign: 'center' },
			text		: 'Duration',
			width		: 100
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'adultCount',
			filter		: { type: 'number', emptyText: 'specify adult count' },
			format		: '0',
			style		: { textAlign: 'center' },
			text		: 'Adults',
			width		: 100
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'childCount',
			filter		: { type: 'number', emptyText: 'specify children count' },
			format		: '0',
			style		: { textAlign: 'center' },
			text		: 'Children',
			width		: 100
		},{
			xtype		: 'templatecolumn',
			dataIndex	: 'price',
			filter		: { type: 'number', emptyText: 'specify the price' },
			style		: { textAlign: 'center' },
			text		: 'Price',
			tpl			: '{price:number("0,000,000.00##")} <tpl if="currency">{currency.name}</tpl>',
			width		: 140
		}];
	}

});