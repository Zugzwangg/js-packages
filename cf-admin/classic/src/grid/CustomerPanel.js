Ext.define('CFJS.admin.grid.CustomerPanel', {
	extend			: 'CFJS.admin.grid.ClientPanel',
	xtype			: 'admin-grid-customer-panel',
	bind			: '{customers}',
	datatipTpl		: '<b>ID:</b> {id}</br><b>Author:</b> {author}</br><b>Created:</b> {startDate:date("d.m.Y")}', 
	iconCls			: CFJS.UI.iconCls.USER,
	reference		: 'customerGridPanel',
    title			: 'Humans',
    
    renderColumns: function() {
		return [{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			align		: 'left',
			dataIndex	: 'middleName',
			filter		: { type: 'string', emptyText: 'middle name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Middle name' 
		},{
			align		: 'left',
			dataIndex	: 'lastName',
			tooltipType	: 'data-tip',
			filter		: { type: 'string', emptyText: 'surname' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Surname'
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'birthday',
			filter		: { type: 'date', dataType: 'date-time' },
			format		: Ext.Date.patterns.NormalDate,
			text		: 'Birthday',
			width		: 100
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'company.name',
			filter		: { type: 'string', emptyText: 'company name' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Company',
			tpl			: '<tpl if="company">{company.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'unit.name',
			filter		: { type: 'string', emptyText: 'unit' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Unit',
			tpl			: '<tpl if="unit">{unit.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'phone',
			filter		: { type: 'string', emptyText: 'phone number', itemDefaults : { maskRe: /[0-9]/ } },
			style		: { textAlign: 'center' },
			text		: 'Phone',
			width		: 130
		},{
			align		: 'left',
			dataIndex	: 'email',
			filter		: { type: 'string', emptyText: 'e-mail' } ,
			style		: { textAlign: 'center' },
			text		: 'E-mail',
			width		: 130
		},{
			align		: 'left',
			dataIndex	: 'type',
			filter		: { type: 'list', dataType: 'string', itemDefaults: { hideOnClick: true }, labelField: 'name', nullValue: '...', operator: 'eq', store: 'customerTypes', single: true },
			renderer	: Ext.util.Format.enumRenderer('customerTypes'),
			style		: { textAlign: 'center' },
			text		: 'Customer type',
			width		: 120
		}];
    }
});
