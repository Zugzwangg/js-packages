Ext.define('CFJS.admin.grid.BankPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-bank-panel',
	bbar			: [{ xtype: 'pagingtoolbar', displayInfo: true }],
	bind			: '{banks}',
	enableColumnHide: false,
	iconCls			: CFJS.UI.iconCls.BANK,
	plugins			: [{ ptype: 'gridfilters' },{ ptype: 'cellediting', clicksToEdit: 1 }],
	reference		: 'bankGridPanel',
    title			: 'Banks',
    
	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	renderColumns: function() {
		return [{ 
			align		: 'left',
			dataIndex	: 'code',
			editor		: { allowBlank: false, emptyText: 'specify bank code', maskRe: /[0-9.]/, maxLength: 6, minLength: 6 },
			filter		: { emptyText: 'specify bank code', itemDefaults: { maskRe: /[0-9.]/, maxLength: 6, minLength: 6 } },
			style		: { textAlign: 'center' },
			text		: 'Bank code',
			width		: 90
		},{
			align		: 'left',
			dataIndex	: 'name',
			editor		: { allowBlank: false, emptyText: 'specify bank name' },
			filter		: { emptyText: 'specify bank name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		}];
	}

});
