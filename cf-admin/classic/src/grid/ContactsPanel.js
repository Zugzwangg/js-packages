Ext.define('CFJS.admin.grid.ContactsPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-contacts-panel',
	requires		: [ 'Ext.grid.plugin.RowEditing' ],
	actions			: {
		addRecord	: { handler: 'onAddContact' },
		clearFilters: false,
		editRecord	: { handler	: 'onEditContact' },
		periodPicker: false,
		reloadStore	: false,
		removeRecord: { handler: 'onRemoveContact' }
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] }
	},
	bbar			: undefined,
	contextMenu		: { items: new Array(3) },
	defContactName	: 'Contact name',
	defContactValue	: 'Contact value',
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(3) },
	height			: 250,
	iconCls			: 'x-fa fa-map-marker',
	plugins			: {
		ptype		: 'rowediting',
		clicksToEdit: 2,
		listeners	: { beforeedit: function(editor, context) { return !this.grid.readOnly; } }
	},
	title			: 'Additional contacts',

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	renderColumns: function() {
		var me = this;
		return [{ 
			align		: 'left',
			dataIndex	: 'name',
			editor		: { allowBlank: false, selectOnFocus: true },
			flex		: 1,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			align		: 'left',
			dataIndex	: 'contact',
			editor		: { allowBlank: false, selectOnFocus: true },
			flex		: 2,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Contact'
		}];
	}

});