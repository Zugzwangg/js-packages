Ext.define('CFJS.admin.grid.RegionPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-region-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{regions}',
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.MAP,
	plugins			: [{ ptype: 'gridfilters' }],
    title			: 'Regions',

    initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	renderColumns: function() {
		return [{ 
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'country.name',
			filter		: { emptyText: 'country name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Country', 
			tpl			: '<tpl if="country">{country.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { emptyText: 'region name' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'population',
			filter		: { type: 'number', emptyText: 'population size' },
			format		:'0',
			style		: { textAlign: 'center' },
			text		: 'Population',
			width		: 140
		}];
	}
});