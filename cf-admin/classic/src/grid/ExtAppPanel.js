Ext.define('CFJS.admin.grid.ExtAppPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-extapp-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{applications}',
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.CERTIFICATE,
	plugins			: [{ ptype: 'gridfilters' }],
	rowNumberer		: false,
    title			: 'External applications',

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	renderColumns: function() {
		return [{
			xtype		: 'numbercolumn',
			dataIndex	: 'id',
			filter		: { type: 'number', emptyText: 'specify ID' },
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'ID',
			width		: 80
		},{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { emptyText: 'application name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			align		: 'left',
			dataIndex	: 'organization',
			flex		: 1,
			menuDisabled: true,
			sortable	: false,
			style		: { textAlign: 'center' },
			text		: 'Organization'
		},{
			align		: 'left',
			dataIndex	: 'unitName',
			flex		: 1,
			menuDisabled: true,
			sortable	: false,
			style		: { textAlign: 'center' },
			text		: 'Unit'
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'certificateNumber',
			format		: '0',
			menuDisabled: true,
			sortable	: false,
			style		: { textAlign: 'center' },
			text     	: 'Certificate (S/N)',
			width		: 140
		},{
			xtype		: 'booleancolumn',
			align		: 'center',
			dataIndex	: 'active',
			falseText	: 'No',
			filter		: { type: 'boolean' },
			text		: 'Active',
			trueText	: 'Yes',
			width		: 100
		}];
	}
	
});