Ext.define('CFJS.admin.grid.PostPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-post-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{posts}',
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.USER_SECRET,
	plugins			: [{ ptype: 'gridfilters' }],
    title			: 'Сompany positions',

	afterRender: function() {
		var me = this, vm = me.lookupViewModel(), permissions = vm && vm.getStore('permissions'),
			column = Ext.Array.findBy(Ext.Array.from(me.getColumns()), function(item, index) {
				return item && item.dataIndex === 'permissionLevel';
			});
		if (column && permissions && permissions.isStore) {
			column.renderer = function(value, metaData, record) {
				value = permissions.findRecord('level', value || record.permissionLevel(), 0, false, false, true) || value;
				return value && value.isModel ? value.get('name') : value;
			}
		}
		me.callParent(arguments);
	},

	getRowClass: function(record, rowIndex, rowParams, store) {
		if (!record.get('active')) return 'gray';
    },
    
	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	renderColumns: function() {
		return [{ 
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'post title' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Title'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'unit.name',
			filter		: { type: 'string', emptyText: 'unit name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Unit', 
			tpl			: '<tpl if="unit">{unit.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'boss.name',
			filter		: { type: 'string', emptyText: 'boss name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Boss', 
			tpl			: '<tpl if="boss">{boss.name}<tpl else>---</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'person.name',
			filter		: { type: 'string', emptyText: 'person name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Person', 
			tpl			: '<tpl if="person">{person.name}<tpl else>---</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'permissionLevel',
			flex		: 1,
			sortable	: false,
			style		: { textAlign: 'center' },
			text		: 'Permissions'
		}];
	}

});