Ext.define('CFJS.admin.grid.ConsumerPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-consumer-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{consumers}',
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.USERS,
	plugins			: [{ ptype: 'gridfilters' }],
	title			: 'Consumers',
	
	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},
	
	renderColumns: function() {
		return [{ 
			align		: 'left',
			dataIndex	: 'namePrefix',
			filter		: { dataIndex: 'namePrefix', emptyText: 'name prefix' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name prefix'
		},{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { dataIndex: 'name', emptyText: 'name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			align		: 'left',
			dataIndex	: 'middleName',
			filter		: { dataIndex: 'middleName', emptyText: 'middle name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Middle name' 
		},{
			align		: 'left',
			dataIndex	: 'lastName',
			tooltipType	: 'data-tip',
			filter		: { dataIndex: 'lastName', emptyText: 'surname' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Surname'
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'birthday',
			filter		: { type: 'date', dataType: 'date-time' },
			format		: Ext.Date.patterns.NormalDate,
			text		: 'Birthday',
			width		: 100
		},{
			align		: 'left',
			dataIndex	: 'phone',
			filter		: { emptyText: 'phone number', itemDefaults : { maskRe: /[0-9]/ } },
			style		: { textAlign: 'center' },
			text		: 'Phone',
			width		: 130
		},{
			align		: 'left',
			dataIndex	: 'email',
			filter		: { emptyText: 'e-mail' } ,
			style		: { textAlign: 'center' },
			text		: 'E-mail',
			width		: 130
		}];
	}

});