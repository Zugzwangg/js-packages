Ext.define('CFJS.admin.grid.HotelPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-hotel-panel',
	actions			: {
		browseFiles: {
			handler	: 'onBrowseFiles',
			iconCls	: CFJS.UI.iconCls.FILES,
			title	: 'Browse files',
			tooltip	: 'View additional files'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'browseFiles' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'browseFiles', 'clearFilters', 'reloadStore' ] }
	},
	actionsDisable	: [ 'removeRecord', 'editRecord', 'browseFiles' ],
	bind			: '{hotels}',
	contextMenu		: { items: new Array(4) },
	header			: { items: new Array(6) },
	iconCls			: CFJS.UI.iconCls.HOTEL,
	plugins			: [{ ptype: 'gridfilters' }],
	title			: 'Hotels',

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	renderColumns: function() {
		return [{ 
			align		: 'left',
			dataIndex	: 'name',
			filter		: { emptyText: 'specify the hotel name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'city.name',
			filter		: { emptyText: 'specify the city' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'City', 
			tpl			: '<tpl if="city">{city.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'hotelClass.name',
			filter		: { emptyText: 'specify the class' },
			style		: { textAlign: 'center' },
			text		: 'Class', 
			tpl			: '<tpl if="hotelClass">{hotelClass.name}</tpl>',
			width		: 120
		},{
			xtype		: 'numbercolumn',
			align		: 'left',
			dataIndex	: 'rating',
			filter		: { type: 'number', emptyText: 'specify the rating' },
			format		:'0.00',
			style		: { textAlign: 'center' },
			text		: 'Rating',
			width		: 120
		},{
			xtype		: 'numbercolumn',
			align		: 'left',
			dataIndex	: 'latitude',
			filter		: { type: 'number', emptyText: 'specify the latitude' },
			format		:'0.0000000',
			style		: { textAlign: 'center' },
			text		: 'Latitude',
			width		: 130
		},{
			xtype		: 'numbercolumn',
			align		: 'left',
			dataIndex	: 'longitude',
			filter		: { type: 'number', emptyText: 'specify the longitude' },
			format		:'0.0000000',
			style		: { textAlign: 'center' },
			text		: 'Longitude',
			width		: 130
		}];
	}
	
});