Ext.define('CFJS.admin.grid.CityPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-city-panel',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	bind			: '{cities}',
	header			: { items: new Array(5) },
	iconCls			: CFJS.UI.iconCls.MAP_MARKER,
	plugins			: [{ ptype: 'gridfilters' }],
	title			: 'Cities',
	
	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	renderColumns: function() {
		return [{ 
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'country.name',
			filter		: { emptyText: 'country name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Country', 
			tpl			: '<tpl if="country">{country.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'region.name',
			filter		: { emptyText: 'region name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Region', 
			tpl			: '<tpl if="region">{region.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { emptyText: 'city name' },
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Name'
		},{
			align		: 'center',
			dataIndex	: 'iataCode',
			filter		: { emptyText: 'specify the IATA code' },
			style		: { textAlign: 'center' },
			text		: 'IATA code',
			width		: 100
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'unit.name',
			filter		: { emptyText: 'unit name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Unit',
			tpl			: '<tpl if="unit">{unit.name}</tpl>'
		},{
			xtype		: 'numbercolumn',
			align		: 'left',
			dataIndex	: 'latitude',
			filter		: { type: 'number', emptyText: 'specify the latitude' },
			format		:'0.0000000',
			style		: { textAlign: 'center' },
			text		: 'Latitude',
			width		: 130
		},{
			xtype		: 'numbercolumn',
			align		: 'left',
			dataIndex	: 'longitude',
			filter		: { type: 'number', emptyText: 'specify the longitude' },
			format		:'0.0000000',
			style		: { textAlign: 'center' },
			text		: 'Longitude',
			width		: 130
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'population',
			filter		: { type: 'number', emptyText: 'population size' },
			format		:'0',
			style		: { textAlign: 'center' },
			text		: 'Population',
			width		: 140
		}];
	}

});