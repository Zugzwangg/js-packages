Ext.define('CFJS.admin.grid.BalancePanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-balance-panel',
	requires		: [
		'Ext.grid.plugin.RowEditing',
		'CFJS.admin.view.BalancesModel',
		'CFJS.app.InlineEditorController',
		'CFJS.form.field.DictionaryComboBox',
		'CFJS.store.dictionary.Currencies'
	],
	actions			: {
		clearFilters: false,
		periodPicker: false,
		saveRecords	: {
			handler	: 'onSaveRecords',
			iconCls	: CFJS.UI.iconCls.SAVE,
			title	: 'Save the changes',
			tooltip	: 'Save the changes'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'saveRecords' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'saveRecords', 'reloadStore' ] }
	},
	contextMenu		: { items: new Array(4) },
	controller		: 'inlineeditor',
	enableColumnHide: false,
	enableColumnMove: false,
	header			: { items: new Array(5) },
	height			: 250,
	iconCls			: CFJS.UI.iconCls.MONEY,
	plugins			: [{
		ptype: 'gridfilters'
	},{
		ptype		: 'rowediting', 
		clicksToEdit: 2, 
		listeners	: { beforeedit: function(editor, context) { return !this.grid.readOnly; } }
	}],
	reference		: 'balanceGridPanel',
    title			: 'Virtual accounts',
	viewModel		: {	type: 'admin.balances' },

	afterRender: function() {
		var me = this, viewModel = me.lookupViewModel();
		me.callParent();
		if (viewModel) viewModel.bind('{_parentItemName_.id}', this.bindClientId, this);
	},

	bindClientId: function(id) {
		var saveRecords = this.actions.saveRecords;
		if (saveRecords) saveRecords.setDisabled(id <= 0);
	},

	initComponent: function() {
		var me = this;
		me.columns = me.renderColumns();
		me.callParent();
	},

	lookupGrid: function() {
		return this;
	},
	
	renderColumns: function() {
		return [{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'currency',
			editor		: {
				xtype			: 'dictionarycombo',
				autoLoadOnValue	: true,
				displayField	: 'code',
				emptyText		: 'specify the currency',
				name			: 'currency',
				minChars 		: 1,
				publishes		: 'value',
				selectOnFocus	: true,
				store			: { type: 'currencies' },
			},
			filter		: { type: 'string', dataIndex: 'currency.name', emptyText: 'specify the currency' },
			flex		: 1,
			sorter		: 'currency.name',
			style		: { textAlign: 'center' },
			text		: 'Currency',
			tpl			: '<tpl if="currency">{currency.code} ({currency.name})</tpl>'
		},{
			xtype		: 'templatecolumn',
			dataIndex	: 'limit',
			editor		: { xtype: 'numberfield', autoStripChars: true, emptyText: 'specify the amount', selectOnFocus: true },
			filter		: { type: 'number', emptyText: 'specify the amount' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Limit',
			tpl			: '{limit:number("0,000,000.00##")} <tpl if="currency">{currency.name}</tpl>'
		},{
			xtype		: 'numbercolumn',
			dataIndex	: 'daysToPay',
			editor		: { xtype: 'numberfield', allowBlank: false, allowDecimals: false, autoStripChars: true, emptyText: 'specify days for payment', minValue: 0, selectOnFocus: true },
			filter		: { type: 'number', emptyText: 'specify days for payment' },
			format		: '0',
			style		: { textAlign: 'center' },
			text     	: 'Days for payment',
			width		: 140
		},{
			xtype		: 'templatecolumn',
			dataIndex	: 'balance',
			flex		: 1,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Balance',
			tpl			: '{balance:number("0,000,000.00##")} <tpl if="currency">{currency.name}</tpl>'
		},{
			xtype		: 'templatecolumn',
			dataIndex	: 'availableAmount',
			flex		: 1,
			menuDisabled: true,
			style		: { textAlign: 'center' },
			text		: 'Available balance',
			tpl			: '{availableAmount:number("0,000,000.00##")} <tpl if="currency">{currency.name}</tpl>'
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'debtDate',
			format		: Ext.Date.patterns.LongDateTime,
			menuDisabled: true,
			text		: 'Date of debt',
			width		: 180
		}];
	},
	
	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.callParent(arguments);
		me.hideComponent(actions.saveRecords, readOnly);
	}
	
});