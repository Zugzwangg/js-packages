Ext.define('CFJS.admin.grid.SecretaryPanel', {
	extend			: 'CFJS.grid.EventMembersPanel',
	xtype			: 'admin-grid-secretary-panel',
	requires		: [ 'CFJS.store.orgchart.Secretaries' ],
	config			: { post: null },
	endDateField	: 'endDate',
	endDateText		: 'End at',
	nameField		: 'secretary',
	nameRenderer	: function(value) { return (value||{}).name; },
	nameSorter		: 'secretary.name',
	nameText		: 'User',
	selectors		: [ 'post' ],
	sortableColumns	: true,
	title			: 'Secretaries',
	valueEditor		: {
		xtype		: 'datefield',
		allowBlank	: false,
		format		: Ext.Date.patterns.NormalDate,
		name		: 'startDate'
	},
	valueField		: 'startDate',
	valueText		: 'Start at',
	
	applyPost: function(post) {
		if (post) {
			if (post.isModel) post = post.getData();
			post = { id: post.id, name: post.name };
		}
		return post;
	},

	renderColumns: function() {
		var me = this, columns = CFJS.apply(me.callParent(), [{},{
			xtype	: 'datecolumn',
			align	: 'center',
			format	: me.valueEditor.format,
			width	: 140
		}]);
		delete columns[1].flex;
		columns.splice(2, 0,  CFJS.apply(Ext.clone(columns[1]), {
			dataIndex	: me.endDateField,
			editor		: me.endDateEditor || me.valueEditor,
			itemId		: me.endDateItemId || me.endDateField,
			text		: me.endDateText
		}));
		return columns;
	},
	
	initComponent: function() {
		var me = this;
		me.postPickerConfig = CFJS.apply(me.postPickerConfig || {}, { readOnly: true });
		me.callParent();
	},
	
	onRemoveMember: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this, store = me.getStore(), user;
		if (!me.readOnly && record && record.isEntity) {
			me.confirmRemove((record.get('secretary')||{}).name, function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					record.erase({
						callback: function(rec, operation, success) {
							if (success) store.remove(record);
							me.disableComponent(component);
						}
					});
				}
			});
		}
	},
	
	onSelectUser: function(selector, record) {
		var me = this, store = me.getStore();
		if (!me.readOnly && store && store.isStore) {
			if (store.add(me.transformUserRecord(record)).length > 0) store.sync();
		}
	},

	privates: {
		
		isRemoveDisabled: function(view, rowIndex, colIndex, item, record) {
			if (!view.grid.readOnly) {
				var endDate = record.get('endDate');
				return !!endDate && !Ext.Date.after(endDate, new Date());
			}
			return true;
		},
		
		transformUserRecord: function(record) {
			var post = this.getPost();
			if (record && record.isModel && post.id !== record.getId()) {
				return {
					post		: post,
					secretary	: { id: record.getId(), name: record.get('fullName') },
					startDate	: new Date()
				}
			}
		}
	
	}

});