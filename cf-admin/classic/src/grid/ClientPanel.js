Ext.define('CFJS.admin.grid.ClientPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-gird-client-panel',
	actions			: {
		browseFiles: {
			handler	: 'onBrowseFiles',
			iconCls	: CFJS.UI.iconCls.FILES,
			title	: 'Browse files',
			tooltip	: 'View additional files'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'browseFiles' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'browseFiles', 'clearFilters', 'reloadStore' ] }
	},
	actionsDisable	: [ 'removeRecord', 'editRecord', 'browseFiles' ],
	contextMenu		: { items: new Array(4) },
	datatipTpl		: '',
	header			: { items: new Array(6) },

	initComponent: function() {
		var me = this, locale = CFJS.getLocale(), 
			plugins = me.plugins = me.plugins || [],
			plugin, dataIndex;
		Ext.Array.each(me.columns = Ext.Array.from(me.columns), function(column, idx, columns) {
			if ((dataIndex = column.dataIndex) && dataIndex.endsWith('name_')) column.dataIndex += locale;
		});
		if (!(plugin = me.findPlugin('gridfilters'))) plugins.push({ ptype: 'gridfilters' });
		if (!(plugin = me.findPlugin('datatip'))) plugins.push({ ptype: 'datatip', tpl: me.datatipTpl });
		else if (!plugin.tpl) plugin.tpl = me.datatipTpl;
		me.columns = me.renderColumns();
		me.callParent();
	}

});