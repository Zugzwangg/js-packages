Ext.define('CFJS.admin.grid.HotelImagesBrowser', {
	extend			: 'CFJS.grid.FileBrowser',
	xtype			: 'admin-grid-hotel-images-browser',
	config			: {
		actions		: {
			makeMain: {
				handler		: 'onMakeMain',
				iconCls		: 'x-fa fa-check',
				isDisabled	: function(view, rowIdx, colIdx, item, record) { var state = record.get('state'); return  state !== 'OK' && state !== 'IN_QUEUE'; },
				title		: 'Make main',
				tooltip		: 'Make main'
			}
		}
	},
	
	renderColumns: function() {
		var me = this, actions = me.getActions();
		return CFJS.apply([{
			align		: 'left',
			dataIndex	: 'name',
			filter		: { type: 'string', emptyText: 'file name' },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Name',
			tpl			: '<span class="{iconCls}" style="margin-right: 5px;"></span>{name}',
			xtype		: 'templatecolumn'
		},{
			align		: 'right',
			dataIndex	: 'size',
			renderer	: Ext.util.Format.fileSize,
			style		: { textAlign: 'center' },
			text		: 'Size',
			width		: 150
		},{
			align		: 'center',
			dataIndex	: 'date',
			filter		: 'date',
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Last modified',
			width		: 170,
			xtype		: 'datecolumn'
		},{
			align		: 'left',
			dataIndex	: 'state',
			renderer	: me.rendererStatus,
			style		: { textAlign: 'center' },
			text		: 'State',
			width		: 150
		},{
			align		: 'center',
			items		: [ actions.downloadFile, actions.removeFile, actions.makeMain ],
			menuDisabled: true,
			sortable	: false,
			width		: 70,
			xtype		: 'actioncolumn'
		}], me.columnsText);
	},
	
	onMakeMain: function(component, rowIndex, colIndex, item, e, record, row) {
		var hotel = this.hotel;
		if (record && record.isModel && hotel && hotel.isModel) {
			hotel.set('mainImage', record.get('name'));
		}
	}
	
});