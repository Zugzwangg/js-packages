Ext.define('CFJS.admin.tree.UnitPanel', {
	extend			: 'CFJS.tree.BaseTreePanel',
	xtype			: 'admin-unit-tree-panel',
	bind			: '{units}',
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'editRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	header			: { items: new Array(6) },
	iconCls			: 'x-fa fa-cubes',
//	plugins			: [{ ptype: 'gridfilters' }],
    title			: 'Сompany departments',
	columns			: [{
		xtype		: 'treecolumn',
		align		: 'left',
		dataIndex	: 'name',
		filter		: { emptyText: 'unit title' },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Title'
	},{
		align		: 'left',
		dataIndex	: 'staffId',
		filter		: { emptyText: 'staff ID' },
		style		: { textAlign: 'center' },
		text		: 'Staff ID'
	},{
		align		: 'left',
		dataIndex	: 'edrpou',
		filter		: { emptyText: 'EDRPOU' },
		style		: { textAlign: 'center' },
		text		: 'EDRPOU'
	},{
		align		: 'left',
		dataIndex	: 'vat',
		filter		: { emptyText: 'VAT ID' },
		style		: { textAlign: 'center' },
		text		: 'VAT ID'
	},{
		align		: 'left',
		dataIndex	: 'tax',
		filter		: { emptyText: 'tax ID' },
		style		: { textAlign: 'center' },
		text		: 'Tax ID',
		width		: 120
	},{
		align		: 'left',
		dataIndex	: 'phone',
		filter		: { type: 'string', emptyText: 'phone', itemDefaults: { maskRe: /[0-9]/ } },
		style		: { textAlign: 'center' },
		text		: 'Phone',
		width		: 130
	}]
});