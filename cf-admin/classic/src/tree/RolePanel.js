Ext.define('CFJS.admin.tree.RolePanel', {
	extend		: 'CFJS.tree.BaseTreePanel',
	xtype		: 'admin-role-tree-panel',
	requires	: [
		'CFJS.admin.controller.Roles',
		'CFJS.admin.panel.RoleEditor',
		'CFJS.admin.view.RolesModel'
	],
	actions		: {
		addRecord	: { title: 'Add a role', tooltip: 'Add new role' },
		clearFilters: false,
		editRecord	: false,
		periodPicker: false,
		removeRecord: { title: 'Delete roles', tooltip: 'Remove selected roles'	},
		saveRecord	: {
			formBind: true,
			iconCls	: CFJS.UI.iconCls.SAVE,
			tooltip	: 'Save the changes',
			handler	: 'onSaveRecord'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord' ] },
		header		: { items: [ 'addRecord', 'removeRecord', 'saveRecord', 'reloadStore' ] }
	},
	bind		: '{roles}',
	columns		: [{
		xtype		: 'treecolumn',
		align		: 'left',
		dataIndex	: 'name',
		filter		: { emptyText: 'role title' },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Title'
	}],
	contextMenu	: { items: new Array(2) },
	controller	: 'admin.roles',
	header		: { items: new Array(4) },
    title		: 'Сompany roles',
    viewModel	: { type: 'admin.roles' },
	
	createRoleEditor: function(config) {
		var me = this,
			editor = CFJS.apply({
				xtype	: 'admin-panel-role-editor',
				dock	: 'bottom',
				name	: 'roleEditor'
			}, config);
		return editor;
	},
	
    initComponent: function() {
		var me = this;
		me.dockedItems = [ me.ensureChild('roleEditor') ];
		me.callParent();
		
	},
	
	lookupGrid: function() {
		return this;
	}

});