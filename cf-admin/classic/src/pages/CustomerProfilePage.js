Ext.define('CFJS.admin.pages.CustomerProfilePage', {
	extend		: 'CFJS.admin.panel.CustomerEditor',
	xtype		: 'admin-customer-profile',
	requires	: [
		'CFJS.admin.controller.CustomerProfile',
		'CFJS.admin.view.CustomerProfileModel',
		'CFJS.grid.FileBrowser'
	],
	actions		: { back: false },
	actionsMap	: { header: { items: [ 'browseFiles', 'refreshRecord', 'saveRecord' ] } },
	controller	: 'admin.customer-profile',
	headerConfig: { defaultType: 'button', titlePosition: 0 },
	header		: { items: new Array(3) },
	iconCls		: CFJS.UI.iconCls.USER,
	title		: 'Customer profile',
	sections	: {
		author			: false,
		contacts		: false,
		customerIDs		: false,
		workPosition	: { collapsed: false },
		address			: { collapsed: false },
		memorableDates	: false,
		additional		: false
	},
	session		: { schema: 'dictionary' },
	viewModel	: { type: 'admin.customer-profile' }
	
});