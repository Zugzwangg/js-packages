Ext.define('CFJS.admin.pages.StaffingTable', {
	extend				: 'Ext.form.Panel',
	alternateClassName	: 'CFJS.pages.StaffingTable',
	xtype				: 'admin-staffing-table',
	requires			: [
		'Ext.layout.container.Border',
		'CFJS.admin.tree.RolePanel',
		'CFJS.admin.view.Posts'
	],
	layout				: 'border',
	readOnly			: false,
	
	destroy: function() {
		var me = this, tree = me.lookupTree(), selModel;
		if (me.rendered && !me.destroyed) {
			selModel = tree && Ext.isFunction(tree.getSelectionModel) && tree.getSelectionModel();
			if (selModel) selModel.deselectAll();
		}
		me.callParent(arguments);
	},

	createPostsView: function(config) {
		var me = this,
			view = CFJS.apply({
				xtype		: 'admin-view-posts',
				collapsible	: false,
				name		: 'postsView',
				region		: 'center'
			}, config);
		return view;
	},
	
	createRolesTree: function(config) {
		var me = this,
			controlTree = CFJS.apply({
				xtype		: 'admin-role-tree-panel',
				collapsible	: true,
				floatable	: false,
				iconCls		: null,
				listeners	: { selectionchange: 'onTreeSelectionChange', scope: me },
				minWidth	: 200,
				name		: 'rolesTree',
				region		:'west',
				split		: true,
				width		: 350
			}, config);
		return controlTree;
	},

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.items = [ me.ensureChild('rolesTree'), me.ensureChild('postsView') ];
		me.callParent();
		me.lookupTree().contextMenu = me.contextMenu;
	},
	
	lookupEditor: function() {
		return this.lookupChild('postsView');
	},
	
	lookupTree: function() {
		return this.lookupChild('rolesTree');
	},

	onTreeSelectionChange: function(selectable, records) {
		var me = this, vm = me.lookupEditor().getViewModel(),
			role = (records = Ext.Array.from(records)).length > 0 ? records[0] : null,
			filter = { id: 'role', property: 'role.id', type: 'numeric', operation: 'eq', value: (role||{}).id };
		if (vm && vm.isBaseModel) vm.setStoreFilter('posts', filter);
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.lookupEditor().setReadOnly(readOnly);
		me.lookupTree().setReadOnly(readOnly);
		me.readOnly = readOnly;
	}

});