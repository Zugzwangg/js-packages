Ext.define('CFJS.admin.view.Tours', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Tours',
	xtype				: 'admin-view-tours',
	requires			: [
		'CFJS.admin.controller.Tours',
		'CFJS.admin.grid.TourPanel',
		'CFJS.admin.panel.TourEditor',
		'CFJS.admin.view.ToursModel'
	],
	controller			: 'admin.tours',
	gridConfig			: { xtype: 'admin-grid-tour-panel', listeners: { rowdblclick: 'onGridDblClick' } },
	session				: { schema: 'travel' },
	viewModel			: { type: 'admin.tours' }
});