Ext.define('CFJS.admin.view.Countries', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Countries',
	xtype				: 'admin-view-countries',
	requires			: [
		'CFJS.admin.controller.Countries',
		'CFJS.admin.grid.CountryPanel',
		'CFJS.admin.panel.CountryEditor',
		'CFJS.admin.view.CountriesModel'
	],
	controller			: 'admin.countries',
	gridConfig			: { xtype: 'admin-grid-country-panel', listeners: { rowdblclick: 'onGridDblClick' } },
	session				: { schema: 'dictionary' },
	viewModel			: { type: 'admin.countries' }
});