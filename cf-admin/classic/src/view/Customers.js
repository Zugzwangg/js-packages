Ext.define('CFJS.admin.view.Customers', {
	extend				: 'CFJS.admin.view.Clients',
	alternateClassName	: 'CFJS.view.Customers',
	xtype				: 'admin-view-customers',
	requires			: [
		'CFJS.admin.grid.CustomerPanel',
		'CFJS.admin.panel.CustomerEditor',
		'CFJS.admin.controller.Customers',
		'CFJS.admin.view.CustomersModel'
	],
	controller			: 'admin.customers',
	gridConfig			: { xtype: 'admin-grid-customer-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	viewModel			: { type: 'admin.customers' }
});