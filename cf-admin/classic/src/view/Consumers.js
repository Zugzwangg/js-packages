Ext.define('CFJS.admin.view.Consumers', {
	extend				: 'CFJS.admin.view.Clients',
	alternateClassName	: 'CFJS.view.Consumers',
	xtype				: 'admin-view-consumers',
	requires			: [
		'CFJS.admin.controller.Consumers',
		'CFJS.admin.grid.ConsumerPanel',
		'CFJS.admin.panel.ConsumerEditor',
		'CFJS.admin.view.ConsumersModel'
	],
	controller			: 'admin.consumers',
	gridConfig			: { xtype: 'admin-grid-consumer-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	viewModel			: { type: 'admin.consumers' }
});