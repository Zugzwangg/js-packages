Ext.define('CFJS.admin.view.Regions', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Regions',
	xtype				: 'admin-view-regions',
	requires			: [
		'CFJS.admin.controller.Regions',
		'CFJS.admin.grid.RegionPanel',
		'CFJS.admin.panel.RegionEditor',
		'CFJS.admin.view.RegionsModel'
	],
	controller			: 'admin.regions',
	gridConfig			: { xtype: 'admin-grid-region-panel', listeners: { rowdblclick: 'onGridDblClick' } },
	session				: { schema: 'dictionary' },
	viewModel			: { type: 'admin.regions' }
});