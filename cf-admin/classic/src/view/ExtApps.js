Ext.define('CFJS.admin.view.ExtApps', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.ExtApps',
	xtype				: 'admin-view-extapps',
	requires			: [
		'CFJS.admin.controller.ExtApps',
		'CFJS.admin.grid.ExtAppPanel',
		'CFJS.admin.panel.ExtAppEditor',
		'CFJS.admin.view.ExtAppsModel'
	],
	controller			: 'admin.extapps',
	gridConfig			: { xtype: 'admin-grid-extapp-panel', listeners: { rowdblclick: 'onGridDblClick' } },
	session				: { schema: 'public' },
	viewModel			: { type: 'admin.extapps' }
});