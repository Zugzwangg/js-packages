Ext.define('CFJS.admin.view.Persons', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Persons',
	xtype				: 'admin-view-persons',
	requires			: [
		'CFJS.admin.controller.Persons',
		'CFJS.admin.grid.PersonPanel',
		'CFJS.admin.panel.PersonEditor',
		'CFJS.admin.view.PersonsModel',
		'CFJS.window.FileBrowser'
	],
	controller			: 'admin.persons',
	gridConfig			: { xtype: 'admin-grid-person-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	session				: { schema: 'org_chart' },
	viewModel			: { type: 'admin.persons' }
});