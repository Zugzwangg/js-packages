Ext.define('CFJS.admin.view.Units', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Units',
	xtype				: 'admin-view-units',
	requires			: [
		'CFJS.admin.controller.Units',
		'CFJS.admin.panel.UnitEditor',
		'CFJS.admin.tree.UnitPanel',
		'CFJS.admin.view.UnitsModel'
	],
	controller			: 'admin.units',
	gridConfig			: { xtype: 'admin-unit-tree-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	viewModel			: { type: 'admin.units' }
});