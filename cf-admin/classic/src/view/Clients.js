Ext.define('CFJS.admin.view.Clients', {
	extend	: 'CFJS.admin.view.DictionaryView',
	requires: [ 'CFJS.window.FileBrowser' ],
	session	: { schema: 'dictionary' }
});