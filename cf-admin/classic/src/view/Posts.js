Ext.define('CFJS.admin.view.Posts', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Posts',
	xtype				: 'admin-view-posts',
	requires			: [ 'CFJS.admin.form.field.PostPickerWindow' ],
	controller			: 'admin.posts',
	gridConfig			: { xtype: 'admin-grid-post-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	session				: { schema: 'org_chart' },
	viewModel			: { type: 'admin.posts' }
});