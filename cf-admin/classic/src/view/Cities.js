Ext.define('CFJS.admin.view.Cities', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Cities',
	xtype				: 'admin-view-cities',
	requires			: [
		'CFJS.admin.controller.Cities',
		'CFJS.admin.grid.CityPanel',
		'CFJS.admin.panel.CityEditor',
		'CFJS.admin.view.CitiesModel'
	],
	controller			: 'admin.cities',
	gridConfig			: { xtype: 'admin-grid-city-panel', listeners: { rowdblclick: 'onGridDblClick' } },
	session				: { schema: 'dictionary' },
	viewModel			: { type: 'admin.cities' }
});