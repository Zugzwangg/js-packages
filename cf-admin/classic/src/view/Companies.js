Ext.define('CFJS.admin.view.Companies', {
	extend				: 'CFJS.admin.view.Clients',
	alternateClassName	: 'CFJS.view.Companies',
	xtype				: 'admin-view-companies',
	requires			: [
		'CFJS.admin.controller.Companies',
		'CFJS.admin.grid.CompanyPanel',
		'CFJS.admin.panel.CompanyEditor',
		'CFJS.admin.view.CompaniesModel'
	],
	controller			: 'admin.companies',
	gridConfig			: { xtype: 'admin-gird-company-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	viewModel			: { type: 'admin.companies' }
});