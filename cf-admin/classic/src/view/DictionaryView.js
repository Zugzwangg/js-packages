Ext.define('CFJS.admin.view.DictionaryView', {
	extend	: 'Ext.container.Container',
	requires: [ 'CFJS.app.BaseEditorModel', 'CFJS.admin.controller.Base' ],
	layout	: { type: 'card', anchor: '100%' },

	initItems: function() {
		var me = this;
		if (me.items && !me.items.isMixedCollection) me.items = null;
		me.callParent();
		if (!me.grid || !me.grid.isPanel) {
			me.grid = me.add(CFJS.apply({ xtype: 'grid' }, me.gridConfig));
		}
	},
	
	lookupGrid: function() {
		return this.grid;
	},

	lookupStore: function() {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	}

});