Ext.define('CFJS.admin.view.Hotels', {
	extend				: 'CFJS.admin.view.DictionaryView',
	alternateClassName	: 'CFJS.view.Hotels',
	xtype				: 'admin-view-hotels',
	requires			: [
		'CFJS.admin.controller.Hotels',
		'CFJS.admin.grid.HotelImagesBrowser',
		'CFJS.admin.grid.HotelPanel',
		'CFJS.admin.panel.HotelEditor',
		'CFJS.admin.view.HotelsModel',
		'CFJS.window.FileBrowser'
	],
	controller			: 'admin.hotels',
	gridConfig			: { xtype: 'admin-grid-hotel-panel', listeners: { itemdblclick: 'onGridDblClick' } },
	session				: { schema: 'hotel' },
	viewModel			: { type: 'admin.hotels' }
});