Ext.define('CFJS.admin.form.field.PersonPickerWindow', {
	extend		: 'CFJS.admin.form.field.DictionaryPickerWindow',
	xtype		: 'admin-window-person-picker',
	requires	: [
		'CFJS.admin.controller.Persons',
		'CFJS.admin.grid.PersonPanel',
		'CFJS.admin.panel.PersonEditor',
		'CFJS.admin.view.PersonsModel'
	],
	actions		: {
		addRecord	: { title: 'Add employee', tooltip: 'Add new employee' },
		editRecord	: { title: 'Edit employee', tooltip: 'Edit the current employee' },
		selectRecord: { title: 'Select employee', tooltip: 'Select the current employee' }
	},
	controller	: 'admin.persons',
	config		: {
		selectorConfig: { xtype: 'admin-grid-person-panel' }
	},
	viewModel	: { 
		type	: 'admin.persons',
		itemName: 'personPicker',
		session : Ext.data.Session.create({ schema: 'org_chart' })
	}
});