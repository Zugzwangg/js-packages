Ext.define('CFJS.admin.form.field.ContractorPicker', {
	extend				: 'Ext.form.field.Text',
	xtype				: 'contractorpicker',
	requires			: [
		'Ext.layout.container.Fit',
		'CFJS.admin.form.field.CompanyPickerWindow',
		'CFJS.admin.form.field.CustomerPickerWindow'
	],
	config				: {
		closeOnFocusLeave	: true,
		displayField		: 'name',
		hideClearTrigger	: true,
		triggers			: {
	         clear		: {
	        	 extraCls	: CFJS.UI.iconCls.CANCEL,
	             handler	: 'onClearClick',
	             tooltip	: 'Clear',
	             weight		: 2
	         },
	         company	: {
	        	 extraCls	: CFJS.UI.iconCls.BUILDING,
	             handler	: 'onTriggerClick',
	             tooltip	: 'Company',
	             weight		: 0
	         },
	         customer	: {
	        	 extraCls	: CFJS.UI.iconCls.USER,
	             handler	: 'onTriggerClick',
	             tooltip	: 'Customer',
	             weight		: 1
	         }
	     }
	},
	isExpanded			: false,
	rawValueData		: true,
	repeatTriggerClick	: true,
	renderConfig		: { editable: false },
	
	applyTriggers: function(triggers) {
		var me = this, key;
		for (key in triggers) {
			Ext.apply(triggers[key], {
	        	 cls	: Ext.baseCSSPrefix + 'form-fa-trigger',
	             scope	: triggers[key].scope || me,
			});
		}
		if (triggers.clear) triggers.clear.hidden = me.hideClearTrigger;  
		return me.callParent([triggers]);
	},
	
	beforeSelectItem: function(selector, record) {
		return record ? true : false;
	},

	clearValue: function() {
		this.setValue(null);
	},

	collapse: function(hidePicker) {
		var me = this, openCls = me.openCls, picker = me.picker;
		if (me.isExpanded && !me.destroyed && !me.destroying) {
			if (hidePicker !== false) picker.close();
			me.picker = picker.pickerField = null;
			me.isExpanded = false;
			me.touchListeners.destroy();
			me.fireEvent('collapse', me);
			me.onCollapse();
		}
	},

	collapseIf: function(e) {
		var me = this;
		if (!me.destroyed && !e.within(me.bodyEl, false, true) && !me.owns(e.target) && !Ext.fly(e.target).isFocusable()) {
			me.collapse();
		}
	},
	
	createPicker: function(type) {
		var me = this,
			picker = Ext.apply({
				xtype			: 'admin-window-' + type + '-picker',
				closeAction		: 'destroy',
				contractorType	: type,
				minWidth		: 540,
				minHeight		: 200,
				selectorType	: me.selectorType || 'grid'
			}, me.pickerConfig),
			selector = picker.selectorConfig = Ext.apply(picker.selectorConfig || {}, me.selectorConfig);

		me.picker = picker = Ext.create(picker);
		me.selector = selector = picker.selector;

		picker.on({
			beforeselect: 'beforeSelectItem',
			close		: 'onPickerCancel',
			select		: 'selectItem',
			scope		: me
		});
		return me.picker;
	},

	doDestroy: function(){
		var me = this;
		if (me.picker) me.picker = me.picker.pickerField = null;
		me.callParent();
    },
    
	expand: function(type) {
		var me = this, picker;
		if (me.rendered && !me.isExpanded && !me.destroyed) {
			picker = me.getPicker(type);
			picker.show();
			me.isExpanded = true;
			me.touchListeners = Ext.getDoc().on({
				translate	: false,
				touchstart	: me.collapseIf,
				scope		: me,
				delegated	: false,
				destroyable	: true
			});
			me.fireEvent('expand', me);
			me.onExpand();
		}
	},

	getContractorType() {
		return (this.picker || {}).contractorType;
	},
	
	getPicker: function(type) {
		var me = this, picker = me.picker;
		if (!picker || picker.contractorType !== type) {
			me.creatingPicker = true;
			if (picker && picker.contractorType !== type) {
				picker.close();
				me.picker = picker.pickerField = null;
			}
			me.picker = picker = me.createPicker(type);
			picker.ownerCmp = me;
			delete me.creatingPicker;
		}
		return me.picker;
	},
	
	getRefItems: function() {
        var result = [];
        if (this.picker) result[0] = this.picker;
        return result;
    },

	getSelection: function() {
		var selModel = this.getSelectionModel(), selection = selModel ? selModel.getSelection() : null;
		return selection && selection.length ? selModel.getLastSelected() : null;
	},
	
	getSelectionModel: function() {
		return this.selector ? this.selector.getSelectionModel() : null;
	},

	getValue: function() {
		var me = this, value = me.value;
		if (me.rawValueData && value && value.isModel) value = value.getData();
		return value;
	},
    
	isEqualAsString: function(value1, value2) {
		if (value1 && value1.isModel) value1 = value1.getData();
		if (value2 && value2.isModel) value2 = value2.getData();
		return JSON.stringify(value1 || '') === JSON.stringify(value2 || '');
	},
	
	onClearClick: function(picker, trigger, e) {
		this.clearValue();
	},
	
	onCollapse: Ext.emptyFn,
	
	onEsc: function(e) {
		if (Ext.isIE) e.preventDefault();
		if (this.isExpanded) {
			this.collapse();
			e.stopEvent();
		}
	},
	
	onExpand: Ext.emptyFn,

	onFocusLeave: function(e) {
		if (this.closeOnFocusLeave) {
			this.collapse();
			this.callParent([e]);
		}
	},

	onPickerCancel: function () {
		this.collapse(false);
	},

	onTriggerClick: function(picker, trigger, e) {
		if (!picker.readOnly && !picker.disabled) {
			if (picker.isExpanded) picker.collapse();
			else picker.expand(trigger.id);
		}
	},

	selectItem: function(selector, record) {
		var me = this;
		me.setValue(record);
		me.fireEvent('select', me, record);
		me.collapse();
	},
	
	setValue: function(value) {
		var me = this;
		value = me.toRecord(value && value.isModel? value.getData() : value);
		if (value && value.isCustomerInfo) {
			me.setRawValue(value.get(me.displayField));
		} else me.setRawValue('');
		me.value = value;
		me.checkChange();
		return me;
	},
	
	toRecord: function(value) {
		if (value && !value.isCustomerInfo) {
			value = new CFJS.model.CustomerInfo({
				customId: value.customId,
				id		: value.id,
				name	: value.name,
				type	: value.type || this.getContractorType()
			});
		}
		return value;
	}

});