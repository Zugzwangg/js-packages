Ext.define('CFJS.admin.form.field.CompanyPickerWindow', {
	extend		: 'CFJS.admin.form.field.DictionaryPickerWindow',
	xtype		: 'admin-window-company-picker',
	requires	: [
		'CFJS.admin.controller.Companies',
		'CFJS.admin.grid.CompanyPanel',
		'CFJS.admin.panel.CompanyEditor',
		'CFJS.admin.view.CompaniesModel'
	],
	actions		: {
		addRecord	: { title: 'Add company', tooltip: 'Add new company' },
		editRecord	: { title: 'Edit company', tooltip: 'Edit the current company' },
		selectRecord: { title: 'Select company', tooltip: 'Select the current company' }
	},
	config		: {
		selectorConfig	: { xtype: 'admin-gird-company-panel' }
	},
	controller	: 'admin.companies',
	viewModel	: {
		type	: 'admin.companies',
		itemName: 'companyPicker',
		stores	: { companies: { filters: [] } },
		session : Ext.data.Session.create({ schema: 'dictionary' })
	}
});