Ext.define('CFJS.admin.form.field.CustomerPickerWindow', {
	extend		: 'CFJS.admin.form.field.DictionaryPickerWindow',
	xtype		: 'admin-window-customer-picker',
	requires	: [
		'CFJS.admin.controller.Customers',
		'CFJS.admin.grid.CustomerPanel',
		'CFJS.admin.panel.CustomerEditor',
		'CFJS.admin.view.CustomersModel'
	],
	actions		: {
		addRecord	: { title: 'Add client', tooltip: 'Add new client' },
		editRecord	: { title: 'Edit client', tooltip: 'Edit the current client' },
		selectRecord	: { title: 'Select client', tooltip: 'Select the current client' }
	},
	controller	: 'admin.customers',
	config		: {
		selectorConfig: { xtype: 'admin-grid-customer-panel' }
	},
	viewModel	: {
		type	: 'admin.customers',
		itemName: 'customerPicker',
		stores	: { customers: { filters: [] } },
		session : Ext.data.Session.create({ schema: 'dictionary' })
	}
});