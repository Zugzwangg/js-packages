Ext.define('CFJS.admin.form.field.DictionaryPickerWindow', {
	extend	: 'CFJS.form.field.DictionaryPickerWindow',
	requires: [ 'Ext.layout.container.Card' ],
	
	initComponent: function() {
		var me = Ext.apply(this, {
			layout				: { type: 'card', anchor: '100%' },
			maximizable			: false,
			maximized			: true,
			modal				: true
		});
		me.callParent();
	},

	lookupGrid: function() {
		return this.selector;
	},
	
	privates: {

		applyController: function (controller) {
			controller = this.callParent(arguments);
			if (controller) {
				controller.onGridDblClick = controller.onRemoveRecord = Ext.emptyFn;
				controller.editorConfig = function(options) {
					return Ext.apply({
						actionsMap	: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord', 'browseFiles' ] } },
						header		: { items: new Array(5) }
					}, options);
				}
			}
			return controller;
		},
		
		applyDefaultListenerScope: function() {
			return false;
		}
	
	}

});