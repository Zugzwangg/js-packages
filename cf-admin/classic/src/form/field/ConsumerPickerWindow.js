Ext.define('CFJS.admin.form.field.ConsumerPickerWindow', {
	extend		: 'CFJS.admin.form.field.DictionaryPickerWindow',
	xtype		: 'admin-window-consumer-picker',
	requires	: [
		'CFJS.admin.controller.Consumers',
		'CFJS.admin.grid.ConsumerPanel',
		'CFJS.admin.panel.ConsumerEditor',
		'CFJS.admin.view.ConsumersModel'
	],
	actions		: {
		addRecord	: { title: 'Add consumer', tooltip: 'Add new consumer' },
		editRecord	: { title: 'Edit consumer', tooltip: 'Edit the current consumer' },
		selectRecord: { title: 'Select consumer', tooltip: 'Select the current consumer' }
	},
	config		: {
		selectorConfig: { xtype: 'admin-grid-consumer-panel' }
	},
	controller	: 'admin.consumers',
	viewModel	: {
		type	: 'admin.consumers',
		itemName: 'customerPicker',
		stores	: { consumers: { filters: [] } },
		session : Ext.data.Session.create({ schema: 'dictionary' })
	}
});