Ext.define('CFJS.admin.form.field.PostPickerWindow', {
	extend		: 'CFJS.admin.form.field.DictionaryPickerWindow',
	xtype		: 'admin-window-post-picker',
	requires	: [
		'CFJS.admin.controller.Posts',
		'CFJS.admin.grid.PostPanel',
		'CFJS.admin.panel.PostEditor',
		'CFJS.admin.view.PostsModel'
	],
	actions		: {
		addRecord	: { title: 'Add position', tooltip: 'Add new position' },
		editRecord	: { title: 'Edit position', tooltip: 'Edit the current position' },
		selectRecord: { title: 'Select position', tooltip: 'Select the current position' }
	},
	controller	: 'admin.posts',
	config		: { selectorConfig: { xtype: 'admin-grid-post-panel' } },
	viewModel	: { 
		type	: 'admin.posts',
		itemName: 'postPicker',
		session : Ext.data.Session.create({ schema: 'org_chart' })
	}
});