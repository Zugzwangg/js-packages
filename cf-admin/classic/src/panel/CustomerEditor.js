Ext.define('CFJS.admin.panel.CustomerEditor', {
	extend		: 'CFJS.admin.panel.ClientEditor',
	xtype		: 'admin-panel-customer-editor',
	requires	: [
		'CFJS.admin.grid.BalancePanel',
		'CFJS.admin.grid.ContactsPanel',
		'CFJS.admin.grid.IDPanel',
		'CFJS.admin.grid.MemorableDatesPanel',
		'CFJS.admin.view.CustomerIDsModel', 
		'CFJS.admin.view.CustomerModel', 
		'CFJS.store.dictionary.Countries', 
		'CFJS.store.dictionary.CustomerTypes',
		'CFJS.store.dictionary.GenderTypes', 
		'CFJS.store.dictionary.PostTypes' 
	],
	actions		: { back: { tooltip: 'Back to humans list' } },
	config		: {
		sections: {
			additional		: {
				title	: 'Special Information',
				name	: 'additional',
				order	: 10,
				items	: [{
					items: [{
						xtype				: 'combobox',
						bind				: { value: '{_itemName_.type}' },
						displayField		: 'name',
						editable			: false,
						emptyText			: 'select customer type',
			            fieldLabel			: 'Customer type',
						flex				: 2,
						name				: 'customerType',
						publishes			: 'value',
						queryMode			: 'local',
						required			: true,
						store				: { type: 'customertypes' },
						valueField			: 'id'
					},{
						xtype				: 'dictionarycombo',
						bind				: { value: '{_itemName_.unit}' },
						emptyText			: 'select a unit',
			            fieldLabel			: 'Unit',
						flex				: 2,
						margin				: '0 0 0 5',
						minChars			: 1,
						name				: 'unit',
						publishes			: 'value',
						required			: true,
						selectOnFocus		: true,
						store				: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'unit' } },
							sorters	: [{ property: 'name'}]
						}
					}]
				},{
					xtype		: 'textarea',
					fieldLabel	: 'Notes',
					bind		: '{_itemName_.notes}',
					grow		: true,
					height		: 200,
					name		: 'notes'
				}]
			},
			address			: {
				title	: 'Mailing address',
				name	: 'address',
				order	: 7,
				items	: [{
					defaults: { margin: '0 0 0 5' },
					items	: [{
						xtype			: 'combobox',
						bind			: '{addressCountry}',
						displayField	: 'name',
						emptyText		: 'select a country',
			            fieldLabel		: 'Country',
						flex			: 2,
						margin			: 0,
						minChars 		: 1,
						name			: 'country',
						publishes		: 'value',
						selectOnFocus	: true,
						store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } },
						listeners		: {
							change: function(field, value) {
								var me = field, combo = me.ownerCt.down('dictionarycombo[name=city]'), store = combo && combo.getStore();
								if (store && store.isStore) {
									if (value) store.addFilter({ id: 'country', property: 'country.id', type: 'numeric', operator: 'eq', value: value.id });
									else store.removeFilter({ id: 'country' });
								}
							}
						}
					},{
						bind			: '{addressZipCode}',
						emptyText		: 'zip code',
						enforceMaxLength: true,
			            fieldLabel		: 'Zip code',
						flex			: 1,
						maskRe			: /[0-9]/,
						maxLength		: 6,
						name			: 'zipCode'
					},{
						bind		: '{addressRegion}',
						emptyText	: 'specify region',
			            fieldLabel	: 'Region',
						flex		: 3,
						name		: 'region'
					},{
						xtype			: 'combobox',
						bind			: { value: '{addressCity}' },
						displayField	: 'name',
						emptyText		: 'select a city',
			            fieldLabel		: 'City',
						flex			: 2,
						minChars 		: 1,
						name			: 'city',
						publishes		: 'value',
						selectOnFocus	: true,
						store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'city' } } }
					}]
				},{
					defaults: { margin: '0 0 0 5' },
					items	: [{
						bind		: '{addressStreet}',
						emptyText	: 'specify street',
			            fieldLabel	: 'Street',
						flex		: 1,
						margin		: 0,
						name		: 'street'
					},{
						bind		: '{addressBuilding}',
						emptyText	: 'number',
			            fieldLabel	: 'Building',
						name		: 'building',
						width		: 100
					},{
						bind		: '{addressRoom}',
						emptyText	: 'number',
			            fieldLabel	: 'Room',
						name		: 'room',
						width		: 100
					}]
				}]
			},
			author			: {
				xtype		: 'fieldcontainer',
				defaults	: { flex: 1, labelAlign: 'left', labelStyle: 'font-weight:bold', labelWidth	: 150, margin: '0 0 0 10' },
				defaultType	: 'displayfield',
				layout		: 'hbox',
				margin		: '10 0',
				name		: 'author',
				order		: 1,
				items		: [
					{ bind: '{_itemName_.id}',		fieldLabel: 'ID',		name: 'id' },
					{ bind: '{_itemName_.author}',	fieldLabel: 'Author',	name: 'author' },
					{ bind: '{_itemName_.created}',	fieldLabel: 'Created',	name: 'created', renderer: Ext.util.Format.dateRenderer(Ext.Date.patterns.NormalDate) }
				]
			},
			balance			: {
				xtype		: 'admin-grid-balance-panel',
				bind		: '{balances}',
				bodyPadding	: 0,
				layout		: 'fit',
				name		: 'balance',
				order		: 7,
				scrollable	: null
			},
			contact			: {
				title	: 'Contact Information',
				name	: 'contact',
				order	: 3,
				items	: [{
					defaults: { flex: 1, margin: '0 0 0 5', required: true, selectOnFocus: true },
					items	: [{
						xtype				: 'combobox',
						bind				: { value: '{_itemName_.genderType}' },
						displayField		: 'name',
						editable			: false,
						emptyText			: 'specify gender',
						fieldLabel			: 'Gender',
				        margin				: 0,
						name				: 'genderType',
						publishes			: 'value',
						queryMode			: 'local',
						selectOnFocus		: false,
						store				: { type: 'gendertypes' },
						valueField			: 'id'
					},{
						xtype				: 'datefield',
						bind				: '{_itemName_.birthday}',
						emptyText			: 'birthday',
						fieldLabel			: 'Birthday',
						format				: Ext.Date.patterns.NormalDate,
						name				: 'birthday',
						required			: false
					},{
						bind				: '{_itemName_.tax}',
						emptyText			: 'tax number',
						enforceMaxLength	: true,
						fieldLabel			: 'Tax number',
						maskRe				: /[0-9]/,
						maxLength			: 12,
						minLength			: 10,
						name				: 'tax',
						required			: false
					}]
				},{
					defaults: { flex: 1, margin: '0 0 0 5', selectOnFocus: true },
					items	: [{
						xtype		: 'phonefield',
						bind		: { value: '{_itemName_.phone}' },
						fieldLabel	: 'Phone',
				        margin		: 0,
				        name		: 'phone',
						phoneCountry: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
						required	: true,
						store		: {
							type	: 'base',
							proxy	: {
								loaderConfig: { dictionaryType: 'country_by_phone' },
								reader		: { rootProperty: 'response.transaction.list.country' }
							}
						}
					},{
						bind		: '{_itemName_.email}',
						emptyText	: 'enter e-mail',
			            fieldLabel	: 'E-mail',
						name		: 'email',
						vtype		: 'email'
					},{
						xtype		: 'checkbox',
						bind		: '{_itemName_.newsAgree}',
			            fieldLabel	: 'Receive news',
			            flex		: 0,		
						name		: 'newsAgree'
					}]
				}]
			},
			contacts		: {
				xtype		: 'admin-grid-contacts-panel',
				bodyPadding	: 0,
				bind		: '{contacts}',
				layout		: 'fit',
			    name		: 'contacts',
				order		: 4,
				scrollable	: null
			},
			customerIDs		: {
				xtype		: 'admin-grid-id-panel',
				bind		: '{customerIDs}',
				bodyPadding	: 0,
				hideOwner	: true,
				layout		: 'fit',
				name		: 'customerIDs',
				order		: 5,
				scrollable	: null,
				viewModel	: {	type: 'admin.customer-ids' }
			},
			fullName		: {
				name	: 'fullName',
				order	: 2,
				title	: 'Full name',
				items	: [{
					defaults	: { flex: 2, hideLabel: true, margin: '0 0 0 5', maskRe: /[a-z\-]/i, required: true, selectOnFocus: true },
					fieldLabel	: 'English',
					required	: true,
					items		: [
						{ bind: '{_itemName_.last_name_en}',	emptyText: 'Last name',		name: 'last_name_en',	margin: 0 },
						{ bind: '{_itemName_.name_en}', 		emptyText: 'First name',	name: 'name_en',		flex: 1 },
						{ bind: '{_itemName_.middle_name_en}',	emptyText: 'Middle name',	name: 'middle_name_en',	required: false }
					]
				},{
					defaults	: { flex: 2, hideLabel: true, margin: '0 0 0 5', maskRe: /[абвгґдеєжзиіїйклмнопрстуфхцчшщьюя'\-]/i, selectOnFocus: true },
					fieldLabel	: 'Ukrainian',
					items		: [
						{ bind: '{_itemName_.last_name_uk}',	emptyText: 'Last name', 	name: 'last_name_uk',	margin: 0 },
						{ bind: '{_itemName_.name_uk}',			emptyText: 'First name',	name: 'name_uk',		flex: 1 },
						{ bind: '{_itemName_.middle_name_uk}',	emptyText: 'Middle name',	name: 'middle_name_uk' }
					]
				},{
					defaults	: { flex: 2, hideLabel: true, margin: '0 0 0 5', maskRe: /[а-я-ё\-]/i, selectOnFocus: true },
					fieldLabel	: 'Russian',
					items		: [
						{ bind: '{_itemName_.last_name_ru}',	emptyText: 'Last name',		name: 'last_name_ru',	margin: 0 },
						{ bind: '{_itemName_.name_ru}',			emptyText: 'First name',	name: 'name_ru',		flex: 1 },
						{ bind: '{_itemName_.middle_name_ru}',	emptyText: 'Middle name',	name: 'middle_name_ru' }
					]
				}]
			},
			workPosition	: {
				title	: 'Company',
				name	: 'workPosition',
				order	: 6,
				items	: [{
					defaults: { disabled: true, flex: 2, margin: '0 0 0 5', publishes: 'value' },
					items	:[{
						xtype			: 'dictionarycombo',
						bind			: { value: '{_itemName_.company}' },
						disabled		: false,
						emptyText		: 'select company',
						fieldLabel		: 'Company',
						margin			: 0,
						name			: 'company',
						reference		: 'customerCompany',
						selectOnFocus	: true,
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'company' } },
							sorters	: [{ property: 'name' }]
						},
						listeners		: {
							change: function(field, newValue, oldValue) {
								var container = field.up('[name=workPosition]'),
									fields = ['postType', 'postName', 'workEmail', 'contactPerson', 'companyNewsAgree', 'onTheBasis'],
									isEmpty = newValue < 1,
									i, fieldName, control; 
								for (i = 0; container && i < fields.length; i++) {
									fieldName = fields[i];
									if (control = container.down('[name=' + fieldName + ']')) {
										control.setDisabled(isEmpty);
										if (isEmpty) control.setValue(null);
										if (fieldName === 'postType' || fieldName === 'postName') control.allowBlank = isEmpty;
									}
								}
							}
						}
					},{
						xtype		: 'combobox',
						bind		: { value: '{_itemName_.postType}' },
						displayField: 'name',
						editable	: false,
						emptyText	: 'specify the type of position',
						fieldLabel	: 'Position type',
						flex		: 1,
						name		: 'postType',
						queryMode	: 'local',
						store		: { type: 'posttypes' },
						valueField	: 'id',
						listeners		: {
							change: function(field, newValue, oldValue) {
								if (newValue === 'OTHER' || !oldValue) return;
								var	key = field && field.isCombo ? field.displayField : null,
									container = field.up('[name=workPosition]'),
									postName = container ? container.down('[name=postName]') : null,
									value;
								if (key && postName) {
									value = field.findRecordByValue(newValue);
									if (value) value = value.isModel ? value.get(key) : value[key];
									postName.setValue(Ext.isString(value) ? value.toLowerCase() : null);
								} 
							}
						}
					},{
						xtype		: 'textfield',
						bind		: '{_itemName_.postName}',
						emptyText	: 'specify the job title',
						fieldLabel	: 'Job title',
						name		: 'postName'
					}]
				},{
					defaults	: { disabled: true, flex: 1, margin: '0 0 0 5' },
					defaultType : 'checkbox',
					items		: [{
						bind		: '{_itemName_.contactPerson}',
			            fieldLabel	: 'Contact person',
						name		: 'contactPerson',
						listeners		: {
							change: function(field, newValue, oldValue) {
								var fieldSet = field.up('fieldset');
								if (fieldSet) {
									var workEmail = fieldSet.down('[name=workEmail]'),
									companyNewsAgree = fieldSet.down('[name=companyNewsAgree]');
									if (workEmail) workEmail.allowBlank = !(newValue || (companyNewsAgree ? companyNewsAgree.getValue() : false));
								}
							}
						}
					},{
						bind		: '{_itemName_.companyNewsAgree}',
			            fieldLabel	: 'Receive news',
						name		: 'companyNewsAgree',
						listeners		: {
							change: function(field, newValue, oldValue) {
								var fieldSet = field.up('fieldset');
								if (fieldSet) {
									var workEmail = fieldSet.down('[name=workEmail]'),
									contactPerson = fieldSet.down('[name=contactPerson]');
									if (workEmail) workEmail.allowBlank = !(newValue || (contactPerson ? contactPerson.getValue() : false));
								}
							}
						}
					},{
						xtype		: 'textfield',
						bind		: '{_itemName_.workEmail}',
						emptyText	: 'enter e-mail',
			            fieldLabel	: 'Working e-mail',
						flex		: 2,
						margin		: 0,
						name		: 'workEmail',
						vtype		: 'email'
					}]
				},{
					xtype		: 'textfield',
					bind		: '{_itemName_.onTheBasis}',
					disabled	: true,
					emptyText	: 'specify the basis for the position actions',
					fieldLabel	: 'Operates on the basis of',
					name		: 'onTheBasis'
				}]
			},
			memorableDates	: {
				xtype		: 'admin-grid-memorable-dates-panel',
				bind		: '{memorableDates}',
				bodyPadding	: 0,
				layout		: 'fit',
				name		: 'memorableDates',
				order		: 9
			}
		}
	},
	title		: 'Editing a customer',
	viewModel	: {	type: 'admin.customer-edit' }
    
});
