Ext.define('CFJS.admin.panel.ConsumerEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-consumer-editor',
	requires		: [ 'CFJS.form.field.PhoneField', 'CFJS.admin.view.ConsumerModel' ],
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: false,
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		defaults: { flex: 1 },
		items	: [{
			bind		: '{_itemName_.namePrefix}',
			emptyText	: 'name prefix',
			fieldLabel	: 'Name prefix',
			margin		: 0,
			name		: 'namePrefix'
		},{
			bind				: '{_itemName_.name}',
			emptyText			: 'first name',
			fieldLabel			: 'First name',
			name				: 'name',
			required			: true
		},{
			bind		: '{_itemName_.middleName}',
			emptyText	: 'middle name',
			fieldLabel	: 'Middle name',
			name		: 'middleName'
		},{
			bind				: '{_itemName_.lastName}',
			emptyText			: 'last name',
			fieldLabel			: 'Last name',
			name				: 'lastName',
			required			: true
		}]
	},{
		defaults: { flex: 1 },
		items	:[{
			xtype				: 'datefield',
			bind				: '{_itemName_.birthday}',
			emptyText			: 'birthday',
			fieldLabel			: 'Birthday',
			format				: Ext.Date.patterns.NormalDate,
			margin				: 0,
			name				: 'birthday',
			required			: true
		},{
			bind			: '{_itemName_.tax}',
			emptyText		: 'tax number',
			enforceMaxLength: true,
			fieldLabel		: 'Tax number',
			maskRe			: /[0-9]/,
			maxLength		: 12,
			minLength		: 10,
			name			: 'tax'
		},{
			bind			: '{_itemName_.document}',
			emptyText		: 'document',
            fieldLabel		: 'Document',
			name			: 'document',
		}]
	},{
		items: [{
			xtype		: 'phonefield',
			bind		: { value: '{_itemName_.phone}' },
			fieldLabel	: 'Phone',
	        margin		: 0,
	        name		: 'phone',
			phoneCountry: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
			store		: {
				type	: 'base',
				proxy	: {
					loaderConfig: { dictionaryType: 'country_by_phone' },
					reader		: { rootProperty: 'response.transaction.list.country' }
				}
			}
		},{
			bind		: '{_itemName_.email}',
			emptyText	: 'enter e-mail',
            fieldLabel	: 'E-mail',
            flex		: 1,
            name		: 'email',
			vtype		: 'email'
		}]
	},{
		items: [{
			xtype			: 'combobox',
			bind			: { value: '{addressCountry}' },
			displayField	: 'name',
			emptyText		: 'select a country',
            fieldLabel		: 'Country',
			flex			: 2,
			margin			: 0,
			minChars 		: 1,
			name			: 'country',
			publishes		: 'value',
			store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } },
			listeners		: {
				change: function(field, newValue, oldValue) {
					var me = field, store = me.getStore(),
						controller = me.lookupController(),
						cityCombo = controller ? controller.lookup('customerCity') : null,
						country;
					if (newValue && store && cityCombo) {
						cityCombo.getStore().clearFilter(true);
						country = store.getData().findBy(function(item, id) {
							return item && item.data.name === newValue;
						});
						if (country) cityCombo.getStore().addFilter({ id: 'country', property: 'country.id', type: 'numeric', operator: 'eq', value: country.id });
					}
				}
			}
		},{
			bind			: '{_itemName_.zipCode}',
			emptyText		: 'zip code',
			enforceMaxLength: true,
            fieldLabel		: 'Zip code',
            flex			: 1,
            maskRe			: /[0-9]/,
			maxLength		: 6,
			name			: 'zipCode'
		},{
			bind			: '{addressRegion}',
			emptyText		: 'specify region',
            fieldLabel		: 'Region',
			flex			: 3,
			name			: 'region'
		},{
			xtype			: 'combobox',
			bind			: { value: '{addressCity}' },
			displayField	: 'name',
			emptyText		: 'select a city',
            fieldLabel		: 'City',
			flex			: 2,
			minChars 		: 1,
			name			: 'city',
			publishes		: 'value',
			reference		: 'customerCity',
			store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'city' } } }
		}]
	},{
		items: [{
			bind		: '{addressStreet}',
			emptyText	: 'specify street',
            fieldLabel	: 'Street',
            flex		: 1,
            margin		: 0,
			name		: 'street'
		},{
			bind		: '{addressBuilding}',
			emptyText	: 'number',
            fieldLabel	: 'Building',
			name		: 'building',
			width		: 100
		},{
			bind		: '{addressRoom}',
			emptyText	: 'number',
            fieldLabel	: 'Room',
			name		: 'room',
			width		: 100
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a person',
	viewModel		: {	type: 'admin.consumer-edit' }
});