Ext.define('CFJS.admin.panel.ExtAppEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-extapp-editor',
	requires		: [ 'CFJS.admin.view.ExtAppModel' ],
	actions			: { back: { tooltip: 'Back to list of applications' } },
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%' },
	defaultType 	: 'textfield',
	fieldDefaults	: {
		labelAlign		: 'left',
		labelWidth		: 140,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		bind		: '{_itemName_.name}',
		emptyText	: 'input application name',
		fieldLabel	: 'Application name',
        maskRe		: /[0-9a-z\s.,\-"']/i,
		name		: 'name',
		required	: true
	},{
		bind		: '{_itemName_.organization}',
		emptyText	: 'input organization name',
		fieldLabel	: 'Organization',
        maskRe		: /[0-9a-z\s.,\-"']/i,
		name		: 'organization',
	},{
		bind		: '{_itemName_.unitName}',
		emptyText	: 'input unit name',
		fieldLabel	: 'Unit',
        maskRe		: /[0-9a-z\s.,\-"']/i,
		name		: 'unitName',
	},{
		xtype		: 'checkbox',
		bind		: '{_itemName_.active}',
		fieldLabel	: 'Active',
		name		: 'active',
	},{
		xtype		: 'checkbox',
		bind		: '{_itemName_.resetKeys}',
		fieldLabel	: 'Reset keys',
		name		: 'resetKeys',
	},{
		xtype		: 'displayfield',
		bind		: '{_itemName_.certificateNumber}',
		fieldLabel	: 'Certificate (S/N)',
		name		: 'certificateNumber',
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a application',
	viewModel		: {	type: 'admin.extapp-edit' }
});