Ext.define('CFJS.admin.panel.UnitEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-unit-editor',
	requires		: [
		'CFJS.container.ImageUpload',
		'CFJS.form.field.PhoneField',
		'CFJS.admin.view.UnitModel'
	],
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		items	: [{
			xtype		: 'displayfield',
			bind		: '{_itemName_.id}',
			fieldLabel	: 'ID',
			labelStyle	: 'font-weight:bold',
			margin		: 0,
			name		: 'id',
			width		: 50
		},{
			bind		: '{_itemName_.staffId}',
			emptyText	: 'staff ID',
			fieldLabel	: 'Staff ID',
			maskRe		: /[0-9a-z\-_]/i,
			name		: 'staffId',
			required	: true,
			width		: 150
		},{
			bind		: '{_itemName_.name}',
			emptyText	: 'unit title',
			fieldLabel	: 'Title',
			flex		: 1,
			name		: 'name',
			required	: true
		},{
			bind		: '{_itemName_.fullName}',
			emptyText	: 'input unit full name',
			fieldLabel	: 'Full name',
			flex		: 2,
			name		: 'fullName',
			required	: true
		}]
	},{
		defaults: { enforceMaxLength: true, flex: 1, maskRe: /[0-9]/, required: true },
		items	: [{
			bind		: '{_itemName_.edrpou}',
			emptyText	: 'specify registration code',
			fieldLabel	: 'Registration code',
			margin		: 0,
//			maxLength	: 8,
			minLength	: 4,
			name		: 'edrpou',
		},{
			bind		: '{_itemName_.vat}',
			emptyText	: 'specify the VAT number',
			fieldLabel	: 'VAT number',
			maxLength	: 8,
			minLength	: 6,
			name		: 'vat',
			required	: false
		},{
			bind		: '{_itemName_.tax}',
			emptyText	: 'specify individual tax number',
			fieldLabel	: 'Tax number',
//			maxLength	: 12,
//			minLength	: 11,
			name		: 'tax'
		}]
	},{
		defaults	: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: 0 },
		defaultType : 'fieldcontainer',
		items		: [{
			items	: [{
				xtype			: 'combobox',
				bind			: { value: '{addressCountry}' },
				displayField	: 'name',
				emptyText		: 'select country',
	            fieldLabel		: 'Country',
				flex			: 2,
				margin			: 0,
				minChars 		: 0,
				name			: 'country',
				publishes		: 'value',
				store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } },
				listeners		: {
					change: function(field, newValue, oldValue) {
						var me = field, store = me.getStore(),
							cityCombo = me.ownerCt.down('[name=city]'),
							country;
						if (newValue && store && cityCombo) {
							country = store.getData().findBy(function(item, id) {
								return item && item.data.name === newValue;
							});
							cityCombo.getStore().clearFilter(true);
							if (country) cityCombo.getStore().addFilter({ id: 'country', property: 'country.id', type: 'numeric', operator: 'eq', value: country.id });
						}
					}
				}
			},{
				bind			: '{addressZipCode}',
				emptyText		: 'specify ZIP code',
				enforceMaxLength: true,
	            fieldLabel		: 'ZIP code',
				flex			: 1,
				maskRe			: /[0-9]/,
				maxLength		: 6,
				name			: 'zipCode'
			},{
				bind			: '{addressRegion}',
				emptyText		: 'specify region',
	            fieldLabel		: 'Region',
				flex			: 3,
				name			: 'region'
			},{
				xtype			: 'combobox',
				bind			: { value: '{addressCity}' },
				displayField	: 'name',
				emptyText		: 'specify city',
	            fieldLabel		: 'City',
				flex			: 2,
				minChars 		: 0,
				name			: 'city',
				store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'city' } } }
			}]
		},{
			items	: [{
				bind		: '{addressStreet}',
				emptyText	: 'specify the street',
	            fieldLabel	: 'Street',
				flex		: 1,
				margin		: 0,
				name		: 'street'
			},{
				bind		: '{addressBuilding}',
				emptyText	: 'building number',
	            fieldLabel	: 'Building',
				name		: 'building',
				width		: 120
			},{
				bind		: '{addressRoom}',
				emptyText	: 'room number',
	            fieldLabel	: 'Room',
				name		: 'room',
				width		: 120
			}]
		}],
		layout		: 'anchor'
	},{
		defaultType	: 'phonefield',	
		defaults	: {
			flex		: 1,
			phoneCountry: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
			store		: {
				type	: 'base',
				proxy	: {
					loaderConfig: { dictionaryType: 'country_by_phone' },
					reader		: { rootProperty: 'response.transaction.list.country' }
				}
			}
		},
		items		: [{
			bind		: { value: '{_itemName_.phone}' },
			fieldLabel	: 'Phone',
			margin		: 0,
			name		: 'phone'
		},{
			bind		: { value: '{_itemName_.fax}' },
            fieldLabel	: 'Fax',
			margin		: '0 0 0 10',
			name		: 'fax'
		}]
	},{
		items	:[{
			xtype			: 'combobox',
			bind			: { store: '{employees}', value: '{_itemName_.director}' },
			displayField	: 'name',
			emptyText		: 'specify the manager',
			fieldLabel		: 'Manager',
			flex			: 1,
			forceSelection  : true,
			margin			: 0,
			minChars		: 0,
			name			: 'director',
			typeAhead		: true
		},{
			bind		: '{_itemName_.post}',
			emptyText	: 'specify the job title',
			fieldLabel	: 'Job title',
			flex		: 1,
			name		: 'post'
		},{
			bind		: '{_itemName_.base}',
			emptyText	: 'specify the basis for the position actions',
			fieldLabel	: 'Operates on the basis of',
			flex		: 3,
			name		: 'base'
		}]
	},{
		items	:[{
			bind		: '{_itemName_.bankCode}',
			emptyText	: 'bank code',
			fieldLabel	: 'Bank code',
			margin		: 0,
			maskRe		: /[0-9]/,
			maxLength	: 6,
			minLength	: 6,
			name		: 'bankCode',
			width		: 80
		},{
			bind		: '{_itemName_.bank}',
			fieldLabel	: 'Bank name',
			flex		: 2,
			name		: 'bank'
		},{
			bind		: '{_itemName_.account}',
			emptyText	: 'bank account number',
			fieldLabel	: 'Account number',
			flex		: 1,
			maskRe		: /[0-9]/,
			maxLength	: 14,
			minLength	: 14,
			name		: 'account'
		}]
	},{
		items	:[{
			xtype		: 'imageupload',
			afterUpload	: function(context, options, success) {
				if (success) {
					var vm = this.lookupViewModel(),
						record = vm && vm.isBaseModel && vm.getItem(), 
						response = CFJS.Api.extractResponse(context, ''),
						file;
					if (record && record.isModel && response.success) {
						file = Ext.Array.from((response.files||{}).file);
						if (file.length > 0) {
							record.set('logo', file[0].name);
							return;
						}
					}
				}
				CFJS.errorAlert('Error uploading data', context);
			},
			bind		: { src: '{logoUrl}' },
			filePath	: 'photos',
			fileType	: 'unit',
			flex		: 1,
			plugins		: [{ ptype: 'filedrop', readFiles: false }],
			title		: 'Logo image'
		},{
			xtype		: 'checkboxgroupfield',
			bind		: '{_itemName_.services}',
			columns		: 1,
			fieldLabel	: 'Services',
			flex		: 1,
			name		: 'services',
			vertical	: true
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	session			: { schema: 'org_chart' },
	scrollable		: 'y',
	title			: 'Editing a departament',
	viewModel		: {	type: 'admin.unit-edit' },
	
	initItems: function() {
		var me = this, modules = CFJS.lookupViewportViewModel().getStore('modules');
		me.callParent();
		if (modules && modules.isSiteModules) modules.initGroupField(me.down('checkboxgroupfield'));
	}
	
});