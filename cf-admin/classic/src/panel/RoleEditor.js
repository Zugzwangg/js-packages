Ext.define('CFJS.admin.panel.RoleEditor', {
	extend			: 'Ext.form.Panel',
	xtype			: 'admin-panel-role-editor',
	requires		: [
		'Ext.tab.Panel',
	],
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	items			: [{
		items	: [{
			xtype		: 'displayfield',
			bind		: '{_itemName_.id}',
			fieldLabel	: 'ID',
			labelStyle	: 'font-weight:bold',
			margin		: 0,
			name		: 'id',
			width		: 50
		},{
			bind		: '{_itemName_.name}',
			emptyText	: 'role title',
			fieldLabel	: 'Title',
			flex		: 1,
			name		: 'name',
			required	: true
		}]
//	},{
//		items	: [{
//			xtype			: 'combo',
//			bind			: { store: '{permissions}', value: '{_itemName_.permissionLevel}' },
//			displayField	: 'name',
//			editable		: false,
//			emptyText		: 'select permission level',
//			fieldLabel		: 'Permission level',
//			flex			: 1,
//			forceSelection  : true,
//			listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
//			name			: 'permissionLevel',
//			queryMode		: 'local',
//			selectOnFocus	: false,
//			valueField		: 'level'
//		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	session			: { schema: 'org_chart' },
	scrollable		: 'y',

	startEdit: function(record) {
		var vm = this.lookupViewModel();
		if (vm && vm.isBaseModel) vm.setItem(record);
	}
});