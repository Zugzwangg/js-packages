Ext.define('CFJS.admin.panel.PostEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-post-editor',
	requires		: [ 'CFJS.form.field.PhoneField', 'CFJS.admin.grid.SecretaryPanel', 'CFJS.admin.view.PostModel' ],
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: false,
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		items: [{
			bind		: '{_itemName_.name}',
			emptyText	: 'post title',
			fieldLabel	: 'Title',
			flex		: 1,
			margin		: 0,
			name		: 'name',
			required	: true
		},{
			xtype		: 'dictionarytreepicker',
			bind		: { value: '{_itemName_.role}' },
			emptyText	: 'select a role',
			fieldLabel	: 'Role',
			filterName	: 'role',
			flex		: 1,
			name		: 'role',
			required	: true,
			store		: { type: 'roles'}
		},{
			xtype		: 'dictionarycombo',
			bind		: { value: '{_itemName_.unit}' },
			emptyText	: 'select unit',
			fieldLabel	: 'Unit',
			flex		: 1,
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'unit',
			publishes	: 'value',
			required	: true,
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'unit' } },
				sorters	: [{ property: 'name'}]
			}
		}]
	},{
		items: [{
			xtype		: 'dictionarycombo',
			bind		: { value: '{_itemName_.person}' },
			emptyText	: 'select user',
			fieldLabel	: 'User',
			flex		: 1,
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			margin		: 0,
			name		: 'person',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'person' } },
				filters	: [{ property: 'active', type: 'boolean', operator: 'eq', value: true }],
				sorters	: [{ property: 'name'}]
			}
		},{
			xtype		: 'dictionarycombo',
			bind		: { value: '{_itemName_.boss}' },
			emptyText	: 'select post',
			fieldLabel	: 'Boss',
			flex		: 1,
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'boss',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'post' } },
				sorters	: [{ property: 'name'}]
			}
		},{
			xtype			: 'combo',
			bind			: { store: '{permissions}', value: '{_itemName_.permissionLevel}' },
			displayField	: 'name',
			editable		: false,
			emptyText		: 'select permission level',
			fieldLabel		: 'Permission level',
			flex			: 1,
			forceSelection  : true,
			listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name			: 'permissionLevel',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'level'
		}]
	},{
		xtype		: 'dictionarytag',
		bind		: { store: '{sectors}', value: '{_itemName_.sectors}' },
		fieldLabel	: 'Sectors',
		name		: 'sectors',
		queryParam	: 'sector.name'
	},{
		xtype	: 'container',
		items	: [{
			xtype		: 'admin-grid-secretary-panel',
			bind		: { post: '{_itemName_}', store: '{secretaries}' },
			flex		: 1,
			height		: 250,
			iconCls		: CFJS.UI.iconCls.USERS,
			layout		: 'fit',
			margin		: 5,
			name		: 'secretaries',
			scrollable	: 'y',
			userCls		: 'shadow-panel'
		}],
		margin	: '10 0 0'

	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a position',
	viewModel		: {	type: 'admin.post-edit' },
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel(), bossCombo, store;
		if (vm && vm.isBaseModel) {
			vm.bind('{_itemName_.id}', function(id) {
				me.setSecretaryPanelReadOnly(!(id > 0));
			});
			vm.bind('{_itemName_.role}', function(role) {
				if (store && store.isStore) {
					if (role && role.id > 0) store.addFilter({
						id		: 'role',
						property: 'role.id',
						type	: 'numeric',
						operator: '<=',
						value	: role.id
					});
					else store.removeFilter('role',  true);
				}
			});
		}
		me.callParent();
		bossCombo = me.down('combo[name=boss]');
		if (bossCombo && bossCombo.isCombo) store = bossCombo.getStore();
	},
	
	lookupSecretaries: function() {
		return this.down('grid[name=secretaries]');
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.setSecretaryPanelReadOnly(readOnly);
		me.callParent(arguments);
	},
	
	setSecretaryPanelReadOnly: function(readOnly) {
		var grid = this.lookupSecretaries();
		if (grid) grid.setReadOnly(readOnly);
	}

});