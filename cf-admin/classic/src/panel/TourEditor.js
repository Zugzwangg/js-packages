Ext.define('CFJS.admin.panel.TourEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-tour-editor',
	requires		: [ 'CFJS.admin.view.TourModel' ],
	actions			: { back: { tooltip: 'Back to list of tours' } },
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		defaults	: {
			flex		: 1,
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			publishes	: 'value',
			required	: true
		},
		defaultType	: 'dictionarycombo',
		items		: [{
			bind		: { value: '{_itemName_.hotel}' },
			emptyText	: 'select a hotel',
			fieldLabel	: 'Hotel',
			margin		: 0,
			name		: 'hotel',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'hotel' }, service: 'iss' },
				sorters	: [{ property: 'name'}]
			}
		}, {
			bind		: { value: '{_itemName_.roomType}' },
			emptyText	: 'room type',
			fieldLabel	: 'Room type',
			name		: 'roomType',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'room_type' }, service: 'iss' },
				sorters	: [{ property: 'name'}]
			}
		}, {
			bind		: { value: '{_itemName_.hotelFood}' },
			emptyText	: 'food',
			fieldLabel	: 'Food',
			name		: 'food',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'hotel_food' }, service: 'iss' },
				sorters	: [{ property: 'name'}]
			}
		}]
	},{
		defaults: { flex: 1, required: true },
		items	: [{
			xtype		: 'datefield',
			bind		: '{_itemName_.checkIn}',
			emptyText	: 'check in',
			fieldLabel	: 'Check in',
			margin		: 0,
			name		: 'checkIn'
		}, {
			xtype		: 'numberfield',
			bind		: '{_itemName_.duration}',
			emptyText	: 'duration',
			fieldLabel	: 'Duration',
			minValue	: 0,
			name		: 'duration'
		}]
	}, {
		defaults	: { allowDecimals: false, flex: 1, minValue: 0, required: true },
		defaultType	: 'numberfield',
		items		: [{
			bind			: '{_itemName_.adultCount}',
			emptyText		: 'adult count',
			fieldLabel		: 'Adult count',
			margin			: 0,
			name			: 'adultCount'
		}, {
			bind			: '{_itemName_.childCount}',
			emptyText		: 'child count',
			fieldLabel		: 'Child count',
			name			: 'childCount'
		}]
	}, {
		defaults: { flex: 1, required: true },
		items	: [{
			xtype		: 'numberfield',
			bind		: '{_itemName_.price}',
			emptyText	: 'price',
			fieldLabel	: 'Price',
			margin		: 0,
			minValue	: 0,
			name		: 'price'
		}, {
			xtype			: 'dictionarycombo',
			bind			: { value: '{_itemName_.currency}' },
			displayField	: 'code',			
			emptyText		: 'currency',
			fieldLabel		: 'Currency',
			name			: 'currency',
			publishes		: 'value',
			selectOnFocus	: false,
			typeAhead		: false,
			store			: { type: 'currencies' },
			width			: 85
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a tour',
	viewModel		: {	type: 'admin.tour-edit' },
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.callParent();
	}

});