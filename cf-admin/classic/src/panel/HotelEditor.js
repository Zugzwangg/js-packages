Ext.define('CFJS.admin.panel.HotelEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-hotel-editor',
	requires		: [
		'CFJS.container.ImageUpload',
		'CFJS.form.field.PhoneField',
		'CFJS.admin.view.HotelModel'
	],
	actions			: {
		back		: { tooltip: 'Back to list of hotels' },
		browseFiles	: {
			handler	: 'onBrowseFiles',
			iconCls	: CFJS.UI.iconCls.FILES,
			title	: 'Browse files',
			tooltip	: 'View additional files'
		}
	},
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord', 'browseFiles' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(5) },
	items			: [{
		defaults	: { flex: 1 },
		items		: [{
			bind		: '{_itemName_.name_en}',
			emptyText	: 'name in English',
			fieldLabel	: 'Name in English',
			margin		: 0,
	        maskRe		: /[0-9a-z\s.,\-"']/i,
			name		: 'name_en',
			required	: true
		},{
			bind		: '{_itemName_.name_uk}',
			emptyText	: 'name in Ukrainian',
			fieldLabel	: 'Name in Ukrainian',
	        maskRe		: /[0-9абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\s.,\-"']/i,
			name		: 'name_uk'
		},{
			bind		: '{_itemName_.name_ru}',
			emptyText	: 'name in Russian',
			fieldLabel	: 'Name in Russian',
	        maskRe		: /[0-9а-я-ё\s.,\-"]/i,
			name		: 'name_ru'
		}]
	},{
		defaults	: { flex: 1, required: true },
		defaultType	: 'dictionarycombo',
		items		: [{
			bind		: '{_itemName_.city}',
			emptyText	: 'select a city',
			fieldLabel	: 'City',
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			margin		: 0,
			name		: 'city',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'city' } },
				sorters	: [{ property: 'name'}]
			}
		},{
			bind		: '{_itemName_.hotelClass}',
			emptyText	: 'select a class',
			fieldLabel	: 'Class',
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'hotelClass',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'hotel_class' }, service: 'iss' },
				sorters	: [{ property: 'name'}]
			}
		},{
			xtype		: 'numberfield',
			bind		: '{_itemName_.rating}',
			emptyText	: 'select a rating',
			fieldLabel	: 'Rating',
			minValue	: 0,
			name		: 'rating',
			required	: false,
		}]
	},{
		defaults	: { flex: 1, minValue: 0 },
		defaultType	: 'numberfield',
		items		: [{
			bind		: '{_itemName_.latitude}',
			emptyText	: 'latitude',
			fieldLabel	: 'Latitude',
			margin		: 0,
			name		: 'latitude'
		},{
			bind		: '{_itemName_.longitude}',
			emptyText	: 'longitude',
			fieldLabel	: 'Longitude',
			name		: 'longitude'
		}]
	},{
		xtype		: 'textareafield',
		bind		: '{_itemName_.location}',
		emptyText	: 'location',
		fieldLabel	: 'Location',
		name		: 'location'
	},{
		xtype		: 'textareafield',
		bind		: '{_itemName_.shortDescription}',
		emptyText	: 'input short description',
		fieldLabel	: 'Short description',
		name		: 'shortDescription'
	},{
		xtype		: 'textareafield',
		bind		: '{_itemName_.fullDescription}',
		emptyText	: 'input full description',
		fieldLabel	: 'Full description',
		height		: 200,
		name		: 'fullDescription'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.beach}',
		emptyText	: 'beach',
		fieldLabel	: 'Beach',
		name		: 'beach'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.room}',
		emptyText	: 'room',
		fieldLabel	: 'Room',
		name		: 'room'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.roomTypes}',
		emptyText	: 'room types',
		fieldLabel	: 'Room types',
		name		: 'roomTypes'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.food}',
		emptyText	: 'food',
		fieldLabel	: 'Food',
		name		: 'food'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.entertainmentAndSports}',
		emptyText	: 'entertainment and sports',
		fieldLabel	: 'Entertainment and sports',
		name		: 'entertainmentAndSports'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.forKids}',
		emptyText	: 'for kids',
		fieldLabel	: 'For kids',
		name		: 'forKids'
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.territory}',
		emptyText	: 'territory',
		fieldLabel	: 'Territory',
		name		: 'territory'
	},{
		defaults	: { flex: 1 },
		defaultType	: 'phonefield',
		items		:[{
			xtype		: 'textfield',
			bind		: '{_itemName_.site}',
			emptyText	: 'site',
			fieldLabel	: 'Site',
			margin		: 0,
			name		: 'site'
		},{
			bind		: '{_itemName_.phone}',
			fieldLabel	: 'Phone',
			phoneCountry: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
			name		: 'phone',
			store		: {
				type	: 'base',
				proxy	: {
					loaderConfig: { dictionaryType: 'country_by_phone' },
					reader		: { rootProperty: 'response.transaction.list.country' }
				}
			}
		},{
			bind		: { value: '{_itemName_.fax}' },
			fieldLabel	: 'Fax',
			phoneCountry: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
			name		: 'fax',
			store		: {
				type	: 'base',
				proxy	: {
					loaderConfig: { dictionaryType: 'country_by_phone' },
					reader		: { rootProperty: 'response.transaction.list.country' }
				}
			}
		}]
	},{
		xtype		: 'textfield',
		bind		: '{_itemName_.forToddlers}',
		emptyText	: 'for toddlers',
		fieldLabel	: 'For toddlers',
		name		: 'forToddlers'
	},{
		fieldLabel	: 'Main image',
		items		:[{
			xtype		: 'imageupload',
			afterUpload	: function(context, options, success) {
				if (success) {
					var vm = this.lookupViewModel(),
						record = vm && vm.isBaseModel && vm.getItem(), 
						response = CFJS.Api.extractResponse(context, ''),
						file;
					if (record && record.isModel && response.success) {
						file = Ext.Array.from((response.files||{}).file);
						if (file.length > 0) {
							record.set('mainImage', file[0].name);
							return;
						}
					}
				}
				CFJS.errorAlert('Error uploading data', context);
			},
			bind		: { filePath: '{_itemName_.id}/images', src: '{_itemName_.mainImageUrl}' },
			fileType	: 'hotels',
			flex		: 1,
			header		: false,
			margin		: 0,
			name		: 'mainImage',
			plugins		: [{ ptype: 'filedrop', readFiles: false }]
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a hotel',
	viewModel		: {	type: 'admin.hotel-edit' },

	afterRender: function() {
		var me = this, viewModel = me.lookupViewModel();
		me.callParent();
		if (viewModel) viewModel.bind('{_itemName_.id}', this.onChangeHotel, this);
	},

	onChangeHotel: function(id) {
		var me = this, hasId = id > 0;
		me.disableComponent(me.actions.browseFiles, !hasId);
	}

});