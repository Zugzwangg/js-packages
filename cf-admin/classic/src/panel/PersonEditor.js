Ext.define('CFJS.admin.panel.PersonEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-person-editor',
	requires		: [
		'CFJS.container.ImageUpload',
		'CFJS.form.field.PhoneField',
		'CFJS.admin.view.PersonModel'
	],
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		required		: false,
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		items	: [{
			bind		: '{_itemName_.name}',
			emptyText	: 'full name',
			fieldLabel	: 'Full name',
			flex		: 2,
			margin		: 0,
			name		: 'name',
			required	: true
		},{
			xtype		: 'dictionarycombo',
			bind		: { value: '{_itemName_.post}' },
			emptyText	: 'select post',
			fieldLabel	: 'Post',
			flex		: 2,
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'post',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'post' } },
				filters	: [{ property: 'employment', type: 'numeric', operator: 'eq' }],
				sorters	: [{ property: 'name'}]
			}
		},{
			bind		: '{_itemName_.username}',
			emptyText	: 'user name',
			fieldLabel	: 'User name',
			flex		: 1,
			name		: 'username',
			required	: true
		}]
	},{
		items: [{
			xtype		: 'phonefield',
			bind		: { value: '{_itemName_.phone}' },
			fieldLabel	: 'Phone',
	        margin		: 0,
	        name		: 'phone',
			phoneCountry: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
			store		: {
				type	: 'base',
				proxy	: {
					loaderConfig: { dictionaryType: 'country_by_phone' },
					reader		: { rootProperty: 'response.transaction.list.country' }
				}
			}
		},{
			bind		: '{_itemName_.email}',
			emptyText	: 'enter e-mail',
			fieldLabel	: 'E-mail',
			flex		: 1,
			name		: 'email',
			vtype		: 'email'
		}]
	},{
		fieldLabel	: 'Photo',
		items		: [{
			xtype		: 'imageupload',
			afterUpload	: function(context, options, success) {
				if (success) {
					var vm = this.lookupViewModel(),
						record = vm && vm.isBaseModel && vm.getItem(), 
						response = CFJS.Api.extractResponse(context, ''),
						file;
					if (record && record.isModel && response.success) {
						file = Ext.Array.from((response.files||{}).file);
						if (file.length > 0) {
							record.set('photo', file[0].name);
							return;
						}
					}
				}
				CFJS.errorAlert('Error uploading data', context);
			},
			bind		: { src: '{_itemName_.photoUrl}' },
			filePath	: 'photos',
			fileType	: 'person',
			flex		: 2,
			header		: false,
			uploadImgUrl: 'images/default_person_photo.png',
			plugins		: [{ ptype: 'filedrop', readFiles: false }],
		},{
			xtype		: 'checkbox',
			bind		: '{_itemName_.active}',
			boxLabel	: 'Active',
			flex		: 1,
			name		: 'active'
		},{
			xtype		: 'checkbox',
			bind		: '{_itemName_.resetPassword}',
			boxLabel	: 'Request new password',
			flex		: 1,
			name		: 'resetPassword'
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a employee',
	viewModel		: {	type: 'admin.person-edit' }
	
});