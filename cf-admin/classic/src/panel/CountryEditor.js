Ext.define('CFJS.admin.panel.CountryEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-country-editor',
	requires		: [ 'CFJS.admin.view.CountryModel' ],
	actions			: { back: { tooltip: 'Back to list of countries' } },
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		defaults: { flex: 1 },
		items	: [{
			bind		: '{_itemName_.name_en}',
			emptyText	: 'name in English',
			fieldLabel	: 'Name in English',
			margin		: 0,
			maskRe		: /[a-z\s.,\-"]/i,
			name		: 'name_en',
			required	: true
		}, {
			bind		: '{_itemName_.name_uk}',
			emptyText	: 'name in Ukrainian',
			fieldLabel	: 'Name in Ukrainian',
			maskRe		: /[абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\s.,\-"']/i,
			name		: 'name_uk'
		}, {
			bind		: '{_itemName_.name_ru}',
			emptyText	: 'name in Russian',
			fieldLabel	: 'Name in Russian',
			maskRe		: /[а-я-ё\s.,\-"]/i,
			name		: 'name_ru'
		}],
	},{
		defaults: { flex: 1 },
		items	: [{
			bind		: '{_itemName_.phoneCode}',
			emptyText	: 'phone code',
			fieldLabel	: 'Phone code',
			margin		: 0,
			maskRe		: /[0-9]/i,
			maxLength	: 5,
			name		: 'phoneCode'
		},{
			bind		: '{_itemName_.iataCode}',
			emptyText	: 'IATA code',
			fieldLabel	: 'IATA code',
			maskRe		: /[a-z0-9]/i,
			maxLength	: 2,
			name		: 'iataCode'
		},{
			bind		: '{_itemName_.railwayCode}',
			emptyText	: 'railway code',
			fieldLabel	: 'Railway code',
			maskRe		: /[a-z0-9]/i,
			name		: 'railwayCode'
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			autoStripChars	: true,
			bind			: '{_itemName_.population}',
			emptyText		: 'population',
			fieldLabel		: 'Population',
			minValue		: 0,
			name			: 'population'
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a country',
	viewModel		: {	type: 'admin.country-edit' }

});