Ext.define('CFJS.admin.panel.CompanyEditor', {
	extend		: 'CFJS.admin.panel.ClientEditor',
	xtype		: 'admin-panel-company-editor',
	requires	: [
		'CFJS.admin.grid.BalancePanel',
		'CFJS.admin.grid.CompanyAccountPanel',
		'CFJS.admin.grid.MemorableDatesPanel',
		'CFJS.admin.view.CompanyModel',
		'CFJS.store.dictionary.Banks',
		'CFJS.store.dictionary.CompanyCustomerTypes', 
		'CFJS.store.dictionary.Countries', 
		'CFJS.store.dictionary.SupplierTypes'
	],
	actions		: { back: { tooltip: 'Назад к списку компаний' } },
	config		: {
		sections: {
			accounts		: {
				xtype		: 'admin-grid-company-account-panel',
				bind		: '{companyAccounts}',
				bodyPadding	: 0,
				layout		: 'fit',
				name		: 'accounts',
				order		: 6,
				scrollable	: null
			},
			additional		: {
				name	: 'additional',
				order	: 9,
				title	: 'Специальная информация',
				items	: [{
					xtype			: 'dictionarycombo',
					bind			: { value: '{_itemName_.unit}' },
					emptyText		: 'выберите подразделение',
					fieldLabel		: 'Подразделение',
					flex			: 1,
					name			: 'unit',
					pageSize		: 25,
					publishes		: 'value',
					required		: true,
					selectOnFocus	: true,
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'unit' } },
						sorters	: [{ property: 'name'}]
					}
				},{
					xtype		: 'textarea',
					fieldLabel	: 'Примечания',
					bind		: '{_itemName_.notes}',
					grow		: true,
					height		: 200,
					name		: 'notes'
				}]
			},
			address			: {
				name	: 'address',
				order	: 5,
				title	: 'Почтовый адрес',
				items	: [{
					defaults: { margin: '0 0 0 5' },
					items	: [{
						xtype			: 'combobox',
						bind			: '{addressCountry}',
						displayField	: 'name',
						emptyText		: 'выберите страну',
			            fieldLabel		: 'Страна',
						flex			: 2,
						margin			: 0,
						minChars 		: 0,
						name			: 'country',
						publishes		: 'value',
						selectOnFocus	: true,
						store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'country' } } },
						listeners		: {
							change: function(field, value) {
								var me = field, combo = me.ownerCt.down('dictionarycombo[name=city]'), store = combo && combo.getStore();
								if (store && store.isStore) {
									if (value) store.addFilter({ id: 'country', property: 'country.id', type: 'numeric', operator: 'eq', value: value.id });
									else store.removeFilter({ id: 'country' });
								}
							}
						}
					},{
						bind			: '{addressZipCode}',
						emptyText		: 'индекс',
						enforceMaxLength: true,
			            fieldLabel		: 'Индекс',
						flex			: 1,
						maskRe			: /[0-9]/,
						maxLength		: 6,
						name			: 'zipCode'
					},{
						bind		: '{addressRegion}',
						emptyText	: 'укажите область',
			            fieldLabel	: 'Область',
						flex		: 3,
						name		: 'region'
					},{
						xtype			: 'combobox',
						bind			: '{addressCity}',
						displayField	: 'name',
						emptyText		: 'выберите город',
			            fieldLabel		: 'Город',
						flex			: 2,
						minChars 		: 0,
						name			: 'city',
						publishes		: 'value',
						selectOnFocus	: true,
						store			: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'city' } } }
					}]
				},{
					defaults: { margin: '0 0 0 5' },
					items	: [{
						bind		: '{addressStreet}',
						emptyText	: 'укажите улицу',
			            fieldLabel	: 'Улица',
						flex		: 1,
						margin		: 0,
						name		: 'street'
					},{
						bind		: '{addressBuilding}',
						emptyText	: 'номер',
			            fieldLabel	: 'Дом',
						name		: 'building',
						width		: 100
					},{
						bind		: '{addressRoom}',
						emptyText	: 'номер',
			            fieldLabel	: 'Помещение',
						name		: 'room',
						width		: 100
					}]
				}]
			},
			author			: {
				xtype		: 'fieldcontainer',
				defaults	: { flex: 1, labelAlign: 'left', labelStyle: 'font-weight:bold', labelWidth	: 150, margin: '0 0 0 10' },
				defaultType	: 'displayfield',
				layout		: 'hbox',
				margin		: '10 0',
				order		: 1,
				items		: [
					{ bind: '{_itemName_.id}',			fieldLabel: 'Идентификатор',	name: 'id' },
					{ bind: '{_itemName_.author}',		fieldLabel: 'Автор записи',		name: 'author' },
					{ bind: '{_itemName_.startDate}',	fieldLabel: 'Создана',			name: 'startDate', renderer: Ext.util.Format.dateRenderer(Ext.Date.patterns.NormalDate) }
				]
			},
			balance			: {
				xtype		: 'admin-grid-balance-panel',
				bind		: '{balances}',
				bodyPadding	: 0,
				layout		: 'fit',
				name		: 'balance',
				order		: 7,
				scrollable	: null
			},
			contact			: {
				name	: 'contact',
				order	: 4,
				title	: 'Контактная информация',
				items	: [{
					defaultType	: 'phonefield',
					defaults	: {
						flex			: 1,
						phoneCountry	: { displayField: 'phoneCode', listConfig: { itemTpl: [ '<div>{name}</div>' ] } },
						selectOnFocus	: true,
						store			: {
							type	: 'base',
							proxy	: {
								loaderConfig: { dictionaryType: 'country_by_phone' },
								reader		: { rootProperty: 'response.transaction.list.country' }
							}
						}
					},
					items		: [{
						bind		: { value: '{_itemName_.phone}' },
						fieldLabel	: 'Телефон',
						name		: 'phone'
					},{
						bind		: { value: '{_itemName_.fax}' },
			            fieldLabel	: 'Факс',
						margin		: '0 0 0 10',
						name		: 'fax'
					}]
				},{
					items		: [{
						bind				: '{_itemName_.email}',
						emptyText			: 'введите электронную почту',
			            fieldLabel			: 'Электронная почта',
						flex				: 1,
						name				: 'email',
						required			: true,
						vtype				: 'email'
					},{
						bind				: '{_itemName_.href}',
						emptyText			: 'введите адрес гиперссылки',
			            fieldLabel			: 'Гиперссылка',
						flex				: 1,
						name				: 'href',
						margin				: '0 0 0 10',
						vtype				: 'url'
					}]
				}]
			},
			credentials		: {
				name	: 'credentials',
				order	: 3,
				title	: 'Учетные данные',
				items	: [{
					defaults: {
						enforceMaxLength: true,
						flex			: 1,
						margin			: '0 0 0 5',
						maskRe			: /[0-9]/,
						selectOnFocus	: true
					},
					items	: [{
						bind		: '{_itemName_.registration}',
						emptyText	: 'укажите регистрационный код',
						fieldLabel	: 'Регистрационный код',
						margin		: 0,
//						maxLength	: 8,
						minLength	: 4,
						name		: 'registration',
						required	: true
					},{
						bind		: '{_itemName_.vat}',
						emptyText	: 'номер свидетельства плательщика НДС',
						fieldLabel	: 'Свид. НДС',
						maxLength	: 8,
						minLength	: 6,
						name		: 'vat'
					},{
						bind		: '{_itemName_.tax}',
						emptyText	: 'индивидуальный налоговый номер',
						fieldLabel	: 'ИНН',
//						maxLength	: 12,
//						minLength	: 11,
						name		: 'tax'
					}]
				}]
			},
			main			: {
				name	: 'main',
				order	: 2,
				title	: 'Основная информация',
				items	: [{
					defaults	: { flex: 1, margin: '0 0 0 5' },
					items		: [{ 
						bind		: '{_itemName_.name_en}',
						emptyText	: 'input short name',
						fieldLabel	: 'Name in English',
						margin		: 0,
				        maskRe		: /[0-9a-z\s.,\-"']/i,
						name		: 'name_en',
						required	: true
					},{
						bind		: '{_itemName_.name_uk}',
						emptyText	: 'введіть коротку назву',
						fieldLabel	: 'Name in Ukrainian',
				        maskRe		: /[0-9абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\s.,\-"']/i,
						name		: 'name_uk'
					},{
						bind		: '{_itemName_.name_ru}',
						emptyText	: 'введите краткое название',
						fieldLabel	: 'Name in Russian',
				        maskRe		: /[0-9а-я-ё\s.,\-"']/i,
						name		: 'name_ru'
					}]
				},{
					xtype		: 'textfield',
					bind		: '{_itemName_.fullName}',
					emptyText	: 'введите полное название компании',
					fieldLabel	: 'Полное',
					name		: 'fullName',
					required	: true
				},{
					items	: [{
						xtype				: 'combobox',
						afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Необходимо указать тип заказчика и/или поставщика">*</span>'],
						bind				: { value: '{_itemName_.customerType}' },
						displayField		: 'name',
						editable			: false,
						emptyText			: 'выберите тип заказчика',
			            fieldLabel			: 'Тип заказчика',
						flex				: 1,
				        labelStyle			: 'font-weight:bold',
						name				: 'customerType',
						publishes			: 'value',
						queryMode			: 'local',
						store				: { type: 'companycustomertypes' },
						valueField			: 'id',
						validator			: function (val) {
							if (!val || val === '...') {
								var record = this.lookupViewModel().getItem();
								if (record && (record.get('supplierType') || '...') === '...') return 'Необходимо указать тип заказчика и/или поставщика';
							}
							return true;
						}
					},{
						xtype				: 'combobox',
						afterLabelTextTpl	: ['<span style="color:red;font-weight:bold" data-qtip="Необходимо указать тип заказчика и/или поставщика">*</span>'],
						bind				: { value: '{_itemName_.supplierType}' },
						displayField		: 'name',
						editable			: false,
						emptyText			: 'выберите тип поставщика',
			            fieldLabel			: 'Тип поставщика',
						flex				: 1,
				        labelStyle			: 'font-weight:bold',
						margin				: '0 0 0 5',
						name				: 'supplierType',
						publishes			: 'value',
						queryMode			: 'local',
						store				: { type: 'suppliertypes' },
						valueField			: 'id',
						validator			: function (val) {
							if (!val || val === '...') {
								var record = this.lookupViewModel().getItem();
								if (record && (record.get('customerType') || '...') === '...') return 'Необходимо указать тип заказчика и/или поставщика';
							}
							return true;
						}
					}]
				},{
					xtype			: 'dictionarycombo',
					bind			: { value: '{_itemName_.parent}' },
					emptyText		: 'выберите родительскую компанию',
		            fieldLabel		: 'Подчиняется',
					flex			: 1,
					name			: 'parent',
					pageSize		: 25,
					publishes		: 'value',
					selectOnFocus	: true,
					store			: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'company' } }
					}
				}]
			},
			memorableDates	: {
				xtype		: 'admin-grid-memorable-dates-panel',
				bind		: '{memorableDates}',
				bodyPadding	: 0,
				layout		: 'fit',
				name		: 'memorableDates',
				order		: 8,
				scrollable	: null
			}
		}
	},
	title		: 'Редактирование компании',
	viewModel	: {	type: 'admin.company-edit' }

});
