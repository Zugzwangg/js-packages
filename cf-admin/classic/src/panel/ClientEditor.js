Ext.define('CFJS.admin.panel.ClientEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admnin-panel-client-editor',
	mixins			: [ 'CFJS.mixin.Sectionable' ],
	requires		: [ 'CFJS.form.field.PhoneField' ],
	actions			: {
		back		: { tooltip: 'Back to list of customers' },
		browseFiles	: {
			handler	: 'onBrowseFiles',
			iconCls	: CFJS.UI.iconCls.FILES,
			title	: 'Browse files',
			tooltip	: 'View additional files'
		}
	},
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord', 'browseFiles' ] } },
	bodyPadding		: '0 10 0 10',
	defaults		: {
		bodyPadding	: '0 10 0 10',
		collapsible	: true,
		defaults	: { anchor: '100%', defaultType: 'textfield', layout: 'hbox' },
		defaultType	: 'fieldcontainer',
		frame		: true,
		layout		: 'anchor',
		margin		: '0 0 10 0'
	},
	defaultType 	: 'form',
	fieldDefaults	: {
		labelAlign	: 'top',
		labelWidth	: 100,
		msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
	},
	header			: { items: new Array(5) },
	layout			: { type: 'anchor', reserveScrollbar: true },
	modelValidation	: true,
	scrollable		: 'y',

	afterRender: function() {
		var me = this, viewModel = me.lookupViewModel();
		me.callParent();
		if (viewModel) viewModel.bind('{_itemName_.id}', this.onChangeClient, this);
	},

	onChangeClient: function(id) {
		var me = this, hasId = id > 0;
		me.disableComponent(me.actions.browseFiles, !hasId);
		Ext.Array.each(Ext.Array.from(me.query('grid-base-panel')), function(cmp) {
			cmp.setReadOnly(!hasId);
		});
	}
    
});
