Ext.define('CFJS.admin.panel.RegionEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-region-editor',
	requires		: [ 'CFJS.admin.view.RegionModel' ],
	actions			: { back: { tooltip: 'Back to list of regions' } },
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		defaults: { flex: 1 },
		items	: [{
			bind		: '{_itemName_.name_en}',
			emptyText	: 'name in English',
			fieldLabel	: 'Name in English',
			margin		: 0,
	        maskRe		: /[0-9a-z\s.,\-"]/i,
			name		: 'name_en',
			required	: true
		},{
			bind		: '{_itemName_.name_uk}',
			emptyText	: 'name in Ukrainian',
			fieldLabel	: 'Name in Ukrainian',
	        maskRe		: /[0-9абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\s.,\-"']/i,
			name		: 'name_uk'
		},{
			bind		: '{_itemName_.name_ru}',
			emptyText	: 'name in Russian',
			fieldLabel	: 'Name in  Russian',
	        maskRe		: /[0-9а-я-ё\s.,\-"]/i,
			name		: 'name_ru'
		}]
	},{
		defaults: { flex: 1 },
		items	: [{
			xtype		: 'dictionarycombo',
			bind		: '{_itemName_.country}',
			emptyText	: 'select a country',
			fieldLabel	: 'Country',
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			margin		: 0,
			name		: 'country',
			publishes	: 'value',
			required	: true,
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'country' } },
				sorters	: [{ property: 'name'}]
			}
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			autoStripChars	: true,
			bind			: '{_itemName_.population}',
			emptyText		: 'population',
			fieldLabel		: 'population',
			minValue		: 0,
			name			: 'population'
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a region',
	viewModel		: {	type: 'admin.region-edit' }

});