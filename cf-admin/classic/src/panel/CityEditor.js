Ext.define('CFJS.admin.panel.CityEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-city-editor',
	requires		: [ 'CFJS.admin.view.CityModel' ],
	actions			: { back: { tooltip: 'Back to list of cities' } },
	actionsMap		: { header: { items: [ 'back', 'tbspacer', 'refreshRecord', 'saveRecord' ] } },
	bodyPadding		: '10',
	defaults		: { anchor: '100%', defaultType: 'textfield', layout: 'hbox', margin: '0 5' },
	defaultType 	: 'fieldcontainer',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		margin			: '0 0 0 5',
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: { items: new Array(4) },
	items			: [{
		defaults: { flex: 1 },
		items	: [{
			bind		: '{_itemName_.name_en}',
			emptyText	: 'name in English',
			fieldLabel	: 'Name in English',
			margin		: 0,
	        maskRe		: /[a-z\s.,\-"]/i,
			name		: 'name_en',
			required	: true
		},{
			bind		: '{_itemName_.name_uk}',
			emptyText	: 'name in Ukrainian',
			fieldLabel	: 'Name in Ukrainian',
	        maskRe		: /[абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\s.,\-"']/i,
			name		: 'name_uk'
		},{
			bind		: '{_itemName_.name_ru}',
			emptyText	: 'name in Russian',
			fieldLabel	: 'Name in Russian',
	        maskRe		: /[а-я-ё\s.,\-"]/i,
			name		: 'name_ru'
		}]
	},{
		defaults	: {
			flex		: 1,
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			publishes	: 'value'
		},
		defaultType	: 'dictionarycombo',
		items		: [{
			bind		: '{_itemName_.country}',
			emptyText	: 'select a country',
			fieldLabel	: 'Country',
			listeners	: {
				change: function(field, value) {
					var me = field, combo = me.ownerCt.down('dictionarycombo[name=region]'), store = combo && combo.getStore();
					if (store && store.isStore) {
						if (value) store.addFilter({ id: 'country', property: 'country.id', type: 'numeric', operator: 'eq', value: value.id });
						else store.removeFilter({ id: 'country' });
					}
				}
			},
			margin		: 0,
			name		: 'country',
			required	: true,
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'country' } },
				sorters	: [{ property: 'name'}]
			}
		},{
			bind		: '{_itemName_.region}',
			emptyText	: 'select a region',
			fieldLabel	: 'Region',
			name		: 'region',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'region' }, service: 'iss' },
				sorters	: [{ property: 'name'}]
			}
		}]
	},{
		defaults	: { autoStripChars: true, flex: 1, minValue: 0 },
		defaultType	: 'numberfield',
		items		: [{
			xtype		: 'textfield',
			bind		: '{_itemName_.iataCode}',
			emptyText	: 'IATA code',
			fieldLabel	: 'IATA code',
			margin		: 0,
			maskRe		: /[a-z0-9]/i,
			maxLength	: 3,
			name		: 'iataCode'
		},{
			bind		: '{_itemName_.latitude}',
			emptyText	: 'latitude',
			fieldLabel	: 'Latitude',
			name		: 'latitude'
		},{
			bind		: '{_itemName_.longitude}',
			emptyText	: 'longitude',
			fieldLabel	: 'Longitude',
			name		: 'longitude'
		},{
			allowDecimals	: false,
			bind			: '{_itemName_.population}',
			emptyText		: 'population',
			fieldLabel		: 'Population',
			name			: 'population'
		}]
	}],
	layout			: 'anchor',
	modelValidation	: true,
	scrollable		: 'y',
	title			: 'Editing a city',
	viewModel		: {	type: 'admin.city-edit' }

});