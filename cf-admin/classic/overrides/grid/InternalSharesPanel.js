Ext.define('CFJS.admin.overrides.grid.InternalSharesPanel', {
	override: 'CFJS.grid.InternalSharesPanel',
	requires: [ 'CFJS.admin.form.field.CustomerPickerWindow', 'CFJS.admin.form.field.PersonPickerWindow' ]
});
