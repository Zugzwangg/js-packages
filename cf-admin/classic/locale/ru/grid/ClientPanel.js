Ext.define('CFJS.admin.locale.ru.grid.ClientPanel', {
    override: 'CFJS.admin.grid.ClientPanel',
    config	: {
    	actions: { browseFiles: { title: 'Просмотр файлов', tooltip: 'Просмотр дополнительных файлов' } }
    }
});
