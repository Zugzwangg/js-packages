Ext.define('CFJS.admin.locale.ru.grid.CityPanel', {
	override: 'CFJS.admin.grid.CityPanel',
	config	: { title: 'Города' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите страну' }, text: 'Страна' },
			{ filter: { emptyText: 'укажите регион' }, text: 'Регион' },
			{ filter: { emptyText: 'укажите название города' }, text: 'Название' },
			{ filter: { emptyText: 'укажите код IATA' }, text: 'Код IATA' },
			{ filter: { emptyText: 'укажите подразделение' }, text: 'Подразделение' },
			{ filter: { emptyText: 'укажите широту' }, text: 'Широта' },
			{ filter: { emptyText: 'укажите долготу' }, text: 'Долгота' },
			{ filter: { emptyText: 'численность населения' }, text: 'Население' }
		]);
	}
});
