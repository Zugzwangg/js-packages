Ext.define('CFJS.admin.locale.ru.grid.CustomerPanel', {
    override	: 'CFJS.admin.grid.CustomerPanel',
	config		: { title: 'Физические лица' },
	datatipTpl	: '<b>Идентификатор:</b> {id}</br><b>Автор:</b> {author}</br><b>Создана:</b> {startDate:date("d.m.Y")}', 
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'имя' }, text: 'Имя' },
			{ filter: { emptyText: 'отчество' }, text: 'Отчество' },
			{ filter: { emptyText: 'фамилия' }, text: 'Фамилия' },
			{ text: 'День рождения' },
			{ filter: { emptyText: 'название компании' }, text: 'Компания' },
			{ filter: { emptyText: 'подразделение' }, text: 'Подразделение' },
			{ filter: { emptyText: 'номер телефона' }, text: 'Телефон' },
			{ text: 'Почта' },
			{ text: 'Тип заказчика' }
		]);
	}
});
