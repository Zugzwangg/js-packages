Ext.define('CFJS.admin.locale.ru.grid.CompanyAccountPanel', {
	override: 'CFJS.admin.grid.CompanyAccountPanel',
	config	: {
		actions	: { saveRecords: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' } },
	    title	: 'Банковские счета'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите банк' }, text: 'Банк', tpl: '<tpl if="bank">{bank.name} (МФО {bank.code})</tpl>' },
			{ editor: { emptyText: 'номер р/с' }, filter: { emptyText: 'номер р/с' }, text: 'Р/с' },
			{ editor: { emptyText: 'название счета' }, filter: { emptyText: 'название счета' }, text: 'Название' }
		]);
	}
});