Ext.define('CFJS.admin.locale.ru.grid.HotelPanel', {
	override: 'CFJS.admin.grid.HotelPanel',
	config	: {
    	actions	: { browseFiles: { title: 'Просмотр файлов', tooltip: 'Просмотр дополнительных файлов' } },
		title	: 'Отели'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите название отеля' }, text: 'Название' },
			{ filter: { emptyText: 'укажите город' }, text: 'Город' },
			{ filter: { emptyText: 'укажите класс' }, text: 'Класс' },
			{ filter: { emptyText: 'укажите рейтинг' }, text: 'Рейтинг' },
			{ filter: { emptyText: 'укажите широту' }, text: 'Широта' },
			{ filter: { emptyText: 'укажите долготу' }, text: 'Долгота' }
		]);
	}
});
