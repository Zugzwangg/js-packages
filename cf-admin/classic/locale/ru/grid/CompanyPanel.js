Ext.define('CFJS.admin.locale.ru.grid.CompanyPanel', {
	override	: 'CFJS.admin.grid.CompanyPanel',
	config		: { title: 'Компании' },
	datatipTpl	: '<b>Идентификатор:</b> {id}</br><b>Автор:</b> {author}</br><b>Создана:</b> {startDate:date("d.m.Y")}', 
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'регистрационный номер' }, text: 'Рег. номер' },
			{ filter: { emptyText: 'название страны' }, text: 'Страна' },
			{ filter: { emptyText: 'название' }, text: 'Название' },
			{ filter: { emptyText: 'название родительской компании' }, text: 'Подчиняется' },
			{ filter: { emptyText: 'подразделение' }, text: 'Подразделение' },
			{ filter: { emptyText: 'телефон' }, text: 'Телефон' },
			{ text: 'Пошта' },
			{ text: 'Тип замовника' },
			{ text: 'Тип поставщика' }
		]);
	}
});
