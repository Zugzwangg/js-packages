Ext.define('CFJS.admin.locale.ru.grid.PersonPanel', {
	override: 'CFJS.admin.grid.PersonPanel',
	config	: { title: 'Работники компании' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'ФИО' }, text: 'ФИО' },
			{ filter: { emptyText: 'должность' }, text: 'Должность' },
			{ filter: { emptyText: 'имя пользователя' }, text: 'Имя пользователя' },
			{ filter: { emptyText: 'номер телефона' }, text: 'Телефон' },
			{ text: 'Почта' },
			{ falseText: 'Нет', text: 'Активный', trueText: 'Да' },
			{ text: 'Последний вход' }
		]);
	}
});
