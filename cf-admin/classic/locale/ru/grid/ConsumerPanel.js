Ext.define('CFJS.admin.locale.ru.grid.ConsumerPanel', {
    override: 'CFJS.admin.grid.ConsumerPanel',
	config	: { title: 'Потребители' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'обращение' }, text: 'Обращение' },
			{ filter: { emptyText: 'имя' }, text: 'Имя' },
			{ filter: { emptyText: 'отчество' }, text: 'Отчество' },
			{ filter: { emptyText: 'фамилия' }, text: 'Фамилия' },
			{ text: 'День рождения' },
			{ filter: { emptyText: 'номер телефона' }, text: 'Телефон' },
			{ text: 'Почта' }
		]);
	}
});
