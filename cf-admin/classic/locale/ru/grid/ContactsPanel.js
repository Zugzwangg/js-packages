Ext.define('CFJS.admin.locale.ru.grid.ContactsPanel', {
	override		: 'CFJS.admin.grid.ContactsPanel',
	config			: { title: 'Дополнительные контакты' },
	defContactName	: 'Название контакта',
	defContactValue	: 'Значение контакта',
	renderColumns	: function() {
		return CFJS.merge(this.callParent(arguments), [{ text: 'Название' },{ text: 'Контакт' }]);
	}
});