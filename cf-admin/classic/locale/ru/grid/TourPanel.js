Ext.define('CFJS.admin.locale.ru.grid.TourPanel', {
	override: 'CFJS.admin.grid.TourPanel',
	config	: { title: 'Туры' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите отель' }, text: 'Отель' },
			{ filter: { emptyText: 'укажите тип комнаты' }, text: 'Тип комнаты' },
			{ filter: { emptyText: 'укажите питание' }, text: 'Питание' },
			{ filter: { emptyText: 'укажите дата заезда' }, text: 'Дата заезда' },
			{ filter: { emptyText: 'укажите продолжительность' }, text: 'Продолжительность' },
			{ filter: { emptyText: 'укажите количество взрослых' }, text: 'Взрослые' },
			{ filter: { emptyText: 'укажите количество детей' }, text: 'Дети' },
			{ filter: { emptyText: 'укажите цену' }, text: 'Цена' }
		]);
	}
});
