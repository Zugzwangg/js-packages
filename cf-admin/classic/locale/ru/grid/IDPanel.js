Ext.define('CFJS.admin.locale.ru.grid.IDPanel', {
	override: 'CFJS.admin.grid.IDPanel',
	config	: {
		actions	: { saveRecords: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' } },
	    title	: 'Идентификационные документы',
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ editor: { emptyText: 'выберите владельца' }, filter: { emptyText: 'выберите владельца' }, text: 'Владелец' },
			{ editor: { emptyText: 'серия и номер' }, filter: { emptyText: 'серия и номер' }, text: 'Серия и номер' },
			{ editor: { emptyText: 'название страны' }, filter: { emptyText: 'название страны' }, text: 'Страна' },
			{ editor: { emptyText: 'тип документа' }, filter: { emptyText: 'тип документа' }, text: 'Тип' },
			{ text: 'Внутренний' },
			{ editor: { emptyText: 'дата выдачи' }, text: 'Дата выдачи' },
			{ editor: { emptyText: 'название органа выдачи' }, text: 'Кем выдан' },
			{ editor: { emptyText: 'срок действия' }, text: 'Заканчивается' },
			{ editor: { emptyText: 'описание' }, text: 'Описание' }
		]);
	}
});