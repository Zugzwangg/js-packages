Ext.define('CFJS.admin.locale.ru.grid.BalancePanel', {
	override: 'CFJS.admin.grid.BalancePanel',
	config	: {
		actions	: { saveRecords: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' } },
		title	: 'Виртуальные счета'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ editor: { emptyText: 'укажите валюту' }, filter: { emptyText: 'укажите валюту' }, text: 'Валюта' },
			{ editor: { emptyText: 'укажите сумму' }, filter: { emptyText: 'укажите сумму' }, text: 'Лимит' },
			{ editor: { emptyText: 'укажите дни для оплаты' }, filter: { emptyText: 'укажите дни для оплаты' }, text: 'Дни для оплаты' },
			{ text: 'Баланс' },
			{ text: 'Доступные средства' },
			{ text: 'Дата задолженности' }
		]);
	}
});
