Ext.define('CFJS.admin.locale.ru.grid.ExtAppPanel', {
	override: 'CFJS.admin.grid.ExtAppPanel',
	config	: { title: 'Внешние приложения' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите код' }, text: 'Код' },
			{ filter: { emptyText: 'укажите название приложения' }, text: 'Название' },
			{ text: 'Организация' },
			{ text: 'Подразделение' },
			{ text: 'Сертификат (С/Н)' },
			{ falseText: 'Нет', text: 'Активное', trueText: 'Да' }
		]);
	}
});
