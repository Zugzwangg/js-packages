Ext.define('CFJS.admin.locale.ru.grid.PostPanel', {
	override: 'CFJS.admin.grid.PostPanel',
	config	: { title: 'Должности компании (штатное расписание)' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'название должности' }, text: 'Название' },
			{ filter: { emptyText: 'название подразделения' }, text: 'Подразделение' },
			{ filter: { emptyText: 'имя руководителя' }, text: 'Руководитель' },
			{ filter: { emptyText: 'имя работника' }, text: 'Работник' },
			{ text: 'Уровень доступа' }
		]);
	}
});
