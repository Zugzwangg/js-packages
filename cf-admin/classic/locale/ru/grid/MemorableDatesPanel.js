Ext.define('CFJS.admin.locale.ru.grid.MemorableDatesPanel', {
	override		: 'CFJS.admin.grid.MemorableDatesPanel',
	config			: { title: 'Памятные даты' },
	defDescription	: 'Описание памятной даты',
	renderColumns	: function() {
		return CFJS.merge(this.callParent(arguments), [{ text: 'Дата' },{ text: 'Описание' }]);
	}
});