Ext.define('CFJS.admin.locale.ru.grid.CountryPanel', {
	override: 'CFJS.admin.grid.CountryPanel',
	config	: { title: 'Страны' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите название страны' }, text: 'Название' },
			{ filter: { emptyText: 'укажите телефонный код' }, text: 'Телефонный код' },
			{ filter: { emptyText: 'укажите код IATA' }, text: 'Код IATA' },
			{ filter: { emptyText: 'численность населения' }, text: 'Население' }
		]);
	}
});
