Ext.define('CFJS.admin.locale.ru.grid.BankPanel', {
	override: 'CFJS.admin.grid.BankPanel',
	config	: { title: 'Банки' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ editor: { emptyText: 'укажите МФО' }, filter: { emptyText: 'укажите МФО' }, text: 'МФО' },
			{ editor: { emptyText: 'укажите название банка' }, filter: { emptyText: 'укажите название банка' }, text: 'Название' }
		]);
	}
});
