Ext.define('CFJS.admin.locale.ru.grid.RegionPanel', {
	override: 'CFJS.admin.grid.RegionPanel',
	config	: { title: 'Регионы' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'укажите страну' }, text: 'Страна' },
			{ filter: { emptyText: 'укажите название региона' }, text: 'Название' },
			{ filter: { emptyText: 'численность населения' }, text: 'Население' }
		]);
	}
});
