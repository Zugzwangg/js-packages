Ext.define('CFJS.admin.locale.ru.grid.SecretaryPanel', {
	override	: 'CFJS.admin.grid.SecretaryPanel',
	config		: { title: 'Секретари' },
	endDateText	: 'Заканчивается',
	nameText	: 'Пользователь',
	valueText	: 'Начинается'
});