Ext.define('CFJS.admin.locale.ru.panel.HotelEditor', {
	override	: 'CFJS.admin.panel.HotelEditor',
	config		: {
		actions	: {
			back		: { tooltip: 'Назад к списку отелей' },
    		browseFiles	: { title: 'Просмотр файлов', tooltip: 'Просмотр дополнительных файлов' }
		},
		title	: 'Редактирование отеля'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'название на английском', fieldLabel: 'Название на английском' },
			{ emptyText: 'название на украинском', fieldLabel: 'Название на украинском' },
			{ emptyText: 'название на русском', fieldLabel: 'Название на русском' }
		]
	},{
		items:[
			{ emptyText: 'выберите город', fieldLabel: 'Город' },
			{ emptyText: 'выберите класс', fieldLabel: 'Класс' },
			{ emptyText: 'выберите рейтинг', fieldLabel: 'Рейтинг' }
		]
	},{
		items: [
			{ emptyText: 'укажите широту', fieldLabel: 'Широта' },
			{ emptyText: 'укажите долготу', fieldLabel: 'Долгота' }
		]
	},
	{ emptyText: 'расположение', fieldLabel: 'Расположение' },
	{ emptyText: 'короткое описание', fieldLabel: 'Короткое описание' },
	{ emptyText: 'полное описание', fieldLabel: 'Полное описание' },
	{ emptyText: 'пляж', fieldLabel: 'Пляж' },
	{ emptyText: 'номер', fieldLabel: 'Номер' },
	{ emptyText: 'типы номеров', fieldLabel: 'Типы номеров' },
	{ emptyText: 'питание', fieldLabel: 'Питание' },
	{ emptyText: 'развлечения и спорт', fieldLabel: 'Развлечения и спорт' },
	{ emptyText: 'для детей', fieldLabel: 'Для детей' },
	{ emptyText: 'территория', fieldLabel: 'Территория' },
	{
		items: [
			{ emptyText: 'сайт', fieldLabel: 'Сайт' },
			{ emptyText: 'телефон', fieldLabel: 'Телефон' },
			{ emptyText: 'факс', fieldLabel: 'Факс' }
		]
	},
	{ emptyText: 'малыши от 8 месяцев до 3 лет', fieldLabel: 'Малыши от 8 месяцев до 3 лет' },
	{ fieldLabel: 'Главное изображение' }]
});
