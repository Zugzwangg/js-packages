Ext.define('CFJS.admin.locale.ru.panel.PostEditor', {
	override	: 'CFJS.admin.panel.PostEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку должностей' } },
		title	: 'Редактирование должности компании'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'название должности', fieldLabel: 'Название' },
			{ emptyText: 'выберите роль', fieldLabel: 'Роль' },
			{ emptyText: 'выберите подразделение', fieldLabel: 'Подразделение' }
		]
	},{
		items: [
			{ emptyText: 'выберите работника', fieldLabel: 'Работник' },
			{ emptyText: 'выберите должность', fieldLabel: 'Руководитель' },
			{ emptyText: 'выберите уровень доступа', fieldLabel: 'Уровень доступа' },
		]
	},{
		fieldLabel: 'Сектора'
	}]
});
