Ext.define('CFJS.admin.locale.ru.panel.RegionEditor', {
	override	: 'CFJS.admin.panel.RegionEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку регионов' } },
		title	: 'Редактирование региона'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'название на английском', fieldLabel: 'Название на английском' },
			{ emptyText: 'название на украинском', fieldLabel: 'Название на украинском' },
			{ emptyText: 'название на русском', fieldLabel: 'Название на русском' }
		]
	},{
		items:[
			{ emptyText: 'выберите страну', fieldLabel: 'Страна' },
			{ emptyText: 'укажите размер населения', fieldLabel: 'Население' }
		]
	}]
});
