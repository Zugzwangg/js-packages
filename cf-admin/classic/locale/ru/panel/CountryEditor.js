Ext.define('CFJS.admin.locale.ru.panel.CountryEditor', {
	override	: 'CFJS.admin.panel.CountryEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку стран' } },
		title	: 'Редактирование страны'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'название на английском', fieldLabel: 'Название на английском' },
			{ emptyText: 'название на украинском', fieldLabel: 'Название на украинском' },
			{ emptyText: 'название на русском', fieldLabel: 'Название на русском' }
		]
	},{
		items:[
			{ emptyText: 'укажите телефонный код', fieldLabel: 'Телефонный код' },
			{ emptyText: 'укажите код IATA', fieldLabel: 'Код IATA' },
			{ emptyText: 'укажите код ЖД', fieldLabel: 'Код ЖД' },
			{ emptyText: 'укажите размер населения', fieldLabel: 'Население' }
		]
	}]
});
