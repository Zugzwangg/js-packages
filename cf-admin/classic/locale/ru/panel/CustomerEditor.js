Ext.define('CFJS.admin.locale.ru.panel.CustomerEditor', {
	override	: 'CFJS.admin.panel.CustomerEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку физ. лиц' } },
		title	: 'Редактирование физ. лица'
	},
	initComponent: function() {
		var me = this,
			sections = {
				additional: {
					title: 'Специальная информация',
					items: [{
						items: [{ emptyText: 'выберите тип заказчика', fieldLabel: 'Тип заказчика' },{ emptyText: 'выберите подразделение', fieldLabel: 'Подразделение' }]
					},{
						fieldLabel: 'Примечания'
					}]
				},
				address: {
					title: 'Почтовый адрес',
					items: [{
						items: [
							{ emptyText: 'выберите страну', fieldLabel: 'Страна' },
							{ emptyText: 'индекс', fieldLabel: 'Индекс' },
							{ emptyText: 'укажите область', fieldLabel: 'Область' },
							{ emptyText: 'выберите город', fieldLabel: 'Город' }
						]
					},{
						items: [
							{ emptyText: 'укажите улицу', fieldLabel: 'Улица' },
							{ emptyText: 'номер', fieldLabel: 'Дом' },
							{ emptyText: 'номер', fieldLabel: 'Помещение' }
						]
					}]
				},
				author: {
					items: [{ fieldLabel: 'Идентификатор' },{ fieldLabel: 'Автор записи' },{ fieldLabel: 'Создана' }]
				},
				contact: {
					title: 'Контактная информация',
					items: [{
						items: [
							{ emptyText: 'укажите пол', fieldLabel: 'Пол' },
							{ emptyText: 'день рождения', fieldLabel: 'День рождения' },
							{ emptyText: 'индивидуальный налоговый номер', fieldLabel: 'ИНН' }
						]
					},{
						items: [
							{ fieldLabel: 'Телефон' },
							{ emptyText: 'введите электронную почту', fieldLabel: 'Электронная почта' },
							{ fieldLabel: 'Получает новости' }
						]
					}]
				},
				fullName: {
					title: 'ФИО',
					items: [
						{ fieldLabel: 'Английский', items: [{ emptyText: 'Фамилия' },{ emptyText: 'Имя' },{ emptyText: 'Отчество' }] },
						{ fieldLabel: 'Украинский', items: [{ emptyText: 'Фамилия' },{ emptyText: 'Имя' },{ emptyText: 'Отчество' }] },
						{ fieldLabel: 'Русский', items: [{ emptyText: 'Фамилия' },{ emptyText: 'Имя' },{ emptyText: 'Отчество' }] }
					]
				},
				workPosition: {
					title: 'Компания',
					items: [{
						items:[
							{ emptyText: 'выберите компанию', fieldLabel: 'Компания' },
							{ emptyText: 'укажите тип должности', fieldLabel: 'Тип должности' },
							{ emptyText: 'укажите название должности', fieldLabel: 'Название должности' }
						]
					},{
						items: [
							{ fieldLabel: 'Контактное лицо' },
							{ fieldLabel: 'Получает новости' },
							{ emptyText: 'введите электронную почту', fieldLabel: 'Рабочая электронная почта' }
						]
					},{
						emptyText: 'укажите основание действий должности',
						fieldLabel: 'Действует на основании'
					}]
				}
			}, key;
		for (key in sections) {
			if (me.sections[key]) CFJS.merge(me.sections[key], sections[key]);
		}
		me.callParent();
	}
});
