Ext.define('CFJS.admin.locale.ru.panel.PersonEditor', {
	override	: 'CFJS.admin.panel.PersonEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку работников' } },
		title	: 'Редактирование работника компании'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'ФИО', fieldLabel: 'ФИО' },
			{ emptyText: 'должность', fieldLabel: 'Должность' },
			{ emptyText: 'имя пользователя', fieldLabel: 'Имя пользователя' }
		]
	},{
		items: [
			{ fieldLabel: 'Телефон' },
			{ emptyText: 'введите электронную почту', fieldLabel: 'Электронная почта' },
		]
	},{
		fieldLabel	: 'Фото',
		items		: [
			{},
			{ boxLabel: 'Активный' },
			{ boxLabel: 'Запросить новый пароль' }
		]
	}]
});
