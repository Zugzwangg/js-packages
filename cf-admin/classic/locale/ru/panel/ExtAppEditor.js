Ext.define('CFJS.admin.locale.ru.panel.ExtAppEditor', {
	override	: 'CFJS.admin.panel.ExtAppEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку приложений' } },
		title	: 'Редактирование приложения'
	},
	itemsConfig	: [
		{ emptyText: 'название приложения', fieldLabel: 'Название приложения' },
		{ emptyText: 'название организации', fieldLabel: 'Организация' },
		{ emptyText: 'название подразделения', fieldLabel: 'Подразделение' },
		{ fieldLabel: 'Активное' },
		{ fieldLabel: 'Сбросить ключи' },
		{ fieldLabel: 'Сертификат (С/Н)' }
	]
});
