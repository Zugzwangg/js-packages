Ext.define('CFJS.admin.locale.ru.panel.ConsumerEditor', {
	override	: 'CFJS.admin.panel.ConsumerEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку потребителей' } },
		title	: 'Редактирование потребителя'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'обращение', fieldLabel: 'Обращение' },
			{ emptyText: 'имя', fieldLabel: 'Имя' },
			{ emptyText: 'отчество', fieldLabel: 'Отчество' },
			{ emptyText: 'фамилия', fieldLabel: 'Фамилия' }
		]
	},{
		items:[
			{ emptyText: 'день рождения', fieldLabel: 'День рождения' },
			{ emptyText: 'индивидуальный налоговый номер', fieldLabel: 'ИНН' },
			{ emptyText: 'документ', fieldLabel: 'Документ' }
		]
	},{
		items: [
			{ fieldLabel: 'Телефон' },
			{ emptyText: 'введите электронную почту', fieldLabel: 'Электронная почта' }
		]
	},{
		items: [
			{ emptyText: 'выберите страну', fieldLabel: 'Страна' },
			{ emptyText: 'индекс', fieldLabel: 'Индекс' },
			{ emptyText: 'укажите область', fieldLabel: 'Область' },
			{ emptyText: 'выберите город', fieldLabel: 'Город' }
		]
	},{
		items: [
			{ emptyText: 'укажите улицу', fieldLabel: 'Улица' },
			{ emptyText: 'номер', fieldLabel: 'Дом' },
			{ emptyText: 'номер', fieldLabel: 'Помещение' }
		]
	}]
});
