Ext.define('CFJS.admin.locale.ru.panel.CityEditor', {
	override	: 'CFJS.admin.panel.CityEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку городов' } },
		title	: 'Редактирование города'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'название на английском', fieldLabel: 'Название на английском' },
			{ emptyText: 'название на украинском', fieldLabel: 'Название на украинском' },
			{ emptyText: 'название на русском', fieldLabel: 'Название на русском' }
		]
	},{
		items:[
			{ emptyText: 'выберите страну', fieldLabel: 'Страна' },
			{ emptyText: 'выберите регион', fieldLabel: 'Регион' }
		]
	},{
		items: [
			{ emptyText: 'укажите код IATA', fieldLabel: 'Код IATA' },
			{ emptyText: 'укажите широту', fieldLabel: 'Широта' },
			{ emptyText: 'укажите долготу', fieldLabel: 'Долгота' },
			{ emptyText: 'укажите размер населения', fieldLabel: 'Население' }
		]
	}]
});
