Ext.define('CFJS.admin.locale.ru.panel.TourEditor', {
	override	: 'CFJS.admin.panel.TourEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад к списку туров' } },
		title	: 'Редактирование тура'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'отель', fieldLabel: 'Отель' },
			{ emptyText: 'тип комнаты', fieldLabel: 'Тип комнаты' },
			{ emptyText: 'питание', fieldLabel: 'Питание' }
		]
	},{
		items:[
			{ emptyText: 'дата заезда', fieldLabel: 'Дата заезда' },
			{ emptyText: 'продолжительность', fieldLabel: 'Продолжительность' }
		]
	},{
		items:[
			{ emptyText: 'количество взрослых', fieldLabel: 'Количество взрослых' },
			{ emptyText: 'количество детей', fieldLabel: 'Количество детей' }
		]
	},{
		items:[
			{ emptyText: 'цена', fieldLabel: 'Цена' },
			{ emptyText: 'валюта', fieldLabel: 'Валюта' }
		]
	}]
});
