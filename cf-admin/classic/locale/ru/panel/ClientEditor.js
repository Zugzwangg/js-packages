Ext.define('CFJS.admin.locale.ru.panel.ClientEditor', {
	override: 'CFJS.admin.panel.ClientEditor',
    config	: {
		actions	: {
			back		: { tooltip: 'Назад к списку клиентов' },
    		browseFiles	: { title: 'Просмотр файлов', tooltip: 'Просмотр дополнительных файлов' }
		}
    }
});
