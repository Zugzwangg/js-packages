Ext.define('CFJS.admin.locale.ru.form.field.PostPickerWindow', {
	override: 'CFJS.admin.form.field.PostPickerWindow',
	config	: {
		actions: {
			addRecord	: { title: 'Добавить должность', tooltip: 'Добавить новую должность' },
			editRecord	: { title: 'Редaктировать должность', tooltip: 'Редaктировать текущую должность' },
			selectRecord: { title: 'Выбрать должность', tooltip: 'Выбрать текущую должность' }
		}
	}
});
