Ext.define('CFJS.admin.locale.ru.form.field.PersonPickerWindow', {
	override: 'CFJS.admin.form.field.PersonPickerWindow',
	config	: {
		actions: {
			addRecord	: { title: 'Добавить сотрудника', tooltip: 'Добавить нового сотрудника' },
			editRecord	: { title: 'Редaктировать сотрудника', tooltip: 'Редaктировать текущего сотрудника' },
			selectRecord: { title: 'Выбрать сотрудника', tooltip: 'Выбрать текущего сотрудника' }
		}
	}
});
