Ext.define('CFJS.admin.locale.ru.form.field.CustomerPickerWindow', {
    override: 'CFJS.admin.form.field.CustomerPickerWindow',
    config	: {
    	actions: {
    		addRecord	: { title: 'Добавить клиента', tooltip: 'Добавить нового клиента' },
    		editRecord	: { title: 'Редaктировать клиента', tooltip: 'Редaктировать текущего клиента' },
    		selectRecord: { title: 'Выбрать клиента', tooltip: 'Выбрать текущего клиента' }
    	}
    }
});
