Ext.define('CFJS.admin.locale.ru.form.field.ContractorPicker', {
	override: 'CFJS.admin.form.field.ContractorPicker',
	config	: {
		triggers: {
			clear	: { tooltip: 'Очистить' },
			company	: { tooltip: 'Компания' },
			customer: { tooltip: 'Физ.лицо' }
		}
	}
});
