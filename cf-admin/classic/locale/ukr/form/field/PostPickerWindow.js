Ext.define('CFJS.admin.locale.ukr.form.field.PostPickerWindow', {
	override: 'CFJS.admin.form.field.PostPickerWindow',
	config	: {
		actions: {
			addRecord	: { title: 'Додати посаду', tooltip: 'Додати нову посаду' },
			editRecord	: { title: 'Редагувати посаду', tooltip: 'Редагувати поточну посаду' },
			selectRecord: { title: 'Обрати посаду', tooltip: 'Обрати поточну посаду' }
		}
	}
});
