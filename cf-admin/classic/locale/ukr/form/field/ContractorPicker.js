Ext.define('CFJS.admin.locale.ukr.form.field.ContractorPicker', {
	override: 'CFJS.admin.form.field.ContractorPicker',
	config	: {
		triggers: {
			clear	: { tooltip: 'Очистити' },
			company	: { tooltip: 'Компанія' },
			customer: { tooltip: 'Фіз.особа' }
		}
	}
});
