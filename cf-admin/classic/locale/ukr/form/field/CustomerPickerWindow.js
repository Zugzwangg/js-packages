Ext.define('CFJS.admin.locale.ukr.form.field.CustomerPickerWindow', {
	override: 'CFJS.admin.form.field.CustomerPickerWindow',
	config	: {
		actions: {
			addRecord	: { title: 'Додати клієнта', tooltip: 'Додати нового клієнта' },
			editRecord	: { title: 'Редагувати клієнта', tooltip: 'Редагувати поточного клієнта' },
			selectRecord: { title: 'Обрати клієнта', tooltip: 'Обрати поточного клієнта' }
		}
	}
});
