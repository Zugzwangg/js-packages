Ext.define('CFJS.admin.locale.ukr.form.field.PersonPickerWindow', {
	override: 'CFJS.admin.form.field.PersonPickerWindow',
	config	: {
		actions: {
			addRecord	: { title: 'Додати співробітника', tooltip: 'Додати нового співробітника' },
			editRecord	: { title: 'Редагувати співробітника', tooltip: 'Редагувати поточного співробітника' },
			selectRecord: { title: 'Обрати співробітника', tooltip: 'Обрати поточного співробітника' }
		}
	}
});
