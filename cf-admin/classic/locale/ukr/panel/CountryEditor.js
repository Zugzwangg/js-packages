Ext.define('CFJS.admin.locale.ukr.panel.CountryEditor', {
	override	: 'CFJS.admin.panel.CountryEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку країн' } },
		title	: 'Редагування країни'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'назва англійською', fieldLabel: 'Назва англійською' },
			{ emptyText: 'назва українською', fieldLabel: 'Назва українською' },
			{ emptyText: 'назва російською', fieldLabel: 'Назва російською' }
		]
	},{
		items:[
			{ emptyText: 'вкажіть телефонний код', fieldLabel: 'Телефонний код' },
			{ emptyText: 'вкажіть код IATA', fieldLabel: 'Код IATA' },
			{ emptyText: 'вкажіть залізничний код', fieldLabel: 'Залізничний код' },
			{ emptyText: 'вкажіть розмір населення', fieldLabel: 'Населення' }
		]
	}]
});
