Ext.define('CFJS.admin.locale.ukr.panel.PersonEditor', {
	override	: 'CFJS.admin.panel.PersonEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку працівників' } },
		title	: 'Редагування працівника компанії'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'ПІБ', fieldLabel: 'ПІБ' },
			{ emptyText: 'посада', fieldLabel: 'Посада' },
			{ emptyText: 'ім\'я користувача', fieldLabel: 'Ім\'я користувача' }
		]
	},{
		items: [
			{ fieldLabel: 'Телефон' },
			{ emptyText: 'введіть електронну пошту', fieldLabel: 'Електронна пошта' }
		]
	},{
		fieldLabel	: 'Фото',
		items		: [
			{},
			{ boxLabel: 'Активний' },
			{ boxLabel: 'Запросити новий пароль' }
		]
	}]
});
