Ext.define('CFJS.admin.locale.ukr.panel.CustomerEditor', {
	override	: 'CFJS.admin.panel.CustomerEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку фіз. осіб' } },
		title	: 'Редагування фіз. особи'
	},
	initComponent: function() {
		var me = this,
			sections = {
				additional: {
					title: 'Cпеціальна інформація',
					items: [{
						items: [{ emptyText: 'оберіть тип замовника', fieldLabel: 'Тип замовника' },{ emptyText: 'оберіть підрозділ', fieldLabel: 'Підрозділ' }]
					},{
						fieldLabel: 'Примітки'
					}]
				},
				address: {
					title: 'Поштова адреса',
					items: [{
						items: [
							{ emptyText: 'виберіть країну', fieldLabel: 'Країна' },
							{ emptyText: 'індекс', fieldLabel: 'Індекс' },
							{ emptyText: 'зазначте область', fieldLabel: 'Область' },
							{ emptyText: 'виберіть місто', fieldLabel: 'Місто' }
						]
					},{
						items: [
							{ emptyText: 'зазначте вулицю', fieldLabel: 'Вулиця' },
							{ emptyText: 'номер', fieldLabel: 'Будинок' },
							{ emptyText: 'номер', fieldLabel: 'Приміщення' }
						]
					}]
				},
				author: {
					items: [{ fieldLabel: 'Ідентифікатор' },{ fieldLabel: 'Автор запису' },{ fieldLabel: 'Створена' }]
				},
				contact: {
					title: 'Контактна інформація',
					items: [{
						items: [
							{ emptyText: 'зазначте стать', fieldLabel: 'Cтать' },
							{ emptyText: 'день народження', fieldLabel: 'День народження' },
							{ emptyText: 'індивідуальний податковий номер', fieldLabel: 'ІПН' }
						]
					},{
						items: [
							{ fieldLabel: 'Телефон' },
							{ emptyText: 'введіть електронну пошту', fieldLabel: 'Електронна пошта' },
							{ fieldLabel: 'Отримує новини' }
						]
					}]
				},
				fullName: {
					title: 'ПІБ',
					items: [
						{ fieldLabel: 'Англійською', items: [{ emptyText: 'Прізвище' },{ emptyText: 'Ім\'я' },{ emptyText: 'Побатькові' }] },
						{ fieldLabel: 'Українською', items: [{ emptyText: 'Прізвище' },{ emptyText: 'Ім\'я' },{ emptyText: 'Побатькові' }] },
						{ fieldLabel: 'Россійською', items: [{ emptyText: 'Прізвище' },{ emptyText: 'Ім\'я' },{ emptyText: 'Побатькові' }] }
					]
				},
				workPosition: {
					title: 'Компанія',
					items: [{
						items:[
							{ emptyText: 'оберіть компанію', fieldLabel: 'Компанія' },
							{ emptyText: 'зазначте тип посади', fieldLabel: 'Тип посади' },
							{ emptyText: 'зазначте назву посади', fieldLabel: 'Назва посади' }
						]
					},{
						items: [
							{ fieldLabel: 'Контактна особа' },
							{ fieldLabel: 'Отримує новини' },
							{ emptyText: 'введіть електронну пошту', fieldLabel: 'Робоча електронна пошта' }
						]
					},{
						emptyText: 'зазначте підставу дій посади',
						fieldLabel: 'Діє на підставі'
					}]
				}
			}, key;
		for (key in sections) {
			if (me.sections[key]) CFJS.merge(me.sections[key], sections[key]);
		}
		me.callParent();
	}
});
