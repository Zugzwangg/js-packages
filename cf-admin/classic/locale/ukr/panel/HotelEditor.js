Ext.define('CFJS.admin.locale.ukr.panel.HotelEditor', {
	override	: 'CFJS.admin.panel.HotelEditor',
	config		: {
		actions	: {
			back		: { tooltip: 'Назад до списку готелів' },
			browseFiles	: {	title: 'Перегляд файлів', tooltip: 'Перегляд додаткових файлів' }
		},
		title	: 'Редагування готелю'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'назва англійською', fieldLabel: 'Назва англійською' },
			{ emptyText: 'назва українською', fieldLabel: 'Назва українською' },
			{ emptyText: 'назва російською', fieldLabel: 'Назва російською' }
		]
	},{
		items:[
			{ emptyText: 'виберіть місто', fieldLabel: 'Місто' },
			{ emptyText: 'виберіть клас', fieldLabel: 'Клас' },
			{ emptyText: 'виберіть рейтинг', fieldLabel: 'Рейтинг' }
		]
	},{
		items: [
			{ emptyText: 'вкажіть широту', fieldLabel: 'Широта' },
			{ emptyText: 'вкажіть довготу', fieldLabel: 'Довгота' }
		]
	},
	{ emptyText: 'розташування', fieldLabel: 'Розташування' },
	{ emptyText: 'короткий опис', fieldLabel: 'Короткий опис' },
	{ emptyText: 'повний опис', fieldLabel: 'Повний опис' },
	{ emptyText: 'пляж', fieldLabel: 'Пляж' },
	{ emptyText: 'номер', fieldLabel: 'Номер' },
	{ emptyText: 'типи номерів', fieldLabel: 'Типи номерів' },
	{ emptyText: 'харчування', fieldLabel: 'Харчування' },
	{ emptyText: 'розваги і спорт', fieldLabel: 'Розваги і спорт' },
	{ emptyText: 'для дітей', fieldLabel: 'Для дітей' },
	{ emptyText: 'Територія', fieldLabel: 'Територія' },
	{
		items: [
			{ emptyText: 'сайт', fieldLabel: 'Сайт' },
			{ emptyText: 'телефон', fieldLabel: 'Телефон' },
			{ emptyText: 'факс', fieldLabel: 'Факс' }
		]
	},
	{ emptyText: 'малюки від 8 місяців до 3 років', fieldLabel: 'Малюки від 8 місяців до 3 років' },
	{ fieldLabel: 'Головне зображення' }]
});
