Ext.define('CFJS.admin.locale.ukr.panel.ExtAppEditor', {
	override	: 'CFJS.admin.panel.ExtAppEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку додатків' } },
		title	: 'Редагування додатку'
	},
	itemsConfig	: [
		{ emptyText: 'назва додатку', fieldLabel: 'Назва додатку' },
		{ emptyText: 'назва організації', fieldLabel: 'Організація' },
		{ emptyText: 'назва підрозділу', fieldLabel: 'Підрозділ' },
		{ fieldLabel: 'Активний' },
		{ fieldLabel: 'Скинути ключі' },
		{ fieldLabel: 'Сертификат (С/Н)' }
	]
});
