Ext.define('CFJS.admin.locale.ukr.panel.RegionEditor', {
	override	: 'CFJS.admin.panel.RegionEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку регіонів' } },
		title	: 'Редагування регіону'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'назва англійською', fieldLabel: 'Назва англійською' },
			{ emptyText: 'назва українською', fieldLabel: 'Назва українською' },
			{ emptyText: 'назва російською', fieldLabel: 'Назва російською' }
		]
	},{
		items:[
			{ emptyText: 'виберіть країну', fieldLabel: 'Країна' },
			{ emptyText: 'вкажіть розмір населення', fieldLabel: 'Населення' }
		]
	}]
});
