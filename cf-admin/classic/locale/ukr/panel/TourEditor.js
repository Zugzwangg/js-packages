Ext.define('CFJS.admin.locale.ukr.panel.TourEditor', {
	override	: 'CFJS.admin.panel.TourEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку турів' } },
		title	: 'Редагування туру'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'готель', fieldLabel: 'Готель' },
			{ emptyText: 'тип кімнати', fieldLabel: 'Тип кімнати' },
			{ emptyText: 'харчування', fieldLabel: 'Харчування' }
		]
	},{
		items:[
			{ emptyText: 'дата заїзду', fieldLabel: 'Дата заїзду' },
			{ emptyText: 'тривалість', fieldLabel: 'Тривалість' }
		]
	},{
		items:[
			{ emptyText: 'кількість дорослих', fieldLabel: 'Кількість дорослих' },
			{ emptyText: 'кількість дітей', fieldLabel: 'Кількість дітей' }
		]
	},{
		items:[
			{ emptyText: 'ціна', fieldLabel: 'Ціна' },
			{ emptyText: 'валюта', fieldLabel: 'Валюта' }
		]
	}]
});
