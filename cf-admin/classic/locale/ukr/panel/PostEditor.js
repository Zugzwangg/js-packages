Ext.define('CFJS.admin.locale.ukr.panel.PostEditor', {
	override	: 'CFJS.admin.panel.PostEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку посад' } },
		title	: 'Редагування посади компанії'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'назва посади', fieldLabel: 'Назва' },
			{ emptyText: 'оберіть роль', fieldLabel: 'Роль' },
			{ emptyText: 'оберіть підрозділ', fieldLabel: 'Підрозділ' }
		]
	},{
		items: [
			{ emptyText: 'оберіть працівника', fieldLabel: 'Працівник' },
			{ emptyText: 'оберіть посаду', fieldLabel: 'Керівник' },
			{ emptyText: 'оберіть pівень доступу', fieldLabel: 'Рівень доступу' },
		]
	},{
		fieldLabel: 'Сектори'
	}]
});
