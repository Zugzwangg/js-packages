Ext.define('CFJS.admin.locale.ukr.panel.ClientEditor', {
	override: 'CFJS.admin.panel.ClientEditor',
	config	: {
		actions	: {
			back		: { tooltip: 'Назад до списку клієнтів' },
			browseFiles	: {	title: 'Перегляд файлів', tooltip: 'Перегляд додаткових файлів' }
		}
	}
});
