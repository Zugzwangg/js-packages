Ext.define('CFJS.admin.locale.ukr.panel.CityEditor', {
	override	: 'CFJS.admin.panel.CityEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку міст' } },
		title	: 'Редагування міста'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'назва англійською', fieldLabel: 'Назва англійською' },
			{ emptyText: 'назва українською', fieldLabel: 'Назва українською' },
			{ emptyText: 'назва російською', fieldLabel: 'Назва російською' }
		]
	},{
		items:[
			{ emptyText: 'виберіть країну', fieldLabel: 'Країна' },
			{ emptyText: 'виберіть регіон', fieldLabel: 'Регіон' }
		]
	},{
		items: [
			{ emptyText: 'вкажіть код IATA', fieldLabel: 'Код IATA' },
			{ emptyText: 'вкажіть широту', fieldLabel: 'Широта' },
			{ emptyText: 'вкажіть довготу', fieldLabel: 'Довгота' },
			{ emptyText: 'вкажіть розмір населення', fieldLabel: 'Населення' }
		]
	}]
});
