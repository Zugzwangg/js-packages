Ext.define('CFJS.admin.locale.ukr.panel.ConsumerEditor', {
	override	: 'CFJS.admin.panel.ConsumerEditor',
	config		: {
		actions	: { back: { tooltip: 'Назад до списку споживачів' } },
		title	: 'Редагування споживача'
	},
	itemsConfig	: [{
		items: [
			{ emptyText: 'звернення', fieldLabel: 'Звернення' },
			{ emptyText: 'ім\'я', fieldLabel: 'Ім\'я' },
			{ emptyText: 'по батькові', fieldLabel: 'По батькові' },
			{ emptyText: 'прізвище', fieldLabel: 'Прізвище' }
		]
	},{
		items:[
			{ emptyText: 'день народження', fieldLabel: 'День народження' },
			{ emptyText: 'індивідуальний податковий номер', fieldLabel: 'ІПН' },
			{ emptyText: 'документ', fieldLabel: 'Документ' }
		]
	},{
		items: [
			{ fieldLabel: 'Телефон' },
			{ emptyText: 'введіть електронну пошту', fieldLabel: 'Електронна пошта' }
		]
	},{
		items: [
			{ emptyText: 'виберіть країну', fieldLabel: 'Країна' },
			{ emptyText: 'індекс', fieldLabel: 'Індекс' },
			{ emptyText: 'зазначте область', fieldLabel: 'Область' },
			{ emptyText: 'виберіть місто', fieldLabel: 'Місто' }
		]
	},{
		items: [
			{ emptyText: 'зазначте вулицю', fieldLabel: 'Вулиця' },
			{ emptyText: 'номер', fieldLabel: 'Будинок' },
			{ emptyText: 'номер', fieldLabel: 'Приміщення'}
		]
	}]
});
