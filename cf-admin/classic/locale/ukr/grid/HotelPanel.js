Ext.define('CFJS.admin.locale.ukr.grid.HotelPanel', {
	override: 'CFJS.admin.grid.HotelPanel',
	config	: {
		actions	: { browseFiles: { title: 'Перегляд файлів', tooltip: 'Перегляд додаткових файлів' } },
		title	: 'Готелі'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть назву отелю' }, text: 'Назва' },
			{ filter: { emptyText: 'вкажіть місто' }, text: 'Місто' },
			{ filter: { emptyText: 'вкажіть клас' }, text: 'Клас' },
			{ filter: { emptyText: 'вкажіть рейтинг' }, text: 'Рейтинг' },
			{ filter: { emptyText: 'вкажіть широту' }, text: 'Широта' },
			{ filter: { emptyText: 'вкажіть довготу' }, text: 'Довгота' }
		]);
	}
});
