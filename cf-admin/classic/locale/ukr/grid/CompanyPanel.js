Ext.define('CFJS.admin.locale.ukr.grid.CompanyPanel', {
	override	: 'CFJS.admin.grid.CompanyPanel',
	config		: { title: 'Компанії' },
	datatipTpl	: '<b>Ідентифікатор:</b> {id}</br><b>Автор:</b> {author}</br><b>Створена:</b> {startDate:date("d.m.Y")}', 
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'реєстраційний номер' }, text: 'Рег. номер' },
			{ filter: { emptyText: 'назва країни' }, text: 'Країна' },
			{ filter: { emptyText: 'назва' }, text: 'Назва' },
			{ filter: { emptyText: 'назва батьківської компанії' }, text: 'Підпорядковується' },
			{ filter: { emptyText: 'підрозділ' }, text: 'Підрозділ' },
			{ filter: { emptyText: 'телефон' }, text: 'Телефон' },
			{ text: 'Почта' },
			{ text: 'Тип замовника' },
			{ text: 'Тип постачальника' }
		]);
	}
});
