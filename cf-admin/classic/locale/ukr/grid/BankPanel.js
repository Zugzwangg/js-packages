Ext.define('CFJS.admin.locale.ukr.grid.BankPanel', {
	override: 'CFJS.admin.grid.BankPanel',
	config	: { title: 'Банки' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ editor: { emptyText: 'вкажіть МФО' }, filter: { emptyText: 'вкажіть МФО' }, text: 'МФО' },
			{ editor: { emptyText: 'вкажіть назву банку' }, filter: { emptyText: 'вкажіть назву банку' }, text: 'Назва' }
		]);
	}
});
