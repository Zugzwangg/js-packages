Ext.define('CFJS.admin.locale.ukr.grid.CityPanel', {
	override: 'CFJS.admin.grid.CityPanel',
	config	: { title: 'Міста' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть країну' }, text: 'Країна' },
			{ filter: { emptyText: 'вкажіть регіон' }, text: 'Регіон' },
			{ filter: { emptyText: 'вкажіть назву міста' }, text: 'Назва' },
			{ filter: { emptyText: 'вкажіть код IATA' }, text: 'Код IATA' },
			{ filter: { emptyText: 'вкажіть підрозділ' }, text: 'Підрозділ' },
			{ filter: { emptyText: 'вкажіть широту' }, text: 'Широта' },
			{ filter: { emptyText: 'вкажіть довготу' }, text: 'Довгота' },
			{ filter: { emptyText: 'чисельність населення' }, text: 'Населення' }
		]);
	}
});
