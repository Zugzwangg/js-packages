Ext.define('CFJS.admin.locale.ukr.grid.IDPanel', {
	override: 'CFJS.admin.grid.IDPanel',
	config	: {
		actions	: { saveRecords: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' } },
		title	: 'Ідентифікаційні документи'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ editor: { emptyText: 'виберіть власника' }, filter: { emptyText: 'виберіть власника' }, text: 'Власник' },
			{ editor: { emptyText: 'серія і номер' }, filter: { emptyText: 'серія і номер' }, text: 'Серія і номер' },
			{ editor: { emptyText: 'назва країни' }, filter: { emptyText: 'назва країни' }, text: 'Країна' },
			{ editor: { emptyText: 'тип документа' }, filter: { emptyText: 'тип документа' }, text: 'Тип' },
			{ text: 'Внутрішній' },
			{ editor: { emptyText: 'дата видачі' }, text: 'Дата видачі' },
			{ editor: { emptyText: 'назва органу видачі' }, text: 'Ким виданий' },
			{ editor: { emptyText: 'термін дії' }, text: 'Закінчується' },
			{ editor: { emptyText: 'опис' }, text: 'Опис' }
		]);
	}
});