Ext.define('CFJS.admin.locale.ukr.grid.BalancePanel', {
	override: 'CFJS.admin.grid.BalancePanel',
	config	: {
		actions	: { saveRecords: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' } },
		title	: 'Віртуальні рахунки'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ editor: { emptyText: 'вкажіть валюту' }, filter: { emptyText: 'вкажіть валюту' }, text: 'Валюта' },
			{ editor: { emptyText: 'вкажіть суму' }, filter: { emptyText: 'вкажіть суму' }, text: 'Ліміт' },
			{ editor: { emptyText: 'вкажіть дні для оплати' }, filter: { emptyText: 'вкажіть дні для оплати' }, text: 'Дні для оплати' },
			{ text: 'Баланс' },
			{ text: 'Доступний залишок' },
			{ text: 'Дата заборгованості' }
		]);
	}
});
