Ext.define('CFJS.admin.locale.ukr.grid.CustomerPanel', {
	override	: 'CFJS.admin.grid.CustomerPanel',
	config		: { title: 'Фізичні особи' },
	datatipTpl	: '<b>Ідентифікатор:</b> {id}</br><b>Автор:</b> {author}</br><b>Створена:</b> {startDate:date("d.m.Y")}', 
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'ім\'я' }, text: 'Ім\'я' },
			{ filter: { emptyText: 'по батькові' }, text: 'По батькові' },
			{ filter: { emptyText: 'прізвище' }, text: 'Прізвище' },
			{ text: 'День народження' },
			{ filter: { emptyText: 'назва компанії' }, text: 'Компанія' },
			{ filter: { emptyText: 'підрозділ' }, text: 'Підрозділ' },
			{ filter: { emptyText: 'номер телефону' }, text: 'Телефон' },
			{ text: 'Пошта' },
			{ text: 'Тип замовника' }
		]);
	}
});
