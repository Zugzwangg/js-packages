Ext.define('CFJS.admin.locale.ukr.grid.SecretaryPanel', {
	override	: 'CFJS.admin.grid.SecretaryPanel',
	config		: { title: 'Секретарі' },
	endDateText	: 'Закінчується',
	nameText	: 'Користувач',
	valueText	: 'Починається'
});