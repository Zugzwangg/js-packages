Ext.define('CFJS.admin.locale.ukr.grid.PersonPanel', {
	override: 'CFJS.admin.grid.PersonPanel',
	config	: { title: 'Працівники компанії' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'ПІБ' }, text: 'ПІБ' },
			{ filter: { emptyText: 'посада' }, text: 'Посада' },
			{ filter: { emptyText: 'ім\'я користувача' }, text: 'Ім\'я користувача' },
			{ filter: { emptyText: 'номер телефону' }, text: 'Телефон' },
			{ text: 'Пошта' },
			{ falseText: 'Ні', text: 'Активний', trueText: 'Так' },
			{ text: 'Останній вхід' }
		]);
	}
});
