Ext.define('CFJS.admin.locale.ukr.grid.CountryPanel', {
	override: 'CFJS.admin.grid.CountryPanel',
	config	: { title: 'Країни' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть назву країни' }, text: 'Назва' },
			{ filter: { emptyText: 'вкажіть телефонний код' }, text: 'Телефонний код' },
			{ filter: { emptyText: 'вкажіть код IATA' }, text: 'Код IATA' },
			{ filter: { emptyText: 'чисельність населення' }, text: 'Населення' }
		]);
	}
});
