Ext.define('CFJS.admin.locale.ukr.grid.CompanyAccountPanel', {
	override: 'CFJS.admin.grid.CompanyAccountPanel',
	config	: {
		actions	: { saveRecords: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' } },
		title	: 'Банківські рахунки'
	},
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть банк' }, text: 'Банк', tpl: '<tpl if="bank">{bank.name} (МФО {bank.code})</tpl>' },
			{ editor: { emptyText: 'номер р/р' }, filter: { emptyText: 'номер р/р' }, text: 'Р/р' },
			{ editor: { emptyText: 'назва рухунка' }, filter: { emptyText: 'назва рухунка' }, text: 'Назва' }
		]);
	}
});