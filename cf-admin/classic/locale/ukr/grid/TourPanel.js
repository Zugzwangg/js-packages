Ext.define('CFJS.admin.locale.ukr.grid.TourPanel', {
	override: 'CFJS.admin.grid.TourPanel',
	config	: { title: 'Тури' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть готель' }, text: 'Готель' },
			{ filter: { emptyText: 'вкажіть тип кімнати' }, text: 'Тип кімнати' },
			{ filter: { emptyText: 'вкажіть харчування' }, text: 'Харчування' },
			{ filter: { emptyText: 'вкажіть дату заїзду' }, text: 'Дата заїзду' },
			{ filter: { emptyText: 'вкажіть тривалість' }, text: 'Тривалість' },
			{ filter: { emptyText: 'вкажіть кількість дорослих' }, text: 'Дорослі' },
			{ filter: { emptyText: 'вкажіть кількість дітей' }, text: 'Діти' },
			{ filter: { emptyText: 'вкажіть ціну' }, text: 'Ціна' }
		]);
	}
});
