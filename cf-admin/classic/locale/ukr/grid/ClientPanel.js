Ext.define('CFJS.admin.locale.ukr.grid.ClientPanel', {
	override: 'CFJS.admin.grid.ClientPanel',
	config	: {
		actions: { browseFiles: { title: 'Перегляд файлів', tooltip: 'Перегляд додаткових файлів' } }
	}
});
