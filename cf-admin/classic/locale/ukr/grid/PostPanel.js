Ext.define('CFJS.admin.locale.ukr.grid.PostPanel', {
	override: 'CFJS.admin.grid.PostPanel',
	config	: { title: 'Посади компанії (штатний розклад)' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'назва посади' }, text: 'Назва' },
			{ filter: { emptyText: 'назва підрозділу' }, text: 'Підрозділ' },
			{ filter: { emptyText: 'ім\'я керівника' }, text: 'Керівник' },
			{ filter: { emptyText: 'ім\'я працівника' }, text: 'Працівник' },
			{ text: 'Рівень доступу' }
		]);
	}
});
