Ext.define('CFJS.admin.locale.ukr.grid.ExtAppPanel', {
	override: 'CFJS.admin.grid.ExtAppPanel',
	config	: { title: 'Зовнішні додатки' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть код' }, text: 'Код' },
			{ filter: { emptyText: 'вкажіть назву додатку' }, text: 'Назва' },
			{ text: 'Організація' },
			{ text: 'Підрозділ' },
			{ text: 'Сертифікат (С/Н)' },
			{ falseText: 'Ні', text: 'Активний', trueText: 'Так' }
		]);
	}
});
