Ext.define('CFJS.admin.locale.ukr.grid.MemorableDatesPanel', {
	override		: 'CFJS.admin.grid.MemorableDatesPanel',
	config			: { title: 'Пам\'ятні дати' },
	defDescription	: 'Опис пам\'ятної дати',
	renderColumns	: function() {
		return CFJS.merge(this.callParent(arguments), [{ text: 'Дата' },{ text: 'Опис' }]);
	}
});