Ext.define('CFJS.admin.locale.ukr.grid.RegionPanel', {
	override: 'CFJS.admin.grid.RegionPanel',
	config	: { title: 'Регіони' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'вкажіть країну' }, text: 'Країна' },
			{ filter: { emptyText: 'вкажіть назву регіону' }, text: 'Назва' },
			{ filter: { emptyText: 'чисельність населення' }, text: 'Населення' }
		]);
	}
});
