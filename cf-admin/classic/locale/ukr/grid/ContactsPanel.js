Ext.define('CFJS.admin.locale.ukr.grid.ContactsPanel', {
	override		: 'CFJS.admin.grid.ContactsPanel',
	config			: { title: 'Додаткові контакти' },
	defContactName	: 'Назва контакту',
	defContactValue	: 'Значення контакту',
	renderColumns	: function() {
		return CFJS.merge(this.callParent(arguments), [{ text: 'Назва' },{ text: 'Контакт' }]);
	}
});