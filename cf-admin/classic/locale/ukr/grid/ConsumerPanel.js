Ext.define('CFJS.admin.locale.ukr.grid.ConsumerPanel', {
	override: 'CFJS.admin.grid.ConsumerPanel',
	config	: { title: 'Споживачі' },
	renderColumns: function() {
		return CFJS.merge(this.callParent(arguments), [
			{ filter: { emptyText: 'звернення' }, text: 'Звернення' },
			{ filter: { emptyText: 'ім\'я' }, text: 'Ім\'я' },
			{ filter: { emptyText: 'по батькові' }, text: 'По батькові' },
			{ filter: { emptyText: 'прізвище' }, text: 'Прізвище' },
			{ text: 'День народження' },
			{ filter: { emptyText: 'номер телефону' }, text: 'Телефон' },
			{ text: 'Пошта' }
		]);
	}
});
