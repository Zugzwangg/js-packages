Ext.define('CFJS.admin.view.BalancesModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.admin.balances',
	requires	: [ 'CFJS.store.financial.Balances' ],
	itemName	: 'editBalance',
	itemModel	: 'CFJS.model.financial.Balance',
	formulas	: {
		customerType: {
			bind: '{_parentItemName_.id}',
			get	: function() {
				var	client = this.getParentItem(),
					type = client && client.entityName && client.entityName.toLowerCase();
				return type || null;
			}
		}
	},
	stores		: {
		balances: {
			type	: 'balances',
			page	: 0,
			filters	: [{
				id		: 'customerId',
				property: 'customer.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_parentItemName_.id}'
			},{
				id		: 'customerClass',
				property: 'customer.class',
				type	: 'string',
				operator: 'eq',
				value	: '{customerType}'
			}],
			sorters	: [ 'currency.name' ]
		}
	},
	
	defaultItemConfig: function() {
		var	parent = this.getParentItem();
		if (parent) return {
			customer	: { id: parent.id, name: parent.name },
			properties	: { field_customer_type: { '@type': 'string', '$': this.get('clientType') } }
		};
	}
	
});
