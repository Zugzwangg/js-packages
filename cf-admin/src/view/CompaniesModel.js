Ext.define('CFJS.admin.view.CompaniesModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.companies',
	requires: [ 'CFJS.model.dictionary.Country', 'CFJS.store.dictionary.Companies' ],
	itemName: 'editCompany',
	stores	: {
		companies: {
			type	: 'companies',
			session	: { schema: 'dictionary' },
			filters	: [{
				id		: 'unit',
				property: 'unit.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{userUnit.id}'
			},{
				property: 'customerType',
				type	: 'string',
				operator: '!eq'
			}]
		}
	},
	
	filesPermissions: function() {
		return this.getPermissions('customers.company.files');
	}
	
});
