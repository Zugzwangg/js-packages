Ext.define('CFJS.admin.view.ToursModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.tours',
	requires: [ 'CFJS.store.travel.Tours' ],
	itemName: 'editTour',
	stores	: {
		tours	: {
			type	: 'tours',
			autoLoad: true,
			session	: { schema: 'travel' }
		}
    }
});
