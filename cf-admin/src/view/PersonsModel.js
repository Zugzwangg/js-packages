Ext.define('CFJS.admin.view.PersonsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.persons',
	requires: [ 'CFJS.store.orgchart.Persons' ],
	itemName: 'editPerson',
	stores	: {
		persons: {
			type	: 'persons',
			session	: { schema: 'org_chart' }
		}
    }
});
