Ext.define('CFJS.admin.view.CompanyAccountsModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.admin.company-accounts',
	requires	: [ 'CFJS.store.dictionary.CompanyAccounts' ],
	itemName	: 'editCompanyAccount',
	itemModel	: 'CompanyAccount',
	stores		: {
		companyAccounts: {
			type	: 'companyaccounts',
			autoLoad: true,
			session	: { schema: 'dictionary' },
			filters	: [{
				id		: 'company',
				property: 'company.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_parentItemName_.id}'
			}]
		}
	},
	
	defaultItemConfig: function() {
		var	company = this.getParentItem();
		if (company) return { companyId: company.id }
	} 
	
});
