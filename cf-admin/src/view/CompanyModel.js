Ext.define('CFJS.admin.view.CompanyModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.company-edit',
	requires	: [ 'CFJS.store.Named', 'CFJS.model.dictionary.Company' ],
	itemName	: undefined,
	itemModel	: 'Company',
	formulas	: {
		// address
		address: {
			bind: '{_itemName_.address}',
			get: function(address) { return this.parseAddress(address) }
		},
		addressCountry	: CFJS.app.EditorViewModel.buildAddressFormula('address', 0),
		addressZipCode	: CFJS.app.EditorViewModel.buildAddressFormula('address', 1),
		addressRegion	: CFJS.app.EditorViewModel.buildAddressFormula('address', 2),
		addressCity		: CFJS.app.EditorViewModel.buildAddressFormula('address', 3),
		addressStreet	: CFJS.app.EditorViewModel.buildAddressFormula('address', 4),
		addressBuilding	: CFJS.app.EditorViewModel.buildAddressFormula('address', 5),
		addressRoom		: CFJS.app.EditorViewModel.buildAddressFormula('address', 6)
	},
	stores		: {
		memorableDates: {
			data	: '{_itemName_.memorableDates}',
			fields	: [
				{ name: 'date', 		type: 'date', 	critical: true },
				{ name: 'description',	type: 'string', critical: true }
			]
		}
	},
	
	defaultItemConfig: function() {
		var me = this, user = me.get('user'),
			post = user && user.isPerson ? user.get('post') : null;
		return {
			author		: user ? user.get('name') : null,
			unit 		: post ? post.unit : null,
			country		: me.getRecord('Country', 206),
			startDate	: new Date()
		}
	}
	
});
