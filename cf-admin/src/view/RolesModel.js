Ext.define('CFJS.admin.view.RolesModel', {
	extend		: 'CFJS.app.BaseEditorModel',
	alias		: 'viewmodel.admin.roles',
	requires	: [ 'CFJS.store.orgchart.Roles' ],
	itemName	: 'editRole',
	itemModel	: 'CFJS.model.orgchart.Role',
	stores		: {
		permissions	: { type: 'permissions', autoLoad: true },
		roles		: {
			type	: 'roles',
			session	: { schema: 'org_chart' },
			sorters	: [ 'name' ]
    	}
    },
    
	setItem: function(item) {
		return this.linkItemRecord(item);
	},

});
