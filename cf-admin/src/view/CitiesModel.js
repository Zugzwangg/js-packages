Ext.define('CFJS.admin.view.CitiesModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.cities',
	requires: [ 'CFJS.store.dictionary.Cities' ],
	itemName: 'editCity',
	stores	: {
		cities: {
			type	: 'cities',
			session	: { schema: 'dictionary' },
			sorters	: [ 'name' ]
		}
    }
});
