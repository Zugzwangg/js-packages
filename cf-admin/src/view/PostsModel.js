Ext.define('CFJS.admin.view.PostsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.posts',
	requires: [ 'CFJS.store.document.Permissions', 'CFJS.store.orgchart.Posts' ],
	itemName: 'editPost',
	stores	: {
		permissions	: { type: 'permissions', autoLoad: true },
		posts		: {
			type	: 'posts',
			session	: { schema: 'org_chart' },
			sorters	: [ 'name' ]
		},
    	sectors		: {
        	type	: 'named',
			autoLoad: true,
        	pageSize: 0,
			proxy	: { loaderConfig: { dictionaryType: 'sector_by_post' } },
			filters	: [
				{ id: 'post',	property: 'post.id',	type: 'numeric',	operator: 'eq', 	value: '{userInfo.id}' },
				{ id: 'month',	property: 'month',		type: 'date',		operator: '!after', value: Ext.Date.getFirstDateOfMonth(new Date()).getTime() }
			],
			sorters	: [ 'sector.name' ]
    	}
    }
});
