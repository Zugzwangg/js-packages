Ext.define('CFJS.admin.view.CustomersModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.customers',
	requires: [ 'CFJS.store.dictionary.Customers' ],
	itemName: 'editCustomer',
	stores	: {
		customers: {
			type	: 'customers',
			session	: { schema: 'dictionary' },
			filters	: [{
				id		: 'unit',
				property: 'unit.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{userUnit.id}'
			},{
				property: 'type',
				type	: 'string',
				operator: '!eq'
			}]
		}
    },
    
	filesPermissions: function() {
		return this.getPermissions('customers.customer.files');
	}

});
