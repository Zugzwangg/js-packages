Ext.define('CFJS.admin.view.CustomerModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.customer-edit',
	requires	: [ 'CFJS.model.dictionary.Customer' ],
	itemName	: undefined,
	itemModel	: 'Customer',
	stores		: {
		contacts	: {
			data	: '{_itemName_.contacts}',
			fields	: [
				{ name: 'name', 	type: 'string',	critical: true },
				{ name: 'contact',	type: 'string', critical: true }
			]
		},
		memorableDates	: {
			data	: '{_itemName_.memorableDates}',
			fields	: [
				{ name: 'date', 		type: 'date', 	critical: true },
				{ name: 'description',	type: 'string', critical: true }
			]
		}
	},
	formulas	: {
		// address
		address: {
			bind: '{_itemName_.address}',
			get: function(address) { return this.parseAddress(address) }
		},
		addressCountry	: CFJS.app.EditorViewModel.buildAddressFormula('address', 0),
		addressZipCode	: CFJS.app.EditorViewModel.buildAddressFormula('address', 1),
		addressRegion	: CFJS.app.EditorViewModel.buildAddressFormula('address', 2),
		addressCity		: CFJS.app.EditorViewModel.buildAddressFormula('address', 3),
		addressStreet	: CFJS.app.EditorViewModel.buildAddressFormula('address', 4),
		addressBuilding	: CFJS.app.EditorViewModel.buildAddressFormula('address', 5),
		addressRoom		: CFJS.app.EditorViewModel.buildAddressFormula('address', 6)
	},
	
	defaultItemConfig: function() {
		var me = this, user = me.get('user'),
			post = user && user.isPerson ? user.get('post') : null;
		return {
			author	: user ? user.get('name') : null,
			unit 	: post ? post.unit : null,
			created	: new Date()
		}
	}
	
});
