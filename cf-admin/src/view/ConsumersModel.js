Ext.define('CFJS.admin.view.ConsumersModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.consumers',
	requires: [ 'CFJS.store.dictionary.Consumers' ],
	itemName: 'editConsumer',
	stores	: {
		consumers: {
			type	: 'consumers',
			session	: { schema: 'dictionary' }
		}
    }
});
