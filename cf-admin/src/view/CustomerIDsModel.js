Ext.define('CFJS.admin.view.CustomerIDsModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.admin.customer-ids',
	requires	: [
		'CFJS.model.dictionary.Country',
		'CFJS.store.dictionary.CustomerIDs',
		'CFJS.store.dictionary.IDTypes'
	],
	itemName	: 'editCustomerID',
	itemModel	: 'CustomerID',
	links		: {
		defaultCountry: {
			type: 'Country',
			id	: 206
		}
	},
	stores		: {
		customerIDs: {
			type	: 'customerids',
			autoLoad: true,
			session	: { schema: 'dictionary' },
			filters	: [{
				id		: 'owner',
				property: 'owner.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_parentItemName_.id}'
			}]
		}
	},
	
	defaultItemConfig: function() {
		var me = this, parent = me.getParent(), country = me.get('defaultCountry'),
			customer = parent && parent.isEditorModel ? parent.getItem() : null, 
			config = { country: country.data, locale: CFJS.getLocale() };
		if (customer) config.owner = customer.isModel ? customer.data : customer;
		return config;
	} 
	
});
