Ext.define('CFJS.admin.view.CountriesModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.countries',
	requires: [ 'CFJS.model.dictionary.Country' ],
	itemName: 'editCountry',
	stores	: {
		countries: {
			type	: 'countries',
			session	: { schema: 'dictionary' },
			sorters	: [ 'name' ]
		}
    }
});
