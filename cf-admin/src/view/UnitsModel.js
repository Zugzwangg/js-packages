Ext.define('CFJS.admin.view.UnitsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.units',
	requires: [ 'CFJS.store.orgchart.Units' ],
	itemName: 'editUnit',
	stores	: {
		units		: {
			type	: 'units',
			session	: { schema: 'org_chart' },
			sorters	: [ 'name' ]
    	}
    }
});
