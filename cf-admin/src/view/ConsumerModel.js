Ext.define('CFJS.admin.view.ConsumerModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.consumer-edit',
	requires	: [ 'CFJS.model.dictionary.Consumer' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.dictionary.Consumer',
	formulas	: {
		// address
		address: {
			bind: '{_itemName_.address}',
			get: function(address) { return this.parseAddress(address) }
		},
		addressCountry	: CFJS.app.EditorViewModel.buildAddressFormula('address', 0),
		addressRegion	: CFJS.app.EditorViewModel.buildAddressFormula('address', 1),
		addressCity		: CFJS.app.EditorViewModel.buildAddressFormula('address', 2),
		addressStreet	: CFJS.app.EditorViewModel.buildAddressFormula('address', 3),
		addressBuilding	: CFJS.app.EditorViewModel.buildAddressFormula('address', 4),
		addressRoom		: CFJS.app.EditorViewModel.buildAddressFormula('address', 5)
	}
});