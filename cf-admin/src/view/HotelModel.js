Ext.define('CFJS.admin.view.HotelModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.hotel-edit',
	requires	: [ 'CFJS.model.hotel.Hotel' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.hotel.Hotel'
});