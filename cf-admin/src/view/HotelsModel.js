Ext.define('CFJS.admin.view.HotelsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.hotels',
	requires: [ 'CFJS.store.hotel.Hotels' ],
	itemName: 'editHotel',
	stores	: {
		hotels		: {
			type	: 'hotels',
			session	: { schema: 'hotel' },
			sorters	: [ 'name' ]
		}
    }
});
