Ext.define('CFJS.admin.view.ExtAppModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.extapp-edit',
	requires	: [ 'CFJS.model.ExtApp' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.ExtApp',

	defaultItemConfig: function() {
		var	locale = CFJS.config.locale || {},
			msg = locale[locale.name] || {};
		return { active: true, organization: msg.companyName };
	}

});