Ext.define('CFJS.admin.view.RegionModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.region-edit',
	requires	: [ 'CFJS.model.dictionary.Region' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.dictionary.Region'
});