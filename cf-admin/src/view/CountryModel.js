Ext.define('CFJS.admin.view.CountryModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.country-edit',
	requires	: [ 'CFJS.model.dictionary.Country' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.dictionary.Country'
});