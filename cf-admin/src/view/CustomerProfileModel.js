Ext.define('CFJS.admin.view.CustomerProfileModel', {
	extend				: 'CFJS.admin.view.CustomerModel',
	alias				: 'viewmodel.admin.customer-profile',
	itemName			: 'profileCustomer',
	defaultItemConfig	: Ext.emptyFn,
	updateItemName		: Ext.emptyFn,
	updateItemModel		: Ext.emptyFn
});
