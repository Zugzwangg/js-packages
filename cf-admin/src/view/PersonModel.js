Ext.define('CFJS.admin.view.PersonModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.person-edit',
	requires	: [ 'CFJS.model.orgchart.Person' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.orgchart.Person',
});