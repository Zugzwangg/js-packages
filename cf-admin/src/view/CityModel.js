Ext.define('CFJS.admin.view.CityModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.city-edit',
	requires	: [ 'CFJS.model.dictionary.City' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.dictionary.City'
});