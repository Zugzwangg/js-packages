Ext.define('CFJS.admin.view.UnitModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.unit-edit',
	requires	: [
		'CFJS.model.orgchart.Unit',
		'CFJS.store.Named',
	],
	itemName	: undefined,
	itemModel	: 'CFJS.model.orgchart.Unit',
	formulas	: {
		// address
		address			: {
			bind: '{_itemName_.address}',
			get	: function(address) { return this.parseAddress(address) }
		},
		addressCountry	: CFJS.app.EditorViewModel.buildAddressFormula('address', 0),
		addressZipCode	: CFJS.app.EditorViewModel.buildAddressFormula('address', 1),
		addressRegion	: CFJS.app.EditorViewModel.buildAddressFormula('address', 2),
		addressCity		: CFJS.app.EditorViewModel.buildAddressFormula('address', 3),
		addressStreet	: CFJS.app.EditorViewModel.buildAddressFormula('address', 4),
		addressBuilding	: CFJS.app.EditorViewModel.buildAddressFormula('address', 5),
		addressRoom		: CFJS.app.EditorViewModel.buildAddressFormula('address', 6),
		logoUrl			: {
			bind: '{_itemName_.logo}',
			get	: function(logo) { return CFJS.model.orgchart.Unit.logoUrl(null, logo); }
		}
	},
	stores		: {
		employees: {
			type	: 'named',
			proxy	: { loaderConfig: { dictionaryType: 'person' } },
			filters	: [{
				id		: 'postUnit',
				property: 'post.unit.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			},{
				property: 'active',
				type	: 'boolean',
				operator: 'eq',
				value	: true
			}],
			sorters	: [{ property: 'name'}]
		}
	},
	
	setItem: function(item) {
		return this.linkItemRecord(item);
	},


});