Ext.define('CFJS.admin.view.RegionsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.regions',
	requires: [ 'CFJS.store.dictionary.Regions' ],
	itemName: 'editRegion',
	stores	: {
		regions: {
			type	: 'regions',
			autoLoad: true,
			session	: { schema: 'dictionary' },
			sorters	: [ 'name' ]
		}
    }
});
