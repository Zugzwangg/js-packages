Ext.define('CFJS.admin.view.PostModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.post-edit',
	requires	: [ 'CFJS.store.Named', 'CFJS.store.orgchart.Secretaries', 'CFJS.model.orgchart.Post' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.orgchart.Post',
	stores		: {
		boses: {
			type	: 'named',
			autoLoad: true,
			proxy	: { loaderConfig: { dictionaryType: 'post' } },
			filters	: [{
				id		: 'role',
				property: 'role.id',
				type	: 'numeric',
				operator: '<=',
				value	: '{_itemName_.role.id}'
			}],
			sorters	: [{ property: 'name'}]
		},
		secretaries: {
			type	: 'secretaries',
			autoLoad: true,
			filters	: [{
				id		: 'post',
				property: 'post.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			},{
				property: 'endDate',
				type	: 'date',
				operator: '!before',
				value	: new Date().getTime()
			}],
			pageSize: 0,
			sorters	: [{ property: 'post.name'}]
		}
	}
});