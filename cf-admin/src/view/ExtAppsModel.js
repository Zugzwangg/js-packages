Ext.define('CFJS.admin.view.ExtAppsModel', {
	extend		: 'CFJS.app.BaseEditorModel',
	alias		: 'viewmodel.admin.extapps',
	requires	: [ 'CFJS.store.ExtApps' ],
	itemName	: 'editApplication',
	stores		: {
		applications: {
			type	: 'extapps',
			session	: { schema: 'public' },
			sorters	: [ 'id' ]
		}
	}	
	
});
