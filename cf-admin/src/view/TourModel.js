Ext.define('CFJS.admin.view.TourModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.admin.tour-edit',
	requires	: [ 'CFJS.model.travel.Tour', 'CFJS.store.dictionary.Currencies' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.travel.Tour'
});