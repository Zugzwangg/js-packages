Ext.define('CFJS.admin.controller.Consumers', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.consumers',
	editor	: 'admin-panel-consumer-editor',
	id		: 'consumersMain',
	routeId	: 'consumers',
	
	capitalizeName: function(record, field) {
		var name = record.get(field);
		if (name && name !== '') record.set(field, name.capitalize()); 
	},

	onBeforeSaveRecord: function(record) {
		var me = this;
		me.capitalizeName(record, 'name');
		me.capitalizeName(record, 'lastName');
		me.capitalizeName(record, 'middleName');
	}
	
});
