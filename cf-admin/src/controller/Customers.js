Ext.define('CFJS.admin.controller.Customers', {
	extend		: 'CFJS.admin.controller.Clients',
	alias		: 'controller.admin.customers',
	editor		: 'admin-panel-customer-editor',
	fileType	: 'customers',
	id			: 'customersMain',
	routeId		: 'customers',
	
	capitalizeFIO: function(record, locale) {
		var me = this;
		me.capitalizeName(record, 'name_' + locale);
		me.capitalizeName(record, 'last_name_' + locale);
		me.capitalizeName(record, 'middle_name_' + locale);
	},
	
	capitalizeName: function(record, field) {
		var name = record.get(field);
		if (name && name !== '') record.set(field, name.capitalize()); 
	},

	getContactsStore: function() {
		var contacts = this.lookupContacts();
		return contacts ? contacts.getStore() : null;
	},

	lookupContacts: function() {
		var me = this;
		return me.contacts = me.contacts || me.getView().down('admin-grid-contacts-panel[name=contacts]');
	},

	onAddContact: function(component) {
		var me = this, grid = me.lookupContacts();
		if (grid) me.addChild(grid, { name: grid.defContactName, contact: grid.defContactValue });
	},
	
	onBeforeSaveRecord: function(record) {
		var me = this, contacts = me.getContactsStore(),
			locales = ['uk', 'ru', 'en'], index;
		if (contacts && contacts.isDirty()) {
			var data = [];
			contacts.commitChanges();
			contacts.each(function(record) { 
				data.push({ name: record.get('name'), contact: record.get('contact')}); 
			});
			record.set('contacts', data, { silent: true });
		}
		for (index in locales) {
			me.capitalizeFIO(record, locales[index]);
		}
		return me.callParent(arguments);
	},
	
	onEditContact: function(component) {
		this.editChild(this.lookupContacts());
	},
	
	onRemoveContact: function(component) {
		this.removeChild(this.lookupContacts());
	}

});
