Ext.define('CFJS.admin.controller.ExtApps', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.extapps',
	editor	: 'admin-panel-extapp-editor',
	id		: 'extappsMain',
	routeId	: 'external_applications'
});
