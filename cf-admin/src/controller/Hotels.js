Ext.define('CFJS.admin.controller.Hotels', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.hotels',
	editor	: 'admin-panel-hotel-editor',
	id		: 'hotelsMain',
	routeId	: 'hotels',
	fileType: 'hotels',

	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.browseFiles, !selection || selection.length !== 1);
		this.callParent(arguments);
	},

	onBrowseFiles: function(component) {
		var me = this, record = me.lookupCurrentItem();
		if (record && record.id > 0) {
			Ext.create({
				xtype			: 'window-file-browser',
				title			: record.get('name'),
				browserConfig	: {
					xtype		: 'admin-grid-hotel-images-browser',
					fileType	: me.fileType,
					hotel		: record,
					pathPrefix	: record.id.toString() + '/images'
				}
			});
		}
	}

});
