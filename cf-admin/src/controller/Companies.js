Ext.define('CFJS.admin.controller.Companies', {
	extend		: 'CFJS.admin.controller.Clients',
	alias		: 'controller.admin.companies',
	editor		: 'admin-panel-company-editor',
	fileType	: 'companies',
	id			: 'companiesMain',
	routeId		: 'companies',
	
	onBeforeSaveRecord: function(record) {
		var country = record.get('country');
		if (country && country.isModel) record.set('country', country.getData());
		this.callParent(arguments);
	}
	
});
