Ext.define('CFJS.admin.controller.Countries', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.countries',
	editor	: 'admin-panel-country-editor',
	id		: 'countriesMain',
	routeId	: 'countries'
});
