Ext.define('CFJS.admin.controller.CustomerProfile', {
	extend		: 'CFJS.admin.controller.Customers',
	alias		: 'controller.admin.customer-profile',
	editor		: undefined,
	id			: 'customerProfile',
	
	lookupCurrentItem: function() {
		return this.lookupRecord();
	},
	
	getEditorComponent: function() {
		return this.getView();
	},
	
	lookupGrid: function() {
		return null;
	},
	
	lookupRecord: function() {
		return this.getViewModel().getItem();
	},

	onSaveRecord: function(component, force) {
		var me = this, record = me.lookupRecord();
		if (record && me.fireEvent('beforesaverecord', record) !== false && (record.dirty || record.phantom || force)) {
			me.disableComponent(component, true);
			record.save({
				callback: function(record, operation, success) {
					me.disableComponent(component);
				}
			});
		}
	}

});