Ext.define('CFJS.admin.controller.Cities', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.cities',
	editor	: 'admin-panel-city-editor',
	id		: 'citiesMain',
	routeId	: 'cities'
});
