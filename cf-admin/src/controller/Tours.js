Ext.define('CFJS.admin.controller.Tours', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.tours',
	editor	: 'admin-panel-tour-editor',
	id		: 'toursMain',
	routeId	: 'tours'
});
