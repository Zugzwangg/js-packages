Ext.define('CFJS.admin.controller.Persons', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.persons',
	editor	: 'admin-panel-person-editor',
	id		: 'personsMain',
	routeId	: 'persons',
	
	capitalizeName: function(record, field) {
		var name = record.get(field);
		if (name && name !== '') record.set(field, name.capitalize()); 
	},

	onBeforeSaveRecord: function(record) {
		var me = this, post = record.get('post');
		me.capitalizeName(record, 'name');
		record.set('post', post && post.id > 0 ? { id: post.id } : undefined);
	}
	
});
