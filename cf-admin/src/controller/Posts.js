Ext.define('CFJS.admin.controller.Posts', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.posts',
	editor	: 'admin-panel-post-editor',
	id		: 'postsMain',
	routeId	: 'posts',
	
	capitalizeName: function(record, field) {
		var name = record.get(field);
		if (name && name !== '') record.set(field, name.capitalize()); 
	},
	
	lookupSecretaries: function() {
		var editor = this.getEditorComponent();
		return editor ? editor.lookupSecretaries() : null ;
	},

	lookupSecretariesStore: function() {
		var panel = this.lookupSecretaries();
		return panel ? panel.getStore() : null;
	},
	
	onBeforeSaveRecord: function(record) {
		var me = this, person = record.get('person'),
			boss = record.get('boss'), role,
			store = me.lookupSecretariesStore();
		me.capitalizeName(record, 'name');
		record.set('person', person && person.id > 0 ? { id: person.id } : undefined);
		record.set('boss', boss && boss.id > 0 ? { id: boss.id } : undefined);
		if (store && store.isStore) store.sync();
	}
	
});
