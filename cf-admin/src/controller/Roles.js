Ext.define('CFJS.admin.controller.Roles', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.roles',
	editor	: 'admin-panel-role-editor',
	id		: 'rolesMain',
	routeId	: 'roles',

	lookupRecord: function() {
		return this.getViewModel().getItem();
	},

	maybeEditRecord: function(record, add) {
		var me = this, tree = me.getView(), selection;
		if (tree && record !== false && !tree.readOnly) {
			selection = tree.getSelection();
			if (!record && !add && selection && selection.length > 0) record = selection[0];
			me.startEditRecord(record, me.getViewModel());
		}
	},
	
	onBackClick: Ext.emptyFn,
	
	onBeforeSaveRecord: function(record) {
		if (!(record.get('parentId') > 0)) record.set('parentId', undefined);
	},
	
	onGridDblClick: Ext.emptyFn,
	
	onSelectionChange: function(selectable, records) {
		this.callParent(arguments);
		this.startEditRecord((records = Ext.Array.from(records)).length > 0 ? records[0] : false);
	},

	privates: {

		startEditRecord: function(record, vm) {
			if (!record && record !== false) {
				var me = this, tree = me.getView(),
				selection = Ext.Array.from(tree && tree.getSelection()),
				target = (selection.length > 0 ? selection[0] : null) || tree && tree.getRootNode(),
				append = function() {
					var node = { parentId: target.id };
					tree.selModel.select(node = target.appendChild(me.startEditRecord(node, vm)));
				};
				if (target && (target.isRoot || target.id > 0)) {
					if (!target.isExpanded()) target.expand(false, append);
					else append.call(me);
				}
				return;
			}
			if ((vm = vm || this.getViewModel()) && vm.isBaseModel) return vm.setItem(record);
		}
	}
	
});
