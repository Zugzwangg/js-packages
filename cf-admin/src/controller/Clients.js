Ext.define('CFJS.admin.controller.Clients', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.clients',
	fileType: 'clients',

	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.browseFiles, !selection || selection.length !== 1);
		this.callParent(arguments);
	},

	getFileBrowserPermissions: Ext.emptyFn,
	
	getMemorableDatesStore: function() {
		var panel = this.lookupMemorableDates();
		return panel ? panel.getStore() : null;
	},

	lookupMemorableDates: function() {
		var me = this;
		return me.memorableDates = me.memorableDates || me.getView().down('admin-grid-memorable-dates-panel[name=memorableDates]');
	},
	
	onAddMemoriableDate: function(component) {
		var me = this, grid = me.lookupMemorableDates();
		if (grid) me.addChild(grid, { date: new Date(), description: grid.defDescription });
	},
	
	onBeforeSaveRecord: function(record) {
		var store = this.getMemorableDatesStore(), data = [];
		if (store && store.isDirty()) {
			store.commitChanges();
			store.each(function(record) {
				data.push({ date: record.get('date'), description: record.get('description') });
			});
			record.set('memorableDates', data, { silent: true });
		}
	},
	
	onBrowseFiles: function(component) {
		var me = this, record = me.lookupCurrentItem();
		if (record && record.id > 0) {
			Ext.create({
				xtype			: 'window-file-browser',
				title			: 'Клиент - ' + record.get('fullName'),
				browserConfig	: {
					fileType	: me.fileType,
					permissions	: me.getViewModel().filesPermissions(), 
					pathPrefix	: record.id.toString()
				}
			});
		}
	},
	
	onEditMemoriableDate: function(component) {
		this.editChild(this.lookupMemorableDates());
	},
	
	onRemoveMemoriableDate: function(component) {
		this.removeChild(this.lookupMemorableDates());
	},
	
	privates: {

		addChild: function(grid, data) {
			var store = grid && !grid.readOnly && grid.getStore();
			if (data && store && store.isStore) this.startEditChild(grid, store.add(data)[0]);
		},
		
		editChild: function(grid) {
			var selection = grid && !grid.readOnly && grid.getSelection();
			if (selection && selection.length > 0) this.startEditChild(grid, selection[0]);
		},
		
		removeChild: function(grid) {
			var selection = grid && !grid.readOnly && grid.getSelection();
			if (selection && selection.length > 0) {
				grid.getStore().remove(selection);
				this.doSelectionChange(grid, [], grid.actions);
			}
		},
		
		startEditChild: function(grid, record) {
			if (record && record.isModel) {
				var editor = grid && !grid.readOnly && (grid.findPlugin('rowediting') || grid.findPlugin('cellediting'));
				if (editor) editor.startEdit(record);
			}
		}

	}

});
