Ext.define('CFJS.admin.controller.Base', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.admin.main',
	listeners	: { beforesaverecord: 'onBeforeSaveRecord', beforeremoverecord: 'onBeforeRemoveRecord' },

	callView: function(name, arguments) {
		var view = this.getView(), func = view && view[name];
		if (Ext.isFunction(func)) view[name].apply(view, arguments);
	},

	onAddRecord: function(component) {
		var me = this;
		me.callParent(arguments);
		me.callView('onAddRecord', arguments);
	},

	onBeforeRemoveRecord: Ext.identityFn,
	
	onBeforeSaveRecord: Ext.identityFn,

	onEditRecord: function(component) {
		var me = this;
		me.callParent(arguments);
		me.callView('onEditRecord', arguments);
	},
	
	onItemContextMenu:  function(view, rec, node, index, e) {
		this.callView('onItemContextMenu', arguments);
	},
	
	onItemDblClick: function(selectable, record) {
		this.callView('onItemDblClick', arguments);
	},

	onSelectionChange: function(selectable, selection) {
		this.callView('onSelectionChange', arguments);
		this.callParent(arguments);
	},

	onSelectRecord: function(component) {
		this.callView('onSelectRecord', arguments);
	}

});
