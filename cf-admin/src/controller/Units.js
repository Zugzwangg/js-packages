Ext.define('CFJS.admin.controller.Units', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.units',
	editor	: 'admin-panel-unit-editor',
	id		: 'unitsMain',
	routeId	: 'departments',
	
	capitalizeName: function(record, field) {
		var name = record.get(field);
		if (name && name !== '') record.set(field, name.capitalize()); 
	},

	lookupTree: function() {
		return this.lookupGrid();
	},
	
	lookupTreeViewModel: function(tree) {
		tree = tree || this.lookupTree();
		return tree ? tree.lookupViewModel() : null;
	},

	onBeforeSaveRecord: function(record) {
		if (!(record.get('parentId') > 0)) record.set('parentId', undefined);
		this.capitalizeName(record, 'name');
	},
	
//	onSelectionChange: function(selectable, records) {
//		this.startEditRecord((records = Ext.Array.from(records)).length > 0 ? records[0] : false);
//	},
	
	privates: {

		startEditRecord: function(record, vm) {
			if (!record) {
				var me = this, tree = me.lookupTree(),
				selection = Ext.Array.from(tree && tree.getSelection()),
				target = (selection.length > 0 ? selection[0] : null) || tree && tree.getRootNode(),
				append = function() {
					var node = { parentId: target.id };
					tree.selModel.select(node = target.appendChild(me.startEditRecord(node, vm)));
				};
				if (target && (target.isRoot || target.id > 0)) {
					if (!target.isExpanded()) target.expand(false, append);
					else append.call(me);
				}
				return;
			}
			return this.callParent(arguments);
		}
	}
	
});
