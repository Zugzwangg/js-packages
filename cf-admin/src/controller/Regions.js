Ext.define('CFJS.admin.controller.Regions', {
	extend	: 'CFJS.admin.controller.Base',
	alias	: 'controller.admin.regions',
	editor	: 'admin-panel-region-editor',
	id		: 'regionsMain',
	routeId	: 'regions'
});
