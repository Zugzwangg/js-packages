Ext.onReady(function() {
	CFJS.overrideStoreData('strictBlankStates', [
		{ id: 'BLANK',		name: 'Чистий' },
		{ id: 'CANCELED',	name: 'Анульований' },
		{ id: 'DAMAGED',	name: 'Зіпсований' },
		{ id: 'PRINTED',	name: 'Надрукований' }
	]);
	CFJS.overrideStoreData('strictOperationTypes', [
		{ id: 'ISSUE',			name: 'Випуск' },
		{ id: 'CANCELLATION',	name: 'Списання' },
		{ id: 'MOVE',			name: 'Переміщення' },
		{ id: 'PRINT',			name: 'Друк' }
	]);
});
Ext.define('CFJS.locale.ukr.document.util.Forms', {
	override		: 'CFJS.document.util.Forms',
	singleton 		: true,
	aggregations	: [
		{ id: 'NONE',	name: '...' },
		{ id: 'COUNT',	name: 'Кількість' },
		{ id: 'MAX',	name: 'Максимальне' },
		{ id: 'MIN',	name: 'Мінімальне' },
		{ id: 'AVG',	name: 'Середнє' },
		{ id: 'SUM',	name: 'Сума' }
	],
	documentActions	: [
		{ id: 'ATTACH_FILES',		name: 'Прикріпити файли' },
		{ id: 'CHANGE_PERMISSION',	name: 'Зміна дозволів' },
		{ id: 'CREATE',				name: 'Створити' },
		{ id: 'DELETE',				name: 'Видалити' },
		{ id: 'PRINT',				name: 'Друкувати' },
		{ id: 'READ',				name: 'Читати' },
		{ id: 'SIGN',				name: 'Підписати' },
		{ id: 'WRITE',				name: 'Змінити' }
	],
	fieldTypes		: [
		{ id: 'boolean',		name: 'Логічне' },
		{ id: 'date',			name: 'Дата' },
		{ id: 'datetime',		name: 'Дата та час' },
		{ id: 'dictionary',		name: 'Довідник' },
		{ id: 'document',		name: 'Документ' },
		{ id: 'graphics',		name: 'Графіка' },
		{ id: 'info',			name: 'Інформаційне' },
		{ id: 'integer',		name: 'Цілочисельне' },
		{ id: 'memo',			name: 'Багаторядковий текст' },
		{ id: 'many_of_many',	name: 'Декілька з багатьох' },
		{ id: 'one_of_many',	name: 'Один з багатьох' },
		{ id: 'real',			name: 'З плаваючою комою' },
		{ id: 'sys_dictionary',	name: 'Системний довідник' },
		{ id: 'table',			name: 'Таблиця' },
		{ id: 'text',			name: 'Текст' }
	],
	formTypes	: [
		{ id: 'DICTIONARY',	name: 'Довідник' },
		{ id: 'DOCUMENT',	name: 'Документ' },
		{ id: 'REPORT',		name: 'Звіт' },
		{ id: 'TABLE',		name: 'Таблиця' }
	],
	hAlign			: [{ id: 'LEFT', name: 'Ліворуч' },{ id: 'CENTER', name: 'Центрувати' }, { id: 'RIGHT', name: 'Праворуч' }],
	lAlign			: [{ id: 'LEFT', name: 'Ліворуч' },{ id: 'TOP', name: 'Зверху' }],
	sysDictionaries	: [
		{ id: 0,	name: 'Car_brand',			title: 'Марка автомобіля' },
		{ id: 1,	name: 'Car_model',			title: 'Модель автомобіля' },
		{ id: 2,	name: 'City',				title: 'Місто' },
		{ id: 3,	name: 'Company',			title: 'Компанія' },
		{ id: 4,	name: 'Company_account',	title: 'Рахунки компанії' },
		{ id: 5,	name: 'Consumer',			title: 'Споживач' },
		{ id: 6,	name: 'Country',			title: 'Країна' },
		{ id: 7,	name: 'Currency',			title: 'Валюта' },
		{ id: 8,	name: 'Customer',			title: 'Клієнт' },
		{ id: 9,	name: 'Customer_id',		title: 'Документи клієнта' },
		{ id: 10,	name: 'Id_type',			title: 'Тип документа' },
		{ id: 11,	name: 'Order',				title: 'Замовлення' },
		{ id: 12,	name: 'Person',				title: 'Робітники' },
		{ id: 13,	name: 'Post',				title: 'Посада' },
		{ id: 14,	name: 'Region',				title: 'Регіон' },
		{ id: 15,	name: 'Strict_form',		title: 'БСЗ' }
	],
	vAlign			: [{ id: 'TOP', name: 'Зверху' },{ id: 'MIDDLE', name: 'Центрувати' },{ id: 'BOTTOM', name: 'Знизу' }]
});
Ext.define('CFJS.locale.ukr.document.view.EditController', {
	override		: 'CFJS.document.view.EditController',
	confirmRemove	: function(fn) {
		return Ext.fireEvent('confirmlostdata', 'видалити вибрані записи', fn, this);
	}
});
Ext.define('CFJS.locale.ukr.document.view.ViewController', {
	override: 'CFJS.document.view.ViewController',
	messages: {
		notAllowCreateFText	: 'Ви не можете створювати нові документи цього типу.',
		notAllowEditFText	: 'Ви не можете редагувати документи цього типу.',
		notAllowEditDText	: 'Ви не можете редагувати цей документ.',
		notAllowPrintDText	: 'Ви не можете роздрукувати цей документ.'
	}
});
