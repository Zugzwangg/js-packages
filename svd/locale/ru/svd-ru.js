Ext.onReady(function() {
	CFJS.overrideStoreData('strictBlankStates', [
		{ id: 'BLANK',		name: 'Чистый' },
		{ id: 'CANCELED',	name: 'Аннулирован' },
		{ id: 'DAMAGED',	name: 'Испорчен' },
		{ id: 'PRINTED',	name: 'Напечатан' }
	]);
	CFJS.overrideStoreData('strictOperationTypes', [
		{ id: 'ISSUE',			name: 'Выпуск' },
		{ id: 'CANCELLATION',	name: 'Списание' },
		{ id: 'MOVE',			name: 'Перемещение' },
		{ id: 'PRINT',			name: 'Печать' }
	]);
});
Ext.define('CFJS.locale.ru.document.util.Forms', {
	override		: 'CFJS.document.util.Forms',
	singleton 		: true,
	aggregations	: [
		{ id: 'NONE',	name: '...' },
		{ id: 'COUNT',	name: 'Количество' },
		{ id: 'MAX',	name: 'Максимальное' },
		{ id: 'MIN',	name: 'Минимальное' },
		{ id: 'AVG',	name: 'Среднее' },
		{ id: 'SUM',	name: 'Сумма' }
	],
	documentActions	: [
		{ id: 'ATTACH_FILES',		name: 'Прикрепить файлы' },
		{ id: 'CHANGE_PERMISSION',	name: 'Изменить разрешения' },
		{ id: 'CREATE',				name: 'Создать' },
		{ id: 'DELETE',				name: 'Удалить' },
		{ id: 'PRINT',				name: 'Печатать' },
		{ id: 'READ',				name: 'Читать' },
		{ id: 'SIGN',				name: 'Подписать' },
		{ id: 'WRITE',				name: 'Изменить' }
	],
	fieldTypes		: [
		{ id: 'boolean',		name: 'Логическое' },
		{ id: 'date',			name: 'Дата' },
		{ id: 'datetime',		name: 'Дата и время' },
		{ id: 'dictionary',		name: 'Справочник' },
		{ id: 'document',		name: 'Документ' },
		{ id: 'graphics',		name: 'Графика' },
		{ id: 'info',			name: 'Информационное' },
		{ id: 'integer',		name: 'Целочисленное' },
		{ id: 'memo',			name: 'Многострочный текст' },
		{ id: 'many_of_many',	name: 'Несколько из многих' },
		{ id: 'one_of_many',	name: 'Один из многих' },
		{ id: 'real',			name: 'С плавающей запятой' },
		{ id: 'sys_dictionary',	name: 'Системный справочник' },
		{ id: 'table',			name: 'Таблица' },
		{ id: 'text',			name: 'Текст' }
	],
	formTypes	: [
		{ id: 'DICTIONARY',	name: 'Справочник' },
		{ id: 'DOCUMENT',	name: 'Документ' },
		{ id: 'REPORT',		name: 'Отчет' },
		{ id: 'TABLE',		name: 'Таблица' }
	],
	hAlign			: [{ id: 'LEFT', name: 'Слева' },{ id: 'CENTER', name: 'Центрировать' },{ id: 'RIGHT', name: 'Справа' }],
	lAlign			: [{ id: 'LEFT', name: 'Слева' },{ id: 'TOP', name: 'Сверху' }],
	sysDictionaries	: [
		{ id: 0,	name: 'Car_brand',			title: 'Марка автомобиля' },
		{ id: 1,	name: 'Car_model',			title: 'Модель автомобиля' },
		{ id: 2,	name: 'City',				title: 'Город' },
		{ id: 3,	name: 'Company',			title: 'Компания' },
		{ id: 4,	name: 'Company_account',	title: 'Счета компании' },
		{ id: 5,	name: 'Consumer',			title: 'Потребитель' },
		{ id: 6,	name: 'Country',			title: 'Страна' },
		{ id: 7,	name: 'Currency',			title: 'Валюта' },
		{ id: 8,	name: 'Customer',			title: 'Клиент' },
		{ id: 9,	name: 'Customer_id',		title: 'Документы клиента' },
		{ id: 10,	name: 'Id_type',			title: 'Тип документа' },
		{ id: 11,	name: 'Order',				title: 'Заказ' },
		{ id: 12,	name: 'Person',				title: 'Работники' },
		{ id: 13,	name: 'Post',				title: 'Должность' },
		{ id: 14,	name: 'Region',				title: 'Регион' },
		{ id: 15,	name: 'Strict_form',		title: 'БСО' }
	],
	vAlign			: [{ id: 'TOP', name: 'Сверху' },{ id: 'MIDDLE', name: 'Центрировать' },{ id: 'BOTTOM', name: 'Снизу' }]
});
Ext.define('CFJS.locale.ru.document.view.EditController', {
	override		: 'CFJS.document.view.EditController',
	confirmRemove	: function(fn) {
		return Ext.fireEvent('confirmlostdata', 'удалить выбранные записи', fn, this);
	}
});
Ext.define('CFJS.locale.ru.document.view.ViewController', {
	override: 'CFJS.document.view.ViewController',
	messages: {
		notAllowCreateFText	: 'Вы не можете создавать новые документы этого типа.',
		notAllowEditFText	: 'Вы не можете редактировать документы этого типа.',
		notAllowEditDText	: 'Вы не можете редактировать этот документ.',
		notAllowPrintDText	: 'Вы не можете распечатать этот документ.'
	}
});
