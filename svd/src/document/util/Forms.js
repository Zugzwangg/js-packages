//	@define CFJS.document.util.Forms
//	@require CFJS
//	@require CFJS.api.Api
Ext.define('CFJS.document.util.Forms', function() {
	var me;
	
	return {
		requires		: [ 'Ext.Date', 'Ext.util.Format' ],
		
		singleton		: true,

		aggregations	: [
			{ id: 'NONE',	name: '...' },
			{ id: 'AVG',	name: 'Average' },
			{ id: 'COUNT',	name: 'Count' },
			{ id: 'MIN',	name: 'Minimum' },
			{ id: 'MAX',	name: 'Maximum' },
			{ id: 'SUM',	name: 'Sum' }
		],

		dateUtil		: Ext.Date,

		documentActions	: [
			{ id: 'ATTACH_FILES',		name: 'Attach files' },
			{ id: 'CHANGE_PERMISSION',	name: 'Change permissions' },
			{ id: 'CREATE',				name: 'Create' },
			{ id: 'DELETE',				name: 'Delete' },
			{ id: 'PRINT',				name: 'Print' },
			{ id: 'READ',				name: 'Read' },
			{ id: 'SIGN',				name: 'Sign' },
			{ id: 'WRITE',				name: 'Update' }
		],

		fieldTypes		: [
			{ id: 'boolean',		name: 'Boolean' },
			{ id: 'date',			name: 'Date' },
			{ id: 'datetime',		name: 'Date-Time' },
			{ id: 'dictionary',		name: 'Dictionary' },
			{ id: 'document',		name: 'Document' },
			{ id: 'graphics',		name: 'Graphics' },
			{ id: 'info',			name: 'Information' },
			{ id: 'integer',		name: 'Integer' },
			{ id: 'memo',			name: 'Memo' },
			{ id: 'many_of_many',	name: 'A few of the many' },
			{ id: 'one_of_many',	name: 'One of the many' },
			{ id: 'real',			name: 'Real' },
			{ id: 'sys_dictionary',	name: 'System dictionary' },
			{ id: 'table',			name: 'Table' },
			{ id: 'text',			name: 'Text' }
		],

		formatter		: Ext.util.Format,

		formTabPrefix	: 'form',

		formTypes		: [
			{ id: 'DICTIONARY',	name: 'Dictionary' },
			{ id: 'DOCUMENT',	name: 'Document' },
			{ id: 'REPORT',		name: 'Report' },
			{ id: 'TABLE',		name: 'Table' }
		],

		hAlign			: [{ id: 'LEFT', name: 'Left' },{ id: 'CENTER', name: 'Center' },{ id: 'RIGHT', name: 'Right' }],

		lAlign			: [{ id: 'LEFT', name: 'Left' },{ id: 'TOP', name: 'Top' }],

		routeSeparator	: '/',

		sysDictionaries	: [
			{ id: 0,	name: 'Car_brand',		title: 'Car Brand' },
			{ id: 1,	name: 'Car_model',		title: 'Car Model' },
			{ id: 2,	name: 'City',			title: 'City' },
			{ id: 3,	name: 'Company',		title: 'Company' },
			{ id: 4,	name: 'Company_account',title: 'Company Account' },
			{ id: 5,	name: 'Consumer',		title: 'Consumer' },
			{ id: 6,	name: 'Country',		title: 'Country' },
			{ id: 7,	name: 'Currency',		title: 'Currency' },
			{ id: 8,	name: 'Customer',		title: 'Customer' },
			{ id: 9,	name: 'Customer_id',	title: 'Customer ID' },
			{ id: 10,	name: 'Id_type',		title: 'ID Type' },
			{ id: 11,	name: 'Order',			title: 'Order' },
			{ id: 12,	name: 'Person',			title: 'Person' },
			{ id: 13,	name: 'Post',			title: 'Post' },
			{ id: 14,	name: 'Region',			title: 'Region' },
			{ id: 15,	name: 'Strict_form',	title: 'Strict Form' }
		],

		tokenSeparator	: '',

		vAlign			: [{ id: 'TOP', name: 'Top' },{ id: 'MIDDLE', name: 'Middle' },{ id: 'BOTTOM', name: 'Bottom' }],

		constructor: function () {
			me = this;
		},

		bindMasterFilter: function(panel, editor, filter) {
			if (editor && filter && Ext.isString(filter)) {
				var vm = panel && panel.lookupViewModel(),
					props = filter.split(/[;/]/), i = 0, pairs;
				filter = { type: 'numeric', operator: 'eq' };
				for (; i < props.length; i++) {
					pairs = props[i].split('=', 2);
					switch (pairs[0]) {
					case 'm': case 'master':
					case 'v': case 'value':
						filter.value = pairs[1];
						break;
					case 'd': case 'detail':
					case 'f': case 'field':
					case 'p': case 'property':
						filter.id = filter.property = pairs[1];
						break;
					case 't': case 'type':
						filter.type = pairs[1];
						break;
					case 'c': case 'comparison':
					case 'o': case 'operator':
						filter.operator = pairs[1];
						break;
					default: break;
					}
				}
				if (vm && !Ext.isEmpty(filter.property) && !Ext.isEmpty(filter.value)) {
					vm.bind('{fields.field_' + filter.value + '}', function(value) {
						var f = panel.down('[name="' + editor.name + '"]'), 
							store = f && f.isCombo && f.getStore();
						if (store && store.isStore) {
							store.addFilter(Ext.apply(filter, { value: (value||{}).id }));
						}
					});
				}
			}
		},

		booleanRenderer: function(field, format) {
			if (Ext.isString(format)) format = format.split(';');
			return function(value, metaData, record) {
				return me.formatter.bool(me.parseBooleanProperty(field, record), format);
			};
		},

		columnSorter: function(a, b) {
			var parse = CFJS.safeParseInt;
			return parse(a && a.columnOrder || a) - parse(b && b.columnOrder || b);
		},

		contains: Ext.Array.contains, 

		dateRenderer: function(field, format) {
			return function(value, metaData, record) {
				return (value = me.parseDateProperty(field, record)) ? me.formatter.date(value, format) : '';
			};
		},

		defaultRenderer: function(field, maxHeight) {
			return function(value, metaData, record) {
				if (maxHeight) metaData.style = 'max-height: ' + maxHeight + ';';
				return record.getPropertyValue(field);
			}
		},

		dictionaryRenderer: function(field, key) {
			return function(value, metaData, record) {
				return record.getPropertyValue(field, key || 'name');
			};
		},

		enumById: function(array, id) {
			array = array || [];
			for (var i = 0; i < array.length; i++) {
				if (array[i] && array[i].id === id) return array[i];
			}
		},

		enumNameById: function(array, id) {
			return (me.enumById(array, id)||{}).name;
		},

		fieldOptions: function(type) {
			var opt = {};
			type = (type || '').toLowerCase();
			switch (type) {
			case 'info'	:
				Ext.apply(opt, { notCopy: true, permissionLevel: CFJS.shortMax, readOnly: true, reference: null });
			case 'table':
				Ext.apply(opt, { columnHidden: true, columnWidth: 0, description: false, masterFilter: null });
				if (type === 'table') Ext.apply(opt, { formula: null, defaultValue: null, readOnly: false });
			case 'dictionary':
			case 'document':
			case 'sys_dictionary': 
				Ext.apply(opt, { aggregation: 'NONE', mask: null });
				break;
			case 'graphics':
				Ext.apply(opt, { columnHidden: true, columnWidth: 0, defaultValue: null, readOnly: false });
			case 'boolean':
			case 'many_of_many':
			case 'one_of_many':
				opt.description = false;
			case 'memo':
				if (type !== 'boolean' && type !== 'many_of_many' && type !=='one_of_many') opt.mask = null;
				opt.aggregation = 'NONE';
			case 'text':
			case 'integer':
			case 'real':
			case 'date':
			case 'datetime':
				Ext.apply(opt, { masterFilter: null, reference: null });
			default:
				break;
			}
			return opt;
		},

		fieldPropertiesMap: function(type) {
			type = (type || '').toLowerCase();
			var isInfo = type === 'info', 
				isTable = type === 'table',
				contains = function(types) { return Ext.Array.contains(types, type); }, 
				map = {
					aggregation	: contains([ 'integer', 'real', 'date', 'datetime' ]),
					defaultValue: !isTable,
					description	: !contains([ 'memo', 'graphics', 'boolean', 'info', 'one_of_many', 'many_of_many', 'table' ]),
					formula		: !isTable && type !== 'graphics',
					notCopy		: !isTable && !isInfo,
					readOnly	: !isTable && !isInfo,
					required	: !isTable && !isInfo,
					reference	: contains([ 'dictionary', 'document', 'sys_dictionary', 'table' ]),
					columnHidden: !contains([ 'graphics', 'info', 'table' ])
				}, defaultValue;
				map.mask = map.aggregation || contains([ 'text', 'boolean', 'one_of_many', 'many_of_many' ]);
				map.masterFilter = map.reference && !isTable;
				map.columnWidth = map.columnHidden;
			return map;
		},

		fieldTypeName: function(type) {
			return me.enumNameById(me.fieldTypes, type);
		},

		formTypeName: function(type) {
			return me.enumNameById(me.formTypes, type);
		},

		iconCls: function(type) {
			switch ((type||'').toUpperCase()) {
				case 'DOCUMENT'		: return CFJS.UI.iconCls.FILE_TEXT;
				case 'DICTIONARY'	: return CFJS.UI.iconCls.BOOK;
				case 'REPORT' 		: return 'x-fa fa-line-chart';
				case 'TABLE'		: return CFJS.UI.iconCls.TABLE;
				default				: return CFJS.UI.iconCls.FOLDER;
			}
		},

		integerRenderer: function(field, format) {
			return function(value, metaData, record) {
				return isNaN(value = me.parseIntProperty(field, record)) ? '' : me.formatter.number(value, format);
			};
		},
		
		manyOfManyRenderer: function(field, format) {
			if (Ext.isString(format)) format = format.split(';');
			return function(value, metaData, record) {
				return isNaN(value = me.parseIntProperty(field, record)) ? '' : me.formatter.manyOfMany(value, format);
			};
		},

		oneOfManyRenderer: function(field, format) {
			if (Ext.isString(format)) format = format.split(';');
			return function(value, metaData, record) {
				return isNaN(value = me.parseIntProperty(field, record)) ? '' : me.formatter.oneOfMany(value, format);
			};
		},

		orderSorter: function(a, b) {
			var parse = CFJS.safeParseInt;
			return parse(a && a.order || a) - parse(b && b.order || b);
		},

		parseAnchor: function(anchor, value) {
			if (anchor && anchor !== 'none' ) {
				if(/^(r|right|b|bottom)$/i.test(anchor)) return value;
				else return parseFloat(anchor);
			}
		},

		parseBooleanProperty: function(field, record) {
			var value = record.getPropertyValue(field);
			return value === 'false' || value === false ? false : new Boolean(value).valueOf();
		},

		parseDateProperty: function(field, record) {
			var value = record.getPropertyValue(field);
			return !value || Ext.isDate(value) ? value : new Date(Date.parse(value));
		},

		parseFloatProperty: function(field, record) {
			var value = record.getPropertyValue(field);
			return Ext.isNumber(value) ? value : parseFloat(String(value).replace(/[\$,%]/g, ''));
		},

		parseIntProperty: function(field, record) {
			var value = record.getPropertyValue(field);
			return Ext.isNumber(value) ? value : parseInt(String(value).replace(/[\$,%]/g, ''), 10);
		},

		parseMeasure: function(measure, anchor) {
			measure = parseFloat(measure);
			if (isNaN(measure)) measure = me.parseAnchor(anchor, measure);
			if (isFinite(measure) && measure >= 0) {
				if (measure <= 1) measure = 100 * measure + '%';
				return measure;
			}
		},

		realRenderer: function(field, format) {
			return function(value, metaData, record) {
				return isNaN(value = me.parseFloatProperty(field, record)) ? '' : me.formatter.number(value, format);
			};
		},

		renderColumn: function(field) {
			if (!field || Ext.isEmpty(field) || field.type === 'table' || field.type === 'info') return null;
			var fieldName = String(field.id).replace(/[\$,%]/g, ''),
				column = {
					align		: 'left',
					columnOrder	: field.columnOrder,
					dataIndex	: fieldName,
					filter		: true,
					hidden		: field.columnHidden,
					style		: { maxHeight: '2em', textAlign: 'center' },
					text		: field.name
				};
			if (field.columnWidth > 10) column.width = field.columnWidth;
			else column.flex = Math.abs(Math.round(field.columnWidth));
			switch (field.type) {
				case 'integer':
				case 'real':
					column = Ext.apply(column, {
						xtype	: 'numbercolumn',
						align	: 'right',
						editor	: { xtype: 'numberfield', selectOnFocus: true },
						renderer:  me[field.type + 'Renderer'](fieldName, field.mask)
					})
					break;
				case 'date':
				case 'datetime':
					column = Ext.apply(column, {
						xtype	: 'datecolumn',
						align	: 'center',
						editor	: { xtype: 'datefield', allowBlank: false, selectOnFocus: true },
						renderer: me.dateRenderer(fieldName, me.dateUtil.javaToExtFormat(field.mask) || me.dateUtil.patterns[field.type === 'datetime' ? 'LongDateTime' : 'ShortDate'])
					})
					break;
				case 'dictionary':
				case 'document':
				case 'sys_dictionary':
					column = Ext.apply(column, {
						editor	: {
						},
						renderer: me.dictionaryRenderer(fieldName)
					})
					break;
				case 'one_of_many':
					var data = (field.mask || '').split(';');
					for (var key = 0; key < data.length; key++) {
						data[key] = { id: key, name: data[key] };
					}
					column = Ext.apply(column, {
						editor	: {
						},
						filter	: {
							type		: 'list',
							dataType	: 'numeric',
							itemDefaults: { hideOnClick: true },
							labelField	: 'name',
							operator	: 'eq',
							store		: { type: 'named', data: data },
							single		: true 
						},
						renderer: me.oneOfManyRenderer(fieldName, field.mask)
					})
					break;
				case 'many_of_many':
					column = Ext.apply(column, {
						editor	: {
						},
						filter	: false,
						renderer: me.manyOfManyRenderer(fieldName, field.mask)
					})
					break;
				case 'boolean':
					column = Ext.apply(column, {
						editor	: {
						},
						renderer: me.booleanRenderer(fieldName, field.mask)
					})
					break;
				case 'text':
				case 'memo':
				default:
					column = Ext.apply(column, {
						editor	: {
						},
						renderer: me.defaultRenderer(fieldName, field.type === "memo" ? '4em' : false)
					});
					break;
			}
			return column;
		},

		renderColumns: function(form) {
			var columns = [], layoutFields = {},
				fields, layouts, data, i, j;
			if (form && form.isForm) {
				fields = form.get('fields');
				if (fields.length > 0) {
					layouts = form.get('layouts');
					for (i = 0; i < layouts.length; i++) {
						data = layouts[i].fields;
						for (j = 0; j < data.length; j++) {
							layoutFields[data[j].id || 0] = true;
						}
					}
					for (i = 0; i < fields.length; i++) {
						if (layoutFields.hasOwnProperty(fields[i].id)) {
							data = me.renderColumn(fields[i]);
							if (!Ext.isEmpty(data)) {
								columns.push(data);
							}
						}
					}
					columns.sort(me.columnSorter);
				}
			}
			return columns;
		},

		renderField: function(fieldCfg, panel, designer) {
			if (!fieldCfg) return null;
			var vm = panel && panel.lookupViewModel(),
				editor = {
					id		: [panel.id, fieldCfg.type, 'field', fieldCfg.id].join('-'),
					itemId	: [panel.id, fieldCfg.type, 'field', fieldCfg.id].join('-'),
					order	: fieldCfg.columnOrder || 0,
					readOnly: !fieldCfg.editable || fieldCfg.readOnly || (panel && panel.readOnly)
				};
			switch (fieldCfg.type || 'unknown') {
				case 'boolean':
					editor.xtype = 'checkbox';
					break;
				case 'date':
					Ext.apply(editor, {
						xtype		: 'datefield',
						altFormats	: CFJS.Api.dateTimeFormat,
						format		: me.dateUtil.javaToExtFormat(fieldCfg.mask) || me.dateUtil.patterns.NormalDate
					});
					break;
				case 'datetime':
					Ext.apply(editor, {
						xtype		: 'datetimefield',
						format		: CFJS.Api.dateTimeFormat,
						fieldTime	: { width: 90 }
					});
					break;
				case 'integer':
					editor.xtype = 'numberfield';
					editor.allowDecimals = false;
					if (!Ext.isEmpty(fieldCfg.mask)) editor.format = fieldCfg.mask;
					break;
				case 'real':
					editor.xtype = 'numberfield';
					if (!Ext.isEmpty(fieldCfg.mask)) editor.format = fieldCfg.mask;
					break;
				case 'text':
					editor.xtype = 'textfield';
					break;
				case 'dictionary':
				case 'document':
					Ext.apply(editor, {
						xtype		: 'referencecombobox',
						bind		: { value: '{fields.field_' + fieldCfg.id + '}' },
						listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
						loaderConfig	: {
							dictionaryType	: fieldCfg.type,
							formId			: (fieldCfg.reference || {}).id,
							period			: vm && vm.get('workingPeriod'),
							unit			: vm && vm.get('userUnit'),
							userInfo		: CFJS.lookupViewportViewModel().userInfo()
						},
						listeners	: {
							addrecord: function(field) {
								var form = fieldCfg.reference ? Ext.apply({ type: 'Form' }, fieldCfg.reference) : null,
									win = Ext.create({ xtype: 'document-window-object-window', viewModel: { form: form } });
								if (!win.autoShow) win.show();
							}
						}
					});
					me.bindMasterFilter(panel, editor, fieldCfg.masterFilter);
					break;
				case 'sys_dictionary':
					var dictionaryType = ((fieldCfg.reference || {}).name || '').toLowerCase(),
						queryParam;
					switch (dictionaryType) {
//					case 'company':
//						Ext.apply(editor, {
//							xtype				: 'dictionarypicker',
//							bind				: { value: '{fields.field_' + fieldCfg.id + '}' },
//							closeOnFocusLeave	: false,
//							publishes			: 'value',
//							rawValueData		: false,
//							selectorWindow		: { xtype: 'admin-window-company-picker' }
//						});
//						break;
//					case 'consumer':
//						Ext.apply(editor, {
//							xtype				: 'dictionarypicker',
//							bind				: { value: '{fields.field_' + fieldCfg.id + '}' },
//							closeOnFocusLeave	: false,
//							displayField		: 'fullName',
//							publishes			: 'value',
//							rawValueData		: false,
//							selectorWindow		: { xtype: 'admin-window-consumer-picker' }
//						});
//						break;
//					case 'customer':
//						Ext.apply(editor, {
//							xtype				: 'dictionarypicker',
//							bind				: { value: '{fields.field_' + fieldCfg.id + '}' },
//							closeOnFocusLeave	: false,
//							displayField		: 'fullName',
//							publishes			: 'value',
//							rawValueData		: false,
//							selectorWindow		: { xtype: 'admin-window-customer-picker' }
//						});
//						break;
						case 'consumer':
						case 'customer':
							queryParam = 'fullName';
							break;
						case 'order':
						case 'customer_id':
							queryParam = 'id';
						case 'city':
						case 'company':
						case 'company_account':
						case 'country':
						case 'currency':
						case 'id_type':
						default: break;
					};
					Ext.apply(editor, {
						xtype		: 'dictionarycombo',
						bind		: { value: '{fields.field_' + fieldCfg.id + '}' },
						listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
						pageSize	: 25,
						publishes	: 'value',
						queryParam	: queryParam,
						store		: {
							type		: 'named',
							proxy		: { loaderConfig: { dictionaryType: dictionaryType } },
							remoteSort	: queryParam !== 'fullName',
							sorters		: [ queryParam === 'id' ? queryParam : 'name' ]
						}
					});
					me.bindMasterFilter(panel, editor, fieldCfg.masterFilter);
					break;
				case 'table':
					Ext.apply(editor, {
						xtype		: 'document-view-objects-view',
						controller	: {
							type	: 'document.' + (editor.readOnly ? 'view' : 'edit'),
							editor	: 'document-panel-object-editor'
						},
						gridConfig	: {
							editable: !editor.readOnly,
							title	: fieldCfg.name
						},
						isTable		: true,
						viewModel	: { form: { type: 'Form', id: (fieldCfg.reference || {}).id } }
					});
					break;
				case 'memo':
					if (designer === true) {
						editor.xtype = 'textarea';
						editor.height = 150;
					} else editor.xtype = 'ckeditorfield';
					break;
				case 'many_of_many':
					Ext.apply(editor, {
						xtype	: 'manyofmanyfield',
						columns	: 1,
						format	: fieldCfg.mask,
						vertical: true
					});
					break;
				case 'one_of_many':
					Ext.apply(editor, {
						xtype	: 'oneofmanyfield',
						columns	: 1,
						format	: fieldCfg.mask,
						vertical: true
					});
					break;
				case 'graphics':
				case 'info':
				default:
					break;
			}
			if (fieldCfg.type !== 'table') {
				Ext.applyIf(editor, {
					bind		: { value: '{fields.field_' + fieldCfg.id + '.$}' },
					emptyText	: (fieldCfg.name || '').toLowerCase(),
					fieldLabel	: fieldCfg.name,
					name		: 'field-' + fieldCfg.id,
					required	: fieldCfg.required
				});
			}
			return editor;
		},

		renderFields: function(formCfg, panel) {
			var layouts = formCfg && formCfg.isForm ? formCfg.get('layouts') : [],
				fields = formCfg && formCfg.isForm ? formCfg.get('fields') : [],
				containers = [], fieldsMap = {}, i;
			for (i = 0; i < fields.length; i++) {
				fieldsMap[fields[i].id || 0] = fields[i];
			}
			for (i = 0; i < layouts.length; i++) {
				var	layout = layouts[i],
					layoutData = layout.layoutData || {},
					fieldAnchor = (layoutData.fieldAnchor||'').split(/[\s;,-]/, 2),
					columns = layoutData.columns || 1,
					editors = [], f = 0, editor, fieldData, measure,
					container = {
						xtype			: 'fieldset',
						border			: layout.borders,
						collapsible		: layout.collapsible,
						defaultType		: 'displayfield',
						fieldDefaults	: {
							hideLabel		: layoutData.hideLabels,	
							labelAlign		: (layoutData.labelAlign || 'left').toLowerCase(),
							labelPad		: layoutData.labelPad || 5,
							labelSeparator	: layoutData.labelSeparator || ':',
							labelWidth		: layoutData.labelWidth || 100,
						},
						items			: editors,
						layout			: {
							type		: 'table',
							columns		: columns,
							tableAttrs	: {
								border		: layoutData.border || 0,
								cellpadding	: layoutData.cellPadding || 0,
								cellspacing	: layoutData.cellSpacing || 0,
								style		: 'table-layout:fixed;width:100%;' + (layoutData.tableStyle || '')
							},
							tdAttrs		: {
								align	: (layoutData.cellHorizontalAlign || 'left').toLowerCase(),
								valign	: (layoutData.cellVerticalAlign || 'top').toLowerCase()
							}
						},
						order			: layout.setOrder || 0,
						padding			: layout.borders ? 10 : 0
					};
				fields = layout.fields || [];
				for (; f < fields.length; f++) {
					editor = me.renderField(fieldsMap[fields[f].id], panel);
					if (!editor) continue;
					fieldData = fields[f].layoutField || {};
					editors.push(editor = Ext.apply({
						cellCls: fieldData.styleName,
						colspan: fieldData.colspan || 1,
						rowspan: fieldData.rowspan || 1
					}, editor));
					if (fieldData.margin > 0) editor.margin = fieldData.margin;
					if (fieldData.padding > 0) editor.padding = fieldData.padding;
					measure = me.parseMeasure(fieldData.width, fieldAnchor[0]);
					if (measure) editor.width = measure;
					measure = me.parseMeasure(fieldData.height, fieldAnchor.length > 1 ? fieldAnchor[1] : null);
					if (measure) editor.height = measure;
					if (!Ext.isEmpty(fieldData.style)) editor.tdAttrs = { style: fieldData.style };
				};
				editors.sort(me.orderSorter);
				if (layout.hideHeader !== true) container.title = layout.name;
				containers.push(container);
			}
			containers.sort(me.orderSorter);
			return containers;
		},
		
		renderValue: function(type, value) {
			switch (type || 'unknown') {
				case 'date':
				case 'datetime':
					return { '@type': 'date', $: Ext.isDate(value) ? me.dateUtil.format(value, CFJS.Api.dateTimeFormat) : value };
				case 'integer':
				case 'many_of_many':
				case 'one_of_many':
					return { '@type': 'long', $: value ? value : 0 }
				case 'real':
					return { '@type': 'double', $: value ? value : 0 }
				case 'dictionary':
				case 'document':
				case 'sys_dictionary':
					return Ext.apply({'@type': 'named_entity'}, value ? new CFJS.model.NamedModel({ id: value.id, name: value.name }).getData() : value);
				case 'boolean':
					return { '@type': 'boolean', $: value === 'false' || value === false ? false : new Boolean(value).valueOf() }
				case 'text':
				case 'memo':
				case 'info':
				case 'graphics':
					return { '@type': 'string', $: value ? new String(value).valueOf() : '' }
				default: break; 
			}
		},
		
		reorderItems: function(newOrder, vm, store, itemField, orderField) {
			var oldOrder = vm.get(itemField + '.' + orderField),
				items = store && store.isStore && store.getRange(), i;
			if (items && items.length > 0) {
				if (newOrder > items.length) newOrder = items.length;
				if (oldOrder !== newOrder) {
					if (newOrder > oldOrder) {
						for (i = oldOrder; i < newOrder; i++) {
							items[i].set(orderField, i);
						}
					} else {
						for (i = newOrder; i < oldOrder; i++) {
							items[i - 1].set(orderField, i + 1);
						}
					}
					vm.set(itemField + '.' + orderField, newOrder);
					store.sort();
				}
			}
		},

		routeToObject: function(data, formType, routeId) {
			if (data && data.id > 0 && !Ext.isEmpty(formType) && routeId) {
				if (data.isModel) data = data.getData();
				if (data.form && data.form.id > 0) {
					var ft = [me.formTabPrefix, data.form.id].join(me.tokenSeparator),
						ot = [formType.toLowerCase(), data.id].join(me.tokenSeparator),
						token = [ routeId, ft, ot ].join(me.routeSeparator);
					if (Ext.util.History.getToken() !== token) Ext.util.History.add(token);
				}
			}
		}

	};
});