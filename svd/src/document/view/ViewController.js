Ext.define('CFJS.document.view.ViewController', {
	extend		: 'Ext.app.ViewController',
	alias		: 'controller.document.view',
	requires	: [ 'CFJS.euscp.Util', 'CFJS.model.FileModel' ],
	isReadOnly	: true,
	messages	: {
		notAllowCreateFText	: 'You are not allowed to create new documents of this type.',
		notAllowEditFText	: 'You are not allowed to edit documents of this type.',
		notAllowReadFText	: 'You are not allowed to view documents of this type.',
		notAllowEditDText	: 'You are not allowed to edit this document.',
		notAllowPrintDText	: 'You are not allowed to print this document.',
	    waitMsg				: 'Wait...',
	},
	
	applyGridStoreFilter: function(filter, suppressEvent) {
		var store = this.lookupGridStore();
		if (store && store.isStore && filter) {
			filter.id = filter.id || filter.property;
			if (filter.value) store.addFilter(filter, suppressEvent);
			else store.removeFilter(filter.id, suppressEvent);
		}
	},
	
	addStoreLoaderConfig: function(options) {
		var store = this.lookupGridStore(), proxy = store && store.isStore && store.getProxy();
		if (proxy && proxy.isCFJSProxy) {
			proxy.addLoaderConfig(options);
			store.reload();
		}
	},
	
	convertToPDF: function(documents, template, callback) {
		var me = this, api = CFJS.Api, len = documents.length,
			templateName = template.get('name'),
			count = 0, files = [], i = 0, document, url,
			makeCallback = function(fileName) {
				count++;
				return function(options, success, context) {
					count--;
					var request, error, file;
					if (success) {
						request = context.request;
						if (request && request.success && context.responseBytes) {
							file = new File([context.responseBytes], fileName, { type: context.getResponseHeader('Content-Type'), lastModified: new Date() });
							file.data = context.data;
							files.push(file);
						} else error = api.extractResponse(context);
					} else error = context.statusText;
					if (error) CFJS.errorAlert('Error downloading data', error);
					Ext.callback(count === 0 && callback, me, [files]);
				};
			};
		for (; i < len; i++) {
			if (me.isAllowed('print', document = documents[i]) && !Ext.isEmpty(url = template.buildPrintUrl(document))) {
				api.request({
					url					: url,
					binary				: true,
					disableExtraParams	: true,
					method				: 'GET',
					signParams			: false,
					callback			: makeCallback([document.get('name'), templateName, document.getId()].join('-') + '.pdf')
				});
			} else me.infoAlert(me.messages.notAllowPrintDText);
		}
		Ext.callback(count === 0 && callback, me, [files]);
	},
	
	createPKReader: function() {
		return picker;
	},
	
	createTemplatesMenu: function(templates, strictForms) {
		if (templates) {
			var items = [], i = 0, template, s;
			strictForms = strictForms || [];
			templates = Ext.Array.from(templates);
			strictForms = Ext.Array.from(strictForms);
			for (; i < templates.length; i++ ) {
				template = templates[i] || {};
				for (s = 0; s < strictForms.length; s++) {
					if (template.code === strictForms[s].printTemplate) {
						template.strictForm = strictForms[s];
						break;
					}
				}
				if (!template.isPrintTemplate) template = Ext.create('CFJS.model.document.FormTemplate', template);
				items.push({
					handler	: 'onExportRecord',
					iconCls	: template.iconCls(),
					scope	: this,
					template: template,
					text	: template.get('name')
				});
			}
			return items;
		}
	},
	
	disableComponent: function(component, disabled) {
		if (component && (component.isComponent || component.isAction)) component[disabled ? 'disable': 'enable']();
	},

	doSelectionChange: function(selectable, selection, actions) {
		var me = this, i = 0, canPrint = false, canSign = false, permissions,
			permissionLevel = me.getViewModel().getPermissionLevel();
		for (; i < selection.length; i++) {
			permissions = selection[i].actions(permissionLevel);
			canSign = canSign || (permissions.SIGN && !selection[i].hasSign());
			canPrint = canPrint || permissions.PRINT;
		}
		me.disableComponent(actions.printRecord, !canPrint);
		me.disableComponent(actions.exportRecord, !canPrint);
		me.disableComponent(actions.shareDocuments, !canPrint || !canSign);
		me.getView().doSelectionChange(selectable, selection, actions);
	},

	infoAlert: CFJS.euscp.Util.infoAlert,
	
	init: function(view) {
		var me = this;
		view.initializing = true;
		view.setReadOnly(me.isReadOnly || view.getReadOnly());
	},
	
	initActionable: function(actionable) {
		var me = this, view = me.getView(), visible,
			actions = actionable && actionable.hasActions && actionable.actions;
		if (actions) {
			me.initShareMenu(actions.shareDocuments, view.getPdfMenu());
			me.initTemplatesMenu(actions.exportRecord, view.getExcelMenu());
			me.initTemplatesMenu(actions.printRecord, view.getPdfMenu());
			if (actions.showProperties && view.propEditor) {
				visible = view.propEditor.isVisible();
				actions.showProperties.each(function(component) {
					if (Ext.isFunction(component.setPressed)) component.setPressed(visible);
				});
			}
			me.onSelectionChange(view.lookupGrid(), view.lookupGrid().getSelection());
		}
	},

	initShareMenu: function(action, menu) {
		if (action) {
			var actionMenu = action.initialConfig.menu, i, template, pdfItem;
			if (menu) {
				menu = Ext.clone(menu);
				if (menu.isPrintTemplate) {
					template = menu;
					menu = null;
				} else if (Ext.isArray(menu.items)) {
					for (i = 0; i < menu.items.length; i++) {
						menu.items[i].handler = 'onShareDocuments';
					}
				}
			}
			if (actionMenu && Ext.isArray(actionMenu.items)) {
				actionMenu = actionMenu.items;
				for (i = 0; i < actionMenu.length; i++) {
					if (actionMenu[i].type === 'pdf') {
						pdfItem = actionMenu[i];
						break;
					}
				}
			} else pdfItem = action.initialConfig;
			Ext.apply(pdfItem, { template: template, menu: menu });
			action.each(function(component) {
				var cmpMenu = component.getMenu();
				if (cmpMenu && cmpMenu.isMenu) {
					cmpMenu.items.each(function(item) {
						if (item.type === 'pdf') {
							item.template = template;
							if (Ext.isFunction(item.setMenu)) item.setMenu(menu, true);
						}
					});
				}
			});
		}
	},
	
	initTemplatesMenu: function(action, menu) {
		if (action) {
			var template;
			if (menu && menu.isPrintTemplate) {
				template = menu;
				menu = null;
			}
			Ext.apply(action.initialConfig, { template: template, menu: menu });
			action.each(function(component) {
				component.template = template;
				if (Ext.isFunction(component.setMenu)) component.setMenu(menu, true);
			});
		}
	},
	
	initViewModel: function(vm) {
		vm.bind('{form}', this.onFormChange, this, { single: true });
		vm.bind('{workingPeriod}', vm.onWorkingPeriodChange);
	},
	
	isAllowed: function(action, record) {
		return this.getViewModel().isAllowed(action, record);
	},

	lookupCurrentItem: function(record) {
		var me = this, record = me.lookupRecord(record), grid;
		if (!record && (grid = me.lookupGrid())) {
			record = grid.getLastSelected();
		}
		return record;
	},
	
	lookupEditor: Ext.emptyFn, 
		
	lookupForm: function() {
		return this.getViewModel().getForm();
	},
	
	lookupGrid: function() {
		return this.getView().lookupGrid();
	},

	lookupGridStore: function() {
		var grid = this.lookupGrid();
		return grid && grid.getStore();
	},
	
	lookupRecord: function(record) {
		if (!record) {
			var editor = this.lookupEditor();
			record = editor ? editor.getViewModel().getItem() : null;
		}
		return record;
	},
	
	onAddRecord: Ext.emptyFn,
	
	onChangeLoaderFilter: function(field, newValue, oldValue) {
		if ((newValue||{}).id !== (oldValue||{}).id) {
			var options = {};
			options[field.filterName] = newValue ? { id: newValue.id, name: newValue.name} : null;
			this.addStoreLoaderConfig(options);
		}
	},

	onClearFilters: function(component) {
		var grid = this.lookupGrid();
		if (grid && Ext.isFunction(grid.onClearFilters)) grid.onClearFilters();
	},
	
	onCopyRecord: Ext.emptyFn,
	
	onDownloadSignedFile: function(component, rowIndex, colIndex, item, event, record, row) {
		var me = this, record = me.lookupCurrentItem(record),
			files = record && record.isModel && Ext.Array.from(record.get('signedFiles')),
			items = [], i = 0, file;
		if (files && files.length > 0) {
			for (; i < files.length; i++) {
				file = new CFJS.model.FileModel(files[i]);
				items.push({
					href		: file.downloadUrl(null, { type: '/svd/print/pdf/' }),
					hrefTarget	: '_self',
					iconCls		: file.get('iconCls'),
					text		: file.get('name')
				});
			}
			Ext.create('Ext.menu.Menu', { items: items }).showAt(event.getXY());
		}
	},
	
	onEditRecord: Ext.emptyFn,
	
	onExportRecord: function(component) {
		var me = this, selection = me.lookupRecord(), grid,
			template = component ? component.template : null;
		if (template && template.isPrintTemplate) {
			if (!selection && (grid = me.lookupGrid())) selection = grid.getSelection();
			Ext.Array.each(selection, function(document, idx) {
				if (me.isAllowed('print', document)) {
					if (template.isStrict()) {
						grid.strictBlankData(template, function(series, number) {
							template.printStrict(document, null, series, number);
						});
					} else template.print(document);
				} else me.infoAlert(me.messages.notAllowPrintDText);
			}, me);
		}
	},

	onFormChange: function(form) {
		var me = this, view = me.getView(), pdfMenu, excelMenu;
		if (form && form.isForm) {
			pdfMenu = me.createTemplatesMenu(form.get('printTemplates'), form.get('strictForms'));
			excelMenu = me.createTemplatesMenu(form.get('exportTemplates'));
		}
		view.setPdfMenu(pdfMenu);
		view.setExcelMenu(excelMenu);
		delete view.initializing;
		me.initActionable(view.getActionable())
	},

	onGridDblClick: Ext.emptyFn,

	onPeriodPicker: function(component) {
		var view = this.lookupGrid();
		if (view && view.hasPeriodPicker) view.showPeriodPicker(component);
	},
	
	onReloadStore: function(component) {
		var store = this.lookupGridStore();
		if (store && store.isStore) {
			if (!store.isBufferedStore) store.rejectChanges();
			store.reload();
		}
	},
	
	onRemoveRecord: Ext.emptyFn,
	
	onSaveRecord: Ext.emptyFn,
	
	onSelectionChange: function(selectable, selection) {
		var me = this, actionable = me.getView().getActionable(), actions;
		if (actionable && (actions = actionable.actions)) {
			selection = selection || [];
			if (selection.isRows) selection = selection.getRecords();
			me.doSelectionChange(selectable, selection, actions);
		}
	},
	
	onShareDocuments: function(component) {
		if (component) {
			var me = this, grid = me.lookupGrid(),
				template = component.template,
				record = me.lookupRecord(),
				selection = Ext.Array.from(record ? record : (grid ? grid.getSelection() : null)); 
			me.getView().mask(me.messages.waitMsg);
			if (component.type === 'xml') {
				me.getView().unmask();
			} else if (template && template.isPrintTemplate) {
				me.convertToPDF(selection, template, function(files) {
					me.signFiles(files, function() {
						me.redirectTo('files');
					});
				}); 
			}
		}
	},
	
	onShowProperties: function(component) {
		var pressed = false, action = component && component.baseAction;
		if (action && action.isAction) {
			action.setPressed(pressed = component.isButton ? component.pressed : !component.pressed);
		}
		this.getView().showProperties(pressed);
	},
	
	onSignRecord: Ext.emptyFn,

	signFiles: function(files, callback) {
		var me = this, view = me.getView(), formData = new FormData(),
			i = 0, count = 0, fileName,
			makeCallback = function(file) {
				count++;
				return function(success, sign) {
					count--;
					if (success === true) {
						fileName = file.name.split(/(?:\.([^.]+))?$/, 2);
						fileName.splice(1, 0, 'p7s');
						file = new File([sign], fileName.join('.'), { type: file.type, lastModified: new Date() });
						file.data = sign;
						formData.append('upload', file, file.name);
					}
					if (count === 0) me.uploadFormData(formData, callback);
				};
			};
		if (Ext.isArray(files)) {
			CFJS.lookupViewportController().readPK(me, function(sign) {
				view.mask(me.messages.waitMsg);
				if (sign !== true || !CFJS.euscp.$sg.IsPrivateKeyReaded()) { 
					for (; i < files.length; i++) {
						formData.append('upload', files[i], files[i].name);
					}
					me.uploadFormData(formData, callback);
				} else {
					for (; i < files.length; i++) {
						CFJS.euscp.signFile({
							file		: files[i],
							saveToFile	: false,
							callback	: makeCallback(files[i])
						});
					}
				}
			});
		} else {
			view.unmask();
			Ext.callback(callback, me, [files]);
		}
	},
	
	uploadFormData: function(formData, callback) {
		var me = this, view = me.getView(), api = CFJS.Api,
			service = api.controller('webdav') || api.controller(api.getService());
		view.mask(me.messages.waitMsg);
		api.request({
			url		: service.url,
			headers	: { 'Content-Type': undefined },
			method	: 'POST',
			params	: { method: [service.method, 'upload'].join('.') },
			rawData	: formData,
			callback: function(options, success, response) {
				view.unmask();
				Ext.callback(callback, me, [options, success, response]);
			}
		});
	}

});