Ext.define('CFJS.document.view.EditController', {
	extend			: 'CFJS.document.view.ViewController',
	alias			: 'controller.document.edit',
	requires		: [ 'CFJS.document.util.Forms' ],
	isReadOnly		: false,
	config			: { editor: undefined, formType: undefined },
	listeners		: { beforesaverecord: 'onBeforeSaveRecord' },
	tokenSeparator	: CFJS.document.util.Forms.tokenSeparator,

	afterLoad: function(record) {
		var me = this, vm = me.getViewModel(),
			vf = me.getViewModel().getForm(), rf;
		if (record && record.isModel) {
			record.unjoin(me);
			rf = record.get('form') || {};
		}
		if (!vf || !rf || vf.id !== rf.id) me.maybeEditRecord(false);
	},

	applyEditor: function(editor) {
		return editor || 'container';
	},
	
	batchUpdate: function(component, operation, scope) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			record = me.lookupRecord(),
			selection = record ? [record] : (grid && !grid.readOnly ? grid.getSelection() : null),
			i = 0;
		if (selection && selection.length > 0) {
			me.disableComponent(component, true);
			for (; i < selection.length; i++) {
				operation.call(scope || me, selection[i], view, grid, i < selection.length - 1);
			}
		}
	},
	
	confirmRemove: function(fn) {
		return Ext.fireEvent('confirmlostdata', 'delete a selected records', fn, this);
	},
	
	doSelectionChange: function(selectable, selection, actions) {
		var me = this, i = 0, canCreate, canEdit, canPrint, canRemove, canSign, permissions,
			permissionLevel = me.getViewModel().getPermissionLevel(), admin = CFJS.isAdministrator();
		if (selection.length > 0) {
			permissions = selection[0].actions(permissionLevel);
			canEdit = permissions.WRITE;
			canCreate = permissions.WRITE && permissions.CREATE;
			for (; i < selection.length; i++) {
				permissions = selection[i].actions(permissionLevel);
				canRemove = canRemove || admin || (permissions.DELETE && !selection[i].readOnly());
				canSign = canSign || (permissions.SIGN && !selection[i].hasSign());
				canPrint = canPrint || permissions.PRINT;
			}
		}
		me.disableComponent(actions.removeRecord, !canRemove);
		me.disableComponent(actions.copyRecord, !canCreate);
		me.disableComponent(actions.editRecord, !canEdit);
		me.disableComponent(actions.signRecord, !canSign);
		me.disableComponent(actions.printRecord, !canPrint);
		me.disableComponent(actions.exportRecord, !canPrint);
		me.disableComponent(actions.shareDocuments, !canPrint || !canSign);
		me.getView().doSelectionChange(selectable, selection, actions);
	},
	
//	editorConfig: Ext.identityFn,

	initActionable: function(actionable) {
		var me = this, readOnly = me.getView().getReadOnly(),
			actions = actionable && actionable.hasActions && actionable.actions;
		if (actions) {
			actionable.hideComponent(actions.addRecord, !me.isAllowed('create') || readOnly);
			actionable.hideComponent(actions.copyRecord, !me.isAllowed('create') || readOnly);
			me.callParent(arguments);
		}
	},

	lookupEditor: function() {
		var me = this, view = me.getView();
		return view ? view.down(me.getEditor()) : null;
	},

	maybeEditRecord: function(record, add) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			editor = me.getEditor(),
			editing = view ? view.editing : false,
			activeItem, actions;
		if (grid && editor) {
			if (view.rendered) Ext.suspendLayouts();
			if (activeItem = view.down(editor)) view.remove(activeItem, true);
			if (record !== false && !grid.readOnly) {
				var selection = grid.getSelection(), 
					actionable = view.getActionable();
				if (!record && !add && selection && selection.length > 0) record = selection[0];
				activeItem = view.bodyAdd(Ext.apply({
					xtype		: editor,
					actionable	: view.getActionable(),
					isTable		: view.isTable,
					readOnly	: me.isReadOnly
				}, me.editorConfig));
				if (actions = activeItem.getActions()) {
					me.initTemplatesMenu(actions.printRecord, view.getPdfMenu());
					me.initTemplatesMenu(actions.exportRecord, view.getExcelMenu());
				}
				record = activeItem.getViewModel().setItem(record);
			} else activeItem = grid;
			view.editing = activeItem !== grid;
			view.setActiveItem(activeItem);
			if (view.rendered) Ext.resumeLayouts(true);
			if (editing !== view.editing) view.fireEvent('editingchange', view, view.editing, record);
			return record;
		}
	},
	
	onAddRecord: function(component) {
		var me = this;
		if (me.isAllowed('create')) me.maybeEditRecord(null, true);
		else me.infoAlert(me.messages.notAllowCreateFText);
	},
	
	onBackClick: function(component) {
		var me = this, 
			editor = me.lookupEditor(),
			record = editor ? editor.getViewModel().getItem() : null;
		if (record && (record.phantom || record.dirty)) {
			Ext.fireEvent('confirmlostdataandgo', function(btn) {
				if (btn === 'yes') {
					record.reject();
					if (!record.phantom) record.load();
					me.maybeEditRecord(false);
				}
			}, me);
		} else me.maybeEditRecord(false);
	},
	
	onBeforeSaveRecord: function(record) {
		var me = this, vm = me.getViewModel(),
			editor = me.lookupEditor(),
			toQueryString = Ext.Object.toQueryString,
			values = record && record.get('properties') || {},
			originalValues = (editor ? editor.getViewModel().getOriginalValues() : values) || {},
			dirty = record && !record.readOnly() && (record.phantom || toQueryString(values) !== toQueryString(originalValues)),
			properties = {}, i = 0, form, fields, field, value, key, type;
		if (dirty) {
			form = vm ? vm.getForm() : null;
			fields = form && form.isForm ? form.get('fields') : null;
			if (fields) {
				if (!Ext.isArray(fields)) fields = [fields];
				for (; i < fields.length; i++) {
					field = fields[i];
					if (!field) {
						fields.splice(i--, 1); 
						continue;
					}
					key = 'field_' + field.id;
					type = null;
					if (value = values[key]) {
						switch (field.type || 'unknown') {
							case 'integer': 
							case 'many_of_many': 
							case 'one_of_many': 
								type = 'long';
								break;
							case 'real':
								type = 'double';
								break;
							case 'date':
							case 'datetime':
								type = 'date';
								if (Ext.isDate(value['$'])) value['$'] = Ext.Date.format(value['$'], CFJS.Api.dateTimeFormat)
								break;
							case 'dictionary':
							case 'document':
							case 'sys_dictionary':
								type = 'named_entity';
								if (value.isModel) value = value.getData();
								delete value['$'];
								break;
							case 'boolean':
								type = 'boolean';
								if (Ext.isBoolean(value['$'])) value['$'] = value['$'].toString();
								break;
							case 'text':
							case 'memo':
								type = 'string';
								break;
							case 'info':
							default: break;
						}
					}
					value = Ext.applyIf({ '@type': type }, value, (field.properties||{}).field_defaultValue);
					if (value['@type'] && (value['$'] || value.id)) properties[key] = value;
				}
			} else {
				for (key in values) {
					if (value = values[key]) {
						if (value.id) value['@type'] = value['@type'] || 'named_entity';
						else if (Ext.isDate(value['$'])) value['$'] = Ext.Date.format(value['$'], CFJS.Api.dateTimeFormat);
						if (value['@type'] && (value['$'] || value.id)) properties[key] = value;
					}
				}
			}
			record.set('properties', properties);
		}
		return dirty;
	},
	
	onCopyRecord: function(component) {
		var me = this, curr = me.lookupCurrentItem(),
			allow = me.isTable && me.isAllowed('write') || me.isAllowed('create');
		if (allow) me.getViewModel().copyRecord(me.maybeEditRecord(null, true), curr);
		else me.infoAlert(me.messages.notAllowCreateFText);
	},
	
	onEditRecord: function(component) {
		var me = this;
		if (me.isAllowed('write')) me.maybeEditRecord();
		else me.infoAlert(me.messages.notAllowEditFText);
	},
	
	onGridDblClick: function(view, record, item, index, e, eOpts) {
		var me = this;
		if (me.isAllowed('write', record)) me.maybeEditRecord(record);
		else me.infoAlert(me.messages.notAllowEditDText);
	},
	
	onRefreshRecord: function(component) {
		var record = this.lookupRecord();
		if (record) {
			record.reject();
			if (!record.phantom) record.load();
		}
	},
	
	onRemoveRecord: function(component) {
		var me = this;
		me.confirmRemove(function(btn) {
			if (btn === 'yes') {
				me.disableComponent(component, true);
				me.batchUpdate(component, function(record, view, grid, hasNext) {
					var callback = function(record, operation, success) {
							me.onReloadStore(component);
							grid.getSelectionModel().deselectAll();
							if (view.editing) {
								view.showProperties(false);
								me.maybeEditRecord(false);
							}
							me.disableComponent(component);
						};
					if (!record.phantom && (!record.readOnly() || CFJS.isAdministrator()) && me.isAllowed('delete', record)) {
						record.erase(hasNext || { callback: callback });
					} else if (!hasNext) callback();
				});
			}
		});
		
	},
	
	onSaveRecord: function(component, force) {
		var me = this, record = me.lookupRecord();
		if (record && me.fireEvent('beforesaverecord', record) !== false && (record.dirty || record.phantom || force)) {
			me.disableComponent(component, true);
			record.save({
				params	: { locale: CFJS.getLocale() },
				callback: function(record, operation, success) {
					me.onReloadStore(component);
					me.disableComponent(component);
				}
			});
		}
	},
	
	onSignRecord: function(component) {
		var me = this;
		me.batchUpdate(component, function(record, view, grid, hasNext) {
			var callback = function(record, operation, success) {
					me.onReloadStore(component);
				}, 
				sign = function(record, operation) {
					record.sign({
						params: { locale: CFJS.getLocale() },
						callback: !hasNext && callback
					});
				};
			if (record && !record.hasSign()) {
				if (me.fireEvent('beforesaverecord', record) !== false) {
					record.save({
						params	: { locale: CFJS.getLocale() },
						success	: sign
					});
				} else sign(record);
			} else if (!hasNext) callback();
		});
	},
	
	setToken: function(token) {
		var me = this, vm = me.getViewModel(), form = vm.get('form'),
			store = me.lookupGridStore(), id, record;
		if (store && store.isStore) {
			if (!form || !form.isModel) {
				vm.bind('{form}', function(form) {
					if (form && form.id > 0) me.setToken(token);
					else me.infoAlert(me.messages.notAllowReadFText);
				}, me, { single: true });
				return;
			}
			if (store.isLoading() || !store.isLoaded()) {
				store.on({ load: function(store, records, successful) { if (successful) me.setToken(token); }, single: true });
				return;
			}
			if (me.isAllowed('read')) {
				if ((id = CFJS.safeParseInt((token||'').replace(me.formType + me.tokenSeparator, ''))) > 0) {
					if (!(record = store.getById(id))) record = { id: id };
				}
				record = record && (editor = me.lookupEditor()) ? editor.getViewModel().setItem(record) : me.maybeEditRecord(record ? record : false);
				if (record && record.isModel) record.join(me);
			} else me.infoAlert(me.messages.notAllowReadFText);
		}
	},
	
	token: function(record) {
		if (record && record.id > 0) {
			var me = this, token = [];
			if (me.formType) token.push(me.formType);
			token.push(record.id);
			return token.join(me.tokenSeparator) || null;
		}
	}

});