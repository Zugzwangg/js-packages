Ext.define('CFJS.document.view.ObjectPropertiesModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.document.properties',
	requires	: [
		'CFJS.model.document.*',
		'CFJS.model.project.Process',
		'CFJS.store.Enumerated',
		'CFJS.store.document.DocumentResources',
		'CFJS.store.document.Forms',
		'CFJS.store.document.Permissions',
		'CFJS.store.project.ProcessPlans'
	],
	isDetail	: true,
	itemName	: 'propertiesObject',
	itemModel	: 'CFJS.model.document.Document',
	data		: {
		hasProcess	: false,
		process		: null
	},
	formulas	: {
		allowed: {
			bind: '{_itemName_.permissions}',
			get	: function(permissions) {
				var me = this,
					level = me.getPermissionLevel(),
					form = me.getForm(),
					document = me.getItem();
				if (document && document.isDocument) return document.actions(level);
				return  form && form.isForm ? form.actions(level) : {};
			}
		},
		canChangeProcessPlan: {
			bind: '{_itemName_.author}',
			get	: function(author) {
				var form = this.get('form');
				return form && form.hasWorkflow() && CFJS.lookupViewportViewModel().isCurrentUser(author);
			}
		},
		canCreateProcessPlan: {
			bind: [ '{hasProcess}', '{canChangeProcessPlan}'],
			get	: function(data) { return !data[0] && data[1]; }
		},
		phase: {
			bind: '{process.currentPhase}',
			get	: function(phase) {
				return (((phase||{}).properties||{}).field_type||{}).$
			}
		}
	},
	stores		: {
		documentActions		: { type: 'enumerated', data: CFJS.document.util.Forms.documentActions },
		documentResources	: {
			type	: 'document_resources',
			autoLoad: true,
			session	: { schema: 'document' },
			filters	: [{
				id		: 'document',
				property: 'document.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			}],
			sorters	: [ 'name' ]
		},
		objectPermissions	: {
			data	: '{_itemName_.permissions}',
			fields	: [
				{ name: 'id',		persist: false },
				{ name: 'action',	type: 'string' },
				{ name: 'level',	type: 'int' }
			]
		},
		principleTypes		: { source: Ext.getStore('principleTypes') },
		signatories			: {
			data	: '{_itemName_.signatures}',
			model	: 'CFJS.model.document.DigitalSignature'
		}
	},
	
	saveProcess: function(vm, callback) {
		var me = this, process = vm.getItem();
		if (process && process.isModel) {
			vm.each(function(key, child) {
				if (child && child.isPlanDesigner) {
					me.saveProcessPlan(child, function(record, operation, success) {
						if (success && (process.dirty || process.phantom)) process.save({ callback: callback });
						else Ext.callback(callback, arguments);
					});
					return false;
				}
			}, me);
		} else Ext.callback(callback);
	},
	
	saveProcessPlan: function(vm, callback) {
		var store = vm.getStore('processPlans'), i = 0,
			records = store && store.isStore && store.getData().items;
		if (records && vm.beforeSaveProcessPlan(records)) {
			for (; i < records.length; i++) {
				records[i].erase(i < records.length - 1 || { callback: callback });
			}
		} else Ext.callback(callback);
	},
	
	beforeSaveProperties: function(record) {
		if (record && record.isModel) {
			var me = this;
			me.updateArrayField(record, 'permissions', 'objectPermissions');
			me.updateArrayField(record, 'signatures');
			me.updateModelField(record, 'sector');
			me.updateModelField(record, 'unit');
			me.updateModelField(record, 'process');
			me.each(function(key, child) {
				if (child && child.isPrincipleEdit) {
					me.saveProcess(child);
					return false;
				}
			}, me);
		}
		return record && record.isModel;
	},
	
	canDeleteResource: function(record, userInfo) {
		if (record && record.isEntity && record.isAuthor(userInfo)) {
			var me = this, parent = me.getParent();
			return parent && parent.isAllowed('write', me.getItem()); 
		}
		return false;
	},
	
	createProcess: function(workflow, lifePhases, callback) {
		var me = this,
			process = me.setProcess(),
			document = me.getItem(),
			processEdit;
		if (document && process) me.each(function(key, child) {
			if (child && child.isPrincipleEdit) {
				processEdit = child;
				return false;
			}
		}, me);
		if (processEdit) {
			processEdit.createProcessPlan(workflow, lifePhases, function() {
				document.set('process', { id: process.getId(), name: process.get('name') });
				document.save({
					callback: function(records, operation, success) {
						if (success) processEdit.each(function(key, child) {
							if (child && child.isPlanDesigner) {
								child.refreshProcessPlan();
								child.startProcess(callback);
								return;
							}
						});
						else Ext.callback(callback);
					}
				});
			});
		} else Ext.callback(callback);
	},
	
	getForm: function() {
		return this.get('form');
	},
	
	getPermissionLevel: function() {
		return this.get('permissionLevel');
	},

	initConfig: function() {
		var me = this;
		me.callParent(arguments);
		me.bind('{_itemName_.process}', me.setProcess);
	},
	
	setItem: function(item) {
		return this.linkItemRecord(item);
	},
	
	setProcess: function(process) {
		var me = this, processEdit = { setItem: Ext.identityFn },
			session = me.getSession(), cached, 
			setProcess = function(record) {
				processEdit.setItem(record);
				me.set('hasProcess', record && record.isModel);
			};
		me.each(function(key, child) {
			if (child && child.isPrincipleEdit) {
				processEdit = child;
				return false;
			}
		}, me);
		if (process) {
			if (process.id > 0) {
				cached = session && session.peekRecord('CFJS.model.project.Process', process.id);
				me.linkTo('process', { type: 'CFJS.model.project.Process', id: process.id });
				process = me.get('process');
				if (cached) setProcess(process)
				else process.join({
					afterLoad: function(record) {
						record.unjoin(this);
						setProcess(record);
					}
				});
				return process;
			}
		} else if (processEdit.isPrincipleEdit) {
			me.linkTo('process', { type: 'CFJS.model.project.Process', create: processEdit.setItem('PROCESS') });
			setProcess(process = me.get('process'));
			return process;
		}
		me.set({ hasProcess: false, process: process = processEdit ? processEdit.setItem(false) : null });
		return process;
	},
	
	privates: {
	
		itemLoadConfig: function(item) {
			return { form: (this.getForm()||{}).id };
		}
	
	}
	
});