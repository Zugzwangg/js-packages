Ext.define('CFJS.document.view.FormDesignerModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.document.form-designer',
	requires	: [
		'CFJS.document.util.Forms',
		'CFJS.model.document.Form',
		'CFJS.model.document.Permission',
		'CFJS.store.document.StrictForms'
	],
	config		: { form: null },
	data		: {
		field		: null,
		layout		: null,
		layoutField	: null
	},
	formulas	: {
		defaultValue: {
			bind: '{field.properties}',
			get	: function(properties) {
				var value = (properties || {}).field_defaultValue;
				if (value) {
					if (!Ext.isObject(value)) value = null;
					else if (value['$']) value = value['$'];
				}
				return value;
			},
			set	: function(value) {
				this.set('field.properties', value ? { field_defaultValue: CFJS.document.util.Forms.renderValue(this.get('fieldType'), value) } : null);
			}
		},
		columnOrder: {
			bind: '{field.columnOrder}',
			get	: function(order) { return order; },
			set	: function(order) { this.reorderFields(order); }
		},
		fieldType: {
			bind: '{field.type}',
			get	: function(type) { return type; },
			set	: function(type) {
				var me = this, field = me.get('field'), map = me.get('fieldsMap');
				field.type = type || 'text';
				if (!map.aggregation) field.aggregation = 'NONE';
				if (!map.formula) delete field.formula;
				if (!map.mask) delete field.mask;
				if (!map.masterFilter) delete field.masterFilter;
				delete field.properties;
				me.set('field', field);
			}
		},
		fieldsMap: {
			bind: '{fieldType}',
			get	: function(type) { return CFJS.document.util.Forms.fieldPropertiesMap(type); }
		},
		fieldsSearch: {
			bind: '{fields}',
			get	: function(fields) {
				return {
					xtype		: 'multiselector-search',
		            height		: 200,
					grid		: { columns: [{ align: 'left', dataIndex: 'name', flex: 1 }], hideHeaders: true },
					searchText	: 'Select fields',
		            store		: fields,
		            width		: 200
				}
			}
		},
		layoutData: {
			bind: '{layout}',
			get	: function(layout) {
				var me = this, layoutFields;
				Ext.TaskManager.start({ fireOnStart: false, interval: 50, repeat: 1,
					run: function() {
						if ((layoutFields = me.getStore('layoutFields')) && layoutFields.isStore) layoutFields.sort('order', 'ASC');
					}
				});
				return (layout||{}).layoutData; 
			}
		},
		setOrder: {
			bind: '{layout.setOrder}',
			get	: function(order) { return order; },
			set	: function(order) { this.reorderLayouts(order) }
		}
	},
	itemName	: 'form',
	itemModel	: 'CFJS.model.document.Form',
	stores		: {
		fields			: {
			data	: '{_itemName_.fields}',
			model	: 'CFJS.model.document.Field',
			sorters	: [ 'columnOrder' ]
		},
		layouts			: {
			data	: '{_itemName_.layouts}',
			model	: 'CFJS.model.document.Layout',
			sorters	: [ 'setOrder' ]
		},
		layoutFields	: {
			data	: '{layout.fields}',
			model	: 'CFJS.model.document.LayoutField',
			sorters	: [ 'order' ]
		},
		formPermissions	: {
			data	: '{_itemName_.permissions}',
			fields	: [
				{ name: 'action', type: 'string' },
				{ name: 'level', type: 'int' }
			]
		},
    	sectors			: {
        	type	: 'named',
			autoLoad: true,
        	pageSize: 0,
			proxy	: { loaderConfig: { dictionaryType: 'sector_by_post' } },
			filters	: [
				{ id: 'post',	property: 'post.id',	type: 'numeric',	operator: 'eq', 	value: '{userInfo.id}' },
				{ id: 'month',	property: 'month',		type: 'date',		operator: '!after', value: Ext.Date.getFirstDateOfMonth(new Date()).getTime() }
			],
			sorters	: [ 'sector.name' ]
    	},
    	strictForms		: { type: 'strictforms', autoLoad: true, sorters: [ 'name' ] },
    	workflows		: {
			type	: 'named',
			autoLoad: true,
        	pageSize: 0,
			proxy	: { loaderConfig: { dictionaryType: 'workflow' } },
			sorters	: [ 'name' ]
		}
	},

	beforeSaveForm: function(form) {
		var me = this, canSave = false, data;
		if (form && form.isModel) {
			me.updateArrayField(me.get('layout'), 'fields', 'layoutFields');
			me.updateArrayField(form, 'permissions', 'formPermissions');
			me.updateArrayField(form, 'layouts');
			me.updateArrayField(form, 'fields', function(field) {
				return field && Ext.apply(field, CFJS.document.util.Forms.fieldOptions(field.type));
			});
			form.setProperty('personId', { '@type': 'long', '$': me.get('userInfo.id') });
			data = form.getData();
			canSave = !!data && !Ext.isEmpty(data.name) && !!data.fields && !!data.layouts && !!data.permissions;
		}
		return canSave;
	},
	
	findField: function(id) {
		var fields = this.getStore('fields'); 
		return fields && fields.getById(id);
	},
	
	reorderFields: function(order) {
		CFJS.document.util.Forms.reorderItems(order, this, this.getStore('fields'), 'field', 'columnOrder');
	},

	reorderLayouts: function(order) {
		CFJS.document.util.Forms.reorderItems(order, this, this.getStore('layouts'), 'layout', 'setOrder');
	},
	
	checkFields: function(form, field, store) {
		if (form) {
			var field = 'fields', fields = this.getStore(field), data = [];
			if (fields && fields.isStore) {
				fields.commitChanges();
				fields.each(function(record) {
					data.push(record.getData());
				});
				if (form.isModel) form.set(field, data, { silent: true });
				else form[field] = data;
			}
		}
		return form;
	},
	
	updateFormData: function() {
		var me = this, form = me.getForm();
		if (form && form.isModel) {
			me.updateArrayField(me.get('layout'), 'fields', 'layoutFields');
			me.updateArrayField(form, 'permissions', 'formPermissions');
			me.updateArrayField(form, 'layouts');
			me.updateArrayField(form, 'fields', function(field) {
				return field && Ext.apply(field, CFJS.document.util.Forms.fieldOptions(field.type));
			});
			form.setProperty('personId', { '@type': 'long', '$': me.get('userInfo.id') });
		}
		return form;
	},
	
	privates: {
		
		configItem: function(item) {
			if (item && item.isForm) {
				if (item.id > 0) {
					item.setTarget('design');
					return item;
				}
				item = item.getData();
			}
			return { type: this.getItemModel(), create: item || true };
		},

		updateForm: function(form) {
			this.setItemName('form' + Ext.id(form));
			this.setItem(form);
		}
		
	}
	
});