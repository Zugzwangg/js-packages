Ext.define('CFJS.document.view.FormsBrowserModel', {
	extend	: 'Ext.app.ViewModel',
	alias	: 'viewmodel.document.forms-browser',
	requires: [
		'CFJS.document.util.Forms',
		'CFJS.store.Enumerated',
		'CFJS.store.document.Forms',
		'CFJS.store.document.FormGroups',
		'CFJS.store.document.Permissions'
	],
	data	: {
		type	: null
	},
	stores	: {
		aggregations	: { type: 'enumerated', data: CFJS.document.util.Forms.aggregations },
		documentActions	: { type: 'enumerated', data: CFJS.document.util.Forms.documentActions },
		fieldTypes		: { type: 'enumerated', data: CFJS.document.util.Forms.fieldTypes },
		formGroups		: { type: 'formgroups', pageSize: 0, remoteSort	: false },
		formTypes		: { type: 'enumerated', data: CFJS.document.util.Forms.formTypes },
		hAlign			: { type: 'enumerated', data: CFJS.document.util.Forms.hAlign },
		lAlign			: { type: 'enumerated', data: CFJS.document.util.Forms.lAlign },
		permissions		: { type: 'permissions', autoLoad: true },
		references		: {
			type	: 'forms',
			filters	: [{ id: 'permissions', type: 'numeric', property: 'permissions[\'READ\']', operator: '!lt', value: '{permissionLevel}' } ]
		},
		sysDictionaries	: { type: 'enumerated', data: CFJS.document.util.Forms.sysDictionaries },
		vAlign			: { type: 'enumerated', data: CFJS.document.util.Forms.vAlign }
	},
	
	createFormModel: function (type, name) {
		var me = this, actions = me.getStore('documentActions'),
			permissions = [];
		if (actions && actions.isStore) {
			actions.each(function(action) {
				permissions.push({ action: action.getId(), level: CFJS.shortMax });
			});
		}
		return new CFJS.model.document.Form({
			checkUnit	: true,
			height		: 600,
			locale		: CFJS.getLocale(),
			name		: name,
			permissions	: permissions,
			startDate	: new Date(),
			type		: type || 'DOCUMENT',
			unit		: me.get('userUnit'),
			width		: 800
		});
	}

});