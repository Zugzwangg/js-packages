Ext.define('CFJS.document.view.WindowController', {
	extend		: 'CFJS.document.view.EditController',
	alias		: 'controller.document.window',
	id			: 'windowDocument',

	init: function(view) {
		var me = this, actions = view && view.hasActions && view.actions;
		view.initializing = true;
		view.setReadOnly(me.isReadOnly);
		if (actions) {
			view.hideComponent(actions.saveRecord, me.isReadOnly);
			view.hideComponent(actions.signRecord, me.isReadOnly);
		}
	},

	initViewModel: function(vm) {
		vm.bind('{form}', this.onFormChange, this, { single: true });
	},
	
	lookupEditor: function() {
		return this.getView().editor;
	},
	
	lookupGrid: Ext.emptyFn,
	
	onAddRecord: Ext.emptyFn,
	
	onBeforeSaveRecord: function(record) {
		var me = this, editor = me.lookupEditor();
		return editor && editor.isValid() && me.callParent(arguments);
	},

	onCopyRecord: Ext.emptyFn,
	
	onClearFilters: Ext.emptyFn,
	
	onEditRecord: Ext.emptyFn,

	onExportRecord: Ext.emptyFn,
	
	onFormChange: function(form) {
		var me = this, record = me.lookupRecord();
		if (record && record.isDocument) {
			if (form && form.isModel) form = form.getData();
			if (form) form = { id: form.id, name: form.name };
			record.set('form', form);
		}
	},

	onGridDblClick: Ext.emptyFn,
	
	onReloadStore:  Ext.emptyFn,
	
	onRemoveRecord: Ext.emptyFn,

	onSelectionChange: Ext.emptyFn,
	
	onShareDocuments: Ext.emptyFn

});