Ext.define('CFJS.document.view.EditModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.document.edit',
	requires	: [ 'CFJS.model.document.*', 'CFJS.store.document.Documents' ],
	itemName	: undefined,
	itemModel	: undefined,
	formulas	: {
		allowed: {
			bind: '{_itemName_.permissions}',
			get	: function(permissions) {
				var me = this, level = me.getPermissionLevel(),
					form = me.getForm(), document = me.getItem(),
					actions = form && form.isForm ? form.actions(level) : {};
				return document && document.isDocument ? Ext.apply(actions, document.actions(level)) : actions;
			}
		},
		editable: {
			bind: '{_itemName_.signatures}',
			get	: function(signatures) {
				return !(Ext.Array.from(signatures).length > 0);
			}
		},
		fields: {
			bind: '{_itemName_.properties}',
			get	: function(properties) {
				this.setOriginalValues(Ext.clone(properties));
				return properties;
			}
		}
	},
	
	defaultItemConfig: function() {
		var me = this, parent = me.getParent(), form = parent ? parent.getForm() : me.getForm(), 
			config = {
				author		: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
				unit		: me.get('userUnit'),
				startDate	: new Date(),
				created		: new Date()
			};
		if (form && form.isModel) form = form.data;
		if (form) form = { id: form.id, name: form.name };
		config.form = form;
		if (parent) {
			if (Ext.isFunction(parent.getParentId)) config.parentId = parent.getParentId();
			if (Ext.isFunction(parent.getFormDefaults)) config.properties = parent.getFormDefaults();
		}
		return config;
	},

	getForm: function() {
		return this.get('form');
	},
	
	getOriginalValues: function() {
		return this.get(this.getItemName() + '-original-values');
	},

	getPermissionLevel: function() {
		return this.get('permissionLevel');
	},

	setItem: function(item) {
		return this.linkItemRecord(item);
	},
	
	setOriginalValues: function(values) {
		return this.set(this.getItemName() + '-original-values', values);
	},

	privates: {
		
		itemLoadConfig: function(item) {
			return { form: (this.getForm()||{}).id };
		}

	}
	
});
