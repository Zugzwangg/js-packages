Ext.define('CFJS.document.view.ViewModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.document.view',
	requires	: [ 'CFJS.model.document.*', 'CFJS.store.document.Documents', 'CFJS.store.document.Permissions' ],
	config		: { 
		form			: null,
		parentDocument	: null,
		storeName		: 'documents'
	},
	itemName	: 'document-form',
	itemModel	: 'CFJS.model.document.Document',
	stores		: {
		permissions	: { type: 'permissions', autoLoad: true },
		sectors		: {
			type		: 'basetree',
			pageSize	: 0,
			parentFilter: 'sector.parent.id',
			proxy		: {
				service		: 'svd',
				loaderConfig: { dictionaryType: 'sector_by_post' },
				reader		: { rootProperty: 'response.transaction.list.sector' }
			},
			filters		: [{
				id		: 'post',
				property: 'post.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{userInfo.id}'
			},{
				id		: 'month',
				property: 'month',	
				type	: 'date',
				operator: '!after',
				value	: Ext.Date.getFirstDateOfMonth(new Date()).getTime()
			}],
			sorters	: [ 'sector.name' ]
		},
		units		: {
			type		: 'basetree',
			pageSize	: 0,
			parentFilter: 'parent.id',
			proxy		: {
				service		: 'svd',
				loaderConfig: { dictionaryType: 'unit' },
				reader		: { rootProperty: 'response.transaction.list.unit' }
			},
			root	: '{userUnit}',
			sorters	: [ 'name' ]
		}
	},

	applyStoreName: function(storeName) {
		return storeName || 'documents';
	},

	copyRecord: function(dest, src) {
		if (dest && dest.isModel && src && src.isModel) {
			var me = this, prop = dest.get('properties');
			src = src.get('properties');
			me.eachField(function(field, i, fields) {
				if (field.notCopy !== true) {
					prop['field_' + field.id] = Ext.clone(src['field_' + field.id]);
				}
			});
			dest.set('properties', prop);
		}
		return dest;
	},
	
	eachField: function(fn, scope) {
		var me = this, form = me.getForm();
		Ext.each(form && form.isForm ? form.get('fields') : null, fn, scope || me);
	},
	
	getForm: function() {
		var me = this, stub = me.getStub('form');
		if (stub && !stub.isLoading()) me._form = stub.getValue();
		return me._form;
	},
	
	getFormDefaults: function() {
		var me = this, defaults = {}, defaultValue;
		me.eachField(function(field, i, fields) {
			if (!(defaultValue = (field.properties || {}).field_defaultValue)) {
				switch (field.type || '') {
					case 'integer':
					case 'real':
					case 'one_of_many':
						defaultValue = { '@type': field.type, $: 0 }
						break;
					case 'date':
					case 'datetime':
						defaultValue = { '@type': field.type, $: Ext.Date.format(new Date(), CFJS.Api.dateTimeFormat) }
						break;
					default:break;
				}
			};
			if (defaultValue) defaults['field_' + field.id] = defaultValue;
		});
		return defaults;
	},
	
	getParentId: function() {
		var store = this.getDocumentsStore(),
			proxy = store && store.isStore ? store.getProxy() : null,
			loaderConfig = proxy && proxy.isSvdProxy ? proxy.getLoaderConfig() : null;
		return (loaderConfig||{}).parentId;
	},
	
	getPermissionLevel: function() {
		return this.get('permissionLevel');
	},
	
	getDocumentsStore: function() {
		return this.get(this.getStoreName());
	},

	isAllowed: function(action, record) {
		var me = this, permissionLevel = me.getPermissionLevel(), form = me.getForm();
		if (record && record.isDocument) return record.isAllowed(action, permissionLevel);
		return form && form.isForm ? form.isAllowed(action, permissionLevel) : false;
	},
	
	onWorkingPeriodChange: function(period) {
		var documents = this.getDocumentsStore();
		if (documents && documents.isStore && !documents.hasPendingLoad()) documents.reload();
	},

	updateForm: function(form) {
		this.linkTo('form', form);
		this.setItemName('document-form' + (form ? form.id : '0'));
	},

	privates: {
		
		getRecord: function(type, id) {
            var record = this.callParent(arguments);
            if (!record.getTarget()) record.setTarget('editor', true);
            return record;
        },
        
		parseDescriptor: function(descriptor) {
			return this.callParent([descriptor.replace('_storeName_', this.getStoreName())]); 
		}

	}
	
});
