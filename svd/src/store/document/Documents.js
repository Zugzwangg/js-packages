Ext.define('CFJS.store.document.Documents', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.documents',
	requires: [ 'CFJS.api.proxy.SvdJson' ], 
	model	: 'CFJS.model.document.Document',
	storeId	: 'documents',
	proxy	: 'json.svdapi'
});
