Ext.define('CFJS.store.document.Fields', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.fields',
	requires: [ 'CFJS.api.proxy.SvdJson' ], 
	model	: 'CFJS.model.document.Field',
	storeId	: 'fields',
	config	: {
		proxy : {
			service		: 'svd',
			loaderConfig: { dictionaryType: 'field_config' },
			reader		: { rootProperty: 'response.transaction.list.field_data' }
		}
	},
	sorters	: [ 'name' ]
});
