Ext.define('CFJS.store.document.StrictBlankStates', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.strictblankstates',
	storeId	: 'strictBlankStates',
	data	: [
		{ id: 'BLANK',		name: 'Blank' },
		{ id: 'CANCELED',	name: 'Canceled' },
		{ id: 'DAMAGED',	name: 'Damaged' },
		{ id: 'PRINTED',	name: 'Printed' }
	]
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'strictblankstates' }));
});