Ext.define('CFJS.store.document.Forms', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.forms',
	requires: [ 'CFJS.api.proxy.SvdJson' ], 
	model	: 'CFJS.model.document.Form',
	storeId	: 'forms',
	config	: {
		proxy : {
			service		: 'svd',
			loaderConfig: { dictionaryType: 'form' },
			reader		: { rootProperty: 'response.transaction.list.form_data' }
		}
	},
	sorters	: [ 'name' ]
});
