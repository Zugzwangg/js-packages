Ext.define('CFJS.store.document.FormTemplates', {
	extend		: 'CFJS.store.Base',
	alias		: 'store.formtemplates',
	model		: 'CFJS.model.document.FormTemplate',
	storeId		: 'formTemplates',
	remoteFilter: false,
	remoteSort	: false,
	sortOnLoad	: true,
	sorters		: 'code',
	config		: {
		form		: undefined,
		document	: undefined,
		templateType: undefined,
		proxy		: {
			api		: { read	: { method: 'load.form_templates', transaction: 'load' } },
			service	: 'svd',
			reader	: { rootProperty: 'response.transaction.template' }
		}
	},
	
	listeners	: {
		beforeload: function(store, operation) {
			return (loaderConfig = operation.getProxy().loaderConfig) && loaderConfig.id > 0;
		}
	},

	privates: {
		
		applyTemplateType: function(type) {
			this.getProxy().setExtraParam('type', type);
		},
		
		applyDocument: function(document) {
			return this.checkObject(document);
		},
		
		applyForm: function(document) {
			return this.checkObject(document);
		},
		
		checkObject: function(obj) {
			if (Ext.isObject(obj)) obj = obj.id;
			return obj;
		},
		
		updateObject: function(obj, old, form) {
			var me = this, proxy = me.getProxy();
			proxy.addLoaderConfig({ id: obj });
			proxy.api.read.method = ['load.', form, 'templates'].join('');
			me.load();
		},

		updateDocument: function(document, old) {
			if (document && document > 0) {
				this.setForm(null);
				this.updateObject(document, old);
			}
		},

		updateForm: function(form, old) {
			if (form && form > 0) {
				this.setDocument(null);
				this.updateObject(form, old, 'form_');
			}
		}

	}
});
