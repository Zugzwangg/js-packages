Ext.define('CFJS.store.document.FormGroups', {
	extend		: 'CFJS.store.BaseTree',
	alias		: 'store.formgroups',
	requires	: [ 'CFJS.api.proxy.SvdJson' ], 
	model		: 'CFJS.model.document.FormGroup',
	parentFilter: 'parent.id',
	storeId		: 'formGroups',
	config		: {
		proxy : {
			service		: 'svd',
			loaderConfig: { dictionaryType: 'form_group' },
			reader		: { rootProperty: 'response.transaction.list.form_group' }
		}
	},
	sorters		: [ 'name' ]
});
