Ext.define('CFJS.store.document.StrictBlanks', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.strictblanks',
	model	: 'CFJS.model.document.StrictBlank',
	storeId	: 'strictBlanks',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'strict_blank' },
			reader	: { rootProperty: 'response.transaction.list.strict_blank' }
		}
	}
});
