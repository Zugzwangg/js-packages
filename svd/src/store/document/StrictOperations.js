Ext.define('CFJS.store.document.StrictOperations', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.strictoperations',
	model	: 'CFJS.model.document.StrictOperation',
	storeId	: 'strictOperations',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'strict_operation' },
			reader	: { rootProperty: 'response.transaction.list.strict_operation' }
		}
	}
});
