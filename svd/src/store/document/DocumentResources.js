Ext.define('CFJS.store.document.DocumentResources', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.document_resources',
	model	: 'CFJS.model.document.DocumentResource',
	storeId	: 'documentResources',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'document_resource' },
			reader		: { rootProperty: 'response.transaction.list.document_resource' },
			service		: 'svd'
		}
	}
});
