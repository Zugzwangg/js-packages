Ext.define('CFJS.store.document.StrictOperationTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.strictoperationtypes',
	storeId	: 'strictOperationTypes',
	data	: [
		{ id: 'ISSUE',			name: 'Issue' },
		{ id: 'CANCELLATION',	name: 'Cancellation' },
		{ id: 'MOVE',			name: 'Move' },
		{ id: 'PRINT',			name: 'Print' }
	]
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'strictoperationtypes' }));
});