Ext.define('CFJS.store.document.StrictForms', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.strictforms',
	model	: 'CFJS.model.document.StrictForm',
	storeId	: 'strictForms',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'strict_form' },
			reader	: { rootProperty: 'response.transaction.list.strict_form' }
		}
	}
});
