Ext.define('CFJS.admin.view.StrictOperationsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.strictoperations',
	requires: [ 'CFJS.store.document.StrictOperations', 'CFJS.store.document.StrictOperationTypes' ],
	itemName: 'editStrictOperation',
	stores	: {
		strictBlanks: {
			type	: 'strictoperations',
			autoLoad: true,
			session	: { schema: 'document' },
			filters	: [{
				id		: 'owner',
				property: 'owner.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{userInfo.id}'
			}]
		}
	}
});
