Ext.define('CFJS.admin.view.StrictFormsModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.strictforms',
	requires: [ 'CFJS.store.document.StrictForms' ],
	itemName: 'editStrictForm',
	stores	: {
		strictBlanks: {
			type	: 'strictforms',
			autoLoad: true,
			session	: { schema: 'document' }
		}
	}
});
