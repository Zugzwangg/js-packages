Ext.define('CFJS.admin.view.StrictBlanksController', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.admin.strictblanks',
	
	doSelectionChange: function(selectable, selection, actions) {
		selectable.disableComponent(actions.removeRecord, !selection || selection.length < 1);
	},
	
	onAddRecord: function(component) {
		this.processBlanks(component, 'issue');
	},
	
	onBackClick: function(component) {
		var me = this,
			view = me.getView(),
			grid = view.lookupGrid(), 
			layout = view.getLayout();
		if (grid && layout.getActiveItem() !== grid) {
			Ext.suspendLayouts();
			layout.setActiveItem(grid);
			Ext.resumeLayouts(true);
		}
	},
	
	onEditRecord: Ext.emptyFn,
	
	onGridDblClick: Ext.emptyFn,
	
	onMoveRecord: function(component) {
		this.processBlanks(component, 'move');
	},
	
	onRemoveRecord: function(component) {
		this.processBlanks(component, 'cancel');
	},
	
	parseSelection: function(selection) {
		selection = Ext.Array.from(selection);
		var len = selection.length, 
			data = {}, i = 0, key, skey, item, form, series, state;
		for (; i < len; i++) {
			item = selection[i].getData();
			if (item.state === 'PRINTED') continue;
			form = data[item.strictForm.id] = data[item.strictForm.id] || { form: item.strictForm, series: {} };
			series = form.series[item.series] = form.series[item.series] || {};
			state = series[item.state] = series[item.state] || [];
			state.push(item.number);
		}
		selection = [];
		for (key in data) {
			item = data[key];
			form = item.form;
			series = item.series;
			for (skey in series) {
				item = series[skey];
				for (state in item) {
					selection.push({
						form: form,
						series: skey,
						state: state,
						numbers: item[state].join()
					});
				}
			}
		}
		return selection;
	},

	processBlanks: function(component, operation) {
		var me = this, 
			view = me.getView(),
			grid = view.lookupGrid(), 
			layout = view.getLayout(),
			panel;
		if (grid && layout.getActiveItem() === grid) {
			Ext.suspendLayouts();
			panel = view.add(view.operations[operation || component.action]);
			if (panel.addSelection) {
				panel.getStore().setData(me.parseSelection(grid.getSelection()));
			}
			panel.on({
				processed: function(record, success) {
					Ext.suspendLayouts();
					layout.setActiveItem(grid);
					grid.getStore().reload();
					Ext.resumeLayouts(true);
				}
			});
			layout.setActiveItem(panel);
			Ext.resumeLayouts(true);
		}
	}
	
});
