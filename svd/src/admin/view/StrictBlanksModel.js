Ext.define('CFJS.admin.view.StrictBlanksModel', {
	extend	: 'CFJS.app.BaseEditorModel',
	alias	: 'viewmodel.admin.strictblanks',
	requires: [ 'CFJS.store.document.StrictBlanks', 'CFJS.store.document.StrictBlankStates' ],
	itemName: 'editStrictBlank',
	stores	: {
		strictBlanks: {
			type	: 'strictblanks',
			autoLoad: true,
			session	: { schema: 'document' },
			filters	: [{
				id		: 'owner',
				property: 'owner.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{user.post.id}'
			}]
		}
	}
});
