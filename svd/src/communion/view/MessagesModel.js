Ext.define('CFJS.communion.view.MessagesModel', {
	extend			: 'CFJS.app.BaseEditorModel',
	alias			: 'viewmodel.communion.messages',
	requires		: [ 'CFJS.store.communion.DiscussionMessages' ],
	itemName		: 'editDiscussionMessage',
	stores			: {
		messages: {
			type	: 'discussion_messages',
			autoLoad: true,
			session	: { schema: 'communion' },
			filters	: [{
				id		: 'authorId',
				property: 'author.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{userInfo.id}'
			},{
				id		: 'authorClass',
				property: 'author.class',
				type	: 'string',
				operator: 'eq',
				value	: '{userInfo.$class}'
			},{
				property: '{userInfo.type}s',
				type	: 'list',
				operator: 'in',
				value	: '{userInfo.id}'
			},{
				id		: 'startDate',
				property: 'created',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'created',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			},{
				id		: 'discussion',
				property: 'discussion.id',
				type	: 'numeric',
				operator: 'eq'
			},{
				id		: 'parent',
				property: 'parent.id',
				type	: 'numeric',
				operator: 'eq'
			}]
		}
	},
	workingPeriod	: [ Ext.Date.getFirstDateOfMonth(new Date()), Ext.Date.addComplex(Ext.Date.getLastDateOfMonth(new Date()), {days: 1, seconds: -1}) ]

});
