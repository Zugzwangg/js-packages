Ext.define('CFJS.communion.view.ComposeDiscussionModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.communion.discussion.compose',
	requires	: [ 'CFJS.model.communion.*' ],
	itemName	: 'composeMessage',
	itemModel	: 'CFJS.model.communion.Discussion',
	
	defaultItemConfig: function() {
		return {
			author	: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
			created	: new Date()
		}
	}

});