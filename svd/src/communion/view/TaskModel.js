Ext.define('CFJS.communion.view.TaskModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.communion.task.edit',
	requires	: [ 'CFJS.model.communion.*' ],
	itemName	: undefined,
	itemModel	: 'CFJS.model.communion.LocalTask',
});
