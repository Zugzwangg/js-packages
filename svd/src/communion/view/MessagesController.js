Ext.define('CFJS.communion.view.MessagesController', {
	extend		: 'CFJS.communion.view.CommunionController',
	alias		: 'controller.communion.messages',
	editor		: 'grid[name="messageViewPanel"]',
	
	editorConfig: function() {
		var grid = this.lookupGrid();
		return {
			xtype			: 'communion.grid.message-view',
			composeWindow	: grid && grid.composeWindow,
			name			: 'messageViewPanel'
		}
	}

});