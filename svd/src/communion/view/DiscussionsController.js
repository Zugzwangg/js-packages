Ext.define('CFJS.communion.view.DiscussionsController', {
	extend		: 'CFJS.communion.view.CommunionController',
	alias		: 'controller.communion.discussions',
	editor		: 'communion-grid-discussion-header[name="discussionViewPanel"]',

	editorConfig: function() {
		return { xtype: 'communion-grid-discussion-view-panel', name: 'discussionViewPanel' }
	},
	
	onComposeDiscussion: function(component) {
		this.doComposeCommunion(component);
	},
	
	onFinishDiscussion: function(component) {
		var me = this, record = me.lookupCurrentItem();
		if (record && record.isDiscussion) {
			record.set('finished', new Date());
			me.onSaveRecord(component);
		}
	}

});