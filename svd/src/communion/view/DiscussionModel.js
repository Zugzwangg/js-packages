Ext.define('CFJS.communion.view.DiscussionModel', {
	extend		: 'CFJS.communion.view.ComposeDiscussionModel',
	alias		: 'viewmodel.communion.discussion.edit',
	itemName	: undefined,
	stores		: {
		childMessages: {
			type	: 'base',
			autoLoad: true,
			model	: 'CFJS.model.communion.DiscussionMessage',
			session	: { schema: 'communion' },
			pageSize: 0,
			proxy	: {
				loaderConfig: { dictionaryType: 'discussion_message' },
				reader		: { rootProperty: 'response.transaction.list.discussion_message' }
			},
			filters	: [{
//				id		: 'authorId',
//				property: 'author.id',
//				type	: 'numeric',
//				operator: 'eq',
//				value	: '{userInfo.id}'
//			},{
//				id		: 'authorClass',
//				property: 'author.class',
//				type	: 'string',
//				operator: 'eq',
//				value	: '{userInfo.$class}'
//			},{
//				property: '{userInfo.type}s',
//				type	: 'list',
//				operator: 'in',
//				value	: '{userInfo.id}'
//			},{
				id		: 'discussion',
				property: 'discussion.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			},{
				id		: 'parent',
				property: 'parent.id',
				type	: 'numeric',
				operator: 'eq'
			}],
			sorters	: [ 'created' ]
		}
	}

});
