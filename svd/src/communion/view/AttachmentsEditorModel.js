Ext.define('CFJS.communion.view.AttachmentsEditorModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.communion.attachments-editor',
	data		: {
		document: null,
		form	: null,
		link	: null,
		linkName: null
	},
	itemName	: undefined,
	itemModel	: undefined,
	stores		: {
		attachments: {
			type	: 'store',
			model	: 'CFJS.model.Attachment',
			data	: '{_parentItemName_.attachments}'
		},
		documents: {
			type		: 'named',
			proxy		: {
				type		: 'json.svdapi',
				loaderConfig: {
					dictionaryType	: 'document',
					formId			: '{form.id}',
					unit			: '{userUnit}',
					userInfo		: '{userInfo}'
				},
				service		: 'svd'
			},
			remoteSort	: false
		},
		forms: {
			type	: 'named',
			proxy	: { loaderConfig: { dictionaryType: 'form' }, service: 'svd' },
			filters	: [{
				type	: 'string',
				property: 'type',
				operator: 'eq',
				value	: 'DOCUMENT'
			},{
				id		: 'permission',
				type	: 'numeric',
				property: 'permissions[\'READ\']',
				operator: '!lt',
				value	: '{permissionLevel}'
			}],
			sorters		: [ 'name' ]
		}
	},
	
	applyChanges: function() {
		this.updateArrayField(this.getParentItem(), 'attachments');
	}
	
});