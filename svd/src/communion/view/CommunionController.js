Ext.define('CFJS.communion.view.CommunionController', {
	extend		: 'CFJS.app.DictionaryController',

	doComposeCommunion: function(component) {
		var me = this, grid = me.lookupActiveGrid(component) || me.lookupGrid(),
			action = component && component.action, win;
		if (grid && grid.composeWindow) {
			win = Ext.create(grid.composeWindow);
			win.on('sendmessage', function(record) { grid.getStore().reload() });
			if (action && Ext.isFunction(win[action])) {
				win[action].call(win, grid.lookupViewModel().getItem());
			}
		}
	},

	lookupActiveGrid: function(component) {
		return component && component.ownerCt && component.ownerCt.grid;
	},
	
	lookupActiveGridStore: function(component) {
		var grid = this.lookupActiveGrid(component);
		return grid && grid.getStore();
	},
	
	onAttachmentsClick: function(component) {
		console.log('onAttachmentsClick', component);
	},
	
	onComposeMessage: function(component) {
		this.doComposeCommunion(component);
	},

	onBackClick: function(component) {
		this.maybeEditRecord(false);
	},
	
	onReloadMessage: function(component) {
		var store = this.lookupActiveGridStore(component);
		if (store && store.isStore) store.reload();
	}
	
});