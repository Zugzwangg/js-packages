Ext.define('CFJS.communion.view.TasksModel', {
	extend			: 'CFJS.app.BaseEditorModel',
	alias			: 'viewmodel.communion.tasks',
	requires		: [ 'CFJS.store.communion.LocalTasks' ],
	itemName		: 'editLocalTask',
	stores			: {
		tasks: {
			type	: 'local_tasks',
			autoLoad: true,
			session	: { schema: 'communion' },
//			filters	: [{
//				id		: 'unit',
//				property: 'unit.id',
//				type	: 'numeric',
//				operator: 'eq',
//				value	: '{userUnit.id}'
//			},{
//				property: 'customerType',
//				type	: 'string',
//				operator: '!eq'
//			}]
		}
	},
	workingPeriod	: [ Ext.Date.getFirstDateOfMonth(new Date()), Ext.Date.addComplex(Ext.Date.getLastDateOfMonth(new Date()), {days: 1, seconds: -1}) ],

	filesPermissions: function() {
		return this.getPermissions('customers.company.files');
	}
});
