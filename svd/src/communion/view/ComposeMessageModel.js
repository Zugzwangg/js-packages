Ext.define('CFJS.communion.view.ComposeMessageModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.communion.message.compose',
	requires	: [ 'CFJS.model.communion.*' ],
	itemName	: 'composeMessage',
	itemModel	: 'CFJS.model.communion.DiscussionMessage',
	
	defaultItemConfig: function() {
		return {
			author	: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
			created	: new Date()
		}
	},
	
	excludeAuthor: function(msg) {
		var author = this.getAuthorData(msg),
			type = author.type + 's',
			items = msg.get(type),
			i = 0;
		for (; i < items.length; i++) {
			if (items[i].id === author.id) {
				items.splice(i, 1);
				break;
			}
		}
		msg.set(type, items);
	},
	
	getAuthorModel: function(message) {
		if (message && message.isEntity) {
			return new CFJS.model.UserInfo(message.get('author'));
		}
	},
	
	getAuthorData: function(message) {
		var author = this.getAuthorModel(message);
		if (author && author.isEntity) return author.getData();
	},
	
	getAuthorOptions: function(message) {
		var author = this.getAuthorData(message), opt;
		if (author) {
			opt = {};
			opt[author.type + 's'] = [author];
		}
		return opt;
	},
	
	setMessageData: function(parent, data) {
		var me = this, msg = me.getItem();
		if (parent && parent.isEntity) {
			if (parent.isDiscussion) msg.setDiscussion(parent);
			else msg.setParent(parent);
			msg.set(Ext.apply({ description: '<br/><blockquote>' + Ext.clone(parent.get('description')) + '</blockquote>' }, data));
			me.excludeAuthor(msg);
		}
		return msg;
	}
	
});
