Ext.define('CFJS.communion.view.DiscussionsModel', {
	extend			: 'CFJS.app.BaseEditorModel',
	alias			: 'viewmodel.communion.discussions',
	requires		: [ 'CFJS.store.communion.Discussions' ],
	itemName		: 'editDiscussion',
	stores			: {
		discussions: {
			type	: 'discussions',
			autoLoad: true,
			session	: { schema: 'communion' },
			filters	: [{
				property: 'author',
				type	: 'numeric',
				operator: 'eq',
				value	: '{userInfo.id}'
			},{
				property: '{userInfo.type}s',
				type	: 'list',
				operator: 'in',
				value	: '{userInfo.id}'
			},{
				id		: 'startDate',
				property: 'created',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'created',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			}],
			sorters	: [{ property: 'created', direction: 'DESC' },{ property: 'finished', direction: 'DESC' }],
		}
	},
	workingPeriod	: [ Ext.Date.getFirstDateOfMonth(new Date()), Ext.Date.addComplex(Ext.Date.getLastDateOfMonth(new Date()), {days: 1, seconds: -1}) ] 
});
