Ext.define('CFJS.model.document.FormTemplate', {
    extend			: 'CFJS.model.CodeNamePair',
	requires		: [ 'CFJS.schema.Document' ],
	isPrintTemplate	: true,
	schema			: 'document',
	fields			: [
		{ name: 'type',			type: 'string', defaultValue: 'PDF_PRINT' },
		{ name: 'code',			type: 'string', defaultValue: 'template' },
		{ name: 'strictForm',	type: 'auto' }
	],
	
	buildPrintUrl: function(document, service, forSign) {
		return CFJS.model.document.Document.createPrintUrl(this.get('code'), this.printMethod(forSign), service, document); 
	},

	buildPrintRequest: function(document, service, series, number) {
		var me = this, api = CFJS.Api, strictForm = me.get('strictForm'), request;
		if ((document = me.documentId(document)) > 0 
				&& strictForm && strictForm.id > 0 
				&& !Ext.isEmpty(series) && !Ext.isEmpty(number)) {
			service = service || 'svd';
			request = {
				url			: api.controllerUrl(service),
				method		: 'POST',
				service		: service,
				signParams	: true,
				params		: { method: [api.controllerMethod(service), 'strict.print'].join('.') },
				success		: function(context) {
					var response = api.extractResponse(context, ''), file;
					if (response.success) {
						if (Ext.isArray(file = (response.files||{}).file||{})) file = file[0];
						window.open(api.buildUrl({
							service: 'resources',
							signParams: true,
							params: {
								method: 'resource.download',
								path: file.name,
								type: 'svd/print/pdf/'
							}
						}), '_blank');
					} else CFJS.errorAlert('Error printing data', context);
				},
				failure		: function(context) { CFJS.errorAlert('Error printing data', context); }
			};
			request[api.payloadFormat() + 'Data'] = api.buildPayload({
				locale	: CFJS.getLocale(),
				document: document,
				form	: strictForm.id,
				series	: series,
				numbers	: number,
			}, 'PRINT');
		}
		return request;
	},
	
	iconCls: function() {
		return 'x-fa fa-file-' + (this.get('type')||'pdf').split('_')[0].toLowerCase() + '-o';
	},

	isStrict: function() {
		var strictForm = this.get('strictForm');
		return strictForm && strictForm.id > 0;
	},
	
	print: function(document, service) {
		var url = this.buildPrintUrl(document, service);
		if (!Ext.isEmpty(url)) window.open(url, '_blank');
	},
	
	printStrict: function(document, service, series, number) {
		var request = this.buildPrintRequest(document, service, series, number);
		if (request) CFJS.Api.request(request);
	},

	printMethod: function(forSign) {
		return forSign === true ? 'download.signed' : this.get('type') === 'EXCEL_PRINT' ? 'export_excel' : 'print';
	},
	
	privates: {
		
		documentId: function(document) {
			if (Ext.isObject(document)) document = document.id;
			return parseInt(document, 10);	
		}
	
	}
	
});