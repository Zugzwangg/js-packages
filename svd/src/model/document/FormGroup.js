Ext.define('CFJS.model.document.FormGroup', {
	extend			: 'CFJS.model.TreeModel',
	requires		: [ 'CFJS.schema.Document' ],
    dictionaryType	: 'form_group',
	schema			: 'document'
});