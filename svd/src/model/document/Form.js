Ext.define('CFJS.model.document.Form', {
    extend	: 'CFJS.model.LocalizableModel',
	requires: [
		'CFJS.model.document.Field',
		'CFJS.model.document.FormTemplate',
		'CFJS.model.document.Layout',
		'CFJS.document.util.Forms'
	],
	isForm	: true,
	schema	: 'document',
	config	: { target: 'grid' },
	fields	: [
		{ name: 'type',				type: 'string' },
		{ name: 'group',			type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'unit',				type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'sectors',			type: 'array',	critical: true, item: { entityName: 'CFJS.model.NamedModel', mapping: 'sector' } },
		{ name: 'strictForms',		type: 'array',	critical: true, item: { entityName: 'CFJS.model.document.StrictForm', mapping: 'strict_form' } },
		{ name: 'workflows',		type: 'array',	critical: true, item: { entityName: 'CFJS.model.NamedModel', mapping: 'named_entity' } },
		{ name: 'checkUnit',		type: 'boolean' },
		{ name: 'height',			type: 'int' },
		{ name: 'width',			type: 'int' },
		{ name: 'fields',			type: 'array',	critical: true, item: { entityName: 'CFJS.model.document.Field', mapping: 'field_data' } },
		{ name: 'layouts',			type: 'array',	critical: true, item: { entityName: 'CFJS.model.document.Layout', mapping: 'field_set' }  },
		{ name: 'planSignature',	type: 'array',	critical: true, item: { entityName: 'CFJS.model.NamedModel', mapping: 'named_entity' } },
		{ name: 'permissions',		type: 'array',	critical: true, item: { mapping: 'permission' } },
		{ name: 'printTemplates',	type: 'array',	critical: true, item: { entityName: 'CFJS.model.document.FormTemplate', mapping: 'template' } },
		{ name: 'exportTemplates',	type: 'array',	critical: true, item: { entityName: 'CFJS.model.document.FormTemplate', mapping: 'template' } },
		{ name: 'iconCls', 			type: 'string',	calculate: function(data) { return CFJS.document.util.Forms.iconCls(data.type); } }
	],
	proxy	: {
		type		: 'json.svdapi',
		api			: { read: { method: 'load.form', transaction: 'load' } },
		loaderConfig: { target: 'grid' },
		service		: 'svd'
	},
	
	setTarget: function(target, reload) {
		var me = this;
		me.updateTarget(me._target = me.applyTarget(target, me._target));
		if (reload) me.load();
	},
	
	actions: function(level) {
		var permissions = this.get('permissions') || [], 
			actions = {}, i = 0, permission;
		if (!Ext.isNumber(level)) level = CFJS.shortMax;
		for (; i < permissions.length; i++) {
			permission = permissions[i] || {};
			if (!Ext.isEmpty(permission.action)) {
				actions[permission.action] = level <= CFJS.safeLevel(permission.level);
			}
		}
		return actions;
	},

	hasWorkflow: function() {
		return Ext.Array.from(this.get('workflows')).length > 0;
	},
	
	isAllowed: function(action, level) {
		return !isNaN(level = CFJS.parseInt(level)) && level <= this.permission(action);
	},
	
	permission: function(action) {
		var level;
		if (Ext.isString(action)) {
			var permissions = this.get('permissions'), i = 0, permission;
			if (!Ext.isEmpty(permissions)) {
				action = action.toUpperCase();
				for (; i < permissions.length; i++) {
					permission = permissions[i];
					if (action === permission.action) {
						level = CFJS.safeLevel(permission.level);
						break;
					}
				}
			}
		}
		return level ? level : CFJS.shortMax;
	},
	
	privates: {
		
		applyTarget: function(target) {
			return target || 'grid'
		},
		
		updateTarget: function(target) {
			var proxy = this.getProxy();
			if (proxy && proxy.isSvdProxy) proxy.setTarget(target);
		}
		
	}

});