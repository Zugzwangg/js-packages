Ext.define('CFJS.model.document.DocumentResource', {
    extend			: 'CFJS.model.webdav.SimpleResource',
	requires		: [ 'CFJS.schema.Document' ],
    dictionaryType	: 'document_resource',
	parentType		: 'document',
    methodDownload	: 'resource.download',
	schema			: 'document',
	proxy			: { type: 'json.svdapi', service: 'svd' }
});