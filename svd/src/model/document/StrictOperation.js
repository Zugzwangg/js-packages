Ext.define('CFJS.model.document.StrictOperation', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Document', 'CFJS.model.NamedModel' ],
    dictionaryType	: 'strict_operation',
	schema			: 'document',
	fields			: [
		{ name: 'type',			type: 'string',	critical: true },
		{ name: 'document',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'owner',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'secondary',	type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'created',		type: 'date',	persist: false }
	]
});