Ext.define('CFJS.model.document.StrictForm', {
    extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Document' ],
    dictionaryType	: 'strict_form',
	isStrictForm	: true,
	schema			: 'document',
	fields			: [{ name: 'printTemplate', type: 'string' }]
});