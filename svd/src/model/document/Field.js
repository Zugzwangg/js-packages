Ext.define('CFJS.model.document.Field', {
    extend			: 'CFJS.model.NamedModel',
	requires		: [ 'CFJS.schema.Document' ],
    dictionaryType	: 'field_config',
	schema			: 'document',
	fields			: [
		{ name: 'type',				type: 'string',		defaultValue: 'UNKNOWN' },
		{ name: 'aggregation',		type: 'string',		defaultValue: 'NONE' },
		{ name: 'columnHidden',		type: 'boolean' },
		{ name: 'columnOrder',		type: 'int' },
		{ name: 'columnWidth',		type: 'int',		defaultValue: 100 },
 		{ name: 'defaultValue',		type: 'property',	mapping: 'properties.field_defaultValue', allowNull: true },
		{ name: 'description',		type: 'boolean' },
		{ name: 'editable',			type: 'boolean' },
		{ name: 'formula',			type: 'string',		allowNull: true },
		{ name: 'mask',				type: 'string',		allowNull: true },
		{ name: 'masterFilter',		type: 'string',		allowNull: true },
		{ name: 'notCopy',			type: 'boolean' },
		{ name: 'permissionLevel',	type: 'int',		defaultValue: CFJS.shortMax },
		{ name: 'readOnly',			type: 'boolean',	defaultValue: true },
		{ name: 'reference',		type: 'auto',		allowNull: true },
		{ name: 'required',			type: 'boolean' }
	],
	proxy	: { type: 'json.svdapi', service: 'svd' }
});