Ext.define('CFJS.model.document.LayoutFieldData', {
    extend	: 'CFJS.model.EntityModel',
	schema	: 'document',
	fields	: [
		{ name: 'colspan',			type: 'int',	defaultValue: 1 },
   		{ name: 'height',			type: 'int',	defaultValue: -1 },
   		{ name: 'horizontalAlign',	type: 'string',	defaultValue: 'LEFT' },
   		{ name: 'margin',			type: 'int' },
   		{ name: 'padding',			type: 'int' },
		{ name: 'rowspan',			type: 'int',	defaultValue: 1 },
   		{ name: 'style',			type: 'string' },
   		{ name: 'styleName',		type: 'string' },
   		{ name: 'verticalAlign',	type: 'string',	defaultValue: 'TOP' },
   		{ name: 'width',			type: 'int',	defaultValue: -1 }
   	]
});
