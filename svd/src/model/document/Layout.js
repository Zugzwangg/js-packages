Ext.define('CFJS.model.document.Layout', {
    extend	: 'CFJS.model.NamedModel',
	requires: [ 'CFJS.model.document.LayoutField', 'CFJS.model.document.LayoutData' ],
	schema	: 'document',
	fields	: [
		{ name: 'borders',			type: 'boolean' },
		{ name: 'collapsible',		type: 'boolean' },
		{ name: 'fields',			type: 'array',	critical: true, item: { entityName: 'CFJS.model.document.LayoutField', mapping: 'field' } },
		{ name: 'hideHeader',		type: 'boolean' },
		{ name: 'layoutData',		type: 'model',	critical: true, entityName: 'CFJS.model.document.LayoutData' },
		{ name: 'setOrder',			type: 'int' }
	],
	proxy	: {
		type		: 'json.svdapi',
		service		: 'svd'
	}

});