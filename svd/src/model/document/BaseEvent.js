Ext.define('CFJS.model.document.BaseEvent', {
    extend	: 'CFJS.model.EntityModel',
	requires: [ 'CFJS.model.NamedModel', 'CFJS.schema.Document' ],
	schema	: 'document',
	fields	: [
		{ name: 'parentId',		type: 'int',	critical: true, allowNull: true },
		{ name: 'document',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'type',			type: 'string',	critical: true },
		{ name: 'description',	type: 'string',	critical: true },
		{ name: 'duration',		type: 'number',	critical: true },
		{ name: 'name',			type: 'string', calculate: function(data) { return data.description } }
	],
	proxy	: { type: 'json.svdapi', service: 'svd' }
});