Ext.define('CFJS.model.document.DocumentJournal', {
    extend	: 'CFJS.model.NamedModel',
	fields	: [
		{ name: 'type',	type: 'string' },
		{ name: 'form',	type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' }
	]
});