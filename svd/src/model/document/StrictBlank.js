Ext.define('CFJS.model.document.StrictBlank', {
    extend			: 'CFJS.model.EntityModel',
	requires		: [ 'CFJS.schema.Document', 'CFJS.model.document.StrictForm' ],
    dictionaryType	: 'strict_blank',
	schema			: 'document',
	fields			: [
		{ name: 'strictForm',	type: 'model',	critical: true,	entityName: 'CFJS.model.document.StrictForm' },
		{ name: 'owner',		type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'state',		type: 'string',	critical: true },
		{ name: 'series',		type: 'string',	critical: true },
		{ name: 'number',		type: 'string',	critical: true },
		{ name: 'title',		type: 'string',	calculate: function(data) { return data.series + '-' + data.number } }
	]
});