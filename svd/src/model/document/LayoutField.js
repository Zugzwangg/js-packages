Ext.define('CFJS.model.document.LayoutField', {
    extend	: 'CFJS.model.EntityModel',
    requires: [ 'CFJS.model.document.LayoutFieldData' ],
	schema	: 'document',
	fields	: [{ name: 'layoutField', type: 'model', critical: true, entityName: 'CFJS.model.document.LayoutFieldData' }]
});
