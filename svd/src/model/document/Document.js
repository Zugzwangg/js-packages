Ext.define('CFJS.model.document.Document', {
    extend			: 'CFJS.model.LocalizableModel',
	requires		: [
		'CFJS.schema.Communion',
		'CFJS.schema.Document',
		'CFJS.model.CustomerInfo',
		'CFJS.model.document.DigitalSignature',
		'CFJS.model.document.Permission'
	],
	schema			: 'document',
	isDocument		: true,
	dictionaryType	: 'document',
	fields			: [
		{ name: 'properties',	type: 'auto',		critical: true,	defaultValue: {} },
		{ name: 'parentId',		type: 'int',		critical: true, allowNull: true },
		{ name: 'form',			type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'author',		type: 'auto',		critical: true },
		{ name: 'sector',		type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'unit',			type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'contractor',	type: 'model',		critical: true,	entityName: 'CFJS.model.CustomerInfo' },
		{ name: 'process',		type: 'model',		critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'permissions',	type: 'array',		critical: true, item: { mapping: 'permission' } },
		{ name: 'signatures',	type: 'array',		critical: true, item: { entityName: 'CFJS.model.document.DigitalSignature', mapping: 'digital_signature' }, persist: false },
		{ name: 'signedFiles',	type: 'array',		critical: true, item: { entityName: 'CFJS.model.NamedModel', mapping: 'named_entity' }, persist: false },
		{ name: 'created',		type: 'date',		persist: false },
		{ name: 'updated',		type: 'date',		persist: false },
		{ name: 'canSign',		type: 'boolean',	depends: ['id'],
			convert: function(value, record) {
				return record.get('id') > 0 && !record.hasSign();
			}
		}
	],
	proxy			: { type: 'json.svdapi', service: 'svd' },
	
	actions: function(level) {
		var me = this, permissions = me.get('permissions') || [],
			actions = {}, i = 0, permission;
		if (!Ext.isNumber(level)) level = CFJS.shortMax;
		for (; i < permissions.length; i++) {
			permission = permissions[i] || {};
			if (!Ext.isEmpty(permission.action)) {
				actions[permission.action] = level <= CFJS.safeLevel(permission.level);
			}
		}
		return actions;
	},
	
	addSign: function(signature, options) {
		if (signature) {
			var me = this, signatures = me.get('signatures') || [];
			if (Ext.isArray(signatures)) {
				signatures.push(signature);
				return me.set({ signatures: signatures, canSign: false }, options || { convert: true, commit: true });
			}
		}
	},
	
	asNamedModel: function() {
		var me = this, form = me.get('form') || {};
		return new CFJS.model.Named({ id: me.getId(), name: form.name + ' (' + me.getId() + ')' });
	},
	
	buildPrintUrl: function(template, method, service) {
		return CFJS.model.document.Document.createPrintUrl(template, method, service, this.getId());
	},

	hasSign: function(phase, userInfo) {
		var me = this, signatures = me.get('signatures'), i = 0;
		userInfo = userInfo || CFJS.lookupViewportViewModel().userInfo();
		phase = phase || (((me.get('process')||{}).properties||{}).field_currentPhaseType||{}).$;
		signatures = Ext.Array.from((signatures||{}).digital_signature || signatures);
		if (!Ext.isEmpty(signatures) && userInfo) {
			for (; i < signatures.length; i++) {
				if (signatures[i] && me.equalUser(userInfo, signatures[i].signer) && (!phase || phase === signatures[i].phase)) return true;
			}
		}
		return false;
	},
	
	isAllowed: function(action, level) {
		return !isNaN(level = CFJS.parseInt(level)) && level <= this.permission(action);
	},
	
	load: function(options) {
		var me = this, proxy = me.getProxy(),
			loaderConfig = { form: CFJS.nestedData('form.id', me.data) };
		if (options) {
			loaderConfig = Ext.apply(loaderConfig, options.loaderConfig);
			if (Ext.isNumber(options.form)) loaderConfig.form = options.form;
		}
		me.getProxy().addLoaderConfig(loaderConfig);
		me.callParent(arguments);
	},

	permission: function(action) {
		var level;
		if (Ext.isString(action)) {
			var permissions = this.get('permissions'), i = 0, permission;
			if (!Ext.isEmpty(permissions)) {
				action = action.toUpperCase();
				for (; i < permissions.length; i++) {
					permission = permissions[i];
					if (action === permission.action) {
						level = CFJS.safeLevel(permission.level);
						break;
					}
				}
			}
		}
		return level ? level : CFJS.shortMax;
	},
	
	print: function(template, method, service) {
		var url = this.buildPrintUrl(template, method, service);
		if (!Ext.isEmpty(url)) window.open(url, '_blank');
	},

	readOnly: function() {
		return Ext.Array.from(this.get('signatures')).length > 0;
	},

	privates: {
		
		equalUser: function(user1, user2) {
			return user1 && user2 && user1.id === user2.id && user1.type === user2.type; 
		}

	},
	
	inheritableStatics: {
		
		sign: function(data, session, options) {
			if (session && !session.isSession) {
				options = session;	
				session = null;
			}
			return new CFJS.model.document.Document(data, session).sign(options);
		},
		
		createPrintUrl: function(template, method, service, document) {
			if (Ext.isObject(document)) document = document.id;
			return CFJS.Api.buildUrl({
				service		: service || 'svd',
				signParams	: true,
				params		: {
					method	: method || 'preview',
					document: parseInt(document, 10),
					template: template || 'template',
					locale	: CFJS.getLocale()
				}
			});
		}
	
	}

}, function() {
	Ext.data.schema.Schema.get('communion').addEntity(Ext.ClassManager.get(this.$className));
});