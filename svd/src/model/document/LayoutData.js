Ext.define('CFJS.model.document.LayoutData', {
    extend	: 'CFJS.model.EntityModel',
	schema	: 'document',
	fields	: [
   		{ name: 'border',				type: 'int' },
   		{ name: 'cellHorizontalAlign',	type: 'string',	defaultValue: 'LEFT' },
   		{ name: 'cellPadding',			type: 'int' },
   		{ name: 'cellSpacing',			type: 'int' },
   		{ name: 'cellVerticalAlign',	type: 'string',	defaultValue: 'TOP' },
   		{ name: 'columns',				type: 'int',	defaultValue: 1 },
   		{ name: 'fieldAnchor',			type: 'string' },
		{ name: 'hideLabels',			type: 'boolean' },
		{ name: 'insertSpacer',			type: 'boolean' },
   		{ name: 'labelAlign',			type: 'string',	defaultValue: 'LEFT' },
   		{ name: 'labelPad',				type: 'int',	defaultValue: 5 },
   		{ name: 'labelSeparator',		type: 'string',	defaultValue: ':' },
		{ name: 'labelWidth',			type: 'int',	defaultValue: 100 },
   		{ name: 'tableStyle',			type: 'string' }
   	]
});
