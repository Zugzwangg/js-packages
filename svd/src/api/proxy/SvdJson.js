Ext.define('CFJS.api.proxy.SvdJson', {
	extend				: 'CFJS.api.proxy.Json',
	alternateClassName	: [ 'CFJS.api.SvdJsonProxy' ],
	alias				: 'proxy.json.svdapi',
	config				: {
		api			: {
			create	: 'save',
			destroy	: 'remove',
			read	: 'load',
			sign	: 'sign',
			summary	: { method: 'dictionary', transaction: 'calculate' },
			update  : 'save'
		},
		extraParams		: { system: false },
		loaderConfig	: {
			dictionaryType	: 'document',
			formId			: undefined,
			locale			: undefined,
			parentId		: undefined,
			period			: undefined,
			postId			: undefined,
			query			: undefined,
			sector			: undefined,
			unit			: undefined
		},
		reader		: { rootProperty: 'response.transaction.list.document' },
		service		: 'svd'
	},
	isSvdProxy			: true,
	
	applyLoaderConfig: function(loaderConfig) {
		loaderConfig = loaderConfig || {};
		loaderConfig.dictionaryType = loaderConfig.dictionaryType || 'document';
		loaderConfig.locale = loaderConfig.locale || CFJS.getLocale();
		loaderConfig.period = this.serializePeriod(loaderConfig.period);
		return loaderConfig;
	},
	
	getForm: function() {
		return this.loaderConfig.form;
	},

	getTarget: function() {
		return this.loaderConfig.target;
	},
	
	serializePeriod: function(period) {
		if (!period) return;
		var cfapi = this.cfapi, result = {};
		if (Ext.isDate(period.startDate)) result.startDate = cfapi.formatDateTime(period.startDate);
		if (Ext.isDate(period.endDate)) result.endDate = cfapi.formatDateTime(period.endDate);
		return Ext.Object.isEmpty(result) ? undefined : result
	},
	
	serializeFilter: function(filter) {
		filter = this.callParent(arguments);
		if (filter && filter.value 
			&& (filter.type === 'date' || filter.type === 'datetime')) {
			if (Ext.isString(filter.value)) filter.value = Date.parse(filter.value);
			if (isNaN(filter.value)) delete filter.value;
		}
		return filter;
	},
	
	setForm: function(form) {
		this.addLoaderConfig({ form: form && form.id ? form.id : form });
	},
	
	setTarget: function(target) {
		this.addLoaderConfig({ target: target });
	},
	
	setQuery: function(query) {
		this.addLoaderConfig({ query: query });
	},

	setSector: function(sector) {
		this.addLoaderConfig({ sector: sector });
	},

	setUnit: function(unit) {
		this.addLoaderConfig({ unit: unit });
	}
	
});