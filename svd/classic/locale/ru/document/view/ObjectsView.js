Ext.define('CFJS.locale.ru.document.view.ObjectsView', {
	override: 'CFJS.document.view.ObjectsView',
	filterPanelConfig: {
		items: [
			{ emptyText: 'выбрать сектор', fieldLabel: 'Сектор' },
			{ emptyText: 'выбрать подразделение', fieldLabel: 'Подразделение' }
		],
		title: 'Дополнительные фильтры'
	}
});