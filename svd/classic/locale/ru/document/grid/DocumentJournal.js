Ext.define('CFJS.locale.ru.document.grid.DocumentJournal', {
	override			: 'CFJS.document.grid.DocumentJournal',
	config				: {
		actions	: { addDocument: { tooltip: 'Добавить документы' } },
		title	: 'Документы'
	},
	emptyText			: 'Нет данных для отображения',
	nameText			: 'Журнал документов',
	removeText			: 'Удалить выделенные документы',
	sameDocumentTitle	: 'Внимание',
	sameDocumentText	: 'Выбранный документ уже есть в списке.',
	confirmRemove		: function(docName, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить документ "' + docName + '"?', fn, this);
	}
});