Ext.define('CFJS.locale.ru.document.grid.FieldsPicker', {
	override			: 'CFJS.document.grid.FieldsPicker',
	config	: {
		actions: {
			levelUp		: { tooltip: 'Назад к предыдущей форме' },
			reloadStore	: { tooltip: 'Обновить данные' }
		},
		title: 'Выбор поля'
	},
	initComponent: function() {
		var me = CFJS.apply(this, {
			columns		: [{ text: 'Код' },{ text: 'Название' },{ text: 'Тип' }],
			datatipTpl	: [
				'<b>Код</b>: {id}</br>',
				'<b>Название</b>: {name}</br>',
				'<b>Тип</b>: {[this.formatType(values.type)]}'
			],
			emptyText	: 'Нет данных для отображения',
			tbar		: [{ fieldLabel: 'Путь' }],
		});
		me.callParent();
	}
});