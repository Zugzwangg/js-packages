Ext.define('CFJS.locale.ru.document.grid.SignatoriesPanel', {
	override			: 'CFJS.document.grid.SignatoriesPanel',
	config				: { title: 'Подписанты' },
	datatipTpl			: [
		'<b>Код:</b> {id}</br>',
		'<tpl if="phase"><b>Фаза:</b> {[this.phaseRenderer(values.phase)]}</br></tpl>',
		'<tpl if="signer"><b>Подписант:</b> {signer.name}</br></tpl>',
		'<tpl if="resolution"><b>Резолюция:</b> {resolution}</br></tpl>',
		'<b>Метка времени:</b> {timestamp:date("' + Ext.Date.patterns.LongDateTime + '")}',
	],
	emptyText			: 'Нет данных для отображения',
	removeText			: 'Удалить выделенную подпись',
	verifySignatureMsg	: 'Подпись успешно подтверждена!',
	verifySignatureText	: 'Проверить выделенную подпись',
	verifySignatureTitle: 'Проверка подписи',
	confirmRemove		: function(signerName, fn) {
		return Ext.fireEvent('confirm', 'Вы уверены, что хотите удалить подпись "' + signerName + '"?', fn, this);
	},
	renderColumns: function() {
		return CFJS.apply(this.callParent(arguments), [{},{ text: 'Фаза' },{ filter: { emptyText: 'имя подписанта' }, text: 'Подписант' }]);
	}
	
});
