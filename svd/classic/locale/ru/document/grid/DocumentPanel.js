Ext.define('CFJS.locale.ru.document.grid.DocumentPanel', {
	override	: 'CFJS.document.grid.DocumentPanel',
	config		: {
		actions		: {
			addRecord	: { title: 'Добавить документ', tooltip: 'Добавить новый документ' },
			editRecord	: { title: 'Редактировать документ', tooltip: 'Редактировать текущий документ' },
			exportRecord: { title: 'Экспортировать в Excel', tooltip: 'Экспортировать выделенные документы в Excel' },
			printRecord	: { title: 'Печатать документ', tooltip: 'Печатать выделенные документы' },
			removeRecord: { title: 'Удалить документ', tooltip: 'Удалить выделенные документы' },
			signRecord	: { title: 'Подписать документ', tooltip: 'Подписать выделенные документы' }
		},
	},
	promptButton: { text: 'Печатать' },
	promptDlg	: { title: 'Печать на бланке' },
	promptField	: { emptyText: 'выберите бланк', fieldLabel: 'Выбор бланков строгой отчетности' }
});
