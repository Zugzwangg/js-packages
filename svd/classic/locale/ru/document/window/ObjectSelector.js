Ext.define('CFJS.locale.ru.document.window.ObjectSelector', {
	override			: 'CFJS.document.window.ObjectSelector',
	config				: { title: 'Выбор документа' },
	documentPanelConfig	: {
		buttons	: [{ text: 'Выбрать' }],
		items	: [
    		{ emptyText	: 'выберите тип документа', fieldLabel: 'Тип документа' },
    		{ emptyText	: 'выберите документ', fieldLabel: 'Документ' }
		]
	}
});