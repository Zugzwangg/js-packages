Ext.define('CFJS.locale.ru.document.panel.LayoutsExplorer', {
	override: 'CFJS.document.panel.LayoutsExplorer',
	config	: {
		newLayoutNameText	: 'Группа',
		title				: 'Отображение'
	},
	confirmRemove: function(name, fn) {
		return Ext.fireEvent('confirmlostdata', 'удалить группу "'+ name + '"', fn, this);
	}
});
