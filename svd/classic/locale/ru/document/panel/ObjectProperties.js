Ext.define('CFJS.locale.ukr.document.panel.ObjectProperties', {
	override: 'CFJS.document.panel.ObjectProperties',
	config	: {
		actions	: {
			createProcess	: { tooltip: 'Создать процесс' },
			saveProperties	: { tooltip: 'Записать внесеные изменения' }
		},
		bind	: { title: 'Свойства объекта: {_itemName_.name}' }
	},
	descriptionTabConfig: {
		items		: [{
			items: [{ fieldLabel: 'Код' },{ fieldLabel: 'Название' }]
		},{
			items: [{ fieldLabel: 'Создано' },{ fieldLabel: 'Обновлено' }]
		},{
			items: [{ fieldLabel: 'Автор' }]
		},{
			items: [{ emptyText: 'выберите подразделение', fieldLabel: 'Подразделение' },{ emptyText: 'выберите сектор', fieldLabel: 'Сектор' }]
		},{
			items: [{ fieldLabel: 'Начало действия' },{ fieldLabel: 'Конец действия' }]
		},{
			nameText: 'Действие', title: 'Права доступа', valueText: 'Уровень'
		}],
		tabConfig	: { tooltip: 'Описание' }
	},
	processTabConfig	: { tabConfig: { tooltip: 'План мероприятий' } },
	resourcesTabConfig	: { tabConfig: { tooltip: 'Вложения' }, title: 'Вложения' },
	signatoriesTabConfig: { tabConfig: { tooltip: 'Подписанты' }, title: 'Подписанты' }
});
