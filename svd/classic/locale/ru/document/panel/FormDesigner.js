Ext.define('CFJS.locale.ru.document.panel.FormDesigner', {
	override: 'CFJS.document.panel.FormDesigner',
	config	: {
		formConfig		: { title: 'Основное' },
		formPreviewTitle: 'Форма предварительного ппросмотра',
		formRefreshText	: 'Обновить данные формы',
    	refreshDataText	: 'обновить данные формы',
		title			: 'Редактор формы'
    }
});
