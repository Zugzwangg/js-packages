Ext.define('CFJS.locale.ru.document.panel.ObjectEditor', {
	override: 'CFJS.document.panel.ObjectEditor',
	config	: {
		actions	: {
			exportRecord	: { tooltip: 'Экспортировать текущую запись в Excel' },
			printRecord		: { tooltip: 'Печатать текущую запись' },
			showProperties	: { tooltip: 'Показать свойства текущей записи' },
			signRecord		: { tooltip: 'Подписать текущую запись' }
		}
	}
});
