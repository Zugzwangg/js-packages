Ext.define('CFJS.locale.ru.document.panel.FieldsExplorer', {
	override: 'CFJS.document.panel.FieldsExplorer',
	config	: { title: 'Поля ввода' },
	confirmRemove: function(name, fn) {
		return Ext.fireEvent('confirmlostdata', 'удалить поле "'+ name + '"', fn, this);
	}
});
