Ext.define('CFJS.locale.ru.document.panel.DocumentEditor', {
    override: 'CFJS.document.panel.DocumentEditor',
    config	: {
    	actions: {
    		back			: { tooltip: 'Назад к списку документов' },
    		addRecord		: { tooltip: 'Добавить новый документ' },
			exportRecord	: { tooltip: 'Экспортировать текущий документ в Excel' },
			printRecord		: { tooltip: 'Печатать текущий документ' },
    		removeRecord	: { tooltip: 'Удалить текущий документ' },
			showProperties	: { tooltip: 'Показать свойства текущего документа' },
    		signRecord		: { tooltip: 'Подписать текущий документ'}
    	}
    }
});
