Ext.define('CFJS.locale.ru.admin.grid.StrictBlankPanel', {
	override	: 'CFJS.admin.grid.StrictBlankPanel',
	config		: {
		actions			: {
			addRecord	: {
				title	: 'Выпуск новых бланков',
				tooltip	: 'Выпуск новых бланков строгой отчетности'
			},
			moveRecord	: {
				title	: 'Перемещение бланков',
				tooltip	: 'Перемещение выбранных бланков'
			},
			removeRecord: {
				title	: 'Аннулирование бланков',
				tooltip	: 'Аннулирование выбранных бланков'
			}
		},
		title			: 'Хранилище бланков строгой отчетности'
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns			: [{
				filter		: { emptyText: 'Имя агента' },
				text		: 'Агент'
			},{
				filter		: { emptyText: 'Наименование формы' },
				text		: 'Форма'
			},{
				filter		: { emptyText: 'серия' },
				text		: 'Серия'
			},{
				filter		: { emptyText: 'номер' },
				text		: 'Номер'
			},{
				text		: 'Состояние'
			}]
		});
		me.callParent();
	}
});