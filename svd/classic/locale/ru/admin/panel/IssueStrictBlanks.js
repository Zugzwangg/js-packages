Ext.define('CFJS.locale.ru.admin.panel.IssueStrictBlanks', {
	override	: 'CFJS.admin.panel.IssueStrictBlanks',
	config		: {
		processToolText		: 'Выпустить все пакеты',
		title				: 'Выпуск новых бланков'
	}
});