Ext.define('CFJS.locale.ru.admin.panel.StrictBlankSelector', {
	override		: 'CFJS.admin.panel.StrictBlankSelector',
	addToolText		: 'Добавить пакет',
	backToolText	: 'Назад к списку бланков',
	emptyText		: 'Нет данных для отображения',
	processToolText	: 'Обработать все пакеты',
	removeRowTip	: 'Удалить этот элемент',
	
	renderColumns: function() {
    	var columns = CFJS.apply(this.callParent(), [{},
    		{ editor: { emptyText: 'выберите форму' }, text: 'Форма' },
    		{ text: 'Серия' },
    		{ text: 'Номер' }
    	]);
    	return columns;
    }
	
});