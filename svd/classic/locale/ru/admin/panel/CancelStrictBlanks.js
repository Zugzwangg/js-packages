Ext.define('CFJS.locale.ru.admin.panel.CancelStrictBlanks', {
	override: 'CFJS.admin.panel.CancelStrictBlanks',
	config	: {
		promptButton	: { text: 'OK' },
		promptDlg		: { title: 'Введите причину аннулирования бланков' },
		promptField		: { emptyText: 'введите причину', fieldLabel: 'причина' },
		processToolText	: 'Отменить все пакеты',
		title			: 'Отменить бланк'
	},
	
	renderColumns: function() {
    	var columns = this.callParent(arguments);
    	CFJS.merge(columns[columns.length - 2], {
    		editor	: { emptyText: 'выберите состояние' },
    		text	: 'Состояние'
    	});
    	return columns;
    }
});