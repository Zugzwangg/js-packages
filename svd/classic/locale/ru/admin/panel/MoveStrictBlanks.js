Ext.define('CFJS.locale.ru.admin.panel.MoveStrictBlanks', {
	override	: 'CFJS.admin.panel.MoveStrictBlanks',
	config		: {
		promptButton	: { text: 'Выбрать' },
		promptDlg		: { title: 'Выберите сообщение' },
		promptField		: { emptyText: 'Выберите сообщение', fieldLabel: 'Cообщение' },
		processToolText	: 'Переместить все пакеты',
		title			: 'Переместить бланки',
	},
	
	renderColumns: function() {
    	var columns = this.callParent(arguments);
    	columns[columns.length - 2].text = 'Состояние';
    	return columns;
    }
	
});