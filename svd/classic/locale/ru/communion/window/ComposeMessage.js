Ext.define('CFJS.locale.ru.communion.window.ComposeMessage', {
	override: 'CFJS.communion.window.ComposeMessage',
	config	: {
		actions	: {
			attachments	: { tooltip: 'Вложения' },
			priority	: { tooltip: 'Приоритет' },
			sendMessage	: { tooltip: 'Послать' }
		},
		title	: 'Написать сообщение'
	},
	createItems: function(me, vm) {
		var me = this, items = me.callParent(arguments);
    	CFJS.merge( items, [{ fieldLabel: 'Пользователи' },{ fieldLabel: 'Клиенты' },{ fieldLabel: 'Отправить по электронной почте' }]);
		return items;
	}
});
