Ext.define('CFJS.locale.ru.communion.window.ComposeDiscussion', {
	override: 'CFJS.communion.window.ComposeDiscussion',
	config	: {	title: 'Начать обсуждение'},
	createItems: function(me, vm) {
		var me = this, items = me.callParent(arguments);
    	CFJS.merge( items, [{ fieldLabel: 'Пользователи' },{ fieldLabel: 'Клиенты' },{ fieldLabel: 'Тема' },{ fieldLabel: 'Общее обсуждение' }]);
		return items;
	}
});
