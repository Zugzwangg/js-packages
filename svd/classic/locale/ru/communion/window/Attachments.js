Ext.define('CFJS.locale.ru.communion.window.Attachments', {
	override			: 'CFJS.communion.window.Attachments',
	config				: {	title: 'Вложения'},
    documentPanelConfig	: {
    	items	: [
    		{ emptyText	: 'выберите тип документа', fieldLabel: 'Тип документа' },
    		{ emptyText	: 'выберите документ', fieldLabel: 'Документ' },
    		{ tooltip	: 'Добавить закладку к документу' }
       	],
		tooltip	: 'Закладка к документу'
    },
    filePanelConfig		: {
    	items	: [
    		{ buttonConfig	: { tooltip: 'Выберите файл' }, emptyText: 'выберите файл', fieldLabel: 'Файл для загрузки' },
    		{ tooltip		: 'Добавить файл' }
       	],
    	tooltip	: 'Загрузить файл'
    },
    gridConfig			: {
    	removeRowTip: 'Удалить этот элемент',
    	viewConfig	: { emptyText: 'Нет данных для отображения' }
    },
    saveText			: 'Записать изменения',
	saveToolText		: 'Записать внесенные изменения',
    uploadingText		: 'Загрузка файла...',
    urlPanelConfig		: {
    	items	: [
    		{ emptyText	: 'укажите название для гиперссылки', fieldLabel: 'Название' },
    		{ emptyText	: 'укажите нужный URL, например: http://google.com', fieldLabel: 'Гиперссылка'},
    		{ tooltip	: 'Добавить внешнюю ссылку'}
    	],
    	tooltip	: 'Внешняя ссылка'
    }
});