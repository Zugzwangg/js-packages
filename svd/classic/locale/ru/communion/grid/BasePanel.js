Ext.define('CFJS.locale.ru.communion.grid.BasePanel', {
	override: 'CFJS.communion.grid.BasePanel',
	config	: {
		actions: {
			attachments	: { tooltip: 'Вложения' },
			back		: { tooltip: 'Назад' }
		}
	},
	whomLabel: 'Кому: '
});
