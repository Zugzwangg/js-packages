Ext.define('CFJS.locale.ru.communion.grid.DiscussionViewPanel', {
	override: 'CFJS.communion.grid.DiscussionViewPanel',
	config	: {
		actions: {
			compose	: { tooltip: 'Написать сообщение' },
			finish	: { tooltip: 'Завершить обсуждение' }
		}
	}
});
