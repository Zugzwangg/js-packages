Ext.define('CFJS.locale.ru.communion.grid.MessagePanel', {
	override: 'CFJS.communion.grid.MessagePanel',
	config	: { title: 'Сообщения' },
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{},{ text: 'Автор', filter: { emptyText: 'имя автора' } },{ text: 'Текст', filter: { emptyText: 'текст сообщения' } },{},{ text: 'Создан' }]
		});
		me.callParent();
	}
});
