Ext.define('CFJS.locale.ru.communion.grid.MessageViewPanel', {
	override: 'CFJS.communion.grid.MessageViewPanel',
	config	: {
		actions: {
			forward	: { tooltip: 'Переслать' },
			reply	: { tooltip: 'Ответить' },
			replyAll: { tooltip: 'Ответить всем' },
			tasks	: { tooltip: 'Задания' }
		}
	}
});
