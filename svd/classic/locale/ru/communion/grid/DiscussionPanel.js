Ext.define('CFJS.locale.ru.communion.grid.DiscussionPanel', {
	override: 'CFJS.communion.grid.DiscussionPanel',
	config	: { title: 'Обсуждение' },
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns: [{},{ text: 'Автор', filter: { emptyText: 'имя автора' } },{ text: 'Тема', filter: { emptyText: 'тема обсуждения' } },{},{ text: 'Создано' },{ text: 'Завершено' }]
		});
		me.callParent();
	}
});
