Ext.define('CFJS.locale.ru.pages.ObjectBrowser', {
	override: 'CFJS.pages.ObjectBrowser',
	config	: {
		actions		: {
			addRecord		: { title: 'Добавить запись', tooltip: 'Добавить новую запись' },
			copyRecord		: { title: 'Скопировать запись', tooltip: 'Скопировать текущую запись' },
			clearFilters	: { title: 'Очистить фильтры', tooltip: 'Очистить все фильтры' },
			editRecord		: { title: 'Редaктировать запись', tooltip: 'Редaктировать текущую запись' },
			exportRecord	: { title: 'Экспортировать в Excel', tooltip: 'Экспортировать выделенные записи в Excel' },
			periodPicker	: { title: 'Рабочий период', tooltip: 'Выбор рабочего периода' },
			printRecord		: { title: 'Печатать записи', tooltip: 'Печатать выделенные записи' },
			reloadStore		: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeRecord	: { title: 'Удалить записи', tooltip: 'Удалить выделенные записи' },
			showProperties	: { title: 'Показать свойства', tooltip: 'Показать свойства текущей записи' },
			signRecord		: { title: 'Подписать записи', tooltip: 'Подписать выделенные записи' }
		},
		searchText	: 'Поиск'
	}
});
