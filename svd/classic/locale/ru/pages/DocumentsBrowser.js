Ext.define('CFJS.locale.ru.pages.DocumentsBrowser', {
	override: 'CFJS.pages.DocumentsBrowser',
	config	: {
		actions			: {
			addRecord		: { title: 'Добавить документ', tooltip: 'Добавить новый документ' },
			copyRecord		: { title: 'Скопировать документ', tooltip: 'Скопировать текущий документ' },
			editRecord		: { title: 'Редактировать документ', tooltip: 'Редактировать текущий документ' },
			exportRecord	: { title: 'Экспортировать в Excel', tooltip: 'Экспортировать выделенные документы в Excel' },
			printRecord		: { title: 'Печатать документы', tooltip: 'Печатать выделенные документы' },
			removeRecord	: { title: 'Удалить документы', tooltip: 'Удалить выделенные документы' },
			shareDocuments	: { title: 'Поделиться документами', tooltip: 'Поделиться выбранными документами' },
			showProperties	: { title: 'Показать свойства', tooltip: 'Показать свойства текущего документа' },
			signRecord		: { title: 'Подписать документы', tooltip: 'Подписать выделенные документы' }
		},
		formGroupAllText: 'Все документы',
		shareAsText		: 'Поделиться {0}-файлом',
		treeFilterTitle	: 'Группы документов',
		title			: 'Мои документы'
	}
});
