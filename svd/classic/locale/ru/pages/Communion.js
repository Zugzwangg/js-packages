Ext.define('CFJS.locale.ru.pages.Communion', {
	override: 'CFJS.pages.Communion',
	config	: {
		actions		: {
			addTask				: { title: 'Создать новую задачу', tooltip: 'Создать новую задачу' },
			composeDiscussion	: { title: 'Создать новое обсуждение', tooltip: 'Создать новое обсуждение' },
			composeMessage		: { title: 'Создать новое сообщение', tooltip: 'Создать новое сообщение' },
			periodPicker		: { title: 'Рабочий период', tooltip: 'Установить рабочий период' },
			reloadStore			: { title: 'Обновить данные', tooltip: 'Обновить данные' }
		},
		searchText	: 'Поиск',
		title		: 'Общение'
	}
});
