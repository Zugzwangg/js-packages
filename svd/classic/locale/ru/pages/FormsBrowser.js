Ext.define('CFJS.locale.ru.pages.FormsBrowser', {
	override: 'CFJS.pages.FormsBrowser',
	config	: {
		actions		: {
			addRecord		: { title: 'Добавить форму', tooltip: 'Добавить новую форму' },
			exportRecord	: { title: 'Экспортировать форму', tooltip: 'Экспортировать текущую форму' },
			filtersPanel	: { title: 'Показать фильтры', tooltip: 'Показать текущие фильтры' },
			removeRecord	: { title: 'Удалить форму', tooltip: 'Удалить текущую форму' },
			refreshRecord	: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			saveRecord		: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' }
		},
		closeTabText: 'закрыть вкладку',
		title		: 'Дизайнер форм'
	},
	confirmRemove: function(formName, fn) {
		return Ext.fireEvent('confirmlostdata', 'удалить форму "'+ formName + '"', fn, this);
	}
});
