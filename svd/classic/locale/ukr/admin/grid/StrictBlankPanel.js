Ext.define('CFJS.locale.ukr.admin.grid.StrictBlankPanel', {
	override	: 'CFJS.admin.grid.StrictBlankPanel',
	config		: {
		actions			: {
			addRecord	: {
				title	: 'Випуск нових бланків',
				tooltip	: 'Випуск нових бланків суворої звітності'
			},
			moveRecord	: {
				title	: 'Переміщення бланків',
				tooltip	: 'Переміщення вибраних бланків'
			},
			removeRecord: {
				title	: 'Анулювання бланків',
				tooltip	: 'Анулювання обраних бланків'
			}
		},
		title			: 'Сховище бланків суворої звітності'
	},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns			: [{
				filter		: { emptyText: 'Им\'я агента' },
				text		: 'Агент'
			},{
				filter		: { emptyText: 'Найменування форми' },
				text		: 'Форма'
			},{
				filter		: { emptyText: 'серія' },
				text		: 'Серія'
			},{
				filter		: { emptyText: 'номер' },
				text		: 'Номер'
			},{
				text		: 'Стан'
			}]
		});
		me.callParent();
	}
});