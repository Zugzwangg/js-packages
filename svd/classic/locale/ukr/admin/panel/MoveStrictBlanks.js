Ext.define('CFJS.locale.ukr.admin.panel.MoveStrictBlanks', {
	override	: 'CFJS.admin.panel.MoveStrictBlanks',
	config		: {
		promptButton	: { text: 'Вибрати' },
		promptDlg		: { title: 'Виберіть повідомлення' },
		promptField		: { emptyText: 'Виберіть повідомлення', fieldLabel: 'Повідомлення' },
		processToolText	: 'Перемістити все пакети',
		title			: 'Перемістити бланки',
	},
	
	renderColumns: function() {
    	var columns = this.callParent(arguments);
    	columns[columns.length - 2].text = 'Стан';
    	return columns;
    }
	
});