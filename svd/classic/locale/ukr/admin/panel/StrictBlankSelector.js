Ext.define('CFJS.locale.ukr.admin.panel.StrictBlankSelector', {
	override		: 'CFJS.admin.panel.StrictBlankSelector',
	addToolText		: 'Додати пакет',
	backToolText	: 'Назад до списку бланків',
	emptyText		: 'Дані для відображення відсутні',
	processToolText	: 'Обробити всі пакети',
	removeRowTip	: 'Видалити цей елемент',
    
	renderColumns: function() {
    	var columns = CFJS.apply(this.callParent(), [{},
    		{ editor: { emptyText: 'виберіть форму' }, text: 'Форма' },
    		{ text: 'Серія' },
    		{ text: 'Номер' }
    	]);
    	return columns;
    }
	
});