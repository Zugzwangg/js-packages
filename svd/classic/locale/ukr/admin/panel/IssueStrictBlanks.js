Ext.define('CFJS.locale.ukr.admin.panel.IssueStrictBlanks', {
	override	: 'CFJS.admin.panel.IssueStrictBlanks',
	config		: {
		processToolText	: 'Випустити всі пакети',
		title			: 'Випуск нових бланків'
	}
});