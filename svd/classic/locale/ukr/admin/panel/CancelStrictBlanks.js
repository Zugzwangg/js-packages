Ext.define('CFJS.locale.ukr.admin.panel.CancelStrictBlanks', {
	override: 'CFJS.admin.panel.CancelStrictBlanks',
	config	: {
		promptButton	: { text: 'OK' },
		promptDlg		: { title: 'Введіть причину анулювання бланків' },
		promptField		: { emptyText: 'введіть причину', fieldLabel: 'причина' },
		processToolText	: 'Скасувати всі пакети',
		title			: 'Скасувати бланк'
	},
	
	renderColumns: function() {
    	var columns = this.callParent(arguments);
    	CFJS.merge(columns[columns.length - 2], {
    		editor	: { emptyText: 'виберіть стан' },
    		text	: 'Стан'
    	});
    	return columns;
    }
});