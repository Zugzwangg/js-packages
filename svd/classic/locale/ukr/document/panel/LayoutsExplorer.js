Ext.define('CFJS.locale.ukr.document.panel.LayoutsExplorer', {
    override: 'CFJS.document.panel.LayoutsExplorer',
    config	: {
	    newLayoutNameText	: 'Група',
	    title				: 'Відображення'
    },
	confirmRemove: function(name, fn) {
		return Ext.fireEvent('confirmlostdata', 'видалити групу "'+ name + '"', fn, this);
	}
});
