Ext.define('CFJS.locale.ukr.document.panel.FieldsExplorer', {
    override: 'CFJS.document.panel.FieldsExplorer',
    config	: {
        title	: 'Поля вводу'
    },
    confirmRemove: function(name, fn) {
		return Ext.fireEvent('confirmlostdata', 'видалити поле "'+ name + '"', fn, this);
	}
});
