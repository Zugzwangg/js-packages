Ext.define('CFJS.locale.ukr.document.panel.TemplateEditor', {
    override: 'CFJS.document.panel.TemplateEditor',
    config	: { excelTitle: 'Шаблон експорту', pdfTitle: 'Шаблон друку' }
});
