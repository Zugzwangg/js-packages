Ext.define('CFJS.locale.ukr.document.panel.ObjectEditor', {
    override: 'CFJS.document.panel.ObjectEditor',
    config	: {
    	actions	: {
    		exportRecord	: { tooltip: 'Експортувати поточний запис в Excel' },
			printRecord		: { tooltip: 'Друкувати поточний запис' },
			showProperties	: { tooltip: 'Показати властивості поточного запису' },
    		signRecord		: { tooltip: 'Підписати поточний запис' }
    	}
    }
});
