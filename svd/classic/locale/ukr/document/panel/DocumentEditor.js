Ext.define('CFJS.locale.ukr.document.panel.DocumentEditor', {
    override: 'CFJS.document.panel.DocumentEditor',
    config	: {
    	actions: {
    		back			: { tooltip: 'Назад до списку документів' },
    		addRecord		: { tooltip: 'Додати новий документ' },
    		exportRecord	: { tooltip: 'Експортувати поточний документ в Excel' },
			printRecord		: { tooltip: 'Друкувати поточний документ' },
    		removeRecord	: { tooltip: 'Видалити поточний документ' },
			showProperties	: { tooltip: 'Показати властивості поточного документу' },
    		signRecord		: { tooltip: 'Підписати поточний документ' }
    	}
    }
});
