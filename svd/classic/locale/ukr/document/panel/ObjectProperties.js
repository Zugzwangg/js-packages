Ext.define('CFJS.locale.ukr.document.panel.ObjectProperties', {
	override: 'CFJS.document.panel.ObjectProperties',
	config	: {
		actions	: {
			createProcess	: { tooltip: 'Створити процес' },
			saveProperties	: { tooltip: 'Записати внесені зміни' }
		},
		bind	: { title: 'Властивості об\'єкта: {_itemName_.name}' }
	},
	descriptionTabConfig: {
		items		: [{
			items: [{ fieldLabel: 'Код' },{ fieldLabel: 'Назва' }]
		},{
			items: [{ fieldLabel: 'Створено' },{ fieldLabel: 'Оновлено' }]
		},{
			items: [{ fieldLabel: 'Автор' }]
		},{
			items: [{ emptyText: 'оберіть підрозділ', fieldLabel: 'Підрозділ' },{ emptyText: 'оберіть сектор', fieldLabel: 'Сектор' }]
		},{
			items: [{ fieldLabel: 'Початок дії' },{ fieldLabel: 'Кінець дії' }]
		},{
			nameText: 'Дія', title: 'Права доступу', valueText: 'Рівень'
		}],
		tabConfig	: { tooltip: 'Опис' }
	},
	processTabConfig	: { tabConfig: { tooltip: 'План заходів' } },
	resourcesTabConfig	: { tabConfig: { tooltip: 'Вкладення' }, title: 'Вкладення' },
	signatoriesTabConfig: { tabConfig: { tooltip: 'Підписанти' }, title: 'Підписанти' }
});
