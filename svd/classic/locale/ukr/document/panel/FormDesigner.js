Ext.define('CFJS.locale.ukr.document.panel.FormDesigner', {
    override: 'CFJS.document.panel.FormDesigner',
    config	: {
		formConfig		: { title: 'Основне' },
		formPreviewTitle: 'Форма попереднього перегляду',
		formRefreshText	: 'Оновити дані форми',
		refreshDataText	: 'оновити дані форми',
		title			: 'Редактор форми'
    }
});
