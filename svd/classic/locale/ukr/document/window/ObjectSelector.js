Ext.define('CFJS.locale.ukr.document.window.ObjectSelector', {
	override			: 'CFJS.document.window.ObjectSelector',
	config				: { title: 'Вибір документа' },
	documentPanelConfig	: {
		buttons	: [{ text: 'Вибрати' }],
		items	: [
			{ emptyText	: 'оберіть тип документа', fieldLabel: 'Тип документа' },
    		{ emptyText	: 'оберіть документ', fieldLabel: 'Документ' }
		]
	}
});