Ext.define('CFJS.locale.ukr.document.view.ObjectsView', {
	override: 'CFJS.document.view.ObjectsView',
	filterPanelConfig: {
		items: [
			{ emptyText: 'виберіть сектор', fieldLabel: 'Сектор' },
			{ emptyText: 'виберіть підрозділ', fieldLabel: 'Підрозділ' }
		],
		title: 'Додаткові фільтри'
	}
});