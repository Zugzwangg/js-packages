Ext.define('CFJS.locale.ukr.document.grid.DocumentJournal', {
	override			: 'CFJS.document.grid.DocumentJournal',
	config				: {
		actions	: { addDocument: { tooltip: 'Додати документи' } },
		title	: 'Документи'
	},
	emptyText			: 'Дані для відображення відсутні',
	nameText			: 'Журнал документів',
	removeText			: 'Видалити обрані документи',
	sameDocumentTitle	: 'Увага',
	sameDocumentText	: 'Обраний документ вже є в списку.',
	confirmRemove		: function(docName, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити документ "' + docName + '"?', fn, this);
	}
});