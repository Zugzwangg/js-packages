Ext.define('CFJS.locale.ukr.document.grid.FieldsPicker', {
	override			: 'CFJS.document.grid.FieldsPicker',
	config	: {
		actions: {
			levelUp		: { tooltip: 'Назад до попередньої форми' },
			reloadStore	: { tooltip: 'Оновити дані' }
		},
		title: 'Вибір поля'
	},
	initComponent: function() {
		var me = CFJS.apply(this, {
			columns		: [{ text: 'Код' },{ text: 'Назва' },{ text: 'Тип' }],
			datatipTpl	: [
				'<b>Код</b>: {id}</br>',
				'<b>Назва</b>: {name}</br>',
				'<b>Тип</b>: {[this.formatType(values.type)]}'
			],
			emptyText	: 'Дані для відображення відсутні',
			tbar		: [{ fieldLabel: 'Шлях' }],
		});
		me.callParent();
	}
});