Ext.define('CFJS.locale.ukr.document.grid.SignatoriesPanel', {
	override			: 'CFJS.document.grid.SignatoriesPanel',
	config				: { title: 'Підписанти' },
	datatipTpl			: [
		'<b>Код:</b> {id}</br>',
		'<tpl if="phase"><b>Фаза:</b> {[this.phaseRenderer(values.phase)]}</br></tpl>',
		'<tpl if="signer"><b>Підписант:</b> {signer.name}</br></tpl>',
		'<tpl if="resolution"><b>Резолюція:</b> {resolution}</br></tpl>',
		'<b>Мітка часу:</b> {timestamp:date("' + Ext.Date.patterns.LongDateTime + '")}',
	],
	emptyText			: 'Дані для відображення відсутні',
	removeText			: 'Видалити вибраний підпис',
	verifySignatureMsg	: 'Підпис успішно підтверджено!',
	verifySignatureText	: 'Перевірити вибраний підпис',
	verifySignatureTitle: 'Перевірка підпису',
	confirmRemove		: function(signerName, fn) {
		return Ext.fireEvent('confirm', 'Ви впевнені, що хочете видалити підпис "' + signerName + '"?', fn, this);
	},
	renderColumns: function() {
		return CFJS.apply(this.callParent(arguments), [{},{ text: 'Фаза' },{ filter: { emptyText: 'ім\'я підписанта' }, text: 'Підписант' }]);
	}
	
});
