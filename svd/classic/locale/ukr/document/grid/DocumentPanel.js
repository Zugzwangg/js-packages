Ext.define('CFJS.locale.ukr.document.grid.DocumentPanel', {
	override	: 'CFJS.document.grid.DocumentPanel',
	config		: {
		actions		: {
			addRecord	: { title: 'Додати документ', tooltip: 'Додати новий документ' },
			editRecord	: { title: 'Редагувати документ', tooltip: 'Редагувати поточний документ' },
			exportRecord: { title: 'Експортувати в Excel', tooltip: 'Експортувати вибрані документи в Excel' },
			printRecord	: { title: 'Друкувати документ', tooltip: 'Друкувати вибрані документы' },
			removeRecord: { title: 'Видалити документ', tooltip: 'Видалити вибрані документи' },
			signRecord	: { title: 'Підписати документ', tooltip: 'Підписати вибрані документи' }
		}
	},
	promptButton: { text: 'Друкувати' },
	promptDlg	: { title: 'Друк на бланці' },
	promptField	: { emptyText: 'оберіть бланк', fieldLabel: 'Вибір бланків суворої звітності' }
});
