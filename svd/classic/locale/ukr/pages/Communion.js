Ext.define('CFJS.locale.ukr.pages.Communion', {
	override: 'CFJS.pages.Communion',
	config	: {
		actions		: {
			addTask				: { title: 'Створити нову задачу', tooltip: 'Створити нову задачу' },
			composeDiscussion	: { title: 'Створити нову тему', tooltip: 'Створити нову тему' },
			composeMessage		: { title: 'Створити нове повідомлення', tooltip: 'Створити нове повідомлення' },
			periodPicker		: { title: 'Робочий період', tooltip: 'Встановити робочий період' },
			reloadStore			: { title: 'Оновити дані', tooltip: 'Оновити дані' }
		},
		searchText	: 'Пошук',
		title		: 'Спілкування'
	}
});
