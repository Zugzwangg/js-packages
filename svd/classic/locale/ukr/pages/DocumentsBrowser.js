Ext.define('CFJS.locale.ukr.pages.DocumentsBrowser', {
	override: 'CFJS.pages.DocumentsBrowser',
	config	: {
		actions			: {
			addRecord		: { title: 'Додати документ', tooltip: 'Додати новий документ' },
			copyRecord		: { title: 'Скопіювати документ', tooltip: 'Скопіювати поточний документ' },
			editRecord		: { title: 'Редагувати документ', tooltip: 'Редагувати поточний документ' },
			exportRecord	: { title: 'Експортувати в Excel', tooltip: 'Експортувати вибрані документи в Excel' },
			printRecord		: { title: 'Друкувати документи', tooltip: 'Друкувати вибрані документи' },
			removeRecord	: { title: 'Видалити документи', tooltip: 'Видалити вибрані документи' },
			shareDocuments	: { title: 'Поділитися документами', tooltip: 'Поділитися вибраними документами' },
			showProperties	: { title: 'Показати властивості', tooltip: 'Показати властивості поточного документу' },
			signRecord		: { title: 'Підписати документи', tooltip: 'Підписати вибрані документи' }
		},
		formGroupAllText: 'Всі документи',
		shareAsText		: 'Поділитися {0}-файлом',
		treeFilterTitle	: 'Групи документів',
		title			: 'Мої документи'
	}
});
