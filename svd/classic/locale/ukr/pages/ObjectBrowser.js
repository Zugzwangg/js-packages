Ext.define('CFJS.locale.ukr.pages.ObjectBrowser', {
	override: 'CFJS.pages.ObjectBrowser',
	config	: {
		actions		: {
			addRecord		: { title: 'Додати запис', tooltip: 'Додати новий запис' },
			copyRecord		: { title: 'Скопіювати запис', tooltip: 'Скопіювати поточний запис' },
			clearFilters	: { title: 'Очистити фільтри', tooltip: 'Видалити всі фільтри' },
			editRecord		: { title: 'Редагувати запис', tooltip: 'Редагувати поточний запис' },
			exportRecord	: { title: 'Експортувати в Excel', tooltip: 'Експортувати вибрані записи в Excel' },
			periodPicker	: { title: 'Робочий період', tooltip: 'Вибір робочого періоду' },
			printRecord		: { title: 'Друкувати записи', tooltip: 'Друкувати вибрані записи' },
			reloadStore		: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeRecord	: { title: 'Видалити записи', tooltip: 'Видалити обрані записи' },
			showProperties	: { title: 'Показати властивості', tooltip: 'Показати властивості поточного запису' },
			signRecord		: { title: 'Підписати записи', tooltip: 'Підписати вибрані записи' }
		},
		searchText	: 'Пошук'
	}
});
