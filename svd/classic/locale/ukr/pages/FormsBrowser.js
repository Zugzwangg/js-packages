Ext.define('CFJS.locale.ukr.pages.FormsBrowser', {
	override: 'CFJS.pages.FormsBrowser',
	config	: {
		actions		: {
			addRecord		: { title: 'Додати форму', tooltip: 'Додати нову форму' },
			exportRecord	: { title: 'Експортувати форму', tooltip: 'Експортувати поточну форму' },
			filtersPanel	: { title: 'Показати фільтри', tooltip: 'Показати поточні фільтри' },
			removeRecord	: { title: 'Видалити форму', tooltip: 'Видалити поточну форму' },
			refreshRecord	: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			saveRecord		: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' }
		},
		closeTabText: 'закрити вкладку',
		title		: 'Конструктор форм'
	},
	confirmRemove: function(formName, fn) {
		return Ext.fireEvent('confirmlostdata', 'видалити форму "'+ formName + '"', fn, this);
	}
});
