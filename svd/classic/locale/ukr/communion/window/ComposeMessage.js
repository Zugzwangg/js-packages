Ext.define('CFJS.locale.ukr.communion.window.ComposeMessage', {
	override: 'CFJS.communion.window.ComposeMessage',
	config	: {
		actions	: {
			attachments	: { tooltip: 'Вкладення' },
			priority	: { tooltip: 'Пріоритет' },
			sendMessage	: { tooltip: 'Надіслати' }
		},
		title	: 'Написати повідомлення'
	},
	createItems: function(me, vm) {
		var me = this, items = me.callParent(arguments);
    	CFJS.merge( items, [{ fieldLabel: 'Користувачі' },{ fieldLabel: 'Клієнти' },{ fieldLabel: 'Відправити електронною поштою' }]);
		return items;
	}
});
