Ext.define('CFJS.locale.ukr.communion.window.ComposeDiscussion', {
	override: 'CFJS.communion.window.ComposeDiscussion',
	config	: {	title: 'Почати обговорення'},
	createItems: function(me, vm) {
		var me = this, items = me.callParent(arguments);
    	CFJS.merge( items, [{ fieldLabel: 'Користувачі' },{ fieldLabel: 'Клієнти' },{ fieldLabel: 'Тема' },{ fieldLabel: 'Загальне обговорення' }]);
		return items;
	}
});
