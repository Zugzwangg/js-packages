Ext.define('CFJS.locale.ukr.communion.window.Attachments', {
	override			: 'CFJS.communion.window.Attachments',
	config				: {	title: 'Вкладення'},
    documentPanelConfig	: {
    	items	: [
    		{ emptyText	: 'оберіть тип документа', fieldLabel: 'Тип документа' },
    		{ emptyText	: 'оберіть документ', fieldLabel: 'Документ' },
    		{ tooltip	: 'Додати закладку до документа' }
       	],
		tooltip	: 'Закладка для документа'
    },
    filePanelConfig		: {
    	items	: [
    		{ buttonConfig	: { tooltip: 'Оберіть файл' }, emptyText: 'оберіть файл', fieldLabel: 'Файл для завантаження' },
    		{ tooltip		: 'Додати файл' }
       	],
    	tooltip	: 'Завантажити файл'
    },
    gridConfig			: {
    	removeRowTip: 'Видалити цей елемент',
    	viewConfig	: { emptyText: 'Дані для відображення відсутні' }
    },
    saveText			: 'Записати зміни',
	saveToolText		: 'Записати внесені зміни',
    uploadingText		: 'Завантаження файлу...',
    urlPanelConfig		: {
    	items	: [
    		{ emptyText	: 'вкажіть назву для гіперпосилання', fieldLabel: 'Назва' },
    		{ emptyText	: 'вкажіть потрібний URL, наприклад: http://google.com', fieldLabel: 'Гіперпосилання'},
    		{ tooltip	: 'Додати зовнішнє посилання'}
    	],
    	tooltip	: 'Зовнішнє посилання'
    }
});