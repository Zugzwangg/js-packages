Ext.define('CFJS.locale.ukr.communion.grid.DiscussionViewPanel', {
	override: 'CFJS.communion.grid.DiscussionViewPanel',
	config	: {
		actions: {
			compose	: { tooltip: 'Написати повідомлення' },
			finish	: { tooltip: 'Завершити обговорення' }
		}
	}
});
