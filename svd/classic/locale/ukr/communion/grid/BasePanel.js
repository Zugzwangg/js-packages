Ext.define('CFJS.locale.ukr.communion.grid.BasePanel', {
	override: 'CFJS.communion.grid.BasePanel',
	config	: {
		actions	: {
			attachments	: { tooltip: 'Вкладення' },
			back		: { tooltip: 'Назад' }
		}
	},
	whomLabel : 'Кому: '
});
