Ext.define('CFJS.locale.ukr.communion.grid.DiscussionPanel', {
	override: 'CFJS.communion.grid.DiscussionPanel',
	config	: { title: 'Обговорення'},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{},{ text: 'Автор', filter: { emptyText: 'им\'я автора' } },{ text: 'Тема', filter: { emptyText: 'тема обговорення' } },{},{ text: 'Створено' },{ text: 'Закінчено' }]
		});
		me.callParent();
	}
});
