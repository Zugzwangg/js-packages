Ext.define('CFJS.locale.ukr.communion.grid.MessageViewPanel', {
	override: 'CFJS.communion.grid.MessageViewPanel',
	config	: {
		actions: {
			forward	: { tooltip: 'Переслати' },
			reply	: { tooltip: 'Відповісти' },
			replyAll: { tooltip: 'Відповісти всім' },
			tasks	: { tooltip: 'Завдання' }
		}
	}
});
