Ext.define('CFJS.locale.ukr.communion.grid.MessagePanel', {
	override: 'CFJS.communion.grid.MessagePanel',
	config	: { title: 'Повідомлення'},
	initComponent: function() {
		var me = this;
		if (me.isFirstInstance) CFJS.merge(me, {
			columns	: [{},{ text: 'Автор', filter: { emptyText: 'им\'я автора' } },{ text: 'Текст', filter: { emptyText: 'текст повідомлення' } },{},{ text: 'Створено' }]
		});
		me.callParent();
	}
});
