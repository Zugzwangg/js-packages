Ext.define('CFJS.overrides.project.tab.ProcessElementDesigner', {
	override		: 'CFJS.project.tab.ProcessElementDesigner',
	requires		: [ 'CFJS.project.grid.ProcessDocumentJournal' ],
	documentsView	: true,
	
	createDocumentJournal: function(config) {
		var me = this,
			grid = CFJS.apply({
				xtype		: 'project-grid-process-document-journal',
				bind		: { readOnly: '{!itemRules.editable}' },
				margin		: 10,
				name		: 'documentJournal',
				userCls		: 'shadow-panel'
			}, config);
		return grid;

	},
	
	createProcessTaskView: function(config) {
		return this.callParent([Ext.apply(config||{}, { documentsView: this.documentsView })]);
	},
	
	initItems: function() {
		var me = this;
		if (me.items && !me.items.isMixedCollection && me.documentsView === true) {
			me.items.push(me.ensureChild('documentJournal'));
		}
		me.callParent();
	},

	lookupDocumentJournal: function() {
		return this.lookupChild('documentJournal');
	},
	
	onItemRulesChange: function(itemRules) {
		if (!itemRules.isTask) {			
			var me = this, activeTab = me.getActiveTab(),
				view = me.lookupDocumentJournal();
			me.hideChild(view, !itemRules.isProcess);
			me.hideChild((view||{}).tab, !itemRules.isProcess);
			me.setActiveTab(activeTab === view && view.isHidden() ? 0 : activeTab);
		}
		this.callParent(arguments);
	}

});