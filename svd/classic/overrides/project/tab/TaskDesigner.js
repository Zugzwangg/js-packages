Ext.define('CFJS.overrides.project.tab.TaskDesigner', {
	override		: 'CFJS.project.tab.TaskDesigner',
	requires		: [ 'CFJS.project.grid.ProcessDocumentJournal' ],
	documentsView	: true,

	createDocumentJournal: function(config) {
		var me = this,
			grid = CFJS.apply({
				xtype		: 'project-grid-process-document-journal',
				bind		: { readOnly: '{!itemRules.editable}' },
				margin		: 10,
				name		: 'documentJournal',
				userCls		: 'shadow-panel'
			}, config);
		return grid;

	},

	initItems: function() {
		var me = this;
		if (me.items && !me.items.isMixedCollection && me.documentsView === true) {
			me.items.push(me.ensureChild('documentJournal'));
		}
		me.callParent();
	},

	lookupDocumentJournal: function() {
		return this.lookupChild('documentJournal');
	}

});