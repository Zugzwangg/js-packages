Ext.define('CFJS.overrides.project.view.TaskView', {
	override: 'CFJS.project.view.TaskView',
	
	createTaskDesigner: function(config) {
		return this.callParent([Ext.apply(config||{}, { documentsView: true })]);
	}

});
