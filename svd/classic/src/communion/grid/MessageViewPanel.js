Ext.define('CFJS.communion.grid.MessageViewPanel', {
	extend				: 'CFJS.communion.grid.BasePanel',
	xtype				: 'communion.grid.message-view',
	requires			: [ 'CFJS.communion.grid.MessageHeader', 'CFJS.communion.view.MessageModel' ],
	actions				: {
		forward	: {
			action	: 'forward',
			iconCls	: CFJS.UI.iconCls.SHARE,
			tooltip	: 'Forward',
			handler	: 'onComposeMessage'
		},
		reply	: {
			action	: 'reply',
			iconCls	: 'x-fa fa-reply',
			tooltip	: 'Reply',
			handler	: 'onComposeMessage'
		},
		replyAll: {
			action	: 'replyAll',
			iconCls	: 'x-fa fa-reply-all',
			tooltip	: 'Reply all',
			handler	: 'onComposeMessage'
		},
		tasks	: {
			iconCls	: CFJS.UI.iconCls.TASKS,
			tooltip	: 'Tasks',
			handler	: 'onTasksView'
		}
	},
	config				: { message: null },
	viewModel			: { type: 'communion.message.edit' },
    
	applyMessage: function(message) {
		return this.lookupViewModel().setItem(message); 
	},
	
	createHeader: function() {
		var me = this, actions = me.getActions(),
			header = {
				xtype		: 'communion-grid-message-header',
				bind		: '{_itemName_}',
				items		: [ actions.reply, actions.replyAll, actions.forward, actions.attachments, actions.tasks ],
				grid		: this,
				whomLabel	: this.whomLabel
			};
		if (!me.rootGrid) {
			header.items.splice(1, 2);
			header.items.splice(0, 0, actions.back, '->');
		}
		return header;
	},

	onMessageChange: function(message) {
		if (message && message.isEntity) {
			var me = this, actions = me.actions, isAuthor = message.isAuthor();
			me.hideComponent(actions.reply, isAuthor);
			me.hideComponent(actions.replyAll, isAuthor);
			me.hideComponent(actions.tasks, !message.get('hasTasks'));
		}
		me.callParent(arguments);
	},
	
	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.hideComponent(actions.forward, readOnly);
		me.hideComponent(actions.reply, readOnly);
		me.hideComponent(actions.replyAll, readOnly);
		me.callParent(arguments);
	}
	
});