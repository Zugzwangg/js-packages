Ext.define('CFJS.communion.grid.BasePanel', {
	extend				: 'CFJS.grid.BasePanel',
	xtype				: 'communion-grid-base-panel',
	requires			: [ 'Ext.grid.plugin.RowWidget' ],
	config				: {
		actions	: {
			attachments: {
				iconCls	: CFJS.UI.iconCls.ATTACH,
				tooltip	: 'Attachments'
			},
			back: {
				iconCls	: CFJS.UI.iconCls.BACK,
				tooltip	: 'Back',
				handler	: 'onBackClick'
			}
		},
		level	: 0
	},
	bind				: { store: '{childMessages}' },
	cls					: 'communion-view-panel',
	columnLines			: false,
	contextMenu			: false,
	disableSelection	: true,
	selModel			: { type: 'rowmodel', mode: 'SINGLE', pruneRemoved: false },
	session				: { schema: 'communion' },
	whomLabel			: 'Whom: ',
	
	renderColumns: function() {
		return [{
			xtype	: 'templatecolumn',
			align	: 'center',
			tdCls	: 'author-photo',
			tpl		: '<tpl if="author"><img alt="author image" src="{author.photoUrl}" /></tpl>',
			width	: 60
		},{
			align		: 'left',
			dataIndex	: 'author',
			tdCls		: 'recipients',
			renderer	: function(value, metaData, record, rowIndex, colIndex) {
				var data = record.getData(true), i = 0,
					sb = [].concat(data.persons).concat(data.customers),
					html = '<h3 class="author-name">' + (value ? value.name : '') + '</h3>';
				for (; i < sb.length; i++) {
					if (Ext.isEmpty(sb[i].name)) {
						sb.splice(i, 1);
						i--;
					} else sb[i] = sb[i].name;
				}
				if (!Ext.isEmpty(sb)) html += '<div class="whom">' + this.whomLabel + ' ' + sb.join(', ') + '</div>';
				return html;
			},
			flex		: 1
		},{
			xtype		: 'templatecolumn',
			align		: 'center',
			tdCls		: 'message-attr',
			tpl			: [
				'<tpl if="priority != \'NORMAL\'"><span class="{colorCls} ' + CFJS.UI.iconCls.FLAG + '"></span></tpl>',
				'<tpl if="!Ext.isEmpty(attachments)"><span class="' + CFJS.UI.iconCls.ATTACH + '"></span></tpl>'
			],
			width		: 20
		},{
			xtype		: 'templatecolumn',
			align		: 'center',
			dataIndex	: 'created',
			format		: '<span class="' + CFJS.UI.iconCls.CLOCK + '"></span>' + Ext.Date.patterns.LongDateTime,
			tdCls		: 'created',
			tpl			: '<span class="' + CFJS.UI.iconCls.CLOCK + '"></span>{created:date("' + Ext.Date.patterns.ISO8601Long + '")}',
			width		: 150
		}];
	},
	
	createHeader: Ext.emptyFn,
	
	getRowClass: function(record, rowIndex, rowParams, store) {
		var cls = ['message-row'];
		if (!record.isAuthor() && !record.isViewed()) cls.push('boldFont');
		return cls.join(' ');
	},

	initComponent: function() {
		var me = Ext.apply(this, {
				dockedItems		: this.createHeader(),
				header			: false,
				hideHeaders		: true,
			}),
			rootGrid = me.rootGrid || me, rowwidget;
		me.columns = me.renderColumns();
		me.plugins = [];
		rowwidget = me.addPlugin({
			ptype			: 'rowwidget',
			columnWidth		: 'auto',
			expandOnDblClick: true,
			headerWidth		: 'auto',
			isFixedSize		: true,
			widget			: {
				xtype			: 'communion.grid.message-view',
				actionsScope	: me.actionsScope || me.lookupController(),
				composeWindow	: me.composeWindow,
				emptyText		: null,
				height			: 'auto',
				level			: me.getLevel() + 1,
				rootGrid		: rootGrid,
				scrollable		: false,
				viewModel		: { type: 'communion.message.edit' }
			}
		});
		me.callParent();
		me.lookupViewModel().bind('{_itemName_}', me.onMessageChange, me);
		me.on({ headerdblclick: function() { me.getStore().reload(); } });
		me.view.on({
			expandbody: function(rowNode, record, expandRow, widget) {
				widget.setMessage(record);
			},
			itemdblclick: function(view, record, row, rowIdx, e) {
				rowwidget.toggleRow(rowIdx, record);
			},
			resize: function(view, width, height, oldWidth, oldHeight) {
				view.updateLayout();
			}
		});
		me.setReadOnly(rootGrid.readOnly);
	},

	onMessageChange: function(message) {
		if (message && message.isEntity) {
			var me = this, action = me.actions.attachments,
				attachments = Ext.Array.from(message.get('attachments')),
				isEmpty = !(attachments.length > 0),
				i = 0, items = [], attachment;
			if (!isEmpty && action && action.isAction) {
				for (; i < attachments.length; i++ ) {
					attachment = attachments[i];
					items.push({
						handler		: 'onAttachmentsClick',
						iconCls		: attachment.iconCls,
						scope		: action.initialConfig.scope,
						attachment	: attachment,
						href		: attachment.url,
						hrefTarget	: '_blank',
						text		: attachment.name
					});
				}
				Ext.apply(action.initialConfig, { menu: items });
				action.each(function(component) {
					if (Ext.isFunction(component.setMenu)) component.setMenu(items, true);
				});
			}
			me.hideComponent(action, isEmpty);
		}
	},
	
	setReadOnly: function(readOnly) {
		var me = this, rowwidget = me.findPlugin('rowwidget');
		if (rowwidget) rowwidget.readOnly = readOnly; 
		me.callParent(arguments);
	}
	
});