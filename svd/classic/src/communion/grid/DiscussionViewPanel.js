Ext.define('CFJS.communion.grid.DiscussionViewPanel', {
	extend				: 'CFJS.communion.grid.BasePanel',
	xtype				: 'communion-grid-discussion-view-panel',
	requires			: [
		'CFJS.communion.grid.DiscussionHeader',
		'CFJS.communion.grid.MessageViewPanel',
		'CFJS.communion.view.DiscussionModel',
		'CFJS.communion.window.ComposeMessage'
	],
	actions				: {
		compose	: {
			action	: 'replyAll',
			iconCls	: CFJS.UI.iconCls.ENVELOPE,
			tooltip	: 'Compose message',
			handler	: 'onComposeMessage'
		},
		finish	: {
			iconCls	: CFJS.UI.iconCls.CANCEL,
			tooltip	: 'Finish discussion',
			handler	: 'onFinishDiscussion'
		}
	},
	composeWindow		: { xtype: 'communion-window-compose-message' },
	viewModel			: { type: 'communion.discussion.edit' },
	
	createHeader: function() {
		var me = this, actions = me.getActions();
		return {
			xtype		: 'communion-grid-discussion-header',
			bind		: '{_itemName_}',
			items		: [ actions.back, '->', actions.compose, actions.attachments, actions.finish ],
			grid		: this,
			whomLabel	: this.whomLabel
		};
	},
	
	onMessageChange: function(message) {
		if (message && message.isDiscussion) {
			var me = this, actions = me.actions, isAuthor = message.isAuthor();
			me.setReadOnly(message.get('finished'));
			me.hideComponent(actions.finish, me.readOnly || !isAuthor);
		}
		me.callParent(arguments);
	},

	setReadOnly: function(readOnly) {
		var me = this, actions = me.actions || {};
		me.hideComponent(actions.compose, readOnly);
		me.hideComponent(actions.finish, readOnly);
		me.callParent(arguments);
	}

});