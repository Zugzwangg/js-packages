Ext.define('CFJS.communion.grid.MessageHeader', {
	extend				: 'Ext.container.Container',
	xtype				: 'communion-grid-message-header',
	config				: { message: null },
	defaultBindProperty	: 'message',
	dock				: 'top',
	messageTpl			: [
		'<tpl if="isRoot">',
			'<img alt="author image" class="author-photo" src="{author.photoUrl}" />',
			'<div class="content-wrap">',
				'<div class="author-name-wrap">',
					'<div class="recipients">',
					'<h4 class="author-name">{author.name}</h4>',
					'<div class="whom">{%this.renderRecipients(out,values)%}</div>',
					'</div>',
					'<span class="created"><span class="' + CFJS.UI.iconCls.CLOCK + '"></span>{created:date("' + Ext.Date.patterns.ISO8601Long + '")}</span>',
				'</div>',
				'<div class="description">{description}</div>',
			'</div>',
		'<tpl else>',
			'<div class="description">{description}</div>',
		'</tpl>'
	],
	padding				: '10 10 5 10',
	renderTpl			: [
		'<div class="message-header" data-ref="msgInnerEl">',
			'{%this.renderContainer(out,values);%}',
		'</div>'
	],
	
	afterRender: function() {
		var me = this;
		me.callParent(arguments);
		me.toolBar.grid = me.ownerCt;
		me.on({
			element: 'el', 
			click: function() { me.ownerCt.fireEventArgs('headerclick', arguments); },
			dblclick: function() { me.ownerCt.fireEventArgs('headerdblclick', arguments); }
		});
	},
	
	applyMessage: function(message) {
		return message && message.isEntity ? message.getData(true) : message;
	},
	
	updateMessage: function(message) {
		var me = this, cmp = me.messageCmp, 
			isRoot = message && message.isRoot,
			el = me.layout.getRenderTarget() || me.el || me.protoEl;
		if (cmp && cmp.isComponent) {
			cmp.setData(message);
			if (isRoot) {
				el.setStyle('paddingRight', Ext.getScrollbarSize(true).width + 'px');
			} else {
				el.setStyle('paddingTop', 0);
			}
		}
	},
	
	initComponent: function() {
		var me = this;
		if (!me.messageCmp || !me.messageCmp.isComponent) {
			me.messageCmp = new Ext.Component(Ext.apply({
				tpl: Ext.apply(me.lookupTpl('messageTpl'), { whomLabel: me.whomLabel, renderRecipients: me.renderRecipients }) 
			}, me.messageCmp));
		}
		if (!me.toolBar || !me.toolBar.isToolbar) {
			me.toolBar = new Ext.toolbar.Toolbar(Ext.apply({
				cls			: 'toolbar-btn-header buttons-delete-focus',
				defaultType	: 'button',
				layout		: { pack: 'end' },
				padding		: 0,
				items		: me.items
			}, me.toolbar));
		}
		me.items = [ me.messageCmp, me.toolBar ];
		me.callParent();
	},

	renderRecipients: function(out, data) {
		var sb = [].concat(data.persons).concat(data.customers), i = 0;
		for (; i < sb.length; i++) {
			if (Ext.isEmpty(sb[i].name)) {
				sb.splice(i, 1);
				i--;
			} else sb[i] = sb[i].name;
		}
		out.push(this.whomLabel, ' ', sb.join(', '));
	}

});