Ext.define('CFJS.communion.grid.TaskPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'communion-grid-tasks',
	iconCls			: CFJS.UI.iconCls.TASKS,
	title			: 'Tasks',
});