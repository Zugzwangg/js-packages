Ext.define('CFJS.communion.grid.DiscussionHeader', {
	extend				: 'CFJS.communion.grid.MessageHeader',
	xtype				: 'communion-grid-discussion-header',
	messageTpl			: [
		'<img alt="author image" class="author-photo" src="{author.photoUrl}" />',
		'<div class="content-wrap">',
			'<div class="author-name-wrap">',
				'<div class="recipients">',
					'<h4 class="author-name">{author.name}</h4>',
					'<div class="whom">{%this.renderRecipients(out,values)%}</div>',
				'</div>',
				'<span class="created"><span class="' + CFJS.UI.iconCls.CLOCK + '"></span>{created:date("' + Ext.Date.patterns.ISO8601Long + '")}</span>',
			'</div>',
			'<div class="subject">{subject}</div>',
			'<div class="description">{description}</div>',
		'</div>',
	],

	applyMessage: function(message) {
		if (message = this.callParent(arguments)) message.isRoot = true;
		return message;
	}

});