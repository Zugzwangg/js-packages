Ext.define('CFJS.communion.grid.DiscussionPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'communion-grid-discussion-panel',
	requires		: [ 'CFJS.communion.grid.DiscussionViewPanel', 'CFJS.communion.window.ComposeDiscussion' ],
	columns			: [{
		xtype		: 'templatecolumn',
		align		: 'center',
		dataIndex	: 'shared',
		menuDisabled: true,
		text		: '<span class="' + CFJS.UI.iconCls.SHARE_ALT + '"></span>',
		tpl			: '<tpl if="shared === true"><span class="' + CFJS.UI.iconCls.SHARE_ALT + '"></span></tpl>',
		width		: 50
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'author.name',
		filter		: { emptyText: 'author name' },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Author',
		tpl			: '<tpl if="author">{author.name}</tpl>'
	},{
		align		: 'left',
		dataIndex	: 'subject',
		filter		: { emptyText: 'subject' },
		flex		: 3,
		style		: { textAlign: 'center' },
		text		: 'Subject',
	},{
		xtype		: 'templatecolumn',
		align		: 'center',
		dataIndex	: 'attachments',
		menuDisabled: true,
		sortable	: false,
		text		: '<span class="' + CFJS.UI.iconCls.ATTACH + '"></span>',
		tpl			: '<tpl if="!Ext.isEmpty(attachments)"><span class="' + CFJS.UI.iconCls.ATTACH + '"></span></tpl>',
		width		: 32
	},{
		xtype		: 'datecolumn',
		align		: 'center',
		dataIndex	: 'created',
		format		: Ext.Date.patterns.NormalDate,
		menuDisabled: true,
		text		: 'Created',
		width		: 100
	},{
		xtype		: 'datecolumn',
		align		: 'center',
		dataIndex	: 'finished',
		format		: Ext.Date.patterns.NormalDate,
		menuDisabled: true,
		text		: 'Finished',
		width		: 100
	}],
	composeWindow	: { xtype: 'communion-window-compose-discussion' },
	contextMenu		: false,
	iconCls			: 'x-fa fa-comments',
	title			: 'Discussions'
});