Ext.define('CFJS.communion.grid.MessagePanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'communion-grid-messages',
	requires		: [ 'CFJS.communion.grid.MessageViewPanel', 'CFJS.communion.window.ComposeMessage' ],
	columns			: [{
		xtype		: 'templatecolumn',
		align		: 'center',
		dataIndex	: 'priority',
		menuDisabled: true,
		text		: '<span class="' + CFJS.UI.iconCls.FLAG + '"></span>',
		tpl			: '<tpl if="priority != \'NORMAL\'"><span class="{colorCls} ' + CFJS.UI.iconCls.FLAG + '"></span></tpl>',
		width		: 50
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		dataIndex	: 'author',
		filter		: { dataIndex: 'author.name', emptyText: 'author name' },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Author',
		tpl			: '<tpl if="author">{author.name}</tpl>'
	},{
		align		: 'left',
		dataIndex	: 'description',
		filter		: { emptyText: 'description' },
		flex		: 3,
		style		: { textAlign: 'center' },
		text		: 'Description',
	},{
		xtype		: 'templatecolumn',
		align		: 'center',
		dataIndex	: 'attachments',
		menuDisabled: true,
		sortable	: false,
		text		: '<span class="' + CFJS.UI.iconCls.ATTACH + '"></span>',
		tpl			: '<tpl if="!Ext.isEmpty(attachments)"><span class="' + CFJS.UI.iconCls.ATTACH + '"></span></tpl>',
		width		: 32
	},{
		xtype		: 'datecolumn',
		align		: 'center',
		dataIndex	: 'created',
		format		: Ext.Date.patterns.NormalDate,
		menuDisabled: true,
		text		: 'Created',
		width		: 100
	}],
	composeWindow	: { xtype: 'communion-window-compose-message' },
	contextMenu		: false,
	iconCls			: 'x-fa fa-comment',
	title			: 'Messages',

	getRowClass: function(record, index, rowParams, store) {
		if (!record.isAuthor() && !record.isViewed()) return 'boldFont';
	}
	
});