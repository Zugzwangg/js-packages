Ext.define('CFJS.communion.view.Tasks', {
	extend		: 'CFJS.communion.view.Base',
	xtype		: 'communion-view-tasks',
	requires	: [
		'CFJS.communion.grid.TaskPanel',
//		'CFJS.panel.communion.TaskEditor',
		'CFJS.communion.view.TasksController',
		'CFJS.communion.view.TasksModel'
	],
	controller	: 'communion.tasks',
	gridConfig	: { 
		xtype	: 'communion-grid-tasks',
		bind	: { store: '{tasks}' },
	},
	session		: { schema: 'communion' },
	viewModel	: { type: 'communion.tasks' }
});