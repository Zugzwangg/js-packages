Ext.define('CFJS.communion.view.Messages', {
	extend		: 'CFJS.communion.view.Base',
	xtype		: 'communion-view-messages',
	requires	: [
		'CFJS.communion.grid.MessagePanel',
		'CFJS.communion.view.MessagesController',
		'CFJS.communion.view.MessagesModel'
	],
	controller	: 'communion.messages',
	gridConfig	: { 
		xtype	: 'communion-grid-messages',
		bind	: { store: '{messages}' },
	},
	session		: { schema: 'communion' },
	viewModel	: { type: 'communion.messages' },

	privates: {
		
		createSearchFilter: function(value) {
			return {
				id		: 'search',
				type	: 'string',
				property: 'description',
				operator: 'like',
	        	value	: value
			}
		}
	
	}
	
});