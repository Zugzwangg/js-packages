Ext.define('CFJS.communion.view.DiscussionView', {
	extend		: 'CFJS.communion.view.Base',
	xtype		: 'communion-view-discussion',
	requires	: [
		'CFJS.communion.grid.DiscussionPanel',
		'CFJS.communion.view.DiscussionsController',
		'CFJS.communion.view.DiscussionsModel'
	],
	controller	: 'communion.discussions',
	gridConfig	: { 
		xtype	: 'communion-grid-discussion-panel',
		bind	: { store: '{discussions}' },
	},
	session		: { schema: 'communion' },
	viewModel	: { type: 'communion.discussions' },

	privates: {
		
		createSearchFilter: function(value) {
			return {
				id		: 'search',
				type	: 'string',
				property: 'subject',
				operator: 'like',
	        	value	: value
			}
		}
	
	}
	
});