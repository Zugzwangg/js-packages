Ext.define('CFJS.communion.view.Base', {
	extend	: 'Ext.container.Container',
	xtype	: 'communion-view-base',
	config	: { actionable: null, readOnly: false },
	layout	: { type: 'card' },
	
	applyReadOnly: function(readOnly) {
		return readOnly === true;
	},

	applyStoreFilter: function(filter, suppressEvent) {
		var store = this.lookupStore();
		if (store && store.isStore && filter) {
			filter.id = filter.id || filter.property;
			if (filter.value) store.addFilter(filter, suppressEvent);
			else store.removeFilter(filter.id, suppressEvent);
		}
	},
	
	initComponent: function() {
		var me = this,
			gridConfig = CFJS.apply({
				enableColumnHide: false,
				enableColumnMove: false,
				columnLines		: false,
				header			: false,
			    headerBorders	: false,
				listeners		: { itemdblclick: 'onGridDblClick' },
				plugins			: [{ ptype: 'gridfilters' }],
		    	readOnly		: me.getReadOnly(),
			    rowLines		: false,
				selModel		: { type: 'rowmodel', mode: 'SINGLE', pruneRemoved: false },
			    viewConfig		: { preserveScrollOnRefresh: true },
			}, me.gridConfig);
		me.callParent();
		if (!me.grid) {
			me.grid = me.add(gridConfig);
			me.actionable = me.actionable || me.grid;
		}
		me.iconCls = me.grid.iconCls;
		me.tooltip = me.grid.title;
		me.title = me.grid.title;
	},
	
	lookupGrid: function() {
		return this.grid;
	},

	lookupStore: function() {
		var grid = this.lookupGrid();
		return grid && grid.getStore();
	},
	
	setSearchFilter: function(value, suppressEvent) {
		this.applyStoreFilter(this.createSearchFilter(value), suppressEvent);
	},
	
	updateReadOnly: function(readOnly) {
		if (this.grid) this.grid.setReadOnly(readOnly);
	},

	privates: {
		
		createSearchFilter: function(value) {
			return {
				id		: 'search',
				type	: 'string',
				property: 'name',
				operator: 'like',
	        	value	: value
			}
		}
	
	}
});