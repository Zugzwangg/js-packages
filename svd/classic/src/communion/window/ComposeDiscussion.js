Ext.define('CFJS.communion.window.ComposeDiscussion', {
	extend		: 'CFJS.communion.window.ComposeMessage',
	xtype		: 'communion-window-compose-discussion',
	requires	: [ 'CFJS.communion.view.ComposeDiscussionModel' ],
	actionsMap	: {
		header: { items: [ 'sendMessage', 'attachments' ] }
	},
	title		: 'Compose discussion',
	viewModel	: { type: 'communion.discussion.compose' },
	
	createItems: function(me, vm) {
		var me = this, items = me.callParent(arguments);
		items.splice(2, 1, {
			allowBlank	: false,
			bind		: '{_itemName_.subject}',
			fieldLabel	: 'Subject',
			name		: 'subject',
		},{
			xtype		: 'checkbox',
			bind		: '{_itemName_.shared}',
            fieldLabel	: 'Shared discussion',
			name		: 'shared'
		});
		return items;
	}

});