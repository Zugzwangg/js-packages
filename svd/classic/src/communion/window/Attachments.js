Ext.define('CFJS.communion.window.Attachments', {
	extend				: 'Ext.window.Window',
	xtype				: 'communion-window-attachments',
	requires			: [
		'Ext.form.field.File',
		'Ext.form.Panel',
		'Ext.tab.Panel',
		'Ext.layout.container.HBox',
		'CFJS.form.field.ReferenceComboBox',
		'CFJS.communion.view.AttachmentsEditorModel'
	],
	bodyPadding			: 10,
	defaultListenerScope: true,
	iconCls				: CFJS.UI.iconCls.ATTACH,
	modal				: true,
	saveText			: 'Save the changes',
	saveToolText		: 'Save the changes',
	title				: 'Attachments',
    uploadingText		: 'Uploading file...',
	viewModel			: { type: 'communion.attachments-editor' },
	
	applyChanges: function() {
		var me = this, vm = me.lookupViewModel();
		if (vm && Ext.isFunction(vm.applyChanges)) vm.applyChanges();
		me.close();
	},
	
	createBasePanelConfig: function(type) {
		return {
			xtype			: 'form',
			attachmentType	: type,
			bodyPadding		: '0 10',
			defaultFocus	: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
			defaults		: { allowBlank: false },
			defaultType		: 'textfield',
			fieldDefaults	: {
				labelAlign		: 'top',
				labelWidth		: 100,
				msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
				selectOnFocus	: true				
			},
			iconCls			: CFJS.model.Attachment.iconCls(type),
			layout			: { type: 'hbox', align: 'end' },
			name			: type.toLowerCase()
		}
	},
	
	createGrid: function(config) {
		var me = this,
			grid = CFJS.apply({
				xtype			: 'grid',
				bind			: { store: '{attachments}' },
				columns			: [{
					align		: 'left',
					dataIndex	: 'name',
					flex		:1
				},{
					align		: 'center',
					processEvent: function(type, view, cell, recordIndex, cellIndex, e, record, row) {
						if (e.type === 'click' || (e.type === 'keydown' && (e.keyCode === e.SPACE || e.keyCode === e.ENTER))) {
							me.getStore().remove(record);
						}						
					},
					renderer	: function () {
						return '<span data-qtip="' + me.grid.removeRowTip + '" role="button" tabIndex="0">' + me.grid.removeRowText + '</span>';
					},
					scope		: me,
					tdCls		: Ext.baseCSSPrefix + 'multiselector-remove',
					updater		: Ext.emptyFn,
					width		: 32,
				}],
				height			: 200,
				hideHeaders		: true,
				removeRowText	: '\u2716',
				removeRowTip	: 'Remove this item',
				width			: 500,
				viewConfig		: { deferEmptyText: false, emptyText: 'No data to display' }
			}, config);
		return Ext.create(grid);
	},
	
	createDocumentPanel: function(config) {
		var me = this,
			type = 'DOCUMENT',
			baseConfig = me.createBasePanelConfig(type),
			panel = Ext.create(CFJS.apply({
				items	: [{
					xtype		: 'dictionarycombo',
					bind		: { store: '{forms}', value: '{form}' },
					emptyText	: 'select a document type',
					fieldLabel	: 'Documents type',
					flex		: 1,
					listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
					name		: 'form'
				},{
					xtype			: 'referencecombobox',
					bind			: { store: '{documents}', value: '{document}' },
					emptyText		: 'select a document',
					fieldLabel		: 'Document',
					flex			: 1,
					hideAddTrigger	: true,
					listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
					margin			: '0 0 0 5',
					name			: 'document',
					required		: true
				},{
					xtype			: 'button',
					attachmentType	: type, 
					formBind		: true,
					handler			: me.onAddRecord,
					iconCls			: CFJS.UI.iconCls.ADD,
					margin			: '0 0 0 5',
					scope			: me,
					tooltip			: 'Add a document bookmark',
					ui				: 'default-toolbar'
				}],
				tooltip	: 'Document bookmark'
			}, config, baseConfig)), 
			formCombo = panel.down('dictionarycombo[name=form]'),
			documentCombo = panel.down('dictionarycombo[name=document]');
		if (formCombo && documentCombo) {
			documentCombo.on('afterquery', function(queryPlan) {
				queryPlan.combo.queryCaching = true;
			});
			formCombo.on('change', function() {
				documentCombo.queryCaching = false;
				documentCombo.setValue();
			});
		}
		return panel;
	},
	
	createFilePanel: function(config) {
		var me = this,
			type = 'FILE',
			baseConfig = me.createBasePanelConfig(type),
			panel = CFJS.apply({
				items	: [{
					xtype		: 'filefield',
					allowBlank	: true,
					buttonConfig: {
						iconCls	: CFJS.UI.iconCls.SEARCH,
						tooltip	: 'Select a file',
						ui		: 'default-toolbar'
					},
					buttonText	: '',
					emptyText	: 'select a file',
					fieldLabel	: 'File to upload',
					flex		: 1,
					name		: 'upload',
				},{
					xtype			: 'button',
					attachmentType	: type,
					formBind		: true,
					handler			: me.onAddRecord,
					iconCls			: CFJS.UI.iconCls.ADD,
					margin			: '0 0 0 5',
					scope			: me,
					tooltip			: 'Add a file',
					ui				: 'default-toolbar'
				}],
				tooltip	: 'Upload file'
			}, config, baseConfig);
		return Ext.create(panel);
	},

	createUrlPanel: function(config) {
		var me = this,
			type = 'URL',
			baseConfig = me.createBasePanelConfig(type),
			panel = CFJS.apply({
				items	: [{
					bind		: '{linkName}',
					emptyText	: 'input name for hyperlink',
					fieldLabel	: 'Name',
					flex		: 1,
					required	: true,
					name		: 'linkName'
				},{
					bind		: '{link}',
					emptyText	: 'input target url, like http://google.com',
					fieldLabel	: 'Hyperlink',
					flex		: 1,
					margin		: '0 0 0 5',
					name		: 'link',
					required	: true,
					vtype		: 'url'
				},{
					xtype			: 'button',
					attachmentType	: type, 
					formBind		: true,
					handler			: me.onAddRecord,
					iconCls			: CFJS.UI.iconCls.ADD,
					margin			: '0 0 0 5',
					scope			: me,
					tooltip			: 'Add an external link',
					ui				: 'default-toolbar'
				}],
				tooltip: 'External link'
			}, config, baseConfig);
		return Ext.create(panel);
	},
	
	initComponent: function() {
		var me = this;
		me.dockedItems = [{
			xtype		: 'tabpanel',
			dock		: 'top',
			items		: [ me.ensureChild('documentPanel'), me.ensureChild('filePanel'), me.ensureChild('urlPanel')],
			plain		: true,
			tabBar		: { layout: { pack: 'center' } },
			ui			: 'navigation'
		}];
		me.items = [ me.ensureChild('grid') ];
		me.buttons = [{
			iconCls	: CFJS.UI.iconCls.APPLY,
			text	: me.saveText,
			tooltip	: me.saveToolText,
			handler	: me.applyChanges,
			scope	: me
		}];
		me.callParent();
	},
	
	getStore: function() {
		return this.grid && this.grid.getStore();
	},
	
	onAddRecord: function(button) {
		var me = this, vm = me.lookupViewModel(),
			type = button && button.attachmentType, data;
		if (!vm) return;
		switch ((type||'').toUpperCase()) {
			case 'DOCUMENT':
				if (!(data = vm.get('document'))) return;
				if (data.isModel) data = data.getData();
				me.getStore().add({ type: type, name: (vm.get('form')||{}).name + ' ' + data.name, link: data.id });
				vm.set('document', null);
				break;
			case 'FILE':
				me.uploadFile(me.filePanel, {
					success: function(form, action) {  
						var files = action.result && action.result.files, i = 0;
						files = files && Ext.Array.from(files.file);
						for (; i < files.length; i++) {
							data = files[i];
							data = me.getStore().add({ type: type, name: data.name, link: [data.path, data.name].join('/') });
						}
					},
					failure: function(form, action) {
						var response = action.response || {};
						CFJS.errorAlert('Error uploading data', { code: response.status, text: response.statusText });
					}
				});
				break;
			case 'URL':
				data = { name: vm.get('linkName'), link: vm.get('link') };
				if (Ext.isEmpty(data.name) || Ext.isEmpty(data.link)) return; 
				me.getStore().add({ type: type, name: data.name, link: data.link });
				vm.set({ linkName: null, link: null });
				break;
			default: break;
		}
	},
	
	uploadFile: function(form, options) {
		if (form && form.isValid()) {
			var me = this, cnt = CFJS.Api.controller('resources');
			options = options || {};
			if (cnt && cnt.url) {
				form.submit({
					url			: cnt.url,
					extraParams	: { method: [cnt.method, 'upload'].join('.') },
//					errorReader	: { type: 'json.cfapi', rootProperty: 'file' },
					waitMsg		: me.uploadingText,
					success		: options.success || Ext.emptyFn,
					failure		: options.failure || Ext.emptyFn
				});
			}
		}
	},
	
});