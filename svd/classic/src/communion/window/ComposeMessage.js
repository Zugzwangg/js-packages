Ext.define('CFJS.communion.window.ComposeMessage', {
	extend				: 'Ext.window.Window',
	xtype				: 'communion-window-compose-message',
	requires			: [
		'Ext.form.field.HtmlEditor',
		'Ext.form.field.Checkbox',
		'CFJS.form.field.DictionaryTag',
		'CFJS.communion.view.ComposeMessageModel',
		'CFJS.communion.window.Attachments'
	],
	actions				: {
		attachments	: {
			iconCls	: CFJS.UI.iconCls.ATTACH,
			tooltip	: 'Attachemnts',
			handler	: 'onAttachmentsClick'
		},
		priority	: {
			iconCls	: CFJS.UI.iconCls.FLAG,
			tooltip	: 'Priority',
		},
		sendMessage	: {
			iconCls	: CFJS.UI.iconCls.SEND,
			tooltip	: 'Send',
			handler	: 'onSendMessage'
		}
	},
	actionsMap	: {
		header: { items: [ 'sendMessage', 'priority', 'attachments' ] }
	},
	autoShow			: true,
	defaultListenerScope: true,
	header				: CFJS.UI.buildHeader({ items: new Array(3) }),
	height				: 200,
	iconCls				: CFJS.UI.iconCls.ENVELOPE,
	layout				: 'fit',
	modal				: true,
	session				: { schema: 'communion' },
	title				: 'Compose message',
	viewModel			: { type: 'communion.message.compose' },
	width				: 200,

	afterRender: function () {
		var me = this, userInfo = CFJS.lookupViewportViewModel().userInfo(), tagField;
		me.callParent(arguments);
		me.syncSize();
		Ext.on(me.resizeListeners = { resize: me.onViewportResize, scope: me, buffer: 50 });
		if (tagField = me.down('dictionarytag[name=' + userInfo.type + 's]')) {
			tagField.on('beforeselect', function(field, record) { return userInfo.id !== record.id; });
		}
	},
	
	createItems: function(me, vm) {
		return [{
				xtype			: 'dictionarytag',
				autoLoadOnValue	: true,
				bind			: { value: '{_itemName_.persons}' },
				fieldLabel		: 'Users',
				name			: 'persons',
				store			: {
		        	type	: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'person' } },
					filters	: [{ id: 'post', property: 'post.id', type: 'numeric', operator: '!eq' }],
					sorters	: [ 'name' ]
				}
			},{
				xtype			: 'dictionarytag',
				autoLoadOnValue	: true,
				bind			: { value: '{_itemName_.customers}' },
				fieldLabel		: 'Customers',
				name			: 'customers',
				queryParam		: 'fullName',
				store			: {
		        	type	: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'customer' } },
					sorters	: [ 'name' ]
				}
			},{
				xtype		: 'checkbox',
				bind		: '{_itemName_.sendEmail}',
	            fieldLabel	: 'Send on email',
				name		: 'sendEmail'
			},{
				xtype		: 'htmleditor',
				bind		: { value: '{_itemName_.description}' },
				flex		: 1,
				labelAlign	: 'top',
				minHeight	: 100,
				name		: 'description'
		}];
	},
	
	initComponent: function() {
		var me = this, priority = me.getActions().priority,
			messagePriorities = Ext.getStore('messagePriorities'),
			vm = me.lookupViewModel(), menu = [];
		if (messagePriorities && priority) {
			messagePriorities.each(function(record) {
				var pr = record.getId() || 'NORMAL';
				menu.push({
					xtype	: 'menucheckitem',
					bind	: { checked: '{_itemName_.priority === "' + pr + '"}' },
					handler	: function() { vm.getItem().set('priority', pr); },
					iconCls	: CFJS.UI.iconCls.FLAG + ' ' + CFJS.schema.Communion.priorityColorCls(pr),
					group	: 'priority',
					text	: record.get('name')
				});
			});
			priority.initialConfig.menu = { items: menu };
			priority.callEach('setMenu', [priority.initialConfig.menu]);
		}
		me.items = CFJS.apply({
			xtype			: 'form',
		    bodyPadding		: 10,
		    defaults		: { labelWidth: 120, labelSeparator: '' },
		    defaultType		: 'textfield',
			items			: me.createItems(me, me.lookupViewModel()),
		    layout			: { type:'vbox', align:'stretch' },
		    modelValidation	: true
		}, me.editorCfg);
		me.callParent();
		me.lookupViewModel().setItem();
	},
	
	forward: function(parent) {
		this.lookupViewModel().setMessageData(parent);
	},
	
	reply: function(parent) {
		var vm = this.lookupViewModel();
		vm.setMessageData(parent, vm.getAuthorOptions(parent));
	},
	
	replyAll: function(parent) {
		var vm = this.lookupViewModel(), data = vm.getAuthorOptions(parent), arr = Ext.Array,
			merge = function(key, data, parent) { return arr.merge(arr.from(data[key]), arr.from(parent.get(key))); };
		if (data) Ext.apply(data, {
			customers: merge('customers', data, parent),
			persons: merge('persons', data, parent)
		});
		vm.setMessageData(parent, data);
	},
	
	onAttachmentsClick: function(component) {
		Ext.create(CFJS.apply({
			xtype		: 'communion-window-attachments',
			viewModel	: { parent: this.lookupViewModel() }
		})).show();
	},
	
	onDestroy: function () {
		Ext.un(this.resizeListeners);
		this.callParent();
	},
	
	onSendMessage: function(component) {
		var me = this, record = me.lookupViewModel().getItem(),
			form = me.down('form');
		if (form.isValid() && record && record.phantom && record.isValid()) {
			me.disableComponent(component, true);
			record.save({
				callback: function(record, operation, success) {
					me.disableComponent(component);
					me.fireEvent('sendmessage', record);
					me.close();
				}
			});
		}
	},
	
	onViewportResize: function () {
		this.syncSize();
	},

	syncSize: function () {
		var width = Ext.Element.getViewportWidth(),
			height = Ext.Element.getViewportHeight();
		this.setSize(Math.floor(width * 0.9), Math.floor(height * 0.9));
		this.setXY([ Math.floor(width * 0.05), Math.floor(height * 0.05) ]);
	}
	
});