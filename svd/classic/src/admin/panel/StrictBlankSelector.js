Ext.define('CFJS.admin.panel.StrictBlankSelector', {
	extend				: 'Ext.grid.Panel',
	xtype				: 'admin-panel-strict-blank-selector',
	requires 			: [
		'Ext.form.Panel',
		'Ext.form.field.TextArea',
		'Ext.layout.container.HBox',
		'Ext.grid.plugin.RowEditing',
		'Ext.window.MessageBox',
		'CFJS.form.field.DictionaryComboBox',
		'CFJS.store.document.StrictForms'
	],
	addToolIconCls		: CFJS.UI.iconCls.ADD,
	addToolText			: 'Add a pack',
	backToolText		: 'Back to the list',
	columnLines			: true,
	defaultFocus		: 'dictionarycombo[name="form"]',
	emptyText			: 'No data to display',
	enableColumnHide	: false,
	enableColumnMove	: false,
	processToolIconCls	: CFJS.UI.iconCls.COG,
	processToolText		: 'Process all packs',
	removeRowText		: '\u2716',
	removeRowTip		: 'Remove this item',
	store				: {
		fields: [
			{ name: 'form',		type: 'model',	critical: true,	entityName: 'CFJS.model.document.StrictForm' },
			{ name: 'series',	type: 'string',	critical: true },
			{ name: 'numbers',	type: 'string',	critical: true }
		]
	},

	additionalProcess: function(button, record, callback) {
		if (callback && Ext.isFunction(callback)) {
			callback.call(this, record);
		}
	},

	createButtons: function(buttons) {
		var me = this;
		return (buttons || []).concat({
			handler	: me.onAddPack,
			iconCls	: me.addToolIconCls,
			margin	: '0 5',
			scope	: me,
			tooltip	: me.addToolText
		},{
			handler	: me.onProcessPacks,
			iconCls	: me.processToolIconCls,
			scope	: me,
			tooltip	: me.processToolText
		})
	},

	renderColumns: function() {
    	var me = this;
    	return [{
			xtype		: 'rownumberer'
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			cellWrap	: true,
			dataIndex	: 'form',
			editor		: {
				xtype			: 'dictionarycombo',
				allowBlank		: false,
				emptyText		: 'select a form',
				listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
				selectOnFocus	: true,
				store			: { type: 'strictforms' },
				width			: 300
			},
			flex		: 2,
			style		: { textAlign: 'center' },
			text		: 'Form',
			tpl			: '<tpl if="form">{form.name}</tpl>'
		},{
			align		: 'left',
			dataIndex	: 'series',
			editor		: { allowBlank: false, selectOnFocus: true },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Series'
		},{
			align		: 'left',
			dataIndex	: 'numbers',
			editor		: { allowBlank: false, selectOnFocus: true },
			flex		: 1,
			style		: { textAlign: 'center' },
			text		: 'Numbers'
		},{
			align			: 'center',
			menuDisabled	: true,
			processEvent	: me.processRowEvent.bind(me),
			renderer		: me.renderRemoveRow,
			scope			: me,
			tdCls			: Ext.baseCSSPrefix + 'multiselector-remove',
			updater			: Ext.emptyFn,
			width			: 32
		}]
    },
    
	initComponent: function() {
		var me = this,
			emptyText = me.emptyText;
		if (emptyText && !me.viewConfig) {
            me.viewConfig = { deferEmptyText: false, emptyText: emptyText };
        }
		if (!me.header || !me.header.isHeader) {
			me.header = CFJS.UI.buildHeader({
				items 			: me.createButtons([{
					iconCls	: CFJS.UI.iconCls.BACK,
					tooltip	: me.backToolText,
					handler	: 'onBackClick'				
				}]),
				titlePosition	: 1
			}); 
		}
		me.columns = me.renderColumns();
		me.plugins = [{ ptype: 'rowediting', autoUpdate: true, clicksToMoveEditor: 1 }];
		me.callParent();
	},
	
	onAddPack: function(button) {
		var me = this, store = me.store, f = 0, record = {}, fields;
			rowEditing = me.findPlugin('rowediting');
		record = store.add(record)[0];
		if (rowEditing) rowEditing.startEdit(record);
		else me.getView().focusNode(record); 
	},
	
	onProcessPacks: function(button) {
		var me = this,
			rowEditing = me.findPlugin('rowediting'),
			store = me.store, data = [], record;
		if (rowEditing) rowEditing.cancelEdit();
		store.each(function(record) {
			record = me.extractInfoData(record);
			if (record) data.push(record);
		});
		if (data.length > 0) record = me.asPayloadModel(data);
		if (record && record.isModel) {
			button.disable();
//			me.additionalProcess(button, record, function(record, operation, success) {
//				button.enable();
//				me.fireEvent('processed', record, success);
//			});
			me.additionalProcess(button, record, function(record) {
		    	record.save({
					callback: function(record, operation, success) {
						button.enable();
						me.fireEvent('processed', record, success);
					}
		    	});
			});
		}
    },
    
	processRowEvent: function (type, view, cell, recordIndex, cellIndex, e, record, row) {
        var body = Ext.getBody();
        if (e.type === 'click' || (e.type === 'keydown' && (e.keyCode === e.SPACE || e.keyCode === e.ENTER))) {
            body.suspendFocusEvents();
            this.store.remove(record);
            body.resumeFocusEvents();
        }
    },
 
    renderRemoveRow: function () {
        return '<span data-qtip="'+ this.removeRowTip + '" role="button" tabIndex="0">' +
            this.removeRowText + '</span>';
    },
    
    privates: {
    	
    	asPayloadModel: function(data) {
    		var me = this, 
    			record = me.createPayloadModel(data)
    			proxy = record.getProxy();
    		proxy.setService(me.service || 'svd');
			proxy.getApi().create = me.apiParams;
    		return record;
    	},

    	createPayloadModel: function(data) {
    		return new CFJS.model.EntityModel({ locale: CFJS.getLocale(), blanks: { 'blank-bundle': data } });
    	},
    	
    	extractInfoData: function(record) {
    		var fields = record.getCriticalFields(),
    			data = {}, i = 0, name, field;
    		for (; i < fields.length; i++) {
    			field = fields[i].name;
    			data[field] = record.get(fields[i].name);
    		}
			if (data.state === 'PRINTED') return;
    		delete data.id;
    		data.form = data.form.id;
    		return data;
    	}

    }

});
