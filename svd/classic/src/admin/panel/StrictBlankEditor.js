Ext.define('CFJS.admin.panel.StrictBlankEditor', {
	extend			: 'CFJS.panel.BaseEditor',
	xtype			: 'admin-panel-strict-blank-editor',
	bodyPadding		: '0 10 0 10',
	fieldDefaults	: {
		labelAlign	: 'top',
		labelWidth	: 100,
		msgTarget	: Ext.supports.Touch ? 'side' : 'qtip'
	}
});