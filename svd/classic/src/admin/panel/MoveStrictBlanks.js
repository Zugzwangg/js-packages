Ext.define('CFJS.admin.panel.MoveStrictBlanks', {
	extend				: 'CFJS.admin.panel.StrictBlankSelector',
	xtype				: 'admin-panel-move-strict-blanks',
	addSelection		: true,
	apiParams			: {
		method		: 'strict.move',
		transaction	: 'save'
	},
	promptButton		: {
		iconCls	: CFJS.UI.iconCls.APPLY,
		text	: 'Select'
	},
	promptDlg			: {
		iconCls	: CFJS.UI.iconCls.USERS,
		title	: 'Selecting target post'
	},
	promptField		: {
		emptyText	: 'select a post',
		fieldLabel	: 'Target post'
	},
	processToolText		: 'Move all packs',
	processToolIconCls	: CFJS.UI.iconCls.APPLY,
	title				: 'Move blanks',

	additionalProcess: function(button, record, callback) {
		var me = this, promptDlg = me.promptDlg,
			body, iconComponent, promptButton, promptField;
		if (!promptDlg || !promptDlg.isWindow) {
			promptField = Ext.create(Ext.apply({
				xtype			: 'dictionarycombo',
				flex			: 1,
				listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
				name			: 'secondary',
				store			: { 
					type	: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'post' } },
					sorters	: [ 'name' ]
				}
			}, me.promptField));
			iconComponent = Ext.create(Ext.apply({
				xtype	: 'box',
				cls		: Ext.MessageBox.QUESTION,
				margin	: '0 10 0 0'
			}, me.iconComponent));
			body = Ext.create(Ext.apply({
				xtype			: 'form',
				defaultFocus	: promptField.xtype,
				defaultType		: promptField.xtype,
				fieldDefaults	: { labelAlign: 'top', labelWidth: 100, msgTarget: Ext.supports.Touch ? 'side' : 'qtip' },
				layout			: { type: 'hbox' },
				bodyPadding		: 10,
				style			: { overflow: 'hidden' },
				items			: [ iconComponent, promptField ]
			}, me.bodyConfig));
			promptButton = Ext.create(Ext.apply({
				xtype		: 'button',
				handler 	: function() {
					var value = promptField.getValue();
					if (value) {
						if (value.isModel) value = value.getData();
						record.set(promptField.name, value);
				    	if (callback && Ext.isFunction(callback)) {
				    		callback.call(me, record);
				    	}
					}
					me.promptDlg.close();
				},
				minWidth	: 100,
				reference	: 'promptButton'
			}, me.promptButton));
			me.promptDlg = Ext.create(Ext.apply({
				xtype			: 'window',
				closeAction		: 'hide',
				defaultButton	: promptButton.reference,
				defaultFocus	: promptField.xtype,
				items			: body,
				layout			: 'fit',
				modal			: true,
				minHeight		: 200,
				minWidth		: 450,
				referenceHolder	: true,
				buttons			: [ promptButton ]
			}, promptDlg));
			me.promptDlg.on({
				close: function() { button.enable(); } 
			});
		}
		me.promptDlg.show();
	},
	
	renderColumns: function() {
    	var me = this, columns = me.callParent(arguments);
    	columns.splice(columns.length - 1, 0, {
    		align		: 'left',
    		dataIndex	: 'state',
    		flex		: 1,
    		style		: { textAlign: 'center' },
    		text		: 'State',
    		renderer	: Ext.util.Format.enumRenderer('strictBlankStates')
    	});
    	return columns;
    }

});
