Ext.define('CFJS.admin.panel.IssueStrictBlanks', {
	extend				: 'CFJS.admin.panel.StrictBlankSelector',
	xtype				: 'admin-panel-issue-strict-blanks',
	apiParams			: {
		method		: 'strict.issue',
		transaction	: 'new'
	},
	processToolText		: 'Issue all packs',
	processToolIconCls	: CFJS.UI.iconCls.APPLY,
    title				: 'Issue new blanks'

});
