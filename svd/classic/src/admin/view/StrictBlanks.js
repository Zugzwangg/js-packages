Ext.define('CFJS.admin.view.StrictBlanks', {
	extend				: 'Ext.container.Container',
	alternateClassName	: 'CFJS.view.StrictBlanks',
	xtype				: 'strictblanksview',
	requires			: [
		'CFJS.admin.grid.StrictBlankPanel',
		'CFJS.admin.panel.CancelStrictBlanks',
		'CFJS.admin.panel.IssueStrictBlanks',
		'CFJS.admin.panel.MoveStrictBlanks',
		'CFJS.admin.panel.StrictBlankEditor',
		'CFJS.admin.view.StrictBlanksController',
		'CFJS.admin.view.StrictBlanksModel'
	],
	controller			: 'admin.strictblanks',
	items				: [{
    	xtype		: 'admin-grid-strict-blank-panel',
    	listeners	: { itemdblclick: 'onGridDblClick' }
	}],
	layout				: { type: 'card', anchor: '100%' },
	operations			: {
		cancel	: { xtype: 'admin-panel-cancel-strict-blanks' },
		issue	: { xtype: 'admin-panel-issue-strict-blanks' },
		move	: { xtype: 'admin-panel-move-strict-blanks' }
	},
	session				: { schema: 'document' },
	viewModel			: { type: 'admin.strictblanks' },
	
	lookupGrid: function() {
		return this.down('admin-grid-strict-blank-panel');
	}
	
});