Ext.define('CFJS.admin.grid.StrictBlankPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'admin-grid-strict-blank-panel',
	actions			: {
		addRecord	: {
			title	: 'Issue new blanks',
			tooltip	: 'Issue new strict blanks'
		},
		editRecord	: false,
		moveRecord	: {
			iconCls	: CFJS.UI.iconCls.SHARE,
			title	: 'Move the blanks',
			tooltip	: 'Move selected blanks',
			handler	: 'onMoveRecord'
		},
		periodPicker: false,
		removeRecord: {
			title	: 'Revoke blanks',
			tooltip	: 'Revoke selected blanks'
		}
	},
	actionsDisable	: [ 'removeRecord' ],
	actionsMap		: {
		contextMenu	: {
			items: [ 'addRecord', 'removeRecord', 'moveRecord' ]
		},
		header		: {
			items: [ 'addRecord', 'removeRecord', 'moveRecord', 'clearFilters', 'reloadStore' ]
		}
	},
	bind			: '{strictBlanks}',
	enableColumnHide: false,
    iconCls			: CFJS.UI.iconCls.ARCHIVE,
	plugins			: [{ ptype: 'gridfilters' }],
	session			: { schema: 'dictionary' },
    title			: 'Warehouse of strict forms',

	columns			: [{
		xtype		: 'templatecolumn',
		align		: 'left',
		cellWrap	: true,
		dataIndex	: 'owner',
		filter		: { type: 'string', dataIndex: 'owner.employment.person.name', emptyText: 'agent\'s name' },
		flex		: 2,
		style		: { textAlign: 'center' },
		text		: 'Agent',
		tpl			: '<tpl if="owner">{owner.name}</tpl>'
	},{
		xtype		: 'templatecolumn',
		align		: 'left',
		cellWrap	: true,
		dataIndex	: 'strictForm',
		filter		: { type: 'string', dataIndex: 'strictForm.name', emptyText: 'form\'s name' },
		flex		: 2,
		style		: { textAlign: 'center' },
		text		: 'Form',
		tpl			: '<tpl if="strictForm">{strictForm.name}</tpl>'
	},{
		align		: 'left',
		dataIndex	: 'series',
		filter		: { type: 'string', emptyText: 'series' },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Series'
	},{
		align		: 'left',
		dataIndex	: 'number',
		filter		: { type: 'string', emptyText: 'number' },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Number'
	},{
		align		: 'left',
		dataIndex	: 'state',
		filter		: { type: 'list', dataType: 'string', itemDefaults: { hideOnClick: true }, labelField: 'name', operator: 'eq', store: 'strictBlankStates', single: true },
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'State',
		renderer	: Ext.util.Format.enumRenderer('strictBlankStates')
	}],

	initComponent: function() {
		var me = this, vm = me.lookupViewModel(), actions = me.actions;
		me.callParent();
		me.lookupViewModel().bind('{permissionLevel}', function(permissionLevel) {
			me.hideComponent(me.actions.addRecord, permissionLevel > 0);
		});
	}

	
});