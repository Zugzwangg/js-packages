Ext.define('CFJS.document.grid.SignatoriesPanel', {
	extend				: 'Ext.grid.Panel',
	xtype				: 'document-grid-signatories-panel',
	columnLines			: true,
	datatipTpl			: [
		'<b>ID:</b> {id}</br>',
		'<tpl if="phase"><b>Phase:</b> {[this.phaseRenderer(values.phase)]}</br></tpl>',
		'<tpl if="signer"><b>Signer:</b> {signer.name}</br></tpl>',
		'<tpl if="resolution"><b>Resolution:</b> {resolution}</br></tpl>',
		'<b>Timestamp:</b> {timestamp:date("' + Ext.Date.patterns.LongDateTime + '")}',
	],
	defaultListenerScope: true,
    emptyText			: 'No data to display',
	enableColumnHide	: false,
	enableColumnMove	: false,
	gridCls				: Ext.baseCSSPrefix + 'property-grid',
	height				: 250,
	iconCls				: 'x-fa fa-check-circle-o',
	plugins				: [ 'gridfilters' ],
	readOnly			: false,
	removeText			: 'Remove selected signature',
	stripeRows			: false,
	trackMouseOver		: false,
	title				: 'Signatories',
	verifySignatureMsg	: 'Signature successfully verified!',
	verifySignatureText	: 'Verify selected signature',
	verifySignatureTitle: 'Signature verification',
	
	confirmRemove: function(signerName, fn) {
		return Ext.fireEvent('confirm', 'Are you sure you want to remove signature of ' + signerName + '?', fn, this);
	},

	initComponent: function() {
		var me = this, datatip = me.findPlugin('datatip'),
			store = Ext.getStore('lifePhaseTypes');
		me.phaseRenderer = function(value) {
			return value ? Ext.util.Format.formatEnum(value, store) : '...';
		};
		if (me.header !== false) me.header = Ext.apply(me.header || {}, me.headerConfig);
		me.datatipTpl = Ext.Array.from(me.datatipTpl);
		me.datatipTpl.push({ phaseRenderer: me.phaseRenderer });
		if (!datatip) me.addPlugin({ ptype: 'datatip', tpl: me.datatipTpl });
		else if (!datatip.tpl) plugin.tpl = me.datatipTpl;
		me.columns = me.renderColumns();
		me.callParent();
	},

	onRemoveSignature: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this, signer;
		if (!me.readOnly && record && record.isEntity) {
			signer = record.get('signer');
			me.confirmRemove((signer||{}).name, function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					record.erase({
						callback: function(record, operation, success) {
							me.getStore().remove(record);
							me.disableComponent(component);
						}
					});
				}
			});
		}
	},

	onVerifySignature: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this;
		if (record && record.isSignature) {
			record.verify(null, function(data, rec) {
				CFJS.infoAlert(me.verifySignatureTitle, me.verifySignatureMsg);
			});
		}
	},
	
	renderColumns: function() {
		var me = this,
			actions = {
				xtype		: 'actioncolumn',
				align		: 'center',
				items		: [{
					handler	: 'onVerifySignature',
					iconCls	: CFJS.UI.iconCls.KEY,
					tooltip	: me.verifySignatureText
				}],
				menuDisabled: true,
				sortable	: false,
				tdCls		: Ext.baseCSSPrefix + 'multiselector-remove',
				width		: 30
			},
			columns = [{
				xtype		: 'rownumberer'
			},{
				align		: 'left',
				dataIndex	: 'phase',
				filter		: { type: 'list', dataType: 'string', itemDefaults: { hideOnClick: true }, labelField: 'name', nullValue: '...', operator: 'eq', store: 'lifePhaseTypes', single: true },
				flex		: 1,
				renderer	: me.phaseRenderer,
				style		: { textAlign: 'center' },
				text		: 'Phase',
			},{
				xtype		: 'templatecolumn',
				align		: 'left',
				dataIndex	: 'signer.name',
				filter		: {
					emptyText: 'signer name',
					filterFn: function(r, v) {
						return v && ((r.get('signer')||{}).name||'').toLowerCase().indexOf(v.toLowerCase()) > -1;
					}
				},
				flex		: 1,
				style		: { textAlign: 'center' },
				text		: 'Signer',
				tpl			: '<tpl if="signer">{signer.name}</tpl>'
			}, actions];
		if (CFJS.isAdministrator()) {
			actions.width = 50;
			actions.items.push({
				handler		: 'onRemoveSignature',
				iconCls		: CFJS.UI.iconCls.CANCEL,
				isDisabled	: function() { return me.readOnly },
				tooltip		: me.removeText
			});
		}
		return columns;
	},

	setReadOnly: function(readOnly) {
		this.readOnly = readOnly;
	}
	
});