Ext.define('CFJS.document.grid.DocumentJournal', {
	extend				: 'Ext.grid.Panel',
	xtype				: 'document-grid-document-journal',
	requires			: [ 'CFJS.document.util.Forms', 'CFJS.document.window.ObjectSelector' ],
	actions				: {
		addDocument	: {
			iconCls	: CFJS.UI.iconCls.ADD,
			tooltip	: 'Add documents',
			handler	: 'onAddDocument'
		}
	},
	browserRouteId		: 'documents',
	columnLines			: false,
	defaultListenerScope: true,
    emptyText			: 'No data to display',
	enableColumnHide	: false,
	enableColumnMove	: false,
	formType			: 'DOCUMENT',
	gridCls				: Ext.baseCSSPrefix + 'property-grid',
	headerConfig		: CFJS.UI.buildHeader(),
	iconCls				: CFJS.UI.iconCls.TABLE,
	nameText			: 'Document journal',
	readOnly			: false,
	removeText			: 'Remove selected document',
	sortableColumns		: true,
	stripeRows			: false,
	sameDocumentTitle	: 'Warning',
	sameDocumentText	: 'The selected document is already listed.',
	title				: 'Documents',
	trackMouseOver		: false,
	
	addTools: function () {
		var me = this, header = me.header;
		if (!header || !header.isHeader) {
			me.header = header = Ext.apply(header || {}, me.headerConfig);
			header.items = [ me.getActions().addDocument ];
		}
		me.updateHeader();
	},
	
	beforeSelectDocument: function(picker, form, document) {
		var me = this;
		if (me.store.getById((document||{}).id)) {
			CFJS.infoAlert(me.sameDocumentTitle, me.sameDocumentText);
			return false;
		}
	},
	
	confirmRemove: function(docName, fn) {
		return Ext.fireEvent('confirm', 'Are you sure you want to remove document "' + docName + '"?', fn, this);
	},

	createDocumentPicker: function(listeners) {
		var me = this,
			picker = Ext.apply({
				xtype			: 'document-window-object-selector',
				closeAction		: 'hide',
				formType		: me.formType,
				minWidth		: 540,
				minHeight		: 200
			}, me.documentPickerConfig);
		picker = Ext.create(picker);
		if (listeners) picker.on(listeners);
		return picker;
	},
	
	getDocumentPicker: function() {
		var me = this, picker = me.documentPicker;
        if (!picker) {
        	me.creatingPicker = true;
        	picker = me.documentPicker = me.createDocumentPicker({ beforeselect: me.beforeSelectDocument, select: me.onSelectDocument, scope: me });
        	picker.ownerCmp = me;
        	delete me.creatingPicker;
        }
        return me.documentPicker;
	},

	getRowClass: Ext.emptyFn,

	initComponent: function() {
		var me = this;
		me.addCls(me.gridCls);
		me.viewConfig = Ext.applyIf(me.viewConfig || {}, {
			deferEmptyText			: false, 
			getRowClass				: me.getRowClass,
			preserveScrollOnReload	: true
		});
		me.columns = me.renderColumns();
		me.callParent();
		me.on({ itemdblclick: me.onDocumentDblClick, scope: me });
	},
	
	onAddDocument: function(component) {
		var me = this, picker;
		if (!me.readOnly && me.rendered && !me.isExpanded && !me.destroyed) {
			picker = me.getDocumentPicker();
			picker.setMaxHeight(picker.initialConfig.maxHeight);
			picker.show();
		}
	},
	
	onDocumentDblClick: function(view, record, item, index, e, eOpts) {
		CFJS.document.util.Forms.routeToObject(record.getData(), this.formType, this.browserRouteId);
	},
	
	onRemoveDocument: function(component, rowIndex, colIndex, item, e, record, row) {
		var me = this;
		if (!me.readOnly && record && record.isEntity) {
			me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					me.store.remove(record);
					me.disableComponent(component);
				}
			});
		}
	},

	onSelectDocument: function(picker, form, document) {
		if (form && document && document.id > 0) this.store.add({
			id	: document.id,
			name: form.name + ' ' + document.name,
			form: form,
			type: picker.formType
		});
	},
	
	renderColumns: function() {
		var me = this;
		return [{
			xtype		: 'rownumberer'
		},{
			align		: 'left',
			dataIndex	: 'name',
			flex		: 1,
			innerCls	: Ext.baseCSSPrefix + 'grid-cell-inner-property-name',
			itemId		: 'name-field',
			menuDisabled: true,
			renderer	: me.nameRenderer,
			sortable	: me.sortableColumns,
			style		: { textAlign: 'center' },
			text		: me.nameText,
			tdCls		: Ext.baseCSSPrefix + 'grid-property-name'
		},{
			align		: 'center',
			items		: [{
				handler		: 'onRemoveDocument',
				iconCls		: CFJS.UI.iconCls.CANCEL,
				isDisabled	: me.isRemoveDisabled,
				tooltip		: me.removeText
			}],
			menuDisabled: true,
			sortable	: false,
			tdCls		: Ext.baseCSSPrefix + 'multiselector-remove',
			width		: 30,
			xtype		: 'actioncolumn'
		}];
	},
	
	setReadOnly: function(readOnly) {
		var me = this, actions = me.getActions() || {};
		me.hideComponent(actions.addDocument, readOnly);
		me.readOnly = readOnly;
		me.view.refreshView();
	},
	
	privates: {
		
		isRemoveDisabled: function(view, rowIndex, colIndex, item, record) {
			return view.grid.readOnly;
		}
		
	}

});