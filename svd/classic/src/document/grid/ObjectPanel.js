Ext.define('CFJS.document.grid.ObjectPanel', {
	extend				: 'CFJS.grid.BasePanel',
	xtype				: 'document-grid-object-panel',
	requires			: [ 
		'Ext.form.field.Number',
		'Ext.form.field.TextArea',
		'CFJS.form.DateTimeField',
		'CFJS.form.field.CheckboxGroupField',
		'CFJS.form.field.RadioGroupField',
		'CFJS.form.field.ReferenceComboBox',
		'CFJS.document.util.Forms',
		'CFJS.model.document.*'
	],
	actions: {
		copyRecord		: {
			iconCls		: CFJS.UI.iconCls.COPY,
			title		: 'Copy record',
			tooltip		: 'Copy the selected record',
			handler		: 'onCopyRecord'
		}
	},
	actionsMap			: {
		contextMenu		: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'editRecord' ] },
		header			: { items: [ 'periodPicker', 'addRecord', 'copyRecord', 'removeRecord', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	actionsDisable		: [ 'copyRecord', 'removeRecord', 'editRecord' ],
	config				: { form: null },
	contextMenu			: { items: new Array(4) },
	downloadSignFileText: 'Download signed file',
	header				: { items: new Array(7) },
	iconCls				: CFJS.UI.iconCls.TABLE,
	plugins				: 'gridfilters',
	selModel			: { type: 'rowmodel', mode: 'MULTI', pruneRemoved: false },

	initComponent: function() {
		var me = this;
		me.columns = [];
		me.callParent();
	},
	
	applyForm: function(form, oldForm) {
		return form && !form.isForm ? CFJS.extractId(form) : form;
	},
	
	getItem: function() { 
		return this.lookupViewModel().getItem();
	},
	
	getLastSelected: function () {
		return this.getSelectionModel().getLastSelected();
	},
	
	getSelection: function () {
		return this.getSelectionModel().getSelection();
	},
	
	renderColumn: function(field) {
		return CFJS.document.util.Forms.renderColumn(field);
	},

	renderColumns: function(form) {
		return CFJS.document.util.Forms.renderColumns(form);
	},

	renderForm: function(form) {
		var me = this, store = me.getStore(), columns = me.renderColumns(form),
			type = form && form.isForm && form.get('type');
		if (store && !store.isInitializing) store.reload();
		Ext.suspendLayouts();
		if (type !== 'TABLE') {
			columns.push({
				xtype		: 'actioncolumn',
				align		: 'center',
				items		: [{
					handler		: 'onDownloadSignedFile',
					iconCls		: CFJS.UI.iconCls.DOWNLOAD,
					isDisabled	: function(view, rowIndex, colIndex, item, record) {
						var files = record && record.get('signedFiles');
						return !(files && files.length > 0);
					},
					tooltip		: me.downloadSignFileText
				}],
				menuDisabled: true,
				sortable	: false,
				width		: 30,
			});
		}
		me.reconfigure(columns);
		Ext.resumeLayouts(true);
	},
	
	setItem: function(item) {
		this.lookupViewModel().setItem(item);
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		me.hideComponent((me.actions||{}).copyRecord, readOnly);
		me.callParent(arguments);
	},
	
	strictBlankData: Ext.emptyFn,
	
	updateForm: function(form, oldForm) {
		if (form !== oldForm) this.renderForm(form);
	}

});