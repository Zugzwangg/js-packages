Ext.define('CFJS.document.grid.FieldsPicker', {
	extend				: 'Ext.grid.Panel',
	xtype				: 'document-grid-fields-picker',
	requires			: [
		'Ext.button.Button',
		'Ext.form.field.Display',
		'CFJS.document.util.Forms',
		'CFJS.store.document.Fields'
	],
	actionsMap			: { header: { items: [ 'levelUp', 'reloadStore' ] } },
	actionsDisable		: [ 'copy' ],
	columnLines			: true,
	columns				: [{
		dataIndex	: 'id',
		style		: { textAlign: 'center' },
		text		: 'ID',
		width		: 80
	},{
		align		: 'left',
		dataIndex	: 'name',
		flex		: 1,
		style		: { textAlign: 'center' },
		text		: 'Name'
	},{
		align		: 'left',
		dataIndex	: 'type',
		style		: { textAlign: 'center' },
		text		: 'Type'
	}],
	config				: {
		actions	: {
			levelUp		: {
				iconCls	: CFJS.UI.iconCls.BACK,
				handler	: 'onLevelUp',
				tooltip	: 'Back to previous form'
			},
			reloadStore	: {
				handler	: 'onReloadStore',
				iconCls	: CFJS.UI.iconCls.REFRESH,
				tooltip	: 'Update the data'
			}
		},
		form	: null
	},
	contextMenu			: false,
	datatipTpl			: [
		'<b>ID</b>: {id}</br>',
		'<b>Name</b>: {name}</br>',
		'<b>Type</b>: {[this.formatType(values.type)]}'
	],
	defaultListenerScope: true,
	deferEmptyText		: false,
    emptyText			: 'No data to display',
	enableColumnHide	: false,
	enableColumnMove	: false,
	header				: CFJS.UI.buildHeader(),
	loadMask			: true,
	plugins				: [{ ptype: 'datatip' }],
	stripeRows			: false,
	scrollable			: 'y',
	tbar				: [{ xtype: 'displayfield', fieldLabel: 'Path', labelWidth: 50, name: 'path' }],
	title				: 'Field picker',
	
	applyForm: function(form) {
		if (form) {
			if (form.isModel) form = { id: form.id, name: form.get('name')}
			else {
				if (!Ext.isObject(form)) form = CFJS.safeParseInt(form, -1);
				if (Ext.isNumber(form)) form = { id: form };
			}
		}
		return form;
	},
	
	createStore: function() {
		var form = this.getForm();
		return Ext.Factory.store({
			type	: 'fields',
			autoLoad: true,
			filters	: [{
				id		: 'form',
				property: 'form.id',
				type	: 'numeric',
				operator: 'eq',
				value	: (form||{}).id
			}]
		});
	},
	
	initConfig: function(config) {
		var me = this;
		me.header = Ext.apply(Ext.clone(me.header), Ext.clone((me.actionsMap||{}).header));
		me.callParent(arguments);
	},
	
	initComponent: function() {
		var me = this, tpl, plugin,
			types = Ext.Factory.store({
				type: 'enumerated',
				data: CFJS.document.util.Forms.fieldTypes
			}),
			typeRenderer = function(value) {
				var type = value ? types.getById(value) : null;
				return type ? type.get('name') : value;
			};
		// initialize header
		if (me.header) Ext.apply(me.header, me.headerConfig);
		// initialize store
		me.store = me.createStore() || me.store || 'ext-empty-store';
		// initialize field type column renderer
		Ext.Array.each(Ext.Array.from(me.columns), function(column, idx, columns) {
			if (column.dataIndex === 'type') {
				column.renderer = typeRenderer;
				return false;
			}
		});
		// initialize datatip plugin
		if (me.datatipTpl) {
			me.plugins = Ext.Array.from(me.plugins);
			tpl = [].concat(me.datatipTpl, { formatType: typeRenderer });
			if (!(plugin = me.findPlugin('datatip'))) me.plugins.push({ ptype: 'datatip', tpl: tpl });
			else if (!plugin.tpl) plugin.tpl = tpl;
		}
		me.callParent();
		me.on({
			destroyable		: true,
			itemdblclick	: me.onItemDblClick,
			selectionchange	: me.onSelectionChange,
			scope: me
		});
		if (me.pathField = me.down('displayfield[name=path]')) {
			me.pathField.renderer = function(value, field) {
				var path = Ext.Array.from(me.fieldPath, true);
				if (value) path.push(value);
				return Ext.isEmpty(path) ? value : '${' + path.join('.') + '}';
			}
		}
	},
	
	onItemDblClick: function(view, field) {
		var me = this, form = me.getForm(), type = field && field.isModel && field.get('type'),
			child = Ext.Array.contains(['dictionary','document','table'], (type||'').toLowerCase()) && field.get('reference');
		if (child && child.id > 0) {
			if (form) {
				me.formPath = me.formPath || [];
				me.formPath.push(form);
			}
			me.fieldPath = me.fieldPath || [];
			me.fieldPath.push(field.getId());
			me.setForm(child);
		}
	},
	
	onLevelUp: function(component) {
		var me = this, parent = (me.formPath||[]).pop();
		if (parent) {
			(me.fieldPath||[]).pop();
			me.setForm(parent);
		}
	},
	
	onReloadStore: function(component) {
		var store = this.getStore();
		if (store && store.isStore) store.reload();
	},
	
	onSelectionChange: function(view, selection) {
		var me = this;
		if (me.pathField) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			me.pathField.setValue(selection && selection.length > 0 ? selection[0].getId() : null);
		}
	},
	
	updateForm: function(form) {
		var me = this, store = me.getStore(), actions = me.getActions() || {};
		if (store && store.isStore) {
			store.addFilter({
				id		: 'form',
				property: 'form.id',
				type	: 'numeric',
				operator: 'eq',
				value	: (form||{}).id
			});
		}
		me.hideComponent(actions.levelUp, Ext.isEmpty(me.formPath));
	}

});
