Ext.define('CFJS.document.grid.DocumentPanel', {
	extend		: 'CFJS.document.grid.ObjectPanel',
	xtype		: 'document-grid-document-panel',
	iconCls		: CFJS.UI.iconCls.FILE_TEXT,
	promptButton: { text: 'Print' },
	promptDlg	: { title: 'Print on blank' },
	promptField	: { emptyText: 'select a blank', fieldLabel: 'Select strict blank' },

	applyForm: function(form, oldForm) {
		return form;
	},
	
	strictBlankData: function(template, callback) {
		var me = this, promptDlg = me.promptDlg,
			strictForm = template.get('strictForm')||{},
			userInfo, body, iconComponent, promptButton, promptField;
		if (!promptDlg || !promptDlg.isWindow) {
			userInfo = CFJS.lookupViewportViewModel().userInfo()||{};
			promptField = Ext.create(Ext.apply({
				xtype		: 'dictionarycombo',
				flex		: 1,
				name		: 'strictBlank',
				queryParam	: 'number',
				store		: {
					type	: 'named',
					proxy	: { loaderConfig: { dictionaryType: 'strict_blank' } },
					filters	: [{
						id		: 'strictForm',
						property: 'strictForm.id',
						type	: 'numeric',
						operator: 'eq',
						value	: strictForm.id
					},{
						property: 'state',
						type	: 'string',
						operator: 'eq',
						value	: 'BLANK'
					},{
						id		: 'owner',
						property: 'owner.id',
						type	: 'numeric',
						operator: 'eq',
						value	: userInfo.id
					}],
					sorters	: ['series', 'number']
				}
			}, me.promptField));
			iconComponent = Ext.create(Ext.apply({
				xtype	: 'box',
				cls		: Ext.MessageBox.QUESTION,
				margin	: '0 10 0 0'
			}, me.iconComponent));
			body = Ext.create(Ext.apply({
				xtype			: 'form',
				defaultFocus	: promptField.xtype,
				defaultType		: promptField.xtype,
				fieldDefaults	: { labelAlign: 'top', labelWidth: 100, msgTarget: Ext.supports.Touch ? 'side' : 'qtip' },
				layout			: { type: 'hbox' },
				bodyPadding		: 10,
				style			: { overflow: 'hidden' },
				items			: [ iconComponent, promptField ]
			}, me.bodyConfig));
			promptButton = Ext.create(Ext.apply({
				xtype		: 'button',
				handler 	: function() {
			    	if (callback && Ext.isFunction(callback)) {
						var value = promptField.getValue();
						if (value && value.isModel) value = value.getData();
			    		callback.apply(me, ((value||{}).name||'').split('-', 2));
			    	}
					me.promptDlg.close();
				},
				iconCls		: CFJS.UI.iconCls.PRINT,
				minWidth	: 100,
				reference	: 'promptButton'
			}, me.promptButton));
			me.promptDlg = Ext.create(Ext.apply({
				xtype			: 'window',
				closeAction		: 'hide',
				defaultButton	: promptButton.reference,
				defaultFocus	: promptField.xtype,
				iconCls			: CFJS.UI.iconCls.PRINT,
				items			: body,
				layout			: 'fit',
				modal			: true,
				minHeight		: 200,
				minWidth		: 450,
				referenceHolder	: true,
				buttons			: [ promptButton ]
			}, promptDlg));
		} else if (promptField = promptDlg.down('dictionarycombo[name="strictBlank"]')){
			promptField.setValue();
			promptField.getStore().addFilter({
				property	: 'strictForm',
				type		: 'numeric',
				operator	: 'eq',
				value	: strictForm.id
			})
		}
		me.promptDlg.show();
	}

});