Ext.define('CFJS.document.window.ObjectSelector', {
	extend				: 'Ext.window.Window',
	xtype				: 'document-window-object-selector',
	requires			: [
		'Ext.form.Panel',
		'Ext.tab.Panel',
		'Ext.layout.container.HBox',
		'CFJS.form.field.ReferenceComboBox',
		'CFJS.communion.view.AttachmentsEditorModel'
	],
	defaultButton		: 'applyButton',
	defaultFocus		: 'dictionarycombo[name=form]',
	defaultListenerScope: true,
	formType			: undefined,
	iconCls				: CFJS.UI.iconCls.ATTACH,
	layout				: 'fit',
	modal				: true,
	referenceHolder		: true,
	title				: 'Document selection',
	
	createDocumentPanel: function(config) {
		var me = this, vm = me.lookupViewModel(),
			panel = CFJS.apply({
				xtype			: 'form',
				bodyPadding		: 10,
				buttons			: [{
					xtype		: 'button',
					formBind	: true,
					handler		: 'onApplySelecttion',
					iconCls		: CFJS.UI.iconCls.APPLY,
					text		: 'Select',
					minWidth	: 100,
					reference	: me.defaultButton
				}],
				defaults		: {
					flex		: 1,
					listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
					required	: true 
				},
				fieldDefaults	: { labelAlign: 'top', labelWidth: 100, msgTarget: Ext.supports.Touch ? 'side' : 'qtip', selectOnFocus: true },
				items			: [{
					xtype		: 'dictionarycombo',
					emptyText	: 'select a document type',
					fieldLabel	: 'Documents type',
					name		: 'form',
					store		: {
						type	: 'named',
						proxy	: { loaderConfig: { dictionaryType: 'form' }, service: 'svd' },
						filters	: [{
							id		: 'permission',
							type	: 'numeric',
							property: 'permissions[\'READ\']',
							operator: '!lt',
							value	: vm.get('permissionLevel')
						}],
						sorters		: [ 'name' ]
					}
				},{
					xtype			: 'referencecombobox',
					bind			: '{documents}',
					emptyText		: 'select a document',
					fieldLabel		: 'Document',
					hideAddTrigger	: true,
					margin			: '0 0 0 5',
					name			: 'document',
					store			: {
						type		: 'named',
						proxy		: {
							type		: 'json.svdapi',
							loaderConfig: {
								dictionaryType	: 'document',
								unit			: vm.get('userUnit'),
								userInfo		: vm.get('userInfo'),
							},
							service		: 'svd'
						},
						remoteSort	: false
					}
				}],
				layout			: 'hbox',
				name			: 'documentPanel'
			}, config);
		return panel;
	},
	
	initComponent: function() {
		var me = this;
		me.items = [ me.ensureChild('documentPanel') ];
		me.callParent();
		me.documentPanel = me.lookupChild('documentPanel');
		me.formCombo = me.down('combo[name=form]');
		me.documentCombo = me.down('combo[name=document]');
		me.documentCombo.on('afterquery', function(queryPlan) {
			queryPlan.combo.queryCaching = true;
		});
		me.formCombo.on('change', function(field, form) {
			me.documentCombo.store.getProxy().addLoaderConfig({ formId: form && form.id ? form.id : form });
			me.documentCombo.queryCaching = false;
			me.documentCombo.setValue();
		});
		if (!Ext.isEmpty(me.formType)) me.formCombo.store.addFilter({ property: 'type', type: 'string', operator: 'eq', value: me.formType.toUpperCase() });
	},

	onApplySelecttion: function(btn) {
		var me = this, form = me.formCombo.getValue(), document = me.documentCombo.getValue();
		if (me.documentPanel.isValid() && me.fireEvent('beforeselect', me, form, document) !== false) {
			me.fireEvent('select', me, form, document);
			me.close();
		}
	}
	
});