Ext.define('CFJS.document.window.ObjectWindow', {
	extend				: 'Ext.window.Window',
	xtype				: 'document-window-object-window',
	requires			: [
		'CFJS.model.document.Document',
		'CFJS.document.panel.DocumentEditor',
		'CFJS.document.view.ViewModel',
		'CFJS.document.view.WindowController'
	],
	autoShow			: true,
	bind				: { title: '{form.name}' },
	config				: {
		actions		: {
			refreshRecord	: {
				iconCls	: CFJS.UI.iconCls.REFRESH,
				tooltip	: 'Update the data',
				handler	: 'onRefreshRecord'
			},
			saveRecord	: {
				bind	: { disabled: '{!allowed.WRITE}' },
				formBind: true,
				iconCls	: CFJS.UI.iconCls.SAVE,
				tooltip	: 'Save the changes',
				handler	: 'onSaveRecord'
			},
			signRecord	: {
				bind	: { disabled: '{!allowed.SIGN}' },
				formBind: true,
				iconCls	: CFJS.UI.iconCls.SIGN,
				tooltip	: 'Sign the selected document',
				handler	: 'onSignRecord'
			}
		},
		actionsMap	: {
			header: { items: [ 'saveRecord', 'signRecord', 'refreshRecord' ] }
		},
		readOnly		: false
	},
	controller			: 'document.window',
	defaultBindProperty	: 'form',
	header				: CFJS.UI.buildHeader({items: new Array(3)}),
	height				: 400,
	iconCls				: CFJS.UI.iconCls.FILE_TEXT,
	maximizable			: false,
	maximized			: true,
	modal				: true,
	session				: { schema: 'document' },
	viewModel			: { type: 'document.view' },
	width				: 600,
	
	applyReadOnly: function(readOnly) {
		return readOnly === true;
	},
	
	initComponent: function() {
		var me = this;
		me.callParent();
		if (!me.editor || !me.editor.isPanel) {
			me.editor = me.add(Ext.apply({
				xtype	: 'document-panel-editor',
				header	: false,
				readOnly	: me.isReadOnly
			}, me.editorConfig));
		}
		me.editor.getViewModel().setItem();
	},
	
	lookupGrid: Ext.emptyFn,
	
	updateReadOnly: function(readOnly) {
		if (this.editor) this.editor.setReadOnly(readOnly);
	}

});