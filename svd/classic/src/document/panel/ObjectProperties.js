Ext.define('CFJS.document.panel.ObjectProperties', {
	extend				: 'Ext.tab.Panel',
	xtype				: 'document-panel-object-properties',
	requires			: [
		'Ext.form.field.*',
		'CFJS.admin.form.field.ContractorPicker',
		'CFJS.document.grid.SignatoriesPanel',
		'CFJS.document.view.ObjectPropertiesModel',
		'CFJS.project.tab.ProcessElementDesigner'
	],
	bind				: { disabled: '{_itemName_.id <= 0}', title: 'Object properties: {_itemName_.name}' },
	config				: {
		actions		: {
			createProcess	: {
				bind	: { hidden: '{!canCreateProcessPlan}' },
				handler	: 'onCreateProcess',
				iconCls	: CFJS.UI.iconCls.SITEMAP,
				tooltip	: 'Create an action plan'
			},
			saveProperties	: {
				bind	: { disabled: '{!_itemName_}', hidden: '{!allowed.WRITE}' },
				handler	: 'onSaveProperties',
				iconCls	: CFJS.UI.iconCls.SAVE,
				tooltip	: 'Save the changes'
			}
		},
		actionsMap	: { header: { items: [ 'saveProperties', 'createProcess' ] } },
		headerConfig: CFJS.UI.buildHeader()
	},
	contextMenu			: false,
	defaultListenerScope: true,
	header				: { items: new Array(2) },
	height				: 400,
	iconCls				: CFJS.UI.iconCls.COG,
	minHeight			: 350,
	minWidth			: 400,
	removePanelHeader	: false,
	session				: { schema: 'document' },
	tabBarHeaderPosition: 1,
	tabIconsOnly		: true,
	width				: 600,
	viewModel			: { type: 'document.properties' },

	createDescriptionTab: function(config) {
		var me = this, vm = me.lookupViewModel(),
			documentActions = vm.get('documentActions'),
			permissions = vm.get('permissions'),
			tab = CFJS.apply({
				xtype			: 'form',
				bind			: { readOnly: '{!allowed.WRITE}' },
				bodyPadding		: 10,
				defaults		: {
					anchor		: '100%',
					defaultType	: 'displayfield',
					defaults	: { flex: 1, margin: '0 0 0 10' },
					layout		: 'hbox'
				},
				defaultType		: 'container',
				fieldDefaults	: { labelAlign: 'left', labelWidth: 110, msgTarget: Ext.supports.Touch ? 'side' : 'qtip', selectOnFocus: true },
				header			: false,
				iconCls			: CFJS.UI.iconCls.HOME,
				items			: [{
					items: [
						{ bind: '{_itemName_.id}', fieldLabel: 'ID', margin: 0, name: 'id' },
						{ bind: '{_itemName_.name}', fieldLabel: 'Name', name: 'name' }
					]
				},{
					items: [
						{ bind: '{_itemName_.created}', fieldLabel: 'Created', renderer: Ext.util.Format.dateRenderer(Ext.Date.patterns.LongDateTime), name: 'created', margin: 0 },
						{ bind: '{_itemName_.updated}', fieldLabel: 'Updated', renderer: Ext.util.Format.dateRenderer(Ext.Date.patterns.LongDateTime), name: 'updated' }
					]
				},{
					items: [{ bind: '{_itemName_.author.name}', fieldLabel: 'Author', margin: 0, name: 'author' }],
					margin: '0 0 10 0'
				},{
					defaultType	: 'dictionarycombo',
					defaults	: {
						flex		: 1,
						listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
						publishes	: 'value',
					},
					items		: [{
						bind			: { value: '{_itemName_.unit}' },
						emptyText		: 'select a unit',
						fieldLabel		: 'Unit',
						name			: 'unit',
						required		: true,
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'unit' } },
							sorters	: [ 'name' ]
						}
					},{
						bind			: { value: '{_itemName_.sector}' },
						emptyText		: 'select a sector',
						fieldLabel		: 'Sector',
						listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
						margin			: '0 0 0 10',
						name			: 'sector',
						store			: {
							type	: 'named',
							proxy	: { loaderConfig: { dictionaryType: 'sector' } },
							sorters	: [ 'name' ]
						}
					}]
				},{
					defaultType	: 'datetimefield',
					defaults	: {
						fieldOffset	: 2,
						fieldTime	: { format: Ext.Date.patterns.LongTime, width: 90 },
						flex		: 1,
						format		: Ext.Date.patterns.LongDateTime
					},
					margin		: '10 0',
					items		: [{
						bind		: { minValue: '{_itemName_.created}', value: '{_itemName_.startDate}' },
						fieldLabel	: 'Start date',
						required	: true,
						name		: 'startDate'
					},{
						bind		: { minValue: '{_itemName_.startDate}', value: '{_itemName_.endDate}' },
						fieldLabel	: 'End date',
						margin		: '0 0 0 10',
						name		: 'endDate'
					}]
				},{
					xtype			: 'grid-enum-panel',
					bind			: { readOnly: '{!allowed.CHANGE_PERMISSION}', store: '{objectPermissions}' },
					defaults		: {},
					layout			: 'fit',
					margin			: '20 0 10',
					name			: 'permissions',
					nameField		: 'action',
					nameRenderer	: function(value) {
						value = documentActions.getById(value) || value;
						return value && value.isModel ? value.get('name') : value;
					},
					nameText		: 'Action',
					title			: 'Permissions',
					userCls			: 'shadow-panel',
					valueEditor		: {
						xtype			: 'combo',
						bind			: { store: '{permissions}' },
						displayField	: 'name',
						editable		: false,
						forceSelection  : true,
						listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
						name			: 'permissionLevel',
						queryMode		: 'local',
						selectOnFocus	: false,
						valueField		: 'level'
					},
					valueField		: 'level',
					valueRenderer	: function(value) {
						value = permissions.findRecord('level', value, 0, false, false, true) || value;
						return value && value.isModel ? value.get('name') : value;
					},
					valueText		: 'Level'
				}],
				layout			: 'anchor',
				name			: 'descriptionTab',
				scrollable		: 'y',
				tabConfig		: { tooltip: 'Description' }
			}, config);
		return tab;
	},

	createProcessTab: function(config) {
		var me = this,
			types = Ext.getStore('principleTypes'),
			type = types && types.getById('PROCESS'),
			tab = CFJS.apply({
				xtype			: 'project-tab-process-element-designer',
				bind			: { title: '{itemTitle}' },
				documentsView	: false,
				header			: false,
				hidden			: true,
				iconCls			: type ? type.get('iconCls') : null,
				name			: 'processTab',
				tabConfig		: { title: null, tooltip: 'Process' },
				tabIconsOnly	: true,
				viewModel		: {
					type		: 'project.control-edit',
					isDetail	: true,
					itemModel	: 'CFJS.model.project.Process',
					itemName	: 'documentProcess'
				}
			}, config);
		return tab;
	},
	
	createResourcesTab: function(config) {
		var me = this,
			tab = CFJS.apply({
				xtype		: 'grid-simple-resource-browser',
				bind		: { readOnly: '{!allowed.ATTACH_FILES}', store: '{documentResources}' },
				iconCls		: CFJS.UI.iconCls.ATTACH,
				margin		: 10,
				name		: 'resourcesTab',
				parentField	: 'documentId',
				service		: 'svd',
				tabConfig	: { tooltip: 'Attachments' },
				title		: 'Attachments',
				userCls		: 'shadow-panel'
			}, config);
		return tab;
	},

	createSignatoriesTab: function(config) {
		var me = this,
			tab = CFJS.apply({
				xtype		: 'document-grid-signatories-panel',
				bind		: '{signatories}',
				margin		: 10,
				name		: 'signatoriesTab',
				tabConfig	: { tooltip: 'Signatories' },
				title		: 'Signatories',
				userCls		: 'shadow-panel'
			}, config);
		return tab;
	},
	
	createWorkflowMenu: function(workflows) {
		var me = this, action = me.getActions().createProcess,
			i = 0, items = [], data = {}, options;
		if (action && action.isAction) {
			options = action.initialConfig;
			options = { handler: options.handler, iconCls: options.iconCls };
			for (; i < workflows.length; i++ ) {
				items.push(Ext.apply({
					scope	: me,
					workflow: workflows[i],
					text	: workflows[i].name
				}, options));
			}
			if (items.length === 1) data.workflow = items[0].workflow;
			else if (items.length > 1) data.menu = { items: items };
			action.each(function(component) {
				component.workflow = data.workflow;
				if (Ext.isFunction(component.setMenu)) component.setMenu(data.menu, true);
			});
		}
	},
	
	hideProcessTab: function(hasProcess) {
		var me = this, panel = me.processTab, vm = me.lookupViewModel();
		if (!hasProcess && me.getActiveTab() === panel) me.setActiveTab(0);
		me.hideComponent(panel.tab, !hasProcess);
	},
	
	initComponent: function() {
		var me = this, actions = me.getActions(), vm = me.lookupViewModel();
		if (me.header) Ext.applyIf(me.header, me.headerConfig);
		me.items = [ me.ensureChild('descriptionTab'), me.ensureChild('resourcesTab'), me.ensureChild('processTab'), me.ensureChild('signatoriesTab') ];
		me.getRecord = me.getRecord || me.lookupRecord;
		me.callParent();
		me.descriptionTab = me.down('[name="descriptionTab"]');
		me.processTab = me.down('[name="processTab"]');
		me.resourcesTab = me.down('[name="resourcesTab"]');
		me.signatoriesTab = me.down('[name="signatoriesTab"]');
		me.on('beforesaverecord', vm.beforeSaveProperties, vm);
		vm.bind('{hasProcess}', me.hideProcessTab, me);
		vm.bind('{form.workflows}', me.createWorkflowMenu, me);
	},
	
	lookupRecord: function() {
		return this.lookupViewModel().getItem();
	},
	
	onCreateProcess: function(component) {
		if (component && component.workflow) {
			var me = this, vm = me.lookupViewModel();
			if (vm && Ext.isFunction(vm.createProcess)) vm.createProcess(component.workflow);
		}
	},
    
	onSaveProperties: function(component, callback) {
		var me = this, record = me.lookupRecord();
		if (record && me.fireEvent('beforesaverecord', record) !== false) {
			if (record.dirty || record.phantom) {
				me.disableComponent(component, true);
				record.save({
					params	: { locale: CFJS.getLocale() },
					callback: function(record, operation, success) {
						me.disableComponent(component);
						Ext.callback(callback, me, [record, success]);
					}
				});
			} else Ext.callback(callback, me, [record, true]);
		}
	},
	
	setRecord: function(record) {
		this.lookupViewModel().setItem(record);
	}

});