Ext.define('CFJS.document.panel.FieldsExplorer', {
	extend			: 'Ext.form.Panel',
	xtype			: 'document-panel-fields-explorer',
	requires		: [
		'Ext.form.field.*',
		'Ext.layout.container.VBox',
		'CFJS.form.DateTimeField',
		'CFJS.form.field.*'
	],
	bodyPadding			: 10,
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	fieldDefaults		: {
		labelAlign		: 'left',
		labelWidth		: 120,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true				
	},
	iconCls				: 'x-fa fa-i-cursor',
	items				: [{
		xtype			: 'dictionarycombo',
		bind			: { store: '{fields}', value: '{field}' },
		editable		: false,
		fieldLabel		: 'Field',
		listConfig		: { itemTpl: ['<div data-qtip="{id}. {name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{id}. {name}</div>'] },
		name			: 'field',
		queryMode		: 'local',
		selectOnFocus	: false,
		typeAhead		: false,
		triggers		: {
			add		: { cls: Ext.baseCSSPrefix + 'form-fa-trigger', extraCls: CFJS.UI.iconCls.ADD,		handler: 'onAddField',		tooltip: 'Add new field' },
			remove	: { cls: Ext.baseCSSPrefix + 'form-fa-trigger', extraCls: CFJS.UI.iconCls.DELETE,	handler: 'onRemoveField',	tooltip: 'Remove current field', weight: 1 }
		}
	},{
		xtype		: 'container',
		bind		: { hidden: '{!field}' },
		defaults	: { anchor: '100%', allowBlank: false },
		defaultType	: 'textfield',
		flex		: 1,
		layout		: 'anchor',
		name		: 'fieldProperty',
		scrollable	: 'y',
		items		: [{
			bind		: '{field.name}',
			emptyText	: 'input name',
			fieldLabel	: 'Name',
			name		: 'name',
			required	: true
		},{
			xtype		: 'numberfield',
			bind		: { value: '{columnOrder}' },
			editable	: false,
			fieldLabel	: 'Order',
			minValue	: 1,
			name		: 'columnOrder'
		},{
			xtype			: 'combo',
			bind			: { store: '{fieldTypes}', value: '{fieldType}' },
			displayField	: 'name',
			editable		: false,
			fieldLabel		: 'Data type',
			forceSelection  : true,
			listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name			: 'type',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'id'
		},{
			xtype		: 'checkbox',
			bind		: { hidden: '{!fieldsMap.required}', value: '{field.required}' },
			fieldLabel	: 'Required',
			name		: 'required'
		},{
			xtype		: 'checkbox',
			bind		: { hidden: '{!fieldsMap.description}', value: '{field.description}' },
			fieldLabel	: 'Description',
			name		: 'description'
		},{
			xtype		: 'checkbox',
			bind		: { hidden: '{!fieldsMap.notCopy}', value: '{field.notCopy}' },
			fieldLabel	: 'Don\'t copy',
			name		: 'notCopy'
		},{
			xtype		: 'checkbox',
			bind		: { hidden: '{!fieldsMap.readOnly}', value: '{field.readOnly}' },
			fieldLabel	: 'Read only',
			name		: 'readOnly'
		},{
			bind		: { hidden: '{!fieldsMap.mask}', value: '{field.mask}' },
			fieldLabel	: 'Mask',
			name		: 'mask'
		},{
			bind		: { hidden: '{!fieldsMap.masterFilter}', value: '{field.masterFilter}' },
			fieldLabel	: 'Master filter',
			name		: 'masterFilter'
		},{
			xtype		: 'acefield',
			bind		: { hidden: '{!fieldsMap.formula}', value: '{field.formula}' },
			fieldLabel	: 'Formula',
			mode		: 'text',
			name		: 'formula'
		},{
			xtype			: 'combo',
			bind			: { hidden: '{!fieldsMap.aggregation}', store: '{aggregations}', value: '{field.aggregation}' },
			displayField	: 'name',
			editable		: false,
			fieldLabel		: 'Aggregation',
			forceSelection  : true,
			name			: 'aggregation',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'id'
		},{
			xtype			: 'combo',
			bind			: { store: '{permissions}', value: '{field.permissionLevel}' },
			displayField	: 'name',
			editable		: false,
			fieldLabel		: 'Permission level',
			forceSelection  : true,
			listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name			: 'permissionLevel',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'level'
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			allowExponential: false,
			bind			: { hidden: '{!fieldsMap.columnWidth}', value: '{field.columnWidth}' },
			fieldLabel		: 'Width',
			minValue		: 0,
			name			: 'columnWidth',
			required		: true
		},{
			xtype		: 'checkbox',
			bind		: { hidden: '{!fieldsMap.columnHidden}', value: '{field.columnHidden}' },
			fieldLabel	: 'Hidden',
			name		: 'columnHidden'
		}]
	}],
	layout				: { type: 'vbox', align: 'stretch' },
	referenceHolder		: true,
	savingMsg			: 'Saving data...',
	title				: 'Fields',
	
	confirmRemove: function(name, fn) {
		return Ext.fireEvent('confirmlostdata', 'delete a field "'+ name + '"', fn, this);
	},
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		vm.bind([ '{field.id}', '{field.type}' ], me.onFieldTypeChange, me);
		me.callParent();
	},

	onAddField: function(combo) {
		var store = combo && combo.getStore(), index;
		if (store && store.isStore) {
			index = store.getCount() + 1;
			combo.setValue(store.add({
				columnOrder	: index,
				type		: 'text',
				name		: Ext.String.format('{0} {1}', CFJS.document.util.Forms.fieldTypeName('text'), index)
			}));
		}
	},
	
	onFieldTypeChange: function(data) {
		data = Ext.Array.from(data || []);
		var me = this, vm = me.lookupViewModel(),
			id = data.length > 0 && data[0], 
			type = data.length > 1 && data[1],
			container = me.down('container[name="fieldProperty"]'),
			items = container.items, editor;
		container.remove(container.child('[name="reference"]'));
		container.remove(container.child('[name="defaultValue"]'));
		if (vm.get('fieldsMap.reference') === true) {
			editor = {
				xtype			: 'dictionarycombo',
				allowBlank		: false,
				bind			: { value: '{field.reference}' },
				fieldLabel		: 'Reference',
				listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
				name			: 'reference',
				selectOnFocus	: true,
				store			: vm.get('references')
			};
			if (type === 'sys_dictionary') {
				editor = Ext.apply(editor, {
					autoLoadOnValue	: true,
					bind			: { value: '{field.reference}' },
					editable		: false,
					displayField	: 'title',
					displayTpl		: [
						'<tpl for=".">{[this.render(values, xindex, xcount)]}</tpl>', 
						{
							displayField: 'title',
							delimiter	: ', ',
							valueField	: 'name',
							render: function(value, idx, count) {
								if (!value) return value;
								var me = this, str = typeof value === "string" ? value : value[me.displayField];
								if (!str) str = value[me.valueField] || '';
								if (idx < count) str += me.delimiter;
								return str;
							}
						}
					],
					listConfig		: { itemTpl: ['<div data-qtip="{title}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{title}</div>'] },
					queryMode		: 'local',
					selectOnFocus	: false,
					store			: vm.get('sysDictionaries'),
					typeAhead		: false,
					valueField		: 'name'
				});
			} else {
				editor.store.addFilter({ id: 'type', type: 'string', property: 'type', operator: 'eq', value: type });
			}
			container.insert(items.indexOf(container.child('[name="type"]')) + 1, editor);
		}
		if (type !== 'table') {
			editor = Ext.apply(CFJS.document.util.Forms.renderField(vm.get('field'), container, true), {
				bind		: { value: '{defaultValue}' },
				fieldLabel	: 'Default value',
				id			: [me.id, type, 'defaultValue', vm.get('field.id')].join('-'),
				itemId		: [me.id, type, 'defaultValue', vm.get('field.id')].join('-'),
				name		: 'defaultValue',
				readOnly	: false,
				required	: false
			});
			container.insert(items.indexOf(container.child('[name="formula"]')) + 1, editor);
		}
	},

	onRemoveField: function(combo) {
		var me = this, store = combo.getStore(), record = combo.getValue();
		if (store && store.isStore && record) {
			if (!record.isModel) record = store.getById(record.id);
			if (record) me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.lookupViewModel().reorderFields(store.getCount());
					store.remove(record);
					if (store.getCount() > 0) combo.setValue(store.getAt(0));
				}
			});
		}
	}
	
});