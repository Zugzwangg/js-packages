Ext.define('CFJS.document.panel.DocumentEditor', {
	extend		: 'CFJS.document.panel.ObjectEditor',
	xtype		: 'document-panel-editor',
	requires	: [ 'CFJS.document.view.EditModel' ],
	actions		: {
		addRecord	: { tooltip: 'Add new document' },
		exportRecord: { tooltip: 'Export current document to Excel' },
		printRecord	: { tooltip: 'Print current document' },
		removeRecord: { tooltip: 'Delete current document' },
		signRecord	: { tooltip: 'Sign the selected document' }
	}
});