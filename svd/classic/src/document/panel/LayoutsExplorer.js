Ext.define('CFJS.document.panel.LayoutsExplorer', {
	extend				: 'Ext.form.Panel',
	xtype				: 'document-panel-layouts-explorer',
	requires			: [
		'Ext.container.Container',
		'Ext.form.CheckboxGroup',
		'Ext.form.field.*',
		'Ext.layout.container.VBox',
		'CFJS.form.field.*'
	],
	bodyPadding			: 10,
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	fieldDefaults		: {
		labelAlign		: 'left',
		labelWidth		: 120,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true				
	},
	iconCls				: 'x-fa fa-eye',
	items				: [{
		xtype			: 'dictionarycombo',
		bind			: { store: '{layouts}', value: '{layout}' },
		editable		: false,
		fieldLabel		: 'Layout',
		listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
		name			: 'layout',
		queryMode		: 'local',
		selectOnFocus	: false,
		typeAhead		: false,
		triggers		: {
			add		: { cls: Ext.baseCSSPrefix + 'form-fa-trigger', extraCls: CFJS.UI.iconCls.ADD,		handler: 'onAddLayout',		tooltip: 'Add new layout' },
			remove	: { cls: Ext.baseCSSPrefix + 'form-fa-trigger', extraCls: CFJS.UI.iconCls.DELETE,	handler: 'onRemoveLayout',	tooltip: 'Remove current layout', weight: 1 }
		}
	},{
		xtype		: 'container',
		bind		: { hidden: '{!layout}' },
		defaults	: { anchor: '100%' },
		defaultType	: 'textfield',
		layout		: 'anchor',
		name		: 'layoutData',
		flex		: 1,
		scrollable	: 'y',
		items		: [{
			bind		: '{layout.name}',
			emptyText	: 'input name',
			fieldLabel	: 'Name',
			name		: 'name',
			required	: true
		},{
			xtype		: 'numberfield',
			bind		: { value: '{setOrder}' },
			editable	: false,
			fieldLabel	: 'Order',
			minValue	: 1,
			name		: 'setOrder'
		},{
			xtype		: 'numberfield',
			bind		: { value: '{layoutData.columns}' },
			editable	: false,
			fieldLabel	: 'Columns',
			minValue	: 1,
			name		: 'columns'
		},{
			xtype		: 'checkboxgroup',
			fieldLabel	: 'Header',
			name		: 'header',
			items		: [
				{ bind: '{layout.borders}',		boxLabel: 'Border',			name: 'borders' },
				{ bind: '{layout.collapsible}',	boxLabel: 'Collapsible',	name: 'collapsible' },
				{ bind: '{layout.hideHeader}',	boxLabel: 'Hidden',			name: 'hideHeader' }
			]
		},{
			bind		: '{layoutData.fieldAnchor}',
			fieldLabel	: 'Field anchor',
			name		: 'fieldAnchor'
		},{
			xtype		: 'checkbox',
			bind		: '{layoutData.hideLabels}',
			fieldLabel	: 'Labels hidden',
			name		: 'hideLabels'
		},{
			xtype			: 'combo',
			bind			: { store: '{lAlign}', value: '{layoutData.labelAlign}' },
			displayField	: 'name',
			editable		: false,
			fieldLabel		: 'Labels align',
			forceSelection  : true,
			name			: 'labelAlign',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'id'
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			allowExponential: false,
			bind			: { value: '{layoutData.labelPad}' },
			fieldLabel		: 'Labels padding',
			minValue		: 0,
			name			: 'labelPad',
			required		: true
		},{
			bind			: '{layoutData.labelSeparator}',
			fieldLabel		: 'Labels separator',
			name			: 'labelSeparator',
			required		: true
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			allowExponential: false,
			bind			: { value: '{layoutData.labelWidth}' },
			fieldLabel		: 'Labels width',
			minValue		: 0,
			name			: 'labelWidth',
			required		: true
		},{
			xtype			: 'combo',
			bind			: { store: '{hAlign}', value: '{layoutData.cellHorizontalAlign}' },
			displayField	: 'name',
			editable		: false,
			fieldLabel		: 'Horizontal align',
			forceSelection  : true,
			name			: 'cellHorizontalAlign',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'id'
		},{
			xtype			: 'combo',
			bind			: { store: '{vAlign}', value: '{layoutData.cellVerticalAlign}' },
			displayField	: 'name',
			editable		: false,
			fieldLabel		: 'Vertical align ',
			forceSelection  : true,
			name			: 'cellVerticalAlign',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'id'
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			allowExponential: false,
			bind			: { value: '{layoutData.border}' },
			fieldLabel		: 'Border width',
			minValue		: 0,
			name			: 'border',
			required		: true
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			allowExponential: false,
			bind			: { value: '{layoutData.cellPadding}' },
			fieldLabel		: 'Padding',
			minValue		: 0,
			name			: 'cellPadding',
			required		: true
		},{
			xtype			: 'numberfield',
			allowDecimals	: false,
			allowExponential: false,
			bind			: { value: '{layoutData.cellSpacing}' },
			fieldLabel		: 'Spacing',
			minValue		: 0,
			name			: 'cellSpacing',
			required		: true
		},{
			xtype		: 'checkbox',
			bind		: '{layoutData.insertSpacer}',
			fieldLabel	: 'Insert spacer',
			name		: 'insertSpacer'
		},{
			xtype		: 'acefield',
			bind		: '{layoutData.tableStyle}',
			emptyText	: 'CSS style rules',
			fieldLabel	: 'Style',
			mode		: 'css',
			name		: 'tableStyle'
		},{
			xtype				: 'multiselector',
			bind				: { search: '{fieldsSearch}', store: '{layoutFields}' },
			addToolText			: 'Search for fields to add',
			bodyStyle			: Ext.theme.name === 'Crisp' ? { borderTopWidth: '1px !important;' } : null,
			convertSearchRecord	: function(record) {
				if (record && record.isModel && record.id) {
					record = new CFJS.model.document.LayoutField({ id: record.id, layoutField: { id: record.id }, order: record.get('columnOrder')});
				}
				return record;
			},
			deferEmptyText		: false,
            emptyText			: 'No fields selected',
			fieldName			: 'id',
			fieldConfig			: {
				align	: 'left',
				renderer: function(value, metaData, record) {
					if (value) {
						var me = this, searchStore = me.getSearch().store;
						if (searchStore && searchStore.isStore) value = searchStore.getById(value);
						else value = me.lookupViewModel().findField(value);
						if (value && value.isModel) {
							if (!record.get('order')) record.set('order', value.get('columnOrder'), { dirty: false, silent: true });
							value = value.get('name');
						}
					}
					return value;
				}
			},
			margin				: '10 5 5 5',
			name				: 'layoutFields',
			removeRowTip		: 'Remove this field',
			title				: 'Layout fields',
			userCls				: 'shadow-panel'
		},{
			xtype		: 'panel',
			bind		: { hidden: '{!layoutField}' },
			defaults	: { anchor: '100%' },
			defaultType	: 'textfield',
			layout		: 'anchor',
			margin		: '10 0 0 0',
			name		: 'layoutFieldData',
			title		: 'Field properties',
			items		: [{
				xtype			: 'numberfield',
				allowExponential: false,
				bind			: { value: '{layoutField.width}' },
				fieldLabel		: 'Width',
				minValue		: -1,
				name			: 'width',
				required		: true
			},{
				xtype			: 'numberfield',
				allowExponential: false,
				bind			: { value: '{layoutField.height}' },
				fieldLabel		: 'Height',
				minValue		: -1,
				name			: 'height',
				required		: true
			},{
				xtype			: 'numberfield',
				allowDecimals	: false,
				allowExponential: false,
				bind			: { value: '{layoutField.colspan}' },
				fieldLabel		: 'Colspan',
				minValue		: 1,
				name			: 'colspan',
				required		: true
			},{
				xtype			: 'numberfield',
				allowDecimals	: false,
				allowExponential: false,
				bind			: { value: '{layoutField.rowspan}' },
				fieldLabel		: 'Rowspan',
				minValue		: 1,
				name			: 'rowspan',
				required		: true
			},{
				xtype			: 'numberfield',
				allowDecimals	: false,
				allowExponential: false,
				bind			: { value: '{layoutField.margin}' },
				fieldLabel		: 'Margin',
				minValue		: 0,
				name			: 'margin',
				required		: true
			},{
				xtype			: 'numberfield',
				allowDecimals	: false,
				allowExponential: false,
				bind			: { value: '{layoutField.padding}' },
				fieldLabel		: 'Padding',
				minValue		: 0,
				name			: 'padding',
				required		: true
			},{
				xtype			: 'combo',
				bind			: { store: '{hAlign}', value: '{layoutField.horizontalAlign}' },
				displayField	: 'name',
				editable		: false,
				fieldLabel		: 'Horizontal align ',
				forceSelection  : true,
				name			: 'horizontalAlign',
				queryMode		: 'local',
				selectOnFocus	: false,
				valueField		: 'id'
			},{
				xtype			: 'combo',
				bind			: { store: '{vAlign}', value: '{layoutField.verticalAlign}' },
				displayField	: 'name',
				editable		: false,
				fieldLabel		: 'Vertical align ',
				forceSelection  : true,
				name			: 'verticalAlign',
				queryMode		: 'local',
				selectOnFocus	: false,
				valueField		: 'id'
			},{
				xtype		: 'acefield',
				bind		: '{layoutField.style}',
				emptyText	: 'CSS style rules',
				fieldLabel	: 'Style',
				mode		: 'css',
				name		: 'style'
			},{
				bind		: '{layoutField.styleName}',
				emptyText	: 'CSS style name',
				fieldLabel	: 'Style name',
				name		: 'styleName'
			}]
		}]
	}],
	layout				: { type: 'vbox', align: 'stretch' },
	newLayoutNameText	: 'Group',
	referenceHolder		: true,
	title				: 'Layouts',
	
	afterRender: function() {
		var me = this,
			layout = me.down('dictionarycombo[name="layout"]'),
			layoutFields = me.down('multiselector[name="layoutFields"]');
		if (layout) layout.on({ beforeselect: me.beforeLayoutSelect, scope: me });
		if (layoutFields) layoutFields.on({ selectionchange: me.onFieldsSelectionChange, scope: me });
		me.callParent(arguments);
	},
	
	beforeLayoutSelect: function(combo, record, index) {
		var me = this, vm = me.lookupViewModel(),
			store = vm.get('layoutFields'), fields = [];
		vm.set('layout.layoutData', vm.get('layoutData'));
		if (store && store.isStore) {
			store.each(function(record) {
				fields.push(record.getData());
			});
			vm.set('layout.fields', fields);
		}
	},
	
	confirmRemove: function(name, fn) {
		return Ext.fireEvent('confirmlostdata', 'delete a layout "'+ name + '"', fn, this);
	},

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
//		vm.bind('{layout.fields}', me.onFieldsChange, me);
		me.callParent();
	},
	
	onAddLayout: function(combo) {
		var store = combo && combo.getStore(), index;
		if (store && store.isStore) {
			index = store.getCount() + 1;
			combo.setValue(store.add({
				setOrder: index,
				name: Ext.String.format('{0} {1}', this.newLayoutNameText, index)
			}));
		}
	},
	
	onFieldsSelectionChange: function(selModel, selected) {
		if (selected) {
			if (selected.length > 0) selected = selected[0];
			selected = selected.isModel && !selected.removedFrom && selected.get('layoutField');
		}
		this.lookupViewModel().set('layoutField', selected || null);
	},
	
	onRemoveLayout: function(combo) {
		var me = this, store = combo.getStore(), record = combo.getValue();
		if (store && store.isStore && record) {
			if (!record.isModel) record = store.getById(record.id);
			if (record) me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.lookupViewModel().reorderLayouts(store.getCount());
					store.remove(record);
					if (store.getCount() > 0) combo.setValue(store.getAt(0));
				}
			});
		}
	}
	
});