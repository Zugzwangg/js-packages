Ext.define('CFJS.document.panel.FormEditor', {
	extend				: 'Ext.form.Panel',
	xtype				: 'document-panel-form-editor',
	requires			: [
		'Ext.form.field.*',
		'Ext.layout.container.Anchor',
		'CFJS.form.DateTimeField',
		'CFJS.form.field.*',
		'CFJS.grid.EnumGrid',
		'CFJS.store.document.FormGroups',
		'CFJS.document.util.Forms'
	],
	bodyPadding			: '10 5',
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	defaults			: { anchor: '100%', allowBlank: false, margin: '0 5 5 5' },
	defaultType			: 'textfield',
	fieldDefaults		: {
		labelAlign		: 'left',
		labelWidth		: 120,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true				
	},
	iconCls				: CFJS.UI.iconCls.HOME,
	items				: [{
		xtype		: 'displayfield',
		bind		: '{_itemName_.id}',
		fieldLabel	: 'ID',
		name		: 'id'
	},{
		bind		: '{_itemName_.name}',
		emptyText	: 'input name',
		fieldLabel	: 'Name',
		name		: 'name',
		required	: true
	},{
		xtype		: 'dictionarytreepicker',
		bind		: { hidden: '{_itemName_.type!=="DOCUMENT"}', value: '{_itemName_.group}' },
		columns		: [{
			xtype		: 'treecolumn',
			align		: 'left',
			flex		: 1,
			dataIndex	: 'name'
		}],
		editable	: true,
		emptyText	: 'choose a group',
		fieldLabel	: 'Group',
		name		: 'group',
		store		: Ext.Factory.store({
			type		: 'formgroups',
			pageSize	: 0,
			remoteSort	: false
		})
	},{
		xtype		: 'dictionarytag',
		bind		: { hidden: '{_itemName_.type==="TABLE"}', store: '{workflows}', value: '{_itemName_.workflows}' },
		emptyText	: 'select a workflow',
		growMax		: 100,
		growMin		: 100,
		fieldLabel	: 'Workflows',
		name		: 'workflows', 
		stacked		: true
	},{
		xtype		: 'dictionarytag',
		bind		: { hidden: '{_itemName_.type!=="DOCUMENT"}', store: '{strictForms}', value: '{_itemName_.strictForms}' },
		growMax		: 100,
		growMin		: 100,
		fieldLabel	: 'Strict forms',
		name		: 'strictForms', 
		stacked		: true
	},{
		xtype		: 'dictionarytag',
		bind		: { hidden: '{_itemName_.type==="TABLE"}', store: '{sectors}', value: '{_itemName_.sectors}' },
		growMax		: 100,
		growMin		: 100,
		fieldLabel	: 'Sector',
		name		: 'sectors',
		queryParam	: 'sector.name',
		stacked		: true
	},{
		xtype		: 'fieldcontainer',
		bind		: { hidden: '{_itemName_.type==="TABLE"}' },
        fieldLabel	: 'Unit',
        layout		: 'hbox',
		items		: [{
			xtype		: 'checkbox',
			bind		: '{_itemName_.checkUnit}',
			margin		: '0 5 0 0',
			name		: 'checkUnit'
		},{
			xtype		: 'dictionarycombo',
			bind		: { value: '{_itemName_.unit}' },
//			bind		: { disabled: '{!_itemName_.checkUnit}', value: '{_itemName_.unit}' },
			emptyText	: 'select a unit',
			flex		: 1,
			minChars	: 1,
			name		: 'unit',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'unit' } },
				sorters	: [ 'name' ]
			}
		}]
	},{
		xtype			: 'numberfield',
		allowDecimals	: false,
		allowExponential: false,
		bind			: '{_itemName_.width}',
		fieldLabel		: 'Width',
		minValue		: 0,
		name			: 'width',
		required		: true
	},{
		xtype			: 'numberfield',
		allowDecimals	: false,
		allowExponential: false,
		bind			: '{_itemName_.height}',
		fieldLabel		: 'Height',
		minValue		: 0,
		name			: 'height',
		required		: true
	},{
		xtype		: 'grid-enum-panel',
		cls			: 'permissions shadow-panel',
		bind		: { hidden: '{_itemName_.type==="TABLE"}', store: '{formPermissions}' },
		headerConfig: { style: { borderWidth: '1px 0 0 0 !important;' } },
		margin		: '10 5 0 5',
		name		: 'permissions',
		nameField	: 'action',
		nameText	: 'Action',
		title		: 'Permissions',
		valueEditor	: {
			xtype			: 'combo',
			bind			: { store: '{permissions}' },
			displayField	: 'name',
			editable		: false,
			forceSelection  : true,
			listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name			: 'permissionLevel',
			queryMode		: 'local',
			selectOnFocus	: false,
			valueField		: 'level'
		},
		valueField	: 'level',
		valueText	: 'Level'
	}],
	layout				: 'anchor',
	referenceHolder		: true,
	scrollable			: 'y',
	title				: 'Main',
	
	afterRender: function() {
		var me = this, grid = me.down('grid-enum-panel[name="permissions"]'), 
			vm = me.lookupViewModel(), documentActions, permissions;
		if (grid) {
			if ((documentActions = vm.get('documentActions')) && documentActions.isStore) {
				grid.nameColumn.renderer = function(value) {
					value = documentActions.getById(value) || value;
					return value && value.isModel ? value.get('name') : value;
				};
			}
			if ((permissions = vm.get('permissions')) && permissions.isStore) {
				grid.valueColumn.renderer = function(value) {
					value = permissions.findRecord('level', value, 0, false, false, true) || value;
					return value && value.isModel ? value.get('name') : value;
				}
			}
		}
		me.callParent(arguments);
	}

});