Ext.define('CFJS.document.panel.TemplateEditor', {
	extend		: 'CFJS.panel.FilesEditor',
	xtype		: 'document-panel-template-editor',
	requires	: [
		'Ext.tree.Panel',
		'Ext.layout.container.Accordion'
	],
	autoUpload	: true,
	config		: { form: null, templateType: 'pdf' },
	excelIconCls: CFJS.UI.iconCls.FILE_EXCEL,
	excelTitle	: 'Export template',
	fileType	: 'svd/templates',
	pdfIconCls	: CFJS.UI.iconCls.FILE_PDF,
	pdfTitle	: 'Print template',
	
	applyForm: function(form) {
		if (form) {
			if (form.isModel) form = { id: form.id, name: form.get('name')}
			else {
				if (!Ext.isObject(form)) form = CFJS.safeParseInt(form, -1);
				if (Ext.isNumber(form)) form = { id: form };
			}
		}
		return form;
	},
	
	applyTemplateType: function(templateType) {
		if (templateType !== 'excel') templateType = 'pdf';
		return templateType;
	},
	
	createFieldsView: function() {
		var me = this,
			vm = me.lookupViewModel(),
			view = CFJS.apply({
				xtype	: 'document-grid-fields-picker',
				form	: me.getForm(),
				name	: 'fieldsView'
			}, me.fieldsViewConfig);
		view = Ext.create(view);
		return view;
	},
	
	createNavigatorWrap: function() {
		var me = this, wrap = Ext.apply(me.callParent(arguments), {
			layout: { type: 'accordion', titleCollapse: false }
		});
		// initialize fields grid
		if (!me.fieldsView || !me.fieldsView.isPanel) me.fieldsView = me.createFieldsView();
		wrap.items = Ext.Array.from(wrap.items);
		wrap.items.push(me.fieldsView);
		return wrap;
	},
	
	updateForm: function(form) {
		var me = this;
		if (me.fieldsView) me.fieldsView.setForm(form);
		me.setPathPrefix([me.getTemplateType(), (form||{}).id].join('/'));
	},
	
	updateTemplateType: function(templateType) {
		var me = this, iconCls = me[templateType + 'IconCls'], title = me[templateType + 'Title'];
		if (iconCls) me.setIconCls(iconCls);
		if (title) me.setTitle(title);
		me.setPathPrefix([templateType, me.getForm()].join('/'));
	}
	
});