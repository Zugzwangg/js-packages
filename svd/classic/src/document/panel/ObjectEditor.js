Ext.define('CFJS.document.panel.ObjectEditor', {
	extend				: 'CFJS.panel.BaseEditor',
	xtype				: 'document-panel-object-editor',
	requires			: [
		'Ext.toolbar.Spacer',
		'CFJS.form.field.*',
		'CFJS.document.util.Forms'
	],
	actions		: {
		exportRecord	: {
			iconCls	: CFJS.UI.iconCls.FILE_EXCEL,
			tooltip	: 'Export current record to Excel',
			handler	: 'onExportRecord'
		},
		printRecord		: {
			iconCls	: CFJS.UI.iconCls.PRINT,
			tooltip	: 'Print current record',
			handler	: 'onExportRecord'
		},
		showProperties	: {
			enableToggle: true,
			iconCls		: CFJS.UI.iconCls.COG,
			pressed		: false,
			tooltip		: 'Show the properties of the current record',
			handler		: 'onShowProperties'
		},
		signRecord		: {
			iconCls	: CFJS.UI.iconCls.SIGN,
			tooltip	: 'Sign the selected record',
			handler	: 'onSignRecord'
		}
	},
	actionsMap	: {
		header: { items: [ 'back', 'tbspacer', 'addRecord', 'removeRecord', 'signRecord', 'saveRecord', 'printRecord', 'exportRecord', 'showProperties', 'refreshRecord' ] }
	},
	bind				: { iconCls: '{form.iconCls}', formCfg: '{form}', readOnly: '{!editable}', title: '{_itemName_.form.name}' },
	config				: { formCfg: null },
	defaultBindProperty	: 'formCfg',
	header				: { items: new Array(10) },
	publishes			: [ 'formCfg' ],
	twoWayBindable		: [ 'formCfg' ],
	scrollable			: 'y',
	viewModel			: {	type: 'document.edit' },

	applyFormCfg: function(formCfg, oldFormCfg) {
		if (formCfg && !formCfg.isForm) {
			if (Ext.isObject(formCfg)) formCfg = formCfg.id;
			if (!Ext.isNumber(formCfg)) formCfg = CFJS.parseInt(formCfg);
			return formCfg && !isNaN(formCfg) ? formCfg : null; 
		}
		return formCfg;
	},

	initComponent: function() {
		var me = this, actions = me.getActions(), vm = me.lookupViewModel();
		me.callParent();
		if (actions && vm) {
			if (!me.isTable) me.on('validitychange', me.onValidityChange, me);
			vm.bind(['{allowed}', '{_itemName_.id}', '{editable}'], function(data) {
				var allowed = data[0] || {}, hasId = data[1] > 0;
				me.hideComponent(actions.addRecord, !(hasId && allowed.CREATE));
				me.hideComponent(actions.exportRecord, me.isTable || !(hasId && allowed.PRINT));
				me.hideComponent(actions.printRecord, me.isTable || !(hasId && allowed.PRINT));
				me.hideComponent(actions.removeRecord, !(allowed.DELETE && data[2]) && !CFJS.isAdministrator());
				me.hideComponent(actions.saveRecord, !allowed.WRITE || !data[2]);
				me.hideComponent(actions.showProperties, me.isTable || !(hasId && allowed.CHANGE_PERMISSION));
				me.hideComponent(actions.signRecord, me.isTable || !(hasId && allowed.SIGN));
				if (!me.isTable) me.onValidityChange(me.getForm(), me.isValid());
			});
		}
	},

	onValidityChange: function(form, valid) {
		var me = this, actions = me.getActions(), vm = me.lookupViewModel(),
			document = vm && vm.getItem(), allowed = vm && vm.get('allowed') || {};
		if (actions && document) {
			valid = valid && document.id > 0;
			me.disableComponent(actions.exportRecord, !(valid && allowed.PRINT));
			me.disableComponent(actions.printRecord, !(valid && allowed.PRINT));
			me.disableComponent(actions.showProperties, !(valid && allowed.CHANGE_PERMISSION));
			me.disableComponent(actions.signRecord, !(valid && allowed.SIGN && !document.hasSign()));
		}
	},
	
	renderForm: function(formCfg) {
		var me = this;
		Ext.suspendLayouts();
		me.removeAll();
		me.add(CFJS.document.util.Forms.renderFields(formCfg, me));
		Ext.resumeLayouts(true);
	},
	
	updateFormCfg: function(formCfg, oldFormCfg) {
		if (formCfg !== oldFormCfg) this.renderForm(formCfg);
	}

});