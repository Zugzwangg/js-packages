Ext.define('CFJS.document.panel.FormDesigner', {
	extend			: 'CFJS.panel.BaseDesigner',
	xtype			: 'document-panel-form-designer',
	requires		: [
		'Ext.form.FieldContainer',
		'Ext.form.FieldSet',
		'Ext.form.Panel',
		'Ext.form.RadioGroup',
		'CFJS.form.DateTimeField',
		'CFJS.document.grid.ObjectPanel',
		'CFJS.document.panel.FieldsExplorer',
		'CFJS.document.panel.FormEditor',
		'CFJS.document.panel.LayoutsExplorer',
		'CFJS.document.panel.ObjectEditor',
		'CFJS.document.view.FormDesignerModel'
	],
	formPreviewTitle: 'Form preview',
	formRefreshText	: 'Update form data',
	iconCls			: CFJS.UI.iconCls.FILE_CODE,
	refreshDataText	: 'reload data of form',
	title			: 'Form editor',
	
	afterRecordSave: function(component, force, record) {
		this.ownerCt.formId = this.actionable.tabSelector(record.getId());
	},

	getFieldsExplorer: function(config) {
		return Ext.apply({}, config, { xtype: 'document-panel-fields-explorer', viewModel: this.lookupViewModel() });
	},

	getFormExplorer: function(config) {
		return Ext.apply({}, config, { xtype: 'document-panel-form-editor', viewModel: this.lookupViewModel() });
	},
	
	getLayoutsExplorer: function(config) {
		return Ext.apply({}, config, { xtype: 'document-panel-layouts-explorer', viewModel: this.lookupViewModel() });
	},
	
	initActionable: function(actionable, valid) {
		var actions = actionable && actionable.actions;
		if (actions) {
			if (!Ext.isBoolean(valid)) valid = this.isValid();
			actionable.disableComponent(actions.filtersPanel, true);
			actionable.disableComponent(actions.removeRecord);
			actionable.disableComponent(actions.refreshRecord);
			actionable.disableComponent(actions.exportRecord, !valid);
			actionable.disableComponent(actions.saveRecord, !valid);
		}
	},
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		if (!me.explorer) {
			me.formExplorer = Ext.create(me.getFormExplorer(me.formConfig));
			me.fieldsExplorer = Ext.create(me.getFieldsExplorer(me.fieldsConfig));
			me.layoutsExplorer = Ext.create(me.getLayoutsExplorer(me.layoutsConfig))
			me.explorer = Ext.create(Ext.apply({
				xtype	: 'panel',
				bind	: { title: '"{_itemName_.name}"' },
				header	: false,
				layout	: 'accordion',
				region	: 'west',
				minWidth: 300,
				maxWidth: 600,
				width	: 350,
				items	: [ me.formExplorer, me.fieldsExplorer, me.layoutsExplorer ]
			}, me.explorerConfig));
		}
		if (!me.objectEditor) {
			me.objectEditor = Ext.create({
				xtype		: 'document-panel-object-editor',
				bind		: '{_itemName_}',
				iconCls		: CFJS.UI.iconCls.EDIT, 
				title		: 'Editor',
				readOnly	: true,
				viewModel	: null
			});
		}
		if (!me.objectGrid) {
			me.objectGrid = Ext.create({
				xtype	: 'document-grid-object-panel',
				bind	: { form: '{_itemName_}' },
				iconCls	: CFJS.UI.iconCls.TABLE,
				title	: 'Grid'
			});
		}
		me.items = [ me.explorer, {
			xtype				: 'tabpanel',
			bodyPadding			: 10,
			collapsible			: false,
			plain				: true,
			region				: 'center',
			items				: [ me.objectEditor, me.objectGrid ],
			listeners			: { tabchange: me.onRenderForm, scope: me },
			name				: 'formPreview',
			tabBarHeaderPosition: 1,
			title				: me.formPreviewTitle,
			tools				: [{
				type	: 'refresh',
				handler	: me.onRenderForm,
				scope	: me,
				tooltip	: me.formRefreshText
			}]
		}];
		me.callParent();
		me.on({ beforesaverecord: vm.beforeSaveForm, scope: vm })
		me.formExplorer.on({ validitychange: me.onValidityChange, scope: me });
	},
	
	isValid: function() {
		return this.formExplorer.isValid();
	},

	onRenderForm: function() {
		var me = this, vm = me.lookupViewModel();
			formPreview = me.down('tabpanel[name="formPreview"]'),
			panel = formPreview && formPreview.getActiveTab();
		if (panel && Ext.isFunction(panel.renderForm)) {
			panel.renderForm.call(panel, vm.updateFormData());
		}
	}
	
});