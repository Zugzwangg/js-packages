Ext.define('CFJS.document.panel.DocumentProperties', {
	extend			: 'CFJS.document.panel.ObjectProperties',
	xtype			: 'document-panel-properties',
	contractorLabel	: 'Contractor',
	phaseLabel		: 'State',
	
	createDescriptionTab: function(config) {
		var me = this,
			tab = me.callParent([config]),
			items = tab.items;
		items[2].items.push({
			bind		: '{phase}',
			fieldLabel	: me.phaseLabel,
			renderer	: Ext.util.Format.enumRenderer('lifePhaseTypes'),
			name		: 'phase'
		});
		items.splice(3, 0, {
			items: [{
				xtype			: 'contractorpicker',
				bind			: '{_itemName_.contractor}',
				fieldLabel		: me.contractorLabel,
				hideClearTrigger: false,
				margin			: 0,
				name			: 'contractor'
			}],
			margin: '0 0 10 0'
		});
		return tab;
	}
});