Ext.define('CFJS.document.view.DocumentView', {
	extend		: 'CFJS.document.view.ObjectsView',
	xtype		: 'document-view-document-view',
	requires	: [
		'CFJS.document.grid.DocumentPanel',
		'CFJS.document.panel.DocumentEditor',
		'CFJS.document.panel.DocumentProperties',
		'CFJS.document.view.DocumentView'
	],
	controller	: { type: 'document.view', editor: 'document-panel-editor' },
	
	createPropEditor: function(config) {
		return this.callParent([CFJS.apply(config, { xtype: 'document-panel-properties' })]);
	},

	createGrid: function(config) {
		return this.callParent([CFJS.apply(config, { xtype: 'document-grid-document-panel' })]);
	}

});
