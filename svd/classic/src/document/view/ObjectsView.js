Ext.define('CFJS.document.view.ObjectsView', {
	extend				: 'Ext.container.Container',
	xtype				: 'document-view-objects-view',
	requires			: [
		'CFJS.document.grid.ObjectPanel',
		'CFJS.document.panel.ObjectEditor',
		'CFJS.document.panel.ObjectProperties',
		'CFJS.document.view.EditController',
		'CFJS.form.field.DictionaryTreePicker',
		'CFJS.model.document.Document',
		'CFJS.store.Buffered',
	],
	config				: {
		actionable	: null,
		excelMenu	: null,
		gridConfig	: null,
		isTable		: false,
		pdfMenu		: null,
		readOnly	: false
	},
	controller			: { type: 'document.view', editor: 'document-panel-object-editor' },
	defaultBindProperty	: 'form',
	layout				: 'border',
	session				: { schema: 'document' },
	viewModel			: { type: 'document.view' },

	applyExcelMenu: function(menu) {
		return this.applyMenu(menu);
	},
	
	applyMenu: function(menu) {
		if (Ext.isArray(menu)) {
			if (menu.length === 1) menu = menu[0].template;
			else if (menu.length > 1) menu = { items: menu };
			else menu = null;
		}
		return menu;
	},
	
	applyPdfMenu: function(menu) {
		return this.applyMenu(menu);
	},

	applyReadOnly: function(readOnly) {
		return !!readOnly;
	},
	
	bodyWrap: function() {
		var me = this;
		if (!me.wrapper || !me.wrapper.isContainer) {
			me.wrapper = me.add({ xtype: 'container', layout: 'card', region: 'center' });
		}
		return me.wrapper;
	},
	
	createFilterPanel: function(config) {
    	var me = this, vm = me.lookupViewModel(),
    		filterPanel = CFJS.apply({
    			xtype			:'form',
    			bodyPadding 	: '0 10',
    			collapsed		: true,
    			collapsible		: true,
				defaultFocus	: 'dictionarytreepicker:focusable:not([hidden]):not([disabled]):not([readOnly])',
				fieldDefaults	: {
					labelAlign		: 'top',
					msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
					selectOnFocus	: true
				},
				floatable		: false,
				iconCls			: CFJS.UI.iconCls.FILTER,
    			items			: [{
    				xtype		: 'dictionarytreepicker',
    				bind		: { store: '{sectors}' },
    				editable	: true,
					emptyText	: 'select a sector',
					fieldLabel	: 'Sector',
					filterName	: 'sector',
					listeners	: { change: 'onChangeLoaderFilter' },
					name		: 'sectorFilter'
    			},{
    				xtype		: 'dictionarytreepicker',
    				bind		: { store: '{units}' },
					emptyText	: 'select a unit',
					fieldLabel	: 'Unit',
					filterName	: 'unit',
					labelAlign	: 'top',
					listeners	: { change: 'onChangeLoaderFilter' },
					name		: 'unitFilter',
					rootVisible	: true,
					value		: vm.get('userUnit')
    			}],
    			layout			: { type: 'vbox', align: 'stretch' },
    			region			: 'west',
    			scrollable		: 'y',
    			split			: true,
    			title			: 'Additional filters',
    			width			: 200
			}, config);
    	return filterPanel;
	},

	createGrid: function(config) {
		var me = this,
			grid = CFJS.apply({
				xtype		: 'document-grid-object-panel',
				bind		: { form: '{form}', store: '{_storeName_}', title: '{form.name}' },
				collapsible	: false,
				listeners	: { itemdblclick: 'onGridDblClick' },
				region		: 'center',
				style		: Ext.theme.name === 'Crisp' ? { border: '1px solid silver;' } : null
			}, config);
		if (me.isTable) grid = Ext.apply(grid, {
			actions	: { clearFilters: false, periodPicker: false },
			header	: { items: new Array(7) },
			margin	: 5,
			style	: null,
			userCls	: 'shadow-panel'
		});
		return grid;
	},

	createPropEditor: function(config) {
    	var me = this,
    		cnt = me.lookupController(),
    		propEditor = CFJS.apply({
				xtype			: 'document-panel-object-properties',
				collapsible		: true,
				floatable		: false,
				hidden			: true,
				readOnly		: me.getReadOnly(),
				region			: 'east',
				split			: true,
				width			: 630
			}, config);
    	propEditor = me.add(propEditor);
    	propEditor.setRecord(cnt ? cnt.lookupCurrentItem() : me.lookupGrid().getLastSelected());
    	return propEditor;
    },

	doSelectionChange: function(selectable, selection, actions) {
		selection = Ext.Array.from(selection);
		var propEditor = this.propEditor, document = selection.length > 0 && selection[0];
		if (propEditor) propEditor.setRecord(document);
	},
	
	findAction: function() {
		var actionable = this.getActionable();
		return actionable ? actionable.findAction.apply(actionable, arguments) : null;
	},

	initComponent: function() {
		var me = this;
		if (me.isTable) me.addCls('panel-background');
		me.callParent();
	},

	initItems: function() {
		var me = this, parent = me.lookupViewModel(true), wrapper;
		if (me.items && !me.items.isMixedCollection) me.items = null;
		me.callParent();
		if (!(wrapper = me.wrapGrid) || !wrapper.isContainer) {
			me.wrapGrid = wrapper = me.bodyAdd({ xtype: 'container', layout: me.isTable ? 'fit' : 'border' });
		}
		if (!me.isTable && (!me.filterPanel || !me.filterPanel.isPanel)) {
			me.filterPanel = wrapper.add(me.ensureChild('filterPanel'));
		}
		if (!me.grid || !me.grid.isPanel) {
			me.grid = wrapper.add(me.ensureChild('grid'));
			me.grid.setReadOnly(me.getReadOnly());
			me.actionable = me.actionable || me.grid;
			if (me.isTable && parent && parent.isEditorModel) {
				parent.bind('{_itemName_.id}', function(id) {
					me.setReadOnly(!me.grid.editable || id <= 0);
				});
			}
		}
	},

	lookupGrid: function() {
		return this.grid;
	},

	setActiveItem: function(item) {
		var me = this, propEditor = me.propEditor, sp;
		if (item === me.grid) item = me.wrapGrid;
		sp = ((item||{}).actions || (me.actionable||{}).actions).showProperties;
		if (sp && sp.isAction) sp.setPressed(propEditor && propEditor.isVisible());
		return me.bodyWrap().setActiveItem(item);
	},
	
	bodyAdd: function(newItems) {
		return this.bodyWrap().add(newItems);
	},
	
	setToken: function(token) {
		var cnt = this.lookupController();
		if (cnt && Ext.isFunction(cnt.setToken)) cnt.setToken(token);
	},
	
	showProperties: function(visible) {
		var me = this, propEditor = me.ensureChild('propEditor');
		if (propEditor) propEditor.setVisible(visible);
	},

	token: function(record) {
		var cnt = this.lookupController();
		return cnt && Ext.isFunction(cnt.token) ? cnt.token(record || record === false? record : cnt.lookupRecord()) : null;
	},

	updateReadOnly: function(readOnly) {
		var me = this;
		if (me.grid) me.grid.setReadOnly(readOnly);
		if (me.propEditor) me.propEditor.setReadOnly(readOnly);
	},
	
	privates: {

		applyViewModel: function(vm) {
			if (vm && !vm.isViewModel) {
				var me = this,
					parent = me.lookupViewModel(true),
					loaderCfg = { formId: '{form.id}' },
					storeCfg = {
						type	: 'basebuffered',
						model	: 'CFJS.model.document.Document',
						session	: { schema: 'document' },
						proxy	: { type: 'json.svdapi', loaderConfig: loaderCfg, service: 'svd' }
					}, 
					storeName = Ext.id(storeCfg, vm.storeName || 'documents'),
					cfg = { storeName: storeName, stores: vm.stores || {} };
				if (parent && parent.isEditorModel) Ext.apply(loaderCfg, { dictionaryType: 'table', parentId: '{'+ parent.getItemName() + '.id}' });
				else Ext.apply(loaderCfg, { period: '{workingPeriod}', userInfo: '{userInfo}', unit: parent.get('userUnit') });
				cfg.stores[storeName] = storeCfg;
				Ext.apply(vm, cfg);
			}
			return this.callParent([vm]);
		}
	
	}

});