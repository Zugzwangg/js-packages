Ext.define('CFJS.pages.DocumentsBrowser', {
	extend			: 'CFJS.pages.ObjectBrowser',
	xtype			: 'document-page-documents-browser',
	requires		: [ 'CFJS.document.view.DocumentView' ],
	actions			: {
		addRecord		: {
			title	: 'Add document',
			tooltip	: 'Add new document'
		},
		copyRecord	: {
			title	: 'Copy the document',
			tooltip	: 'Copy the selected document'
		},
		editRecord		: {
			title	: 'Edit the document',
			tooltip	: 'Edit the selected document'
		},
		exportRecord	: {
			title	: 'Export to Excel',
			tooltip	: 'Export the selected documents to Excel'
		},
		printRecord		: {
			title	: 'Print the document',
			tooltip	: 'Print the selected documents'
		},
		removeRecord	: {
			title	: 'Delete documents',
			tooltip	: 'Remove the selected documents',
		},
		shareDocuments	: {
			iconCls	: CFJS.UI.iconCls.SHARE,
			title	: 'Share documents',
			tooltip	: 'Share the selected documents'
		},
		signRecord		: {
			title	: 'Sign the document',
			tooltip	: 'Sign the selected document'
		}
	},
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'editRecord', 'signRecord', 'menuseparator', 'printRecord', 'exportRecord', 'showProperties', 'shareDocuments' ] },
		toolBar		: { items: [ 'periodPicker', 'tbseparator', 'addRecord', 'copyRecord', 'removeRecord', 'editRecord', 'signRecord', 'tbseparator', 
								'printRecord', 'exportRecord', 'showProperties', 'shareDocuments', 'tbseparator',  'clearFilters', 'reloadStore', 'tbseparator' ] }
	},
	contextMenu		: { items: new Array(10) },
	formGroupAllText: 'All items',
	iconCls			: 'x-fa fa-leanpub',
	formType		: 1,
	tabDefaults		: { xtype: 'document-view-document-view' },
	title			: 'Documents',
	toolBar			: { items: new Array(16) },
	treeFilterTitle	: 'Document Groups',
	shareAsText		: 'Share as {0}-file',
	
	createBrowserView: function(cfg) {
		var me = this;
		return Ext.apply(me.callParent(arguments), { collapsible: false, region: 'center' });
	},
	
	initComponent: function() {
		var me = this, action = (me.getActions()||{}).shareDocuments;
		if (action && action.isAction) {
			action.initialConfig.menu = CFJS.apply({
				items: [{
					iconCls 	: CFJS.UI.iconCls.FILE_CODE,
					scope		: action.scope,
					title		: Ext.String.format(me.shareAsText, 'XML'),
					type		: 'xml',
					handler 	: 'onActionClick',
					handlerFn	: 'onShareDocuments'
				},{
					iconCls 	: CFJS.UI.iconCls.FILE_PDF,
					scope		: action.scope,
					title		: Ext.String.format(me.shareAsText, 'PDF'),
					type		: 'pdf',
					handler 	: 'onActionClick',
					handlerFn	: 'onShareDocuments'
				}]
			}, me.shareDocumentsMenu);
			action.each(function(component) {
				if (Ext.isFunction(component.setMenu)) component.setMenu(action.initialConfig.menu, true);
			});
		}
		if (!me.browserWrap) {
			me.browserWrap = Ext.create({
				xtype		: 'panel',
				bodyPadding	: 0,
				isHome		: true,
				layout		: 'border',
				reorderable	: false,
				tabConfig	: { hidden: true }
			});
		}
		if (!me.treeFilter) {
			me.treeFilter = me.browserWrap.add({
				xtype		: 'treepanel',
				collapsible	: true,
				columns		: [{ xtype: 'treecolumn', align: 'left', dataIndex: 'name', flex: 1 }],
				floatable	: false,
				iconCls		: CFJS.UI.iconCls.FILES,
				lines		: true,
				listeners	: { scope: me, selectionchange: me.changeFormGroupFilter },
				minWidth	: 200,
				region		:'west',
				split		: true,
				store		: Ext.Factory.store({ type: 'formgroups', pageSize: 0, root: { name: me.formGroupAllText } }),
				title		: me.treeFilterTitle,
				width		: 250
			});
			me.treeFilter.isAutoTree = true,
			me.treeFilter.addCls(me.treeFilter.autoWidthCls);
		}
		me.callParent();
		Ext.suspendLayouts();
		me.browserWrap.add(me.browserView);
		me.add(me.browserWrap);
		Ext.resumeLayouts(true);
	},

	changeFormGroupFilter: function(selectable, record) {
		record = (record = Ext.Array.from(record)).length > 0 ? record[0] : null;
		this.applyBrowserStoreFilter({
			id		: 'group',
			type	: 'numeric',
			property: 'group.id',
			operator: 'eq',
			value	: record && !record.isRoot() ? record.id : undefined
		});
	}
	
});