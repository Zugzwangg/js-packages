Ext.define('CFJS.pages.DictionariesBrowser', {
	extend	: 'CFJS.pages.ObjectBrowser',
	xtype	: 'document-page-dictionaries-browser',
	iconCls	: CFJS.UI.iconCls.BOOK,
	formType: 0,
	title	: 'Dictionaries',

});