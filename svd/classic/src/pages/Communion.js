Ext.define('CFJS.pages.Communion', {
	extend				: 'Ext.tab.Panel',
	xtype				: 'page-communion',
	requires			: [
		'Ext.form.field.Text',
		'CFJS.communion.view.*'
	],
	actions				: {
		addTask				: {
			iconCls		: 'x-fa fa-calendar-plus-o',
			title		: 'Create new task',
			tooltip		: 'Create new task',
			handlerFn	: 'onAddTask'
		},
		composeDiscussion	: {
			iconCls		: CFJS.UI.iconCls.EDIT,
			title		: 'Compose new discussion',
			tooltip		: 'Compose new discussion',
			handlerFn	: 'onComposeDiscussion'
		},
		composeMessage		: {
			iconCls		: CFJS.UI.iconCls.EDIT,
			title		: 'Compose new message',
			tooltip		: 'Compose new message',
			handlerFn	: 'onComposeMessage'
		},
		periodPicker: {
			iconCls	: CFJS.UI.iconCls.CALENDAR,
			title	: 'Working period',
			tooltip	: 'Set the working period',
			handler	: 'onPeriodPicker'
		},
		reloadStore			: {
			iconCls	: CFJS.UI.iconCls.REFRESH,
			title	: 'Update the data',
			tooltip	: 'Update the data',
			handler	: 'onReloadStore'
		}
	},
	actionsMap			: {
		toolBar		: { items: [ 'periodPicker', 'tbseparator', 'composeDiscussion', 'composeMessage', 'addTask', 'reloadStore' ] }
	},
	contextMenu			: false,
	defaultListenerScope: true,
	filterConfig		: true,
	header				: { padding: '0 10' },
	iconCls				: 'x-fa fa-inbox',
	itemId				: 'communionPage',
	plain				: true,
	searchText			: 'Search',
	tabBarHeaderPosition: 2,
	title				: 'Communion',
	toolBar				: {
		xtype	: 'toolbar',
		cls		: [ 'toolbar-btn-header', CFJS.UI.defHeaderCls ].join(' '),
		items	: new Array(6),
		padding	: '0 5 0 10'
	},
	ui					: 'navigation',

	afterRender: function() {
		var me = this;
		if (me.header && me.header.isHeader) {
			if (me.toolBar) me.header.add(me.toolBar);
			if (me.filterConfig) me.header.add(Ext.apply({
				xtype		: 'textfield',
				emptyText	: me.searchText,
				listeners	: { change: 'onFilterFieldChange', buffer: 300, scope	: me },
				name		: 'searchFilter',
				triggers	: {
					clear	: {
						cls		: Ext.baseCSSPrefix + 'form-clear-trigger',
						handler	: 'onFilterClearTriggerClick',
						hidden	: true
					},
					search	: {
						cls		: Ext.baseCSSPrefix + 'form-search-trigger',
						weight	: 1,
						handler	: 'onFilterSearchTriggerClick'
					}
				},
				width		: 250
			}, me.filterConfig));
		}
		me.callParent(arguments);
		me.onTabChange(me, me.getActiveTab());
	},
	
	createDisscusionView: function(config) {
		var me = this,
			disscusionView = CFJS.apply({
				xtype			: 'communion-view-discussion',
				actionsVisible	: [ 'periodPicker', 'composeDiscussion', 'reloadStore' ],
				style			: Ext.theme.name === 'Crisp' ? { border: '1px solid silver;' } : null
			}, config);
		return Ext.create(disscusionView);
	},
	
	createMessageView: function(config) {
		var me = this,
			messageView = CFJS.apply({
				xtype			: 'communion-view-messages',
				actionsVisible	: [ 'periodPicker', 'composeMessage', 'reloadStore' ],
				style			: Ext.theme.name === 'Crisp' ? { border: '1px solid silver;' } : null
			}, config);
		return Ext.create(messageView);
	},
	
	initComponent: function() {
		var me = this;
		if (me.toolBar && !me.toolBar.isToolBar) me.toolBar = Ext.create(me.toolBar);
//		if (!me.taskView) {
//			me.taskView = Ext.create(CFJS.apply({
//				xtype			: 'communion-view-tasks',
//				actionsVisible	: [ 'periodPicker', 'addTask', 'reloadStore' ],
//				style			: Ext.theme.name === 'Crisp' ? { border: '1px solid silver;' } : null
//			}, me.taskConfig));
//		}
		me.items = [ me.ensureChild('disscusionView'), me.ensureChild('messageView') ];
		if (me.plain) me.getTabBar().setMargin(0);
		me.callParent();
		me.on({ tabchange: me.onTabChange, scope: me });
	},
	
	lookupActiveGrid: function() {
		var tab = this.getActiveTab();
		return tab && tab.lookupGrid();
	},
	
	lookupActiveStore: function() {
		var grid = this.lookupActiveGrid();
		return grid && grid.getStore();
	},

	onFilterClearTriggerClick: function() {
		this.searchFilter().setValue();
	},
	
	onFilterFieldChange: function(field, value) {
		var tab = this.getActiveTab();
		field.getTrigger('clear').setVisible(value && value.length > 0);
		if (tab && Ext.isFunction(tab.setSearchFilter)) tab.setSearchFilter(value);
	},
	
	onFilterSearchTriggerClick: function() {
		var me = this, searchFilter = me.searchFilter();
		me.onFilterFieldChange(searchFilter, searchFilter.getValue());
	},

	onPeriodPicker: function(component) {
		var grid = this.lookupActiveGrid();
		if (grid && grid.hasPeriodPicker) grid.showPeriodPicker(component);
	},
	
	onReloadStore: function(component) {
		var store = this.lookupActiveStore();
		if (store) store.reload();
	},
	
	onTabChange: function(tabPanel, newCard, oldCard) {
		var me = this, searchFilter = me.searchFilter(),
			actionsVisible = Ext.Array.from(newCard.actionsVisible, true);
		Ext.Object.eachValue(me.actions, function(action) {
			var index = Ext.Array.indexOf(actionsVisible, action.itemId),
				hidden = index === -1;
			if (!hidden) actionsVisible.splice(index, 1);
			action.setHidden(hidden);
		});
		if (searchFilter) {
			if (oldCard) oldCard.searchFilterCash = searchFilter.getValue();
			searchFilter.suspendCheckChange++;
			searchFilter.setValue(newCard.searchFilterCash);
			searchFilter.suspendCheckChange--;
		}
	},
	
	privates: {
		
		searchFilter: function() {
			return this.down('textfield[name=searchFilter]');
		}

	}
	
});