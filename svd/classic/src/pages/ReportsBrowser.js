Ext.define('CFJS.pages.ReportsBrowser', {
	extend	: 'CFJS.pages.ObjectBrowser',
	xtype	: 'document-page-reports-browser',
	iconCls	: 'x-fa fa-line-chart',
	formType: 2,
	title	: 'Reports'
});