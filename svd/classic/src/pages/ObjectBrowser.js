Ext.define('CFJS.pages.ObjectBrowser', {
	extend			: 'CFJS.panel.HistoryBrowser',
	xtype			: 'document-page-object-browser',
	requires		: [
		'CFJS.document.util.Forms',
		'CFJS.document.view.ObjectsView',
		'CFJS.document.window.ObjectWindow',
		'CFJS.model.document.Form',
		'CFJS.store.Named',
		'CFJS.store.document.FormGroups'
	],
	config			: {
		actions			: {
			addRecord		: {
				iconCls		: CFJS.UI.iconCls.ADD,
				title		: 'Add a record',
				tooltip		: 'Add new record',
				handlerFn	: 'onAddRecord'
			},
			clearFilters	: {
				iconCls		: CFJS.UI.iconCls.CLEAR,
				title		: 'Clear filters',
				tooltip		: 'Clear all filters',
				handlerFn	: 'onClearFilters'
			},
			copyRecord		: {
				iconCls		: CFJS.UI.iconCls.COPY,
				title		: 'Copy record',
				tooltip		: 'Copy the selected record',
				handlerFn	: 'onCopyRecord'
			},
			editRecord		: {
				iconCls		: CFJS.UI.iconCls.EDIT,
				title		: 'Edit the record',
				tooltip		: 'Edit the current record',
				handlerFn	: 'onEditRecord'
			},
			exportRecord	: {
				iconCls		: CFJS.UI.iconCls.FILE_EXCEL,
				title		: 'Export to Excel',
				tooltip		: 'Export the current record to Excel',
				handlerFn	: 'onExportRecord'
			},
			periodPicker	: {
				iconCls		: CFJS.UI.iconCls.CALENDAR,
				title		: 'Working period',
				tooltip		: 'Set the working period',
				handlerFn	: 'onPeriodPicker'
			},
			printRecord		: {
				iconCls		: CFJS.UI.iconCls.PRINT,
				title		: 'Print the record',
				tooltip		: 'Print the current record',
				handlerFn	: 'onExportRecord'
			},
			reloadStore		: {
				iconCls		: CFJS.UI.iconCls.REFRESH,
				title		: 'Update the data',
				tooltip		: 'Update the data',
				handlerFn	: 'onReloadStore'
			},
			removeRecord	: {
				iconCls		: CFJS.UI.iconCls.DELETE,
				title		: 'Delete records',
				tooltip		: 'Remove selected records',
				handlerFn	: 'onRemoveRecord'
			},
			showProperties	: {
				enableToggle: true,
				iconCls		: CFJS.UI.iconCls.COG,
				pressed		: false,
				title		: 'Show the properties',
				tooltip		: 'Show the properties of the current record',
				handlerFn	: 'onShowProperties'
			},
			signRecord		: {
				iconCls		: CFJS.UI.iconCls.SIGN,
				title		: 'Sign the record',
				tooltip		: 'Sign the current record',
				handlerFn	: 'onSignRecord'
			}
		},
		actionsDisable	: [ 'copyRecord', 'removeRecord', 'editRecord', 'signRecord'],
		actionsMap		: {
			contextMenu	: { items: [ 'addRecord', 'copyRecord', 'removeRecord', 'editRecord', 'signRecord', 'menuseparator', 'printRecord', 'exportRecord', 'showProperties' ] },
			toolBar		: { items: [ 'periodPicker', 'tbseparator', 'addRecord', 'copyRecord', 'removeRecord', 'editRecord', 'signRecord', 'tbseparator',
									'printRecord', 'exportRecord', 'showProperties', 'tbseparator', 'clearFilters', 'reloadStore', 'tbseparator' ] }
		},
		contextMenu		: { items: new Array(9) },
		formType		: undefined
	},
	plain			: true,
	routeSeparator	: CFJS.document.util.Forms.routeSeparator,
	session			: { schema: 'document' },
	tabIdPrefix		: CFJS.document.util.Forms.formTabPrefix,
	tabIdSeparator	: CFJS.document.util.Forms.tokenSeparator,
	tabDefaults		: { xtype: 'document-view-objects-view' },
	toolBar			: { items: new Array(15) },
	
	applyFormType: function(formType) {
		if (Ext.isNumber(formType)) {
			var types = CFJS.document.util.Forms.formTypes;
			formType = types[formType < types.length ? formType : types.length - 1].id
		}
		return Ext.isString(formType) ? formType.toLowerCase() : formType;
	},
	
	initComponent: function() {
		var me = this;
		me.mainTitle = me.title;
		me.callParent();
	},

	createChildTab: function(record) {
		var me = this, tab = me.callParent(arguments);
		if (tab) {
			tab.controller = Ext.apply(tab.controller || {}, {
				type	: 'document.' + (me.readOnly ? 'view' : 'edit'),
				formType: me.formType
			});
			tab = me.add(Ext.apply(tab, {
				gridConfig	: { header: false },
				viewModel	: { form: record }
			}));
			me.relayEvents(tab.lookupGrid(), [ 'itemcontextmenu' ]);
		}
		return tab;
	},
	
	createBrowserView: function(cfg) {
		var me = this, type = me.formType,
			store = Ext.Factory.store({
				type	: 'named',
				autoLoad: true,
				model	: 'Form',
				pageSize: 50,
				proxy	: { loaderConfig: { dictionaryType: 'form' }, service: 'svd' },
				filters	: [
					{ id: 'type',			type: 'string',		property: 'type',					operator: 'eq',		value: type },
					{ id: 'permissions',	type: 'numeric',	property: 'permissions[\'READ\']',	operator: '!lt',	value: me.lookupViewModel().get('permissionLevel') }
				],
				sorters	: [ 'name' ]
			});
		return Ext.apply(cfg, {
			bbar		: [{ xtype: 'pagingtoolbar', displayInfo: true, store: store }],
			iconCls		: CFJS.UI.iconCls.HOME,
			reorderable	: false,
			store		: store,
			token		: Ext.emptyFn,
			viewConfig	: { itemIconCls: CFJS.document.util.Forms.iconCls(type) }
		})
	},
	
	onEditingChange: function(card, editing, record) {
		var me = this;
		me.callParent(arguments);
		if (!me.historyChange) me.redirectTo(me.cardRoute(card, record));
	},
	
	onFilterFieldChange: function(field, value) {
		var me = this, view = (me.getActiveTab()||{}).grid || me.browserView,
			store = view && view.getStore(), proxy = store && store.getProxy();
		if (value && value.length > 0) {
			field.getTrigger('clear').show();
			if (proxy && proxy.isSvdProxy) {
				proxy.setQuery(value);
				store.reload();
			} else store.getFilters().add(me.createSearchFilter(value));
		} else {
			if (proxy && proxy.isSvdProxy) {
				proxy.setQuery(null);
				store.reload();
			} else store.removeFilter('search');
			field.getTrigger('clear').hide();
        }
	},
	
	onTabChange: function(tabPanel, newCard, oldCard) {
		var me = this, titles = [me.mainTitle],
			searchFilter = me.searchFilter();
		if (searchFilter) {
			if (oldCard) oldCard.searchFilterCash = searchFilter.getValue();
			searchFilter.suspendCheckChange++;
			searchFilter.setValue(newCard.searchFilterCash);
			searchFilter.suspendCheckChange--;
		}
		if (!newCard.isHome) titles.push(newCard.title);
		me.setTitle(titles.join(' / '));
		me.callParent(arguments);
	},
	
	privates: {
		
		buildRouteToken: function(card, form, record) {
			var me = this, route = me.routeId, token;
			if (form && form.id) {
				route = me.tabHashToken(form.id);
				if (!Ext.isEmpty(token = card.token(record))) route = [route, token].join(me.routeSeparator);
			}
			return route;
		},
		
		cardRoute: function(card, record) {
			var me = this, vm = card && card.lookupViewModel(),
				form = vm && Ext.isFunction(vm.getForm) && vm.getForm();
			return me.buildRouteToken(card, form, record);
		}
	
	}

});