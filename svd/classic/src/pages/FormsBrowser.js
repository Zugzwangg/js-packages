Ext.define('CFJS.pages.FormsBrowser', {
	extend			: 'CFJS.panel.HistoryBrowser',
	xtype			: 'document-page-forms-browser',
	requires 		: [
		'CFJS.form.field.DictionaryTreePicker',
		'CFJS.document.panel.FormDesigner',
		'CFJS.document.panel.TemplateEditor',
		'CFJS.document.view.FormDesignerModel',
		'CFJS.document.view.FormsBrowserModel'
	],
	actions			: {
		addRecord		: {
			iconCls	: CFJS.UI.iconCls.ADD,
			title	: 'Add a form',
			tooltip	: 'Add new form',
			handler	: 'onAddRecord'
		},
		exportRecord	: {
			iconCls		: CFJS.UI.iconCls.DOWNLOAD,
			title		: 'Export a form',
			tooltip		: 'Export current form',
			handlerFn	: 'onExportRecord'
		},
		filtersPanel	: {
			enableToggle	: true,
			iconCls			: CFJS.UI.iconCls.FILTER,
			title			: 'Show filters',
			tooltip			: 'Show current filters',
			toggleHandler	: 'onFilterFormsToggle'
		},
		removeRecord	: {
			iconCls	: CFJS.UI.iconCls.DELETE,
			title	: 'Remove a form',
			tooltip	: 'Remove current form',
			handler	: 'onRemoveRecord'
		},
		refreshRecord	: {
			iconCls		: CFJS.UI.iconCls.REFRESH,
			title		: 'Update the data',
			tooltip		: 'Update the data',
			handlerFn	: 'onRefreshRecord'
		},
		saveRecord	: {
			iconCls		: CFJS.UI.iconCls.SAVE,
			title		: 'Save a form',
			tooltip		: 'Save current form',
			handlerFn	: 'onSaveRecord'
		}
	},
	actionsDisable	: [ 'removeRecord', 'saveRecord', 'refreshRecord', 'exportRecord' ],
	actionsMap		: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord', 'menuseparator', 'exportRecord' ] },
		toolBar		: { items: [ 'filtersPanel', 'tbseparator', 'addRecord', 'removeRecord', 'tbseparator', 'saveRecord', 'refreshRecord', 'tbseparator', 'exportRecord' ] }
	},
	closeTabText	: 'close tab',
	contextMenu		: { items: new Array(4) },
	formSeed		: 0,
	iconCls			: "x-fa fa-list-alt",
	overflowHandler	: 'scroller',
	plain			: true,
	routeSeparator	: CFJS.document.util.Forms.routeSeparator,
	session			: { schema: 'document' },
	tabIdPrefix		: CFJS.document.util.Forms.formTabPrefix,
	tabIdSeparator	: CFJS.document.util.Forms.tokenSeparator,
	tabPosition		: 'bottom',
	toolBar			: { hidden: false, items: new Array(9) },
	title			: 'Form designer',
	viewModel		: { type: 'document.forms-browser' },
	
	beforeTabClose: function(tab) {
		var me = this, designer = tab && tab.formDesigner;
		if (designer && designer.isDirtyRecord()) {
			Ext.fireEvent('confirmlostdata', me.closeTabText, function(btn) { if (btn === 'yes') me.remove(tab); }, me);
			return false;
		}
	},
	
	confirmRemove: function(formName, fn) {
		return Ext.fireEvent('confirmlostdata', 'delete a form "'+ formName + '"', fn, this);
	},
	
	createChildTab: function(record) {
		var me = this, tab = me.callParent(arguments);
		if (tab) {
			tab = me.add(Ext.apply(tab, {
				xtype		: 'tabpanel',
				formId		: me.tabSelector(record.id),
				editing		: true,
				items		: [{
					xtype		: 'document-panel-form-designer',
					actionable	: me,
					name		: 'formDesigner'
				},{
					xtype		: 'document-panel-template-editor',
					actionable	: me,
					form		: record,
					hidden		: record.get('type') === 'TABLE',
					name		: 'templateDesignerPDF',
					templateType: 'pdf'
				},{
					xtype		: 'document-panel-template-editor',
					actionable	: me,
					form		: record,
					hidden		: record.get('type') === 'TABLE',
					name		: 'templateDesignerXLS',
					templateType: 'excel'
				}],
				plain		: true,
				viewModel	: { type: 'document.form-designer', form: record }
			}));
			tab.formDesigner = tab.child('[name="formDesigner"]');
			tab.pdfEditor = tab.child('[templateType="pdf"]');
			tab.excelEditor = tab.child('[templateType="excel"]');
			tab.on({ beforeclose: me.beforeTabClose, tabchange: me.onChildTabChange, scope: me });
			tab.lookupViewModel().bind({ id: '{_itemName_.id}', type: '{_itemName_.type}'}, function(form) {
				me.hideComponent((tab.pdfEditor||{}).tab, form.type === 'TABLE' || form.id <= 0);
				me.hideComponent((tab.excelEditor||{}).tab, form.type === 'TABLE' || form.id <= 0);
			});
		}
		return tab;
	},
	
	createFilterBar: function() {
		var me = this, vm = me.lookupViewModel(),
			month = Ext.Date.getFirstDateOfMonth(new Date()).getTime(),
			comboCfg = {
				displayField	: 'name',
				forceSelection	: true,
				labelAlign		: 'top',
				listConfig		: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
				selectOnFocus	: true,
				valueField		: 'id'
			},
			dictcomboCfg = Ext.apply({},{
				xtype: 'dictionarycombo',
				store: {
					type	: 'named',
					pageSize: 0,
					proxy	: { loaderConfig: { dictionaryType: 'sector_by_post' } },
					filters	: [
						{ id: 'post',	property: 'post.id',	type: 'numeric',	operator: 'eq', 	value: vm.get('userInfo.id') },
						{ id: 'month',	property: 'month',		type: 'date',		operator: '!after', value: month }
					],
					sorters	: [ 'sector.name' ]
				}
			}, comboCfg),
			filterBar = CFJS.apply({
				clearFilters: {
					xtype	: 'button',
					iconCls	: CFJS.UI.iconCls.CLEAR,
					tooltip	: 'Clear all filters'
				},
				formGroup	: Ext.apply({
					xtype		: 'dictionarytreepicker',
					bind		: { store: '{formGroups}' },
					columns		: [{
						xtype		: 'treecolumn',
						align		: 'left',
						flex		: 1,
						dataIndex	: 'name'
					}],
					disabled	: true,
					emptyText	: 'select a group',
					fieldLabel	: 'Form group',
					labelAlign	: 'top',
					name		: 'formGroup'
				}),
				group		: Ext.apply({
					emptyText	: 'select a sector group',
					fieldLabel	: 'Group',
					name		: 'group'
				}, dictcomboCfg),
				sector		: Ext.apply({
					emptyText	: 'select a sector',
					fieldLabel	: 'Sector',
					name		: 'sector'
				}, dictcomboCfg),
				type		: Ext.apply({
					xtype		: 'combo',
					bind		: { store: '{formTypes}'},
					emptyText	: 'select a type',
					fieldLabel	: 'Form type',
					listeners	: { change: me.changeFormTypeFilter, scope: me },
					name		: 'formType',
					queryMode	: 'local'
				}, comboCfg)
			}, me.filterBarConfig),
			clearFilters = Ext.create(filterBar.clearFilters),
			formGroup = Ext.create(filterBar.formGroup),
			group = Ext.create(filterBar.group),
			sector = Ext.create(filterBar.sector),
			type = Ext.create(filterBar.type);
		clearFilters.on({ click: function() { type.setValue(); formGroup.setValue(); sector.setValue(); group.setValue() } });
		formGroup.on({ change: function(field, value) { me.changeFormGroupFilter(value); } });
		group.on({ change: function(field, value) { me.changeSectorFilter(value || sector.getValue()); } });
		sector.getStore().addFilter({ id: 'parent', type: 'numeric', property: 'sector.parent.id', operator: 'eq' });
		sector.on({
			change: function(field, value) {
				group.getStore().addFilter({
					id		: 'parent',
					type	: 'numeric',
					property: 'sector.parent.id',
					operator: 'eq',
					value	: (value||{}).id
				});
				me.changeSectorFilter(value);
			}
		});
		return {
			hidden			: !clearFilters.pressed,
			items			: [ type, formGroup, sector, group, clearFilters ],
			layout			: { align: 'end', pack: 'end' },
			name			: 'filterBar',
			padding			: '0 10 10',
			referenceHolder	: true
		}
	},
	
	createBrowserView: function(cfg) {
		var me = this, store = Ext.Factory.store({
			type	: 'forms',
			autoLoad: true,
			pageSize: 50,
			filters	: [
//				{ id: 'type',			type: 'string',		property: 'type',					operator: 'eq',		value: '{type}' },
				{ id: 'permissions',	type: 'numeric',	property: 'permissions[\'READ\']',	operator: '!lt',	value: this.lookupViewModel().get('permissionLevel') }
			]
		});
		return Ext.apply(cfg, {
			bbar		: [{ xtype: 'pagingtoolbar', displayInfo: true, store: store }],
			iconCls		: CFJS.UI.iconCls.HOME,
			reorderable	: false,
			store		: store,
			tbar		: me.createFilterBar(),
			viewConfig	: { itemIconCls: '{iconCls}' }
		})
	},
	
	changeFormGroupFilter: function(value, suppressEvent) {
		this.applyBrowserStoreFilter({
			id		: 'group',
			type	: 'numeric',
			property: 'group.id',
			operator: 'eq',
			value	: (value||{}).id
		});
	},
	
	changeFormTypeFilter: function(field, value) {
		var me = this, isDocument = value === 'DOCUMENT',
			formGroup = me.down('dictionarytreepicker[name=formGroup]');
		if (formGroup) {
			formGroup.setDisabled(!isDocument);
			me.changeFormGroupFilter(isDocument && formGroup.getValue(), true);
		}
		me.applyBrowserStoreFilter({
			id		: 'type',
			type	: 'string',
			property: 'type',
			operator: 'eq',
			value	: value
		});
	},
	
	changeSectorFilter: function(value) {
		this.applyBrowserStoreFilter({
			id		: 'sector',
			type	: 'list',
			property: 'sector.id',
			operator: 'in',
			value	: (value||{}).id
		});
	},
	
	createAddFormMenu: function(types) {
		var me = this, actions = me.actions || {}, 
			addRecord = actions.addRecord,
			scope = me.lookupScope(me),
			items = [];
		if (addRecord.isAction) addRecord = addRecord.initialConfig;
		if (!addRecord.formType && addRecord.menu !== false) {
			if (types && types.isStore) {
				types.each(function(record) {
					var data = record.data;
					items.push({
						disabled: data.disabled,
						handler	: addRecord.handler,
						iconCls : CFJS.document.util.Forms.iconCls(data.id),
						formType: data.id,
						scope	: scope,
						text	: data.name
					});
				});
			}
			delete addRecord.handler;
			addRecord.menu = { items: items };
			actions.addRecord.callEach('setMenu', [addRecord.menu]);
			actions.addRecord.setDisabled(items.length === 0);
		}
	},
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.mainTitle = me.title;
		me.createAddFormMenu(vm && vm.getStore('formTypes'));
		me.callParent();
		me.relayEvents(me.browserView, [ 'itemcontextmenu' ]);
		me.browserView.on({ selectionchange: me.onItemsSelectionChange, scope: me });
	},
	
	lookupScope: function(component) {
		return this.callParent([component && component.formDesigner || component]);
	},
	
	onAddRecord: function(component) {
		var me = this, vm = me.lookupViewModel();
		me.startEdit(vm.createFormModel(component.formType, Ext.String.format('{0} {1}', component.text, ++me.formSeed)));
	},
	
	onFilterFormsToggle: function(button, state) {
		this.hideComponent(this.filterBar(), !state);
	},
	
	onItemsSelectionChange: function(selectable, selection) {
		var me = this, actions = me.actions, disabled;
		if (actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			disabled = !selection || selection.length !== 1;
			me.disableComponent(actions.filtersPanel);
			me.disableComponent(actions.exportRecord, disabled);
			me.disableComponent(actions.removeRecord, disabled);
			me.disableComponent(actions.refreshRecord, true);
			me.disableComponent(actions.saveRecord, true);
		}
	},
		
	onRemoveRecord: function(component) {
		var me = this, tab = me.getActiveTab(), selection, record;
		if (tab && tab.isHome) {
			selection = tab.getSelection();
			record = selection && selection.length > 0 && selection[0];
		} else if (tab && tab.formDesigner) {
			record = tab.lookupViewModel().getForm();
		}
		if (record && record.isModel) {
			me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					record.erase({
						callback: function(record, operation, success) {
							me.disableComponent(component);
							if (tab.formDesigner) me.remove(tab);
							else me.remove(me.itemTab(record.getId()));
							me.reloadStore();
						}
					});
				}
			});
		}
	},

	onChildTabChange: function(panel, newCard, oldCard) {
		this.disableComponent(this.toolBar, newCard === panel.pdfEditor || newCard === panel.excelEditor);
	},
	
	onTabChange: function(panel, newCard, oldCard) {
		var me = this, searchFilter = me.searchFilter();
		if (searchFilter) searchFilter.setHidden(newCard.editing);
		if (newCard.isHome) {
			me.setTitle(me.mainTitle);
			me.onChildTabChange(newCard, true);
			me.onItemsSelectionChange(newCard, newCard.getSelectionModel().getSelection());
		} else {
			me.setTitle([me.mainTitle, newCard.title].join(' / '));
			me.onChildTabChange(newCard, newCard.getActiveTab());
			if (!newCard.initializing) me.executeChildAction('initActionable', [me]);
		}
		me.redirectTo(me.cardRoute(newCard));
	},
	
	privates: {

    	cardRoute: function(card) {
    		var me = this, vm = card && card.lookupViewModel(),
				form = vm && Ext.isFunction(vm.getForm) && vm.getForm();
    		return form && form.id ? me.tabHashToken(form.id) : me.routeId;
    	},
	
		filterBar: function() {
			return this.down('toolbar[name="filterBar"]');
		},

		itemTab: function(id) {
			return this.child('[formId="' + this.tabSelector(id) + '"]');
		}
		
	}

});
