Ext.define('CFJS.project.grid.ProcessDocumentJournal', {
	extend		: 'CFJS.document.grid.DocumentJournal',
	xtype		: 'project-grid-process-document-journal',
	bodyStyle	: Ext.theme.name === 'Crisp' ? { borderTopWidth: '1px !important;' } : null,
	hideHeaders	: true,
	iconCls		: CFJS.UI.iconCls.FILE_TEXT,
	title		: 'Documents',

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.store = Ext.Factory.store({ model: 'CFJS.model.NamedModel', pageSize: 0, sorters	: [ 'name' ] });
		me.callParent();
		if (vm && vm.isBaseModel) {
			vm.bind('{_itemName_.documents}', function(documents) { me.store.setData(documents); });
			me.store.on({
				datachanged: function(store) {
					var documents = [];
					store.each(function(rec) {
						documents.push(rec.getData());
					});
					vm.getItem().set('documents', documents);
				}
			})
		}
		
	},

});