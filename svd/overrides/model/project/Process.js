Ext.define('CFJS.overrides.model.project.Process', {
	override: 'CFJS.model.project.Process',
}, function() {
	var model = Ext.data.schema.Schema.lookupEntity(this);
	model.addFields([{ name: 'documents',	type: 'array',	critical: true,	item: { entityName: 'CFJS.model.document.DocumentJournal', mapping: 'document_journal' } }]);
});