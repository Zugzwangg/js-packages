Ext.define('CFJS.desktop.locale.ukr.Desktop', {
    override: 'CFJS.desktop.Desktop',
    config: { homeButtonConfig: { text: 'Старт', tooltip: 'Почніть роботу з натискання цієї кнопки.' } },
	initComponent: function() {
		var me = CFJS.merge(this, {
	    	taskMenuConfig	: { items: [{ text: 'Закрити' },{ text: 'Закрити інші' },'-',{ text: 'Закрити все' }] }
		});
		me.callParent();
	}
});