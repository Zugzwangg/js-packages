Ext.define('CFJS.desktop.locale.ru.Desktop', {
    override: 'CFJS.desktop.Desktop',
    config: { homeButtonConfig: { text: 'Старт', tooltip: 'Начните работу с нажатия этой кнопки' } },
	initComponent: function() {
		var me = CFJS.merge(this, {
	    	taskMenuConfig	: { items: [{ text: 'Закрыть' },{ text: 'Закрыть другие' },'-',{ text: 'Закрыть всё' }] }
		});
		me.callParent();
	}
});