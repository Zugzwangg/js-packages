Ext.define('CFJS.desktop.Viewport', {
	extend		: 'CFJS.container.Viewport',
	xtype		: 'desktop-viewport',
	requires	: [ 'CFJS.desktop.ViewportController', 'CFJS.desktop.ViewportModel' ],
	ui			: 'desktop',
	controller	: 'desktop-viewport',
	viewModel	: { type: 'desktop-viewport' },

	getToolBar: function() {
		return this.desktopWrap.toolbar;
	},

	initDesktop: function(app) {
		var me = this;
		me.items = Ext.apply({ xtype: 'desktop-panel', id: 'viewport-detail-wrap', flex: 1 },  me.desktopConfig);
		return me;
	},
	
	initToolBarItems: Ext.identityFn,
	
	setToolBarItems: function(items) {
		return this.desktopWrap.setToolBarItems(items);
	}

});