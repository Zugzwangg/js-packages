Ext.define('CFJS.desktop.Taskbar', {
	extend		: 'Ext.toolbar.Toolbar',
	alias		: 'widget.desktop-taskbar',
	requires	: [
		'Ext.button.Button',
		'Ext.resizer.Splitter',
		'Ext.menu.Menu'
	],
	taskMenu	: null,
	
	initComponent: function () {
		var me = this;
		me.buttonsBar = new Ext.toolbar.Toolbar(me.getButtonsBarConfig());
		me.tray = new Ext.toolbar.Toolbar(me.getTrayConfig());
		me.items = [ me.buttonsBar, '-', me.tray ];
        me.callParent();
        me.addCls('desktop-taskbar');
	},
	
	afterLayout: function () {
		var me = this;
		me.callParent();
		me.buttonsBar.el.on('contextmenu', me.onButtonContextMenu, me);
	},

	addTaskButton: function(cmp) {
		var me = this, routeModel = cmp.routeModel,
			data = routeModel ? (routeModel.isModel ? routeModel.getData() : routeModel) 
					: { iconCls: cmp.iconCls, routeId: cmp.routeId, text: cmp.title }
			btn = me.buttonsBar.add({
				routeItem	: cmp,
				allowDepress: false,
				enableToggle: true,
				iconCls		: data.iconCls,
				handler		: 'onTaskBtnClick',
				margin		: '0 2 0 3',
				routeId		: data.routeId,
				text		: Ext.util.Format.ellipsis(data.text, 20),
				width		: 140
			});
		return btn;
	},

	getTrayConfig: function () {
		var me = this, ret = { items: me.trayItems, ui: me.ui, cls: 'desktop-tray' };
		delete me.trayItems;
		return ret;
	},
	
	getButtonsBarConfig: function () {
		return {
			flex	: 1,
			ui		: this.ui,
			cls		: 'desktop-buttons-bar',
			layout	: { overflowHandler: 'Scroller' }
		};
	},
	
	getTaskBtnFromEl: function (el) {
		return this.buttonsBar.getChildByElement(el) || null;
	},

	onButtonContextMenu: function (e) {
		var me = this, t = e.getTarget(), btn = me.getTaskBtnFromEl(t);
		if (btn && me.taskMenu) {
			e.stopEvent();
			me.taskMenu.routeItem = btn.routeItem;
			me.taskMenu.showBy(t);
		}
	},

	removeTaskButton: function (btn) {
		var me = this, found;
		me.buttonsBar.items.each(function (item) {
			if (item === btn) { found = item; }
			return !found;
		});
		if (found) me.buttonsBar.remove(found);
		return found;
	},
	
	setActiveButton: function(btn) {
    	this.buttonsBar.items.each(function (item) {
    		if (item.isButton) item.toggle(item === btn);
    	});
	}

});