Ext.define('CFJS.desktop.HomeButton', {
	extend				: 'Ext.button.Button',
	alternateClassName	: 'CFJS.HomeButton',
	alias				: [ 'widget.desktop-home-button', 'widget.desktop-homebtn' ],
	requires			: [ 'CFJS.desktop.HomeMenu' ],
	iconCls				: CFJS.UI.iconCls.NAVICON,
	text				: 'Start',
	
	initComponent: function() {
		var me = this;
		me.homeMenu = new CFJS.desktop.HomeMenu({ toolItems: me.toolItems, store: me.store });
		delete me.toolItems;
		me.callParent();
	},
	
	getNavigator: function() {
		return this.homeMenu.getNavigator();
	},

	getStore: function() {
		return this.homeMenu.getStore();
	},
	
	onClick: function(e) {
		var me = this;
		// Event is optional if we're called from click() 
		if (e) me.doPreventDefault(e);
		if (e && e.type !== 'keydown' && e.button) return;
		if (me.homeMenu && !me.disabled) me.showHomeMenu(e);
	},

	onDestroy: function() {
		var me = this;
		if (me.homeMenu) Ext.destroy(me.homeMenu);
		me.callParent();
	},
	
	onDownKey: function(e) {
		var me = this;
		if (me.homeMenu && !me.disabled) {
			me.showHomeMenu(e);
			e.stopEvent();
			return false;
		}
	},

	setStore: function (store) {
		this.homeMenu.setStore(store);
	},
	
	setToolItems: function(toolItems) {
		this.homeMenu.setToolItems(toolItems);
	},

	showHomeMenu: function(clickEvent) {
		var me = this, homeMenu = me.homeMenu,
			isPointerEvent = !clickEvent || clickEvent.pointerType;
		if (homeMenu && me.rendered) {
			if (me.tooltip && Ext.quickTipsActive && me.getTipAttr() !== 'title') {
				Ext.tip.QuickTipManager.getQuickTip().cancelShow(me.el);
			}
			if (homeMenu.isVisible()) {
				if (isPointerEvent) homeMenu.close();
				else homeMenu.focus();
			} else if (!clickEvent || homeMenu.items.getCount() > 0) {
				homeMenu.autoFocus = !isPointerEvent;
				homeMenu.showBy(me.el, me.menuAlign);
			}
		} 
		return me;
	}

});