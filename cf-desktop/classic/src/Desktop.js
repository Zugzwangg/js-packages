Ext.define('CFJS.desktop.Desktop', {
	extend			: 'Ext.panel.Panel',
	xtype			: 'desktop-panel',
	requires		: [
		'Ext.Img',
		'Ext.layout.container.Card',
		'Ext.toolbar.TextItem',
		'Ext.toolbar.Toolbar',
		'CFJS.button.LocaleButton',
		'CFJS.desktop.HomeButton',
		'CFJS.desktop.Taskbar',
		'CFJS.desktop.TrayClock'
	],
	logoConfig		: {
		xtype		: 'component',
		autoEl		: { tag: 'a', href: '#home', target: '_self' },
		bind		: { html: '{companyLogo}' },
		cls			: 'company-logo ' + (CFJS.isFrame ? ' in-frame' : ''),
		reference	: 'logotype',
		publishes	: 'width'
	},
	taskbarConfig	: {
		trayItems	: [{
			xtype	: 'btnlocale',
			minWidth: 55
		},{
			xtype	: 'desktop-trayclock',
			flex	: 1
		}]
	},
	taskMenuConfig	: {
		defaultAlign: 'br-tr',
		items		: [{
			handler	: 'onCloseTask',
			iconCls	: CFJS.UI.iconCls.CANCEL,
			text	: 'Close'
		},{
			handler	: 'onCloseOtherTasks',
			iconCls	: CFJS.UI.iconCls.CANCEL,
			text	: 'Close others'
		},'-',{
			handler	: 'onCloseAllTasks',
			iconCls	: CFJS.UI.iconCls.CANCEL,
			text	: 'Close all'
		}]
	},
	toolbarConfig	: {
		cls		: 'viewport-headerbar toolbar-btn-shadow buttons-delete-focus',
		height	: 54,
		itemId	: 'headerBar'
	},

	getContentPanel: function() {
		return this;
	},

	getNavigator: function() {
		return this.homeButton.getNavigator();
	},

	getToolBar: function() {
		return this.toolbar;
	},

	initComponent: function() {
		var me = Ext.apply(this, {
				itemId		: 'contentPanel',
				items		: [],
				layout		: { type: 'card', anchor: '100%', deferredRender: true }
			}),
			vm = me.lookupViewModel(),
			menuStore = vm ? vm.get('appMenu') : null,
			hbConfig = Ext.apply({
				action		: 'home-menu',
				arrowVisible: false,
				icon		: 'resources/images/logo.png', 
				iconCls		: 'desktop-home-button-icon', 
				cls			: 'desktop-home-button',
				margin		: '0 10 0 0',
				scale		: 'medium',
				text		: 'Start',
				tooltip		: 'Start by pressing this button'
			}, me.homeButtonConfig),
			taskMenuConfig = Ext.apply({ listeners: { hide: 'onTaskMenuHide' } }, me.taskMenuConfig),
			hbPosition = hbConfig.position || 'bottom';

		if (menuStore && menuStore.isStore) hbConfig.store = menuStore;
		delete hbConfig.position;
		me.homeButton = new CFJS.desktop.HomeButton(hbConfig);
		
		me.tbar = me.toolbar = new Ext.toolbar.Toolbar(me.toolbarConfig);
		me.setToolBarItems();

		me.bbar = me.taskbar = new CFJS.desktop.Taskbar(me.taskbarConfig);
		if (hbPosition === 'top') me.tbar.insert(1, me.homeButton);
		else me.taskbar.insert(0, [me.homeButton, '-']);
		
		me.taskMenu = me.taskbar.taskMenu = new Ext.menu.Menu(taskMenuConfig);
		me.callParent();
		me.addCls('desktop-panel');
		if (vm) {
			vm.bind('{appConfig.locale.name}', function(locale) {
				var btn = me.down('btnlocale');
				if (btn && Ext.isFunction(btn.setLocale)) btn.setLocale(locale);
			}, me, { single: true });
		}
	},
	
	initToolBarItems: function(items) {
		return [ this.logoConfig, '->'].concat(items||[]);
	},
	
	onAdd: function(component, position) {
		var me = this, rm = component.routeModel;
		if (rm && rm.isModel && !rm.get('system')) {
			component.taskButton = me.taskbar.addTaskButton(component);
			component.animateTarget = component.taskButton.el;
		}
		component.on({
			activate: me.updateActiveItem,
			beforeshow: me.updateActiveItem,
			deactivate: me.updateActiveItem,
			destroy: me.onDestoryItem,
			scope: me
		});
		me.callParent(arguments);
	},
	
	onDestoryItem: function(component) {
		var me = this;
		me.taskbar.removeTaskButton(component.taskButton);
		me.updateActiveItem();
	},
	
	setToolBarItems: function(items) {
		var me = this, toolbar = me.getToolBar();
		if (toolbar.rendered) Ext.suspendLayouts();
		toolbar.removeAll();
		toolbar.add(me.initToolBarItems(items));
		if (toolbar.rendered) Ext.resumeLayouts(true);
	},

	updateActiveItem: function(item) {
		var me = this,
			active = me.getLayout().getActiveItem(),
			last = me.lastActiveItem;
		if (last && last.destroyed) {
			me.lastActiveItem = null;
			return;
		}
		if (active === last) return;
		if (last) last.active = false;
		me.lastActiveItem = active;
		if (active) active.active = true;
        me.taskbar.setActiveButton(active && active.taskButton);
	}

});