Ext.define('CFJS.desktop.HomeMenu', {
	extend				: 'Ext.window.Window',
	alias				: 'desktop-home-menu',
	requires			: [ 'Ext.data.StoreManager', 'Ext.Img', 'CFJS.store.AppMenu' ],
	border				: true,
	bodyBorder			: true,
	bodyPadding			: '10 5',
	defaultBindProperty	: 'store',
	height				: 400,
	plain				: true,
	scrollable			: 'y', 
	width				: 350,
	
	addTool: function(tools) {
		if (!Ext.isArray(tools)) tools = [tools];
		for (var t = 0; t < tools.length; t++) {
			if (tools[t].xtype === 'tool' && tools[t].type === 'close') {
				tools.splice(t, 1);
				break;
			} 
		}
		this.callParent(arguments);
	},
	
	initComponent: function() {
		var me = this;
		me.layout = { type: 'vbox', align: 'stretch' }
		me.closeAction = 'hide';
		me.draggable = false;
		me.resizable = false,
		me.header = {
			bind	: { title: '{user.fullName}' },
			items	: [{
				xtype	: 'image',
				alt		: 'current user image',
				bind	: { src: '{user.photoUrl}' },
				cls		: 'desktop-home-menu-profile-image',
				height	: 36,
				width	: 36
			}]
		}
		me.items = me.navigator = new Ext.list.Tree({
			expanderOnly	: false,
			highlightPath	: true,
			singleExpand	: true,
			store			: me.store || { type: 'appmenu' },
			listeners: {
				selectionchange	: me.onNavigatorSelectionChange,
				scope			: me
			}
		});
		me.callParent();
		me.toolbar = new Ext.toolbar.Toolbar(Ext.apply({
			cls			: 'home-menu-toolbar',
			dock		: 'right',
			layout		: { align: 'stretch' },
			padding		: '10',
			vertical	: true,
			width		: 150
		}, me.toolConfig));
		me.setToolItems(me.toolItems);
		me.addDocked(me.toolbar);
		delete me.toolItems;
	},
	
	getNavigator: function() {
		return this.navigator;
	},
	
	getStore: function() {
		return this.navigator.getStore();
	},
	
	onFocusLeave: function(e) {
		this.callParent([e]);
		this.close();
	},
	
	onNavigatorSelectionChange: function(navigator, selected) {
    	var token = Ext.util.History.getToken() || '',
    		hashTag = selected ? selected.get('routeId') : null,
    		isCurrent = hashTag && token.indexOf(hashTag) === 0;
    	if (hashTag && !isCurrent) Ext.util.History.add(hashTag);
    	this.close();
	},
	
	setStore: function (store) {
		this.navigator.setStore(store);
	},

	setToolItems: function(toolItems) {
		var me = this, tb = me.toolbar;
    	if (tb.rendered) Ext.suspendLayouts();
		tb.removeAll();
		if (toolItems) tb.add(toolItems);
		if (tb.rendered) Ext.resumeLayouts(true);
	}
	
});