Ext.define('CFJS.desktop.ViewportController', {
	extend			: 'CFJS.view.ViewportController',
	alias			: 'controller.desktop-viewport',
	
	onCloseAllTasks: function() {
		var me = this, panel = me.getContentPanel();
    	Ext.suspendLayouts();
    	panel.removeAll();
    	me.routeToItem();
    	Ext.resumeLayouts(true);
	},
	
	onCloseOtherTasks: function() {
		var me = this, panel = me.getContentPanel(),
			routeItem = panel.taskMenu.routeItem;
		Ext.suspendLayouts();
		me.routeToItem(routeItem);
		panel.items.each(function (item) {
			if (routeItem !== item) panel.remove(item);
    	});
		Ext.resumeLayouts(true);
	},
	
	onCloseTask: function() {
		var me = this,
			panel = me.getContentPanel(),
			layout = panel.getLayout(),
			current = panel.taskMenu.routeItem;
		Ext.suspendLayouts();
		if (layout.getActiveItem() === current) me.routeToItem(layout.getPrev()||layout.getNext());
		panel.remove(current);
		Ext.resumeLayouts(true);
	},

	onTaskBtnClick: function(btn) {
    	var cmp = btn && btn.routeItem,
    		hashToken = cmp && cmp.hashToken,
    		token = Ext.isFunction(hashToken) && cmp.hashToken() || hashToken || btn && btn.routeId,
    		isCurrent = Ext.util.History.getToken() === token;
        if (token && !isCurrent) Ext.util.History.add(token);
	},
	
	onTaskMenuHide: function (menu) {
		Ext.defer(function() { menu.routeItem = null; }, 1);
	},

	onToggleNavigationSize: Ext.emptyFn,
	
	privates: {
		
		routeToItem: function(item) {
			var me = this, data = (item||{}).routeModel || {},
				token = (data.isModel ? data.get('routeId') : data.routeId) || me.getDefaultToken();
			me.setCurrentView(token);
			Ext.util.History.add(token);
		}
		
	}
	
});