Ext.define('CFJS.desktop.ViewportModel', {
	extend	: 'CFJS.view.ViewportModel',
	alias	: 'viewmodel.desktop-viewport',
	
	updateMenus: function(menus, isAdministrator) {
		var me = this, cfg = me.callParent(arguments),
			homeTools = cfg && cfg.homeTools,
			homeButton = me.getView().getDesktopWrap().homeButton;
		if (homeButton && homeTools) homeButton.setToolItems(me.hideSettings(homeTools, !isAdministrator));
	}
	
});