Ext.onReady(function() {
	CFJS.overrideStoreData('lifePhaseTypes', [{
		id			: 'ARCHIVE',
		name		: 'Архівування',
		apply		: { text: 'Архівувати',				status: 'Архівовано',	iconCls: CFJS.UI.iconCls.ARCHIVE },
		applyRework	: { text: 'Архівувати з доробками' },
		cancel		: {	text: 'Відхилити', 				status: 'Відхилено' }
	},{
		id			: 'APPROVEMENT',
		name		: 'Узгодження',
		apply		: { text: 'Узгодити',				status: 'Узгоджено',	iconCls: CFJS.UI.iconCls.GAVEL },
		applyRework	: { text: 'Узгодити з доробками' },
		cancel		: {	text: 'Відхилити', 				status: 'Відхилено' }
	},{
		id			: 'CANCELLATION',
		name		: 'Анулювання',
		apply		: { text: 'Анулювати',				status: 'Анульовано',	iconCls: CFJS.UI.iconCls.CANCEL },
		applyRework	: { text: 'Анулювати з доробками' },
		cancel		: {	text: 'Відхилити', 				status: 'Відхилено' }
	},{
		id			: 'EXECUTION',
		name		: 'Виконання',
		apply		: { text: 'Виконати',				status: 'Виконано',		iconCls: CFJS.UI.iconCls.COGS },
		applyRework	: { text: 'Виконати з доробками'},
		cancel		: {	text: 'Відхилити', 				status: 'Не виконано' }
	},{
		id			: 'FAMILIARIZATION',
		name		: 'Ознайомлення',
		apply		: { text: 'Ознайомитися',			status: 'Ознайомлений', iconCls: 'x-fa fa-newspaper-o' }
	},{
		id			: 'REGISTRATION',
		name		: 'Реєстрація',
		apply		: { text: 'Зареєструвати',			status: 'Зареєстровано',iconCls: CFJS.UI.iconCls.SERVER },
		applyRework	: { text: 'Зареєструвати з доробками' },
		cancel		: { text: 'Відхилити',				status: 'Відхилено' }
	},{
		id			: 'REWORK',
		name		: 'Доопрацювання',	
		apply		: { text: 'Завершити',				status: 'Доопрацьовано',iconCls: 'x-fa fa-repeat' }
	},{
		id			: 'SIGNING',
		name		: 'Підписання',
		apply		: { text: 'Підписати',				status: 'Підписано',	iconCls: CFJS.UI.iconCls.SIGN },
		applyRework	: { text: 'Підписати з доробками' },
		cancel		: {	text: 'Відхилити', 				status: 'Відхилено' }
	}]);
	CFJS.overrideStoreData('memberTypes', [
		{ id: 'LEADER',			name: 'Керівник' },
		{ id: 'OWNER',			name: 'Власник' },
		{ id: 'PARTICIPANT',	name: 'Учасник' },
		{ id: 'VIEWER',			name: 'Глядач' }
	]);
	CFJS.overrideStoreData('principleTypes', [
		{ id: 'PROJECT',	name: 'Проект',	iconCls: CFJS.UI.iconCls.FOLDER,		order: 0 },
		{ id: 'PROCESS',	name: 'Процес',	iconCls: CFJS.UI.iconCls.CODE_FORK,		order: 1 },
		{ id: 'TASK',		name: 'Задача',	iconCls: CFJS.UI.iconCls.STICKY_NOTE,	order: 2 }
	]);
	CFJS.overrideStoreData('transitionTypes', [
		{ id: 'CANCEL',					name: 'Скасовано' },
		{ id: 'OVERDUE',				name: 'Прострочено' },
		{ id: 'SUCCESS',				name: 'Підтверджено' },
		{ id: 'SUCCESS_WITH_REWORK',	name: 'Підтверджено з доопрацюванням' }
	]);
});
