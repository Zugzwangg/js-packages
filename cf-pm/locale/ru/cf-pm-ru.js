Ext.onReady(function() {
	CFJS.overrideStoreData('lifePhaseTypes', [{
		id			: 'ARCHIVE',
		name		: 'Архивирование',
		apply		: { text: 'Архивировать',			status: 'Архивировано',		iconCls: CFJS.UI.iconCls.ARCHIVE },
		applyRework	: { text: 'Архивировать с доработками' },
		cancel		: {	text: 'Отклонить', 				status: 'Отклонено' }
	},{
		id			: 'APPROVEMENT',
		name		: 'Согласование',
		apply		: { text: 'Согласовать',			status: 'Согласовано',		iconCls: CFJS.UI.iconCls.GAVEL },
		applyRework	: { text: 'Согласовать с доработками' },
		cancel		: {	text: 'Отклонить', 				status: 'Отклонено' }
	},{
		id			: 'CANCELLATION',
		name		: 'Аннулирование',
		apply		: { text: 'Аннулировать',			status: 'Аннулировано',		iconCls: CFJS.UI.iconCls.CANCEL },
		applyRework	: { text: 'Аннулировать с доработками' },
		cancel		: {	text: 'Отклонить', 				status: 'Отклонено' }
	},{
		id			: 'EXECUTION',
		name		: 'Выполнение',
		apply		: { text: 'Выполнить',				status: 'Выполннено',		iconCls: CFJS.UI.iconCls.COGS },
		applyRework	: { text: 'Выполнить с доработками'},
		cancel		: {	text: 'Отклонить', 				status: 'Не виконано' }
	},{
		id			: 'FAMILIARIZATION',
		name		: 'Ознакомление',
		apply		: { text: 'Ознакомиться',			status: 'Ознакомлен',		iconCls: 'x-fa fa-newspaper-o' }
	},{
		id			: 'REGISTRATION',
		name		: 'Регистрация',
		apply		: { text: 'Зарегистрировать',		status: 'Зарегистрировано',	iconCls: CFJS.UI.iconCls.SERVER },
		applyRework	: { text: 'Зарегистрировать с доработками' },
		cancel		: { text: 'Отклонить',				status: 'Отклонено' }
	},{
		id			: 'REWORK',
		name		: 'Доработка',	
		apply		: { text: 'Завершить',				status: 'Доработано',	iconCls: 'x-fa fa-repeat' }
	},{
		id			: 'SIGNING',
		name		: 'Подписание',
		apply		: { text: 'Подписать',				status: 'Подписано',	iconCls: CFJS.UI.iconCls.SIGN },
		applyRework	: { text: 'Подписать с доработками' },
		cancel		: {	text: 'Отклонить', 				status: 'Отклонено' }
	}]);
	CFJS.overrideStoreData('memberTypes', [
		{ id: 'LEADER',			name: 'Руководитель' },
		{ id: 'OWNER',			name: 'Владелец' },
		{ id: 'PARTICIPANT',	name: 'Участник' },
		{ id: 'VIEWER',			name: 'Зритель' }
	]);
	CFJS.overrideStoreData('principleTypes', [
		{ id: 'PROJECT',	name: 'Проект',		iconCls: CFJS.UI.iconCls.FOLDER,		order: 0 },
		{ id: 'PROCESS',	name: 'Процесс',	iconCls: CFJS.UI.iconCls.CODE_FORK,		order: 1 },
		{ id: 'TASK',		name: 'Задача',		iconCls: CFJS.UI.iconCls.STICKY_NOTE,	order: 2 }
	]);
	CFJS.overrideStoreData('transitionTypes', [
		{ id: 'CANCEL',					name: 'Отменено' },
		{ id: 'OVERDUE',				name: 'Просрочено' },
		{ id: 'SUCCESS',				name: 'Подтверждено' },
		{ id: 'SUCCESS_WITH_REWORK',	name: 'Подтверждено с доработкой' }
	]);
});
