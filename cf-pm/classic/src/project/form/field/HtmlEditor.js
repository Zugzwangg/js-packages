Ext.define('CFJS.project.form.field.HtmlEditor', {
	extend	: 'Ext.form.field.HtmlEditor',
	xtype	: 'project.htmleditor',
	
	getToolbarCfg: function() {
		var me = this, cfg = me.callParent(arguments);
		return cfg;
	}

});