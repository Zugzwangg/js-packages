Ext.define('CFJS.project.panel.WorkflowEditor', {
	extend				: 'Ext.form.Panel',
	xtype				: 'project-panel-workflow-editor',
	requires			: [
		'Ext.form.field.Display',
		'Ext.grid.Panel',
		'Ext.layout.container.Anchor',
		'CFJS.form.field.DictionaryComboBox'
	],
	bodyPadding			: 10,
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	defaults			: { anchor: '100%', allowBlank: false },
	defaultType			: 'textfield',
	fieldDefaults		: {
		labelAlign		: 'left',
		labelWidth		: 120,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true				
	},
	iconCls				: CFJS.UI.iconCls.HOME,
	layout				: 'anchor',
	newRecordName		: 'Life Phase',
	referenceHolder		: true,
	scrollable			: 'y',
	session				: { schema: 'project' },
	title				: 'Main',

	afterRender: function() {
		var me = this, vm = me.lookupViewModel(),
			actions = me.lookupPhaseExplorer().getActions();
		if (vm) {
			vm.bind('{_itemName_.id}', me.onWorkflowChange, me);
			vm.bind('{lifePhases}', function(store) {
				if (actions && store && store.isStore) {
					store.on({
						datachanged	: function() { me.disableComponent(actions.saveRecord, !store.isDirty()); },
						update		: function() { me.disableComponent(actions.saveRecord, !store.isDirty()); }
					});
				}
			}, me, { single: true });
			me.on({ beforesaverecord: vm.beforeSaveLifePhase, scope: vm })
		}
		me.callParent();
	},

	createPhaseExplorer: function(config) {
		var me = this,
			phaseExplorer = CFJS.apply({
				xtype			: 'grid',
				actions			: {
					addRecord	: {
						handler	: 'onAddRecord',
						iconCls	: CFJS.UI.iconCls.ADD,
						title	: 'Add life phase',
						tooltip	: 'Add new life phase'
					},
					refreshStore: {
						handler	: 'onReloadStore',
						iconCls	: CFJS.UI.iconCls.REFRESH,
						title	: 'Update the data',
						tooltip	: 'Update the data' 
					},
					removeRecord: {
						handler	: 'onRemoveRecord',
						iconCls	: CFJS.UI.iconCls.DELETE,
						title	: 'Remove life phase',
						tooltip	: 'Remove selected life phase'
					},
					saveRecord	: {
						handler	: 'onSaveChanges',
						iconCls	: CFJS.UI.iconCls.SAVE,
						title	: 'Save the changes',
						tooltip	: 'Save the changes'
					}
				},
				actionsDisable	: [ 'addRecord', 'removeRecord', 'saveRecord', 'refreshStore' ],
				actionsMap		: {
					contextMenu	: { items: [ 'addRecord', 'removeRecord', 'saveRecord' ] },
					header		: { items: [ 'addRecord', 'removeRecord', 'saveRecord', 'refreshStore' ] }
				},
				actionsScope	: me,
				allowDeselect	: true,
				bind			: '{lifePhases}',
				columnLines		: true,
				columns			: [{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'name',
					flex		: 1,
					style		: { textAlign: 'center' },
					text		: 'Name',
					tpl			: '<span class="' + CFJS.UI.iconCls.CODE_FORK + '" style="padding-right:10px"></span>{name}',
				}],
				contextMenu		: { items: new Array(3) },
				enableColumnHide: false,
				header			: CFJS.UI.buildHeader({ items: new Array(4), title: 'Life phases' }),
				loadMask		: true,
				name			: 'phaseExplorer',
				plugins			: [{
					ptype: 'datatip',
					tpl: [
						'<tpl if="name"><b>Name</b>: {name}</br></tpl>',
						'<tpl if="description"><b>Description</b>: {description}</tpl>'
					]
				}],
				userCls			: 'shadow-panel',
				viewConfig		: { emptyText: 'No data to display', deferEmptyText: false },
			}, config);
		phaseExplorer = Ext.create(phaseExplorer);
		phaseExplorer.on({ selectionchange: me.onSelectionChange, scope: me });
		return phaseExplorer;
	},
	
	initComponent: function() {
		var me = this;
		me.items = CFJS.apply([{
			xtype		: 'displayfield',
			bind		: '{_itemName_.id}',
			fieldLabel	: 'ID',
			name		: 'id'
		},{
			bind		: '{_itemName_.name}',
			emptyText	: 'input name',
			fieldLabel	: 'Name',
			name		: 'name',
			required	: true
		},{
			xtype		: 'dictionarycombo',
			bind		: { value: '{_itemName_.initialPhase}'},
			emptyText	: 'select initial phase',
			fieldLabel	: 'Initial phase',
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'initialPhase',
			publishes	: 'value',
			store		: {
				type	: 'named',
				proxy	: { loaderConfig: { dictionaryType: 'life_phase' } },
				sorters	: [ 'name' ]
			}
		}, me.ensureChild('phaseExplorer')], me.fieldsConfig);
		me.callParent();
	},
	
	lookupPhaseExplorer: function() {
		return this.lookupChild('phaseExplorer');
	},

	lookupPhasesStore: function() {
		var explorer = this.lookupPhaseExplorer(); 
		return explorer && explorer.getStore();
	},
	
	onAddRecord: function(component) {
		var me = this, vm = me.lookupViewModel(), store = me.lookupPhasesStore();
		me.startEdit(vm.createLifePhaseModel(Ext.String.format('{0} {1}', me.newRecordName, (store && store.isStore && store.getTotalCount() || 0) + 1)));
	},
	
	onReloadStore: function(component) {
		var store = this.lookupPhasesStore();
		if (store && store.isStore) {
			if (Ext.isFunction(store.rejectChanges)) store.rejectChanges();
			store.reload();
		}
	},
	
	onRemoveRecord: function(component) {
		var me = this, explorer = me.lookupPhaseExplorer(),
			store = explorer && explorer.getStore(), i = 0, records;
		if (store && store.isStore && !explorer.readOnly
				&& me.fireEvent('beforeremoverecord', records = explorer.getSelection()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].erase(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},
	
	onSaveChanges: function(component, force) {
		var me = this, explorer = me.lookupPhaseExplorer(),
			store = explorer && explorer.getStore(), i = 0, records;
		if (store && store.isStore && !explorer.readOnly
				&& me.fireEvent('beforesaverecord', records = store.getModifiedRecords()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].save(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},
	
	onSelectionChange: function(selectable, selection) {
		var me = this, vm = me.lookupViewModel(), explorer = me.lookupPhaseExplorer(),
			actions = (explorer && explorer.getActions()) || {};
		if (Ext.isObject(selection)) selection = selection.selectedRecords;
		me.disableComponent(actions.removeRecord, !(selection && selection.length > 0));
		vm.set('lifePhase', selection && selection.length > 0 ? selection[0] : null);
	},

	onWorkflowChange: function(workflowId) {
		var me = this, explorer = me.lookupPhaseExplorer(),
			actions = explorer && explorer.getActions(),
			initialPhase = me.down('dictionarycombo[name=initialPhase]'),
			hasId = workflowId > 0;
		if (actions) {
			me.disableComponent(actions.addRecord, !hasId);
			me.disableComponent(actions.removeRecord, !(hasId && Ext.Array.from(explorer.getSelection()).length > 0));
			me.disableComponent(actions.refreshStore, !hasId);
			me.disableComponent(actions.saveRecord, true);
		}
		if (initialPhase && initialPhase.store) {
			initialPhase.store.addFilter({
				id		: 'workflow',
				property: 'workflow.id',
				type	: 'numeric',
				operator: 'eq',
				value	: workflowId
			}, true);
		}
	},
	
	startEdit: function(record) {
		var me = this, explorer = me.lookupPhaseExplorer(),
			context = new Ext.grid.CellContext(explorer.view);
		context.setPosition(record = explorer.getStore().add(record)[0], 0);
		explorer.getSelectionModel().select(record);
	}

});