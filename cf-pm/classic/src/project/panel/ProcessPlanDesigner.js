Ext.define('CFJS.project.panel.ProcessPlanDesigner', {
	extend			: 'CFJS.panel.BaseDesigner',
	xtype			: 'project-panel-process-plan-designer',
	requires		: [
		'Ext.ux.DataTip',
		'CFJS.project.panel.ProcessPlanEditor',
		'CFJS.project.view.ProcessPlanDesignerModel'
	],
	header			: false,
	iconCls			: 'x-fa fa-life-ring',
	newRecordName	: 'New Task',
	session			: { schema: 'project' },
	startProcessMsg	: 'Starting process...',
	scrollable		: 'x',
	title			: 'Plan',
	viewModel		: { type: 'project.process-plan-designer' },

	createExplorer: function(config) {
		var me = this,
			explorer = CFJS.apply({
				xtype			: 'form',
				actions			: {
					addRecord	: {
						bind	: { hidden: '{!itemRules.editWorkflow}' },
						handler	: 'onAddRecord',
						iconCls	: CFJS.UI.iconCls.ADD,
						title	: 'Add task',
						tooltip	: 'Add new task'
					},
					refreshStore: {
						bind	: { hidden: '{!itemRules.editWorkflow}' },
						handler	: 'onReloadStore',
						iconCls	: CFJS.UI.iconCls.REFRESH,
						title	: 'Update the data', 
						tooltip	: 'Update the data' 
					},
					removeRecord	: {
						bind	: { hidden: '{!itemRules.editWorkflow}' },
						handler	: 'onRemoveRecord',
						iconCls	: CFJS.UI.iconCls.DELETE,
						title	: 'Remove task',
						tooltip	: 'Remove selected task'
					},
					saveRecord	: {
						bind	: { hidden: '{!itemRules.editWorkflow}' },
						formBind: true,
						handler	: 'onSaveChanges',
						iconCls	: CFJS.UI.iconCls.SAVE,
						title	: 'Save the changes',
						tooltip	: 'Save the changes'
					},
					startProcess: {
						bind	: { hidden: '{!executable}' },
						formBind: true,
						handler	: 'onStartProcess',
						iconCls	: 'x-fa fa-bolt',
						title	: 'Start process',
						tooltip	: 'Start process'
					}
				},
				actionsDisable	: [ 'removeRecord', 'saveRecord' ],
				actionsMap		: {
					contextMenu	: { items: [ 'addRecord', 'removeRecord', 'saveRecord' ] },
					header		: { items: [ 'addRecord', 'removeRecord', 'saveRecord', 'refreshStore', 'startProcess' ] }
				},
				actionsScope	: me,
				bind			: { readOnly: '{!itemRules.editWorkflow}' },
				contextMenu		: { items: new Array(3) },
				defaults		: { anchor: '100%', required: false },
				header			: CFJS.UI.buildHeader({ items: new Array(5), title: 'Tasks' }),
				items			: [{
					xtype		: 'dictionarycombo',
					anchor		: '100%',
					bind		: { readOnly: '{!itemRules.editWorkflow}', store: '{phases}', value: '{_parentItemName_.currentPhase}'},
					editable	: false,
					emptyText	: 'select initial task',
					fieldLabel	: 'Current task',
					labelAlign	: 'top',
					listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
					msgTarget	: Ext.supports.Touch ? 'side' : 'qtip',
					margin		: '0 10',
					name		: 'currentPhase',
					publishes	: 'value',
					queryMode	: 'local',
					typeAhead	: false
				}, me.ensureChild('explorerGrid') ],
				layout			: 'anchor',
				maxWidth		: 600,
				minWidth		: 250,
				name			: 'explorer',
				region			: 'west',
				scrollable		: 'y',
				width			: 300
			}, config);
		return explorer;
	},
	
	createExplorerGrid: function(config) {
		var me = this,
			grid = CFJS.apply({
				xtype			: 'grid',
				allowDeselect	: true,
				bind			: '{processPlans}',
				columnLines		: true,
				columns			: [{
					xtype		: 'templatecolumn',
					align		: 'left',
					dataIndex	: 'name',
					flex		: 1,
					menuDisabled: true,
					style		: { textAlign: 'center' },
					text		: 'Name',
					tpl			: '<span class="' + CFJS.UI.iconCls.CODE_FORK + '" style="padding-right:10px"></span>{name}',
				}],
				enableColumnHide: false,
				flex			: 1,
				loadMask		: true,
				margin			: 10,
				name			: 'explorerGrid',
				plugins			: [{
					ptype: 'datatip',
					tpl: [
						'<tpl if="name"><b>Name</b>: {name}</br></tpl>',
						'<tpl if="description"><b>Comment</b>: {description}</tpl>'
					]
				}],
				readOnly		: false,
				userCls			: 'shadow-panel',
				viewConfig		: { emptyText: 'No data to display', deferEmptyText: false },
			}, config);
		return grid;
	},
	
	createPlanEditor: function(config) {
		var me = this,
			planEditor = CFJS.apply({
				xtype		: 'project-panel-process-plan-editor',
				bind		: { disabled: '{!_itemName_}', readOnly: '{!itemRules.editWorkflow}' },
				disabled	: true,
				collapsible	: false,
				region		: 'center',
				minWidth	: 400,
				name		: 'planEditor'
			}, config);
		return Ext.create(planEditor);
	},

	initActionable: Ext.emptyFn,

	initComponent: function() {
		var me = this, vm = me.lookupViewModel(), actions;
		me.items = [ me.ensureChild('explorer'), me.ensureChild('planEditor') ];
		me.callParent();
		me.on({ beforesaverecord: vm.beforeSaveProcessPlan, beforeremoverecord: vm.beforeRemoveProcessPlan, scope: vm });
		if (me.actionable = me.lookupChild('explorer')) {
			me.explorer.on({ validitychange: me.onValidityChange, scope: me });
			if (me.lookupChild('explorerGrid')) {
				me.explorerGrid.contextMenu = me.explorer.contextMenu;
				me.explorerGrid.on({ selectionchange: me.onPhaseSelectionChange, scope: me });
			}
			vm.bind('{_parentItemName_.id}', function() {
				me.explorerGrid.getSelectionModel().deselectAll();
			});
			if (actions = me.explorer.getActions()) {
				vm.bind('{processPlans}', function(store) {
					if (store && store.isStore) {
						store.on({
							datachanged	: function(store) { me.disableComponent(actions.saveRecord, !store.isDirty()); },
							load		: function(store, records) { vm.set('hasProcessPlan', !Ext.isEmpty(records)) },
							update		: function(store) { me.disableComponent(actions.saveRecord, !store.isDirty()); }
						});
					}
				}, me, { single: true });
			}
		}
	},
	
	lookupProcessStore: function() {
		return this.explorerGrid.getStore();
	},

	onAddRecord: function(component) {
		var me = this, vm = me.lookupViewModel(), store = me.lookupProcessStore(),
			totalCount = store && store.isStore && store.getTotalCount() || 0;
		me.startEdit(vm.createProcessPlanModel(Ext.String.format('{0}. {1}', totalCount + 1, me.newRecordName), totalCount));
	},
	
	onPhaseSelectionChange: function(selectable, selection) {
		var me = this, vm = me.lookupViewModel(),
			actions = me.explorer.getActions() || {};
		if (Ext.isObject(selection)) selection = selection.selectedRecords;
		me.disableComponent(actions.removeRecord, !(selection && selection.length > 0));
		vm.setItem(selection && selection.length > 0 ? selection[0] : false);
	},
	
	onReloadStore: function(component) {
		var store = this.lookupProcessStore();
		if (store && store.isStore) {
			if (Ext.isFunction(store.rejectChanges)) store.rejectChanges();
			store.reload();
		}
	},

	onRemoveRecord: function(component) {
		var me = this, explorerGrid = me.explorerGrid,
			store = explorerGrid && explorerGrid.getStore(), i = 0, records;
		if (store && store.isStore && !explorerGrid.readOnly
				&& me.fireEvent('beforeremoverecord', records = explorerGrid.getSelection()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].erase(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload();
						me.disableComponent(component);
					}
				});
			}
		}
	},

	onSaveChanges: function(component, callback) {
		var me = this, explorerGrid = me.explorerGrid,
			store = explorerGrid && explorerGrid.getStore(), i = 0, records;
		if (!Ext.isFunction(callback)) callback = undefined; 
		if (store && store.isStore && !explorerGrid.readOnly
				&& me.fireEvent('beforesaverecord', records = store.getModifiedRecords()) !== false && records.length > 0) {
			me.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].save(i < records.length - 1 || {
					callback: function(record, operation, success) {
						store.reload({ callback: callback });
						me.disableComponent(component);
					}
				});
			}
		} else Ext.callback(callback, me, [store.getRange(), null, true]);
	},
	
	onStartProcess: function(component) {
		var me = this, vm = me.lookupViewModel();
		me.mask(me.startProcessMsg);
		me.onSaveChanges(component, function(records, operation, success) {
			if (success && vm && Ext.isFunction(vm.startProcess)) {
				vm.startProcess(function(record, success) { me.unmask() });
			}
		});
	},

	startEdit: function(record) {
		var me = this, explorerGrid = me.explorerGrid,
			context = new Ext.grid.CellContext(explorerGrid.view);
		context.setPosition(record = explorerGrid.getStore().add(record)[0], 0);
		explorerGrid.getSelectionModel().select(record);
	}

});