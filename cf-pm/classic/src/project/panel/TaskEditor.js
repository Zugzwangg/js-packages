Ext.define('CFJS.project.panel.TaskEditor', {
	extend	: 'CFJS.project.panel.ProcessElementEditor',
	xtype	: 'project-panel-task-editor',
	requires: [ 'Ext.form.field.Checkbox', 'Ext.form.field.Number' ],
	
	createItems: function() {
		var me = this, items = me.callParent();
		items[1].items.splice(2, 0, {
			xtype			: 'combobox',
			bind			: { value: '{_itemName_.phase}' },
			displayField	: 'name',
			editable		: false,
			emptyText		: 'select a phase',
			fieldLabel		: 'Phase',
			forceSelection	: true,
			name			: 'phase',
			publishes		: 'value',
			queryMode		: 'local',
			required		: true,
			selectOnFocus	: false,
			store			: 'lifePhaseTypes',
			valueField		: 'id',
			width			: 150
		});
		items.splice(3, 0, {
			xtype		:'fieldcontainer',
			defaults	: { margin: '0 0 0 5' },
			defaultType	: 'checkbox',
			fieldLabel	: 'Goal',
			items		: [{
				bind	: { readOnly: '{!itemRules.editGoal}', value: '{_itemName_.numerical}' },
				boxLabel: 'Numerical',
				margin	: 0,
				name	: 'numerical'
			},{
				xtype			: 'numberfield',
				allowExponential: false,
				bind			: { hidden: '{!_itemName_.numerical}', value: '{_itemName_.goal}' },
				emptyText		: 'input numerical goal',
				minValue		: 1,
				name			: 'goal',
				required		: true
			},{
				bind	: '{_itemName_.withReport}',
				boxLabel: 'With report',
				name	: 'withReport'
			}],
			layout		: 'hbox',
			name		: 'goalData'
		});
		return items;
	}

});