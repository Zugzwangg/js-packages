Ext.define('CFJS.project.panel.TaskStatusEditor', {
	extend			: 'Ext.form.Panel',
	xtype			: 'project-panel-task-status-editor',
	requires		: [
		'Ext.form.field.Tag',
		'Ext.layout.container.Anchor',
	],
	bodyPadding		: '0 10 10',
	defaultFocus	: 'textarea:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaults		: { anchor	: '100%' },
	defaultType		: 'container',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	items			: [{
		xtype		: 'tagfield',
		bind		: { value: '{_itemName_.involvement}' },
		emptyText	: 'select employees to participate',
		displayField: 'name',
		fieldLabel	: 'Requires involvement',
		minChars	: 0,
		name		: 'involvement',
		store		: {
        	type	: 'named',
			proxy	: { loaderConfig: { dictionaryType: 'person' } },
			filters	: [{ id: 'post', property: 'post.id', type: 'numeric', operator: '!eq' }],
			sorters	: [ 'name' ]
		},
		typeAhead		: true
	},{
		xtype		: 'textareafield',
		bind		: '{_itemName_.description}',
		emptyText	: 'input the comment',
		fieldLabel	: 'Comment',
		height		: 200,
		name		: 'description'
	}],
	layout			: 'anchor',
	progressLabel	: 'Execution',
	sendMessageText	: 'Send message',
	title			: 'Status',
	
	initComponent: function() {
		var me = this;
		me.bbar	= {
			bind	: { disabled: '{!editable}' },
			layout	: { pack: 'center' },
			padding	: '0 0 10 10',
			items	: [{
				xtype			: 'numberfield',
				allowExponential: false,
				bind			: { hidden: '{numericalHidden}', value: '{_itemName_.progress}' },
				fieldLabel		: me.progressLabel,
				labelAlign		: 'left',
				minValue		: 0,
				name			: 'progress',
				required		: true
			},{
				action	: 'apply',
				bind	: { hidden: '{applyHidden}', iconCls: '{applyIconCls}', text: '{applyText}' },
				handler	: 'onButtonClick'
			},{
				action	: 'applyRework',
				bind	: { hidden: '{applyReworkHidden}', iconCls: '{applyReworkIconCls}', text: '{applyReworkText}' },
				handler	: 'onButtonClick'
			},{
				action	: 'send',
				handler	: 'onButtonClick',
				iconCls	: CFJS.UI.iconCls.SEND,
				text	: me.sendMessageText
			},'->',{
				action	: 'cancel',
				bind	: { hidden: '{cancelHidden}', text: '{cancelText}' },
				handler	: 'onButtonClick',
				iconCls	: CFJS.UI.iconCls.CANCEL
			}]
		};
		me.callParent();
	}

});
