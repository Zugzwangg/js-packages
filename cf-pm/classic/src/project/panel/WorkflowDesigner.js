Ext.define('CFJS.project.panel.WorkflowDesigner', {
	extend			: 'CFJS.panel.BaseDesigner',
	xtype			: 'project-panel-workflow-designer',
	requires 		: [ 'CFJS.project.panel.LifePhaseEditor', 'CFJS.project.panel.WorkflowEditor' ],
	iconCls			: CFJS.UI.iconCls.SITEMAP,
	refreshDataText	: 'reload data of workflow',
	title			: 'Workflow editor',

	afterRecordSave: function(component, force, record) {
		this.explorer.onSaveChanges(component, force);
		this.ownerCt.workflowId = this.actionable.tabSelector(record.getId());
	},
	
	createExplorer: function(config) {
		var me = this,
			explorer = CFJS.apply({
				xtype	: 'project-panel-workflow-editor',
				header	: false,
				region	: 'west',
				minWidth: 300,
				maxWidth: 600,
				width	: 350
			}, config);
		explorer = Ext.create(explorer);
		explorer.on({ validitychange: me.onValidityChange, scope: me });
		return explorer;
	},
	
	createPhaseEditor: function(config) {
		var me = this,
			phaseEditor = CFJS.apply({
				xtype		: 'project-panel-life-phase-editor',
				collapsible	: false,
				region		: 'center',
				name		: 'phaseEditor'
			}, config);
		return Ext.create(phaseEditor);
	},
	
	initActionable: function(actionable, valid) {
		var actions = actionable && actionable.actions;
		if (actions) {
			if (!Ext.isBoolean(valid)) valid = this.isValid();
			actionable.disableComponent(actions.removeRecord);
			actionable.disableComponent(actions.refreshRecord);
			actionable.disableComponent(actions.saveRecord, !valid);
		}
	},

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.items = [ me.ensureChild('explorer'), me.ensureChild('phaseEditor') ];
		me.callParent();
		me.on({ beforesaverecord: vm.beforeSaveWorkflow, scope: vm });
	}

});