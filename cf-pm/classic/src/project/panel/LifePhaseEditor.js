Ext.define('CFJS.project.panel.LifePhaseEditor', {
	extend				: 'Ext.form.Panel',
	xtype				: 'project-panel-life-phase-editor',
	requires			: [
		'Ext.form.field.Number',
		'Ext.layout.container.Anchor',
		'CFJS.form.field.DictionaryComboBox',
		'CFJS.project.grid.ProcessMembersPanel'
	],
	bind				: { disabled: '{!lifePhase}'},
	bodyPadding			: 10,
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	defaults			: { anchor: '100%' },
	defaultType			: 'container',
	fieldDefaults		: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true				
	},
	items				: [{
		name		: 'main',
		defaults	: { margin: '0 0 0 5' },
		defaultType	: 'textfield',
		layout		: 'hbox',
		items		: [{
			bind		: '{lifePhase.name}',
			emptyText	: 'phase name',
			fieldLabel	: 'Phase name',
			flex		: 1,
			margin		: 0,
			name		: 'name',
			required	: true
		},{
			xtype			: 'combobox',
			bind			: { value: '{lifePhase.type}' },
			displayField	: 'name',
			editable		: false,
			emptyText		: 'select a type',
			fieldLabel		: 'Phase type',
			forceSelection	: true,
			name			: 'type',
			publishes		: 'value',
			queryMode		: 'local',
			required		: true,
			selectOnFocus	: false,
			store			: 'lifePhaseTypes',
			valueField		: 'id',
			width			: 150
		},{
			xtype			: 'numberfield',
			allowExponential: false,
			bind			: '{lifePhase.duration}',
			fieldLabel		: 'Duration (days)',
			minValue		: 0,
			name			: 'duration',
			required		: true,
			width			: 200
		}]
	},{
		xtype		: 'textarea',
		bind		: '{lifePhase.description}',
		emptyText	: 'input description',
		fieldLabel	: 'Description',
		height		: 200,
		name		: 'description'
	},{
		xtype			: 'grid-enum-panel',
		bind			: { store: '{lifePhaseTransitions}' },
		iconCls			: CFJS.UI.iconCls.COGS,
		margin			: '10 0 0 0',
		name			: 'transitions',
		nameField		: 'type',
		nameRenderer	: Ext.util.Format.enumRenderer('transitionTypes'),
		nameText		: 'Transition condition',
		sortableColumns	: false,
		title			: 'Transitions',
		userCls			: 'shadow-panel',
		valueEditor		: {
			xtype		: 'dictionarycombo',
			bind		: { store: '{nextElements}' },
			emptyText	: 'select a phase',
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'nextElement'
		},
		valueField		: 'data',
		valueRenderer	: function(value) { return (value || {}).name; },
		valueText		: 'Life phase'
	},{
		xtype	: 'project-grid-process-members-panel',
		bind	: { store: '{lifePhaseMembers}' },
		iconCls	: CFJS.UI.iconCls.USERS,
		margin	: '10 0 0 0',
		name	: 'members',
		title	: 'Members',
		userCls	: 'shadow-panel'
	}],
	layout				: 'anchor',
	scrollable			: 'y'

});