Ext.define('CFJS.project.panel.ProcessPlanEditor', {
	extend				: 'Ext.form.Panel',
	xtype				: 'project-panel-process-plan-editor',
	requires			: [
		'Ext.grid.Panel',
		'Ext.form.field.Display',
		'Ext.layout.container.Anchor',
		'CFJS.form.field.DictionaryComboBox'
	],
	bodyPadding			: 10,
	defaultFocus		: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaultListenerScope: true,
	defaults			: { anchor: '100%' },
	defaultType			: 'container',
	fieldDefaults		: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true				
	},
	items				: [{
		name		: 'main',
		defaults	: { margin: '0 0 0 5' },
		defaultType	: 'textfield',
		layout		: 'hbox',
		items		: [{
			bind		: '{_itemName_.name}',
			emptyText	: 'input task name',
			fieldLabel	: 'Name',
			flex		: 1,
			margin		: 0,
			name		: 'name',
			required	: true
		},{
			xtype			: 'combobox',
			bind			: { value: '{_itemName_.type}' },
			displayField	: 'name',
			editable		: false,
			emptyText		: 'select a phase',
			fieldLabel		: 'Phase',
			forceSelection	: true,
			name			: 'type',
			publishes		: 'value',
			queryMode		: 'local',
			required		: true,
			selectOnFocus	: false,
			store			: 'lifePhaseTypes',
			valueField		: 'id',
			width			: 140
		},{
			xtype			: 'numberfield',
			allowExponential: false,
			bind			: '{_itemName_.duration}',
			fieldLabel		: 'Duration (days)',
			minValue		: 0,
			name			: 'duration',
			required		: true,
			width			: 150
		}]
	},{
		xtype		: 'textarea',
		bind		: '{_itemName_.description}',
		emptyText	: 'input comment',
		fieldLabel	: 'Comment',
		height		: 200,
		name		: 'description'
	},{
		xtype			: 'grid-enum-panel',
		bind			: { store: '{planTransitions}' },
		iconCls			: CFJS.UI.iconCls.COGS,
		margin			: '10 0 0 0',
		name			: 'transitions',
		nameField		: 'type',
		nameRenderer	: Ext.util.Format.enumRenderer('transitionTypes'),
		nameText		: 'Transition condition',
		sortableColumns	: false,
		title			: 'Transitions',
		userCls			: 'shadow-panel',
		valueEditor		: {
			xtype		: 'dictionarycombo',
			bind		: { store: '{nextTasks}' },
			emptyText	: 'select a task',
			listConfig	: { itemTpl: ['<div data-qtip="{name}" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">{name}</div>'] },
			name		: 'nextElement',
			queryMode	: 'local'
		},
		valueField		: 'data',
		valueRenderer	: function(value) { return (value || {}).name; },
		valueText		: 'Task'
	},{
		xtype	: 'project-grid-process-members-panel',
		bind	: { store: '{planMembers}' },
		iconCls	: CFJS.UI.iconCls.USERS,
		margin	: '10 0 0 0',
		name	: 'members',
		title	: 'Members',
		userCls	: 'shadow-panel'
	}],
	layout				: 'anchor',
	scrollable			: 'y',
	
	setReadOnly: function(readOnly) {
		var me = this;
		Ext.Array.each(Ext.Array.from(me.query('grid')), function(grid) {
			if (Ext.isFunction(grid.setReadOnly)) grid.setReadOnly(readOnly);
		});
		me.callParent(arguments);
	}

});