Ext.define('CFJS.project.panel.ProcessElementEditor', {
	extend			: 'Ext.form.Panel',
	xtype			: 'project-panel-process-element-editor',
	requires		: [
		'Ext.ProgressBar',
		'Ext.button.Button',
		'Ext.form.field.Display',
		'Ext.form.field.TextArea',
		'Ext.form.FieldContainer',
		'Ext.layout.container.Anchor',
		'Ext.layout.container.HBox',
		'CFJS.form.DateTimeField',
		'CFJS.project.grid.ProcessMembersPanel'
	],
	bind			: { readOnly: '{!itemRules.editable}' },
	bodyPadding		: 10,
	defaultFocus	: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	defaults		: { anchor	: '100%' },
	defaultType		: 'container',
	fieldDefaults	: {
		labelAlign		: 'top',
		labelWidth		: 100,
		msgTarget		: Ext.supports.Touch ? 'side' : 'qtip',
		selectOnFocus	: true
	},
	header			: false,
	iconCls			: CFJS.UI.iconCls.HOME,
	layout			: 'anchor',
	progressLabel	: 'Process progress',
	scrollable		: 'y',
	title			: 'Main',
	
	createItems: function() {
		var me = this, messagePriorities = Ext.getStore('messagePriorities'),
			vm = me.lookupViewModel(), items = [], priorityMenu;
		if (messagePriorities) {
			messagePriorities.each(function(record) {
				var pr = record.getId() || 'NORMAL';
				items.push({
					xtype	: 'menucheckitem',
					bind	: { checked: '{_itemName_.priority === "' + pr + '"}' },
					handler	: function() { vm.getItem().set('priority', pr); },
					iconCls	: CFJS.UI.iconCls.FLAG + ' ' + CFJS.schema.Project.priorityColorCls(pr),
					group	: 'priority',
					text	: record.get('name')
				});
			});
			if (items.length > 0) priorityMenu = { items: items };
		}
		return [{
			defaultType	: 'displayfield',
			defaults	: { labelStyle: 'font-weight:bold', margin: '0 0 0 5' },
			items		: [
				{ bind: '{_itemName_.id}',		fieldLabel: 'ID',		name: 'id',			margin: 0 },
				{ bind: '{_itemName_.type}',	fieldLabel: 'Type',		name: 'type',		renderer: Ext.util.Format.enumRenderer('principleTypes'),	width: 80 },
				{ bind: '{_itemName_.author}',	fieldLabel: 'Author',	name: 'author',		renderer: Ext.util.Format.objectRenderer('name'),			flex: 1 },
				{ bind: '{_itemName_.created}',	fieldLabel: 'Created',	name: 'created',	renderer: Ext.util.Format.dateRenderer(Ext.Date.patterns.LongDateTime) },
				{ bind: '{_itemName_.updated}',	fieldLabel: 'Updated',	name: 'updated', 	renderer: Ext.util.Format.dateRenderer(Ext.Date.patterns.LongDateTime) }
			],
			layout		: 'hbox',
			name		: 'author'
		},{
			defaults	: { margin: '0 0 0 5' },
			layout		: { type: 'hbox', align: 'end' },
			items		: [{
				xtype		: 'textfield',
				bind		: '{_itemName_.name}',
				emptyText	: 'input name of the element',
				fieldLabel	: 'Name',
				flex		: 1,
				margin		: 0,
				name		: 'name',
				required	: true
			},{
				xtype		: 'displayfield',
				bind		: '{_itemName_}',
				fieldLabel	: 'Path in the tree',
				flex		: 2,
				name		: 'path',
				renderer	: function(value) {
					if (value && value.isNode) {
						var sep = ' / ';
						value = value.getPath('name', sep);
						if (value.startsWith(sep + sep)) value = value.substring(sep.length);
					}
					return value;
				}
			},{
				xtype	: 'button',
				bind	: { iconCls: '{priorityIconCls}' },
				menu	: priorityMenu,
				name	: 'priority',
				text	: 'Priority',
				tooltip	: 'Priority of the element',
				ui		: 'default-toolbar'
			}],
			name		: 'resource'
		},{
			xtype		: 'textarea',
			bind		: '{_itemName_.description}',
			emptyText	: 'input description of the element',
			fieldLabel	: 'Description',
			height		: 200,
			name		: 'description'
		},{
			defaults	: { margin: '0 0 0 5' },
			layout		: { type: 'hbox', align: 'end' },
			defaultType	: 'datetimefield',
			items		: [{
				bind		: '{_itemName_.startDate}',
				fieldLabel	: 'Start at',
				fieldOffset	: 5,
				fieldTime	: { width: 90 },
				margin		: 0,
				name		: 'startDate',
				required	: true
			},{
				bind		: '{_itemName_.endDate}',
				fieldLabel	: 'Deadline at',
				fieldOffset	: 5,
				fieldTime	: { width: 90 },
				name		: 'endDate'
			},{
				bind		: '{_itemName_.finished}',
				editable	: false,
				fieldLabel	: 'Finished at',
				fieldOffset	: 5,
				fieldTime	: { width: 90 },
				name		: 'finished',
			},{
				xtype	: 'button',
				bind	: { hidden: '{!itemRules.canFinish}' },
				iconCls	: CFJS.UI.iconCls.APPLY,
				name	: 'finish',
				text	: 'Finish',
				tooltip	: 'Finish the process',
				ui		: 'default-toolbar'
			}],
			name		: 'dates'
		},{
			xtype			: 'project-grid-process-members-panel',
			bind			: { store: '{principleMembers}' },
			margin			: '10 0',
			name			: 'members',
			userCls			: 'shadow-panel'
		}];
	},
	
	initComponent: function() {
		var me = this;
		me.bbar = {
			xtype		:'fieldcontainer',
			margin		: '5 10 10',
			fieldLabel	: me.progressLabel,
			items		: { xtype: 'progressbar', bind: '{_itemName_.progress}' },
			name		: 'progress'
		};
		me.callParent();
	},
	
	initItems: function() {
		var me = this;
		if (!me.items || !me.items.isMixedCollection) me.items = me.createItems();
		me.callParent();
	},
	
	setReadOnly: function(readOnly) {
		var me = this;
		Ext.Array.each(Ext.Array.from(me.query('button[name!=finish],grid')), function(cmp) {
			if (Ext.isFunction(cmp.setReadOnly)) cmp.setReadOnly(readOnly);
			else me.disableComponent(cmp, readOnly);
		});
		me.callParent(arguments);
	}

});