Ext.define('CFJS.project.grid.ProcessMembersPanel', {
	extend				: 'CFJS.grid.EventMembersPanel',
	xtype				: 'project-grid-process-members-panel',
	requires			: [
		'Ext.form.field.ComboBox',
		'CFJS.admin.form.field.PostPickerWindow',
		'CFJS.store.project.MemberTypes'
	],
	memberTypeDefault	: 'PARTICIPANT',
	memberTypeEditor	: {
		xtype			: 'combo',
		displayField	: 'name',
		editable		: false,
		forceSelection  : true,
		name			: 'memberType',
		queryMode		: 'local',
		store			: 'memberTypes',
		selectOnFocus	: false,
		valueField		: 'id'
	},
	memberTypeField		: 'memberType',
	memberTypeRenderer	: Ext.util.Format.enumRenderer('memberTypes'),
	memberTypeText		: 'Role',
	selectors			: [ 'post' ],
	title				: 'Members',
	
	renderColumns: function() {
		var me = this, columns = me.callParent();
		columns[1].width = 200;
		delete columns[1].flex;
		columns.splice(1, 0, {
			align		: 'left',
			dataIndex	: me.memberTypeField,
			editor		: me.memberTypeEditor,
			flex		: 1,
			itemId		: me.memberTypeField,
			menuDisabled: true,
			renderer	: me.memberTypeRenderer,
			sortable	: me.sortableColumns,
			style		: { textAlign: 'center' },
			text		: me.memberTypeText
		});
		return columns;
	},
	
	privates: {
		
		transformUserRecord: function(record) {
			var me = this, user = me.callParent(arguments);
			if (user) {
				user.name = record.get('name');
				user[me.memberTypeField] = me.memberTypeDefault;
			}
			return user;
		}
	
	}


});