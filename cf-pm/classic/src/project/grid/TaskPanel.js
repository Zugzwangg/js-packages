Ext.define('CFJS.project.grid.TaskPanel', {
	extend			: 'CFJS.grid.BasePanel',
	xtype			: 'project-grid-task-panel',
	mixins			: [ 'CFJS.mixin.PeriodPicker' ],
	actions			: {
		addRecord	: false,
		editRecord	: { title: 'Edit the task', tooltip	: 'Edit the current task' },
		removeRecord: false
	},
	actionsMap		: {
		contextMenu	: { items: [ 'editRecord' ] },
		header		: { items: [ 'periodPicker', 'editRecord', 'clearFilters', 'reloadStore' ] }
	},
	actionsDisable	: [ 'editRecord' ],
	bind			: '{tasks}',
	contextMenu		: { items: new Array(1) },
	datatipTpl		: [
		'<tpl if="author"><b>Author</b>: {author.name}</br></tpl>',
		'<tpl if="name"><b>Name</b>: {name}</br></tpl>',
		'<tpl if="description"><b>Comment</b>: {description}</tpl>'
	],
	header			: { items: new Array(4) },
	iconCls			: CFJS.UI.iconCls.TASKS,
	multiColumnSort	: true,
	title			: 'Tasks',
	selModel		: { type: 'rowmodel', mode: 'SINGLE', pruneRemoved: false },

	getRowClass: function(record, rowIndex, rowParams, store) {
		if (record.get('overdue')) return 'soft-red';
		if (record.get('finished')) return 'soft-green italicFont';
    },

	initColumns: function(config) {
		return CFJS.apply([{
			xtype		: 'templatecolumn',
			align		: 'center',
			dataIndex	: 'priority',
			menuDisabled: true,
			text		: '<span class="' + CFJS.UI.iconCls.FLAG + '"></span>',
			tpl			: '<tpl if="priority != \'NORMAL\'"><span class="{colorCls} ' + CFJS.UI.iconCls.FLAG + '"></span></tpl>',
			width		: 45
		},{
			xtype		: 'templatecolumn',
			align		: 'center',
			dataIndex	: 'overdue',
			menuDisabled: true,
			sortable	: false,
			text		: '<span class="' + CFJS.UI.iconCls.EXCLAMATION_TRIANGLE + '"></span>',
			tpl			: '<tpl if="overdue"><span class="soft-red ' + CFJS.UI.iconCls.EXCLAMATION + '"></span></tpl>',
			width		: 45
		},{
			xtype		: 'templatecolumn',
			align		: 'left',
			dataIndex	: 'author.name',
			filter		: { emptyText: 'author name' },
			style		: { textAlign: 'center' },
			text		: 'Author',
			tpl			: '<tpl if="author">{author.name}</tpl>',
			width		: 250
		},{
			dataIndex	: 'name',
			align		: 'left',
			filter		: { emptyText: 'name' },
			style		: { textAlign: 'center' },
			text		: 'Name',
			width		: 350
		},{
			dataIndex	: 'description',
			align		: 'left',
			filter		: { emptyText: 'comment' },
			style		: { textAlign: 'center' },
			text		: 'Comment',
			width		: 350
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'startDate',
			filter		: true,
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Start at',
			width		: 165
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'endDate',
			filter		: true,
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Deadline at',
			width		: 165
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'finished',
			filter		: true,
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Finished at',
			width		: 165
		},{
			xtype		: 'widgetcolumn',
			align		: 'center',
			filter		: { type: 'number', dataIndex: 'progress', emptyText: 'absolute value' },
			sorter		: 'progress',
			text		: 'Progress',
			widget		: { xtype: 'progressbarwidget', bind: '{record.progress}', textTpl: [ '{percent:number("0.00")}%' ] },
			width		: 120
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'created',
			filter		: true,
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Created',
			width		: 165
		},{
			xtype		: 'datecolumn',
			align		: 'center',
			dataIndex	: 'created',
			filter		: true,
			format		: Ext.Date.patterns.LongDateTime,
			text		: 'Updated',
			width		: 165
		}], config);
	},
	
	initComponent: function() {
		var me = this, plugins = me.plugins = me.plugins || [], plugin;
		if (!(plugin = me.findPlugin('gridfilters'))) plugins.push({ ptype: 'gridfilters' });
		if (!(plugin = me.findPlugin('datatip'))) plugins.push({ ptype: 'datatip', tpl: me.datatipTpl });
		else if (!plugin.tpl) plugin.tpl = me.datatipTpl;
		me.columns = [].concat(me.initColumns(me.columnsConfig));
		me.callParent();
	}

});