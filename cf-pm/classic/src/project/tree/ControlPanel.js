Ext.define('CFJS.project.tree.ControlPanel', {
	extend			: 'Ext.tree.Panel',
	xtype			: 'project-tree-control-panel',
	mixins			: [ 'CFJS.mixin.PeriodPicker' ],
	requires		: [ 'CFJS.store.project.ControlTree' ],
	bind			: { store: '{principles}' },
	emptyText		: 'No data to display',
	enableColumnHide: false,
	enableColumnMove: false,
	iconCls			: CFJS.UI.iconCls.SITEMAP,
	lines			: true,
	loadMask		: true,
	readOnly		: false,
	reserveScrollbar: true,
	rootVisible		: false,
	title			: 'Controls',

	getRowClass: function(record, rowIndex, rowParams, store) {
		if (record.get('overdue')) return 'soft-red';
		if (record.get('finished')) return 'soft-green italicFont';
    },
	
	initComponent: function() {
		var me = this;
		me.viewConfig = Ext.apply(me.viewConfig || {}, {
			deferEmptyText			: false, 
			getRowClass				: me.getRowClass,
			preserveScrollOnReload	: true
		});
		me.columns = [{
			xtype		: 'treecolumn',
			align		: 'left',
			dataIndex	: 'name',
			flex		: 1,
			renderer	: function(value, metaData, record) {
	            metaData.glyph = record.glyph;
	            return value;
	        }
		},{
			xtype		: 'templatecolumn',
			align		: 'center',
			dataIndex	: 'priority',
			tpl			: '<tpl if="priority != \'NORMAL\'"><span class="priority-{colorCls} ' + CFJS.UI.iconCls.FLAG + '"></span></tpl>',
			width		: 40
		},{
			align		: 'center',
			dataIndex	: 'progress',
			renderer: function(value, metaData, record) {
				var progress = record.get('progress');
				metaData.tdCls = 'soft-red';
				if (progress >= .9) metaData.tdCls = 'soft-green';
				else if (progress >= .5) metaData.tdCls = 'soft-orange';
				return Ext.util.Format.percent(value, '0.00');
			},
			width		: 65
		}],
		me.callParent();
	},
	
	setReadOnly: function(readOnly) {
		this.readOnly = readOnly;
	}
	
});