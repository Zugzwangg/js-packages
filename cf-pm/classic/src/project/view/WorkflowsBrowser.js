Ext.define('CFJS.project.view.WorkflowsBrowser', {
	extend				: 'CFJS.panel.HistoryBrowser',
	alternateClassName	: 'CFJS.pages.WorkflowsBrowser',
	xtype				: 'project-view-workflows-browser',
	requires 			: [
		'CFJS.project.panel.WorkflowDesigner',
		'CFJS.project.view.WorkflowDesignerModel',
		'CFJS.project.view.WorkflowsBrowserModel'
	],
	actions				: {
		addRecord		: {
			iconCls	: CFJS.UI.iconCls.ADD,
			title	: 'Add workflow',
			tooltip	: 'Add new workflow',
			handler	: 'onAddRecord'
		},
		removeRecord	: {
			iconCls	: CFJS.UI.iconCls.DELETE,
			title	: 'Remove a workflow',
			tooltip	: 'Remove current workflow',
			handler	: 'onRemoveRecord'
		},
		refreshRecord	: {
			iconCls		: CFJS.UI.iconCls.REFRESH,
			title		: 'Update the data',
			tooltip		: 'Update the data',
			handlerFn	: 'onRefreshRecord'
		},
		saveRecord		: {
			iconCls		: CFJS.UI.iconCls.SAVE,
			title		: 'Save a workflow',
			tooltip		: 'Save current workflow',
			handlerFn	: 'onSaveRecord'
		}
	},
	actionsDisable		: [ 'removeRecord', 'saveRecord', 'refreshRecord' ],
	actionsMap			: {
		contextMenu	: { items: [ 'addRecord', 'removeRecord' ] },
		toolBar		: { items: [ 'addRecord', 'removeRecord', 'tbseparator', 'saveRecord', 'refreshRecord' ] }
	},
	closeTabText		: 'close tab',
	contextMenu			: { items: new Array(2) },
	iconCls				: CFJS.UI.iconCls.SITEMAP,
	newRecordName		: 'Workflow',
	overflowHandler		: 'scroller',
	plain				: true,
	session				: { schema: 'document' },
	tabIdPrefix			: 'workflow',
	tabIdSeparator		: '',
	tabPosition			: 'bottom',
	toolBar				: { hidden: false, items: new Array(5) },
	title				: 'Workflow designer',
	viewModel			: { type: 'project.workflows-browser' },

	beforeTabClose: function(tab) {
		var me = this;
		if (tab && tab.isDirtyRecord()) {
			Ext.fireEvent('confirmlostdata', me.closeTabText, function(btn) { if (btn === 'yes') me.remove(tab); }, me);
			return false;
		}
	},

	confirmRemove: function(workflowName, fn) {
		return Ext.fireEvent('confirmlostdata', 'delete a workflow "'+ workflowName + '"', fn, this);
	},

	createChildTab: function(record) {
		var me = this, tab = me.callParent(arguments);
		if (tab) {
			tab = me.add(Ext.apply(tab, {
				xtype		: 'project-panel-workflow-designer',
				actionable	: me,
				editing		: true,
				name		: 'workflowDesigner' + record.id,
				viewModel	: { type: 'project.workflow-designer', workflow: record },
				workflowId	: me.tabSelector(record.id)
			}));
			tab.lookupViewModel().bind('{_itemName_.name}', function(name) {
				tab.tab.setText(Ext.isNumber(me.tabWidth) ? Ext.util.Format.ellipsis(name, me.tabWidth) : name);
				tab.tab.setTooltip(name);
			});
		}
		return tab;
	},

	createBrowserView: function(cfg) {
		var me = this, store = Ext.Factory.store({ type: 'workflows', autoLoad: true, pageSize: 50 });
		return Ext.apply(cfg, {
			bbar		: [{ xtype: 'pagingtoolbar', displayInfo: true, store: store }],
			iconCls		: CFJS.UI.iconCls.HOME,
			reorderable	: false,
			store		: store,
			viewConfig	: { itemIconCls: me.iconCls }
		})
	},

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.mainTitle = me.title;
		me.callParent();
		me.relayEvents(me.browserView, [ 'itemcontextmenu' ]);
		me.browserView.on({ selectionchange: me.onItemsSelectionChange, scope: me });
	},
	
	lookupScope: function(component) {
		return this.callParent([component && component.workflowDesigner || component]);
	},

	onAddRecord: function(component) {
		var me = this, vm = me.lookupViewModel(), store = me.lookupBrowserStore();
		me.startEdit(vm.createWorkflowModel(Ext.String.format('{0} {1}', me.newRecordName, (store && store.isStore && store.getTotalCount() || 0) + 1)));
	},

	onItemsSelectionChange: function(selectable, selection) {
		var me = this, actions = me.actions, disabled;
		if (actions) {
			if (Ext.isObject(selection)) selection = selection.selectedRecords;
			disabled = !selection || selection.length !== 1;
			me.disableComponent(actions.removeRecord, disabled);
			me.disableComponent(actions.refreshRecord, true);
			me.disableComponent(actions.saveRecord, true);
		}
	},
	
	onRemoveRecord: function(component) {
		var me = this, tab = me.getActiveTab(), selection, record;
		if (tab && tab.isHome) {
			selection = tab.getSelection();
			record = selection && selection.length > 0 && selection[0];
		} else if (tab && tab.editing) {
			record = tab.lookupViewModel().getWorkflow();
		}
		if (record && record.isModel) {
			me.confirmRemove(record.get('name'), function(btn) {
				if (btn === 'yes') {
					me.disableComponent(component, true);
					record.erase({
						callback: function(record, operation, success) {
							me.disableComponent(component);
							if (tab.editing) me.remove(tab);
							else me.remove(me.itemTab(record.getId()));
							me.reloadStore();
						}
					});
				}
			});
		}
	},
	
	onTabChange: function(tabPanel, newCard, oldCard) {
		var me = this, searchFilter = me.searchFilter();
		if (searchFilter) searchFilter.setHidden(newCard.editing);
		if (newCard.isHome) {
			me.setTitle(me.mainTitle);
			me.onItemsSelectionChange(newCard, newCard.getSelectionModel().getSelection());
		} else {
			me.setTitle([me.mainTitle, newCard.title].join(' / '));
			if (!newCard.initializing) me.executeChildAction('initActionable', [me]);
		}
		me.redirectTo(me.cardRoute(newCard));
	},
	
	privates: {

    	cardRoute: function(card) {
    		var me = this, vm = card && card.lookupViewModel(),
    			workflow = vm && Ext.isFunction(vm.getWorkflow) && vm.getWorkflow();
    		return workflow && workflow.id ? me.tabHashToken(workflow.id) : me.routeId;
    	},
	
		itemTab: function(id) {
			return this.child('[workflowId="' + this.tabSelector(id) + '"]');
		}
    	
	}
	
});