Ext.define('CFJS.project.view.TaskStatusView', {
	extend		: 'Ext.panel.Panel',
	xtype		: 'project-view-task-statuses',
	requires	: [
		'Ext.view.View',
		'Ext.layout.container.Border',
		'CFJS.project.panel.TaskStatusEditor',
		'CFJS.project.view.TaskStatusViewController',
		'CFJS.project.view.TaskStatusViewModel'
	],
	cls			: 'task-status-view',
	controller	: 'project.task.status',
	iconCls		: CFJS.UI.iconCls.TASKS,
	layout		: 'border',
	statusLabel	: 'Status',
	title		: 'Execution',
	viewModel	: { type: 'project.task.status' },
	
	createStatusEditor: function(config) {
		var me = this,
			editor = CFJS.apply({
				xtype		: 'project-panel-task-status-editor',
				bind		: { collapsed: '{completed}', hidden: '{itemRules.isViewer}' },
				cls			: 'shadow-panel',
				collapsible	: true,
				margin		: '0 10 10',
				name		: 'statusEditor',
				region		: 'south'
			}, config);
		return editor;
	},
	
	createTimeLine: function(config) {
		var me = this, vm = me.lookupViewModel(),
			timeLine = CFJS.apply({
				xtype			: 'dataview',
				addEmptyText	: Ext.emptyFn,
				bind			: '{statuses}',
				cls				: 'task-status-items-wrap shadow-panel',
				collapsible		: false,
				disableSelection: true,
				flex			: 1,
				itemSelector	: '.task-status-item',
				itemTpl			: [
					'<div class="task-status-item{id:this.cls(xindex-1,xcount)}">',
						'<div class="profile-pic-wrap">' +
							'<tpl if="{author}"><img src="{author.photoUrl}" alt="User photo"></tpl>',
							'<div>{created:date("' + Ext.Date.patterns.ISO8601Long + '")}</div>',
						'</div>',
		                '<div class="line-wrap">',
		                	'<div class="contents-wrap">',
		                		'<div class="shared-by">',
		                			'<span class="author">{author.name}</span>',
		                			'<tpl if="involvement">',
		                				'<span class="x-fa fa-long-arrow-right"></span>',
		                				'<span class="involvement">{[values.involvement.split(",").join(", ")]}</span>',
		                			'</tpl>',
		                			'<tpl if="progress === 0 || progress &gt; 0">',
		                				'<div class="status">' + me.statusLabel + ': {progress:this.progress(values)}</div>',
		                			'</tpl>',
		                		'</div>',
		                		'<div class="description">{description}</div>',
			                '</div>',
		                '</div>',
					'</div>',
					{
						cls: function (id, index, count) {
							var cls = '';
							if (!index) cls += ' task-status-item-first';
							if (index + 1 === count) cls += ' task-status-item-last';
							return cls;
						},
						description: function(description, data) {
							return description || '<div style="height:1em"></div>';
						},
						progress: function(progress, data) {
							if (data.numerical === true) {
								progress = progress > 0 && data.goal > 0? progress/data.goal : 0;
								var cls = 'soft-red';
								if (progress >= .9) cls = 'soft-green';
								else if (progress >= .5) cls = 'soft-orange';
								return '<span class="' + cls + '">' + Ext.util.Format.percent(progress, '0.00') + '</span>';
							}
							return '<span class="boolean' + (progress === 0 ? ' soft-red' : '') + '">' + (vm.statusText(progress) || progress) + '</span>';
						}
			        }
				],
				margin			: '5 10 10',
				name			: 'timeLine',
				region			: 'center',
				scrollable		: 'y'
			}, config);
		return timeLine;
	},
	
	initComponent: function() {
		var me = this;
		me.items = [ me.ensureChild('timeLine'), me.ensureChild('statusEditor') ];
		me.callParent();
	},
	
	lookupStatusEditor: function() {
		return this.lookupChild('statusEditor');
	},
	
	lookupTimeLine: function() {
		return this.lookupChild('timeLine');
	},

	setReadOnly: function(readOnly) {
		this.hideComponent(this.lookupStatusEditor(), readOnly);
	}

});