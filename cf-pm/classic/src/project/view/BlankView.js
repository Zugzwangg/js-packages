Ext.define('CFJS.project.view.BlankView', {
	extend		: 'Ext.container.Container',
	xtype		: 'project-view-blank-page',
	bodyText	: 'Select an item from the control tree to continue.',
	cls			: Ext.baseCSSPrefix + 'project-blank-page',
	headerText	: 'No data to display!',

	initComponent: function() {
		var me = this;
		me.anchor = '100% -1';
		me.layout = { type: 'vbox', pack: 'center', align: 'center' };
		me.items = { xtype: 'box', cls: me.cls + '-container', html: me.renderHtml() }
		me.callParent();
	},
	
	renderHtml: function() {
		return [
			'<div class="fa-outer-class"><span class="' + CFJS.UI.iconCls.SITEMAP + '"></span></div>',
		   	'<h1>', this.headerText, '</h1>',
		   	'<span class="' + this.cls + '-text">', this.bodyText, '</span>'
		].join('');
	}
});
