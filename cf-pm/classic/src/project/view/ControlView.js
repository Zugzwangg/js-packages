Ext.define('CFJS.project.view.ControlView', {
	extend				: 'Ext.form.Panel',
	alternateClassName	: 'CFJS.pages.ControlView',
	xtype				: 'project-view-controls',
	requires			: [
		'CFJS.project.tree.ControlPanel',
		'CFJS.project.view.ProcessElementView',
		'CFJS.project.view.ControlViewController',
		'CFJS.project.view.ControlViewModel'
	],
	actions				: {
		addRecord	: {
			bind	: { hidden: '{!itemRules.canAdd}' },
			iconCls	: CFJS.UI.iconCls.ADD,
			title	: 'Add item',
			tooltip	: 'Add new item',
			handler	: 'onAddRecord'
		},
		filters		: {
			iconCls	: 'x-fa fa-eye',
			menu	: [{
				checked		: false,
				checkHandler: 'onVisibleFilterChange',
				htype		: 'showHidden',
				text		: 'Hidden',
			},{
				checked		: true,
				checkHandler: 'onVisibleFilterChange',
				htype		: 'showVisible',
				text		: 'Visible'
			}],
			title	: 'Show items',
			tooltip	: 'Show items'
		},
		periodPicker: {
			iconCls	: CFJS.UI.iconCls.CALENDAR,
			tooltip	: 'Set the working period',
			handler	: 'onPeriodPicker'
		},
		reloadStore	: {
			iconCls	: CFJS.UI.iconCls.REFRESH,
			tooltip	: 'Update the data',
			handler	: 'onReloadStore'
		},
		saveRecord	: {
			bind	: { hidden: '{!itemRules.editable}' },
			fomBind	: true,
			handler	: 'onSaveChanges',
			iconCls	: CFJS.UI.iconCls.SAVE,
			tooltip	: 'Save the changes'
		}
	},
	actionsMap			: {
		contextMenu	: { items: [ 'addRecord', 'filters' ] },
		header		: { items: [ 'periodPicker', 'addRecord', 'saveRecord', 'filters', 'reloadStore' ] }
	},
	contextMenu	: { items: new Array(2) },
	controller	: 'project.control-view',
	defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([readOnly])',
	header		: CFJS.UI.buildHeader({ items: new Array(5) }),
	iconCls		: CFJS.UI.iconCls.SITEMAP,
	layout		: 'border',
	readOnly	: false,
	session		: { schema: 'project' },
	title		: 'Control view',
	viewModel	: { type: 'project.control-view'},
	waitMsg		: 'Wait until the plan of the process will not be created...',

	destroy: function() {
		var me = this;
		if (me.rendered && !me.destroyed) me.startEditRecord(false);
		me.callParent(arguments);
	},
    
	createAddRecordMenu: function(types) {
		var me = this, actions = me.getActions(), 
			addRecord = actions && actions.addRecord,
			scope = me.lookupScope(me), items = [];
		if (addRecord.isAction) addRecord = addRecord.initialConfig;
		if (!addRecord.principleType && addRecord.menu !== false) {
			if (types && types.isStore) {
				types.each(function(record) {
					var data = record.data;
					items.push({
						bind			: { hidden: '{!itemRules.canAdd' + data.id + '}' },
						disabled		: data.disabled,
						handler			: addRecord.handler,
						iconCls 		: data.iconCls,
						principleType	: data.id,
						scope			: scope,
						text			: data.name
					});
				});
			}
			delete addRecord.handler;
			addRecord.menu = { items: items };
			actions.addRecord.callEach('setMenu', [addRecord.menu]);
			actions.addRecord.setDisabled(items.length === 0);
		}
		return addRecord.menu;
	},

	createControlTree: function(config) {
		var me = this,
			controlTree = CFJS.apply({
				xtype		: 'project-tree-control-panel',
				collapsible	: true,
				floatable	: false,
				iconCls		: null,
				listeners	: { selectionchange: 'onTreeSelectionChange' },
				minWidth	: 200,
				name		: 'controlTree',
				region		:'west',
				split		: true,
				width		: 350
			}, config);
		return controlTree;
	},

	createElementView: function(config) {
		var me = this,
			elementView = CFJS.apply({
				xtype		: 'project-view-process-element',
				collapsible	: false,
				name		: 'elementView',
				region		: 'center'
			}, config);
		return elementView;
	},
	
	createWorkflowPicker: function(config) {
		var me = this,
			field = {
				xtype		: 'dictionarycombo',
				emptyText	: 'select a workflow',
				flex		: 1,
				fieldLabel	: 'Select workflow',
				name		: 'workflow',
				queryParam	: 'name',
				required	: true,
				store		: { type: 'named', proxy: { loaderConfig: { dictionaryType: 'workflow' } } }
			},
			applyBtn = {
				xtype		: 'button',
				formBind	: true,
				iconCls		: CFJS.UI.iconCls.APPLY,
				text		: 'Select',
				minWidth	: 100,
				reference	: 'applyButton'
			},
			emptyBtn = {
				xtype		: 'button',
				iconCls		: CFJS.UI.iconCls.CODE_FORK,
				text		: 'Empty process',
				minWidth	: 100,
				reference	: 'emptyButton'
			},
			picker = CFJS.apply({
				xtype			: 'window',
				closeAction		: 'hide',
				defaultButton	: applyBtn.reference,
				defaultFocus	: field.xtype,
				iconCls			: CFJS.UI.iconCls.CODE_FORK,
				items			: {
					xtype			: 'form',
					defaultFocus	: field.xtype,
					defaultType		: field.xtype,
					fieldDefaults	: { labelAlign: 'top', labelWidth: 100, msgTarget: Ext.supports.Touch ? 'side' : 'qtip' },
					layout			: 'hbox',
					bodyPadding		: 10,
					buttons			: [ applyBtn, emptyBtn ],
					style			: { overflow: 'hidden' },
					items			: [ field ]
				},
				layout			: 'fit',
				modal			: true,
				minHeight		: 200,
				minWidth		: 450,
				referenceHolder	: true,
				title			: 'Workflow selection'
			}, config);
		picker = Ext.create(picker);
		field = picker.down(field.xtype);
		picker.lookup(applyBtn.reference).setHandler(function() {
			var editor = me.lookupEditor(), vm = editor && editor.lookupViewModel();
			if (vm && vm.isPrincipleEdit) {
				me.mask(me.waitMsg);
				vm.createProcessPlan(field && field.getValue(), null, function() {
					me.lookupEditor().lookupElementDesigner().lookupPlanDesigner().onReloadStore();
					me.unmask();
				});
			}
			picker.close();
		});
		picker.lookup(emptyBtn.reference).setHandler(function() {
			picker.close();
		});
		return picker;
	},
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.createAddRecordMenu(vm && vm.getStore('principleTypes'));
		me.items = [ me.ensureChild('controlTree'), me.ensureChild('elementView') ];
		me.callParent();
		me.lookupTree().contextMenu = me.contextMenu;
	},
	
	lookupEditor: function() {
		return this.lookupChild('elementView');
	},
	
	lookupTree: function() {
		return this.lookupChild('controlTree');
	},

	showWorkflowPicker: function(record) {
		var me = this, picker;
		if (me.rendered && !me.disabled) {
			picker = me.ensureChild('workflowPicker');
			picker.show();
		}
		return me;
	},
	
	startEditRecord: function(record) {
		var tree = this.lookupTree(), 
			selModel = tree && Ext.isFunction(tree.getSelectionModel) && tree.getSelectionModel();
		if (selModel) {
			if(record) selModel.select(record);
			else selModel.deselectAll();
		} 
	}

});