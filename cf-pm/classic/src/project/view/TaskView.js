Ext.define('CFJS.project.view.TaskView', {
	extend				: 'Ext.panel.Panel',
	alternateClassName	: 'CFJS.pages.TasksView',
	xtype				: 'project-view-tasks',
	requires			: [
		'CFJS.project.grid.TaskPanel',
		'CFJS.project.tab.TaskDesigner',
		'CFJS.project.view.TasksViewController',
		'CFJS.project.view.TasksViewModel'
	],
	controller	: 'project.tasks',
	header		: false,
	iconCls		: CFJS.UI.iconCls.TASKS,
	layout		: { type: 'card', anchor: '100%' },
	session		: { schema: 'project' },
	title		: 'Tasks',
	viewModel	: { type: 'project.tasks' },
	
	addTasksFilters: function(filters) {
		this.lookupViewModel().addTasksFilters(filters);
	},
	
	createGrid: function(config) {
		return CFJS.apply({ xtype: 'project-grid-task-panel' }, config);
	},
	
	createTaskDesigner: function(config) {
		var me = this, actions, vm,
			designer = CFJS.apply({
				xtype		: 'project-tab-task-designer',
				actions		: {
					back		: {
						iconCls	: CFJS.UI.iconCls.BACK,
						tooltip	: 'Back to the tasks list',
						handler	: 'onBackClick'
					},
					refreshRecord	: {
						iconCls	: CFJS.UI.iconCls.REFRESH,
						tooltip	: 'Update the data',
						handler	: 'onRefreshRecord'
					},
					saveRecord	: {
						hidden	: true,
						iconCls	: CFJS.UI.iconCls.SAVE,
						tooltip	: 'Save the changes',
						handler	: 'onSaveRecord'
					}
				},
				actionsMap	: {
					header: { items: [ 'back', 'tbspacer', 'saveRecord', 'refreshRecord' ] }
				},
				activeTab	: 1,
				bind		: { title: '{itemTitle}' },
				header		: CFJS.UI.buildHeader({ padding: '5 10', items: new Array(4), titlePosition: 1 }),
				tabBar		: { margin: 0 },
				tabIconsOnly:  me.tabIconsOnly,
				name		: 'taskDesigner',
				viewModel	: { type: 'project.control-edit', itemModel: 'CFJS.model.project.Task' }
			}, config);
		designer = me.add(designer);
		actions = designer.getActions();
		vm = designer.lookupViewModel();
		designer.on({
			tabchange: function(panel, card, previous) {
				me.hideComponent(actions.saveRecord, !card || card.hideSaveBtn || !vm.get('itemRules.editable'));
			}
		});
		vm.bind('{itemRules.editable}', function(editable) {
			var card = designer.getActiveTab();
			me.hideComponent(actions.saveRecord, !card || card.hideSaveBtn || !editable);
		});
		return designer;
	},
	
	initItems: function() {
		var me = this;
		if (me.items && !me.items.isMixedCollection) me.items = null;
		me.callParent();
		if (!me.grid || !me.grid.isPanel) {
			me.grid = me.add(me.ensureChild('grid'));
			me.grid.on({ itemdblclick: 'onGridDblClick' });
		}
	},

	lookupEditor: function() {
		return this.ensureChild('taskDesigner');
	},
	
	lookupGrid: function() {
		return this.grid;
	},

	lookupStore: function() {
		var grid = this.lookupGrid();
		return grid ? grid.getStore() : null;
	}

});