Ext.define('CFJS.project.view.ProcessElementView', {
	extend		: 'Ext.container.Container',
	xtype		: 'project-view-process-element',
	requires	: [
		'CFJS.project.tab.ProcessElementDesigner',
		'CFJS.project.tab.TaskDesigner',
		'CFJS.project.view.BlankView',
		'CFJS.project.view.PrincipleViewModel'
	],
	layout		: 'card',
	viewModel	: { type: 'project.control-edit' },

	createEmptyBox: function(config) {
		var me = this,
			emptyPanel = CFJS.apply({
				xtype		: 'project-view-blank-page',
				name		: 'emptyBox',
			}, config);
		return Ext.create(emptyPanel);
	},
	
	createElementDesigner: function(config) {
		var me = this,
			designer = CFJS.apply({
				xtype	: 'project-tab-process-element-designer',
				bind	: { iconCls: '{itemIconCls}', title: '{itemTitle}' },
				name	: 'elementDesigner'
			}, config);
		return designer;
	},

	createTaskDesigner: function(config) {
		var me = this,
			designer = CFJS.apply({
				xtype		: 'project-tab-task-designer',
				bind		: { iconCls: '{itemIconCls}', title: '{itemTitle}' },
				name		: 'taskDesigner',
				viewModel	: false
			}, config);
		return designer;
	},

	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.items = [ me.ensureChild('elementDesigner'), me.ensureChild('taskDesigner'), me.ensureChild('emptyBox') ];
		me.activeItem = me.items.length - 1;
		me.callParent();
		if (vm && vm.isViewModel) vm.bind('{itemRules}', function(itemRules) {
			me.setActiveItem(me.lookupChild(itemRules.editorActiveItem || 'emptyBox'));
		});
	},
	
	lookupElementDesigner: function() {
		return this.lookupChild('elementDesigner');
	},
	
	lookupTaskDesigner: function() {
		return this.lookupChild('taskDesigner');
	}
	
});