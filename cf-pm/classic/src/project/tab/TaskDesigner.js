Ext.define('CFJS.project.tab.TaskDesigner', {
	extend				: 'Ext.tab.Panel',
	xtype				: 'project-tab-task-designer',
	requires			: [
		'CFJS.grid.SimpleResourceBrowser',
		'CFJS.project.panel.TaskEditor',
		'CFJS.project.view.PrincipleViewModel',
		'CFJS.project.view.TaskStatusView'
	],
	removePanelHeader	: false,
	tabBarHeaderPosition: 2,

	createTaskEditor: function(config) {
		var me = this,
			editor = CFJS.apply({
				xtype	: 'project-panel-task-editor',
				header	: false,
				name	: 'taskEditor'
			}, config);
		return editor;
	},
	
	createTaskResourcesBrowser: function(config) {
		var me = this, api = CFJS.Api,
			browser = CFJS.apply({
				xtype		: 'grid-simple-resource-browser',
				bind		: { readOnly: '{!itemRules.editResources}', store: '{principleResources}' },
				hideSaveBtn	: true,
				iconCls		: CFJS.UI.iconCls.ATTACH,
				margin		: 10,
				name		: 'taskResourcesBrowser',
				parentField	: 'principleId',
				service		: Ext.apply(Ext.clone(api.controller(api.getService())), { method: 'principle'}), 
				title		: 'Attachments',
				userCls		: 'shadow-panel'
			}, config);
		return browser;
	},

	createTaskStatusView: function(config) {
		var me = this,
			view = CFJS.apply({
				xtype		: 'project-view-task-statuses',
				header		: false,
				hideSaveBtn	: true,
				name		: 'taskStatusView'
			}, config);
		return view;
	},
	
	initComponent: function() {
		var me = this;
		me.items = [ me.ensureChild('taskEditor'), me.ensureChild('taskStatusView'), me.ensureChild('taskResourcesBrowser') ];
		me.callParent();
		if (me.header && !me.header.isHeader) delete me.header.itemPosition;
	},
	
	lookupTaskEditor: function() {
		return this.lookupChild('taskEditor');
	},

	lookupTaskResourcesBrowser: function() {
		return this.lookupChild('taskResourcesBrowser');
	},
	
	lookupTaskStatusView: function() {
		return this.lookupChild('taskStatusView');
	}

});