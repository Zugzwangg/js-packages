Ext.define('CFJS.project.tab.ProcessElementDesigner', {
	extend				: 'Ext.tab.Panel',
	xtype				: 'project-tab-process-element-designer',
	requires			: [
		'CFJS.grid.SimpleResourceBrowser',
		'CFJS.project.panel.ProcessElementEditor',
		'CFJS.project.panel.ProcessPlanDesigner',
		'CFJS.project.view.TaskView'
	],
	removePanelHeader	: false,
	tabBarHeaderPosition: 1,

	createPlanDesigner: function(config) {
		var me = this,
			designer = CFJS.apply({
				xtype	: 'project-panel-process-plan-designer',
				name	: 'planDesigner'
			}, config);
		return designer;
	},
	
	createProcessElementEditor: function(config) {
		var me = this,
			editor = CFJS.apply({
				xtype	: 'project-panel-process-element-editor',
				name	: 'processElementEditor'
			}, config);
		return editor;
	},
	
	createProcessTaskView: function(config) {
		var me = this, vm = me.lookupViewModel(),
			view = CFJS.apply({
				xtype		: 'project-view-tasks',
				gridConfig	: {
					actions		: { periodPicker: false },
					actionsMap	: { header: { items: [ 'editRecord', 'clearFilters', 'reloadStore' ] } }
				},
				tabIconsOnly:  me.tabIconsOnly,
				name		: 'processTaskView',
				session		: false,
				viewModel	: { itemName: 'editControlTask', parent: vm.getParent() || CFJS.lookupViewportViewModel(), workingPeriod: null }
			}, config);
		return view;
	},
	
	createResourcesBrowser: function(config) {
		var me = this, api = CFJS.Api,
			browser = CFJS.apply({
				xtype		: 'grid-simple-resource-browser',
				bind		: { readOnly: '{!itemRules.editResources}', store: '{principleResources}' },
				iconCls		: CFJS.UI.iconCls.ATTACH,
				margin		: 10,
				name		: 'resourcesBrowser',
				parentField	: 'principleId',
				service		: Ext.apply(Ext.clone(api.controller(api.getService())), { method: 'principle'}), 
				title		: 'Attachments',
				userCls		: 'shadow-panel'
			}, config);
		return browser;
	},
	
	initComponent: function() {
		var me = this, vm = me.lookupViewModel();
		me.items = [ me.ensureChild('processElementEditor'), me.ensureChild('planDesigner'), me.ensureChild('processTaskView'), me.ensureChild('resourcesBrowser') ];
		me.callParent();
		if (vm && vm.isViewModel) vm.bind('{itemRules}', me.onItemRulesChange, me);
	},
	
	lookupPlanDesigner: function() {
		return this.lookupChild('planDesigner');
	},

	lookupProcessElementEditor: function() {
		return this.lookupChild('processElementEditor');
	},
	
	lookupProcessTaskView: function() {
		return this.lookupChild('processTaskView');
	},
	
	lookupResourcesBrowser: function() {
		return this.lookupChild('resourcesBrowser');
	},
	
	onItemRulesChange: function(itemRules) {
		if (!itemRules.isTask) {
			var me = this,
				item = me.lookupViewModel().getItem(),
				designer = me.lookupPlanDesigner(),
				view = me.lookupProcessTaskView(),
				activeTab = me.getActiveTab();
			me.hideChild(designer, !itemRules.isProcess);
			me.hideChild(designer.tab, !itemRules.isProcess);
			if (view && item) view.addTasksFilters({ id: 'parent', property: 'parent.id', type: 'child', operator: 'in', value: item.id });
			me.setActiveTab(activeTab === designer && designer.isHidden() ? 0 : activeTab);
		}
	}

});