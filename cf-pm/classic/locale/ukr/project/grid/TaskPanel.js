Ext.define('CFJS.locale.ukr.project.grid.TaskPanel', {
    override		: 'CFJS.project.grid.TaskPanel',
	config			: {
		actions		: {
			editRecord	: { title: 'Редагувати задачу', tooltip: 'Редагувати поточну задачу' }
		},
		datatipTpl	: [
			'<tpl if="author"><b>Автор</b>: {author.name}</br></tpl>',
			'<tpl if="name"><b>Назва</b>: {name}</br></tpl>',
			'<tpl if="description"><b>Коментар</b>: {description}</tpl>'
		],
		title		: 'Завдання'
	},
	columnsConfig	: [{},{},
		{ filter: { emptyText: 'ім\'я автора' }, text: 'Автор' },
		{ filter: { emptyText: 'назва' }, text: 'Назва' },
		{ filter: { emptyText: 'коментар' }, text: 'Коментар' },
		{ text: 'Розпочато' },
		{ text: 'Виконати до' },
		{ text: 'Завершено' },
		{ filter: { emptyText: 'абсолютна величина' }, text: 'Виконання' },
		{ text: 'Створено' },
		{ text: 'Оновлено' }
	],
	emptyText	: 'Дані для відображення відсутні'
});