Ext.define('CFJS.locale.ukr.project.view.ControlView', {
	override			: 'CFJS.project.view.ControlView',
	config				: {
		actions	: {
			addRecord	: { title: 'Додати елемент', tooltip: 'Додати новий елемент' },
			filters		: {
				menu	: [{
					checked		: false,
					checkHandler: 'onVisibleFilterChange',
					htype		: 'showHidden',
					text		: 'Виконані',
				},{
					checked		: true,
					checkHandler: 'onVisibleFilterChange',
					htype		: 'showVisible',
					text		: 'В роботі'
				}],
				title	: 'Показати елементи',
				tooltip	: 'Показати елементи'
			},
			periodPicker: { tooltip: 'Вибір робочого періоду' },
			reloadStore	: { tooltip: 'Оновити дані' },
			saveRecord	: { tooltip: 'Записати внесені зміни' },
		},
		title	: 'Дерево управління'
	},
	workflowPickerConfig: {
		items: {
			buttons	: [{ text: 'Вибрати' }, { text: 'Пустий процес' }],
			items	: [{ emptyText: 'виберіть робочий процес', fieldLabel: 'Виберіть робочий процес' }]
		},
		title: 'Вибір робочого процесу'
	}
});