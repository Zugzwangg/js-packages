Ext.define('CFJS.locale.ukr.project.view.TaskStatusView', {
	override	: 'CFJS.project.view.TaskStatusView',
	config		: { title: 'Виконання' },
	statusLabel	: 'Статус'
});