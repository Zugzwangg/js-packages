Ext.define('CFJS.locale.ukr.project.view.BlankView', {
	override	: 'CFJS.project.view.BlankView',
	bodyText	: 'Виберіть елемент з дерева керування для продовження.',
	headerText	: 'Дані для відображення відсутні!'
});