Ext.define('CFJS.locale.ukr.pages.WorkflowsBrowser', {
	override: 'CFJS.pages.WorkflowsBrowser',
	config	: {
		actions			: {
			addRecord		: {	title: 'Додати робочий процес',		tooltip	: 'Додати робочий процес' },
			removeRecord	: {	title: 'Видалити робочий процес',	tooltip	: 'Видалити робочий процес'	},
			refreshRecord	: { title: 'Оновити дані',				tooltip : 'Оновити дані' },
			saveRecord		: { title: 'Зберегти робочий процес',	tooltip : 'Зберегти робочий процес'	}
		},
		closeTabText	: 'закрити вкладку',
		newRecordName	: 'Робочий процес',
		title			: 'Дизайнер робочого процесу'
	},
	confirmRemove: function(formName, fn) {
		return Ext.fireEvent('confirmlostdata', 'видалити робочий процес "'+ formName + '"', fn, this);
	}
});
