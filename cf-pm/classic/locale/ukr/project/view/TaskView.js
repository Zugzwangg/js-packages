Ext.define('CFJS.locale.ukr.project.view.TaskView', {
	override			: 'CFJS.project.view.TaskView',
	config				: { title: 'Завдання' },
	taskDesignerConfig	: {
		actions: {
			back			: { tooltip: 'Назад до списку завдань' },
			refreshRecord	: {	tooltip: 'Оновити дані' },
			saveRecord		: { tooltip: 'Записати внесені зміни' }
		}
	}
});