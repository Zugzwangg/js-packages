Ext.define('CFJS.locale.ukr.project.tree.ControlPanel', {
	override	: 'CFJS.project.tree.ControlPanel',
	config		: { title: 'Елементи керування' },
	emptyText	: 'Дані для відображення відсутні'
});