Ext.define('CFJS.locale.ukr.project.panel.ProcessPlanDesigner', {
	override			: 'CFJS.project.panel.ProcessPlanDesigner',
	config				: { title: 'План' },
	explorerConfig		: {
		actions	: {
			addRecord	: { title: 'Додати завдання', tooltip: 'Додати нове завдання' },
			refreshStore: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeRecord: { title: 'Видалити завдання', tooltip: 'Видалити поточне завдання' },
			saveRecord	: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' },
			startProcess: { title: 'Розпочати процес', tooltip: 'Розпочати виконання процесу' }
		},
		header	: { title: 'Завдання' },
		items	: [{ emptyText: 'виберіть початкове завдання', fieldLabel: 'Поточне завдання' }]
	},
	explorerGridConfig	: {
		columns		: [{ text: 'Назва' }],
		plugins		: [{
			tpl: [
				'<tpl if="name"><b>Назва</b>: {name}</br></tpl>',
				'<tpl if="description"><b>Коментар</b>: {description}</tpl>'
			]
		}],
		viewConfig	: { emptyText: 'Дані для відображення відсутні' }
	},
	newRecordName		: 'Нове завдання',
	startProcessMsg		: 'Починаю процес...'
});