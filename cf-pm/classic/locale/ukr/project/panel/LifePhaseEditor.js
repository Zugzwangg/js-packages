Ext.define('CFJS.locale.ukr.project.panel.LifePhaseEditor', {
	override	: 'CFJS.project.panel.LifePhaseEditor',
	itemsConfig	: [{
		items: [
			{ emptyText: 'введіть назву', fieldLabel: 'Назва фази' },
			{ emptyText: 'виберіть тип', fieldLabel: 'Тип фази' },
			{ fieldLabel: 'Тривалість (дні)' }
		]
	},{
		emptyText	: 'введіть опис',
		fieldLabel	: 'Опис',
	},{
		nameText	: 'Умова переходу',
		title		: 'Переходи',
		valueEditor	: { emptyText: 'виберіть фазу' },
		valueText	: 'Життєва фаза'
	},{
		title		: 'Учасники'
	}]
});
