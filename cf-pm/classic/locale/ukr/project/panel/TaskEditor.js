Ext.define('CFJS.locale.ukr.project.panel.TaskEditor', {
	override: 'CFJS.project.panel.TaskEditor',
	createItems: function() {
		var me = this, items = me.itemsConfig;
		items[1].items.splice(2, 0, { emptyText: 'оберіть фазу', fieldLabel: 'Фаза' });
		items.splice(3, 0, { fieldLabel: 'Мета', items: [{ boxLabel: 'Чисельна' },{ emptyText: 'введіть чисельну мету' },{ boxLabel: 'Зі звітом' }] });
		return me.callParent();
	}
});