Ext.define('CFJS.locale.ukr.project.panel.ProcessPlanEditor', {
	override			: 'CFJS.project.panel.ProcessPlanEditor',
	itemsConfig: [{
		items: [
			{ emptyText: 'введіть назву завдання', fieldLabel: 'Назва' },
			{ emptyText: 'оберіть фазу', fieldLabel: 'Фаза' } ,
			{ fieldLabel: 'Тривалість (дні)' }
		]
	},{
		emptyText	: 'введіть коментар',
		fieldLabel	: 'Коментар',
	},{
		nameText	: 'Умова переходу',
		title		: 'Переходи',
		valueEditor	: { emptyText: 'виберіть завдання' },
		valueText	: 'Завдання'
	},{
		title		: 'Учасники'
	}]
});