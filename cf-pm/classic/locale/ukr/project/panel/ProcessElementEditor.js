Ext.define('CFJS.locale.ukr.project.panel.ProcessElementEditor', {
	override		: 'CFJS.project.panel.ProcessElementEditor',
	config			: { title: 'Основне'},
	itemsConfig		: [{
		items: [{ fieldLabel: 'Код' },{ fieldLabel: 'Тип' },{ fieldLabel: 'Автор' },{ fieldLabel: 'Створено' },{ fieldLabel: 'Оновлено' }]
	},{
		items: [{ emptyText: 'введіть назву елемента', fieldLabel: 'Назва' },{ fieldLabel: 'Шлях в дереві' },{ text: 'Пріоритет', tooltip: 'Пріоритет елемента' }]
	},{
		emptyText: 'введіть опис елемента', fieldLabel: 'Опис'
	},{
		items: [{ fieldLabel: 'Розпочато' },{ fieldLabel: 'Виконати до' },{ fieldLabel: 'Завершено' },{ text: 'Завершити', tooltip: 'Завершити процес' }]
	}],
	progressLabel	: 'Стан виконання'
});