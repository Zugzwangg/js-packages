Ext.define('CFJS.locale.ukr.project.panel.WorkflowEditor', {
	override			: 'CFJS.project.panel.WorkflowEditor',
	config				: { newRecordName: 'Життєва фаза', title: 'Основне' },
	fieldsConfig		: [{ fieldLabel: 'Код' },{ emptyText: 'введіть назву', fieldLabel: 'Назва' }],
	phaseExplorerConfig	: {
		actions		: {
			addRecord	: { title: 'Додати фазу', tooltip: 'Додати нову фазу' },
			refreshStore: { title: 'Оновити дані', tooltip: 'Оновити дані' },
			removeRecord: { title: 'Видалити фазу', tooltip: 'Видалити поточну фазу' },
			saveRecord	: { title: 'Записати зміни', tooltip: 'Записати внесені зміни' }
		},
		columns		: [{ text: 'Назва' }],
		header		: { title: 'Життєві фази' },
		plugins		: [{
			tpl: [
				'<tpl if="name"><b>Назва</b>: {name}</br></tpl>',
				'<tpl if="description"><b>Опис</b>: {description}</tpl>'
			]
		}],
		viewConfig	: { emptyText: 'Дані для відображення відсутні' }
	}
});
