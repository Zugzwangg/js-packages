Ext.define('CFJS.locale.ukr.project.panel.WorkflowDesigner', {
	override: 'CFJS.project.panel.WorkflowDesigner',
	config	: {
		title			: 'Редактор робочого процесу',
		refreshDataText	: 'перезавантажити дані робочого процесу'
	}
});
