Ext.define('CFJS.locale.ukr.project.panel.TaskStatusEditor', {
	override		: 'CFJS.project.panel.TaskStatusEditor',
	config			: { title: 'Статус' },
	itemsConfig		: [{ emptyText: 'виберіть співробітників для участі', fieldLabel: 'Потрібна участь' },{ emptyText: 'введіть коментар', fieldLabel: 'Коментар' }],
	progressLabel	: 'Виконання',
	sendMessageText	: 'Надіслати повідомлення'
});