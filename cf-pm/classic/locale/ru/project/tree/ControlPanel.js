Ext.define('CFJS.locale.ru.project.tree.ControlPanel', {
	override	: 'CFJS.project.tree.ControlPanel',
	config		: { title: 'Элементы управления' },
	emptyText	: 'Нет данных для отображения'
});