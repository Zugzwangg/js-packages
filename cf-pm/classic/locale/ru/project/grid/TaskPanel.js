Ext.define('CFJS.locale.ru.project.grid.TaskPanel', {
    override		: 'CFJS.project.grid.TaskPanel',
	config			: {
		actions		: {
			editRecord	: { title: 'Редактировать задачу', tooltip: 'Редактировать текущую задачу' }
		},
		datatipTpl	: [
			'<tpl if="author"><b>Автор</b>: {author.name}</br></tpl>',
			'<tpl if="name"><b>Название</b>: {name}</br></tpl>',
			'<tpl if="description"><b>Комментарий</b>: {description}</tpl>'
		],
		title		: 'Задания'
	},
	columnsConfig	: [{},{},
		{ filter: { emptyText: 'имя автора' }, text: 'Автор' },
		{ filter: { emptyText: 'название' }, text: 'Название' },
		{ filter: { emptyText: 'комментарий' }, text: 'Комментарий' },
		{ text: 'Начато' },
		{ text: 'Выполнить до' },
		{ text: 'Завершено' },
		{ filter: { emptyText: 'абсолютная величина' }, text: 'Выполнение' },
		{ text: 'Создано' },
		{ text: 'Обновлено' }
	],
	emptyText		: 'Нет данных для отображения'
});