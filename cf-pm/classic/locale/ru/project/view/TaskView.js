Ext.define('CFJS.locale.ru.project.view.TaskView', {
	override			: 'CFJS.project.view.TaskView',
	config				: { title: 'Задания' },
	taskDesignerConfig	: {
		actions: {
			back			: { tooltip: 'Назад к списку заданий' },
			refreshRecord	: {	tooltip: 'Обновить данные' },
			saveRecord		: { tooltip: 'Записать внесенные изменения' }
		}
	}
});