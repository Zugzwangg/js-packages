Ext.define('CFJS.locale.ru.pages.WorkflowsBrowser', {
	override: 'CFJS.pages.WorkflowsBrowser',
	config	: {
		actions			: {
			addRecord		: {	title: 'Добавить рабочий процесс',	tooltip	: 'Добавить рабочий процесс' },
			removeRecord	: {	title: 'Удалить рабочий процесс',	tooltip	: 'Удалить рабочий процесс'	},
			refreshRecord	: { title: 'Обновление данных', 		tooltip : 'Обновление данных' },
			saveRecord		: { title: 'Сохранить рабочий процесс', tooltip : 'Сохранить рабочий процесс' }
		},
		closeTabText	: 'закрыть вкладку',
		newRecordName	: 'Рабочий процесс',
		title			: 'Дизайнер рабочего процесса'
	},
	confirmRemove: function(formName, fn) {
		return Ext.fireEvent('confirmlostdata', 'удалить рабочий процесс "'+ formName + '"', fn, this);
	}
});
