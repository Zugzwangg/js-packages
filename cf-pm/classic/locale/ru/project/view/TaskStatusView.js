Ext.define('CFJS.locale.ru.project.view.TaskStatusView', {
	override	: 'CFJS.project.view.TaskStatusView',
	config		: { title: 'Выполнение' },
	statusLabel	: 'Статус'
});