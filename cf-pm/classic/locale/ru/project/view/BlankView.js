Ext.define('CFJS.locale.ru.project.view.BlankView', {
	override	: 'CFJS.project.view.BlankView',
	bodyText	: 'Выберите элемент из дерева управления для продолжения.',
	headerText	: 'Нет данных для отображения!'
});