Ext.define('CFJS.locale.ru.project.view.ControlView', {
	override: 'CFJS.project.view.ControlView',
	config	: {
		actions	: {
			addRecord	: { title: 'Добавить элемент', tooltip: 'Добавить новый элемент' },
			filters		: {
				menu	: [{
					checked		: false,
					checkHandler: 'onVisibleFilterChange',
					htype		: 'showHidden',
					text		: 'Выполненные',
				},{
					checked		: true,
					checkHandler: 'onVisibleFilterChange',
					htype		: 'showVisible',
					text		: 'В работе'
				}],
				title	: 'Показать элементы',
				tooltip	: 'Показать элементы'
			},
			periodPicker: { tooltip: 'Выбор рабочего периода' },
			reloadStore	: { tooltip: 'Обновить данные' },
			saveRecord	: { tooltip: 'Записать внесенные изменения' },
		},
		title	: 'Дерево управления'
	},
	workflowPickerConfig: {
		items: {
			buttons	: [{ text: 'Выбрать' }, { text: 'Пустой процесс' }],
			items	: [{ emptyText: 'выберите рабочий процесс', fieldLabel: 'Выберите рабочий процесс' }]
		},
		title: 'Выбор рабочего процесса'
	}
});