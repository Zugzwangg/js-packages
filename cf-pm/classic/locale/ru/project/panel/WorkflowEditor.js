Ext.define('CFJS.locale.ru.project.panel.WorkflowEditor', {
	override			: 'CFJS.project.panel.WorkflowEditor',
	config				: { newRecordName: 'Фаза жизни', title: 'Основное' },
	fieldsConfig		: [{ fieldLabel: 'Код' },{ emptyText: 'введите название', fieldLabel: 'Название' }],
	phaseExplorerConfig	: {
		actions		: {
			addRecord	: { title: 'Добавить фазу', tooltip: 'Добавить новую фазу' },
			refreshStore: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeRecord: { title: 'Удалить фазу', tooltip: 'Удалить текущую фазу' },
			saveRecord	: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' }
		},
		columns		: [{ text: 'Название' }],
		header		: { title: 'Фазы жизни' },
		plugins		: [{
			tpl: [
				'<tpl if="name"><b>Название</b>: {name}</br></tpl>',
				'<tpl if="description"><b>Описание</b>: {description}</tpl>'
			]
		}],
		viewConfig	: { emptyText: 'Нет данных для отображения' }
	}
});
