Ext.define('CFJS.locale.ru.project.panel.ProcessPlanDesigner', {
	override			: 'CFJS.project.panel.ProcessPlanDesigner',
	config				: { title: 'План' },
	explorerConfig		: {
		actions	: {
			addRecord	: { title: 'Добавить задачу', tooltip: 'Добавить новую задачу' },
			refreshStore: { title: 'Обновить данные', tooltip: 'Обновить данные' },
			removeRecord: { title: 'Удалить задачу', tooltip: 'Удалить текущую задачу' },
			saveRecord	: { title: 'Записать изменения', tooltip: 'Записать внесенные изменения' },
			startProcess: { title: 'Начать процесс', tooltip: 'Приступить к выполнению процесса' }
		},
		header	: { title: 'Задания' },
		items	: [{ emptyText: 'выберите исходную задачу', fieldLabel: 'Текущее задание' }]
	},
	explorerGridConfig	: {
		columns		: [{ text: 'Название' }],
		plugins		: [{
			tpl: [
				'<tpl if="name"><b>Название</b>: {name}</br></tpl>',
				'<tpl if="description"><b>Комментарий</b>: {description}</tpl>'
			]
		}],
		viewConfig	: { emptyText: 'Нет данных для отображения' }
	},
	newRecordName		: 'Новая задача',
	startProcessMsg		: 'Начинаю процесс...'
});