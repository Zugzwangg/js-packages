Ext.define('CFJS.locale.ru.project.panel.TaskStatusEditor', {
	override		: 'CFJS.project.panel.TaskStatusEditor',
	config			: { title: 'Статус' },
	itemsConfig		: [{ emptyText: 'выберите сотрудников для участия', fieldLabel: 'Требуется участие' },{ emptyText: 'введите комментарий', fieldLabel: 'Комментарий' }],
	progressLabel	: 'Выполнение',
	sendMessageText	: 'Отправить сообщение'
});