Ext.define('CFJS.locale.ru.project.panel.TaskEditor', {
	override: 'CFJS.project.panel.TaskEditor',
	createItems: function() {
		var me = this, items = me.itemsConfig;
		items[1].items.splice(2, 0, { emptyText: 'выберите фазу', fieldLabel: 'Фаза' });
		items.splice(3, 0, { fieldLabel: 'Цель', items: [{ boxLabel: 'Численная' },{ emptyText: 'ввод числовой цели' },{ boxLabel: 'С отчетом' }] });
		return me.callParent();
	}
});