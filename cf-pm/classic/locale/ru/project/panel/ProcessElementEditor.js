Ext.define('CFJS.locale.ru.project.panel.ProcessElementEditor', {
	override		: 'CFJS.project.panel.ProcessElementEditor',
	config			: { title: 'Основное'},
	itemsConfig		: [{
		items: [{ fieldLabel: 'Код' },{ fieldLabel: 'Тип' },{ fieldLabel: 'Автор' },{ fieldLabel: 'Создано' },{ fieldLabel: 'Обновлено' }]
	},{
		items: [{ emptyText: 'введите название элемента', fieldLabel: 'Название' },{ fieldLabel: 'Путь в дереве' },{ text: 'Приоритет', tooltip: 'Приоритет элемента' }]
	},{
		emptyText: 'введите описание элемента', fieldLabel: 'Описание'
	},{
		items: [{ fieldLabel: 'Начато' },{ fieldLabel: 'Выполнить до' },{ fieldLabel: 'Завершено' },{ text: 'Завершить', tooltip: 'Завершить процесс' }]
	}],
	progressLabel	: 'Состояние выполнения'
});