Ext.define('CFJS.locale.ru.project.panel.LifePhaseEditor', {
	override	: 'CFJS.project.panel.LifePhaseEditor',
	itemsConfig	: [{
		items: [
			{ emptyText: 'введите название', fieldLabel: 'Название фазы' },
			{ emptyText: 'выберите тип', fieldLabel: 'Тип фазы' },
			{ fieldLabel: 'Продолжительность (дни)' }
		]
	},{
		emptyText	: 'введите описание',
		fieldLabel	: 'Описание',
	},{
		nameText	: 'Условие перехода',
		title		: 'Переходы',
		valueEditor	: { emptyText: 'выберите фазу' },
		valueText	: 'Фаза жизни'
	},{
		title		: 'Участники'
	}]
});
