Ext.define('CFJS.locale.ru.project.panel.WorkflowDesigner', {
	override: 'CFJS.project.panel.WorkflowDesigner',
	config	: {
		title			: 'Редактор рабочего процесса',
		refreshDataText	: 'перезагрузить данные рабочего процесса'
	}
});
