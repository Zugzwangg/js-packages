Ext.define('CFJS.locale.ru.project.panel.ProcessPlanEditor', {
	override	: 'CFJS.project.panel.ProcessPlanEditor',
	itemsConfig	: [{
		items: [
			{ emptyText: 'введите название задания', fieldLabel: 'Название' },
			{ emptyText: 'выберите фазу', fieldLabel: 'Фаза' } ,
			{ fieldLabel: 'Длительность (дни)' }
		]
	},{
		emptyText	: 'введите комментарий',
		fieldLabel	: 'Комментарий',
	},{
		nameText	: 'Условие перехода',
		title		: 'Переходы',
		valueEditor	: { emptyText: 'выберите задание' },
		valueText	: 'Задания'
	},{
		title		: 'Участники'
	}]
});