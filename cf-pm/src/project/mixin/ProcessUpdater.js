Ext.define('CFJS.project.mixin.ProcessUpdater', {
	extend		: 'CFJS.mixin.Updatable',
	mixinId		: 'processupdater',
	descriptors	: [ 'members', 'transitions' ],

	privates: {
		
		initMembersStore: function(store) {
			if (store && store.isStore) {
				var me = this,
					updater = function() {
						var item = me.modelForUpdate(), members = [];
						if (item && item.isModel) {
							store.each(function(member) {
								members.push(member.getData());
							});
							item.set('members', members);
						}
					};
				store.on({ datachanged: updater, update: updater });
			}
		},
		
		initTransitionsStore: function(store) {
			if (store && store.isStore) {
				var me = this,
					updater = function() {
						var item = me.modelForUpdate(), transitions = [], transition, data;
						if (item && item.isModel) {
							store.each(function(rec) {
								transitions.push(transition = { type: rec.get('type'), data: null });
								if ((data = rec.get('data')) && data.id > 0) {
									transition.data = { id: data.id, name: data.name };
								}
							});
							item.set('transitions', transitions);
						}
					};
				store.on({ update: updater });
			}
		},
		
		modelForUpdate: function() {
			return this.getItem();
		}
		
	}

});