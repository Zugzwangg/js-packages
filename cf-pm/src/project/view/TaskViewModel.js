Ext.define('CFJS.project.view.TaskViewModel', {
	extend		: 'CFJS.project.view.PrincipleViewModel',
	alias		: 'viewmodel.project.task-edit',
	requires	: [ 'CFJS.model.project.Task' ],
	itemModel	: 'CFJS.model.project.Task'
});
