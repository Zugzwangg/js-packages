Ext.define('CFJS.project.view.ControlViewModel', {
	extend			: 'CFJS.app.BaseEditorModel',
	alias			: 'viewmodel.project.control-view',
	requires		: [
		'Ext.data.ChainedStore',
		'CFJS.store.project.ControlTree',
		'CFJS.store.project.PrincipleTypes',
		'CFJS.project.view.PrincipleViewModel'
	],
	isControlView	: true,
	data			: {
		showHidden		: false,
		showVisible		: true,
	},
	itemName		: 'editPrinciple',
	itemModel		: 'CFJS.model.project.Principle',
	formulas		: {
		itemRules	: {
			bind: {
				id		: '{_itemName_.id}',
				type	: '{_itemName_.type}',
				finished: '{_itemName_.finished}'
			},
			get	: function(data) { return CFJS.project.view.PrincipleViewModel.buildItemRules(this.getItem(), this.get('itemRules')); }
		}
	},
	stores			: {
		principles		: {
			type	: 'controltree',
			pageSize: 0,
			filters	: [{
				property: 'author',
				type	: 'numeric',
				operator: 'eq',
				value	: '{user.post.id}'
			},{
				property: 'members',
				type	: 'set',
				operator: 'in',
				value	: '{user.post.id}'
			},{
				id		: 'startDate',
				property: 'startDate',
				type	: 'date-time',
				operator: '!before',
				value	: '{workingPeriod.startDate}'
			},{
				id		: 'endDate',
				property: 'startDate',
				type	: 'date-time',
				operator: '!after',
				value	: '{workingPeriod.endDate}'
			}]
		},
		principleTypes	: { source: Ext.getStore('principleTypes') }
	},
	workingPeriod	: [ Ext.Date.getFirstDateOfYear(new Date()), new Date() ],
	
	changeVisibleFilter: function(data) {
		var me = this, hidden = data[0], visible = data[1],
			store = this.getStore('principles'), filters;
		if (store && store.isStore) {
			filters = store.getFilters();
			filters.beginUpdate();
			filters.removeByKey('visible');
			filters.removeByKey('hidden');
			if (hidden && !visible) {
				filters.add({ id: 'hidden', property: 'finished', operator: '!eq', value: null });
			} else if (visible && !hidden) {
				filters.add({ id: 'visible', property: 'finished', operator: 'eq', value: null });
			}
			filters.endUpdate();
		}
	},

	initConfig: function() {
		var me = this;
		me.callParent(arguments);
		me.bind(['{showHidden}', '{showVisible}' ], me.changeVisibleFilter, me);
	},
	
	setItem: function(item) {
		return this.linkItemRecord(item);
	}

}, function(type) {
	type.prototype.configItemRecord = CFJS.project.view.PrincipleViewModel.configPrinciple;
});