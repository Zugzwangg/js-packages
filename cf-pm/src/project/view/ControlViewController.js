Ext.define('CFJS.project.view.ControlViewController', {
	extend		: 'Ext.app.ViewController',
	alias		: 'controller.project.control-view',
	id			: 'viewControlTree',
	
	lookupEditor: function() {
		return this.executeView('lookupEditor');
	},
	
	lookupEditorViewModel: function() {
		var editor = this.lookupEditor();
		return editor ? editor.lookupViewModel() : null;
	},
	
	lookupTree: function() {
		return this.executeView('lookupTree');
	},
	
	lookupTreeStore: function(name) {
		var tree = this.lookupTree();
		return tree ? tree.getStore() : null;
	},
	
	onAddRecord: function(component) {
		var me = this, vm = me.lookupEditorViewModel(), tree = me.lookupTree(),
			selection = Ext.Array.from(tree && tree.getSelection()),
			target = (selection.length > 0 ? selection[0] : null) || tree && tree.getRootNode(),
			type = (component && component.principleType || 'PROJECT').toUpperCase(),
			append = function() {
				var node = { type: type, parentId: target.id };
				if (type !== 'TASK') node.children = [];
				if (vm && vm.isBaseModel) node = vm.setItem(node);
				tree.selModel.select(node = target.appendChild(node));
				if (type === 'PROCESS') me.executeView('showWorkflowPicker', node);
			};
		if (target && (target.$className !== 'CFJS.model.project.Task'
			|| target.$className === 'CFJS.model.project.Process' && type !== 'TASK')) {
			if (!target.isExpanded()) target.expand(false, append);
			else append.call(me);
		}
	},
	
	onPeriodPicker: function(component) {
		var tree = this.lookupTree();
		if (tree && tree.hasPeriodPicker) tree.showPeriodPicker(component);
	},

	onReloadStore: function(component) {
		var tree = this.lookupTree(), store;
		if (tree && tree.isTree) tree.collapseAll(function() {
			if ((store = tree.getStore()) && store.isStore) {
				store.rejectChanges();
				store.reload();
			}
		});
	},
	
	onSaveChanges: function(component, force) {
		var me = this, tree = me.lookupTree(), store = tree && tree.getStore(),
			i = 0, records;
		if (store && store.isStore && !tree.readOnly
				&& me.fireEvent('beforesaverecord', records = store.getModifiedRecords()) !== false && records.length > 0) {
			tree.disableComponent(component, true);
			for (; i < records.length; i++) {
				records[i].save(i < records.length - 1 || {
					callback: function(record, operation, success) {
						tree.disableComponent(component);
					}
				});
			}
		}
	},
	
	onTreeSelectionChange: function(selectable, records) {
		this.startEditRecord((records = Ext.Array.from(records)).length > 0 ? records[0] : false);
	},

	onVisibleFilterChange: function(component, checked) {
		if (component) Ext.callback('set', this.getViewModel(), [component.htype, !!checked]);
	},
	
	startEditRecord: function(record) {
		var me = this, vm = me.getViewModel();
		if (vm && vm.isBaseModel) return vm.setItem(record);
	},
	
	privates: {
		
		executeView: function() {
			if (arguments.length > 0) {
				var args = Array.prototype.slice.call(arguments);
				return Ext.callback(args.splice(0, 1)[0], this.getView(), args);
			}
		}
	
	}

});