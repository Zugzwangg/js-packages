Ext.define('CFJS.project.view.PrincipleViewModel', {
	extend			: 'CFJS.app.EditorViewModel',
	alias			: 'viewmodel.project.control-edit',
	requires		: [
		'CFJS.model.project.Process',
		'CFJS.model.project.Task',
		'CFJS.model.project.Workflow',
		'CFJS.store.project.PrincipleResources',
		'CFJS.store.project.ProcessMembers',
	],
	mixins			: [ 'CFJS.project.mixin.ProcessUpdater' ],
	descriptors		: { members: '{principleMembers}' },
	isPrincipleEdit	: true,
	itemName		: undefined,
	itemModel		: undefined,
	formulas		: {
		itemIconCls: {
			bind: '{_itemName_.type}',
			get	: function(type) {
				var types = Ext.getStore('principleTypes');
				type = types && types.getById(type || 'PROJECT');
				return type ? type.get('iconCls') : null;
			}
		},
		itemTitle: {
			bind: { name: '{_itemName_.name}', type: '{_itemName_.type}' },
			get	: function(data) {
				var types = Ext.getStore('principleTypes'),
					type = types && types.getById(data.type || 'PROJECT');
				return type ? type.get('name') + ': ' + (data.name||'').quote() : null;
			}
		},
		phaseInfo: {
			bind: '{_itemName_.phase}',
			get	: function(phase) {
				return CFJS.project.view.PrincipleViewModel.phaseInfo(phase);
			}
		},
		priorityIconCls: {
			bind: '{_itemName_.priority}',
			get	: function(priority) { return CFJS.UI.iconCls.FLAG + ' ' + CFJS.schema.Project.priorityColorCls(priority || 'NORMAL'); }
		}
	},
	stores			: {
		principleResources		: {
			type	: 'principle_resources',
			autoLoad: true,
			filters	: [{
				id		: 'principle',
				property: 'principle.id',
				operator: 'eq',
				type	: 'numeric',
				value	: '{_itemName_.id}'
			}],
			sorters	: [ 'name' ]
		},
		principleMembers: {	type: 'processmembers', data: '{_itemName_.members}' }
	},

	canDeleteResource: function(record, userInfo) {
		return record && record.isEntity && record.isAuthor(userInfo); 
	},
	
	createProcessPlan: function(workflow, lifePhases, callback) {
		var me = this, process = me.getItem(),
			options = {
				callback: function(records, operation, success) {
					if (success) me.createProcessPlan(workflow, lifePhases, callback);
					else Ext.callback(callback);
				}
			};
		// check process and workflow
		if (!(process && process.isProcess && workflow && workflow.id > 0)) return;
		// save new process model
		if (process.phantom || !(process.id > 0)) {
			process.save(options);
			return;
		}
		// load workflow
		if (!workflow.isModel) {
			CFJS.model.project.Workflow.load(workflow.id, {
				success: function(record, operation) {
					process.set('name', process.get('name') + ': ' + record.get('name'));
					me.createProcessPlan(record, lifePhases, callback);
				},
				failure: callback
			});
			return;
		}
		// load life phases
		if (!lifePhases || !lifePhases.isStore) {
			lifePhases = Ext.Factory.store({
				type	: 'lifephases',
				filters	: [{
					id		: 'workflow',
					property: 'workflow.id',
					type	: 'numeric',
					operator: 'eq',
					value	: workflow.id
				}]
			});
			lifePhases.load(options);
			return;
		}
		me.buildProcessPlan(process, lifePhases, workflow.get('initialPhase') || {}, callback);
	},

	initConfig: function(config) {
		var me = this, parent = (config||{}).parent;
		if (parent && parent.isControlView) {
			me.setItem = parent.setItem;
		} else {
			CFJS.apply(config, {
				formulas: {
					itemRules	: {
						bind: {
							id		: '{_itemName_.id}',
							type	: '{_itemName_.type}',
							finished: '{_itemName_.finished}'
						},
						get	: function(data) { return CFJS.project.view.PrincipleViewModel.buildItemRules(me.getItem(), me.get('itemRules')); }
					}
				}
			});
		}
		me.callParent(arguments);
	},

	setItem: function(item) {
		return this.linkItemRecord(item);
	},
	
	privates: {
		
		buildProcessPlan: function(process, lifePhases, initialPhase, callback) {
			var me = this, saveCnt = 0, defTrs = [], lpMap = {}, processRef, trTypes = Ext.getStore('transitionTypes'),
				owner = Ext.apply({ memberType: 'OWNER', priority: 0 }, CFJS.lookupViewportViewModel().postInfoAsAuthor()),
				alignIndexes = function(lifePhase) {
					var plan = lpMap[lifePhase.getId()], transitions = [], nextPlan;
					Ext.Array.each(lifePhase.get('transitions'), function(transition) {
						nextPlan = lpMap[(transition.data||{}).id];
						transitions.push({
							type: transition.type,
							data: nextPlan && nextPlan.isModel ? { id: nextPlan.getId(), name: nextPlan.get('name') } : ""
						});
					});
					plan.set('transitions', transitions);
					saveCnt++;
					plan.save({
						callback: function() {
							saveCnt--;
							if (saveCnt === 0) process.save({ callback: callback }); 
						}
					});
				};
			if (process && process.isProcess && process.id > 0
					&& lifePhases && lifePhases.isStore && lifePhases.getCount() > 0) {
				processRef = { id: process.id, name: process.get('name') };
				if (trTypes && trTypes.isStore) {
					trTypes.each(function(type) {
						defTrs.push({ type: type.getId(), data: null });
					});
				}
				lifePhases.each(function(lifePhase) {
					var plan = lifePhase.asProcessPlan(),
						currentPhase = (lifePhase.id === initialPhase.id),
						members = Ext.Array.from(plan.get('members'));
					if (members.length === 0) members.push(owner);
					plan.set({ process: processRef, transitions: defTrs, members: members });
					saveCnt++;
					plan.save({
						callback: function(record, operation, success) {
							saveCnt--;
							if (success) {
								lpMap[lifePhase.getId()] = record;
								if (currentPhase) process.set('currentPhase', { id: record.id, name: record.get('name') });
							}
							if (saveCnt == 0) lifePhases.each(alignIndexes);
						}
					});
				});
			} else Ext.callback(callback);
		}

	},
	
	statics: {
		
		buildItemRules: function(item, oldRules) {
			var rules = {}, isAuthor = false, data = {}, postInfo;
			Ext.Object.each(oldRules, function(key) { rules[key] = false; });
			if (!(rules.isRoot = !item)) {
				data = item.getData();
				rules = Ext.apply(rules, {
					isProject		: data.type === 'PROJECT',
					isProcess		: data.type === 'PROCESS',
					isTask			: data.type === 'TASK',
					isLeader		: false,
					isOwner			: false,
					isParticipant	: false,
					isViewer		: false,
					finished		: !!data.finished
				});
				rules.isOwner = isAuthor = item.isAuthor(postInfo = CFJS.lookupViewportViewModel().postInfoAsAuthor());
				if (postInfo) {
					Ext.Array.each(Ext.Array.from(data.members), function(member, index) {
						if (member && member.id === postInfo.id && member.type === postInfo.type) {
							Ext.apply(rules, {
								isOwner			: rules.isOwner || member.memberType === 'OWNER',
								isLeader		: member.memberType === 'LEADER',
								isParticipant	: member.memberType === 'PARTICIPANT',
								isViewer		: member.memberType === 'VIEWER'
							});
							return false;
						}
					});
				}
				rules.editable = rules.isOwner && !rules.finished;
				rules.editResources = data.id > 0 && !rules.finished;
				rules.editWorkflow = rules.isProcess && rules.editable;
				if (rules.isTask) Ext.apply(rules, { editorActiveItem: 'taskDesigner' });
				else if (rules.isProject || rules.isProcess) Ext.apply(rules, { editorActiveItem: 'elementDesigner' });
			}
			rules.canAdd = rules.isRoot || rules.editable && data.id > 0 && !rules.isTask;
			return Ext.apply(rules, {
				canAddPROJECT	: rules.isRoot || rules.canAdd && rules.isProject,
				canAddPROCESS	: rules.isRoot || rules.canAdd && rules.isProject,
				canAddTASK		: rules.isRoot || rules.canAdd && !rules.isTask,
				canFinish		: (rules.isOwner || rules.isLeader) && !rules.finished,
				editGoal		: rules.editable && rules.isTask && !(data.id > 0)
			}); 

		},
		
		configPrinciple: function(item) {
			if ((item && item.isModel) || item === false) {
				if (item && item.needUpdate()) item.load();
				return item;
			}
			item = item || {};
			if (Ext.isString(item)) item = { type: item };
			var types = Ext.getStore('principleTypes'), principleType = types && types.getById(item.type || 'PROJECT');
			if (principleType) {
				Ext.applyIf(item, {
					author		: CFJS.lookupViewportViewModel().postInfoAsAuthor(),
					name		: principleType.get('name'),
					startDate	: new Date(),
					type		: principleType.id
				});
				if (item.author) item.members = [ Ext.apply({ memberType: 'OWNER' }, item.author) ];
			}
			return item;
		},

		phaseInfo: function(phase) {
			var store = phase && Ext.getStore('lifePhaseTypes');
			return store && store.isStore && store.getById(phase) || null;
		}

	}

}, function(type) {
	type.prototype.configItemRecord = type.configPrinciple;
});