Ext.define('CFJS.project.view.TasksViewModel', {
	extend			: 'CFJS.app.BaseEditorModel',
	alias			: 'viewmodel.project.tasks',
	requires		: [ 'CFJS.store.project.Tasks' ],
	itemModel		: 'CFJS.model.project.Task',
	itemName		: 'editTask',
	stores			: {
		tasks: {
			type	: 'tasks',
			filters	: [{
				property: 'author',
				type	: 'numeric',
				operator: 'eq',
				value	: '{user.post.id}'
			},{
				property: 'members',
				type	: 'set',
				operator: 'in',
				value	: '{user.post.id}'
			}]
		}
    },
	workingPeriod	: [ Ext.Date.getFirstDateOfYear(new Date()), new Date() ],

	addTasksFilters: function(filters) {
		var me = this;
		Ext.Array.each(me.tasksFilters = Ext.Array.from(filters), function(filter, index) {
			if (!filter) me.tasksFilters.splice(index, 1);
		}, null, true);
		me.initTasks(me.getStore('tasks'));
	},
    
	initConfig: function() {
		var me = this;
		me.callParent(arguments);
		me.bind('{tasks}', me.initTasks, me, { single: true });
		me.bind([ '{tasks}', '{workingPeriod}' ], me.updateTasksPeriod);
	},

	initTasks: function(store) {
		if (store && store.isStore) {
			store.getFilters().beginUpdate();
			store.addFilter(this.tasksFilters);
			store.getFilters().endUpdate();
		}
	},
	
	updateTasksPeriod: function(data) {
		if (data[0] && data[0].isStore) {
			var filters = data[0].getFilters(); 
			filters.beginUpdate();
			filters.removeByKey('startDate');
			filters.removeByKey('endDate');
			if (data[1]) {
				if (data[1].startDate) filters.add({
					id		: 'startDate',
					property: 'startDate',
					type	: 'date-time',
					operator: '!before',
					value	: data[1].startDate
				});
				if (data[1].endDate) filters.add({
					id		: 'endDate',
					property: 'startDate',
					type	: 'date-time',
					operator: '!after',
					value	: data[1].endDate
				});
			}
			filters.endUpdate();
		}
	}

});
