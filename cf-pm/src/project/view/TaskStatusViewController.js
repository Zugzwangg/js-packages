Ext.define('CFJS.project.view.TaskStatusViewController', {
	extend	: 'Ext.app.ViewController',
	alias	: 'controller.project.task.status',
	
	onButtonClick: function(btn) {
		var me = this, view = me.getView(),
			vm = view && view.lookupViewModel(), statuses;
		if (vm && Ext.isFunction(vm.sendStatus)) {
			view.mask(view.saveMsg);
			vm.sendStatus((btn||{}).action, function(record, operation, success) {
				view.unmask();
				if (success) vm.setItem();
				if (statuses = vm.getStore('statuses')) statuses.reload();
			});
		}
	}

});