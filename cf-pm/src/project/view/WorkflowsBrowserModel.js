Ext.define('CFJS.project.view.WorkflowsBrowserModel', {
	extend	: 'Ext.app.ViewModel',
	alias	: 'viewmodel.project.workflows-browser',
	requires: [
		'CFJS.store.project.LifePhaseTypes',
		'CFJS.store.project.TransitionTypes',
		'CFJS.store.project.Workflows'
	],
	createWorkflowModel: function(name) {
		return new CFJS.model.project.Workflow({
			locale	: CFJS.getLocale(),
			name	: name
		});
	}
});