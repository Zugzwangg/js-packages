Ext.define('CFJS.project.view.WorkflowDesignerModel', {
	extend		: 'CFJS.app.EditorViewModel',
	alias		: 'viewmodel.project.workflow-designer',
	requires	: [
		'CFJS.store.project.LifePhases',
		'CFJS.store.project.ProcessMembers',
		'CFJS.store.project.Transitions'
	],
	mixins		: [ 'CFJS.project.mixin.ProcessUpdater' ],
	config		: { workflow: null },
	descriptors	: { members: '{lifePhaseMembers}', transitions: '{lifePhaseTransitions}' },
	data		: { lifePhase: null },
	itemName	: 'workflow',
	itemModel	: 'CFJS.model.project.Workflow',
	stores		: {
		lifePhases			: {
			type	: 'lifephases',
			autoLoad: true,
			pageSize: 0,
			session	: { schema: 'document' },
			filters	: [{
				id		: 'workflow',
				property: 'workflow.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			}]
		},
		lifePhaseMembers	: {	type: 'processmembers', data: '{lifePhase.members}' },
		lifePhaseTransitions: { type: 'transitions', data: '{lifePhase.transitions}' },
		nextElements		: {
			type	: 'lifephases',
			autoLoad: true,
			pageSize: 0,
			session	: { schema: 'document' },
			filters	: [{
				id		: 'workflow',
				property: 'workflow.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_itemName_.id}'
			},{
				property: 'id',
				type	: 'numeric',
				operator: '!eq',
				value	: '{lifePhase.id}'
			}]
		}
	},
	
	beforeSaveWorkflow: function(workflow) {
		var me = this, canSave = false, data;
		if (workflow && workflow.isModel) {
//			data = me.getStore('lifePhases');
//			if (data && data.isStore) {
//				canSave = me.beforeSaveLifePhase(data.getRange());
//			}
			canSave = true;
		}
		return canSave;
	},
	
	beforeSaveLifePhase: function(lifePhase) {
		var me = this, i = 0, t = 0, data, items, item;
		lifePhase = Ext.Array.from(lifePhase);
		for (; i < lifePhase.length; i++) {
			if ((data = lifePhase[i]) && data.isModel) {
				items = Ext.Array.from(data.get('transitions'));
				for (; t < items.length; t++) {
					if (item = items[t]) {
						item.data = item.data && item.data.id > 0 ? { id: item.data.id, name: item.data.name } : "";
					} else items.splice(t--, 1);
				}
				data.data.transitions = items;
			}  else lifePhase.splice(i--, 1);
		}
		return lifePhase.length > 0;
	},
	
	createLifePhaseModel: function(name) {
		var me = this, transitionTypes = Ext.getStore('transitionTypes'), transitions = [];
		if (transitionTypes && transitionTypes.isStore) {
			transitionTypes.each(function(type) {
				transitions.push({ type: type.getId(), data: null });
			});
		}
		return new CFJS.model.project.LifePhase({
			locale		: CFJS.getLocale(),
			name		: name,
			duration	: 1,
			transitions	: transitions,
			workflow	: me.getItem().getData()
		});
	},
	
	setItem: function(item) {
		return this.linkItemRecord(item);
	},

	privates: {
		
		modelForUpdate: function() {
			return this.get('lifePhase');
		},

		updateWorkflow: function(workflow) {
			this.setItemName('workflow' + Ext.id(workflow));
			this.setItem(workflow);
		}

	}

});