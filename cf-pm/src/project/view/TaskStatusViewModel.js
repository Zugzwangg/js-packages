Ext.define('CFJS.project.view.TaskStatusViewModel', {
	extend		: 'CFJS.app.InlineEditorViewModel',
	alias		: 'viewmodel.project.task.status',
	requires	: [ 'CFJS.project.view.PrincipleViewModel', 'CFJS.store.project.TaskStatuses' ],
	mixins		: [ 'CFJS.mixin.Updatable' ],
	itemModel	: 'CFJS.model.project.TaskStatus',
	itemName	: 'editStatus',
	data		: { completed: false },
	descriptors	: 'statuses',
	stores		: {
		statuses: {
			type	: 'taskstatuses',
			autoLoad: true,
			filters	: [{
				id		: 'task',
				property: 'task.id',
				type	: 'numeric',
				operator: 'eq',
				value	: '{_parentItemName_.id}'
			}]
		}
    },
    
    applyButtonRules: function(data) {
		if (!data[0] || !data[0].isModel) return;
		var me = this, info = data[0].getData(), 
			task = me.getParentItem(), hidden = !!data[1] || !data[2],
			numerical = !!(task && task.isModel && task.get('numerical')),
			apply = info.apply || {}, applyRework = info.applyRework, cancel = info.cancel;
		me.set({
			applyHidden			: hidden,
			applyIconCls		: apply.iconCls || CFJS.UI.iconCls.APPLY,
			applyText			: apply.text,
			applyReworkHidden	: !applyRework || hidden || numerical,
			applyReworkIconCls	: applyRework && applyRework.iconCls || CFJS.UI.iconCls.WRENCH,
			applyReworkText		: applyRework && applyRework.text,
			cancelHidden		: !cancel || hidden || numerical,
			cancelText			: cancel && cancel.text,
			numericalHidden		: hidden || !numerical
		});
    },
    
    defaultItemConfig: function(item) {
    	var parentItem = this.getParentItem();
    	return Ext.apply(item || {}, parentItem && parentItem.isTask ? parentItem.asStatus() : null); 
    },
    
    initConfig: function() {
		var me = this;
		me.callParent(arguments);
		me.bind(['{phaseInfo}', '{completed}', '{itemRules.isParticipant}' ], me.applyButtonRules, me);
		me.bind(['{_parentItemName_.id}', '{_parentItemName_.goal}'], function(data) {
			var parentItem = me.getParentItem();
			if (parentItem && parentItem.isTask) me.setItem();
		});
	},

    phaseInfo: function() {
    	var me = this, phaseInfo = me.get('phaseInfo'), task;
    	if (!phaseInfo && (task = me.getParentItem()) && task.isModel) {
    		return CFJS.project.view.PrincipleViewModel.phaseInfo(task.get('phase'));
    	}
    	return phaseInfo;
    },
    
    sendStatus: function(action, callback, scope) {
    	var me = this, item = me.getItem(), progress;
    	if (item && item.isModel) {
    		switch (action) {
	    		case 'apply':
	    			progress = item.get('numerical') === false ? 1 : (item.get('progress') || 0);
	    			break;
	    		case 'applyRework':
	    			progress = item.get('numerical') === false ? .5 : (item.get('progress') || 0);
	    			break;
	    		case 'cancel':
	    			progress = 0;
	    			break;
	    		case 'send':
	    			if (Ext.isEmpty(item.get('description')||item.get('involvement'))) return Ext.callback(callback);
	    			break;
	    		default: return Ext.callback(callback);
    		}
			item.set('progress', progress);
			item.save({ callback: callback });
			return;
    	}
    	Ext.callback(callback);
    },
    
	setItem: function(item) {
		return this.linkItemRecord(item);
	},
	
    statusText: function(progress) {
    	var me = this, key = progress > 0 ? 'apply' : 'cancel', phaseInfo = me.phaseInfo();
    	return (phaseInfo && (phaseInfo.isModel ? phaseInfo.get(key) : phaseInfo[key])||{}).status;
    },
    
    privates: {
    	
    	initStatusesStore: function(store) {
    		if (store && store.isStore) {
    			var me = this, completed,
    				author = CFJS.lookupViewportViewModel().userInfoAsAuthor(),
					updater = function() {
	        			store.each(function(status) {
	        				if ((completed = !Ext.isEmpty(status.get('progress'))) && status.isAuthor(author)) {
		        				me.set('completed', completed);
		        				return false;
	        				}
	        			});
	    			};
				store.on({ datachanged: updater, update: updater });
    		}
    	}
    
    }

});