Ext.define('CFJS.project.view.TasksViewController', {
	extend		: 'CFJS.app.DictionaryController',
	alias		: 'controller.project.tasks',
	editor		: 'project.task-designer',
	
	editorConfig: function(config) {
		return CFJS.apply({}, config);
	},
	
	getEditorComponent: function() {
		var me = this, view = me.getView();
		return view ? view.lookupEditor() : null;
	},
	
	maybeEditRecord: function(record, add) {
		var me = this, view = me.getView(),
			grid = view ? view.lookupGrid() : null,
			activeItem;
		if (grid) {
			if (view.rendered) Ext.suspendLayouts();
			if (record !== false && !grid.readOnly) {
				var selection = grid.getSelection();
				if (!record && !add && selection && selection.length > 0) record = selection[0];
				activeItem = view.lookupEditor();
				activeItem.getViewModel().setItem(record);
			} else activeItem = grid;
			view.getLayout().setActiveItem(activeItem);
			if (view.rendered) Ext.resumeLayouts(true);
		}
	},

	onAddRecord: Ext.emptyFn,
	
	onBackClick: function(component) {
		var me = this, record = me.lookupRecord();
		if (record && !record.get('finished') && (record.phantom || record.dirty)) {
			Ext.fireEvent('confirmlostdataandgo', function(btn) {
				if (btn === 'yes') {
					record.reject();
					me.maybeEditRecord(false);
				}
			}, me);
		} else me.maybeEditRecord(false);
	},

	onRemoveRecord : Ext.emptyFn
	
});