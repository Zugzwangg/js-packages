Ext.define('CFJS.project.view.ProcessPlanDesignerModel', {
	extend			: 'CFJS.app.EditorViewModel',
	alias			: 'viewmodel.project.process-plan-designer',
	requires		: [
		'CFJS.store.project.ProcessMembers',
		'CFJS.store.project.ProcessPlans',
		'CFJS.store.project.Transitions',
		'CFJS.store.project.TransitionTypes'
	],
	mixins			: [ 'CFJS.project.mixin.ProcessUpdater' ],
	descriptors		: { members: '{planMembers}', transitions: '{planTransitions}' },
	isDetail		: true,
	isPlanDesigner	: true,
	itemName		: 'processPlan',
	itemModel		: 'CFJS.model.project.ProcessPlan',
	data			: { hasProcessPlan: false },
	formulas		: {
		executable: {
			bind: {
				editable	: '{itemRules.editWorkflow}',
				executable	: '{_parentItemName_.executable}',
				hasPlan		: '{hasProcessPlan}',
				phase		:'{_parentItemName_.currentPhase}'
			},
			get	: function(data) {
				return data.editable && data.executable && data.hasPlan 
					&& data.phase && data.phase.id && data.phase.id > 0;
			}
		}
	},
	stores			: {
		nextTasks		: {
			source	: '{processPlans}',
			autoLoad: true,
			filters	: [{
				property: 'id',
				operator: '>',
				value: 0
			},{
				property: 'id',
				operator: '!=',
				value	: '{_itemName_.id}'
			}],
			sorters	: [	'name' ]
		},
		phases			: {
			source	: '{processPlans}',
			autoLoad: true
		},
		processPlans	: {
			type	: 'processplans',
			pageSize: 0,
			session	: { schema: 'project' },
			sorters	: [	'name' ]
		},
		planMembers		: {	type: 'processmembers', data: '{_itemName_.members}' },
		planTransitions	: { type: 'transitions', data: '{_itemName_.transitions}' },
	},

	beforeRemoveProcessPlan: function(records) {
		var me = this, store = me.getStore('processPlans'), parent = me.getParentItem(),
			currentPhase = parent && parent.isProcess && parent.get('currentPhase');
		records = Ext.Array.from(records);
		if (currentPhase && store && store.isStore
				&& records.length < store.getTotalCount()) {
			Ext.Array.each(records, function(record, index) {
				if ((record||{}).id === currentPhase.id) {
					records.splice(index, 1);
					return false;
				}
			});
		}
		return records.length > 0;
	},

	beforeSaveProcessPlan: function(records) {
		records = Ext.Array.from(records);
		Ext.Array.each(records, function(rec, i) {
			if (rec && rec.isModel) {
				var transitions = Ext.Array.from(rec.get('transitions'));
				Ext.Array.each(transitions, function(transition, t) {
					if (transition) {
						transition.data = (transition.data || {}).id > 0 ? { id: transition.data.id, name: transition.data.name } : "";
					} else transitions.splice(t, 1);
				}, null, true);
				rec.data.transitions = transitions;
			} else records.splice(i, 1);
		}, null, true);
		return records.length > 0;
	},

	createProcessPlanModel: function(name, totalCount) {
		var me = this, session = me.getSession(), 
			transitionTypes = Ext.getStore('transitionTypes'), 
			parent = me.getParentItem() || {}, transitions = [];
		if (transitionTypes && transitionTypes.isStore) {
			transitionTypes.each(function(type) {
				transitions.push({ type: type.getId(), data: null });
			});
		}
		if (parent && parent.isProcess) parent = parent.getData();
		return me.createRecord('CFJS.model.project.ProcessPlan', {
			locale		: CFJS.getLocale(),
			name		: name,
			duration	: 1,
			transitions	: transitions,
			members		: Ext.clone(parent.members),
			process		: { id: parent.id, name: parent.name }
		});
	},
		
	initConfig: function() {
		var me = this;
		me.callParent(arguments);
		me.bind('{_parentItemName_.id}', me.onChangeParentItem, me);
	},
	
	onChangeParentItem: function(id) {
		var me = this, parent = me.getParentItem();
		if (parent && parent.isProcess) {
			me.getStore('processPlans').addFilter({
				id		: 'process',
				property: 'process.id',
				type	: 'numeric',
				operator: 'eq',
				value	: parent.id || 0
			});
		}
	},
	
	refreshProcessPlan: function() {
		var store = this.getStore('processPlans');
		if (store && store.isStore) store.reload();
	},
	
	setItem: function(item) {
		return this.linkItemRecord(item);
	},
	
	startProcess: function(callback, scope) {
		var me = this, process = me.getParentItem();
		if (process && process.isProcess) {
			process.loadCurrentPhase(function(phase, operation, success) {
				if (success && phase) {
					phase.asTask().save({
						callback: function(task, operation, success) {
							if (success) {
								process.set('executable', false);
								process.commit();
							}
							Ext.callback(callback, scope || me, [task, success]);
						}
					});
				} else Ext.callback(callback, scope || me, [phase, success]);
			});
		} else Ext.callback(callback, scope || me, [process, false]);
	}

});