Ext.define('CFJS.store.project.Workflows', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.workflows',
	model	: 'CFJS.model.project.Workflow',
	storeId	: 'workflows',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'workflow' },
			reader		: { rootProperty: 'response.transaction.list.workflow' }
		}
	},
	sorters	: [ 'name' ]
});
