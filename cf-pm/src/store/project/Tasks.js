Ext.define('CFJS.store.project.Tasks', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.tasks',
	model	: 'CFJS.model.project.Task',
	storeId	: 'tasks',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'task' },
			reader		: { rootProperty: 'response.transaction.list.task' }
		}
	},
	sorters	: [ 'startDate', 'name' ]
});
