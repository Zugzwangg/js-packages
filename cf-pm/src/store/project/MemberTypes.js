Ext.define('CFJS.store.project.MemberTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.membertypes',
	storeId	: 'memberTypes',
	data	: [
		{ id: 'LEADER',			name: 'Leader' },
		{ id: 'OWNER',			name: 'Owner' },
		{ id: 'PARTICIPANT',	name: 'Participant' },
		{ id: 'VIEWER',			name: 'Viewer' }
	]
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'membertypes' }));
});