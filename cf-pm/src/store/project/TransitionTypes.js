Ext.define('CFJS.store.project.TransitionTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.transitiontypes',
	storeId	: 'transitionTypes',
	data	: [
		{ id: 'CANCEL',					name: 'Cancel' },
		{ id: 'OVERDUE',				name: 'Overdue' },
		{ id: 'SUCCESS',				name: 'Success' },
		{ id: 'SUCCESS_WITH_REWORK',	name: 'Success with rework' }
	]
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'transitiontypes' }));
});