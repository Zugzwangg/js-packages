Ext.define('CFJS.store.project.ProcessMembers', {
	extend	: 'Ext.data.Store',
	alias	: 'store.processmembers',
	model	: 'CFJS.model.project.ProcessMember',
	storeId	: 'processMembers',
	sorters	: [ 'memberType', 'priority', 'name' ]
});