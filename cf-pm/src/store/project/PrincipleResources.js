Ext.define('CFJS.store.project.PrincipleResources', {
	extend	: 'CFJS.store.Buffered',
	alias	: 'store.principle_resources',
	model	: 'CFJS.model.project.PrincipleResource',
	storeId	: 'principleResources',
	config	: {
		proxy: {
			loaderConfig: { dictionaryType: 'principle_resource' },
			reader		: { rootProperty: 'response.transaction.list.principle_resource' },
			service		: 'svd'
		}
	}
});
