Ext.define('CFJS.store.project.ProcessPlans', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.processplans',
	model	: 'CFJS.model.project.ProcessPlan',
	storeId	: 'processPlans',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'process_plan' },
			reader		: { rootProperty: 'response.transaction.list.process_plan' }
		}
	}
});
