Ext.define('CFJS.store.project.LifePhaseTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.lifephasetypes',
	storeId	: 'lifePhaseTypes',
	data	: [{ 
		id			: 'ARCHIVE',
		name		: 'Archiving',
		apply		: { text: 'Archive',				status: 'Archived',		iconCls: CFJS.UI.iconCls.ARCHIVE },
		applyRework	: { text: 'Archive with rework' },
		cancel		: {	text: 'Decline', 				status: 'Declined' }
	},{
		id			: 'APPROVEMENT',
		name		: 'Approvement',
		apply		: { text: 'Agree',					status: 'Agreed',		iconCls: CFJS.UI.iconCls.GAVEL },
		applyRework	: { text: 'Agree with rework' },
		cancel		: {	text: 'Decline', 				status: 'Declined' }
	},{
		id			: 'CANCELLATION',
		name		: 'Cancellation',
		apply		: { text: 'Cancel',					status: 'Canceled',		iconCls: CFJS.UI.iconCls.CANCEL },
		applyRework	: { text: 'Cancel with rework' },
		cancel		: {	text: 'Decline', 				status: 'Declined' }
	},{
		id			: 'EXECUTION',
		name		: 'Execution',
		apply		: { text: 'Done',					status: 'Done',			iconCls: CFJS.UI.iconCls.COGS },
		applyRework	: { text: 'Done with rework' },
		cancel		: {	text: 'Decline', 				status: 'Not done' }
	},{
		id			: 'FAMILIARIZATION',
		name		: 'Familiarization',
		apply		: { text: 'Familiarize',			status: 'Familiar', 	iconCls: 'x-fa fa-newspaper-o' }
	},{
		id			: 'REGISTRATION',
		name		: 'Registration',
		apply		: { text: 'Register now',			status: 'Registered',	iconCls: CFJS.UI.iconCls.SERVER },
		applyRework	: { text: 'Register with rework' },
		cancel		: { text: 'Decline',				status: 'Declined' }
	},{
		id			: 'REWORK',
		name		: 'Rework',	
		apply		: { text: 'Finalize',				status: 'Reworked',		iconCls: 'x-fa fa-repeat' }
	},{
		id			: 'SIGNING',
		name		: 'Signing',
		apply		: { text: 'Sign',					status: 'Signed',		iconCls: CFJS.UI.iconCls.SIGN },
		applyRework	: { text: 'Sign with rework' },
		cancel		: {	text: 'Decline', 				status: 'Declined' }
	}]
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'lifephasetypes' }));
});