Ext.define('CFJS.store.project.PrincipleTypes', {
	extend	: 'CFJS.store.Enumerated',
	alias	: 'store.principletypes',
	storeId	: 'principleTypes',
	data	: [
		{ id: 'PROJECT',	name: 'Project',	iconCls: CFJS.UI.iconCls.FOLDER,		order: 0 },
		{ id: 'PROCESS',	name: 'Process',	iconCls: CFJS.UI.iconCls.CODE_FORK,		order: 1 },
		{ id: 'TASK',		name: 'Task',		iconCls: CFJS.UI.iconCls.STICKY_NOTE,	order: 2 }
	],
	sorters: 'order'
}, function() {
	Ext.regStore(Ext.Factory.store({ type: 'principletypes' }));
});