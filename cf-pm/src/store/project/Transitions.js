Ext.define('CFJS.store.project.Transitions', {
	extend	: 'Ext.data.Store',
	alias	: 'store.transitions',
	model	: 'CFJS.model.project.Transition',
	storeId	: 'transitions',
	sorters	: [ 'name' ]
});