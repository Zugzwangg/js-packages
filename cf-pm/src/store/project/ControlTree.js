Ext.define('CFJS.store.project.ControlTree', {
	extend		: 'CFJS.store.BaseTree',
	alias		: 'store.controltree',
	requires	: [ 'CFJS.model.project.Process', 'CFJS.model.project.Task' ],
	model		: 'CFJS.model.project.Principle',
	parentFilter: 'parent.id',
	storeId		: 'controlTree',
	config		: {
		proxy : {
			loaderConfig: { dictionaryType: 'principle' },
			reader		: { 
				rootProperty: 'response.transaction.list.principle',
				typeProperty: function(data) {
					return 'CFJS.model.project.' + ((data||{}).type || 'principle').toLowerCase().replace('project', 'principle').capitalize(); 
				}
			}
		}
	},
	sorters		: [ 'name' ]
});
