Ext.define('CFJS.store.project.TaskStatuses', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.taskstatuses',
	model	: 'CFJS.model.project.TaskStatus',
	storeId	: 'taskStatuses',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'task_status' },
			reader		: { rootProperty: 'response.transaction.list.task_status' }
		}
	},
	sorters	: [ 'created' ]
});
