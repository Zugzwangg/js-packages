Ext.define('CFJS.store.project.LifePhases', {
	extend	: 'CFJS.store.Base',
	alias	: 'store.lifephases',
	model	: 'CFJS.model.project.LifePhase',
	storeId	: 'lifePhases',
	config	: {
		proxy : {
			loaderConfig: { dictionaryType: 'life_phase' },
			reader		: { rootProperty: 'response.transaction.list.life_phase' }
		}
	},
	sorters	: [ 'name' ]
});
