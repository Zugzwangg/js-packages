Ext.define('CFJS.model.project.LifePhase', {
    extend			: 'CFJS.model.project.BaseProcess',
	requires		: [ 'CFJS.model.project.ProcessPlan', 'CFJS.model.project.Workflow' ],
    dictionaryType	: 'life_phase',
	fields			: [
		{ name: 'workflow', type: 'model', critical: true, entityName: 'CFJS.model.project.Workflow' }
	]
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});