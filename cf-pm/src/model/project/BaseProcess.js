Ext.define('CFJS.model.project.BaseProcess', {
    extend			: 'CFJS.model.NamedModel',
	requires		: [
		'CFJS.model.project.ProcessMember',
		'CFJS.model.project.Transition'
	],
	schema			: 'project',
	fields			: [
		{ name: 'type',			type: 'string',	critical: true },
		{ name: 'description',	type: 'string',	critical: true },
		{ name: 'duration',		type: 'number',	critical: true },
		{ name: 'members',		type: 'array',	critical: true,	item: { entityName: 'CFJS.model.project.ProcessMember', mapping: 'process_member' } },
		{ name: 'transitions',	type: 'array',	critical: true, item: { mapping: 'transition' } }
	],
	
	asProcessPlan: function() {
		var data = this.getData();
		return new CFJS.model.project.ProcessPlan({
			type		: data.type,
			name		: data.name,
			description	: data.description,
			duration	: data.duration,
			members		: Ext.clone(data.members)
		});
	}

});