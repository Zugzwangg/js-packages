Ext.define('CFJS.model.project.ProcessPlan', {
    extend			: 'CFJS.model.project.BaseProcess',
    dictionaryType	: 'process_plan',
    isProcessPlan	: true,
	fields			: [
		{ name: 'parentId',		type: 'int',	allowNull: true },
		{ name: 'process',		type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	],
	
	asTask: function(postInfo) {
		var data = this.getData(), startDate = new Date();
		return new CFJS.model.project.Task({
			parentId	: (data.process||{}).id,
			author		: CFJS.lookupViewportViewModel().postInfoAsAuthor(postInfo),
			phase		: data.type,
			name		: data.name,
			description	: data.description,
			startDate	: startDate,
			endDate		: Ext.Date.add(startDate, Ext.Date.DAY, data.duration),
			members		: Ext.clone(data.members)
		});
	}
	
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});