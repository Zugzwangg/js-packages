Ext.define('CFJS.model.project.Process', {
    extend		: 'CFJS.model.project.Principle',
    requires	: [ 'CFJS.model.NamedModel' ],
    glyph		: 'xf126@FontAwesome',
    isProcess	: true,
	fields		: [
		{ name: 'type',			type: 'string',	critical: true,	defaultValue: 'PROCESS' },
		{ name: 'currentPhase',	type: 'model',	critical: true,	entityName: 'CFJS.model.NamedModel' },
		{ name: 'executable',	type: 'boolean' },
	],
	
	loadCurrentPhase: function(callback, session) {
		var phase = this.get('currentPhase')||{};
		if (phase && phase.id && phase.id > 0) {
			return CFJS.model.project.ProcessPlan.load(phase.id, { callback: callback }, session); 
		}
	}

}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});