Ext.define('CFJS.model.project.Task', {
    extend	: 'CFJS.model.project.Principle',
    requires: [ 'CFJS.model.NamedModel' ],
    glyph	: 'xf24a@FontAwesome',
    isTask	: true,
	fields	: [
		{ name: 'type',			type: 'string',		critical: true,	defaultValue: 'TASK' },
		{ name: 'leaf',			type: 'boolean',	persist: false,	defaultValue: true},
		{ name: 'phase',		type: 'string',		allowNull: true },
		{ name: 'numerical',	type: 'boolean',	critical: true },
		{ name: 'goal',			type: 'number',		critical: true,	defaultValue: 1 },
		{ name: 'withReport',	type: 'boolean',	critical: true }
	],
	
	asStatus: function() {
		var me = this.getData();
		return me.id && me.id > 0 ? {
			taskId		: me.id,
			author		: CFJS.lookupViewportViewModel().userInfoAsAuthor(),
			phase		: me.phase,
			numerical	: me.numerical,
			goal		: me.goal,
			progress	: me.numerical ? me.goal : null,
			created		: new Date()
		} : null;
	}

}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});