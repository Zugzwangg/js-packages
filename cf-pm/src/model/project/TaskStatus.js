Ext.define('CFJS.model.project.TaskStatus', {
    extend			: 'CFJS.model.EntityModel',
    requires		: [ 'CFJS.model.NamedModel', 'CFJS.schema.Project' ],
    dictionaryType	: 'task_status',
	schema			: 'project',
	fields			: [
		{ name: 'taskId',		type: 'int',		allowNull: true },
		{ name: 'author',		type: 'model',		critical: true,	entityName: 'CFJS.model.UserInfo' },
		{ name: 'phase',		type: 'string',		persist: false },
		{ name: 'involvement',	type: 'string',		allowNull: true },
		{ name: 'description',	type: 'string',		allowNull: true },
		{ name: 'numerical',	type: 'boolean',	persist: false },
		{ name: 'goal',			type: 'number',		persist: false },
		{ name: 'progress',		type: 'number',		allowNull: true },
		{ name: 'created',		type: 'date',		persist: false }
	]
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});