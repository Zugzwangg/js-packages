Ext.define('CFJS.model.project.Workflow', {
    extend	: 'CFJS.model.NamedModel',
	requires: [ 'CFJS.schema.Project' ],
	schema	: 'project',
	fields	: [{ name: 'initialPhase', type: 'model', critical: true, entityName: 'CFJS.model.NamedModel' }]
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});