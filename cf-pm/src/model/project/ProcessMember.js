Ext.define('CFJS.model.project.ProcessMember', {
    extend			: 'CFJS.model.EventMember',
	requires		: [ 'CFJS.schema.Project' ],
    dictionaryType	: 'process_member',
	schema			: 'project',
	fields			: [{ name: 'memberType', type: 'string', critical: true }]
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});