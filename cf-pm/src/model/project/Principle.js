Ext.define('CFJS.model.project.Principle', {
    extend	: 'CFJS.model.TreeModel',
	requires: [ 'CFJS.model.UserInfo', 'CFJS.model.project.ProcessPlan', 'CFJS.schema.Project' ],
	schema	: 'project',
	fields	: [
		{ name: 'type',			type: 'string',		critical: true,		defaultValue: 'PROJECT' },
		{ name: 'author',		type: 'model',		critical: true,		entityName: 'CFJS.model.UserInfo' },
		{ name: 'description',	type: 'string',		allowNull: true },
		{ name: 'priority',		type: 'string',		critical: true,		defaultValue: 'NORMAL' },
		{ name: 'progress',		type: 'number',		allowNull: true,	persist: false },
		{ name: 'startDate',	type: 'date',		critical: true },
		{ name: 'endDate',		type: 'date',		allowNull: true },
		{ name: 'finished',		type: 'date',		allowNull: true },
		{ name: 'created',		type: 'date',		persist: false },
		{ name: 'updated',		type: 'date',		persist: false },
		{ name: 'members',		type: 'array',		critical: true,		item: { entityName: 'CFJS.model.project.ProcessMember', mapping: 'process_member' } },
		{ name: 'overdue',		type: 'boolean',
			calculate: function(data) {
				if (Ext.isDate(data.endDate)) {
					return !Ext.Date.after(data.endDate, data.finished || new Date())
				}
				return false;
			}
		},
		{ name: 'colorCls', 	type: 'string',
			calculate: function(data) {
				return CFJS.schema.Project.priorityColorCls(data.priority);
			}
		}
	],
	
	needUpdate: function() {
		return this.$className !== 'CFJS.model.project.Principle' && !this.dirty && !this.phantom && !this.isLoading() && !this.isLoaded();
	}
	
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});