Ext.define('CFJS.model.project.PrincipleResource', {
    extend			: 'CFJS.model.webdav.SimpleResource',
	requires		: [ 'CFJS.schema.Project' ],
    dictionaryType	: 'principle_resource',
	parentType		: 'principle',
    methodDownload	: 'resource.download',
	schema			: 'project'
}, function() {
	Ext.data.schema.Schema.get('document').addEntity(Ext.ClassManager.get(this.$className));
});