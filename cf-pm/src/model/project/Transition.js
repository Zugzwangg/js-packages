Ext.define('CFJS.model.project.Transition', {
	extend		: 'Ext.data.Model',
	requires	: [ 'CFJS.schema.Project' ],
	idProperty	: 'type',
	schema		: 'project',
	fields		: [
		{ name: 'type',	type: 'string',	critical: true },
		{ name: 'data',	type: 'model',	critical: true, entityName: 'CFJS.model.NamedModel' }
	]
});